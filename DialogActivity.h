#pragma once

#include "Activity.h"

class DialogActivity : public Activity {

public:
	DialogActivity(const char *name);

	virtual const Animation *getEntranceAnimation();
	virtual const Animation *getExitAnimation();
};
