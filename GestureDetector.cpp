#include "GestureDetector.h"
#include "utils.h"
#include "DisplayManager.h"

#define CLICK_DISTANCE_TOLERANCE_CMS 0.4f

#define DRAG_MIN_DISTANCE_CMS 0.1f

#define FLICK_DURATION 100 //50
#define FLICK_MIN_DISTANCE_CMS 0.25f //0.5f

#define GESTURE_CANCEL_IDLE_DURATION_MILLIS 3000

GestureDetector::GestureDetector() {
	init();
}

GestureDetector::GestureDetector(GestureListener *gestureListener) {
	init(gestureListener);
}

void GestureDetector::init(GestureListener *gestureListener) {
	this->gestureListener = gestureListener;
	pointersDownCount = 0;
	lastZoomRatio = 1;
	touchState.reset();
	touchEvent.touchState = &touchState;
}

void GestureDetector::setGestureListener(GestureListener *gestureListener) {
	this->gestureListener = gestureListener;
}

void GestureDetector::cancelAllGestures() {

	// Finish all previous gestures (they'll no longer be delivered),
	int pointersCount = touchState.getPointersCount();
	for (int i=0; i<pointersCount; i++) {
		PointerState &pointerState = touchState.getPointerState(i);
		if (pointerState.isDown) {
			onTouchCancel(i, pointerState.x, pointerState.y);
		}
	}

	// Reset everything.
	reset();
}

void GestureDetector::onSurfaceChanged() {
	cancelAllGestures();
}

void GestureDetector::doLostTouchUpMitigation(int concernedPointerId) {

	// If this pointer is already down, we missed its touch up,
	int pointersCount = touchState.getPointersCount();
	if (pointersCount > concernedPointerId) {
		PointerState &concernedPointerState = touchState.getPointerState(concernedPointerId);
		if (concernedPointerState.isDown) {
			// Cancel its previous gestures,
			onTouchCancel(concernedPointerId, concernedPointerState.x, concernedPointerState.y);
		}
	}

	/*
	// Cancel any touches not modified for certain amount of time,
	double currentTime = getTimeMillis();
	for (int i=0; i<pointersCount; i++) {
		PointerState &pointerState = touchState.getPointerState(i);
		if (pointerState.isDown &&
			((currentTime - pointerState.lastUpdatedTimeMillis) > GESTURE_CANCEL_IDLE_DURATION_MILLIS)) {
			onTouchCancel(i, pointerState.x, pointerState.y);
		}
	}
	*/
}

bool GestureDetector::onTouchDown(int pointerId, float x, float y) {

	doLostTouchUpMitigation(pointerId);

	touchState.pointerDown(pointerId, x, y);
	pointersDownCount++;

	bool consumed = true;
	if (gestureListener) {
		touchEvent.setAsTouchDown(pointerId, x, y);
		consumed = gestureListener->onTouchEvent(&touchEvent);
	}

	if (pointersDownCount == 1) {

		singlePointerGesture = true;
		touchDownTime = getTimeMillis();
		touchDownX = x;
		touchDownY = y;
		maxDraggedDistanceCms = 0;

		flickStartTime = touchDownTime;
		flickStartX = x;
		flickStartY = y;

		pointer1Id = pointerId;
		pointer1TouchDownX = pointer1CurrentX = x;
		pointer1TouchDownY = pointer1CurrentY = y;

	} else {

		singlePointerGesture = false;
		if (pointersDownCount == 2) {
			// TODO: This should ALWAYS be pointer 2. Switch pointers if needed on touch up or cancel.
			if (pointerId == pointer1Id) {
				pointer1TouchDownX = pointer1CurrentX = x;
				pointer1TouchDownY = pointer1CurrentY = y;
			} else {
				pointer2Id = pointerId;
				pointer2TouchDownX = pointer2CurrentX = x;
				pointer2TouchDownY = pointer2CurrentY = y;
			}

			if (maxDraggedDistanceCms > DRAG_MIN_DISTANCE_CMS) {
				// Report drag end,
				// TODO: don't end drags here. Allow dragging with multiple fingers.
				if (gestureListener) {
					touchEvent.setAsTouchDragEnd(
							pointerId,
							touchDownX, touchDownY,
							x, y,
							maxDraggedDistanceCms);
					consumed |= gestureListener->onTouchEvent(&touchEvent);
				}
			}
		}
	}

	return consumed;
}

bool GestureDetector::onTouchDrag(int pointerId, float x, float y) {

	touchState.pointerMove(pointerId, x, y);

	// TODO: reconsider this,
	if (!gestureListener) return false;

	bool consumed = true;
	if (singlePointerGesture) {

		DisplayManager *displayManager = DisplayManager::getSingleton();
		float distanceWidthCms  = displayManager->pixelsToCms(x - touchDownX);
		float distanceHeightCms = displayManager->pixelsToCms(y - touchDownY);
		float draggedDistanceCms =  magnitude(distanceWidthCms, distanceHeightCms);
		if (draggedDistanceCms > maxDraggedDistanceCms) {
			maxDraggedDistanceCms = draggedDistanceCms;
		}

		if (maxDraggedDistanceCms > DRAG_MIN_DISTANCE_CMS) {

			// Flick start update (if gesture duration is more than max flick duration, account only
			// for the last max flick duration milliseconds),
			double currentTime = getTimeMillis();
			double flickDuration = currentTime - flickStartTime;
			if (flickDuration > FLICK_DURATION) {
				double ratio = FLICK_DURATION / flickDuration;
				flickStartX = x + ((flickStartX - x) * ratio);
				flickStartY = y + ((flickStartY - y) * ratio);
				flickStartTime = currentTime + ((flickStartTime - currentTime) * ratio);
			}

			// Report drag,
			touchEvent.setAsTouchDrag(
					pointerId,
					touchDownX, touchDownY,
					x, y,
					maxDraggedDistanceCms);
			consumed = gestureListener->onTouchEvent(&touchEvent);
		}

	} else {

		if (pointersDownCount == 2) {

			if (pointerId == pointer1Id) {
				pointer1CurrentX = x;
				pointer1CurrentY = y;
			} else  if (pointerId == pointer2Id) {
				pointer2CurrentX = x;
				pointer2CurrentY = y;
			}

			// Report zoom,
			float initialDistance = distance(
					pointer1TouchDownX, pointer1TouchDownY,
					pointer2TouchDownX, pointer2TouchDownY);
			float newDistance = distance(
					pointer1CurrentX, pointer1CurrentY,
					pointer2CurrentX, pointer2CurrentY);

			float zoomRatio = newDistance / initialDistance;
			if ((zoomRatio > 0) && (zoomRatio < 1000)) {
				lastZoomRatio = zoomRatio;
			}

			touchEvent.setAsZoom(
					(pointer1TouchDownX + pointer2TouchDownX) * 0.5f,
					(pointer1TouchDownY + pointer2TouchDownY) * 0.5f,
					lastZoomRatio);
			consumed = gestureListener->onTouchEvent(&touchEvent);
		}
	}

	return consumed;
}

bool GestureDetector::onTouchUp(int pointerId, float x, float y) {

	touchState.pointerUp(pointerId, x, y);
	pointersDownCount--;
	if (pointersDownCount < 0) pointersDownCount = 0;

	// TODO: reconsider this,
	if (!gestureListener)
		return false;

    touchEvent.setAsTouchUp(pointerId, x, y, maxDraggedDistanceCms);
    bool consumed = gestureListener->onTouchEvent(&touchEvent);

	if (singlePointerGesture) {

		if (maxDraggedDistanceCms <= CLICK_DISTANCE_TOLERANCE_CMS) {

			// Report click,
			touchEvent.setAsClick(pointerId, x, y);
			consumed |= gestureListener->onTouchEvent(&touchEvent);
		}

		if (maxDraggedDistanceCms >= DRAG_MIN_DISTANCE_CMS) {

			// Report drag end,
			touchEvent.setAsTouchDragEnd(
					pointerId,
					touchDownX, touchDownY,
					x, y,
					maxDraggedDistanceCms);
			consumed |= gestureListener->onTouchEvent(&touchEvent);

			// Flick start update (if gesture duration is more than max flick duration, account only
			// for the last max flick duration milliseconds),
			double currentTime = getTimeMillis();
			double flickDuration = currentTime - flickStartTime;
			if (flickDuration > FLICK_DURATION) {
				double ratio = FLICK_DURATION / flickDuration;
				flickStartX = x + ((flickStartX - x) * ratio);
				flickStartY = y + ((flickStartY - y) * ratio);
				flickStartTime = currentTime + ((flickStartTime - currentTime) * ratio);
			}

			// Get flick distance,
			DisplayManager *displayManager = DisplayManager::getSingleton();
			float flickDistanceWidthCms  = displayManager->pixelsToCms(x - flickStartX);
			float flickDistanceHeightCms = displayManager->pixelsToCms(y - flickStartY);
			float flickDistanceCms =  magnitude(flickDistanceWidthCms, flickDistanceHeightCms);
			if (flickDistanceCms >= FLICK_MIN_DISTANCE_CMS) {

				// Report a flick,
				touchEvent.setAsFlick(
							pointerId,
							x, y,
							(x - flickStartX) / FLICK_DURATION,
							(y - flickStartY) / FLICK_DURATION);
				consumed |= gestureListener->onTouchEvent(&touchEvent);
			}
		}
 	} else if (pointersDownCount == 1) {

		float initialDistance = distance(
				pointer1TouchDownX, pointer1TouchDownY,
				pointer2TouchDownX, pointer2TouchDownY);
		float newDistance = distance(
				pointer1CurrentX, pointer1CurrentY,
				pointer2CurrentX, pointer2CurrentY);

		float zoomRatio = initialDistance / newDistance;
		if ((zoomRatio > 0) && (zoomRatio < 1000)) {
			lastZoomRatio = zoomRatio;
		}

		touchEvent.setAsZoomEnd(
				(pointer1TouchDownX + pointer2TouchDownX) * 0.5f,
				(pointer1TouchDownY + pointer2TouchDownY) * 0.5f,
				lastZoomRatio);
		consumed |= gestureListener->onTouchEvent(&touchEvent);

		lastZoomRatio = 1;
	}

	return consumed;
}

bool GestureDetector::onTouchCancel(int pointerId, float x, float y) {

	// TODO: do special handling to terminate individual gestures,
	return onTouchUp(pointerId, x, y);
}

bool GestureDetector::onScroll(float x, float y, float xOffset, float yOffset) {

	if (!gestureListener) {
		return false;
	}

	DisplayManager *displayManager = DisplayManager::getSingleton();
	touchEvent.setAsScroll(x, y, xOffset, yOffset);
	return gestureListener->onTouchEvent(&touchEvent);
}
