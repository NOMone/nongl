#include "Achievements.h"

#include "OnScreenNotification.h"
#include "SystemInterface.h"
#include "SystemGameServices.h"

using namespace Ngl;

namespace {

    ////////////////////////////////////////////
    // On achievement submitted task
    ////////////////////////////////////////////

    // TODO: struct
    class OnAchievementSubmittedTask : public Runnable {
    public:
        bool showAchievements=false;
        int queuedSubmittedAchievementsCount=0;
        bool run(void *data=0);
    };

    Ngl::shared_ptr<OnAchievementSubmittedTask> onAchievementSubmittedTask{new OnAchievementSubmittedTask()};

    bool OnAchievementSubmittedTask::run(void *) {

        queuedSubmittedAchievementsCount--;
        if (showAchievements && (!queuedSubmittedAchievementsCount)) {
            showAchievements = false;
            #if NONGL_GAME_SERVICES
            systemGameServicesShowServicesDialog();
            #endif
        }
        return true;
    }

    ////////////////////////////////////////////
    // Set show achievements flag task
    ////////////////////////////////////////////

    class SetShowAchievementsFlagTask : public Runnable {
    public:
        bool run(void *data);
    };
    Ngl::shared_ptr<SetShowAchievementsFlagTask> setShowAchievementsFlagTask{new SetShowAchievementsFlagTask()};

    bool SetShowAchievementsFlagTask::run(void *) {

        // When all the achievements have been successfully submitted, show
        // the service achievements dialog,
        onAchievementSubmittedTask->showAchievements = true;

        // There is a good chance that all the queued achievements have already
        // been submitted. In such case, simulate the case where an achievement
        // has just been submitted to trigger showing the service achievements
        // dialog,
        onAchievementSubmittedTask->queuedSubmittedAchievementsCount++;
        onAchievementSubmittedTask->run();
        return true;
    }
}

////////////////////////////////////////////
// Achievements
////////////////////////////////////////////

void Achievements::declareAchievement(
        const char *preferenceName,
        const char *achievementName,
        const TextBuffer &notificationMessage,
        const Ngl::TextureRegion *notificationIcon) {
    
    if (!javaGetBooleanPreference(preferenceName, false)) {
        
        javaSetBooleanPreference(preferenceName, true);

        #if NONGL_GAME_SERVICES
        onAchievementSubmittedTask->queuedSubmittedAchievementsCount++;
        systemGameServicesSubmitAchievement(achievementName, 100.0f, false, onAchievementSubmittedTask);
        #endif

        OnScreenNotificationData notificationData(
                notificationMessage,
                notificationIcon,
                4000);
        notificationData.setOnTouchedTask(setShowAchievementsFlagTask);
        OnScreenNotification::queueOnScreenNotification(notificationData);
    }
}
