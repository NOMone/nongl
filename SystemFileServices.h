#pragma once

#include <stdint.h>
#include <vector>

namespace Ngl {

	class File;

	bool systemFileExists(const Ngl::File &file);
	int32_t systemGetFileSize(const Ngl::File &file);
	void systemMakeDir(const Ngl::File &file);

	int32_t systemReadFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile=0, int32_t maxReadSize=0); // Returns read bytes count.
	int32_t systemWriteFile(const Ngl::File &file, const void *data, int32_t sizeBytes, bool append=false); // Returns written bytes count.
}
