#include "ScreenShotActivity.h"

#include "FrameBuffer.h"
#include "SystemInterface.h"
#include "SystemLog.h"
#include "PixelMap.h"
#include "DraggableLayout.h"
#include "TextUtils.h"
#include "DisplayManager.h"
#include "File.h"

using namespace Ngl;

const char *ScreenShotActivity::activityName = "Screen shot activity";
ScreenShotActivity::ScreenShotActivity(int screenShotWidth, int screenShotHeight) : DialogActivity(activityName) {

	setConsumesAllInputs(false);

	// Setup screenShot frame buffer,
	// Set screen shot dimensions,
	if (!screenShotWidth ) screenShotWidth  = DisplayManager::getSingleton()->getDisplayWidth ();
	if (!screenShotHeight) screenShotHeight = DisplayManager::getSingleton()->getDisplayHeight();
	this->screenShotWidth  = screenShotWidth ;
	this->screenShotHeight = screenShotHeight;

	// Create pixel map to hold the screen shot,
    pixelMap = new PixelMap(screenShotWidth, screenShotHeight);

	// Add a capture button,
	captureButton = new Button({"Capture", 0.4f});
	captureButton->setListener(this);

	DraggableLayout *draggableLayout = new DraggableLayout(0, 0, captureButton->getWidth(), captureButton->getHeight());
	draggableLayout->addView(captureButton);
	rootLayout.addView(draggableLayout);
}

ScreenShotActivity::~ScreenShotActivity() {
	delete pixelMap;
}

void ScreenShotActivity::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {
	DialogActivity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);

	// If this is the last living activity, it should finish as well,
	if (Nongl::getActivitiesCount() == 1) finish();
}

void ScreenShotActivity::buttonOnClick(Button *button) {

    // Create a frame buffer with the specified size,
    if (!frameBuffer) frameBuffer = new FrameBuffer(screenShotWidth, screenShotHeight, true);

	// Take screenshot,
	captureButton->setVisible(false);
	Nongl::bindFrameBuffer(frameBuffer);
	Nongl::onDrawFrame();
	pixelMap->readFromFrameBuffer(0, 0);
	Nongl::bindFrameBuffer(0);
	captureButton->setVisible(true);

	// Save screen shot,
    static int screenShotNumber = 0;
    TextBuffer fileName;
    do {
        fileName.clear();
        fileName.append(++screenShotNumber);
        while (fileName.getTextLength() < 4) fileName.prepend('0');
        fileName.prepend("screenShots/").append(".png");
    } while (Ngl::File(Ngl::FileLocation::SDCARD, fileName).exists());

    LOGE("Screen shot: %s", fileName.getText());

    pixelMap->saveToFile(Ngl::File(Ngl::FileLocation::SDCARD, fileName), true);

    // Update clock as if this happened in 0 time,
    Nongl::tickTheClock();
}
