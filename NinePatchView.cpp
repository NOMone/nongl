#include "NinePatchView.h"

void NinePatchView::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool transparent) {

	float spriteRotation = sprite.getRotation();
	if (spriteRotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(spriteRotation);
		float sinAngle = sinf(spriteRotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		sprite.setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);

		// Draw,
		if (drawAllInOrder) {
			sprite.drawAllInOrder(spriteBatch);
		} else if (transparent) {
			sprite.drawTransparentParts(spriteBatch);
		} else {
			sprite.drawOpaqueParts(spriteBatch);
		}

		// Restore original position,
		sprite.setCenter(centerX, centerY);

	} else {

		// Just draw,
		if (drawAllInOrder) {
			sprite.drawAllInOrder(spriteBatch);
		} else if (transparent) {
			sprite.drawTransparentParts(spriteBatch);
		} else {
			sprite.drawOpaqueParts(spriteBatch);
		}
	}
}

void NinePatchSprite::attachToAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += x;
				currentComponent->setTargetAddress(&x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += y;
				currentComponent->setTargetAddress(&y);
				break;

			case AnimationTarget::YAW:
				currentComponent->setTargetAddress(&rotation);
				break;

			case AnimationTarget::COLOR_MASK:
				// TODO:
				//currentComponent->setTargetAddress(&sprite.colorMask, true);
				break;

			default:
				break;
		}
	}
}
