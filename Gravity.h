#pragma once

class Orientation {
	inline Orientation() {}
	public: enum Value {
		VERTICAL, HORIZONTAL
	};
};

class HorizontalGravity {
	inline HorizontalGravity() {}
	public: enum Value {
		LEFT, CENTER, RIGHT
	};
};

class VerticalGravity {
	inline VerticalGravity() {}
	public: enum Value {
		TOP, CENTER, BOTTOM
	};
};

class Gravity {
	inline Gravity() {}
	public:
	enum Value {
		TOP_LEFT, TOP_CENTER, TOP_RIGHT,
		CENTER_LEFT, CENTER, CENTER_RIGHT,
		BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
	};

	static void getGravityComponents(Gravity::Value gravity, VerticalGravity::Value *outVerticalGravity, HorizontalGravity::Value *outHorizontalGravity);
	static VerticalGravity::Value getVerticalGravity(Gravity::Value gravity);
	static HorizontalGravity::Value getHorizontalGravity(Gravity::Value gravity);
	static Gravity::Value getGravityFromComponents(VerticalGravity::Value verticalGravity, HorizontalGravity::Value horizontalGravity);
};
