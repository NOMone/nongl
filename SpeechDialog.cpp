#include <Nongl/SpeechDialog.h>

#include <Nongl/DisplayManager.h>
#include <Nongl/LinearLayout.h>
#include <Nongl/AbsoluteLayout.h>
#include <Nongl/NinePatchView.h>
#include <Nongl/ImageView.h>
#include <Nongl/TextView.h>

#include <string.h>
#include <stdio.h>

using namespace Ngl;

//////////////////////////////
// Text animation
//////////////////////////////

#define ESCAPE_CHARACTER '#'

void TextAnimation::reset() {

	// NOTE: doesn't free allocated text.
	this->text = 0;
	this->visibleText = 0;
	this->formattedText = 0;
	this->currentChar = 0;
	this->currentVisibleChar = 0;
	this->charDelay = 0;
	this->delay = 0;
	this->remainingTime = 0;
}

void TextAnimation::pushVisibleChar(char character) {

	// Apply the formatted text,
	if (formattedText) {
		while (formattedText[currentVisibleChar] != character) {
			visibleText[currentVisibleChar] = formattedText[currentVisibleChar];
			currentVisibleChar++;
		}
	}

	// Push the character,
	visibleText[currentVisibleChar] = character;
	currentVisibleChar++;
}

void TextAnimation::removeEscapeSequencesFromText(char *text) {

	int currentChar = 0;
	int indexDisplacement = 0;

	while (text[currentChar]) {

		if (text[currentChar] == ESCAPE_CHARACTER) {
			if (text[currentChar+1] == ESCAPE_CHARACTER) {
				text[currentChar-indexDisplacement] = ESCAPE_CHARACTER;
				indexDisplacement++;
				currentChar+=2;

			} else {
				int parameterLength = 0;
				int index = currentChar+1;
				while (text[index] && (text[index] != 'r') && (text[index] != 'd')) {
					parameterLength++;
					index = currentChar + 1 + parameterLength;
				}
				if (!text[index]) {
					text[currentChar-indexDisplacement] = text[currentChar];
					currentChar++;
					continue;
				}

				indexDisplacement += parameterLength + 2;
				currentChar += parameterLength + 2;
			}
		} else {
			text[currentChar-indexDisplacement] = text[currentChar];
			currentChar++;
		}
	}
	text[currentChar-indexDisplacement] = 0;
}

bool TextAnimation::processEscapeSequence(const char *text) {

	if (text[0] == ESCAPE_CHARACTER) {
		if (text[1] == ESCAPE_CHARACTER) {
			pushVisibleChar(ESCAPE_CHARACTER);
			currentChar += 2;
			return true;
		} else {

			float parameter=0;
			sscanf(&text[1], "%f", &parameter);

			int parameterLength = 0;
			while (text[1+parameterLength] && (text[1+parameterLength] != 'r') && (text[1+parameterLength] != 'd')) {
				parameterLength++;
			}
			if (!text[1+parameterLength]) return false;

			if (text[1+parameterLength] == 'r') {
				charDelay = parameter;
			} else if (text[1+parameterLength] == 'd') {
				delay += parameter;

				// Subtract elapsed delay time,
				remainingTime -= delay;
				if (remainingTime >= 0) {
					delay = 0;
				} else {
					delay = -remainingTime;
					remainingTime = 0;
				}
			}

			currentChar += 2 + parameterLength;
			return true;
		}
	}

	return false;
}

void TextAnimation::update(float elapsedTimeMillis) {

	// If there is any text unprocessed yet,
	if (text[currentChar]) {

		// Add elapsed time to processing time,
		remainingTime += elapsedTimeMillis;

		// Subtract elapsed delay time,
		remainingTime -= delay;
		if (remainingTime >= 0) {
			delay = 0;
		} else {
			delay = -remainingTime;
			remainingTime = 0;
		}

		// Move characters one by one to the visible buffer,
		while (text[currentChar] && (remainingTime > charDelay)) {

			// Check for escaping,
			if (processEscapeSequence(&text[currentChar])) continue;

			remainingTime -= charDelay;
			pushVisibleChar(text[currentChar]);
			currentChar++;
		}
		visibleText[currentVisibleChar] = 0;
	}
}

const char *TextAnimation::getText() {
	return visibleText;
}

void TextAnimation::setText(const char *text, const char *formattedText) {

	// Delete any previous text set,
	if (this->text) {
		delete[] this->text;
		delete[] this->visibleText;
		if (this->formattedText) delete[] this->formattedText;
	}

	// Reset everything,
	reset();

	// Store a copy of the text,
	int32_t textLength = (int32_t) strlen(text)+1;
	this->text = new char[textLength];
	strcpy(this->text, text);

	// Allocate space for visible text and copy formatted text,
	if (formattedText) {
		int32_t formattedTextLength = (int32_t) strlen(formattedText)+1;
		this->visibleText = new char[formattedTextLength];
		this->formattedText = new char[formattedTextLength];
		strcpy(this->formattedText, formattedText);
	} else {
		this->visibleText = new char[textLength];
		this->formattedText = 0;
	}
}

void TextAnimation::finish() {
	currentChar = (int32_t) strlen(text);

	strcpy(visibleText, formattedText);
	currentVisibleChar = (int32_t) strlen(formattedText);
}

//////////////////////////////
// Speech dialog
//////////////////////////////

#define SPEECH_DIALOG_OUTER_HEIGHT 340
#define SPEECH_DIALOG_INNER_HEIGHT 242

const char *SpeechDialog::activityName = "Speech dialog";

SpeechDialog::SpeechDialog() : DialogActivity(activityName) {

	this->setConsumesAllInputs(true);
	this->setBuffersUnconsumedInputs(false);

	this->onDialogDismissedListener = 0;
	this->onDialogDismissedListenerData = 0;
	this->readyForInput = true;
	this->backPressed = false;
	this->autoSkip = false;

	// Setup exit animation,
	setupDialogAnimations();

	// Initialize GUI,
	DisplayManager *displayManager = DisplayManager::getSingleton();

	// Layout to set dialog position,
	dialogPositionLayout = new LinearLayout(
			0, 0, displayManager->designWidth, displayManager->designHeight,
			Orientation::HORIZONTAL, Gravity::TOP_LEFT, 0);
	rootLayout.addView(dialogPositionLayout);

	// Dialog root layout,
	AbsoluteLayout *dialogRootLayout = new AbsoluteLayout(0, 0, dialogPositionLayout->getWidth(), SPEECH_DIALOG_OUTER_HEIGHT);
	dialogPositionLayout->addView(dialogRootLayout);

	// Dialog background,
	dialogBackground = new NinePatchView();
	dialogRootLayout->addView(dialogBackground);

	// Portrait and text layout,
	portraitAndTextLayout = new LinearLayout(
			0, 0, dialogRootLayout->getWidth(), SPEECH_DIALOG_INNER_HEIGHT,
			Orientation::HORIZONTAL, Gravity::BOTTOM_LEFT, 10);
	portraitAndTextLayout->setBottom(dialogRootLayout->getBottom());
	dialogRootLayout->addView(portraitAndTextLayout);

	// Character portrait,
	characterImage = new ImageView();

	// Dialog text,
	dialogTextView = new TextView(
			0, 0,
			portraitAndTextLayout->getWidth () - characterImage->getScaledWidth () - 45,
			portraitAndTextLayout->getHeight(),
			Gravity::CENTER_LEFT, 0xffff0000);
	dialogTextView->setTextScale(2, 2);

	portraitAndTextLayout->addView(characterImage);
	portraitAndTextLayout->addView(dialogTextView);
}

void SpeechDialog::setupDialogAnimations() {

	Interpolator interpolator(125);

	// Entrance animation,
	// Alpha,
	entranceAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			interpolator,
			0, 0x00ffffff, false,
			0, 0xffffffff, false);

	// Exit animation,
	// Alpha,
	exitAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			interpolator,
			0, 0xffffffff, false,
			0, 0x00ffffff, false);

	// Transparent delay,
	interpolator.set(125, 125);
	exitAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			interpolator,
			0, 0x00000000, false,
			0, 0x00000000, false);
}

void SpeechDialog::set(
		const Ngl::TextureRegion *characterPicture, const NinePatchData *background,
		Gravity::Value gravity, const char *text, int textColor) {

	setGravity(gravity);
	setBackground(background);
	setCharacterPortrait(characterPicture);
	setTextColor(textColor);
	updateLayout();
	setText(text);
}

void SpeechDialog::setGravity(Gravity::Value gravity) {
	dialogPositionLayout->setGravity(gravity);
}

void SpeechDialog::setBackground(const NinePatchData *background) {
	dialogBackground->init(background);
}

void SpeechDialog::setCharacterPortrait(const Ngl::TextureRegion *characterPortrait) {
	characterImage->set(characterPortrait);
}

void SpeechDialog::setText(const char *text) {

	// You the layout should be updated before setting text because it
	// affects the text animation.

	// Get the text without escape sequences,
	std::vector<char> unescapedText;
	unescapedText.resize(strlen(text)+1);
	std::copy(text, text+unescapedText.size(), unescapedText.begin());
	textAnimation.removeEscapeSequencesFromText(&unescapedText[0]);

	// Get the formatted text,
	std::vector<char> formattedText;
	dialogTextView->getFormattedText(&unescapedText[0], formattedText);

	// Setup text animation,
	textAnimation.setText(text, &formattedText[0]);
}

void SpeechDialog::setTextColor(int textColor) {
	dialogTextView->textColor = textColor;
}

void SpeechDialog::setTextScale(float scaleX, float scaleY) {
    dialogTextView->setTextScale(scaleX, scaleY);
}

void SpeechDialog::updateLayout() {

	// Background,
	dialogBackground->setWidth(DisplayManager::getSingleton()->designWidth);
	dialogBackground->setHeight(SPEECH_DIALOG_INNER_HEIGHT);

	// Portrait and text layout,
	Gravity::Value gravity = dialogPositionLayout->getGravity();
	portraitAndTextLayout->setGravity(Gravity::getGravityFromComponents(
			VerticalGravity::BOTTOM,
			Gravity::getHorizontalGravity(gravity)));

	// Dialog text,
	dialogTextView->setWidth(dialogBackground->getWidth() - characterImage->getScaledWidth () - 45);

	// Add portrait and text in the correct order,
	portraitAndTextLayout->removeView(characterImage);
	portraitAndTextLayout->removeView(dialogTextView);

	if (Gravity::getHorizontalGravity(gravity) == HorizontalGravity::LEFT) {
		portraitAndTextLayout->addView(characterImage);
		portraitAndTextLayout->addView(dialogTextView);
	} else {
		portraitAndTextLayout->addView(dialogTextView);
		portraitAndTextLayout->addView(characterImage);
	}

	rootLayout.layout();
}

void SpeechDialog::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {

	// Update text animation,
	textAnimation.update(elapsedTimeMillis);
	const char *text = textAnimation.getText();
	if (text) dialogTextView->setText(text);

	// Draw normally,
	DialogActivity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
}

void SpeechDialog::sendToBack() {
	if (readyForInput) {
		if (textAnimation.hasFinished()) {
			readyForInput = false;
			//Nongl::sendActivityToBack(activityName);
			Nongl::scheduler.schedule(Ngl::shared_ptr<Runnable>(new RunnableWrapper(this)), 150);
		} else {
			textAnimation.finish();
		}
	}
}

bool SpeechDialog::onBackPressed() {
	backPressed = true;
	sendToBack();
	return true;
}

bool SpeechDialog::onTouchEvent(const TouchEvent *event) {
	if (event->type == TouchEvent::TOUCH_DOWN) sendToBack();
	return true;
}

void SpeechDialog::onShowed() {
	if (autoSkip) sendToBack();
}

bool SpeechDialog::run(void *data) {

	if (onDialogDismissedListener) onDialogDismissedListener->run(onDialogDismissedListenerData);

	readyForInput = true;
	return true;
}
