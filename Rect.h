#pragma once

namespace Ngl {

	struct IntersectionResult;

	class Rect {
	public:
		float left, bottom, right, top;

		Rect() { clear(); }
		Rect(float left, float bottom, float right, float top);

		void clear() { left = right = bottom = top = 0; }

		IntersectionResult intersect(const Rect &rect) const;
		bool intersects(const Rect &rect) const;
	};

	struct IntersectionResult {
		bool intersecting;
		Rect intersection;
	};
}
