#include "ByteBuffer.h"

#include <stdexcept>

/////////////////////////////////
// Byte buffer input stream
/////////////////////////////////

int32_t Ngl::ByteBufferInputStream::readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) {
	if (byteBuffer.expired()) throw std::runtime_error("Attempt to read from an expired buffer.");

	std::vector<uint8_t> &bufferVector = byteBuffer->getVector();
	int32_t dataSize = ((int32_t) bufferVector.size()) - currentByteIndex;
	outputVector.insert(outputVector.begin()+offsetInVector, bufferVector.begin()+currentByteIndex, bufferVector.end());
	currentByteIndex += dataSize;
	return dataSize;
}

int32_t Ngl::ByteBufferInputStream::readData(OutputStream &outputStream) {
	if (byteBuffer.expired()) throw std::runtime_error("Attempt to read from an expired buffer.");

	std::vector<uint8_t> &bufferVector = byteBuffer->getVector();
	int32_t dataSize = ((int32_t) bufferVector.size()) - currentByteIndex;
	outputStream.writeData(&bufferVector[currentByteIndex], dataSize);
	currentByteIndex += dataSize;
	return dataSize;
}

bool Ngl::ByteBufferInputStream::hasData() {
	if (byteBuffer.expired()) throw std::runtime_error("Attempt to access an expired buffer.");

	return byteBuffer->getVector().size() > currentByteIndex;
}

/////////////////////////////////
// Byte buffer output stream
/////////////////////////////////

void Ngl::ByteBufferOutputStream::writeData(const void *data, int32_t sizeBytes) {
	if (byteBuffer.expired()) throw std::runtime_error("Attempt to write to an expired buffer.");
	byteBuffer->pushData(data, sizeBytes);
}

int32_t Ngl::ByteBufferOutputStream::writeData(InputStream &inputStream) {
	if (byteBuffer.expired()) throw std::runtime_error("Attempt to write to an expired buffer.");
	return inputStream.readData(byteBuffer->getVector());
}

