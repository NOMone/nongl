#pragma once

#include "Scheduler.h"
#include "FrameTimeTracker.h"
#include "Animation.h"
#include <vector>

class Activity;
class GestureDetector;
class GestureListener;
class TouchEvent;
class Shader;
class PlainShader;
class SeparateAlphaShader;
class FrameBuffer;
class AudioManager;

namespace Ngl {
	class TextureRegion;
}

class Nongl {

	inline Nongl() {}

    static bool mainCalled;
    static int pausedInternallyCount;
    static bool pausedExternally;
	static bool exitRecieved;

	// Use shared_ptr for default resources...?
	static GestureDetector *gestureDetector;
	static GestureListener *gestureListener;
	static bool discardTouchEventsTillFrameDraw;

	static PlainShader *plainShader;
	static SeparateAlphaShader *separateAlphaShader;
	static Shader *lastUsedShader;

	static int32_t clearColor;
	static bool glBlendEnabled;
	static bool glDepthTestEnabled;

	static FrameBuffer *defaultFrameBuffer;
	static FrameBuffer *boundFrameBuffer;

	static int contextLostCount;
	static std::vector<Activity *> activities;
	static std::vector<Activity *> alwaysOnTopActivities;
	static int topActivityStackIndex;
	static int bottomActivityStackIndex;

	static Ngl::TextureRegion *_whiteBlockTextureRegion;

	static Animation defaultActivityEntranceAnimation, defaultActivityExitAnimation;
	static Animation defaultDialogEntranceAnimation, defaultDialogExitAnimation;

	static FrameTimeTracker frameTimeTracker;
	static float maxAllowedFrameDurationMillis;

	static void hideAllActivities(Activity *exception);
	static void sortActivities();
	static void bringAlwaysOnTopActivitiesToFront();

public:

	// TODO: reconsider what's public and what's private...

	static AudioManager *audioManager;
	static Scheduler scheduler;

	static Ngl::TextureRegion * const & whiteBlockTextureRegion;

	static void main();

	// Application life events,
	static void initialize();
	static void onSurfaceChanged(int width, int height);
	static void onSurfaceCreated();
	static void discardOpenGlObjects();
    static void tickTheClock();
	static void onNewFrame();
	static void onDrawFrame();
	static void onPause(bool external);
	static void onResume(bool external);

    static void destroy();
	static inline void exit() { exitRecieved = true; }  // This should NEVER be called by library users. Use
														// javaFinishActivity() instead.

	// Input events,
	static bool onBackPressed();
	static bool onMenuPressed();
	static void onMenuItemSelected(int itemId);
	static bool onTouchDown(int pointerId, float x, float y);
	static bool onTouchDrag(int pointerId, float x, float y);
	static bool onTouchUp(int pointerId, float x, float y);
	static bool onTouchCancel(int pointerId, float x, float y);
	static bool onScroll(float x, float y, float xOffset, float yOffset);
	static bool onTouchEvent(const TouchEvent *event);
	static bool onKeyDown(int keyCode);
	static bool onKeyUp(int keyCode);

	// Opengl state,
	static void setClearColor(int32_t clearColor) { Nongl::clearColor = clearColor; }
	static void setBlendEnabled(bool enabled);
	static void setDepthTestEnabled(bool enabled);
	static inline bool getBlendEnabled() { return glBlendEnabled; }
	static inline bool getDepthTestEnabled() { return glDepthTestEnabled; }

	static void bindFrameBuffer(FrameBuffer *frameBuffer);
	static inline FrameBuffer *getBoundFrameBuffer() { return boundFrameBuffer; }

	static PlainShader *getDefaultPlainShader();
	static SeparateAlphaShader *getDefaultSeparateAlphaShader();
	static inline Shader *getLastUsedShader() { return lastUsedShader; }
	static inline void setLastUsedShader(Shader *shader) { lastUsedShader = shader; }

	// Activities,
	static inline Animation *getDefaultActivityEntranceAnimation() { return &defaultActivityEntranceAnimation; }
	static inline Animation *getDefaultActivityExitAnimation() { return &defaultActivityExitAnimation; }
	static inline Animation *getDefaultDialogEntranceAnimation() { return &defaultDialogEntranceAnimation; }
	static inline Animation *getDefaultDialogExitAnimation() { return &defaultDialogExitAnimation; }

	static inline int getContextLostCount() { return contextLostCount; } // Shouldn't be called by the library users.
	static bool getActivityHiddenState(Activity *activity); // Shouldn't be called by the library users.
	static void updateActivitiesHiddenState(Activity *activityToSkip=0); // Shouldn't be called by the library users.

	static Activity *getActivity(const char *activityName);
	static inline int32_t getActivitiesCount() { return (int32_t) activities.size(); }
	static Activity *getFrontActivity();
	static void startActivity(Activity *activity, bool scheduleOnNextFrame=true);
	static void bringActivityToFront(Activity *activity, bool scheduleOnNextFrame=true);
	static Activity *bringActivityToFront(const char *activityName, bool scheduleOnNextFrame=true);
	static void sendActivityToBack(Activity *activity, bool scheduleOnNextFrame=true);
	static Activity *sendActivityToBack(const char *activityName, bool scheduleOnNextFrame=true);
	static void setActivityAlwaysOnTop(Activity *activity, bool alwaysOnTop=true);
	static void removeActivity(Activity *activity);
	static bool isActivityInputBlocked(const Activity *activity);

	static inline float getFrameTime() {
		float elapsedTimeMillis = frameTimeTracker.getElapsedTimeMillis();
		return (elapsedTimeMillis > maxAllowedFrameDurationMillis) ? maxAllowedFrameDurationMillis : elapsedTimeMillis;
	}
};
