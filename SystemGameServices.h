#pragma once

// For now, this is supported only on iOS and Android through Game Center and Google Play Services respectively.
#if NONGL_GAME_SERVICES

#include "TextUtils.h"

namespace Ngl {
	template <typename T>
	class ManagedPointer;
}
class Runnable;

/////////////////////////////
// Achievement
/////////////////////////////

class Achievement {
    TextBuffer identifier;
    float percentCompleted;
public:
    inline Achievement(const char *identifier, float percentCompleted) {
        this->identifier.append(identifier);
        this->percentCompleted = percentCompleted;
    }
    inline const TextBuffer &getIdentifer() const { return identifier; }
    inline float getPercentCompleted() const { return percentCompleted; }
};

/////////////////////////////
// API
/////////////////////////////

// Connects to the games service without showing sign in dialog,
void systemGameServicesConnect();

// If connected to game services after delay, show sign in dialog. Otherwise,
// show it as soon as connection is completed,
void systemGameServicesShowSignInDialog(float delayMillis=0);

// Returns whether the sign in completed or not. Sign in could be completed
// without showing any sign in dialogs. It is also considered completed if
// the sign in dialog was dismissed even if the user didn't actually sign in,
bool systemGameServicesIsSignInComplete();

// Returns whether the user is signed in or not,
bool systemGameServicesIsUserSignedIn();

// Show the game services main-menu or whatever is equivalent,
void systemGameServicesShowServicesDialog();

void systemGameServicesSubmitAchievement(const char *achievementIdentifier, float percent, bool showBanner=false);
void systemGameServicesSubmitAchievement(const char *achievementIdentifier, float percent, bool showBanner, const Ngl::ManagedPointer<Runnable> &onSubmittedTask, void *onSubmittedTaskData=0);
void systemGameServicesResetAchievements();
void systemGameServicesGetAchievements(const Ngl::ManagedPointer<Runnable> &onAchievementsLoadedTask);

#endif
