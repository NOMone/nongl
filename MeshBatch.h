#pragma once

#include <stdint.h>
#include <vector>

#include "ManagedObject.h"

// TODO: attempt removing this,
//#include "Mesh.h"

namespace Ngl {

	template <typename VertexType>
	class Mesh;
	class Material;

	class MeshBatch {

		// TODO: make material const (?)
		shared_ptr<Material> material;
		bool blendEnabled=false;

		// VBO data,
		bool vboPowered = false;
		bool isStatic = false;

		int32_t verticesVboId=0;
		int32_t indicesVboId=0;

		int32_t verticesVboSizeBytes=0;
		int32_t indicesVboSizeShorts=0;

		bool vbosModified=false;

		// Batching data,
		int32_t batchedVerticesSizeBytes=0;
		int32_t batchedVerticesCount=0;
		int32_t batchedIndicesCount=0;

		// TODO: add "batched" to the name,
		std::vector<uint8_t> verticesData;
		std::vector<uint16_t> indices;


	public:

		int32_t flushesDueToMaterialChangeCount=0;

		MeshBatch(bool vboPowered=false, bool isStatic=false);
		~MeshBatch();

		void prepareForBatch(const shared_ptr<Material> &material, int32_t verticesCount, int32_t indicesCount);

		template <typename VertexType>
		void batchMesh(const Mesh<VertexType> &mesh) {
			mesh.draw(*this);
		}

		void batch(
				const shared_ptr<Material> &material,
				const uint8_t *formattedVerticesData, int32_t verticesCount,
				const uint16_t *indices, int32_t indicesCount);

		void clear();

		void onVbosDiscarded();
		void adjustAndPrepareVbos();

		void flush();
		void changeBlendMode(bool enabled);
		void draw();
	};
}
