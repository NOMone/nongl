#ifndef GESTURE_LISTENERS_GROUP_H
#define GESTURE_LISTENERS_GROUP_H

#include "GestureListener.h"
#include <vector>

class GestureListenersGroup : public GestureListener {

	std::vector<GestureListener *> gestureListeners;

public:

	void addGestureListener(GestureListener *gestureListener);
	void removeGestureListener(GestureListener *gestureListener);

	virtual bool onTouchEvent(const TouchEvent *event);
};

#endif
