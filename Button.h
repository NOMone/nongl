#pragma once

#include "View.h"
#include "AbstractSprite.h"
#include "Animation.h"
#include "Font.h"

class Button;
class TextView;
class SpriteBatch;
class Sound;

class ButtonListener {
public:
	inline virtual ~ButtonListener() {}
	inline virtual void buttonOnClick(Button *button) {}
	inline virtual void buttonOnPressed(Button *button) {}
	inline virtual void buttonOnReleased(Button *button) {}
};

class ButtonParams {
public:
	float x, y;
	float width, height;
	const char *text;
	float rotation;
	int color;
	int textColor;
	float textScaleX, textScaleY;
	float textPaddingX, textPaddingY;
	const Ngl::TextureRegion *buttonUpTextureRegion, *buttonDownTextureRegion;
	const SegmentedSpriteParameters *buttonUpSegmentedSpriteParams, *buttonDownSegmentedSpriteParams;
	const NinePatchData *buttonUpNinePatchData, *buttonDownNinePatchData;
	ButtonParams();
	ButtonParams(const Ngl::TextureRegion *buttonUpTextureRegion);
	ButtonParams(const char *text, float textScale=1.0f, float maxWidth=10000);
};

class Button : public View {

	float localAnimationScaleX, localAnimationScaleY;
	float localAnimationX, localAnimationY;

protected:

	TextView *textView;

	static Sound *clickSound;
	bool consumesTouches;   // TODO: what is the purpose of this .. ?
	bool pressed;
	int32_t pressingPointerId;

	ButtonListener *buttonListener;
	AbstractSprite downSprite, upSprite, *currentSprite;

	TouchState *touchState;

	static const NinePatchData *getDefaultNinePatchData();
	void attachToLocalAnimation(Animation *animation);

public:

	float x, y;
	float maxZ, minZ;
	float width, height;
	float scaleX, scaleY;
	float rotation;
	float rotationPivotXDisplacement, rotationPivotYDisplacement;
	int color, textColor;
	Font *font=0;
	int colorMask;
	float textScaleX, textScaleY;
	float textPaddingX, textPaddingY;
	Animation onPressAnimation, onReleaseAnimation;

	int tag;

	Button(const ButtonParams *parameters=0);
	Button(const ButtonParams &parameters);
	Button(float x, float y, const char *text=0);
	~Button();

	void set(const ButtonParams *parameters);

	/*
	virtual void set(
			float x, float y,
			float width, float height,
			const char *text=0,
			float rotation=0,
			int buttonUpColorMask=0xffffffff, int buttonDownColorMask=0xff808080,
			const TextureRegion *buttonUpRegion=0, const TextureRegion *buttonDownRegion=0);
	*/

	inline float getWidth() { return width; }
	inline float getHeight() { return height; }
	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + getScaledWidth(); }
	inline float getTop() { return y + getScaledHeight();  }
	inline float getCenterX() { return x + (0.5f*getScaledWidth());  }
	inline float getCenterY() { return y + (0.5f*getScaledHeight());  }

	inline void setLeft(float left) { this->x = left; }
	inline void setBottom(float bottom) { this->y = bottom; }
	inline void setRight(float right) { this->x = right - getScaledWidth(); }
	inline void setTop(float top) { this->y = top - getScaledHeight(); }
	inline void setCenterX(float centerX) { this->x = centerX - (0.5f * getScaledWidth()); }
	inline void setCenterY(float centerY) { this->y = centerY - (0.5f * getScaledHeight()); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }

	inline void setRotation(float rotation)  { this->rotation = rotation; }
	inline void setRotationPivotDisplacementFromCenter(float xDisplacement, float yDisplacement) {
		this->rotationPivotXDisplacement = xDisplacement;
		this->rotationPivotYDisplacement = yDisplacement;
	}

	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }
    inline void setColorMask(int colorMask) { this->color = colorMask; }

	inline void adjustDepths(float maxZ, float minZ) { this->maxZ = maxZ; this->minZ = minZ; }

	void drawOpaqueParts(SpriteBatch *spriteBatch);
	void drawTransparentParts(SpriteBatch *spriteBatch);
	void drawAllInOrder(SpriteBatch *spriteBatch);

	virtual void update(float elapsedTimeMillis);

	virtual bool onTouchEvent(const TouchEvent *event);

	virtual void attachToAnimation(Animation *animation);

	// Button specific methods,
	void setTextFont(Font *font) { this->font = font; }
	inline void setTextColor(int color) { this->textColor = color; }
	inline void setTextScale(float scaleX, float scaleY) { this->textScaleX = scaleX; this->textScaleY = scaleY; }

	void setTextureRegions(const Ngl::TextureRegion *buttonUpRegion=0, const Ngl::TextureRegion *buttonDownRegion=0, bool resize=true);

	inline void setListener(ButtonListener *buttonListener) { this->buttonListener = buttonListener; }

	bool testHit(float x, float y);
	bool isTouched();

	inline static void setClickSound(Sound *clickSound) { Button::clickSound = clickSound; }
	inline static Sound *getClickSound() { return clickSound; }
};
