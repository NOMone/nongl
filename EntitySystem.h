#pragma once

#include "ManagedObject.h"
#include "TextUtils.h"
#include "utils.h"

#include <vector>

#define ALLOW_DESTRUCTION_BY_MANAGED_POINTERS \
		template <typename AnyT> friend void Ngl::managed_ptr<AnyT>::die(); \
		template <typename AnyT> friend class Ngl::shared_ptr;  // destruct() is private. Had to give access to the entire class.

#define COMPONENT_DEFINITION_OVERHEAD \
		ALLOW_DESTRUCTION_BY_MANAGED_POINTERS \
		friend class Ngl::ComponentFactory;

#define SYSTEM_DEFINITION_OVERHEAD \
		ALLOW_DESTRUCTION_BY_MANAGED_POINTERS

#define CREATE_SYSTEM(SystemType, systemObject, entitiesManager) { \
		Ngl::shared_ptr<Ngl::System> tempSystem; \
		new SystemType(&entitiesManager, tempSystem); \
		systemObject = tempSystem; }

#define CREATE_COMPONENT(ComponentType, entityObject) Ngl::ComponentFactory::createComponent(Ngl::EntitiesManager::getComponentTypeId<ComponentType>(), entityObject.get());

#define ON_COMPONENT_CREATED(managingPointer) parentEntity->entitiesManager->onComponentCreated<decltype(removeReferenceFromType(*this))>(managingPointer);
#define ON_COMPONENT_DELETED parentEntity->entitiesManager->onComponentDeleted(this);

namespace Ngl {

	class ComponentFactory;
	class Component;
	class Entity;
	class EntitiesManager;

	//////////////////////
	// Attributes
	//////////////////////

	class Attribute {
		int _id;
	protected:
		inline Attribute(int id) : id(_id), _id(id) { }
	public:
		int const &id;
	};

	class Attributes {
		// TODO: use std::map<int, Attribute *> instead.
		std::vector<Attribute *> attributes;
	public:

		~Attributes();
		void addAttribute(Attribute *attribute);
		Attribute *getAttribute(int attributeId);
	};

	//////////////////////
	// Common attributes
	//////////////////////

	class IntegerAttribute : public Attribute {
		int value;
		int *valuePointer;
	public:
		inline IntegerAttribute(int id, int value) : Attribute(id) {
			this->value = value;
			valuePointer = &this->value;
		}
		inline int get() { return *valuePointer; }
		inline void set(int value) { *this->valuePointer = value; }
	};

	class FloatAttribute : public Attribute {
		float value;
		float *valuePointer;
	public:
		inline FloatAttribute(int id, float value) : Attribute(id) {
			this->value = value;
			valuePointer = &this->value;
		}
		inline float get() { return *valuePointer; }
		inline void set(float value) { *this->valuePointer = value; }
	};

	//////////////////////
	// System
	//////////////////////

	class System {
		SYSTEM_DEFINITION_OVERHEAD
	protected:
		EntitiesManager *_entitiesManager;
		WeakVector<Component> components;

		virtual inline ~System() {}

		inline System(EntitiesManager *entitiesManager, Ngl::shared_ptr<System> &managingPointer) : entitiesManager(_entitiesManager) {
			this->_entitiesManager = entitiesManager;
			managingPointer.manage(this);
		}

	public:

		// Read only property,
		EntitiesManager * const &entitiesManager;

		virtual inline void onComponentCreated(const weak_ptr<Component> &component, int componentTypeId) {
			components.push_back(component);
		}
		virtual inline void onComponentDeleted(Component *component, int componentTypeId) {}
	};

	//////////////////////
	// Component
	//////////////////////

	class Component {

		COMPONENT_DEFINITION_OVERHEAD

		Entity *_parentEntity;
		Attributes *attributes;

	protected:
		Component(Entity *parentEntity, shared_ptr<Component> &managingPointer);
		virtual ~Component();
	public:

		Entity * const & parentEntity;
		virtual inline Attributes *getAttributes() {
			if (attributes) return attributes;
			attributes = new Attributes();
			return attributes;
		}
	};

	//////////////////////
	// Entity
	//////////////////////

	class Entity {

		friend class EntitiesManager; // To create entities.
		friend class Component; // To attach components to entities.

		ALLOW_DESTRUCTION_BY_MANAGED_POINTERS

		EntitiesManager *_entitiesManager;
		SharedVector<Component> _components;

		Entity(EntitiesManager *entitiesManager);
		virtual ~Entity();

		inline void addComponent(const shared_ptr<Component> &component) { _components.push_back(component); }

	public:

		EntitiesManager * const & entitiesManager;
		SharedVector<Component> const & components;
		Attributes attributes;

		template <typename ComponentType>
		WeakVector<ComponentType> getComponents() {

			WeakVector<ComponentType> matchingComponents;
			int32_t componentsCount = (int32_t) _components.size();
			for (int32_t i=0; i<componentsCount; i++) {
				if (dynamic_cast<ComponentType *>(_components[i].get())) {
					matchingComponents.push_back(_components[i]);
				}
			}
			return matchingComponents;
		}

		template <typename ComponentType>
		weak_ptr<ComponentType> getFirstComponent() {
			int32_t componentsCount = (int32_t) _components.size();
			for (int i=0; i<componentsCount; i++) {
				if (dynamic_cast<ComponentType *>(_components[i].get())) {
					return _components[i];
				}
			}
			return weak_ptr<ComponentType>();
		}


	};

	//////////////////////
	// Entities manager
	//////////////////////

	class EntitiesManager {

		int createdEntitiesCount;
		WeakVector<Entity> entities;
		std::vector<TextBuffer> attributeNames;

		std::vector<SharedVector<System> > listeningSystems;

		inline void addEntity(const weak_ptr<Entity> &entity) {
			entities.push_back(entity);
			createdEntitiesCount++;
		}

		static inline int newComponentTypeId() {
			static int lastAssignedComponentTypeId = 0;
			return lastAssignedComponentTypeId++;
		}

		// Makes sure there is a systems vector allocated to this component type,
		void allocateSystemsVector(int componentTypeId);

	public:

		inline EntitiesManager() {
			createdEntitiesCount = 0;
		}
		~EntitiesManager();

		template <class T>
		static inline int getComponentTypeId() {
			static_assert(std::is_base_of<Component, T>::value, "Can't get ids of types that aren't derived from Component.");
			static int myTypeId = newComponentTypeId();
			return myTypeId;
		}

		// Entities' methods,
		shared_ptr<Entity> createEntity();

		// Attributes' methods,
		int getAttributeId(const char *attributeName);

		// Systems' methods,
		template <class T>
		void addSystem(const shared_ptr<System> &system) {

			// Make sure there's a listening systems vector allocated to this component type,
			int componentTypeId = getComponentTypeId<T>();

			allocateSystemsVector(componentTypeId);
			listeningSystems[componentTypeId].push_back(system);
		}

		// You ABSOLUTELY MUST call this method in your components' constructors,
		template <class T>
		void onComponentCreated(const weak_ptr<Component> &object) {

			// Make sure there's a listening systems vector allocated to this component type,
			int componentTypeId = getComponentTypeId<T>();
			allocateSystemsVector(componentTypeId);

			// Notify all relevant systems of component creation,
			for (int i=0; i<listeningSystems[componentTypeId].size(); i++) {
				if (!listeningSystems[componentTypeId][i].expired()) {
					listeningSystems[componentTypeId][i]->onComponentCreated(object, componentTypeId);
				}
			}
		}

		// You ABSOLUTELY MUST call this method in your components' destructors,
		template <class T>
		void onComponentDeleted(T *object) {

			// Notify all relevant systems of component deletion,
			int componentTypeId = getComponentTypeId<T>();
			for (int i=0; i<listeningSystems[componentTypeId].size(); i++) {
				if (!listeningSystems[componentTypeId][i].expired()) {
					listeningSystems[componentTypeId][i]->onComponentDeleted(object, componentTypeId);
				}
			}
		}
	};
}
