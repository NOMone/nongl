#pragma once

#include "Threads.h"
#include "ManagedObject.h"

#include <stdint.h>
#include <list>
#include <functional>

class Activity;

class Scheduler {

	struct Task {
		Ngl::ManagedPointer<Runnable> task;
		void *data;
		float delay;
	};

	std::list<Task> tasks;

public:

	~Scheduler();

	inline int32_t getTasksCount() { return (int32_t) tasks.size(); }

	//void schedule(Runnable *task, float delay=0, void *data=0);
	//void schedule(void (* runnable)(void *data), float delay=0, void *data=0, Activity *activity=0);

	void schedule(Ngl::ManagedPointer<Runnable> task, float delay=0, void *data=0);
	void schedule(bool (* runnable)(void *data), float delay=0, void *data=0, Activity *activity=0);

    template <typename T>
	void schedule(const std::function<void()> &function, const Ngl::weak_ptr<T> &aliveToken, float delayMillis);
    template <typename T>
    void schedule(const std::function<void()> &function, const Ngl::shared_ptr<T> &aliveToken, float delayMillis);

	void updateAndDispatch(float elapsedTime);
	void removeTask(Runnable *task);
	void removeActivityTasks(Activity *activity);
};

template <typename T>
void Scheduler::schedule(const std::function<void()> &function, const Ngl::weak_ptr<T> &aliveToken, float delayMillis) {

    class SmartRunnable : public Runnable {
        std::function<void()> function;
        Ngl::weak_ptr<T> aliveToken;
    public:
        SmartRunnable(const std::function<void()> &function, const Ngl::weak_ptr<T> &aliveToken) :
            function(function), aliveToken(aliveToken) {}

        bool run(void *data) {
            if (aliveToken.expired()) return true;
            function();
            return true;
        }
    };

    schedule(Ngl::shared_ptr<Runnable>(new SmartRunnable(function, aliveToken)), delayMillis, nullptr);
}

template <typename T>
void Scheduler::schedule(const std::function<void()> &function, const Ngl::shared_ptr<T> &aliveToken, float delayMillis) {
    schedule(function, Ngl::weak_ptr<T>(aliveToken), delayMillis);
}
