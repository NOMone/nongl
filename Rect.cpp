#include "Rect.h"

using namespace Ngl;

Rect::Rect(float left, float bottom, float right, float top) : left(left), bottom(bottom), right(right), top(top) {}

static float max(float val1, float val2) { return val1 > val2 ? val1 : val2; }
static float min(float val1, float val2) { return val1 < val2 ? val1 : val2; }

IntersectionResult Rect::intersect(const Rect &rect) const {

	IntersectionResult result;

	// Check if intersecting at all,
	if (!intersects(rect)) {
		result.intersecting = false;
		return result;
	}

	// Compute intersections,
	result.intersecting = true;
	result.intersection.left   = max(left  , rect.left  );
	result.intersection.bottom = max(bottom, rect.bottom);
	result.intersection.right  = min(right , rect.right );
	result.intersection.top    = min(top   , rect.top   );

	return result;
}

bool Rect::intersects(const Rect &rect)  const {
	return ((     left   < rect.right) &&
			(rect.left   <      right) &&
			(     bottom < rect.top  ) &&
	        (rect.bottom <      top  ));
}
