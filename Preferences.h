#pragma once

#include <map>
#include <cstdint>

class TextBuffer;

namespace NglImpDetails {
	class Preference;
}

namespace Ngl {

	class OutputStream;
	class InputStream;

	class Preferences {
		std::map<TextBuffer, NglImpDetails::Preference *> preferences;

		template <typename TPreference, typename T>
		void setPreference(const TextBuffer &key, T value);

		template <typename TPreference, typename T>
		T getPreference(const TextBuffer &key, T defaultValue);

	public:

		void setBoolPreference(const TextBuffer &key, bool value);
		bool getBoolPreference(const TextBuffer &key, bool defaultValue);
		void setIntPreference(const TextBuffer &key, int32_t value);
		int32_t getIntPreference(const TextBuffer &key, int32_t defaultValue);
		void setStringPreference(const TextBuffer &key, const TextBuffer &value);
		TextBuffer getStringPreference(const TextBuffer &key, const TextBuffer &defaultValue);

		void write(OutputStream &outputStream);
		inline void write(OutputStream &&outputStream) { write(outputStream); }

		// Overwrites existing keys and appends new ones,
		void read(InputStream &inputStream);
		inline void read(InputStream &&inputStream) { read(inputStream); }

		void log();
		bool isIdenticalTo(const Preferences &rightHandSide);
	};
}
