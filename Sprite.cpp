#include "DisplayManager.h"
#include "Sprite.h"
#include "SpriteBatch.h"
#include "utils.h"
#include "Animation.h"

#include <math.h>

/////////////////////////////////
// Sprite animation listener
/////////////////////////////////

void SpriteAnimationListener::onAnimationComponentApplied(AnimationTarget::Target target) {
	if (target == AnimationTarget::SCALE_ABOUT_CENTER) {
		float centerX = sprite->getCenterX();
		float centerY = sprite->getCenterY();
		sprite->scaleX = sprite->scaleY = scaleAboutCenter;
		sprite->setCenter(centerX, centerY);
	}
}

/////////////////////////////////
// Sprite
/////////////////////////////////

Sprite::Sprite() {
	animationListener = 0;
}

Sprite::Sprite(const Sprite &spriteToClone) {
	clone(&spriteToClone);
	animationListener = 0;
}

Sprite::Sprite(const Ngl::TextureRegion *textureRegion) {
	setFromTextureRegion(textureRegion);
	animationListener = 0;
}

Sprite::Sprite(float width, float height, Ngl::TextureRegion *textureRegion) {
	this->animationListener = 0;

	this->width = width;
	this->height = height;
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->scaleX = 1;
	this->scaleY = 1;
	this->rotation = 0;
	this->flipX = 0;
	this->flipY = 0;
	this->colorMask = 0xffffffff;
	this->textureRegion = textureRegion;
}

Sprite::~Sprite() {
	if (animationListener) delete animationListener;
}

void Sprite::clone(const Sprite *spriteToClone) {

	width         = spriteToClone->width;
	height        = spriteToClone->height;
	x             = spriteToClone->x;
	y             = spriteToClone->y;
	z             = spriteToClone->z;
	scaleX        = spriteToClone->scaleX;
	scaleY        = spriteToClone->scaleY;
	rotation      = spriteToClone->rotation;
	flipX         = spriteToClone->flipX;
	flipY         = spriteToClone->flipY;
	colorMask     = spriteToClone->colorMask;
	textureRegion = spriteToClone->textureRegion;
}

void Sprite::setFromTextureRegion(const Ngl::TextureRegion *textureRegion) {
	if (textureRegion) {
		this->width = textureRegion->originalWidth;
		this->height = textureRegion->originalHeight;
	} else {
		this->width = 0;
		this->height = 0;
	}
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->scaleX = 1;
	this->scaleY = 1;
	this->rotation = 0;
	this->flipX = 0;
	this->flipY = 0;
	this->colorMask = 0xffffffff;
	this->textureRegion = textureRegion;
}

void Sprite::set(
		float width, float height,
		float x, float y, float z,
		float scaleX, float scaleY,
		float rotation,
		bool flipX, bool flipY,
		int colorMask,
		const Ngl::TextureRegion *textureRegion) {

	this->x = x;
	this->y = y;
	this->z = z;
	this->width = width;
	this->height = height;
	this->scaleX = scaleX;
	this->scaleY = scaleY;
	this->rotation = rotation;
	this->flipX = flipX;
	this->flipY = flipY;
	this->colorMask = colorMask;
	this->textureRegion = textureRegion;
}

void Sprite::setTextureRegion(const Ngl::TextureRegion *textureRegion) {
	this->textureRegion = textureRegion;
}

bool Sprite::hit(float x, float y) {

	if ((this->x <= x) &&
		(this->y <= y) &&
		((this->x + (width * scaleX)) > x) &&
		((this->y + (height * scaleY)) > y)) {

		return true;
	}

	return false;
}

bool Sprite::hit(float x, float y, float rightTolerance, float leftTolerance, float topTolerance, float bottomTolerance) {

	if (((getLeft  () - leftTolerance  ) <= x) &&
		((getBottom() - bottomTolerance) <= y) &&
		((getRight () + rightTolerance ) >  x) &&
		((getTop   () + topTolerance   ) >  y)) {

		return true;
	}

	return false;
}

float Sprite::getDistance(float x, float y) {

	float horizontalDistance;
	float verticalDistance;

	if (x < getLeft()) {
		horizontalDistance = getLeft() - x;
	} else if (x > getRight()) {
		horizontalDistance = x - getRight();
	} else {
		horizontalDistance = 0;
	}

	if (y < getBottom()) {
		verticalDistance = getBottom() - y;
	} else if (y > getTop()) {
		verticalDistance = y - getTop();
	} else {
		verticalDistance = 0;
	}

	if (horizontalDistance && verticalDistance) {
		return magnitude(horizontalDistance, verticalDistance);
	} else if (horizontalDistance) {
		return horizontalDistance;
	} else {
		return verticalDistance;
	}
}


#define UPPER_LEFT 0
#define BOTTOM_LEFT 1
#define UPPER_RIGHT 2
#define BOTTOM_RIGHT 3

#define UPPER_LEFT_X 0
#define UPPER_LEFT_Y 1
#define UPPER_LEFT_Z 2
#define UPPER_LEFT_TEX_X 3
#define UPPER_LEFT_TEX_Y 4
#define UPPER_LEFT_COLOR_MASK 5

#define BOTTOM_LEFT_X 6
#define BOTTOM_LEFT_Y 7
#define BOTTOM_LEFT_Z 8
#define BOTTOM_LEFT_TEX_X 9
#define BOTTOM_LEFT_TEX_Y 10
#define BOTTOM_LEFT_COLOR_MASK 11

#define UPPER_RIGHT_X 12
#define UPPER_RIGHT_Y 13
#define UPPER_RIGHT_Z 14
#define UPPER_RIGHT_TEX_X 15
#define UPPER_RIGHT_TEX_Y 16
#define UPPER_RIGHT_COLOR_MASK 17

#define BOTTOM_RIGHT_X 18
#define BOTTOM_RIGHT_Y 19
#define BOTTOM_RIGHT_Z 20
#define BOTTOM_RIGHT_TEX_X 21
#define BOTTOM_RIGHT_TEX_Y 22
#define BOTTOM_RIGHT_COLOR_MASK 23

void Sprite::draw(float *vertexData, unsigned short *indices, int verticesOffset) {

	// If you ever modify this method, don't forget to modify drawWireframe as well.

	int flipsCount = 0;
	float commonConstant;
	float leftX, rightX;
	float topY, bottomY;
	float centerX, centerY;
	float cosAngle, sinAngle;
	float commonConstant1, commonConstant2, commonConstant3, commonConstant4;

	if (!rotation) {

		// Vertices,
		if (flipX) {
			flipsCount++;
			commonConstant = ((width + textureRegion->originalWidth) * 0.5f) - textureRegion->offsetX;
			vertexData[UPPER_LEFT_X] = vertexData[BOTTOM_LEFT_X] = x + (commonConstant * scaleX);
			vertexData[UPPER_RIGHT_X] = vertexData[BOTTOM_RIGHT_X] = x + ((commonConstant - textureRegion->width) * scaleX);
		} else {
			commonConstant = ((width - textureRegion->originalWidth) * 0.5f) + textureRegion->offsetX;
			vertexData[UPPER_LEFT_X] = vertexData[BOTTOM_LEFT_X] = x + (commonConstant * scaleX);
			vertexData[UPPER_RIGHT_X] = vertexData[BOTTOM_RIGHT_X] = x + ((commonConstant + textureRegion->width) * scaleX);
		}

		if (flipY) {
			flipsCount++;
			commonConstant = ((height + textureRegion->originalHeight) * 0.5f) - textureRegion->offsetY;
			vertexData[UPPER_LEFT_Y] = vertexData[UPPER_RIGHT_Y] = y + ((commonConstant - textureRegion->height) * scaleY);
			vertexData[BOTTOM_LEFT_Y] = vertexData[BOTTOM_RIGHT_Y] = y + (commonConstant * scaleY);
		} else {
			commonConstant = ((height - textureRegion->originalHeight) * 0.5f) + textureRegion->offsetY;
			vertexData[UPPER_LEFT_Y] = vertexData[UPPER_RIGHT_Y] = y + ((commonConstant + textureRegion->height) * scaleY);
			vertexData[BOTTOM_LEFT_Y] = vertexData[BOTTOM_RIGHT_Y] = y + (commonConstant  * scaleY);
		}

	} else {

		// Rotation is non-zero,
		// Vertices,
		if (flipX) {
			flipsCount++;
			commonConstant = ((width + textureRegion->originalWidth) * 0.5f) - textureRegion->offsetX;
			leftX = x + (commonConstant * scaleX);
			rightX = x + ((commonConstant - textureRegion->width) * scaleX);
		} else {
			commonConstant = ((width - textureRegion->originalWidth) * 0.5f) + textureRegion->offsetX;
			leftX = x + (commonConstant * scaleX);
			rightX = x + ((commonConstant + textureRegion->width) * scaleX);
		}

		if (flipY) {
			flipsCount++;
			commonConstant = ((height + textureRegion->originalHeight) * 0.5f) - textureRegion->offsetY;
			topY = y + ((commonConstant - textureRegion->height) * scaleY);
			bottomY = vertexData[BOTTOM_RIGHT_Y] = y + (commonConstant * scaleY);
		} else {
			commonConstant = ((height - textureRegion->originalHeight) * 0.5f) + textureRegion->offsetY;
			topY = vertexData[UPPER_RIGHT_Y] = y + ((commonConstant + textureRegion->height) * scaleY);
			bottomY = vertexData[BOTTOM_RIGHT_Y] = y + (commonConstant  * scaleY);
		}

		// Do the rotation,
		cosAngle = cosf(rotation);
		sinAngle = sinf(rotation);
		centerX = getCenterX();
		centerY = getCenterY();
		leftX -= centerX;
		rightX -= centerX;
		topY -= centerY;
		bottomY -= centerY;
		commonConstant1 = centerX + (leftX * cosAngle);
		commonConstant2 = - (topY * sinAngle);
		commonConstant3 = centerX + (rightX * cosAngle);
		commonConstant4 = - (bottomY * sinAngle);

		vertexData[UPPER_LEFT_X] = commonConstant1 + commonConstant2;
		vertexData[UPPER_RIGHT_X] = commonConstant3 + commonConstant2;
		vertexData[BOTTOM_LEFT_X] = commonConstant1 + commonConstant4;
		vertexData[BOTTOM_RIGHT_X] = commonConstant3 + commonConstant4;

		commonConstant1 = centerY + (topY * cosAngle);
		commonConstant2 = leftX * sinAngle;
		commonConstant3 = rightX * sinAngle;
		commonConstant4 = centerY + (bottomY * cosAngle);

		vertexData[UPPER_LEFT_Y] = commonConstant1 + commonConstant2;
		vertexData[UPPER_RIGHT_Y] = commonConstant1 + commonConstant3;
		vertexData[BOTTOM_LEFT_Y] = commonConstant4 + commonConstant2;
		vertexData[BOTTOM_RIGHT_Y] = commonConstant4 + commonConstant3;
	}

	vertexData[UPPER_LEFT_Z] = vertexData[BOTTOM_LEFT_Z] = vertexData[UPPER_RIGHT_Z] = vertexData[BOTTOM_RIGHT_Z] = z;

	// Texture coordinates,
	vertexData[UPPER_LEFT_TEX_X] = vertexData[BOTTOM_LEFT_TEX_X] = textureRegion->u1;
	vertexData[UPPER_RIGHT_TEX_X] = vertexData[BOTTOM_RIGHT_TEX_X] = textureRegion->u2;
	vertexData[UPPER_LEFT_TEX_Y] = vertexData[UPPER_RIGHT_TEX_Y] = textureRegion->v1;
	vertexData[BOTTOM_LEFT_TEX_Y] = vertexData[BOTTOM_RIGHT_TEX_Y] = textureRegion->v2;

	// Color mask,
	((unsigned int *) vertexData)[UPPER_LEFT_COLOR_MASK] =
	((unsigned int *) vertexData)[BOTTOM_LEFT_COLOR_MASK] =
	((unsigned int *) vertexData)[UPPER_RIGHT_COLOR_MASK] =
	((unsigned int *) vertexData)[BOTTOM_RIGHT_COLOR_MASK] =
			colorMask;

	// Indices,
	// Preserve counter-clockwise order,
	if (flipsCount&1) {
		indices[0] = verticesOffset + UPPER_RIGHT;
		indices[1] = indices[4] = verticesOffset + BOTTOM_RIGHT;
		indices[2] = indices[3] = verticesOffset + UPPER_LEFT;
		indices[5] = verticesOffset + BOTTOM_LEFT;
	} else {
		indices[0] = verticesOffset + UPPER_LEFT;
		indices[1] = indices[4] = verticesOffset + BOTTOM_LEFT;
		indices[2] = indices[3] = verticesOffset + UPPER_RIGHT;
		indices[5] = verticesOffset + BOTTOM_RIGHT;
	}
}

void Sprite::drawWireFrame(SpriteBatch *spriteBatch, int color) {

	// If you ever modify this method, don't forget to modify draw as well.

	int flipsCount = 0;
	float commonConstant;
	float leftX, rightX;
	float topY, bottomY;
	float centerX, centerY;
	float cosAngle, sinAngle;
	float commonConstant1, commonConstant2, commonConstant3, commonConstant4;

	float upperRightX, upperRightY;
	float bottomRightX, bottomRightY;
	float upperLeftX, upperLeftY;
	float bottomLeftX, bottomLeftY;

	if (!rotation) {

		// Vertices,
		if (flipX) {
			flipsCount++;
			commonConstant = ((width + textureRegion->originalWidth) * 0.5f) - textureRegion->offsetX;
			upperLeftX = bottomLeftX = x + (commonConstant * scaleX);
			upperRightX = bottomRightX = x + ((commonConstant - textureRegion->width) * scaleX);
		} else {
			commonConstant = ((width - textureRegion->originalWidth) * 0.5f) + textureRegion->offsetX;
			upperLeftX = bottomLeftX = x + (commonConstant * scaleX);
			upperRightX = bottomRightX = x + ((commonConstant + textureRegion->width) * scaleX);
		}

		if (flipY) {
			flipsCount++;
			commonConstant = ((height + textureRegion->originalHeight) * 0.5f) - textureRegion->offsetY;
			upperLeftY = upperRightY = y + ((commonConstant - textureRegion->height) * scaleY);
			bottomLeftY = bottomRightY = y + (commonConstant * scaleY);
		} else {
			commonConstant = ((height - textureRegion->originalHeight) * 0.5f) + textureRegion->offsetY;
			upperLeftY = upperRightY = y + ((commonConstant + textureRegion->height) * scaleY);
			bottomLeftY = bottomRightY = y + (commonConstant  * scaleY);
		}

	} else {

		// Rotation is non-zero,
		// Vertices,
		if (flipX) {
			flipsCount++;
			commonConstant = ((width + textureRegion->originalWidth) * 0.5f) - textureRegion->offsetX;
			leftX = x + (commonConstant * scaleX);
			rightX = x + ((commonConstant - textureRegion->width) * scaleX);
		} else {
			commonConstant = ((width - textureRegion->originalWidth) * 0.5f) + textureRegion->offsetX;
			leftX = x + (commonConstant * scaleX);
			rightX = x + ((commonConstant + textureRegion->width) * scaleX);
		}

		if (flipY) {
			flipsCount++;
			commonConstant = ((height + textureRegion->originalHeight) * 0.5f) - textureRegion->offsetY;
			topY = y + ((commonConstant - textureRegion->height) * scaleY);
			bottomY = bottomRightY = y + (commonConstant * scaleY);
		} else {
			commonConstant = ((height - textureRegion->originalHeight) * 0.5f) + textureRegion->offsetY;
			topY = upperRightY = y + ((commonConstant + textureRegion->height) * scaleY);
			bottomY = bottomRightY = y + (commonConstant  * scaleY);
		}

		// Do the rotation,
		cosAngle = cosf(rotation);
		sinAngle = sinf(rotation);
		centerX = getCenterX();
		centerY = getCenterY();
		leftX -= centerX;
		rightX -= centerX;
		topY -= centerY;
		bottomY -= centerY;
		commonConstant1 = centerX + (leftX * cosAngle);
		commonConstant2 = - (topY * sinAngle);
		commonConstant3 = centerX + (rightX * cosAngle);
		commonConstant4 = - (bottomY * sinAngle);

		upperLeftX = commonConstant1 + commonConstant2;
		upperRightX = commonConstant3 + commonConstant2;
		bottomLeftX = commonConstant1 + commonConstant4;
		bottomRightX = commonConstant3 + commonConstant4;

		commonConstant1 = centerY + (topY * cosAngle);
		commonConstant2 = leftX * sinAngle;
		commonConstant3 = rightX * sinAngle;
		commonConstant4 = centerY + (bottomY * cosAngle);

		upperLeftY = commonConstant1 + commonConstant2;
		upperRightY = commonConstant1 + commonConstant3;
		bottomLeftY = commonConstant4 + commonConstant2;
		bottomRightY = commonConstant4 + commonConstant3;
	}

	// Indices,
	// Preserve counter-clockwise order,
	if (flipsCount&1) {
		// Face 1,
		spriteBatch->drawLine(upperRightX, upperRightY, z, bottomRightX, bottomRightY, z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, z, upperLeftX, upperLeftY, z, color);
		spriteBatch->drawLine(upperLeftX, upperLeftY, z, upperRightX, upperRightY, z, color);

		// Face 2,
		spriteBatch->drawLine(upperLeftX, upperLeftY, z, bottomRightX, bottomRightY, z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, z, bottomLeftX, bottomLeftY, z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, z, upperLeftX, upperLeftY, z, color);
	} else {
		// Face 1,
		spriteBatch->drawLine(upperLeftX, upperLeftY, z, bottomLeftX, bottomLeftY, z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, z, upperRightX, upperRightY, z, color);
		spriteBatch->drawLine(upperRightX, upperRightY, z, upperLeftX, upperLeftY, z, color);

		// Face 2,
		spriteBatch->drawLine(upperRightX, upperRightY, z, bottomLeftX, bottomLeftY, z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, z, bottomRightX, bottomRightY, z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, z, upperRightX, upperRightY, z, color);
	}
}

void Sprite::draw(SpriteBatch *spriteBatch) {
	spriteBatch->drawSprite(this);
}

void Sprite::setupAnimationListener(Animation *animation) {
	if (!animationListener) animationListener = new SpriteAnimationListener(this);
	animation->setAnimationListener(animationListener);
}

void Sprite::attachToAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += x;
				currentComponent->setTargetAddress(&x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += y;
				currentComponent->setTargetAddress(&y);
				break;

			case AnimationTarget::Z:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += z;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += z;
				currentComponent->setTargetAddress(&z);
				break;

			case AnimationTarget::YAW:
				currentComponent->setTargetAddress(&rotation);
				break;

			case AnimationTarget::COLOR_MASK:
				if (currentComponent->initialValueRelative) currentComponent->initialValueInt = multiplyColors(currentComponent->initialValueInt, colorMask);
				if (currentComponent->  finalValueRelative) currentComponent->finalValueInt   = multiplyColors(currentComponent->finalValueInt  , colorMask);
				currentComponent->setTargetAddress(&colorMask, true);
				break;

			case AnimationTarget::SCALE_X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat *= scaleX;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   *= scaleX;
				currentComponent->setTargetAddress(&scaleX);
				break;

			case AnimationTarget::SCALE_Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat *= scaleY;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   *= scaleY;
				currentComponent->setTargetAddress(&scaleY);
				break;

			case AnimationTarget::SCALE_ABOUT_CENTER:
				setupAnimationListener(animation);
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat *= scaleX;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   *= scaleX;
				currentComponent->setTargetAddress(&animationListener->scaleAboutCenter);
				break;

			default:
				break;
		}
	}
}

////////////////////////
// Sprites group
////////////////////////

void SpritesGroup::deleteAllSprites() {

	if (deletesChildrenOnDestruction) {
		std::list<Sprite *>::iterator iterator = sprites.begin();
		std::list<Sprite *>::iterator endIterator = sprites.end();
		for (; iterator!=endIterator; ++iterator) {
			Sprite *currentSprite = *iterator;
			delete currentSprite;
		}
	}

	sprites.clear();
}

void SpritesGroup::draw(SpriteBatch *spriteBatch) {

	std::list<Sprite *>::iterator iterator = sprites.begin();
	std::list<Sprite *>::iterator endIterator = sprites.end();
	for (; iterator!=endIterator; ++iterator) {
		Sprite *currentSprite = *iterator;
		int colorMask = currentSprite->colorMask & 0xff000000;
		if (colorMask || drawsFullyTransparentSprites) {
			spriteBatch->drawSprite(currentSprite);
		}
	}
}

void SpritesGroup::draw(SpriteBatch *spriteBatch, float zOffset, float zScale) {

	std::list<Sprite *>::iterator iterator = sprites.begin();
	std::list<Sprite *>::iterator endIterator = sprites.end();
	for (; iterator!=endIterator; ++iterator) {
		Sprite *currentSprite = *iterator;
		int colorMask = currentSprite->colorMask & 0xff000000;
		if (colorMask || drawsFullyTransparentSprites) {
			float originalZ = currentSprite->z;
			currentSprite->z = zOffset + (originalZ * zScale);
			spriteBatch->drawSprite(currentSprite);
			currentSprite->z = originalZ;
		}
	}
}
