#include "BufferedOutputStream.h"
#include "InputStream.h"
#include "SystemLog.h"

#include <stdexcept>

Ngl::BufferedOutputStream::BufferedOutputStream(ManagedPointer<OutputStream> outputStream, int32_t maxSizeBytes) {
	this->outputStream = outputStream;
	this->maxSizeBytes = maxSizeBytes;
}

Ngl::BufferedOutputStream::~BufferedOutputStream() {
	try {
		flush();
	} catch (const std::exception &e) {
		// Since flush() may throw, and exceptions may not
		// be thrown in destructor, just warn and discard,
		LOGE("%s", e.what());
	}
}

void Ngl::BufferedOutputStream::writeData(const void *data, int32_t sizeBytes) {

	const unsigned char *byteDataBlock = (const unsigned char *) data;
	buffer.insert(buffer.end(), byteDataBlock, byteDataBlock + sizeBytes);

	if (maxSizeBytes && (buffer.size() >= maxSizeBytes)) flush();
}

int32_t Ngl::BufferedOutputStream::writeData(InputStream &inputStream) {

	int32_t readSize = inputStream.readData(buffer);
	if (maxSizeBytes && (buffer.size() >= maxSizeBytes)) flush();

	return readSize;
}

void Ngl::BufferedOutputStream::flush() {

	if (outputStream.expired()) throw std::runtime_error("Attempt to flush buffer to an expired output stream.");

	outputStream->writeData(&buffer[0], buffer.size());

	// If writing throws, buffer won't be cleared,
	buffer.clear();
}
