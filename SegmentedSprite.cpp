#include "SegmentedSprite.h"
#include "DisplayManager.h"
#include "Sprite.h"
#include "SpriteBatch.h"

#include <stdio.h>
#include <string.h>

#define REGIONS_SEPARATOR "."

//////////////////////////////
// Segmented sprite params
//////////////////////////////

SegmentedSpriteParameters::SegmentedSpriteParameters(
		int columnsCount, int rowsCount,
		const char *regionPrefix,
		float imageWidth, float imageHeight) {

	this->columnsCount = columnsCount;
	this->rowsCount = rowsCount;
	this->regionPrefix = regionPrefix;
	this->imageWidth = imageWidth;
	this->imageHeight = imageHeight;
}

void SegmentedSpriteParameters::set(
		int columnsCount, int rowsCount,
		const char *regionPrefix,
		float imageWidth, float imageHeight) {

	this->columnsCount = columnsCount;
	this->rowsCount = rowsCount;
	this->regionPrefix = regionPrefix;
	this->imageWidth = imageWidth;
	this->imageHeight = imageHeight;
}

//////////////////////////////
// Segmented sprite
//////////////////////////////

SegmentedSprite::SegmentedSprite(const SegmentedSpriteParameters *params) {
	set(
		params->columnsCount, params->rowsCount,
		params->regionPrefix,
		params->imageWidth, params->imageHeight);
}

SegmentedSprite::SegmentedSprite(int columnsCount, int rowsCount, const char *regionPrefix, float imageWidth, float imageHeight) {
	set(columnsCount, rowsCount, regionPrefix, imageWidth, imageHeight);
}

SegmentedSprite::~SegmentedSprite() {

	for (int i=0; i<regionSprites.size(); i++) {
		if (regionSprites[i]) delete regionSprites[i];
	}
}

void SegmentedSprite::set(int columnsCount, int rowsCount, const char *regionPrefix, float imageWidth, float imageHeight) {

	this->columnsCount = columnsCount;
	this->rowsCount = rowsCount;

	this->segmentWidth = this->segmentHeight = 0;

	this->x = this->y = this->z = 0;
	this->width = imageWidth;
	this->height = imageHeight;
	this->scaleX = this->scaleY = 1;
	this->rotation = false;
	this->colorMask = 0xffffffff;

	this->modified = true;

	// Find and buffer regions,
	int regionsCount = columnsCount * rowsCount;
	regionSprites.reserve(regionsCount);

	DisplayManager *displayManager = DisplayManager::getSingleton();
	char currentRegionName[strlen(regionPrefix) + 10];

	hasOpaqueSegments = hasTransparentSegments = false;
	for (int i=0; i<regionsCount; i++) {

		sprintf(currentRegionName, "%s%s%d", regionPrefix, REGIONS_SEPARATOR, i);
		const Ngl::TextureRegion *currentRegion = displayManager->getTextureRegion(currentRegionName, false);

		Sprite *newSprite = 0;
		if (currentRegion) {
			newSprite = new Sprite(currentRegion);
			segmentWidth = currentRegion->originalWidth;
			segmentHeight = currentRegion->originalHeight;
			if (currentRegion->hasAlpha) {
				hasTransparentSegments = true;
			} else {
				hasOpaqueSegments = true;
			}
		}
		regionSprites.push_back(newSprite);
	}
}

void SegmentedSprite::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly) {

	// Update all sprites if parameters changed,
	if (modified) {
		modified = false;

		float scaledSegmentWidth = segmentWidth * scaleX;
		float scaledSegmentHeight = segmentHeight * scaleY;

		float cosAngle, sinAngle;
		float segmentedSpriteCenterX, segmentedSpriteCenterY;
		if (rotation) {
			cosAngle = cosf(rotation);
			sinAngle = sinf(rotation);
			segmentedSpriteCenterX = getCenterX();
			segmentedSpriteCenterY = getCenterY();
		}

		float initialLeft = getLeft() + ((columnsCount - 1) * scaledSegmentWidth);
		float currentBottom = getBottom() - ((rowsCount * scaledSegmentHeight) - getScaledHeight());

		int32_t currentSpriteIndex = (int32_t) regionSprites.size() - 1;

		for (int j=0; j<rowsCount; j++) {

			float currentLeft = initialLeft;
			for (int i=0; i<columnsCount; i++) {
				Sprite *currentSegment = regionSprites[currentSpriteIndex];
				if (currentSegment) {

					currentSegment->scaleX = scaleX;
					currentSegment->scaleY = scaleY;
					currentSegment->setLeft(currentLeft);
					currentSegment->setBottom(currentBottom);
					currentSegment->z = z;
					currentSegment->colorMask = colorMask;

					if (rotation) {
						float displacementX = currentSegment->getCenterX() - segmentedSpriteCenterX;
						float displacementY = currentSegment->getCenterY() - segmentedSpriteCenterY;

						float rotatedDisplacementX = (displacementX * cosAngle) - (displacementY * sinAngle);
						float rotatedDisplacementY = (displacementY * cosAngle) + (displacementX * sinAngle);

						currentSegment->setCenter(
								segmentedSpriteCenterX + rotatedDisplacementX,
								segmentedSpriteCenterY + rotatedDisplacementY);
					}
					currentSegment->rotation = rotation;

					if ((currentSegment->textureRegion->hasAlpha == drawTransparentOnly) || drawAllInOrder) {
						currentSegment->draw(spriteBatch);
					}
				}

				currentSpriteIndex--;
				currentLeft -= scaledSegmentWidth;
			}

			currentBottom += scaledSegmentHeight;
		}

	} else {

		// Just draw, no updates,
		for (int i=0; i<regionSprites.size(); i++) {
			if (regionSprites[i] &&
				((regionSprites[i]->textureRegion->hasAlpha == drawTransparentOnly) || drawAllInOrder)) {
				regionSprites[i]->draw(spriteBatch);
			}
		}
	}
}
