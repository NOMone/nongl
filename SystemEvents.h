#pragma once

#include <stdint.h>

void systemEventTouchDown(int32_t pointerId, float x, float y);
void systemEventTouchDrag(int32_t pointerId, float x, float y);
void systemEventTouchUp(int32_t pointerId, float x, float y);
void systemEventTouchCancel(int32_t pointerId, float x, float y);
void systemEventScroll(float x, float y, float xOffset, float yOffset);
void systemEventBackPressed();
void systemEventMenuPressed();
void systemEventKeyDown(int32_t keyCode);
void systemEventKeyUp(int32_t keyCode);
void systemEventMenuItemSelected(int32_t itemId);

void systemDispatchEvents();
