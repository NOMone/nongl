#include "Interpolator.h"
#include "Nongl.h"

/////////////////////////////
// Interpolator
/////////////////////////////

Interpolator::Interpolator() : duration(0), currentDuration(0), delay(0), running(false), inverted(false) {}

void Interpolator::set(float duration, float delay) {
	this->duration = duration;
	this->delay = delay;
	this->currentDuration = 0;
	this->running = false;
	this->inverted = false;
}

int Interpolator::interpolateColors(int startColor, int endColor) {

	float ratio = getRatio();
	int startComponent = startColor & 255;
	int endComponent   = endColor   & 255;
	int interpolatedColor = startComponent + (int) ((endComponent - startComponent) * ratio);
	if (interpolatedColor > 255) interpolatedColor = 255;
	if (interpolatedColor <   0) interpolatedColor = 0;

	startComponent = (startColor >> 8) & 255;
	endComponent   = (endColor   >> 8) & 255;
	int interpolatedComponent = startComponent + (int) ((endComponent - startComponent) * ratio);
	if (interpolatedComponent > 255) interpolatedComponent = 255;
	if (interpolatedComponent <   0) interpolatedComponent = 0;
	interpolatedColor |= interpolatedComponent << 8;

	startComponent = (startColor >> 16) & 255;
	endComponent   = (endColor   >> 16) & 255;
	interpolatedComponent = startComponent + (int) ((endComponent - startComponent) * ratio);
	if (interpolatedComponent > 255) interpolatedComponent = 255;
	if (interpolatedComponent <   0) interpolatedComponent = 0;
	interpolatedColor |= interpolatedComponent << 16;

	startComponent = (startColor >> 24) & 255;
	endComponent   = (endColor   >> 24) & 255;
	interpolatedComponent = startComponent + (int) ((endComponent - startComponent) * ratio);
	if (interpolatedComponent > 255) interpolatedComponent = 255;
	if (interpolatedComponent <   0) interpolatedComponent = 0;
	interpolatedColor |= interpolatedComponent << 24;

	return interpolatedColor;
}

// Returns true when finished.
bool Interpolator::update(float elapsedTime) {

	if (running) {

		if (currentDuration == duration + delay)
			return true;

		currentDuration += elapsedTime;
		if (hasFinished()) {

			currentDuration = duration + delay;
			if (!onInterpolationEndListener.expired()) Nongl::scheduler.schedule(onInterpolationEndListener);

			return true;
		}
	}

	return false;
}

/////////////////////////////
// Elastic Out
/////////////////////////////

float ElasticOutInterpolator::getRatio() {

	float time = Interpolator::getRatio();
	if (time==0) return 0;
	if (time==1) return 1;

	float dampingFactor = -((damping + 0.1f) * 10 + 10);

	float p = 0.3f;
	float s = p * 0.25f;

	return powf(2, dampingFactor*time) * sinf((time-s) * TWO_PI / p) + 1;
}


/////////////////////////////
// Elastic In Out
/////////////////////////////

float ElasticInOutInterpolator::getRatio() {

	float time = Interpolator::getRatio();

	if (time==0) return 0;
	if (time==1) return 1;

	time *= 2;

	float p = 0.3f * 1.5f;
	float s = p * 0.25f;

	if (time < 1) {
		float postFix = powf(2, 10 * (time-=1)); // postIncrement is evil
		return -0.5f * postFix * sinf((time-s) * TWO_PI / p);
	}

	float postFix = powf(2, -10 * (time-=1)); // postIncrement is evil

	return postFix * sinf((time-s) * TWO_PI / p) * 0.5f + 1;
}

/////////////////////////////
// Elastic In Out
/////////////////////////////

float CycleInterpolator::getRatio() {

	float time = Interpolator::getRatio();

	if ((time==0) || (time==1)) return 0;

	float ratio;
	if (allowNegative) {
		ratio = sinf(time*TWO_PI*cyclesCount);
	} else {
		ratio = 0.5f - (0.5 * cosf(time*TWO_PI*cyclesCount));
	}
	if (damping) ratio *= (1.0f-time);

	return ratio;
}
