#include "SystemEvents.h"
#include "Nongl.h"

#include <pthread.h>

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// Events
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

class Mutex {
	pthread_mutexattr_t mutexAttribute;
public:
	pthread_mutex_t mutex;

	Mutex() {
		pthread_mutexattr_init(&mutexAttribute);
		pthread_mutexattr_settype(&mutexAttribute, PTHREAD_MUTEX_NORMAL);
		pthread_mutex_init(&mutex, &mutexAttribute);
	}

	~Mutex() {
		pthread_mutex_destroy(&mutex);
		pthread_mutexattr_destroy(&mutexAttribute);
	}
};

class Lock {
    pthread_mutex_t &m;
public:
    explicit Lock(Mutex &mutex) : m(mutex.mutex) { pthread_mutex_lock(&m); }
    ~Lock() { pthread_mutex_unlock(&m); }
};

class JavaEvent {
public:
	virtual ~JavaEvent() {}
	virtual bool dispatch() = 0;
};

class JavaBackKeyEvent : public JavaEvent { public: bool dispatch(); };
class JavaMenuKeyEvent : public JavaEvent { public: bool dispatch(); };

class JavaTouchEvent : public JavaEvent {
protected:
	int pointerId;
	float x, y;
public:
	JavaTouchEvent(int pointerId, float x, float y) : pointerId(pointerId), x(x), y(y) {}
};

class JavaTouchDownEvent : public JavaTouchEvent {
public:
	JavaTouchDownEvent(int pointerId, float x, float y) : JavaTouchEvent(pointerId, x, y) {}
	bool dispatch();
};

class JavaTouchDragEvent : public JavaTouchEvent {
public:
	JavaTouchDragEvent(int pointerId, float x, float y) : JavaTouchEvent(pointerId, x, y) {}
	bool dispatch();
};

class JavaTouchUpEvent : public JavaTouchEvent {
public:
	JavaTouchUpEvent(int pointerId, float x, float y) : JavaTouchEvent(pointerId, x, y) {}
	bool dispatch();
};

class JavaTouchCancelEvent : public JavaTouchEvent {
public:
	JavaTouchCancelEvent(int pointerId, float x, float y) : JavaTouchEvent(pointerId, x, y) {}
	bool dispatch();
};

class JavaScrollEvent : public JavaEvent {
	float x, y;
	float xOffset, yOffset;
public:
	JavaScrollEvent(float x, float y, float xOffset, float yOffset) : x(x), y(y), xOffset(xOffset), yOffset(yOffset) {}
	bool dispatch();
};

class JavaKeyDownEvent : public JavaEvent {
	int keyCode;
public:
	JavaKeyDownEvent(int keyCode) : keyCode(keyCode) {}
	bool dispatch();
};

class JavaKeyUpEvent : public JavaEvent {
	int keyCode;
public:
	JavaKeyUpEvent(int keyCode) : keyCode(keyCode) {}
	bool dispatch();
};

#include <queue>

static Mutex eventsQueueMutex;
static std::queue<JavaEvent *> eventsQueue;
static std::queue<JavaBackKeyEvent> backKeyEventsQueue;
static std::queue<JavaMenuKeyEvent> menuKeyEventsQueue;
static std::queue<JavaTouchDownEvent> touchDownEventsQueue;
static std::queue<JavaTouchDragEvent> touchDragEventsQueue;
static std::queue<JavaTouchUpEvent> touchUpEventsQueue;
static std::queue<JavaTouchCancelEvent> touchCancelEventsQueue;
static std::queue<JavaScrollEvent> scrollEventsQueue;
static std::queue<JavaKeyDownEvent> keyDownEventsQueue;
static std::queue<JavaKeyUpEvent> keyUpEventsQueue;

bool JavaBackKeyEvent::dispatch() {
	bool consumed = Nongl::onBackPressed();
	if (!consumed) {
		backKeyEventsQueue.push(*this);
		eventsQueue.push(&backKeyEventsQueue.back());
	}
	backKeyEventsQueue.pop();
	return consumed;
}

bool JavaMenuKeyEvent::dispatch() {
	bool consumed = Nongl::onMenuPressed();
	if (!consumed) {
		menuKeyEventsQueue.push(*this);
		eventsQueue.push(&menuKeyEventsQueue.back());
	}
	menuKeyEventsQueue.pop();
	return consumed;
}

bool JavaTouchDownEvent::dispatch() {
	bool consumed = Nongl::onTouchDown(pointerId, x, y);
	if (!consumed) {
		touchDownEventsQueue.push(*this);
		eventsQueue.push(&touchDownEventsQueue.back());
	}
	touchDownEventsQueue.pop();
	return consumed;
}

bool JavaTouchDragEvent::dispatch() {
	bool consumed = Nongl::onTouchDrag(pointerId, x, y);
	if (!consumed) {
		touchDragEventsQueue.push(*this);
		eventsQueue.push(&touchDragEventsQueue.back());
	}
	touchDragEventsQueue.pop();
	return consumed;
}

bool JavaTouchUpEvent::dispatch() {
	bool consumed = Nongl::onTouchUp(pointerId, x, y);
	if (!consumed) {
		touchUpEventsQueue.push(*this);
		eventsQueue.push(&touchUpEventsQueue.back());
	}
	touchUpEventsQueue.pop();
	return consumed;
}

bool JavaTouchCancelEvent::dispatch() {
	bool consumed = Nongl::onTouchCancel(pointerId, x, y);
	if (!consumed) {
		touchCancelEventsQueue.push(*this);
		eventsQueue.push(&touchCancelEventsQueue.back());
	}
	touchCancelEventsQueue.pop();
	return consumed;
}

bool JavaScrollEvent::dispatch() {
	bool consumed = Nongl::onScroll(x, y, xOffset, yOffset);
	if (!consumed) {
		scrollEventsQueue.push(*this);
		eventsQueue.push(&scrollEventsQueue.back());
	}
	scrollEventsQueue.pop();
	return consumed;
}

bool JavaKeyDownEvent::dispatch() {
	bool consumed = Nongl::onKeyDown(keyCode);
	if (!consumed) {
		keyDownEventsQueue.push(*this);
		eventsQueue.push(&keyDownEventsQueue.back());
	}
	keyDownEventsQueue.pop();
	return consumed;
}

bool JavaKeyUpEvent::dispatch() {
	bool consumed = Nongl::onKeyUp(keyCode);
	if (!consumed) {
		keyUpEventsQueue.push(*this);
		eventsQueue.push(&keyUpEventsQueue.back());
	}
	keyUpEventsQueue.pop();
	return consumed;
}

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

void systemEventTouchDown(int32_t pointerId, float x, float y) {
	Lock lock(eventsQueueMutex);
	JavaTouchDownEvent event(pointerId, x, y);
	touchDownEventsQueue.push(event);
	eventsQueue.push(&touchDownEventsQueue.back());
}

void systemEventTouchDrag(int32_t pointerId, float x, float y) {
	Lock lock(eventsQueueMutex);
	JavaTouchDragEvent event(pointerId, x, y);
	touchDragEventsQueue.push(event);
	eventsQueue.push(&touchDragEventsQueue.back());
}

void systemEventTouchUp(int32_t pointerId, float x, float y) {
	Lock lock(eventsQueueMutex);
	JavaTouchUpEvent event(pointerId, x, y);
	touchUpEventsQueue.push(event);
	eventsQueue.push(&touchUpEventsQueue.back());
}

void systemEventTouchCancel(int32_t pointerId, float x, float y) {
	Lock lock(eventsQueueMutex);
	JavaTouchCancelEvent event(pointerId, x, y);
	touchCancelEventsQueue.push(event);
	eventsQueue.push(&touchCancelEventsQueue.back());
}

void systemEventScroll(float x, float y, float xOffset, float yOffset) {
	Lock lock(eventsQueueMutex);
	JavaScrollEvent event(x, y, xOffset, yOffset);
	scrollEventsQueue.push(event);
	eventsQueue.push(&scrollEventsQueue.back());
}

void systemEventBackPressed() {
	Lock lock(eventsQueueMutex);
	JavaBackKeyEvent event;
	backKeyEventsQueue.push(event);
	eventsQueue.push(&backKeyEventsQueue.back());
}

void systemEventMenuPressed() {
	Lock lock(eventsQueueMutex);
	JavaMenuKeyEvent event;
	menuKeyEventsQueue.push(event);
	eventsQueue.push(&menuKeyEventsQueue.back());
}

void systemEventKeyDown(int32_t keyCode) {
	Lock lock(eventsQueueMutex);
	JavaKeyDownEvent event(keyCode);
	keyDownEventsQueue.push(event);
	eventsQueue.push(&keyDownEventsQueue.back());
}

void systemEventKeyUp(int32_t keyCode) {
	Lock lock(eventsQueueMutex);
	JavaKeyUpEvent event(keyCode);
	keyUpEventsQueue.push(event);
	eventsQueue.push(&keyUpEventsQueue.back());
}

void systemEventMenuItemSelected(int32_t itemId) {
	Nongl::onMenuItemSelected(itemId);
}

///////////////////////////////////////////////////////
// Dispatch
///////////////////////////////////////////////////////

void systemDispatchEvents() {

	// Intended scope (RAII),
	{
		Lock lock(eventsQueueMutex);

		int32_t eventsCount = (int32_t) eventsQueue.size();
		for (int32_t i=0; i<eventsCount; i++) {
			JavaEvent *event = eventsQueue.front();
			eventsQueue.pop();
			event->dispatch();
		}
	}
}
