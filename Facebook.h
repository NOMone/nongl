#if NONGL_FACEBOOK_SDK

#pragma once

#include <Nongl/TextUtils.h>
#include <Nongl/SystemFacebookSdk.h>
#include <Nongl/HttpRequest.h>

#include <functional>

namespace JSON {
    class Value;
}

namespace Ngl {

    template <typename T>
    class ManagedPointer;

    class Facebook {
        static TextBuffer appId, clientToken;
    public:

        static void isLoggedIn(std::function<void(bool loggedIn, TextBuffer id, TextBuffer name)> listener);

        static void login(
                const TextBuffer &appId,
                const TextBuffer &clientToken,
                const TextBuffer &permissions,
                const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener);

        static void logout();

        static void devicesLogin(
                const TextBuffer &appId,
                const TextBuffer &clientToken,
                const TextBuffer &permissions,
                const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener);

        static void performSimpleGraphApiRequest(
                const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
                Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onResponseReceivedListener);

        static void me(
                const TextBuffer &fields,
                Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onResponseReceivedListener);
    };
}

#endif