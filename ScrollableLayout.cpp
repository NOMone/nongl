#include "ScrollableLayout.h"

#include "DisplayManager.h"
#include "SpriteBatch.h"
#include "utils.h"

using namespace Ngl;

#define BLOCK_CHILDREN_EVENTS_DRAG_DISTANCE_CMS 0.25f

#define MINIMUM_ZOOM_RATIO 0.01f
#define MAXIMUM_ZOOM_RATIO 0.1f

#define DEFAULT_FLICK_X_MULTIPLIER 0.5f
#define DEFAULT_FLICK_Y_MULTIPLIER 0.5f

#define FLICK_DECCELARATION_PER_SECOND 3.0f

#define SPRING_BACK_DURATION_MILLIS 250.0f
#define MOVEMENT_DURATION_MILLIS 250.0f

#define SCROLL_EVENT_TIMEOUT_MILLIS 250.0f
#define ZOOM_EVENT_TIMEOUT_MILLIS 250.0f

#define DEFAULT_MIN_ZOOM_SCALE  0.1f
#define DEFAULT_MAX_ZOOM_SCALE 10.0f

ScrollableLayout::ScrollableLayout() {
	set(0, 0, 100, 100, 100, 100);
}

ScrollableLayout::ScrollableLayout(
		float x, float y, float width, float height,
		float scrollWidth, float scrollHeight) {

	set(x, y, width, height, scrollWidth, scrollHeight);
}

void ScrollableLayout::set(
		float x, float y, float width, float height,
		float scrollWidth, float scrollHeight) {

	this->x = x;
	this->y = y;
	this->z = 0;
	this->width = width;
	this->height = height;
	this->scrollWidth = scrollWidth;
	this->scrollHeight = scrollHeight;
	this->scrollX = 0;
	this->scrollY = 0;
	this->zoomScale = 1;

	this->isReceivingGesture = false;
	this->receivingGestureTimeout = 0;

	this->lastTouchX = 0;
	this->lastTouchY = 0;

	this->scrollSpeedX = 0;
	this->scrollSpeedY = 0;

	this->lastScrollX = 0;
	this->lastScrollY = 0;
	this->horizontalSpringInterpolator.set(SPRING_BACK_DURATION_MILLIS, 0);
	this->verticalSpringInterpolator.set(SPRING_BACK_DURATION_MILLIS, 0);
	this->zoomSpringInterpolator.set(SPRING_BACK_DURATION_MILLIS, 0);

	this->scrollMovementInterpolator = &this->defaultScrollMovementInterpolator;
	this->zoomMovementInterpolator   = &this->defaultZoomMovementInterpolator;
	this->defaultScrollMovementInterpolator.set(MOVEMENT_DURATION_MILLIS, 0);
	this->defaultZoomMovementInterpolator  .set(MOVEMENT_DURATION_MILLIS, 0);
	this->defaultScrollMovementInterpolator.finish();
	this->defaultZoomMovementInterpolator.finish();
	this->targetScrollX = 0;
	this->targetScrollY = 0;
	this->initialScrollX = 0;
	this->initialScrollY = 0;
	this->targetZoomScale = 1;
	this->initialZoomScale = 1;
	this->movementAboutCenter = true;

	this->isZooming = false;
	this->lastZoomEventScale = 0;

	this->draggable = true;
	this->flickable = true;
	this->zoomable = true;
	this->clipping = true;
	this->horizontalSpring = false;
	this->verticalSpring = false;
	this->zoomSpring = false;

	this->minZoomScale = DEFAULT_MIN_ZOOM_SCALE;
	this->maxZoomScale = DEFAULT_MAX_ZOOM_SCALE;

	this->flickXMultiplier = DEFAULT_FLICK_X_MULTIPLIER;
	this->flickYMultiplier = DEFAULT_FLICK_Y_MULTIPLIER;
	this->snapToGridCellWidth  = 0;
	this->snapToGridCellHeight = 0;
}

ScrollableLayout::~ScrollableLayout() {
	if (scrollMovementInterpolator != &defaultScrollMovementInterpolator) delete scrollMovementInterpolator;
	if (  zoomMovementInterpolator != &  defaultZoomMovementInterpolator) delete   zoomMovementInterpolator;
}

void ScrollableLayout::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly) {

	// Flush anything previously drawn so it won't be affected by changes,
	spriteBatch->flush();

	// Save current settings,
	// Viewport transformation,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	displayManager->pushViewPortTransformation();

	// Clipping,
	bool clipped = clipping;   // Buffer the value of clipping,
	if (clipped) {
		displayManager->pushClipping();

		// Adjust clipping,
		displayManager->setClipping(getLeft(), getBottom(), width, height);
	}

	// Adjust view port,
	// Add view and scroll positions,
	displayManager->viewPortTransformation.transformationX = ((displayManager->viewPortTransformation.transformationX + getLeft  ()) / zoomScale) - scrollX;
	displayManager->viewPortTransformation.transformationY = ((displayManager->viewPortTransformation.transformationY + getBottom()) / zoomScale) - scrollY;
	displayManager->viewPortTransformation.transformationZ += z;
	displayManager->viewPortTransformation.transformationWidth  /= zoomScale;
	displayManager->viewPortTransformation.transformationHeight /= zoomScale;

	// Draw,
	// Draw children,
	if (drawAllInOrder) {
		Layout::drawAllInOrder(spriteBatch);
	} else 	if (drawTransparentOnly) {
		Layout::drawTransparentParts(spriteBatch);
	} else {
		Layout::drawOpaqueParts(spriteBatch);
	}

	// Flush everything before restoring old settings,
	spriteBatch->flush();

	// Restore settings,
	displayManager->popViewPortTransformation();
	if (clipped) displayManager->popClipping();
}

Ngl::Rect ScrollableLayout::transformParentRectToLocalRect(const Ngl::Rect &rect) {
	return Rect(
			((rect.left   - getLeft  ()) / zoomScale) + scrollX,
			((rect.bottom - getBottom()) / zoomScale) + scrollY,
			((rect.right  - getLeft  ()) / zoomScale) + scrollX,
			((rect.top    - getBottom()) / zoomScale) + scrollY);
}

void ScrollableLayout::adjustDepths(float maxZ, float minZ) {

	this->z = maxZ;

	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (!childrenViewsCount) return ;
	if (childrenViewsCount==1) {
		childrenViews[0]->adjustDepths(0, minZ-maxZ);
	} else {

		int depthLayersCount = getDepthLayersCount();
		if (!depthLayersCount) return ;

		float depthRange = maxZ - minZ;
		float totalPadding = depthRange / 100;
		float childPadding = totalPadding / (childrenViewsCount-1);
		float layerRange = (depthRange - totalPadding) / depthLayersCount;

		maxZ = 0;   // Because depth is relative in this layout.

		for (int i=0; i<childrenViewsCount; i++) {
			float currentChildRange = layerRange * childrenViews[i]->getDepthLayersCount();
			childrenViews[i]->adjustDepths(maxZ, maxZ-currentChildRange);
			maxZ -= currentChildRange + childPadding;
		}
	}
}

void ScrollableLayout::update(float elapsedTimeMillis) {

	// Update flick motion,
	if (!flickInterpolator.hasFinished()) {
		flickInterpolator.update(elapsedTimeMillis);
		scrollX = scrollX + flickInterpolator.interpolate(scrollSpeedX, 0) * elapsedTimeMillis;
		scrollY = scrollY + flickInterpolator.interpolate(scrollSpeedY, 0) * elapsedTimeMillis;
	}

	// Update movement,
	if (!scrollMovementInterpolator->hasFinished()) {
		scrollMovementInterpolator->update(elapsedTimeMillis);
		zoomMovementInterpolator  ->update(elapsedTimeMillis);

		if (movementAboutCenter) {
			float initialScrollCenterX = initialScrollX + (0.5f * width  / initialZoomScale);
			float initialScrollCenterY = initialScrollY + (0.5f * height / initialZoomScale);
			float  targetScrollCenterX =  targetScrollX + (0.5f * width  /  targetZoomScale);
			float  targetScrollCenterY =  targetScrollY + (0.5f * height /  targetZoomScale);

			float currentScrollCenterX = scrollMovementInterpolator->interpolate(initialScrollCenterX, targetScrollCenterX);
			float currentScrollCenterY = scrollMovementInterpolator->interpolate(initialScrollCenterY, targetScrollCenterY);
			zoomScale = zoomMovementInterpolator->interpolate(initialZoomScale, targetZoomScale);
			scrollX = currentScrollCenterX - (0.5f * width  / zoomScale);
			scrollY = currentScrollCenterY - (0.5f * height / zoomScale);
		} else {
			scrollX = scrollMovementInterpolator->interpolate(initialScrollX, targetScrollX);
			scrollY = scrollMovementInterpolator->interpolate(initialScrollY, targetScrollY);
			zoomScale = zoomMovementInterpolator->interpolate(initialZoomScale, targetZoomScale);
		}
	}

	horizontalSpringInterpolator.update(elapsedTimeMillis);
	verticalSpringInterpolator.update(elapsedTimeMillis);
	zoomSpringInterpolator.update(elapsedTimeMillis);

	// Enforce boundary conditions,
	enforceBoundaries();

	// Update children,
	Layout::update(elapsedTimeMillis);

	// Update receiving gestures timeout,
	receivingGestureTimeout -= elapsedTimeMillis;
	if (receivingGestureTimeout < 0) receivingGestureTimeout = 0;
}

bool ScrollableLayout::onTouchEvent(const TouchEvent *event) {

	if (!isEnabled()) return false;

	// If clipping, check if hit within boundaries before sending to children,
	if (clipping) {
		if ((event->downX < getLeft  ()) || (event->downX >= getRight()) ||
			(event->downY < getBottom()) || (event->downY >= getTop  ())) {
			return false;
		}
	}

	// Transform the event to the local coordinates,
	TouchEvent transformedTouchEvent(*event);
	transformedTouchEvent.x = ((transformedTouchEvent.x - getLeft()) / zoomScale) + scrollX;
	transformedTouchEvent.y = ((transformedTouchEvent.y - getBottom()) / zoomScale) + scrollY;
	transformedTouchEvent.downX = ((transformedTouchEvent.downX - getLeft()) / zoomScale) + scrollX;
	transformedTouchEvent.downY = ((transformedTouchEvent.downY - getBottom()) / zoomScale) + scrollY;

	// If consumed by a child, then skip,
	if ((!isReceivingGesture) && Layout::onTouchEvent(&transformedTouchEvent)) {

		// Stop flick scrolling
		if (event->type == TouchEvent::TOUCH_DOWN) {
		    scrollSpeedX = scrollSpeedY = 0;

		    // Track last touch (children may not consume drag events, in which
		    // case the lastTouchX and lastTouchY are undefined),
	        lastTouchX = event->downX;
	        lastTouchY = event->downY;
		}

		return true;
	}

	// If not clipping, check if hit within boundaries after sending to children,
	if (!clipping) {
		if ((event->downX < getLeft  ()) || (event->downX >= getRight()) ||
			(event->downY < getBottom()) || (event->downY >= getTop  ())) {
			return false;
		}
	}

	// Event not consumed by a child,
	// TODO: break into methods,
	if (event->type == TouchEvent::TOUCH_DOWN) {

		lastTouchX = event->downX;
		lastTouchY = event->downY;

		// Stop flick scrolling
		scrollSpeedX = scrollSpeedY = 0;

	} else if (event->type == TouchEvent::TOUCH_DRAG) {

		if (draggable) {
			if (event->maxDraggedDistanceCms > BLOCK_CHILDREN_EVENTS_DRAG_DISTANCE_CMS) isReceivingGesture = true;

			scrollX = scrollX - (event->x - lastTouchX) / zoomScale;
			scrollY = scrollY - (event->y - lastTouchY) / zoomScale;

			lastTouchX = event->x;
			lastTouchY = event->y;
		}

	} else if (event->type == TouchEvent::TOUCH_UP) {

		// TODO: when pointers count = 0 instead of touch up?
		isReceivingGesture = false;

		// Since touch downs are delivered to children, we should
		// match them with touch ups as well,
		Layout::onTouchEvent(&transformedTouchEvent);

	} else if (event->type == TouchEvent::SCROLL) {

		// Proceed only if zooming is enabled,
		if (!zoomable) return false;

		// Adjust zoom,
		float newZoomScale = zoomScale + (0.1f * event->yMagnitude);

		// Force min and max zoom scales,
		if (newZoomScale <= minZoomScale) {
			newZoomScale = minZoomScale;
		} else if (newZoomScale >= maxZoomScale) {
			newZoomScale = maxZoomScale;
		}

		// Skip immediately if spring effect not enabled,
		if (!zoomSpring) {
			if (newZoomScale * scrollWidth  <  width) newZoomScale = width  /  scrollWidth;
			if (newZoomScale * scrollHeight < height) newZoomScale = height / scrollHeight;
		}

		// If nothing changed, event is not consumed,
		if (newZoomScale == zoomScale) return false;
		zoomScale = newZoomScale;

		// Keep scroll location intact,
		float zoomedX = ((event->x - getLeft  ()) / zoomScale) + scrollX;
		float zoomedY = ((event->y - getBottom()) / zoomScale) + scrollY;

		scrollX += transformedTouchEvent.x - zoomedX;
		scrollY += transformedTouchEvent.y - zoomedY;

		receivingGestureTimeout = SCROLL_EVENT_TIMEOUT_MILLIS;

	} else if (event->type == TouchEvent::ZOOM) {

		// Proceed only if zooming is enabled,
		if (!zoomable) return false;

		// Set initial zoom event scale,
		if (!isZooming) {
			isZooming = true;
			lastZoomEventScale = 1;
		}

		// Adjust zoom,
		// Force minimum ratio,
		float ratio = event->ratio / lastZoomEventScale;
		float absoluteRatio = ((ratio - 1) > 0) ? (ratio - 1) : (1 - ratio);
		if (absoluteRatio >= MINIMUM_ZOOM_RATIO) {

			// Force maximum ratio,
			if (absoluteRatio > MAXIMUM_ZOOM_RATIO) {
				if (ratio > 1) {
					ratio = 1 + MAXIMUM_ZOOM_RATIO;
				} else {
					ratio = 1 - MAXIMUM_ZOOM_RATIO;
				}
			}

			// Handle event,
			lastZoomEventScale = event->ratio;

			float newZoomScale = zoomScale * ratio;
			if (newZoomScale <= 0) newZoomScale = zoomScale;

			// Force min and max zoom scales,
			if (newZoomScale <= minZoomScale) {
				newZoomScale = minZoomScale;
			} else if (newZoomScale >= maxZoomScale) {
				newZoomScale = maxZoomScale;
			}

			// Skip immediately if spring effect not enabled,
			if (!zoomSpring) {
				if (newZoomScale * scrollWidth  <  width) newZoomScale = width  /  scrollWidth;
				if (newZoomScale * scrollHeight < height) newZoomScale = height / scrollHeight;
			}

			// If nothing changed, event is not consumed,
			if (newZoomScale == zoomScale) return false;
			zoomScale = newZoomScale;

			// Keep scroll location intact,
			float zoomedX = ((event->x - getLeft  ()) / zoomScale) + scrollX;
			float zoomedY = ((event->y - getBottom()) / zoomScale) + scrollY;

			scrollX += transformedTouchEvent.x - zoomedX;
			scrollY += transformedTouchEvent.y - zoomedY;
		}

		receivingGestureTimeout = ZOOM_EVENT_TIMEOUT_MILLIS;
	} else if (event->type == TouchEvent::ZOOM_END) {
		isZooming = false;

	} else if (event->type == TouchEvent::FLICK) {
		if (flickable) {
			scrollSpeedX = - event->xMagnitude * flickXMultiplier / zoomScale;
			scrollSpeedY = - event->yMagnitude * flickYMultiplier / zoomScale;

			float speed = magnitude(scrollSpeedX, scrollSpeedY);
			flickInterpolator.set(speed / (FLICK_DECCELARATION_PER_SECOND/1000.0f));
			flickInterpolator.restart();
		}
	} else {
		return false;
	}

	return true;
}

void ScrollableLayout::zoomAbout(float zoomX, float zoomY, float zoomScale) {

	float untransformedX = ((zoomX - scrollX) * this->zoomScale) + getLeft();
	float untransformedY = ((zoomY - scrollY) * this->zoomScale) + getBottom();

	float zoomedX = ((untransformedX - getLeft  ()) / zoomScale) + scrollX;
	float zoomedY = ((untransformedY - getBottom()) / zoomScale) + scrollY;

	scrollX += zoomX - zoomedX;
	scrollY += zoomY - zoomedY;

	this->zoomScale = zoomScale;
}

void ScrollableLayout::moveTo(
		float scrollX, float scrollY, float zoomScale, float durationMillis, float delayMillis,
		Interpolator *scrollInterpolator, Interpolator *zoomInterpolator, bool aboutCenter) {

	initialScrollX = this->scrollX;
	initialScrollY = this->scrollY;
	initialZoomScale = this->zoomScale;

	targetScrollX = scrollX;
	targetScrollY = scrollY;
	targetZoomScale = zoomScale;

	// Set the scroll interpolator,
	if (scrollMovementInterpolator != &defaultScrollMovementInterpolator) {
		delete scrollMovementInterpolator;
	}
	if (scrollInterpolator) {
		scrollMovementInterpolator = scrollInterpolator->clone();
	} else {
		scrollMovementInterpolator = &defaultScrollMovementInterpolator;
	}

	// Set the zoom interpolator,
	if (zoomMovementInterpolator != &defaultZoomMovementInterpolator) {
		delete zoomMovementInterpolator;
	}
	if (zoomInterpolator) {
		zoomMovementInterpolator = zoomInterpolator->clone();
	} else {
		zoomMovementInterpolator = &defaultZoomMovementInterpolator;
	}

	// Set durations and trigger,
	if (durationMillis < 1) durationMillis = 1;
	scrollMovementInterpolator->set(durationMillis, delayMillis);
	zoomMovementInterpolator  ->set(durationMillis, delayMillis);
	scrollMovementInterpolator->restart();
	zoomMovementInterpolator  ->restart();
}

void ScrollableLayout::enforceBoundaries() {

	// Zoom correction,
	if (zoomSpring) {
		// Spring back,
		// Work only when no zooming is taking action,
		if (isReceivingGesture || (receivingGestureTimeout != 0) || (lastZoomScale != zoomScale)) {
			zoomSpringInterpolator.restart();
		}

		if (scrollMovementInterpolator->hasFinished()) {
			if (zoomScale * scrollWidth  <  width) {
				zoomScale = zoomSpringInterpolator.interpolate(zoomScale, width  /  scrollWidth);
			}
			if (zoomScale * scrollHeight < height) {
				zoomScale = zoomSpringInterpolator.interpolate(zoomScale, height / scrollHeight);
			}
			lastZoomScale = zoomScale;
		}
	} else {
		// Jump back,
		if (zoomScale * scrollWidth  <  width) zoomScale = width  /  scrollWidth;
		if (zoomScale * scrollHeight < height) zoomScale = height / scrollHeight;
	}

	// Scroll position correction,
	// X,
	if (snapToGridCellWidth != 0) {

		// Spring back,
		// Work only when no scrolling is taking action,
		if (isScrolling()) {
			horizontalSpringInterpolator.restart();
		}

		if (scrollMovementInterpolator->hasFinished()) {
			if ((scrollX >= 0) && (scrollX <= scrollWidth - (width / zoomScale))) {
				int closestCellColumn = getClosestCellColumn();
				int closestCellScrollX = closestCellColumn * snapToGridCellWidth;
				scrollX = horizontalSpringInterpolator.interpolate(scrollX, closestCellScrollX);
			}
			lastScrollX = scrollX;
		}
	}

	if (horizontalSpring) {
		// Spring back,
		// Work only when no scrolling is taking action,
		if (isScrolling()) {
			horizontalSpringInterpolator.restart();
		}

		if (scrollMovementInterpolator->hasFinished()) {
			if (scrollX < 0) {
				scrollX = horizontalSpringInterpolator.interpolate(scrollX, 0);
			} else if (scrollX > scrollWidth - (width / zoomScale)) {
				float newScrollX = scrollWidth - (width / zoomScale);
				if (newScrollX >= 0) {
					scrollX = horizontalSpringInterpolator.interpolate(scrollX, scrollWidth - (width / zoomScale));
				}
			}
			lastScrollX = scrollX;
		}
	} else {
		// Jump back,
		if (scrollX > scrollWidth - (width / zoomScale)) {
			scrollX = scrollWidth - (width / zoomScale);
			scrollSpeedX = 0;
		}
		if (scrollX < 0) {
			scrollX = 0;
			scrollSpeedX = 0;
		}
	}

	// Y,
	if (snapToGridCellHeight != 0) {

		// Spring back,
		// Work only when no scrolling is taking action,
		if (isScrolling()) {
			verticalSpringInterpolator.restart();
		}

		if (scrollMovementInterpolator->hasFinished()) {
			if ((scrollY >= 0) && (scrollY <= scrollHeight - (height / zoomScale))) {
				int closestCellRow = getClosestCellRow();
				int closestCellScrollY = closestCellRow * snapToGridCellHeight;
				scrollY = verticalSpringInterpolator.interpolate(scrollY, closestCellScrollY);
			}
			lastScrollY = scrollY;
		}
	}

	if (verticalSpring) {
		// Spring back,
		// Work only when no scrolling is taking action,
		if (isScrolling()) {
			verticalSpringInterpolator.restart();
		}

		if (scrollMovementInterpolator->hasFinished()) {
			if (scrollY < 0) {
				scrollY = verticalSpringInterpolator.interpolate(scrollY, 0);
			} else if (scrollY > scrollHeight - (height / zoomScale)) {
				float newScrollY = scrollHeight - (height / zoomScale);
				if (newScrollY >= 0) {
					scrollY = verticalSpringInterpolator.interpolate(scrollY, scrollHeight - (height / zoomScale));
				}
			}
			lastScrollY = scrollY;
		}
	} else {
		// Jump back,
		if (scrollY > scrollHeight - (height / zoomScale)) {
			scrollY = scrollHeight - (height / zoomScale);
			scrollSpeedY = 0;
		}
		if (scrollY < 0) {
			scrollY = 0;
			scrollSpeedY = 0;
		}
	}
}
