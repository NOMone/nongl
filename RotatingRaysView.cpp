#include "RotatingRaysView.h"

RotatingRaysView::RotatingRaysView(
                 float centerX, float centerY,
                 float radius,
                 float cyclesPerSecond,
                 int trianglesCount,
                 int centerColor1, int tipColor1,
                 int centerColor2, int tipColor2,
                 const Ngl::TextureRegion *textureRegion) {
        
    this->currentAngle = 0;
    this->radius = radius;
    this->cyclesPerSecond = cyclesPerSecond;
    this->trianglesCount = trianglesCount;
    
    this->centerColor[0] = centerColor1;
    this->tipColor[0] = tipColor1;
    this->centerColor[1] = centerColor2;
    this->tipColor[1] = tipColor2;
    
    if (!textureRegion) textureRegion = Nongl::whiteBlockTextureRegion;
    rayQuad.setTextureRegion(textureRegion);
    
    rayQuad.x = centerX;
    rayQuad.y = centerY;
}
    
void RotatingRaysView::draw(SpriteBatch *spriteBatch, bool drawTransparent, bool drawOpaque) {
    
    // Detect triangles transparency,
    bool triangleTransparent[2];
    for (int i=0; i<2; i++) {
        triangleTransparent[i] =
            rayQuad.textureRegion->hasAlpha ||
            ((rayQuad.colorMask & 0xff000000) != 0xff000000) ||
            ((centerColor[i] & 0xff000000) != 0xff000000) ||
            ((tipColor[i] & 0xff000000) != 0xff000000);
    }
    
    // Draw triangles,
    float angleStep = (2 * PI) / trianglesCount;
    for (int i=0; i<trianglesCount; i++) {
        
        // Switch colors every iteration,
        int colorIndex = i&1;
        
        // Check if this triangle is going to be drawn,
        if (triangleTransparent[colorIndex]) {
            if (!drawTransparent) continue;
        } else {
            if (!drawOpaque) continue;
        }
        
        // Adjust triangle color,
        rayQuad.points[0].colorMask = rayQuad.points[1].colorMask = centerColor[colorIndex];
        rayQuad.points[2].colorMask = rayQuad.points[3].colorMask = tipColor[colorIndex];
        
        // Adjust triangle location,
        rayQuad.points[2].x = radius;
        rayQuad.points[2].y = 0;
        rayQuad.points[2].yaw(currentAngle + (i*angleStep));
        
        rayQuad.points[3].x = radius;
        rayQuad.points[3].y = 0;
        rayQuad.points[3].yaw(currentAngle + ((i+1)*angleStep));
        
        // Draw!
        rayQuad.draw(spriteBatch);
    }
}

void RotatingRaysView::update(float elapsedTimeMillis) {
    currentAngle += elapsedTimeMillis * (cyclesPerSecond * 2 * PI) / 1000.0f;
}

