1- import RainbowDrops.epf: File->Import->General->Preferences
2- Copy "plugins" to "eclipse/dropins"

Or just drag to install from here: https://marketplace.eclipse.org/content/eclipse-moonrise-ui-theme

3- Window->preferences->General->Appearance->theme
4- Choose Dark Juno (or whatever other name it has).
5- Restart eclipse.
6- sudo apt-get install ttf-bitstream-vera
7- Window->preferences->General->Appearance->Colors and Fonts->Basic->Text Font->BitStream Vera Sans Mono
