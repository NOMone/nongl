package com.nom.utils.texture_packing;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

public class FontDataFileGenerator implements ApplicationListener {

	@Override
	public void create() {
		
		generateFontDataFile(
				"packedTexture/pack", 
				"images1.png",
				50, 25,
				-6, -6,
				"packedTexture/font.fnt");
	}
	
    private void generateFontDataFile(
    		String textureAtlasPackFilePath, 
    		String textureAtlasImageFileName,
    		int lineHeight, int baseY,
    		int displacementX, int displacementY,
    		String outputFilePath) {
    	
    	TextureAtlas textureAtlas = new TextureAtlas(textureAtlasPackFilePath);  
    	FileHandle outputFile = Gdx.files.getFileHandle(outputFilePath, FileType.Absolute);
    	
    	Array<AtlasRegion> regions = textureAtlas.getRegions();
    	
    	String quotes = Character.toString('"'); 
    	String enter = Character.toString('\n');
    	outputFile.writeString(
    			"info face=" + quotes + "Levenim MT" + quotes + " size=32 bold=0 italic=0 charset=" + quotes + quotes + " unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1" + enter, 
    			false);
    	outputFile.writeString(
    			"common lineHeight=" + lineHeight + " base=" + baseY + " scaleW=" + regions.get(0).getTexture().getWidth() + " scaleH=" + regions.get(0).getTexture().getHeight() + " pages=1 packed=0" + enter, 
    			true);
    	outputFile.writeString(
    			"page id=0 file=" + quotes + "images1.png" + quotes + enter, 
    			true);
       	outputFile.writeString(
    			"chars count=" + regions.size + enter, 
    			true);

       	int paddingX = 2;
       	for (int i=0; i<regions.size; i++) {
       		AtlasRegion region = regions.get(i);
           	outputFile.writeString(
           			"char id=" + region.name + "   x=" + (region.getRegionX()+displacementX) + "     y=" + (region.getRegionY()+displacementY) + "     width=" + region.getRegionWidth() + "     height=" + region.getRegionHeight() + "     xoffset=0     yoffset=0    xadvance=" + (region.getRegionWidth() + paddingX) + "     page=0  chnl=0" + enter, 
        			true); 
       	}

       	outputFile.writeString(
       			"kernings count=-1", 
    			true); 
    }

	@Override public void resize(int width, int height) {}
	@Override public void render() {
		Gdx.gl20.glClearColor(1f, 0, 0f, 1.0f);
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}
	@Override public void pause() {}
	@Override public void resume() {}
	@Override public void dispose() {}
}
