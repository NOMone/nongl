package com.nom.utils.texture_packing;

import gdxOld.TexturePacker;
import gdxOld.TexturePacker.Settings;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.JOptionPane;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;

public class TexturePacking {

	private static final String HISTORY_FILE_NAME = "LastModified.txt";
	private static final String INPUT_IMAGES_DIRECTORY_NAME = "InputImages";
	//private static final String OUTPUT_TEXTURES_DIRECTORY_NAME = "PackedTextures-min256-max256-2-flmnl-";
	//private static final String OUTPUT_TEXTURES_DIRECTORY_NAME = "PackedTextures-min512-max512-2-flmnl-";
	//private static final String OUTPUT_TEXTURES_DIRECTORY_NAME = "PackedTextures-min1024-max1024-2-flmnl-";
	private static final String OUTPUT_TEXTURES_DIRECTORY_NAME = "PackedTextures-min2048-max2048-2-flmnl-";
	private static final String PACKED_PACKS_DIRECTORY_NAME = "PackedPacks";
	private static final String COMBINED_PACK_FILE_NAME = "CombinedPack.txt";
	private static final String PACK_FILE_EXTENSION = ".txt";
	private static final String GROUPED_PACK_FILE_NAME = "TexturesPack.txt";
	private static final String PACKING_ARGUMENTS_SEPARATOR = "-";
	private static final String PARENT_NAME_CHILD_NAME_SEPARATOR = ".";
	
    public static void main (String[] args) throws Exception { 

    	// Set default packing settings,
    	/*
        Settings settings = new Settings();
        settings.paddingX = 12; 
		settings.paddingY = 12;
        settings.filterMin = TextureFilter.Linear;
        settings.filterMag = TextureFilter.Linear;
        settings.edgePadding = false;
        settings.duplicatePadding = false;
        settings.stripWhitespaceX = true;
        settings.stripWhitespaceY = true;
        settings.minWidth = 1;
        settings.minHeight = 1;
        settings.maxWidth = 2036;
        settings.maxHeight = 2036;
        //settings.incremental = false;
        settings.alias = true;
        settings.pot = false;
        settings.fast = false;
        */
        Settings settings = new Settings();
        settings.padding = 12; 
        settings.defaultFilterMag = TextureFilter.Linear;
        settings.defaultFilterMag = TextureFilter.Linear;
        settings.edgePadding = false;
        settings.duplicatePadding = false;
        settings.stripWhitespace = true;
        settings.minWidth = 1;
        settings.minHeight = 1;
        settings.maxWidth = 2048;
        settings.maxHeight = 2048;
        settings.incremental = false;
        settings.alias = true;
        settings.pot = false;
        settings.defaultFormat = Format.RGBA8888;
    	
    	// Pack the main textures,
        System.out.println("besm Allah :)\n");
        System.out.println("Packing textures");
    	packTextures(INPUT_IMAGES_DIRECTORY_NAME, OUTPUT_TEXTURES_DIRECTORY_NAME, settings);
    	
    	// Pack the packed textures,
    	System.out.println("\n============================\n");
        System.out.println("Packing packs");
        URL root = TexturePacking.class.getProtectionDomain().getCodeSource().getLocation();
        String path = (new File(root.toURI())).getParentFile().getPath();
		packImage(
				new File(path, OUTPUT_TEXTURES_DIRECTORY_NAME), 
				new File(path, PACKED_PACKS_DIRECTORY_NAME), 
				settings);
    	
    	// Modify the textures pack file to reflect the images' offsets within the packs,
        System.out.println("Combining packs");
		File packedTexturesPackFile = new File(new File(path, OUTPUT_TEXTURES_DIRECTORY_NAME).getPath(), GROUPED_PACK_FILE_NAME);
		NONGLTexturePack packedTexturesPack = new NONGLTexturePack(packedTexturesPackFile); 
		
		String packedPacksPackFileName;
		if (OUTPUT_TEXTURES_DIRECTORY_NAME.contains(PACKING_ARGUMENTS_SEPARATOR)) {
			packedPacksPackFileName = OUTPUT_TEXTURES_DIRECTORY_NAME.split(PACKING_ARGUMENTS_SEPARATOR)[0] + PACK_FILE_EXTENSION;
		} else {
			packedPacksPackFileName = OUTPUT_TEXTURES_DIRECTORY_NAME + PACK_FILE_EXTENSION;
		}
		File packedPacksPackFile = new File(new File(path, PACKED_PACKS_DIRECTORY_NAME).getPath(), packedPacksPackFileName);
		NONGLTexturePack packedPacksPack = new NONGLTexturePack(packedPacksPackFile); 

		NONGLTexturePack combinedPack = NONGLTexturePack.combinePacks(packedTexturesPack, packedPacksPack);
		File combinedPackFile = new File(new File(path, PACKED_PACKS_DIRECTORY_NAME).getPath(), COMBINED_PACK_FILE_NAME);
		combinedPack.saveTo(combinedPackFile);
		
    	/*
    	// Legacy code, kept for reference,
        packTrees(settings, "c:\\Textures");
		startFontDataFileGenerator();
		*/
        
    	JOptionPane.showMessageDialog(null, "al 7amd le-Allah, finished packing :)");
    } 

    static void packTextures(String inputImagesDirectoryName, String outputDirectoryName, Settings defaultPackingSettings) throws Exception {
    	
        URL root = TexturePacking.class.getProtectionDomain().getCodeSource().getLocation();
        String path = (new File(root.toURI())).getParentFile().getPath();
       
        //JOptionPane.showMessageDialog(null, new File(path, "images").getPath());
      
        // Check for input directory existence,
        File inputDirectory = new File(path, inputImagesDirectoryName);
        if (!inputDirectory.exists()) {
        	JOptionPane.showMessageDialog(null, "Expecting input images file at:\n  " + inputDirectory.getPath());
        	System.exit(0);
        }
        
        // Create an output directory if non-available,
        File outputDirectory = new File(path, outputDirectoryName);
        if (!outputDirectory.exists())
        	outputDirectory.mkdir();
                
        // Pack all the child directories,
        // Get the child directories,
        File[] imageDirectories = inputDirectory.listFiles(new FileFilter() {
			@Override public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});
                
        if (imageDirectories.length  == 0) {
        	JOptionPane.showMessageDialog(null, "Nothing to pack, expecting directories inside InputImages");
        	return;
        }
        
        // Get last modified dates from file,
        boolean[] directoryIntact = getModifiedFoldersAndCleanUp(imageDirectories, outputDirectory, path);
        		
        // Pack them,
        for (int i=0; i<imageDirectories.length; i++) {
        	if (!directoryIntact[i]) {
        		packImage(imageDirectories[i], outputDirectory, defaultPackingSettings);
        	} else {
        		System.out.println("Directory skipped: " + imageDirectories[i].getName());
        	}
        }

        // Create one large pack file containing all other pack files,
        groupPackFiles(imageDirectories, outputDirectory);
        
        // Save new last modified dates file,
        saveLastModifiedDatesFile(imageDirectories, path);
    }
    
    static void groupPackFiles(File[] imageDirectories, File outputDirectory) {

		try {

			// Create file for output,
			File outputFile = new File(outputDirectory, GROUPED_PACK_FILE_NAME);
			if (outputFile.exists())
				outputFile.delete();
			outputFile.createNewFile();
			
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
			
			// Append individual pack files to it,
			for (int i=0; i<imageDirectories.length; i++) {
    			
				// Read pack file,
				String untaggedDirectoryName = getUntaggedDirectoryName(imageDirectories[i].getName());
				File currentPackFile = new File(outputDirectory, untaggedDirectoryName + PACK_FILE_EXTENSION);
    			InputStream inputStream = new FileInputStream(currentPackFile);
    			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    		    byte[] buffer = new byte[1024];
    		    int length;
    		    while ((length = inputStream.read(buffer)) != -1) {
    		    	byteArrayOutputStream.write(buffer, 0, length);
    		    }
    		    
    		    String packData = new String(byteArrayOutputStream.toByteArray());
    		    byteArrayOutputStream.close();
    		    inputStream.close();
    		    
    		    // Write it to output,
    		    bufferedWriter.write(packData);
    		}
			
			bufferedWriter.close();
		} catch (Exception e) {
        	JOptionPane.showMessageDialog(null, "Couldn't create grouped pack file.");
        	JOptionPane.showMessageDialog(null, e.toString());
		}
    }
    
    static void packImage(File imageDirectory, File outputDirectory, Settings defaultPackingSettings) {
    	
    	// Copy the default settings,
    	Settings settings = new Settings();
        settings.padding = defaultPackingSettings.padding;
        settings.defaultFilterMin = defaultPackingSettings.defaultFilterMin;
        settings.defaultFilterMag = defaultPackingSettings.defaultFilterMag;
        settings.edgePadding = defaultPackingSettings.edgePadding;
        settings.duplicatePadding = defaultPackingSettings.duplicatePadding;
        settings.stripWhitespace = defaultPackingSettings.stripWhitespace;
        settings.minWidth = defaultPackingSettings.minWidth;
        settings.minHeight = defaultPackingSettings.minHeight;
        settings.maxWidth = defaultPackingSettings.maxWidth;
        settings.maxHeight = defaultPackingSettings.maxHeight;
        settings.incremental = defaultPackingSettings.incremental;
        settings.alias = defaultPackingSettings.alias;
        settings.pot = defaultPackingSettings.pot;
        settings.defaultFormat = defaultPackingSettings.defaultFormat;
    	
    	//TexturePacker.Settings settings = new TexturePacker.Settings(defaultPackingSettings);
    	
        // To avoid putting all images in one very long row,
        // enforce width and height maximums,
        //setRecommendedDimensions(imageDirectory, settings);
        if (settings.maxWidth > 2048)
        	settings.maxWidth = 2048;
        if (settings.minWidth > 2048)
        	settings.minWidth = 2048;

        if (settings.maxHeight > 2048)
        	settings.maxHeight = 2048;
        if (settings.minHeight > 2048)
        	settings.minHeight = 2048;
        
        // Override settings with any options specified in the folder tags,
		String directoryName = imageDirectory.getName();
		String untaggedDirectoryName = getUntaggedDirectoryName(directoryName);
		
		String[] packingArguments = directoryName.split(PACKING_ARGUMENTS_SEPARATOR);
		for (int i=1; i<packingArguments.length; i++) {
			applyArgument(packingArguments[i], settings);
		}
		
		// Pack the folder,
		/*
		TexturePacker.process(
				settings, 
				imageDirectory.getPath(),
				outputDirectory.getPath(), 
				untaggedDirectoryName + PACK_FILE_EXTENSION);
		*/
		
		TexturePacker.process(
				settings, 
				imageDirectory.getPath(),
				outputDirectory.getPath(), 
				untaggedDirectoryName + PACK_FILE_EXTENSION);

		// Remove tags from textures' names and within pack file,
		if (!imageDirectory.equals(untaggedDirectoryName)) {

			// Rename textures to remove tagging,
			int currentTextureNumber = 1;
			do {
	    		File textureFile = new File(outputDirectory, directoryName + currentTextureNumber + ".png");
	    		if (!textureFile.exists())
	    			break;
	    		
				textureFile.renameTo(new File(outputDirectory, untaggedDirectoryName + currentTextureNumber + ".png"));
	    		currentTextureNumber++;
			} while (true);
			
			// Apply renamed textures to pack file,
			fixPackFile(
					new File(outputDirectory, untaggedDirectoryName + PACK_FILE_EXTENSION),
					untaggedDirectoryName);
		}
    }
    
    static void applyArgument(String packingArgument, TexturePacker.Settings packingSettings) {
    	
    	// Dimensions,
    	if (packingArgument.startsWith("min")) {
    		int length = Integer.parseInt(packingArgument.substring(3));
    		packingSettings.minWidth = length;
    		packingSettings.minHeight = length;
    	// Max dimensions,
    	} else if (packingArgument.startsWith("max")) {
    		int length = Integer.parseInt(packingArgument.substring(3));
    		packingSettings.maxWidth = length;
    		packingSettings.maxHeight = length;
    	// Padding,
    	} else if (packingArgument.startsWith("p")) {
    		packingSettings.duplicatePadding = true;
    		packingSettings.edgePadding = true;
    	// Padding,
    	} else if (packingArgument.startsWith("e")) {
    		packingSettings.edgePadding = true;
    	// Power of two dimensions,
    	} else if (packingArgument.startsWith("2")) {
    		packingSettings.pot = true;
    	// Filters,
    	} else if (packingArgument.startsWith("fnn")) {
    		packingSettings.defaultFilterMin = TextureFilter.Nearest;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("fln")) {
    		packingSettings.defaultFilterMin = TextureFilter.Linear;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("fnl")) {
    		packingSettings.defaultFilterMin = TextureFilter.Nearest;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    	} else if (packingArgument.startsWith("fll")) {
    		packingSettings.defaultFilterMin = TextureFilter.Linear;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    		
    	// Mipmap filters,
    	} else if (packingArgument.startsWith("fnmnn")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapNearestNearest;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("fnmnl")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapNearestNearest;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    		
    	} else if (packingArgument.startsWith("flmnn")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapLinearNearest;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("flmnl")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapLinearNearest;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    		
    	} else if (packingArgument.startsWith("fnmln")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapNearestLinear;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("fnmll")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapNearestLinear;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    		
    	} else if (packingArgument.startsWith("flmln")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapLinearLinear;
    		packingSettings.defaultFilterMag = TextureFilter.Nearest;
    	} else if (packingArgument.startsWith("flmll")) {
    		packingSettings.defaultFilterMin = TextureFilter.MipMapLinearLinear;
    		packingSettings.defaultFilterMag = TextureFilter.Linear;
    	}
    }
    
    static void saveLastModifiedDatesFile(File[] imageDirectories, String historyFileLocation) {
    	
        File lastModifiedDatesFile = new File(historyFileLocation, HISTORY_FILE_NAME);
        
        if (lastModifiedDatesFile.exists())
        	lastModifiedDatesFile.delete();
        
        try {
	        lastModifiedDatesFile.createNewFile();
	    	BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(lastModifiedDatesFile));
	        for (int i=0; i<imageDirectories.length; i++) {
	        	bufferedWriter.write(imageDirectories[i].getName() + "\n");
	        	bufferedWriter.write(Long.toString(getDirectoryLastModified(imageDirectories[i])) + "\n");
	        }
	        bufferedWriter.close();
        } catch (Exception e) {}
    }
    
    static boolean[] getModifiedFoldersAndCleanUp(File[] imageDirectories, File outputDirectory, String historyFileLocation) {
    	
        boolean[] directoryIntact = new boolean[imageDirectories.length];
        File lastModifiedDatesFile = new File(historyFileLocation, HISTORY_FILE_NAME);
        if (lastModifiedDatesFile.exists()) {
 
        	try {
	        	BufferedReader bufferedReader = new BufferedReader(new FileReader(lastModifiedDatesFile));
	        	while (bufferedReader.ready()) {
		        	
	        		String currentDirectoryName = bufferedReader.readLine();
	        		long currentDirectoryLastModified = Long.parseLong(bufferedReader.readLine());
		    		
		        	// Search for the directory in the images directory,
		        	int imageIndex = -1;
		        	for (int i=0; i<imageDirectories.length; i++) {
		        		if (imageDirectories[i].getName().equals(currentDirectoryName)) {
		        			imageIndex = i;
		        			break;
		        		}
		        	}
		        	
		        	if ((imageIndex == -1) || (currentDirectoryLastModified != getDirectoryLastModified(imageDirectories[imageIndex]))) {
		        		        		
		        		// Directory no longer exists or was modified,
		        		// Delete all traces of this texture,
		        		String currentUntaggedDirectoryName = getUntaggedDirectoryName(currentDirectoryName);
		        		File packFile = new File(outputDirectory, currentUntaggedDirectoryName + PACK_FILE_EXTENSION);
		        		if (packFile.exists())
		        			packFile.delete();
	
	        			int currentTextureNumber = 1;
		        		do {
			        		File textureFile = new File(outputDirectory, currentUntaggedDirectoryName + currentTextureNumber + ".png");
			        		if (!textureFile.exists())
			        			break;
			        		
		        			textureFile.delete();
			        		currentTextureNumber++;
		        		} while (true);
		        		
		        	} else {
		        		
		        		// The folder was not modified, check if the textures still need
		        		// to be created due to deletion,
		        		String currentUntaggedDirectoryName = getUntaggedDirectoryName(currentDirectoryName);
		        		File packFile = new File(outputDirectory, currentUntaggedDirectoryName + PACK_FILE_EXTENSION);
		        		File firstTextureFile = new File(outputDirectory, currentUntaggedDirectoryName + "1.png");
		        		if (packFile.exists() && firstTextureFile.exists())
		        			directoryIntact[imageIndex] = true;
		        	}	        	
	        	}
	        	
	        	bufferedReader.close();
        	} catch (Exception e) {}
        	
        } else {
        
        	// Delete everything in the output folder,
        	File[] outputDirectoryChildren = outputDirectory.listFiles();
        	for (int i=0; i<outputDirectoryChildren.length; i++) {
        		try{ outputDirectoryChildren[i].delete(); } catch (Exception e) {}
        	}
        }

        return directoryIntact;
    }
    
    static String getUntaggedDirectoryName(String directoryName) {

		int separatorIndex = directoryName.indexOf(PACKING_ARGUMENTS_SEPARATOR);
		if (separatorIndex != -1) {
			return directoryName.substring(0, separatorIndex);
		}

		return directoryName;
    }
    
    static void fixPackFile(File packFile, String parentDirectoryName) {

    	StringBuilder newFileContents = new StringBuilder();
    	try { 
    		BufferedReader bufferedReader = new BufferedReader(new FileReader(packFile));
	    	while (bufferedReader.ready()) {
	    		String currentLine = bufferedReader.readLine();
	    		String currentLineTrimmed = currentLine.trim();
	    		String newLine;
	    		if (currentLineTrimmed.endsWith(".png") || currentLineTrimmed.endsWith(".jpg")) {
	    			if (currentLineTrimmed.contains(PACKING_ARGUMENTS_SEPARATOR)) {
	    				newLine = getUntaggedDirectoryName(currentLineTrimmed) + currentLineTrimmed.substring(currentLineTrimmed.lastIndexOf(PACKING_ARGUMENTS_SEPARATOR) + 1);
	    			} else {
	    				newLine = currentLineTrimmed;
	    			}
	    			
	    			File imageFile = new File(packFile.getParentFile(), newLine);
	            	Dimension imageDimensions = getImageDimensions(imageFile);

	    			newLine += "\n";
	    			newLine += "dimensions: " + imageDimensions.width + ", " + imageDimensions.height + "\n";
	    		} else if ((currentLineTrimmed.length() > 0) && !currentLineTrimmed.contains(":")) {
	    			newLine = parentDirectoryName + PARENT_NAME_CHILD_NAME_SEPARATOR + currentLineTrimmed + "\n";
	    		} else {
	    			newLine = currentLine + "\n";
	    		}

    			newFileContents.append(newLine);
	    	}
	    	bufferedReader.close();
	    	
	    	BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(packFile));
	    	bufferedWriter.append(newFileContents);
	    	bufferedWriter.close();
	    	
    	} catch (Exception e) { e.printStackTrace(); }
    }

    
    
    
    static class NONGLTextureRegion {
    	public String name;
    	public int x, y;
    	public int width, height;
    	public int originalWidth, originalHeight;
    	public int offsetX, offsetY;
    	public boolean hasAlpha;
    	
    	public NONGLTextureRegion() {}
    	
    	public NONGLTextureRegion(NONGLTextureRegion regionToClone) {
    		name = regionToClone.name;
    		x = regionToClone.x;
    		y = regionToClone.y;
    		width = regionToClone.width;
    		height = regionToClone.height;
    		originalWidth = regionToClone.originalWidth;
    		originalHeight = regionToClone.originalHeight;
    		offsetX = regionToClone.offsetX;
    		offsetY = regionToClone.offsetY;
    		hasAlpha = regionToClone.hasAlpha; 
    	}
    	
    	public void saveTo(StringBuilder stringBuilder) {

    		stringBuilder.append(name).append("\n");
    		stringBuilder.append("  xy: ").append(x).append(", ").append(y).append("\n");
    		stringBuilder.append("  size: ").append(width).append(", ").append(height).append("\n");
    		stringBuilder.append("  orig: ").append(originalWidth).append(", ").append(originalHeight).append("\n");
    		stringBuilder.append("  offset: ").append(offsetX).append(", ").append(offsetY).append("\n");
    		stringBuilder.append("  hasAlpha: ").append(hasAlpha ? 1 : 0).append("\n");
    	}
    	
    	public void detectAlpha(ArrayImage texture) {

    		for (int j=y; j<y+height; j++) {
    			for (int i=x; i<x+width; i++) {
    				try{
	    				if ((texture.getPixel(i, j) & 0xff000000) != 0xff000000) {
	    					hasAlpha = true;
	    					return;
	    				}
    				} catch (Exception e) {
    					System.out.println("Name: " + name + ", x: " + x + ", y: " + y + ", width: " + width + ", height: " + height + ", textureWidth: " + texture.width + ", textureHeight: " + texture.height);
    				}
    			}
    		}
    		
    		hasAlpha = false;
    	}
    }
    
    static class NONGLTexture {
    	public String fileName;
    	public int width, height;
    	public String filter;

    	public ArrayList<NONGLTextureRegion> textureRegions = new ArrayList<NONGLTextureRegion>();
    	
    	public void saveTo(StringBuilder stringBuilder) {

    		stringBuilder.append(fileName).append("\n");
    		stringBuilder.append("dimensions: ").append(width).append(", ").append(height).append("\n");
    		stringBuilder.append("filter: ").append(filter).append("\n");

    		Iterator<NONGLTextureRegion> iterator = textureRegions.iterator(); 
    		while(iterator.hasNext()) {
    			iterator.next().saveTo(stringBuilder);
    		}
    		
    		stringBuilder.append("\n");
    	}
    }
    
    static class NONGLTexturePack {
    	public ArrayList<NONGLTexture> textures = new ArrayList<NONGLTexture>();
    	
    	NONGLTexturePack() {};
    	
    	NONGLTexturePack(File packFile) {

    		NONGLTexture currentTexture=null;
    		NONGLTextureRegion currentRegion=null;
    		boolean foundHasAlphaField = false;
    		ArrayImage currentTextureImage=null;

        	try { 
        		BufferedReader bufferedReader = new BufferedReader(new FileReader(packFile));
        		
    	    	while (bufferedReader.ready()) {

    	    		String currentLine = bufferedReader.readLine();
    	    		String currentLineTrimmed = currentLine.trim();
    	    		String currentLineTrimmedNoSpace = currentLineTrimmed.replaceAll("\\s","");
    	    		
    	    		if (currentLine.isEmpty()) {}
    	    		else if (currentLineTrimmed.endsWith(".png") || currentLineTrimmed.endsWith(".jpg")) {

    	    			// Set "hasAlpha" field of the previous region,
    	    			if ((currentRegion != null) && (!foundHasAlphaField)) {
    	    				if (currentTextureImage == null) {
    	    					currentTextureImage = new ArrayImage(
    	    							new File(packFile.getParent(), currentTexture.fileName).getAbsolutePath(),
    	    							null);
    	    				}
    	    				currentRegion.detectAlpha(currentTextureImage);
        	    			foundHasAlphaField = true;	// So when the next region is found nothing should be done with regard to alpha.
    	    			}    	    			
    	    			
    	    			// Set new texture,
    	    			currentTexture = new NONGLTexture();
    	    			currentTexture.fileName = currentLineTrimmed;
    	    			textures.add(currentTexture);
    	    			currentTextureImage = null;
    	    		} else if (!currentLineTrimmed.contains(":")) {
    	    			
    	    			// Set "hasAlpha" field of the previous region,
    	    			if ((currentRegion != null) && (!foundHasAlphaField)) {
    	    				if (currentTextureImage == null) {
    	    					currentTextureImage = new ArrayImage(
    	    							new File(packFile.getParent(), currentTexture.fileName).getAbsolutePath(),
    	    							null);
    	    				}
    	    				currentRegion.detectAlpha(currentTextureImage);
    	    			}
    	    			foundHasAlphaField = false;

    	    			// Add new region,
    	    			currentRegion = new NONGLTextureRegion();
    	    			currentRegion.name = currentLineTrimmed;
    	    			currentTexture.textureRegions.add(currentRegion);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("filter:")) {
    	    			currentTexture.filter = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    		} else if (currentLineTrimmedNoSpace.startsWith("dimensions:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			String[] dimensions = taglessPart.split(","); 
    	    			currentTexture.width = Integer.parseInt(dimensions[0]);
    	    			currentTexture.height = Integer.parseInt(dimensions[1]);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("xy:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			String[] xy = taglessPart.split(","); 
    	    			currentRegion.x = Integer.parseInt(xy[0]);
    	    			currentRegion.y = Integer.parseInt(xy[1]);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("size:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			String[] dimensions = taglessPart.split(","); 
    	    			currentRegion.width = Integer.parseInt(dimensions[0]);
    	    			currentRegion.height = Integer.parseInt(dimensions[1]);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("orig:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			String[] dimensions = taglessPart.split(","); 
    	    			currentRegion.originalWidth = Integer.parseInt(dimensions[0]);
    	    			currentRegion.originalHeight = Integer.parseInt(dimensions[1]);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("offset:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			String[] offset = taglessPart.split(","); 
    	    			currentRegion.offsetX = Integer.parseInt(offset[0]);
    	    			currentRegion.offsetY = Integer.parseInt(offset[1]);
    	    		} else if (currentLineTrimmedNoSpace.startsWith("hasAlpha:")) {
    	    			String taglessPart = currentLineTrimmedNoSpace.split(":")[1].trim();
    	    			currentRegion.hasAlpha = Integer.parseInt(taglessPart) > 0;
    	    			foundHasAlphaField = true;
    	    		} 
    	    	}
    	    	bufferedReader.close();
    	    	
        	} catch (Exception e) { e.printStackTrace(); }    

			// Set "hasAlpha" field of the last region,
			if (!foundHasAlphaField) {
				try {
					if (currentTextureImage == null) {
						currentTextureImage = new ArrayImage(
								new File(packFile.getParent(), currentTexture.fileName).getAbsolutePath(),
								null);
					}
					currentRegion.detectAlpha(currentTextureImage);
				} catch (Exception e) {}
			}
    	}
    	
    	public NONGLTexture findTexture(String textureFileName) {
    		
    		for (int i=0; i<textures.size(); i++) {
    			if (textures.get(i).fileName.equals(textureFileName)) {
    				return textures.get(i);
    			}
    		}
    		
    		return null;
    	}
    	
    	static NONGLTexturePack combinePacks(NONGLTexturePack packedTexturesPack, NONGLTexturePack packedPacksPack) {

    		NONGLTexturePack combinedPack = new NONGLTexturePack();
    	
    		// For every texture in packedPacks, add texture,
    		for (int i=0; i<packedPacksPack.textures.size(); i++) {
    			
    			NONGLTexture currentTexture = packedPacksPack.textures.get(i);
    			NONGLTexture newTexture = new NONGLTexture();
    			
    			newTexture.fileName = currentTexture.fileName;
    			newTexture.width = currentTexture.width;
    			newTexture.height = currentTexture.height;
    			newTexture.filter = currentTexture.filter;

    			combinedPack.textures.add(newTexture);
    			
    			// For every sub-texture within this texture, add its regions with modified locations,
    			for (int j=0; j<currentTexture.textureRegions.size(); j++) {
    				
    				NONGLTextureRegion currentSubTextureRegion = currentTexture.textureRegions.get(j);
    				String currentSubTextureName = currentSubTextureRegion.name.substring(currentSubTextureRegion.name.indexOf(".") + 1) + ".png";
    				NONGLTexture currentSubTexture = packedTexturesPack.findTexture(currentSubTextureName);

    				for (int k=0; k<currentSubTexture.textureRegions.size(); k++) {    					
    					NONGLTextureRegion currentTextureRegion = currentSubTexture.textureRegions.get(k);
    					NONGLTextureRegion newTextureRegion = new NONGLTextureRegion(currentTextureRegion);
    					newTextureRegion.x += currentSubTextureRegion.x - currentSubTextureRegion.offsetX;
    					newTextureRegion.y += currentSubTextureRegion.y - (currentSubTextureRegion.originalHeight - currentSubTextureRegion.height);    					
    					newTexture.textureRegions.add(newTextureRegion);					
    				}
    			}
    		}

    		return combinedPack;
    	}

    	public void saveTo(File packFile) {
    		
        	StringBuilder newFileContents = new StringBuilder();
        	saveTo(newFileContents);
        	
        	try { 
    	    	BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(packFile));
    	    	bufferedWriter.append(newFileContents);
    	    	bufferedWriter.close();
        	} catch (Exception e) {}
    	}
    	
    	public void saveTo(StringBuilder stringBuilder) {
   
    		Iterator<NONGLTexture> iterator = textures.iterator(); 
    		while(iterator.hasNext()) {
    			iterator.next().saveTo(stringBuilder);
    		}
    	}
    }
	
    
    static final float AREA_TOLERANCE_MULTIPLIER_FOR_WIDTH = 1.1f;
    static final float AREA_TOLERANCE_MULTIPLIER_FOR_HEIGHT = 1.1f; 
    static void setRecommendedDimensions(File imagesDirectory, Settings packingSettings)  {
    	
        // Get the image files,
        File[] imageFiles = imagesDirectory.listFiles(new FileFilter() {
			@Override public boolean accept(File pathname) {
				String name = pathname.getName(); 
				return name.endsWith(".png") || name.endsWith(".jpg");
			}
		});

        // Get images' dimensions and areas,
        int totalArea=0;
        int maxWidth=0;
		int maxHeight=0;
        for (int i=0; i<imageFiles.length; i++) {

    		/*
        	BufferedImage image = ImageIO.read(imageFiles[i]);
        	int width = image.getWidth();
        	int height = image.getHeight();
        	*/

        	Dimension currentImageDimensions = getImageDimensions(imageFiles[i]);
        	if (currentImageDimensions != null) {

	        	if (currentImageDimensions.width > maxWidth)
	        		maxWidth = currentImageDimensions.width;
	        	
	        	if (currentImageDimensions.height > maxHeight)
	        		maxHeight = currentImageDimensions.height;
	        	
	        	totalArea += currentImageDimensions.width * currentImageDimensions.height;
        	}
        }
        
        int idealWidth = (int) Math.sqrt(totalArea * AREA_TOLERANCE_MULTIPLIER_FOR_WIDTH);
        int idealHeight = (int) Math.sqrt(totalArea * AREA_TOLERANCE_MULTIPLIER_FOR_HEIGHT);
        
        //packingSettings.minWidth = nextPot( (idealWidth > maxWidth) ? idealWidth : maxWidth);        
        packingSettings.minHeight = nextPot( (idealHeight > maxHeight) ? idealHeight : maxHeight);
    }
    
    static Dimension getImageDimensions(File imageFile) {

    	Dimension dimensions=null;
    	
    	ImageInputStream inputStream=null;
    	try {
    		
    		int width=0;
    		int height=0;
    		
    		inputStream = ImageIO.createImageInputStream(imageFile);
    		
		    final Iterator<ImageReader> readers = ImageIO.getImageReaders(inputStream);
		    if (readers.hasNext()) {
		        ImageReader imageReader = readers.next();
		        try {
		            imageReader.setInput(inputStream);
		            width = imageReader.getWidth(0);
		            height = imageReader.getHeight(0);
		        } finally {
		            imageReader.dispose();
		        }
		    }

		    dimensions = new Dimension(width, height);
        	
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
		    if (inputStream != null) {
				try { inputStream.close(); } catch (IOException e) {}
		    }
		}

    	return dimensions;
    }
    
    static int nextPot(int value) {
    	
    	int currentPot = 1;
    	while (currentPot<value) {
    		currentPot <<= 1;
    	}
    	
    	return currentPot;
    }
    
    
    
    
    
    static void packTrees(Settings settings, String treesDirectory) {
    	
    	treesDirectory = treesDirectory + "\\";
    	String outPutDirectory = treesDirectory + "packed";
    	
    	String bigTreePrefix = "big_tree_0";
    	String smallTreePrefix = "small_tree_0";
    	
    	
    	for (int i=1; i<=8; i++) {
    		
    		TexturePacker.process(
    				settings, 
    				treesDirectory + bigTreePrefix + Integer.toString(i),
    				outPutDirectory, bigTreePrefix + Integer.toString(i));
    		deBlackize(
    				outPutDirectory + "\\" + bigTreePrefix + Integer.toString(i) + "1.png",		
    				outPutDirectory + "\\" + bigTreePrefix + Integer.toString(i) + "1.png");
    		deBlackize(
    				outPutDirectory + "\\" + bigTreePrefix + Integer.toString(i) + "2.png",		
    				outPutDirectory + "\\" + bigTreePrefix + Integer.toString(i) + "2.png");


    		TexturePacker.process(
    				settings, 
    				treesDirectory + smallTreePrefix + Integer.toString(i),
    				outPutDirectory, smallTreePrefix + Integer.toString(i));
    		deBlackize(
    				outPutDirectory + "\\" + smallTreePrefix + Integer.toString(i) + "1.png",		
    				outPutDirectory + "\\" + smallTreePrefix + Integer.toString(i) + "1.png");
    		deBlackize(
    				outPutDirectory + "\\" + smallTreePrefix + Integer.toString(i) + "2.png",		
    				outPutDirectory + "\\" + smallTreePrefix + Integer.toString(i) + "2.png");

    		System.out.println(Integer.toString(i) + " done!");
    	}
    }
    
    static void deBlackize(String inputImageFile, String outputImageFile) {
    	try {
	    	File inputFile = new File(inputImageFile);
	    	BufferedImage image = ImageIO.read(inputFile);
	    	
	    	for (int j=0; j<image.getHeight(); j++) {
		    	for (int i=0; i<image.getWidth(); i++) {
		    		if (image.getRGB(i, j) == 0)
		    			image.setRGB(i, j, 16777215);
		    	}
	    	}
	    	
	    	File outputFile = new File(outputImageFile);
	    	ImageIO.write(image, "png", outputFile);
    	} catch (Exception e) {
    	}
    }
    
	private static void startFontDataFileGenerator() {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
	    //config.useGL20 = true; 
	    config.vSyncEnabled = true;
	    config.width = 320;
	    config.height = 480; 
	    config.title = "FontDataFileGenerator";
	    new LwjglApplication(new FontDataFileGenerator(), config);
	}
	
	private static long getDirectoryLastModified(File directory) {
		
		long lastModified = directory.lastModified(); 
				
		File[] files = directory.listFiles();
		for (int i=0; i<files.length; i++) {
			long currentFileLastModified = files[i].lastModified();
			if (currentFileLastModified > lastModified) lastModified = currentFileLastModified;
		}
		
		return lastModified;
	}
}
