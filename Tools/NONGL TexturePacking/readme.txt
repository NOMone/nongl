To build the app:
=================
- Download eclipse downloader: http://download.eclipse.org/
- Install the Java version.
- Select a folder to be the workspace.
- Import this project into the workspace.
- Right click the project in the package explorer and select "Export".
- Select "Java=>Runnable Jar file".
- Select "Extract required libraries into generated JAR".
- Set the other options and enjoy!

To run from command line:
=========================
- Place the JAR file next to the "InputImages" folder.
- Run using:
   java -jar "PackTextures 2048x2048.jar"
   
if that's what you called your jar file.
