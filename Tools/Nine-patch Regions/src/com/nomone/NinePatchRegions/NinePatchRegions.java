package com.nomone.NinePatchRegions;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;

public class NinePatchRegions {

	public static void main(String[] args) {
		
		System.out.println("besm Allah :)");
		System.out.println();
		
        URL root = NinePatchRegions.class.getProtectionDomain().getCodeSource().getLocation();
        String path=null;
        try { path = (new File(root.toURI())).getParentFile().getPath(); } catch (Exception e) {}

		File inputDirectory = new File(path, "InputImages");
		File[] inputImageFiles = inputDirectory.listFiles(new FilenameFilter() {
			@Override public boolean accept(File file, String filename) {
				return filename.endsWith(".9.png");
			}
 		});
		
		for (int i=0; i<inputImageFiles.length; i++) {
			printNinePatchRegionsAndRemoveBorder(inputImageFiles[i]);
		}
 	}
	
	static int count = 1;
	public static void printNinePatchRegionsAndRemoveBorder(File imageFile) {

		System.out.println("Processing: " + imageFile.getName());
		
		// Load the image,
		ArrayImage image = null;
		try { 
			image = new ArrayImage(imageFile.getAbsolutePath(), null);
		} catch (Exception e) { 
			System.out.println("Failed to load image!"); 
			e.printStackTrace(); 
		}

		// Extract patches data,
		// Column widths,
		ArrayList<Integer> regionWidths = new ArrayList<Integer>();
		boolean firstColumnHorizontallyStretchable = image.getPixel(1, 0) != 0;
		boolean lastPixelSet = firstColumnHorizontallyStretchable;
		int lastColumnX = 1;
		for (int i=2; i<image.width-1; i++) {
			boolean currentPixelSet = image.getPixel(i, 0) != 0;
			if (currentPixelSet != lastPixelSet) {
				//regionWidths.add((i - lastColumnX) / (image.width - 2.0f));
				regionWidths.add(i - lastColumnX);
				lastColumnX = i;
			}
			lastPixelSet = currentPixelSet;
		}
		
		// Add last column,
		//regionWidths.add(((image.width-1) - lastColumnX) / (image.width - 2.0f));
		regionWidths.add((image.width-1) - lastColumnX);
		
		System.out.println("First column horizontally stretchable: " + firstColumnHorizontallyStretchable);
		System.out.print("Widths: ");
		for (int i=0; i<regionWidths.size()-1; i++) {
			System.out.print(regionWidths.get(i) + ", ");
		}
		System.out.println(regionWidths.get(regionWidths.size()-1));
		
		// Row heights,
		ArrayList<Integer> regionHeights = new ArrayList<Integer>();
		boolean firstRowVerticallyStretchable = image.getPixel(0, 1) != 0;
		lastPixelSet = firstRowVerticallyStretchable;
		int lastRowY = 1;
		for (int i=2; i<image.height-1; i++) {
			boolean currentPixelSet = image.getPixel(0, i) != 0;
			if (currentPixelSet != lastPixelSet) {
				//regionHeights.add((i - lastRowY) / (image.height - 2.0f));
				regionHeights.add(i - lastRowY);
				lastRowY = i;
			}
			lastPixelSet = currentPixelSet;
		}
		
		// Add last row,
		//regionHeights.add(((image.height-1) - lastRowY) / (image.height - 2.0f));
		regionHeights.add((image.height-1) - lastRowY);
		
		System.out.println("First row vertically stretchable: " + firstRowVerticallyStretchable);
		System.out.print("Heights: ");
		for (int i=0; i<regionHeights.size()-1; i++) {
			System.out.print(regionHeights.get(i) + ", ");
		}
		System.out.println(regionHeights.get(regionHeights.size()-1));
		System.out.println();
		
		// Create a new image without the 9-patch border,
		ArrayImage newImage = new ArrayImage(image.width-2, image.height-2, null);
		newImage.drawImageFine(image, 
				1, 1, image.width-1, image.height-1, 
				0, 0, image.width-2, image.height-2, false);
		
		String imageName = imageFile.getName();
		String imageNameWithoutExtension = imageName.substring(0, imageName.lastIndexOf(".9.png"));
		String newImageFilePath = (new File(imageFile.getParentFile(), imageNameWithoutExtension + ".png")).getAbsolutePath(); 
		newImage.saveImage(newImageFilePath);
	}
		
}
