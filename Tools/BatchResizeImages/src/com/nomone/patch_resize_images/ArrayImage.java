package com.nomone.patch_resize_images;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author raslanove
 */
public class ArrayImage {

    public int width, height;
    int[] data;
    MemoryImageSource imageSource;
    Image image;
    BufferedImage bufferedImage;
    Graphics2D graphics;
        
    JLabel outputLabel;

    public ArrayImage(int width, int height, JLabel outputLabel) {

        this.outputLabel = outputLabel;
        setTo(new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB));
    }

    public ArrayImage(String imageFilePath, JLabel outputLabel) throws IOException {

        this.outputLabel = outputLabel;
        BufferedImage loadedImage = ImageIO.read(new File(imageFilePath));
        BufferedImage finalImage = loadedImage;
        
        DataBuffer dataBuffer = loadedImage.getRaster().getDataBuffer();
        if (!(dataBuffer instanceof DataBufferInt)) {
        	
        	// Since we only deal with ARGB images in this library, we have to
        	// convert the loaded image,
        	width  = loadedImage.getWidth();
        	height = loadedImage.getHeight();
        	
        	finalImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            data = ((DataBufferInt) finalImage.getRaster().getDataBuffer()).getData();

            loadedImage.getRGB(0, 0, width, height, data, 0, width);
    	}
        		
        setTo(finalImage);
    }

    public void setPixel(int x, int y, int color) {
        /*
        if ((x >= width) || (x<0) || (y>=height) || (y>0)) {
            return;
        }
        */
        data[x + y * width] = color;
    }

    public int getPixel(int x, int y) {
        /*
        if ((x >= width) || (x<0) || (y>=height) || (y>0)) {
            return 0;
        }
        */
        return data[x + y * width];
    }

    public void flush() {
        imageSource.newPixels();
        outputLabel.paintImmediately(
                0, 0, 
                outputLabel.getWidth(), outputLabel.getHeight());
    }

    public void clear(int clearColor) {

        for (int i=0; i<data.length; i++) {
            data[i] = clearColor;
        }
    }

    public void setTo(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
        width  = bufferedImage.getWidth ();
        height = bufferedImage.getHeight();
        data = ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData();
        graphics = bufferedImage.createGraphics(); //bufferedImage.getGraphics();
        
        graphics.setRenderingHint(
            RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        if (outputLabel != null) {
            imageSource = new MemoryImageSource(width, height, data, 0, width);
            imageSource.setAnimated(true);
            image = outputLabel.createImage(imageSource);

            outputLabel.setIcon(new ImageIcon(image));
        }
    }

    public void copyFrom(BufferedImage bufferedImage) {
        
        if ((bufferedImage.getWidth() != width) ||
            (bufferedImage.getHeight() != height)) {
            throw new RuntimeException("Can't copy from buffered image with different dimensions.");
        } else {
            /*
            System.arraycopy(
                    ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData(), 
                    0, data, 0, data.length);
            */
            System.arraycopy(
                    ((DataBufferInt) bufferedImage.getData().getDataBuffer()).getData(), 
                    0, data, 0, data.length);
        }
    }
    
    public void copyFrom(ArrayImage srcImage) {
        System.arraycopy(srcImage.data, 0, data, 0, data.length);
    }
    
    /** @return: the number of different pixels. */
    public int compareTo(ArrayImage srcImage) {
        
        if ((width != srcImage.width) ||
            (height != srcImage.height)) {
            
            return -1;
        }

        int differentPixelsCount = 0;
        int[] srcImageData = srcImage.data;
        for (int i=0; i<data.length; i++) {
            if (data[i] != srcImageData[i]) {
                differentPixelsCount++;
            }
        }
        
        return differentPixelsCount;
    }
    
    public void shiftRight(int shiftValue) {
        for (int i=0; i<data.length; i++) {
            data[i] >>= shiftValue;
        }
    }
    
    public void setAlpha(int alpha) {
        alpha <<= 24;
        for (int i=0; i<data.length; i++) {
            data[i] |= alpha;
        }
    }

    public void drawLine(Point point1, Point point2, int color) {
        
        float xDifference = point2.x - point1.x;
        float yDifference = point2.y - point1.y;
        float absoluteXDifference = xDifference > 0 ? xDifference : -xDifference;
        float absoluteYDifference = yDifference > 0 ? yDifference : -yDifference;
        
        int pixelsCount = (absoluteXDifference > absoluteYDifference) ? (int) (absoluteXDifference + 0.999f) : (int) (absoluteYDifference + 0.999f);
        float xStep = xDifference / pixelsCount;
        float yStep = yDifference / pixelsCount;
        
        float x=point1.x;
        float y=point1.y;
        for (; pixelsCount>0; pixelsCount--) {
            drawPixel((int) x, (int) y, color);
            x += xStep;
            y += yStep;
        }
    }
    
    public void drawLine(Point point1, Point point2, int value1, int value2) {
        
        float xDifference = point2.x - point1.x;
        float yDifference = point2.y - point1.y;
        float absoluteXDifference = xDifference > 0 ? xDifference : -xDifference;
        float absoluteYDifference = yDifference > 0 ? yDifference : -yDifference;
        
        int pixelsCount = (absoluteXDifference > absoluteYDifference) ? (int) (absoluteXDifference + 0.999f) : (int) (absoluteYDifference + 0.999f);
        float xStep = xDifference / pixelsCount;
        float yStep = yDifference / pixelsCount;
        
        float currentValue = value1;
        float valueStep = (value2 - value1) / (float) pixelsCount;
        
        float x=point1.x;
        float y=point1.y;
        for (; pixelsCount>0; pixelsCount--) {
            setPixel((int) x, (int) y, (int) currentValue);
            x += xStep;
            y += yStep;
            currentValue += valueStep;
        }
    }
    
    public void drawLine(float x1, float y1, float x2, float y2, int color) {
        
        float xDifference = x2 - x1;
        float yDifference = y2 - y1;
        float absoluteXDifference = xDifference > 0 ? xDifference : -xDifference;
        float absoluteYDifference = yDifference > 0 ? yDifference : -yDifference;
        
        int pixelsCount = (absoluteXDifference > absoluteYDifference) ? (int) (absoluteXDifference + 0.999f) : (int) (absoluteYDifference + 0.999f);
        float xStep = xDifference / pixelsCount;
        float yStep = yDifference / pixelsCount;
        
        for (; pixelsCount>0; pixelsCount--) {
            setPixel((int) x1, (int) y1, color);
            x1 += xStep;
            y1 += yStep;
        }
    }
    
    public void drawTriangleWireframe(Triangle triangle, int color) {
        drawLine(triangle.points[0], triangle.points[1], color);
        drawLine(triangle.points[1], triangle.points[2], color);
        drawLine(triangle.points[2], triangle.points[0], color);
    }
    
    public void filter() {
        
        for (int j=1; j<height-1; j++) {
            for (int i=1; i<width-1; i++) {
                
                int xs = 0;
                int ys = 0;
                for (int l=j-1; l<j+2; l++) {
                    for (int k=i-1; k<i+2; k++) {
                        xs += getPixel(k, l);
                        ys += getPixel(k, l);
                    }
                }
                
                xs /= 9;
                ys /= 9;
                
                setPixel(i, j, xs);
                setPixel(i, j, ys);
            }
        }
    }
    
    public void drawPixel(int x, int y, int color) {

        if ((x<0) || (x>=width) || (y<0) || (y>=height)) {
            return ;
        }
        int backColor = getPixel(x, y);
        
        int backBlue = backColor & 255;
        int backGreen = (backColor >>> 8) & 255;
        int backRed = (backColor >>> 16) & 255;
        int backAlpha = (backColor >>> 24);
        
        int frontBlue = color & 255;
        int frontGreen = (color >>> 8) & 255;
        int frontRed = (color >>> 16) & 255;
        int frontAlpha = (color >>> 24);
        
        float ratio = frontAlpha / 255f;
        
        int newBlue = backBlue + (int) ((frontBlue - backBlue) * ratio);
        int newGreen = backGreen + (int) ((frontGreen - backGreen) * ratio);
        int newRed = backRed + (int) ((frontRed - backRed) * ratio);
        int newAlpha = backAlpha + (int) ((frontAlpha - backAlpha) * ratio);

        int newColor = newBlue | (newGreen << 8) | (newRed << 16) | (newAlpha << 24);
        setPixel(x, y, newColor);
    }
    
    public int rgba(int red, int green, int blue, int alpha) {
        return blue | (green << 8) | (red << 16) | (alpha << 24);
    }

    static public enum Anchor {
        TOP_LEFT, TOP_CENTER, TOP_RIGHT, 
        LEFT_CENTER, CENTER, RIGHT_CENTER, 
        BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
    };
    
    public void setFont(Font font) {
        graphics.setFont(font);
    }
    
    public void setFontSize(int size) {
        Font currentFont = graphics.getFont();
        graphics.setFont(new Font(currentFont.getFamily(), currentFont.getStyle(), size));
    }

    public static enum FontStyle {
        PLAIN, BOLD, ITALIC, BOLD_ITALIC
    };
    
    public void setFontStyle(FontStyle fontStyle) {
        
        Font currentFont = graphics.getFont();
        int style;
        switch(fontStyle) {
            case BOLD:
                style = Font.BOLD;
                break;
                
            case ITALIC:
                style = Font.ITALIC;
                break;

            case BOLD_ITALIC:
                style = Font.BOLD + Font.ITALIC;
                break;

            case PLAIN:
            default:
                style = Font.PLAIN;
                break;
        }
        graphics.setFont(new Font(currentFont.getFamily(), style, currentFont.getSize()));
    }

    public void setFontColor(int color) {
        graphics.setColor(new Color(color, true));
    }

    public void drawText(String string, Anchor anchor, int xOffset, int yOffset) {
       
        FontMetrics fontMetrics = graphics.getFontMetrics();
        
        switch(anchor) {
                
            case TOP_LEFT:
                yOffset += fontMetrics.getHeight();
                break;
                
            case TOP_CENTER:
                xOffset += (width - fontMetrics.stringWidth(string)) / 2;
                yOffset += fontMetrics.getHeight(); 
                break;
                
            case TOP_RIGHT:
                xOffset += width - fontMetrics.stringWidth(string);
                yOffset += fontMetrics.getHeight();
                break;
                
            case LEFT_CENTER:
                yOffset += (height - fontMetrics.getHeight()) / 2;
                break;
                
            case CENTER:
                xOffset += (width - fontMetrics.stringWidth(string)) / 2;
                yOffset += (height - fontMetrics.getHeight()) / 2;
                break;
                
            case RIGHT_CENTER:
                xOffset += width - fontMetrics.stringWidth(string);
                yOffset += (height - fontMetrics.getHeight()) / 2;
                break;

            case BOTTOM_LEFT:
                yOffset += height;
                break;

            case BOTTOM_CENTER: 
                xOffset += (width - fontMetrics.stringWidth(string)) / 2;
                yOffset += height;
                break;
                
            case BOTTOM_RIGHT:
                xOffset += width - fontMetrics.stringWidth(string);
                yOffset += height;
                break;

            default:
                break;
        }
        
        graphics.drawString(string, xOffset, yOffset);
    }
    
    public void drawImage(ArrayImage image, int alpha) {
        
        float ratio = alpha / 255f;
        for (int j=0; j<image.height; j++) {
            for (int i=0; i<image.width; i++) {
         
                int backColor = getPixel(i, j);
                int frontColor = image.getPixel(i, j);
                int backBlue = backColor & 255;
                int backGreen = (backColor >>> 8) & 255;
                int backRed = (backColor >>> 16) & 255;
                int frontBlue = frontColor & 255;
                int frontGreen = (frontColor >>> 8) & 255;
                int frontRed = (frontColor >>> 16) & 255;
                
                int newBlue = backBlue + (int) ((frontBlue - backBlue) * ratio);
                int newGreen = backGreen + (int) ((frontGreen - backGreen) * ratio);
                int newRed = backRed + (int) ((frontRed - backRed) * ratio);
                
                int newColor = newBlue | (newGreen << 8) | (newRed << 16) | 0xff000000;
                setPixel(i, j, newColor);
            }
        }
    }
    
    public double gammaCorrect(double value) {
    	return Math.pow(value, 1.0/2.2);
    }
    
    public double gammaUncorrect(double value) {
    	return Math.pow(value, 2.2);
    }
    
    public void drawImageFine(
            ArrayImage srcImage, 
            double srcLeft, double srcTop, double srcRight, double srcBottom,
            int destLeft, int destTop, int destRight, int destBottom, 
            boolean blend, boolean gammaCorrection) {
        
        int destWidth  = (destRight - destLeft);
        int destHeight = (destBottom - destTop);
        
        double xStep = (srcRight - srcLeft) / destWidth;
        double yStep = (srcBottom - srcTop) / destHeight;

        double sampleArea = xStep * yStep;
        
        for (int y=0; y<destHeight; y++) {
            
            double sampleTop = srcTop + (y * yStep);
            double sampleBottom = srcTop + ((y+1) * yStep);
            if (sampleBottom > srcBottom) {
                sampleBottom = srcBottom;
            }
            
            for (int x=0; x<destWidth; x++) {
        
                double sampleLeft = srcLeft + (x * xStep);
                double sampleRight = srcLeft + ((x+1) * xStep);
                if (sampleRight > srcRight) {
                    sampleRight = srcRight;
                }
        
                double red = 0;
                double green = 0;
                double blue = 0;
                double alpha = 0;
                double currentSampleArea = 0;
                        
                // Sample the source image,
                double sampleY = sampleTop;
                do {

                    double nextSampleY = (int) (sampleY + 1);
                    if (nextSampleY > sampleBottom) {
                        nextSampleY = sampleBottom;
                    }

                    double sampleX = sampleLeft;
                    do {
                        
                        double nextSampleX = (int) (sampleX + 1);
                        if (nextSampleX > sampleRight) {
                            nextSampleX = sampleRight;
                        }
                        
                        double subSampleArea = (nextSampleX - sampleX) * (nextSampleY - sampleY);
                        currentSampleArea += subSampleArea;
                        double areaRatio = subSampleArea / sampleArea;
                        
                        int subSampleColor = srcImage.getPixel((int) sampleX, (int) sampleY);
                        
                        int subSampleBlue = subSampleColor & 255;
                        int subSampleGreen = (subSampleColor >>> 8) & 255;
                        int subSampleRed = (subSampleColor >>> 16) & 255;
                        int subSampleAlpha = subSampleColor >>> 24;

                        if (gammaCorrection) {
	                        red += gammaUncorrect(subSampleRed/255.0) * areaRatio;
	                        green += gammaUncorrect(subSampleGreen/255.0) * areaRatio;
	                        blue += gammaUncorrect(subSampleBlue/255.0) * areaRatio;
                        } else {
	                        red += subSampleRed * areaRatio;
	                        green += subSampleGreen * areaRatio;
	                        blue += subSampleBlue * areaRatio;                        	
                        }
                        alpha += subSampleAlpha * areaRatio;
                        
                        sampleX = nextSampleX;                        
                    } while (sampleX < sampleRight);
                    
                    sampleY = nextSampleY;
                } while (sampleY < sampleBottom);
                
                // Scale the components to make up for floating point losses,
                double scaleFactor = sampleArea / currentSampleArea;
                if (gammaCorrection) {
	                red = gammaCorrect(red * scaleFactor) * 255.0;
	                green = gammaCorrect(green * scaleFactor) * 255.0;
	                blue = gammaCorrect(blue * scaleFactor) * 255.0;
                } else {
                    red *= scaleFactor;
                    green *= scaleFactor;
                    blue *= scaleFactor;
                }
                alpha = alpha * scaleFactor;
                if (red > 255) {
                    red = 255;
                }
                if (green > 255) {
                    green = 255;
                }
                if (blue > 255) {
                    blue = 255;
                }
                if (alpha > 255) {
                    alpha = 255;
                }
                                
                // Draw the resulting color,
                int packedColor = rgba(
                        (int) (red + 0.5), 
                        (int) (green + 0.5), 
                        (int) (blue + 0.5), 
                        (int) (alpha + 0.5));
                if (blend) {
                	drawPixel(destLeft + x, destTop + y, packedColor);
                } else {
                	setPixel(destLeft + x, destTop + y, packedColor);
                }
            }
        }
    }

    public void loadImage(String filePath) {
        loadImage(filePath, 0, 0);
    }

    public void loadImage(String filePath, int x, int y) {
        
        // Do nothing if out of boundaries,
        if ((x >= width) || (y >= height)) {
            return;
        }
        
        BufferedImage loadedImage=null;
        try {
            loadedImage = ImageIO.read(new File(filePath));
        } catch (Exception e) {}
        
        int loadedImageWidth  = loadedImage.getWidth ();
        int loadedImageHeight = loadedImage.getHeight();

        // Do nothing if out of boundaries,
        if (((x + loadedImageWidth ) <= 0) ||
            ((y + loadedImageHeight) <= 0)) {
            return;
        }
        
        // Find the visisble region,
        int clippedWidth = loadedImageWidth;
        int clippedHeight = loadedImageHeight;
        if ((x + loadedImageWidth) > width) {
            clippedWidth -= (x + loadedImageWidth) - width;
        }
        if ((y + loadedImageHeight) > height) {
            clippedHeight -= (y + loadedImageHeight) - height;
        }
        
        int imageX=0;
        int imageY=0;
        if (x < 0) {
            imageX = -x;
        }
        if (y < 0) {
            imageY = -y;
        }
        
        clippedWidth -= imageX;
        clippedHeight -= imageY;
        
        int destinationOffest = (x+imageX) + ((y+imageY)*width);
        loadedImage.getRGB(imageX, imageY, clippedWidth, clippedHeight, data, destinationOffest, width);
    }

    public BufferedImage createBufferedImageRGBA() {
        
        //BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        //bufferedImage.setRGB(0, 0, width, height, data, 0, width);
        
        return bufferedImage;
    }

    public BufferedImage createBufferedImageRGB() {        
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        bufferedImage.setRGB(0, 0, width, height, data, 0, width);
        
        return bufferedImage;
    }

    public void saveImage(String filePath) {       

        try {                    
            String extension = filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase();
            
            BufferedImage bufferedImage;
            if (extension.equals("jpg") || extension.equals("jpeg")) {
                bufferedImage = createBufferedImageRGB();
            } else {
                bufferedImage = createBufferedImageRGBA();
            }
            
            File outputfile = new File(filePath);
            if (!ImageIO.write(bufferedImage, extension, outputfile)) {
                JOptionPane.showMessageDialog(null, "Failed to save image to: " + filePath + "\nError: invalid or unsupported extension '" + extension + "'.", "Error saving picture", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Failed to save image to: " + filePath + "\nError: " + e, "Error saving picture", JOptionPane.ERROR_MESSAGE);
        }        
    }

    private void saveImageToObject(Object output, String format, float quality) throws IOException {

        Iterator writers = ImageIO.getImageWritersByFormatName(format);
        ImageWriter writer = (ImageWriter) writers.next();

        BufferedImage bufferedImage;
        if (format.equals("jpg") || format.equals("jpeg")) {
            bufferedImage = createBufferedImageRGB();
        } else {
            bufferedImage = createBufferedImageRGBA();
        }

        ImageOutputStream ios = ImageIO.createImageOutputStream(output);
        writer.setOutput(ios);
        if (quality != -1) {
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);

            writer.write(null, new IIOImage(bufferedImage, null, null), param);
        } else {
            writer.write(bufferedImage);
        }
        ios.flush(); // otherwise the buffer size will be zero!
        
        writer.dispose();
    }
    
    public void saveImage(String filePath, float quality) {        
        try {
            String extension = filePath.substring(filePath.lastIndexOf(".") + 1);
            File f = new File(filePath);

            saveImageToObject(f, extension, quality);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to save image to: " + filePath + "\nError: " + e, "Error saving picture", JOptionPane.INFORMATION_MESSAGE);
        }    
    }
    
    public void saveImage(OutputStream outputStream, String format, float quality) {
        try {
            saveImageToObject(outputStream, format, quality);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to save "+ format.toUpperCase() + " image.\nError: " + e, "Error saving picture", JOptionPane.INFORMATION_MESSAGE);
        }           
    }
    
    public void saveImage(OutputStream outputStream, String format) {
        saveImage(outputStream, format, -1);
    }
        
    public void drawCrossHairs(int x, int y, int length, int color) {
        
        int centerDisplacement = length >>> 1;
        x -= centerDisplacement;
        y -= centerDisplacement;
        
        for (int i=0; i<length; i++) {
            if (((x+i) > -1) && 
                ((x+i) < width) &&
                ((y+centerDisplacement) > -1) &&
                ((y+centerDisplacement) < height)) {                    
                setPixel(x+i, y+centerDisplacement, color);
            }
            if (((x+centerDisplacement) > -1) && 
                ((x+centerDisplacement) < width) &&
                ((y+i) > -1) &&
                ((y+i) < height)) {                    
                setPixel(x+centerDisplacement, y+i, color);
            }
        }
    }

    Point fourthPoint = new Point();
    Point[] points = new Point[3];
    public void drawTriangle(Triangle triangle, int color) {
        
        // Arrange points,
        points[0] = triangle.points[triangle.offset  ];
        points[1] = triangle.points[triangle.offset+1];
        points[2] = triangle.points[triangle.offset+2];
        
        Point topPoint, middlePoint, bottomPoint;
        if (points[0].y <= points[1].y) {
            
            if (points[0].y <= points[2].y) {
                topPoint = points[0];
                if (points[1].y <= points[2].y) {
                    middlePoint = points[1];
                    bottomPoint = points[2];
                } else {
                    middlePoint = points[2];
                    bottomPoint = points[1];
                }    
            } else {
                topPoint = points[2];
                middlePoint = points[0];
                bottomPoint = points[1];
            }
        } else if (points[1].y <= points[2].y) {
            topPoint = points[1];
            if (points[0].y <= points[2].y) {
                middlePoint = points[0];
                bottomPoint = points[2];
            } else {
                middlePoint = points[2];
                bottomPoint = points[0];
            }    
        } else {
            topPoint = points[2];
            middlePoint = points[1];
            bottomPoint = points[0];
        }
                
        // Find the fourth point,
        fourthPoint.y = middlePoint.y;
        float bisectionRatio = 
                (fourthPoint.y - topPoint.y) / 
                (bottomPoint.y - topPoint.y);
        
        fourthPoint.x = topPoint.x + 
                        ((bottomPoint.x - topPoint.x) * bisectionRatio);
        
        // Start rasterizing,
        float triangleHeight = bottomPoint.y - topPoint.y;
        float halfTriangleHeight = fourthPoint.y - topPoint.y;

        float scanLineStart, scanLineEnd;
        scanLineStart = scanLineEnd = topPoint.x;
        
        float scanLineStartStep, scanLineEndStep;
        if (middlePoint.x >= fourthPoint.x) {
            scanLineStartStep = (bottomPoint.x - topPoint.x) / triangleHeight;
            scanLineEndStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
        } else {
            scanLineStartStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
            scanLineEndStep = (bottomPoint.x - topPoint.x) / triangleHeight;
        }
        
        int currentY = (int) topPoint.y;
        int integerHalfTriangleHeight = (int) fourthPoint.y - (int) topPoint.y;
        for (int scanLinesCount=integerHalfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
        //for (int scanLinesCount=(int) halfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
            int currentX = (int) scanLineStart;
            int currentPixelIndex = currentX + (currentY * width);
            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                data[currentPixelIndex] = color;
                currentPixelIndex ++;
            }
            
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
        }

        
        float remainingTriangleHeight = triangleHeight - halfTriangleHeight;
        if (middlePoint.x > fourthPoint.x) {
            scanLineEndStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineEnd = middlePoint.x;
        } else {
            scanLineStartStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineStart = middlePoint.x;
        }

        int integerRemainingTriangleHeight = (int) triangleHeight - (int) halfTriangleHeight;
        if ((remainingTriangleHeight == 0) && (integerRemainingTriangleHeight == 0)) {
            integerRemainingTriangleHeight--;
        }
        for (int scanLinesCount=integerRemainingTriangleHeight; scanLinesCount>=0; scanLinesCount--) {

            int currentX = (int) scanLineStart;
            int currentPixelIndex = currentX + (currentY * width);
            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                data[currentPixelIndex] = color;
                currentPixelIndex ++;
            }
          
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
        }
    }
    
    Point[] texturePoints = new Point[3];
    Point textureFourthPoint = new Point();
    public void drawTexturedTriangle(Triangle dstTriangle, Triangle srcTriangle, ArrayImage texture) {
        
        // Arrange points,
        points[0] = dstTriangle.points[dstTriangle.offset  ];
        points[1] = dstTriangle.points[dstTriangle.offset+1];
        points[2] = dstTriangle.points[dstTriangle.offset+2];
        texturePoints[0] = srcTriangle.points[srcTriangle.offset  ];
        texturePoints[1] = srcTriangle.points[srcTriangle.offset+1];
        texturePoints[2] = srcTriangle.points[srcTriangle.offset+2];
        
        Point topPoint, middlePoint, bottomPoint;
        Point textureTopPoint, textureMiddlePoint, textureBottomPoint;
        if (points[0].y <= points[1].y) {
            
            if (points[0].y <= points[2].y) {
                topPoint = points[0];
                textureTopPoint = texturePoints[0];
                if (points[1].y <= points[2].y) {
                    middlePoint = points[1];
                    bottomPoint = points[2];
                    textureMiddlePoint = texturePoints[1];
                    textureBottomPoint = texturePoints[2];
                } else {
                    middlePoint = points[2];
                    bottomPoint = points[1];
                    textureMiddlePoint = texturePoints[2];
                    textureBottomPoint = texturePoints[1];
                }    
            } else {
                topPoint = points[2];
                middlePoint = points[0];
                bottomPoint = points[1];
                textureTopPoint    = texturePoints[2];
                textureMiddlePoint = texturePoints[0];
                textureBottomPoint = texturePoints[1];
            }
        } else if (points[1].y <= points[2].y) {
            topPoint = points[1];
            textureTopPoint = texturePoints[1];
            if (points[0].y <= points[2].y) {
                middlePoint = points[0];
                bottomPoint = points[2];
                textureMiddlePoint = texturePoints[0];
                textureBottomPoint = texturePoints[2];
            } else {
                middlePoint = points[2];
                bottomPoint = points[0];
                textureMiddlePoint = texturePoints[2];
                textureBottomPoint = texturePoints[0];
            }    
        } else {
            topPoint = points[2];
            middlePoint = points[1];
            bottomPoint = points[0];
            textureTopPoint    = texturePoints[2];
            textureMiddlePoint = texturePoints[1];
            textureBottomPoint = texturePoints[0];
        }
                
        // Find the fourth point,
        fourthPoint.y = middlePoint.y;
        float bisectionRatio = 
                (fourthPoint.y - topPoint.y) / 
                (bottomPoint.y - topPoint.y);
        fourthPoint.x = topPoint.x + 
                        ((bottomPoint.x - topPoint.x) * bisectionRatio);

        textureFourthPoint.x = textureTopPoint.x + 
                        ((textureBottomPoint.x - textureTopPoint.x) * bisectionRatio);
        textureFourthPoint.y = textureTopPoint.y + 
                        ((textureBottomPoint.y - textureTopPoint.y) * bisectionRatio);
        
        // Start rasterizing,
        float triangleHeight = bottomPoint.y - topPoint.y;
        float halfTriangleHeight = fourthPoint.y - topPoint.y;

        float scanLineStart, scanLineEnd;
        scanLineStart = scanLineEnd = topPoint.x;
        
        float textureScanLineStartX = textureTopPoint.x;
        float textureScanLineStartY = textureTopPoint.y;
        float textureScanLineXStep, textureScanLineYStep;
        
        float scanLineStartStep, scanLineEndStep;
        float textureScanLineStartXStep, textureScanLineStartYStep;
        if (middlePoint.x >= fourthPoint.x) {
            scanLineStartStep = (bottomPoint.x - topPoint.x) / triangleHeight;
            scanLineEndStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
            
            textureScanLineStartXStep = (textureBottomPoint.x - textureTopPoint.x) / triangleHeight;
            textureScanLineStartYStep = (textureBottomPoint.y - textureTopPoint.y) / triangleHeight;
           
            float dstTriangleWidth = middlePoint.x - fourthPoint.x;
            textureScanLineXStep = (textureMiddlePoint.x - textureFourthPoint.x) / dstTriangleWidth;
            textureScanLineYStep = (textureMiddlePoint.y - textureFourthPoint.y) / dstTriangleWidth;
        } else {
            scanLineStartStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
            scanLineEndStep = (bottomPoint.x - topPoint.x) / triangleHeight;

            textureScanLineStartXStep = (textureMiddlePoint.x - textureTopPoint.x) / halfTriangleHeight;
            textureScanLineStartYStep = (textureMiddlePoint.y - textureTopPoint.y) / halfTriangleHeight;
            
            float dstTriangleWidth = fourthPoint.x - middlePoint.x;
            textureScanLineXStep = (textureFourthPoint.x - textureMiddlePoint.x) / dstTriangleWidth;
            textureScanLineYStep = (textureFourthPoint.y - textureMiddlePoint.y) / dstTriangleWidth;
        }
        
        int currentY = (int) topPoint.y;
        int integerHalfTriangleHeight = (int) fourthPoint.y - (int) topPoint.y;
        for (int scanLinesCount=integerHalfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
        //for (int scanLinesCount=(int) halfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
            int currentX = (int) scanLineStart;
            float currentTextureX = textureScanLineStartX;
            float currentTextureY = textureScanLineStartY;
            
            int currentPixelIndex = currentX + (currentY * width);
            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                try {
                    data[currentPixelIndex] = texture.getPixel((int) currentTextureX, (int) currentTextureY);
                } catch (Exception e) {}
                currentPixelIndex ++;
                
                currentTextureX += textureScanLineXStep;
                currentTextureY += textureScanLineYStep;
            }
            
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
            
            textureScanLineStartX += textureScanLineStartXStep;
            textureScanLineStartY += textureScanLineStartYStep;
        }

        
        float remainingTriangleHeight = triangleHeight - halfTriangleHeight;
        if (middlePoint.x > fourthPoint.x) {
            scanLineEndStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineEnd = middlePoint.x;
        } else {
            scanLineStartStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineStart = middlePoint.x;
            
            textureScanLineStartXStep = (textureBottomPoint.x - textureMiddlePoint.x) / remainingTriangleHeight;
            textureScanLineStartYStep = (textureBottomPoint.y - textureMiddlePoint.y) / remainingTriangleHeight;
            textureScanLineStartX = textureMiddlePoint.x;
            textureScanLineStartY = textureMiddlePoint.y;
        }

        int integerRemainingTriangleHeight = (int) triangleHeight - (int) halfTriangleHeight;
        if ((remainingTriangleHeight == 0) && (integerRemainingTriangleHeight == 0)) {
            integerRemainingTriangleHeight--;
        }
        for (int scanLinesCount=integerRemainingTriangleHeight; scanLinesCount>=0; scanLinesCount--) {

            int currentX = (int) scanLineStart;
            float currentTextureX = textureScanLineStartX;
            float currentTextureY = textureScanLineStartY;

            int currentPixelIndex = currentX + (currentY * width);
            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                try {
                    data[currentPixelIndex] = texture.getPixel((int) currentTextureX, (int) currentTextureY);
                } catch (Exception e) {}
                currentPixelIndex ++;
                
                currentTextureX += textureScanLineXStep;
                currentTextureY += textureScanLineYStep;
            }
          
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
            
            textureScanLineStartX += textureScanLineStartXStep;
            textureScanLineStartY += textureScanLineStartYStep;
        }
    }

    public void drawTexturedTriangleWithAlpha(Triangle dstTriangle, Triangle srcTriangle, ArrayImage texture) {
        
        // Arrange points,
        points[0] = dstTriangle.points[dstTriangle.offset  ];
        points[1] = dstTriangle.points[dstTriangle.offset+1];
        points[2] = dstTriangle.points[dstTriangle.offset+2];
        texturePoints[0] = srcTriangle.points[srcTriangle.offset  ];
        texturePoints[1] = srcTriangle.points[srcTriangle.offset+1];
        texturePoints[2] = srcTriangle.points[srcTriangle.offset+2];
        
        Point topPoint, middlePoint, bottomPoint;
        Point textureTopPoint, textureMiddlePoint, textureBottomPoint;
        if (points[0].y <= points[1].y) {
            
            if (points[0].y <= points[2].y) {
                topPoint = points[0];
                textureTopPoint = texturePoints[0];
                if (points[1].y <= points[2].y) {
                    middlePoint = points[1];
                    bottomPoint = points[2];
                    textureMiddlePoint = texturePoints[1];
                    textureBottomPoint = texturePoints[2];
                } else {
                    middlePoint = points[2];
                    bottomPoint = points[1];
                    textureMiddlePoint = texturePoints[2];
                    textureBottomPoint = texturePoints[1];
                }    
            } else {
                topPoint = points[2];
                middlePoint = points[0];
                bottomPoint = points[1];
                textureTopPoint    = texturePoints[2];
                textureMiddlePoint = texturePoints[0];
                textureBottomPoint = texturePoints[1];
            }
        } else if (points[1].y <= points[2].y) {
            topPoint = points[1];
            textureTopPoint = texturePoints[1];
            if (points[0].y <= points[2].y) {
                middlePoint = points[0];
                bottomPoint = points[2];
                textureMiddlePoint = texturePoints[0];
                textureBottomPoint = texturePoints[2];
            } else {
                middlePoint = points[2];
                bottomPoint = points[0];
                textureMiddlePoint = texturePoints[2];
                textureBottomPoint = texturePoints[0];
            }    
        } else {
            topPoint = points[2];
            middlePoint = points[1];
            bottomPoint = points[0];
            textureTopPoint    = texturePoints[2];
            textureMiddlePoint = texturePoints[1];
            textureBottomPoint = texturePoints[0];
        }
                
        // Find the fourth point,
        fourthPoint.y = middlePoint.y;
        float bisectionRatio = 
                (fourthPoint.y - topPoint.y) / 
                (bottomPoint.y - topPoint.y);
        fourthPoint.x = topPoint.x + 
                        ((bottomPoint.x - topPoint.x) * bisectionRatio);

        textureFourthPoint.x = textureTopPoint.x + 
                        ((textureBottomPoint.x - textureTopPoint.x) * bisectionRatio);
        textureFourthPoint.y = textureTopPoint.y + 
                        ((textureBottomPoint.y - textureTopPoint.y) * bisectionRatio);
        
        // Start rasterizing,
        float triangleHeight = bottomPoint.y - topPoint.y;
        float halfTriangleHeight = fourthPoint.y - topPoint.y;

        float scanLineStart, scanLineEnd;
        scanLineStart = scanLineEnd = topPoint.x;
        
        float textureScanLineStartX = textureTopPoint.x;
        float textureScanLineStartY = textureTopPoint.y;
        float textureScanLineXStep, textureScanLineYStep;
        
        float scanLineStartStep, scanLineEndStep;
        float textureScanLineStartXStep, textureScanLineStartYStep;
        if (middlePoint.x >= fourthPoint.x) {
            scanLineStartStep = (bottomPoint.x - topPoint.x) / triangleHeight;
            scanLineEndStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
            
            textureScanLineStartXStep = (textureBottomPoint.x - textureTopPoint.x) / triangleHeight;
            textureScanLineStartYStep = (textureBottomPoint.y - textureTopPoint.y) / triangleHeight;
           
            float dstTriangleWidth = middlePoint.x - fourthPoint.x;
            textureScanLineXStep = (textureMiddlePoint.x - textureFourthPoint.x) / dstTriangleWidth;
            textureScanLineYStep = (textureMiddlePoint.y - textureFourthPoint.y) / dstTriangleWidth;
        } else {
            scanLineStartStep = (middlePoint.x - topPoint.x) / halfTriangleHeight;
            scanLineEndStep = (bottomPoint.x - topPoint.x) / triangleHeight;

            textureScanLineStartXStep = (textureMiddlePoint.x - textureTopPoint.x) / halfTriangleHeight;
            textureScanLineStartYStep = (textureMiddlePoint.y - textureTopPoint.y) / halfTriangleHeight;
            
            float dstTriangleWidth = fourthPoint.x - middlePoint.x;
            textureScanLineXStep = (textureFourthPoint.x - textureMiddlePoint.x) / dstTriangleWidth;
            textureScanLineYStep = (textureFourthPoint.y - textureMiddlePoint.y) / dstTriangleWidth;
        }
        
        int currentY = (int) topPoint.y;
        int integerHalfTriangleHeight = (int) fourthPoint.y - (int) topPoint.y;
        for (int scanLinesCount=integerHalfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
        //for (int scanLinesCount=(int) halfTriangleHeight; scanLinesCount>0; scanLinesCount--) {
            int currentX = (int) scanLineStart;
            float currentTextureX = textureScanLineStartX;
            float currentTextureY = textureScanLineStartY;
            
            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                try {
                    drawPixel(currentX, currentY, texture.getPixel((int) currentTextureX, (int) currentTextureY));
                } catch (Exception e) {}
                currentX++;
                
                currentTextureX += textureScanLineXStep;
                currentTextureY += textureScanLineYStep;
            }
            
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
            
            textureScanLineStartX += textureScanLineStartXStep;
            textureScanLineStartY += textureScanLineStartYStep;
        }
        
        float remainingTriangleHeight = triangleHeight - halfTriangleHeight;
        if (middlePoint.x > fourthPoint.x) {
            scanLineEndStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineEnd = middlePoint.x;
        } else {
            scanLineStartStep = (bottomPoint.x - middlePoint.x) / remainingTriangleHeight;
            scanLineStart = middlePoint.x;
            
            textureScanLineStartXStep = (textureBottomPoint.x - textureMiddlePoint.x) / remainingTriangleHeight;
            textureScanLineStartYStep = (textureBottomPoint.y - textureMiddlePoint.y) / remainingTriangleHeight;
            textureScanLineStartX = textureMiddlePoint.x;
            textureScanLineStartY = textureMiddlePoint.y;
        }

        int integerRemainingTriangleHeight = (int) triangleHeight - (int) halfTriangleHeight;
        if ((remainingTriangleHeight == 0) && (integerRemainingTriangleHeight == 0)) {
            integerRemainingTriangleHeight--;
        }
        for (int scanLinesCount=integerRemainingTriangleHeight; scanLinesCount>=0; scanLinesCount--) {

            int currentX = (int) scanLineStart;
            float currentTextureX = textureScanLineStartX;
            float currentTextureY = textureScanLineStartY;

            int integerScanLineWidth = (int) scanLineEnd - (int) scanLineStart;
            for ( ; integerScanLineWidth>0; integerScanLineWidth--) {
                try {
                    drawPixel(currentX, currentY, texture.getPixel((int) currentTextureX, (int) currentTextureY));
                } catch (Exception e) {}
                currentX++;
                
                currentTextureX += textureScanLineXStep;
                currentTextureY += textureScanLineYStep;
            }
          
            scanLineStart += scanLineStartStep;
            scanLineEnd += scanLineEndStep;
            currentY ++;
            
            textureScanLineStartX += textureScanLineStartXStep;
            textureScanLineStartY += textureScanLineStartYStep;
        }
    }

    public static class Triangle {
     
        Point[] points;
        int offset;
        
        public Triangle(Point[] points, int offset) {
            this.points = points;
            this.offset = offset;
         }
    }
}