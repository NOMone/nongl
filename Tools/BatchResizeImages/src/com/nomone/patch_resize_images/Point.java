package com.nomone.patch_resize_images;

public class Point {

    public float x, y, z;

    public Point() {}

    public Point(Point point) {
        x = point.x;
        y = point.y;
        z = point.z;
    }

    public Point(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Point(Point point1, Point point2, float ratio) {
        lerp(point1, point2, ratio);
    }

    public float scalerProduct(Point vector) {
        return (x * vector.x) + (y * vector.y) + (z * vector.z);
    }
    
    public void lerp(Point point1, Point point2, float ratio) {
        this.x = point1.x + ((point2.x - point1.x) * ratio);
        this.y = point1.y + ((point2.y - point1.y) * ratio);
        this.z = point1.z + ((point2.z - point1.z) * ratio);
    }
    
    public Point(Point point1, Point point2, float ratioX, float ratioY, float ratioZ) {
        this.x = point1.x + ((point2.x - point1.x) * ratioX);
        this.y = point1.y + ((point2.y - point1.y) * ratioY);
        this.z = point1.z + ((point2.z - point1.z) * ratioZ);
    }

    public void clone(Point point) {
        x = point.x;
        y = point.y;
        z = point.z;
    }

    public void add(Point point) {
        x += point.x;
        y += point.y;
        z += point.z;
    }
    
    public void sub(Point point) {
        x -= point.x;
        y -= point.y;
        z -= point.z;
    }
    
    public void sub(float value) {
        x -= value;
        y -= value;
        z -= value;
    }

    public void mul(float value) {
        x *= value;
        y *= value;
        z *= value;
    }
    
    public float magnitude() {
        return (float) Math.sqrt((x * x) + (y * y) + (z * z));
    }
    
    public float distance(Point point) {
        return (float) Math.sqrt( ((point.x-x) * (point.x-x)) + ((point.y-y) * (point.y-y)));
    }

    public void rotateAroundZ(float angle) {
            float newX = (float) (x * Math.cos(angle) - y * Math.sin(angle));
            float newY = (float) (x * Math.sin(angle) + y * Math.cos(angle));
            x = newX;
            y = newY;
    }

    public void rotateAroundX(float angle) {
            float newZ = (float) (z * Math.cos(angle) - y * Math.sin(angle));
            float newY = (float) (z * Math.sin(angle) + y * Math.cos(angle));
            z = newZ;
            y = newY;
    }

    public void rotateAroundY(float angle) {
            float newZ = (float) (z * Math.cos(angle) - x * Math.sin(angle));
            float newX = (float) (z * Math.sin(angle) + x * Math.cos(angle));
            z = newZ;
            x = newX;
    }
}