package com.nomone.patch_resize_images;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;

public class PatchResizer {

	static final boolean STRETCH = false;
	static final int TARGET_WIDTH = 8;
	static final int TARGET_HEIGHT = 16;
	static final String INPUT_FORMAT = ".png";
	
	public static void main(String[] args) {

		System.out.println("besm Allah :)");
		System.out.println();
		
        URL root = PatchResizer.class.getProtectionDomain().getCodeSource().getLocation();
        String path=null;
        try { path = (new File(root.toURI())).getParentFile().getPath(); } catch (Exception e) {}

		File inputDirectory = new File(path, "InputImages");
		File[] inputImageFiles = inputDirectory.listFiles(new FilenameFilter() {
			@Override public boolean accept(File file, String filename) {
				return filename.endsWith(INPUT_FORMAT);
			}
 		});
		
		for (int i=0; i<inputImageFiles.length; i++) {
			resizeImage(inputImageFiles[i]);
		}
		
		System.out.println("al 7amd le-Allah, done :)");
 	}
	
	public static void resizeImage(File imageFile) {

		System.out.println("Processing: " + imageFile.getName());
		
		// Load the image,
		ArrayImage image = null;
		try { 
			image = new ArrayImage(imageFile.getAbsolutePath(), null);
		} catch (Exception e) { 
			System.out.println("Failed to load image!"); 
			e.printStackTrace(); 
		}
		
		// Create a new resized image,
		ArrayImage newImage = new ArrayImage(TARGET_WIDTH, TARGET_HEIGHT, null);
		if (STRETCH) {
			newImage.drawImageFine(
					image, 
					0, 0, image.width, image.height, 
					0, 0, TARGET_WIDTH, TARGET_HEIGHT, false, false);
		} else {
			newImage.drawImageFine(
					image, 
					0, 0, TARGET_WIDTH, TARGET_HEIGHT, 
					0, 0, TARGET_WIDTH, TARGET_HEIGHT, false, false);
		}
		
		// Save image (overwrite),
		newImage.saveImage(imageFile.getAbsolutePath());	
	}
}
