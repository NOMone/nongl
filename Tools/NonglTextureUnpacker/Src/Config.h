#pragma once

#define VERBOSE true
#define CHECK_GL_ERRORS true

#define APPLICATION_TITLE "Nongl Texture Unpacker"

#define TEXTURE_PACK ""

#define DESIGN_WIDTH 800.0f
#define DESIGN_HEIGHT 1280.0f

#define MAC_WINDOW_SCALE 0.5f
