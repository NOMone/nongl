
#include "Nongl/Nongl.h"
#include "Nongl/SystemLog.h"
#include "Nongl/File.h"
#include "Nongl/SystemFileServices.h"
#include "Nongl/DisplayManager.h"
#include "Nongl/TextureRegion.h"
#include "Nongl/PixelMap.h"

using namespace Ngl;

#define TEXTURE_PACK_FILE "Packed/CombinedPack.txt"
#define UNPACKING_FOLDER "Unpacked"

void Nongl::main() {

	auto lambda = []() { LOGE("besm Allah :)"); };
	lambda();

	// Load texture atlas,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	displayManager->loadTextureAtlas(File(FileLocation::SDCARD, TEXTURE_PACK_FILE));

	// If unpacking folder doesn't exist, create it,
	File UnpackingFolder = File(FileLocation::SDCARD, UNPACKING_FOLDER);
	if (!UnpackingFolder.exists()) UnpackingFolder.makeDir();

	// Unpack given texture atlas,
	int textureRegionsCount = displayManager->getTextureRegionsCount();
	for (int i=0; i<textureRegionsCount; i++) {
		const TextureRegion *currentTextureRegion = displayManager->getTextureRegion(i);
		LOGE("Processing %s\n", currentTextureRegion->imageName.getConstText());
		PixelMap textureRegionPixelMap(currentTextureRegion->originalWidth, currentTextureRegion->originalHeight);
		textureRegionPixelMap.readFromTextureRegion(currentTextureRegion, 0, 0);
		textureRegionPixelMap.saveToFile(
				File(UnpackingFolder, TextBuffer(currentTextureRegion->imageName).append(".png")),
				false);
	}
}
