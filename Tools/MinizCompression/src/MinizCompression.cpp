//============================================================================
// Name        : MinizCompression.cpp
// Author      : Raslanove
// Version     :
// Copyright   : None :D
//============================================================================

#include "miniz.h"
#include <sys/stat.h>
#include <stdio.h>

void compressFile(const char *inputFilePath, const char *outputFilePath) {

	// Get file size,
	struct stat fileState;
	stat(inputFilePath, &fileState);
	int inputFileSize = fileState.st_size;

	// Read file,
	void *inputData = malloc(inputFileSize);
	FILE *inputFile = fopen(inputFilePath, "rb");
	fread(inputData, 1, inputFileSize, inputFile);
	fclose(inputFile);

	// Compress,
	long compressedDataSize = inputFileSize + 100000;
	void *compressedData = malloc(compressedDataSize);
	int returnValue = compress2(
			(unsigned char *) compressedData,
			(unsigned long int *) &compressedDataSize,
			(unsigned char *) inputData,
			(unsigned long int) inputFileSize,
			9);

	// Free input data,
	free(inputData);

	// Save output file,
	FILE *outputFile = fopen(outputFilePath, "wb");
	fwrite(&inputFileSize, 4, 1, outputFile);
	fwrite(&compressedDataSize, 4, 1, outputFile);
	fwrite(compressedData, 1, compressedDataSize, outputFile);
	fclose(outputFile);

	// Free compressed data,
	free(compressedData);

	// Print status,
	if (returnValue == Z_OK) {
		printf("Success! Input size: %d, compressed size: %d, ratio: %f\n", inputFileSize, compressedDataSize, compressedDataSize / (float) inputFileSize);
	} else {
		printf("Failed! Error: %d\n", returnValue);
	}
}

void uncompressFile(const char *inputFilePath, const char *outputFilePath) {

	// Read file,
	FILE *inputFile = fopen(inputFilePath, "rb");
	long uncompressedDataSize=0;
	long compressedDataSize=0;
	fread(&uncompressedDataSize, 4, 1, inputFile);
	fread(&compressedDataSize, 4, 1, inputFile);

	void *inputData = malloc(compressedDataSize);
	fread(inputData, 1, compressedDataSize, inputFile);
	fclose(inputFile);

	// Uncompress,
	void *uncompressedData = malloc(uncompressedDataSize);
	int returnValue = uncompress(
			(unsigned char *) uncompressedData,
			(unsigned long int *) &uncompressedDataSize,
			(unsigned char *) inputData,
			(unsigned long int) compressedDataSize);

	// Free input data,
	free(inputData);

	// Save output file,
	FILE *outputFile = fopen(outputFilePath, "wb");
	fwrite(uncompressedData, 1, uncompressedDataSize, outputFile);
	fclose(outputFile);

	// Free uncompressed data,
	free(uncompressedData);

	// Print status,
	if (returnValue == Z_OK) {
		printf("Success!\n");
	} else {
		printf("Failed! Error: %d\n", returnValue);
	}
}

/*
// Nice programming trick:
// Class with read only member...
class Test {

	int _x;
public:

	const int &x;
	Test() : x(_x) { }
};
*/

int main() {

	//compressFile("Input/PackedTextures1.pvr", "Output/PackedTextures1.xmf");
	//compressFile("Input/PackedTextures2.pvr", "Output/PackedTextures2.xmf");
	//compressFile("Input/PackedTextures3.pvr", "Output/PackedTextures3.xmf");
	//compressFile("Input/PackedTextures4.pvr", "Output/PackedTextures4.xmf");
	//compressFile("Input/PackedTextures5.pvr", "Output/PackedTextures5.xmf");
	//compressFile("Input/PackedTextures6.pvr", "Output/PackedTextures6.xmf");
	//compressFile("Input/PackedTextures7.pvr", "Output/PackedTextures7.xmf");

	compressFile("Input/PackedTextures1.ktx", "Output/PackedTextures1.jet");
	compressFile("Input/PackedTextures2.ktx", "Output/PackedTextures2.jet");
	compressFile("Input/PackedTextures3.ktx", "Output/PackedTextures3.jet");
	compressFile("Input/PackedTextures4.ktx", "Output/PackedTextures4.jet");
	compressFile("Input/PackedTextures5test.ktx", "Output/PackedTextures5test.jet");
	compressFile("Input/PackedTextures6.ktx", "Output/PackedTextures6.jet");
	compressFile("Input/PackedTextures7.ktx", "Output/PackedTextures7.jet");
	compressFile("Input/PackedTextures8.ktx", "Output/PackedTextures8.jet");
	compressFile("Input/PackedTextures9.ktx", "Output/PackedTextures9.jet");
	compressFile("Input/PackedTextures10.ktx", "Output/PackedTextures10.jet");

	compressFile("Input/PackedTextures1_alpha.ktx", "Output/PackedTextures1_alpha.jet");
	compressFile("Input/PackedTextures2_alpha.ktx", "Output/PackedTextures2_alpha.jet");
	compressFile("Input/PackedTextures3_alpha.ktx", "Output/PackedTextures3_alpha.jet");
	compressFile("Input/PackedTextures4_alpha.ktx", "Output/PackedTextures4_alpha.jet");
	compressFile("Input/PackedTextures5test_alpha.ktx", "Output/PackedTextures5test_alpha.jet");
	compressFile("Input/PackedTextures6_alpha.ktx", "Output/PackedTextures6_alpha.jet");
	compressFile("Input/PackedTextures7_alpha.ktx", "Output/PackedTextures7_alpha.jet");
	compressFile("Input/PackedTextures8_alpha.ktx", "Output/PackedTextures8_alpha.jet");
	compressFile("Input/PackedTextures9_alpha.ktx", "Output/PackedTextures9_alpha.jet");
	compressFile("Input/PackedTextures10_alpha.ktx", "Output/PackedTextures10_alpha.jet");


	//uncompressFile("PackedTextures2.jet", "PackedTextures2.ktx");
	printf("al 7amd le-Allah :) :)");
	return 0;
}
