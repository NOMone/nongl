package com.nomone.filenames_editor;

import java.io.File;
import java.net.URL;

public class FileNamesEditor {

	static final String INPUT_DIRECTORY = "Files";

	public static String removeUpToFirstDotInclusive(String text) {
		String[] pieces = text.split("\\.", 2);
		if (pieces.length < 2) return text;
		return pieces[1];
	}

	public static String removeExtention(String text) {
		if (!text.contains(".")) return text;
		return text.substring(0, text.lastIndexOf('.'));
	}

	public static String removePrefix(String text, String prefix) {
		if (text.startsWith(prefix)) return text.substring(prefix.length());
		return text;
	}
	
	public static String prependPaddingChar(String text, String paddingChar, int requiredTotalLength) {
		requiredTotalLength -= text.length();
		while (requiredTotalLength-- != 0) text = paddingChar + text;
		return text;
	}
	
	// This is the function you need to fiddle with,
	public static String getNewName(File file) {
		String finalName = file.getName();
		return "smallPlatform." + removePrefix(finalName, "road.");
	}
	
	public static void main(String[] args) {
		
		System.out.println("besm Allah :)");
		System.out.println();
		
        URL root = FileNamesEditor.class.getProtectionDomain().getCodeSource().getLocation();
        String path=null;
        try { path = (new File(root.toURI())).getParentFile().getPath(); } catch (Exception e) {}

		File inputDirectory = new File(path, INPUT_DIRECTORY);
		File[] files = inputDirectory.listFiles();
		
		for (int i=0; i<files.length; i++) {
			String newName = getNewName(files[i]);
			files[i].renameTo(new File(inputDirectory, newName));
		}
		
		System.out.println("Finished");
		System.out.println();
	}
}
