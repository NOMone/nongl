#pragma once

#include "View.h"
#include "Sprite.h"

class ImageView;

class ImageViewAnimationListener : public AnimationListener {
	ImageView *imageView;
public:
	float scaleAboutCenter=1;

	inline ImageViewAnimationListener(ImageView *targetImageView) { this->imageView = targetImageView; }
	void onAnimationComponentApplied(AnimationTarget::Target target);
};

class ImageView : public View {

protected:
	Sprite sprite;
	float rotationPivotXDisplacement=0, rotationPivotYDisplacement=0;
	float depth=-1;

	virtual void draw(SpriteBatch *spriteBatch);
	void setupAnimationListener(Animation *animation);

public:

	inline ImageView() { sprite.textureRegion = 0; }
	inline ImageView(const Ngl::TextureRegion *textureRegion) { set(textureRegion); }

	inline void set(const Ngl::TextureRegion *textureRegion) {
		sprite.setFromTextureRegion(textureRegion);
	}

	inline void setTextureRegion(const Ngl::TextureRegion *textureRegion) { sprite.textureRegion = textureRegion; }

	inline float getWidth() { return sprite.width; }
	inline float getHeight() { return sprite.height; }
	inline float getScaledWidth() { return sprite.getScaledWidth(); }
	inline float getScaledHeight() { return sprite.getScaledHeight(); }
	inline float getScaleX() { return sprite.scaleX; }
	inline float getScaleY() { return sprite.scaleY; }
	inline float getLeft() { return sprite.getLeft(); }
	inline float getBottom() { return sprite.getBottom(); }
	inline float getRight() { return sprite.getRight(); }
	inline float getTop() { return sprite.getTop();  }
	inline float getCenterX() { return sprite.getCenterX();  }
	inline float getCenterY() { return sprite.getCenterY();  }
	inline float getDepth() { return depth; }

	inline void setLeft(float left) { sprite.setLeft(left); }
	inline void setBottom(float bottom) { sprite.setBottom(bottom); }
	inline void setRight(float right) { sprite.setRight(right); }
	inline void setTop(float top) { sprite.setTop(top); }
	inline void setCenterX(float centerX) { sprite.setCenterX(centerX); }
	inline void setCenterY(float centerY) { sprite.setCenterY(centerY); }
	inline void setCenter(float centerX, float centerY) { sprite.setCenter(centerX, centerY); }
	inline void setScale(float scaleX, float scaleY) { sprite.scaleX = scaleX; sprite.scaleY = scaleY; }
	inline void setDepth(float depth) { this->depth = depth; }

	inline void setRotation(float angleRadians) { sprite.rotation = angleRadians; }
	inline void setRotationPivotDisplacementFromCenter(float xDisplacement, float yDisplacement) {
		this->rotationPivotXDisplacement = xDisplacement;
		this->rotationPivotYDisplacement = yDisplacement;
	}

	inline void setColorMask(int colorMask) { sprite.colorMask = colorMask; }

	inline void adjustDepths(float maxZ, float minZ) { sprite.z = maxZ; }

	void drawOpaqueParts(SpriteBatch *spriteBatch);
	void drawTransparentParts(SpriteBatch *spriteBatch);
	void drawAllInOrder(SpriteBatch *spriteBatch);

	void attachToAnimation(Animation *animation);
};
