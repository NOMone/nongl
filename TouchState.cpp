#include "TouchState.h"
#include "utils.h"

/////////////////////////
// Pointer state
/////////////////////////

void PointerState::down(float x, float y) {
	downTimeMillis = lastUpdatedTimeMillis = getTimeMillis();
	this->downX = this->x = x;
	this->downY = this->y = y;
	this->isDown = true;
}

void PointerState::up(float x, float y) {
	this->lastUpdatedTimeMillis = getTimeMillis();
	this->x = x;
	this->y = y;
	this->isDown = false;
}

void PointerState::move(float x, float y) {
	this->lastUpdatedTimeMillis = getTimeMillis();
	this->x = x;
	this->y = y;
}

/////////////////////////
// Touch state
/////////////////////////

void TouchState::reset() {
	int32_t pointersCount = (int32_t) pointerStates.size();
	for (int i=0; i<pointersCount; i++) pointerStates[i].isDown = false;
}

bool TouchState::isRectangleTouched(float left, float bottom, float right, float top) {

	int32_t pointersCount = (int32_t) pointerStates.size();
	for (int i=0; i<pointersCount; i++) {
		if (!pointerStates[i].isDown)
			continue;

		if ((pointerStates[i].x >= left  ) && (pointerStates[i].x < right) &&
			(pointerStates[i].y >= bottom) && (pointerStates[i].y < top  )) {
			return true;
		}
	}

	return false;
}
