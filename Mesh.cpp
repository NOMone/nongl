#include "Mesh.h"
using namespace Ngl;

MeshBase::~MeshBase() {}

void MeshBase::setMaterial(const shared_ptr<Material> &material) {
	this->material = material;
}

void MeshBase::addFace(int16_t vertex1Index, int16_t vertex2Index, int16_t vertex3Index) {
	faceVertexIndices.push_back(vertex1Index);
	faceVertexIndices.push_back(vertex2Index);
	faceVertexIndices.push_back(vertex3Index);
}

void MeshBase::addFace(const int16_t *verticesIndices) {
	faceVertexIndices.push_back(verticesIndices[0]);
	faceVertexIndices.push_back(verticesIndices[1]);
	faceVertexIndices.push_back(verticesIndices[2]);
}

