#include "StaticLayout.h"
#include "Nongl.h"

StaticLayout::StaticLayout() :
	staticTransparentSpriteBatch(0, true, true),
	staticOpaqueSpriteBatch     (0, true, true),
	staticAllSpriteBatch        (0, true, true) {

	this->drawOutOfBoundChildren = true;

	transparentLastContextLostCount = -1;
	opaqueLastContextLostCount = -1;
	allLastContextLostCount = -1;
}

void StaticLayout::addView(View *view) {
	Layout::addView(view);

	// Set the transparent, opaque and all sprite batches for redrawing on the next draw,
	transparentLastContextLostCount--;
	opaqueLastContextLostCount--;
	allLastContextLostCount--;
}

void StaticLayout::drawOpaqueParts(SpriteBatch *spriteBatch) {

	spriteBatch->flush();
	//Nongl::setBlendEnabled(false);
	staticOpaqueSpriteBatch.colorMask = spriteBatch->colorMask;

	int contextLostCount = Nongl::getContextLostCount();
	if (contextLostCount != opaqueLastContextLostCount) {
		staticOpaqueSpriteBatch.clear();
		Layout::drawOpaqueParts(&staticOpaqueSpriteBatch);
		opaqueLastContextLostCount = contextLostCount;
	}

	staticOpaqueSpriteBatch.draw();
}

void StaticLayout::drawTransparentParts(SpriteBatch *spriteBatch) {

	spriteBatch->flush();
	staticTransparentSpriteBatch.colorMask = spriteBatch->colorMask;
	//Nongl::setBlendEnabled(true);

	int contextLostCount = Nongl::getContextLostCount();
	if (contextLostCount != transparentLastContextLostCount) {
		staticTransparentSpriteBatch.clear();
		Layout::drawTransparentParts(&staticTransparentSpriteBatch);
		transparentLastContextLostCount = contextLostCount;
	}

	staticTransparentSpriteBatch.draw();
}

void StaticLayout::drawAllInOrder(SpriteBatch *spriteBatch) {

	spriteBatch->flush();
	staticAllSpriteBatch.colorMask = spriteBatch->colorMask;
	//Nongl::setBlendEnabled(true);

	int contextLostCount = Nongl::getContextLostCount();
	if (contextLostCount != allLastContextLostCount) {
		staticAllSpriteBatch.clear();
		Layout::drawAllInOrder(&staticAllSpriteBatch);
		allLastContextLostCount = contextLostCount;
	}

	staticAllSpriteBatch.draw();
}
