#pragma once

#include "InputStream.h"
#include "OutputStream.h"
#include "ManagedObject.h"

#include <vector>
#include <string.h>
#include <stdint.h>

namespace Ngl {

	/////////////////////////////////
	// Byte buffer
	/////////////////////////////////

	class ByteBuffer {
		std::vector<uint8_t> data;
	public:

		inline ByteBuffer(int capacity=0) { data.reserve(capacity); }

		inline void resize(int newSize) { data.resize(newSize); }

		inline std::vector<uint8_t> &getVector() { return data; }
		inline void *getData() { return (void *) &data[0]; }

		inline int32_t getDataSize() { return (int32_t) data.size(); }

		inline ByteBuffer &pushChar(char value) {
			data.push_back((unsigned char) value);
			return *this;
		}

		inline ByteBuffer &pushByte(uint8_t value) {
			data.push_back(value);
			return *this;
		}

		inline ByteBuffer &pushData(const void *dataBlock, int32_t dataBlockSize) {
			const unsigned char *byteDataBlock = (const unsigned char *) dataBlock;
			data.insert(data.end(), byteDataBlock, byteDataBlock + dataBlockSize);
			return *this;
		}

		inline ByteBuffer &pushString(const char *string) {
			int32_t stringLength = (int32_t) strlen(string) + 1;
			data.insert(data.end(), string, string + stringLength);
			return *this;
		}

		inline ByteBuffer &pushInt(int32_t value) {
			uint8_t *currentChar = (uint8_t *) &value;
			for (int i=0; i<4; i++) data.push_back(*currentChar++);
			return *this;
		}

		// TODO: pushFloat, pushBool ... etc.

		inline void padFor4ByteAlignment() {
			while (data.size() & 3) data.push_back(0);
		}
	};

	/////////////////////////////////
	// Byte buffer input stream
	/////////////////////////////////

	class ByteBufferInputStream : public InputStream {
		ManagedPointer<ByteBuffer> byteBuffer;
		int32_t currentByteIndex=0;
	public:
		inline ByteBufferInputStream(ManagedPointer<ByteBuffer> byteBuffer) : byteBuffer(byteBuffer) {}

		int32_t readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector);
		int32_t readData(OutputStream &outputStream);
		bool hasData();
	};

	/////////////////////////////////
	// Byte buffer output stream
	/////////////////////////////////

	class ByteBufferOutputStream : public OutputStream {
		ManagedPointer<ByteBuffer> byteBuffer;
	public:
		inline ByteBufferOutputStream(ManagedPointer<ByteBuffer> byteBuffer) : byteBuffer(byteBuffer) {}

		void writeData(const void *data, int32_t sizeBytes);
		int32_t writeData(InputStream &inputStream);
	};
}
