#pragma once

#include <stdint.h>
#include <vector>

class PointerState {
public:
	bool isDown = false;
	float downX, downY;
	float x, y;
	double downTimeMillis;
	double lastUpdatedTimeMillis;

	void down(float x, float y);
	void up(float x, float y);
	void move(float x, float y);
};

class TouchState {
	std::vector<PointerState> pointerStates;
public:

	void reset();

	inline void allocateStates(int size) {
		while (pointerStates.size()<size) pointerStates.push_back(PointerState());
	}

	inline void pointerDown(int pointerId, float x, float y) { allocateStates(pointerId+1); pointerStates[pointerId].down(x, y); }
	inline void pointerUp(int pointerId, float x, float y) { allocateStates(pointerId+1); pointerStates[pointerId].up(x, y); }
	inline void pointerMove(int pointerId, float x, float y) { allocateStates(pointerId+1); pointerStates[pointerId].move(x, y); }

	inline int32_t getPointersCount() { return (int32_t) pointerStates.size(); }
	inline PointerState &getPointerState(int pointerId) { return pointerStates[pointerId]; }

	bool isRectangleTouched(float left, float bottom, float right, float top);
};
