#pragma once

#include <stdint.h>
#include <vector>

class Audio;
class Music;
class Sound;

class AudioManager {

	friend class Nongl;
	inline AudioManager() {}
	~AudioManager();

	std::vector<Audio *> audios;

	inline Audio *getAudio(int index) {
		return audios[index];
	}
	int32_t getAudioIndex(const char *name, int *insertIndex=0);

	void addAudio(Audio *audio);
public:

	Audio *getAudio(const char *name, bool verbose=true);

	Music *addMusic(const char *filename, const char *musicName);
	Sound *addSound(const char *filename, const char *soundName);
};
