#pragma once

#include "Nongl.h"
#include "Sprite.h"

#include <vector>

class PlainShader;
class SeparateAlphaShader;
class Quad;

namespace Ngl {
	class Texture;
	class TextureRegion;
}

class SpriteBatch {

	Ngl::Texture *texture;
	PlainShader *shader;
	SeparateAlphaShader *separateAlphaShader;

	bool vboPowered, isStatic;
	unsigned int verticesVBO, indicesVBO;
	int verticesVBOSize, indicesVBOSize;
	bool vbosModified;

	int verticesCount;
	std::vector<float> vertexData;
	std::vector<unsigned short> indices;
	int vertexDataSize, indicesSize;

	Sprite filledRectangleSprite;
	Sprite lineSprite;
	float lineThickness, halfLineThickness, lineSpriteWidthReciprocal;

	void prepareForDraw(Ngl::Texture *texture, int vertexFloatsCount, int indicesShortsCount);
	void adjustVBOs();

public:

	int colorMask;

	int flushesDueToTextureChangeCount;

	SpriteBatch(int initialSpritesCapacity=0, bool vboPowered=false, bool isStatic=false);
	~SpriteBatch();

	Ngl::Texture *getTexture() { return texture; }
	void setTexture(Ngl::Texture *texture);

	inline void setShaders(PlainShader *shader, SeparateAlphaShader *separateAlphaShader) { this->shader = shader; this->separateAlphaShader = separateAlphaShader; }
	inline void setColorMask(int colorMask) { this->colorMask = colorMask; }

	inline void changeBlendMode(bool enabled) {
		if (Nongl::getBlendEnabled() != enabled) {
			flush();
			Nongl::setBlendEnabled(enabled);
		}
	}

	void drawSprite(Sprite *sprite);
	void drawQuad(Quad *quad);
	void drawRect(float left, float bottom, float width, float height, float depth, int color, bool fillCorners=true);
	void drawFilledRect(float left, float bottom, float width, float height, float depth, int color);
	void setLineThickness(float lineThickness);
	void drawLine(float x1, float y1, float z1, float x2, float y2, float z2, int color);

	void clear();
	void onVBOsDiscarded();

	void flush();
	void draw();
};
