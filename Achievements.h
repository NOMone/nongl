#pragma once

class TextBuffer;

namespace Ngl {

    class TextureRegion;

    class Achievements {
    public:
        static void declareAchievement(
                const char *preferenceName,
                const char *achievementName,
                const TextBuffer &notificationMessage,
                const Ngl::TextureRegion *notificationIcon);
    };
}
