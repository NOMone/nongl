#include "Vertex.h"
using namespace Ngl;

const Vec3 TexturedVertex::defaultXYZ(0, 0, 0);
const Vec2 TexturedVertex::defaultTextureCoordinates(0, 0);
const int32_t TexturedVertex::defaultColor(0xffffffff);

const Vec2 TextureRegionWrappedVertex::defaultTextureRegionCoordinates(0, 0);
const Vec2 TextureRegionWrappedVertex::defaultTextureRegionSize(1, 1);
