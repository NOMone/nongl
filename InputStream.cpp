#include "InputStream.h"
#include "OutputStream.h"

#include <stdexcept>

int32_t Ngl::InputStream::readData(OutputStream &outputStream) {

	std::vector<uint8_t> data;
	readData(data);
	outputStream.writeData(&data[0], data.size());

	return data.size();
}

int32_t Ngl::InputStream::readAllData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) {

	// Call readData() as many times as it takes until all data is consumed,
	int32_t dataSize=0;
	while (hasData()) {
		dataSize += readData(outputVector, offsetInVector+dataSize);
	}

	return dataSize;
}

int32_t Ngl::InputStream::readAllData(OutputStream &outputStream) {

	// Call readData() as many times as it takes until all data is consumed,
	int32_t dataSize=0;
	while (hasData()) {
		dataSize += readData(outputStream);
	}

	return dataSize;
}

void Ngl::InputStream::mark(int32_t readLimit) {
	if (!markSupported()) throw std::runtime_error("Marking not supported.");
}

void Ngl::InputStream::reset() {
	if (!markSupported()) throw std::runtime_error("Marking not supported.");
}
