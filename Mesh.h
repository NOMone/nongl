#pragma once

#include "ManagedObject.h"
#include "Material.h"
#include "Vertex.h"
#include "MeshBatch.h"
#include "SpriteBatch.h"

#include <stdexcept>
#include <stdint.h>
#include <vector>
#include <type_traits>

namespace Ngl {

	class MeshBase {

	protected:
		const int32_t MAX_VERTICES_COUNT = 65535;

		shared_ptr<Material> material;
		std::vector<uint16_t> faceVertexIndices;

		shared_ptr<Material> lastUsedMaterial;
		std::vector<uint8_t> formattedVerticesData;

	public:
		virtual ~MeshBase();

		void setMaterial(const shared_ptr<Material> &material);

		virtual void addVertex(const Vertex &vertex)=0;
		void addFace(int16_t vertex1Index, int16_t vertex2Index, int16_t vertex3Index);
		void addFace(const int16_t *verticesIndices);

		virtual int32_t getVerticesCount()=0;

		virtual void draw(MeshBatch &meshBatch) = 0;
		virtual void drawWireFrame(SpriteBatch &spriteBatch, int32_t color) = 0;
	};

	template <typename VertexType>
	class Mesh : public MeshBase {

	    static_assert(std::is_base_of<Vertex, VertexType>::value, "Mesh template type must inherit from Vertex.");
	    std::vector<VertexType> vertices;

	public:

	    void addVertex(const Vertex &vertex) {
			if (vertices.size() >= MAX_VERTICES_COUNT) {
		    	throw std::runtime_error("Can't add vertex to mesh, max allowed count reached.");
			}
			vertices.push_back(vertex.clone<VertexType>());
	    }

		void addVertex(const VertexType &vertex) {
			if (vertices.size() >= MAX_VERTICES_COUNT) {
		    	throw std::runtime_error("Can't add vertex to mesh, max allowed count reached.");
			}
			vertices.push_back(vertex);
		}

		int32_t getVerticesCount() { return (int32_t) vertices.size(); }

		void draw(MeshBatch &meshBatch) {

			if (material.expired()) {
		    	throw std::runtime_error("Can't draw mesh without a valid material.");
			}

			int32_t formattedVerticesDataSizeBytes = material->getFormatedVerticesDataSizeBytes(vertices.size());

			// If material changed, generate new formatted vertex data,
			if (lastUsedMaterial != material) {

				// Make sure the vector is large enough,
				if (formattedVerticesDataSizeBytes > formattedVerticesData.capacity()) formattedVerticesData.resize(formattedVerticesDataSizeBytes);

				// Get the data,
				material->getFormatedVerticesData(&vertices[0], sizeof(VertexType), vertices.size(), &formattedVerticesData[0]);

				// Set the new material as the last used one,
				lastUsedMaterial = material;
			}

			// Buffer into the mesh batch,
			meshBatch.batch(
					material,
					&formattedVerticesData[0], vertices.size(),
					&faceVertexIndices[0], faceVertexIndices.size());
		}

		void drawWireFrame(SpriteBatch &spriteBatch, int32_t color) {

			Vec3 defaultXYZ(0, 0, 0);
			int32_t faceVertexIndicesCount = faceVertexIndices.size();
			for (int32_t i=0; i<faceVertexIndicesCount; i+=3) {
				const VertexType &vertex1 = vertices[faceVertexIndices[i  ]];
				const VertexType &vertex2 = vertices[faceVertexIndices[i+1]];
				const VertexType &vertex3 = vertices[faceVertexIndices[i+2]];
				Vec3 vertex1XYZ = vertex1.getXYZ(defaultXYZ);
				Vec3 vertex2XYZ = vertex2.getXYZ(defaultXYZ);
				Vec3 vertex3XYZ = vertex3.getXYZ(defaultXYZ);

				spriteBatch.drawLine(vertex1XYZ.x, vertex1XYZ.y, vertex1XYZ.z, vertex2XYZ.x, vertex2XYZ.y, vertex2XYZ.z, color);
				spriteBatch.drawLine(vertex2XYZ.x, vertex2XYZ.y, vertex2XYZ.z, vertex3XYZ.x, vertex3XYZ.y, vertex3XYZ.z, color);
				spriteBatch.drawLine(vertex3XYZ.x, vertex3XYZ.y, vertex3XYZ.z, vertex1XYZ.x, vertex1XYZ.y, vertex1XYZ.z, color);
			}
		}
	};
}
