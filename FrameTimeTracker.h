#ifndef FRAME_TIME_TRACKER_H
#define FRAME_TIME_TRACKER_H

class FrameTimeTracker {

	double lastFrameTimeMillis;
	double elapsedTimeMillis;
	double *elapsedTimeMillisHistory;
	float minFrameDurationMillis;
	int totalHistoryLength;
	int usedHistoryLength;
	int historyIndex;

	bool skipFrame;

public:

	FrameTimeTracker(int totalHistoryLength=10, float minFrameDurationMillis=0);
	~FrameTimeTracker();

	inline void skipNextFrame() { skipFrame = true; }
	double newFrame();
	inline double getElapsedTimeMillis() { return elapsedTimeMillis; }
};

#endif
