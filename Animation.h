#pragma once

#include "Threads.h"
#include "Interpolator.h"
#include "ManagedObject.h"

#include <stdint.h>
#include <vector>
#include <list>

class AnimationTarget {
	inline AnimationTarget() {}
	public: enum Target {
		X, Y, Z, X_DISPLACEMENT, Y_DISPLACEMENT, YAW, SCALE_X, SCALE_Y, SCALE_ABOUT_CENTER, COLOR_MASK
	};
};

class AnimationComponent {

	enum TargetType {FLOAT_TARGET, COLOR_TARGET, INT_TARGET, CHAR_TARGET};

public:

	AnimationTarget::Target target;
	Interpolator *interpolator;
	float initialValueFloat, finalValueFloat;
	float initialValueFloatBackup, finalValueFloatBackup;
	unsigned int initialValueInt, finalValueInt;
	unsigned int initialValueIntBackup, finalValueIntBackup;
	bool initialValueRelative, finalValueRelative;

	TargetType targetType;
	void *targetAddress;
	int dummyTarget;

	AnimationComponent(AnimationTarget::Target target, const Interpolator &interpolator, float initialValueFloat, int initialValueInt, bool initialValueRelative, float finalValueFloat, int finalValueInt, bool finalValueRelative);
	~AnimationComponent();

	void setTargetAddress(float *targetAddress);
	void setTargetAddress(int *targetAddress, bool isColor=false);
	void setTargetAddress(char *targetAddress);

	inline bool update(float elapsedTime) {

		if (interpolator->hasFinished()) return true;

		bool finished = interpolator->update(elapsedTime);
		if (isDelayed()) return finished;

		switch(targetType) {
			case FLOAT_TARGET: *((float *) targetAddress) = interpolator->interpolate(initialValueFloat, finalValueFloat); break;
			case COLOR_TARGET: *((int *) targetAddress) = interpolator->interpolateColors(initialValueInt, finalValueInt); break;
			case INT_TARGET: *((int *) targetAddress) = (int) interpolator->interpolate(initialValueFloat, finalValueFloat); break;
			case CHAR_TARGET: *((char *) targetAddress) = (char) interpolator->interpolate(initialValueFloat, finalValueFloat); break;
		}
		return finished;
	}

	inline void reset() { interpolator->reset(); }
	inline void start() { interpolator->start(); }
	inline void restart() { interpolator->restart(); }
	inline void pause() { interpolator->pause(); }

	inline bool isDelayed() { return interpolator->isDelayed(); }
	inline bool hasFinished() { return interpolator->hasFinished(); }

	inline float getTotalDuration() { return interpolator->getTotalDuration(); }
};

class AnimationListener {
public:
	virtual inline ~AnimationListener() {}
	virtual void onAnimationComponentApplied(AnimationTarget::Target target) = 0;
};

class Animation {

	bool started;
	bool ended;
	Ngl::ManagedPointer<Runnable> onAnimationEndTask;
	void *onAnimationEndTaskData;

	bool *onUpdatedFlagTarget;
	bool dummyOnUpdatedFlagTarget;
	AnimationListener *animationListener;

public:

	void *targetObject;
	bool garbage;
	bool repeat;

	std::vector<AnimationComponent *> components;

	inline Animation(void *targetObject=0) {
		started = false;
		ended = false;
		onAnimationEndTaskData = 0;
		onUpdatedFlagTarget = &dummyOnUpdatedFlagTarget;
		animationListener = 0;

		this->targetObject = targetObject;
		garbage = false;
		repeat = false;
	}
	~Animation();
	Animation *clone() const;

	void addComponent(AnimationTarget::Target target, const Interpolator &interpolator, float initialValueFloat, int initialValueInt, bool initialValueRelative, float finalValueFloat, int finalValueInt, bool finalValueRelative);
	void removeAllComponents();
	void removeAllComponentsOfTarget(AnimationTarget::Target target);

	inline void setOnAnimationEndTask(const Ngl::ManagedPointer<Runnable> &onAnimationEndTask, void *onAnimationEndTaskData=0) {
		this->onAnimationEndTask = onAnimationEndTask;
		this->onAnimationEndTaskData = onAnimationEndTaskData;
	}

	inline void setOnUpdatedFlagTarget(bool *onUpdatedFlagTarget) { this->onUpdatedFlagTarget = onUpdatedFlagTarget; }
	inline void setAnimationListener(AnimationListener *animationListener) { this->animationListener = animationListener; }

	bool update(float elapsedTime);	// Returns true when finish.
	void reset();
	void start();
	void restart();
	void pause();

	bool hasFinished();
	float getTotalDuration();
};

class AnimationController {
	std::list<Animation *> animations;
public:

	bool deletesFinishedAnimations;
	bool deletesAnimationsOnDestruction;

	inline AnimationController() : deletesFinishedAnimations(true), deletesAnimationsOnDestruction(true) {}
	~AnimationController();

	inline int32_t getAnimationsCount() { return (int32_t) animations.size(); }
	void addAnimation(Animation *animation);
	void markAnimationsAsGarbage(const void *targetObject);
	void getAnimations(const void *targetObject, std::vector<Animation *> &outAnimations);
	void deleteAllAnimations();
	void update(float elapsedTime);
};
