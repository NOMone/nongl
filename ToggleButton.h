#pragma once

#include "Button.h"

class ToggleButton;

class ToggleButtonOnToggledListener {
public:
	inline virtual ~ToggleButtonOnToggledListener() {}

	virtual void toggleButtonOnToggled(ToggleButton *toggleButton, bool toggled);
};

class ToggleButton : public Button {

	ToggleButtonOnToggledListener *toggleButtonOnToggledListener;

	bool toggled;

public:

	ToggleButton(const ButtonParams *parameters=0);
	ToggleButton(float x, float y, const char *text=0);

	inline void setToggled(bool toggled) { this->toggled = toggled; }
	inline bool isToggled() { return toggled; }

	virtual bool onTouchEvent(const TouchEvent *event);

	void update(float elapsedTimeMillis);

	inline void setOnToggledListener(ToggleButtonOnToggledListener *toggleButtonOnToggledListener) { this->toggleButtonOnToggledListener = toggleButtonOnToggledListener; }
};
