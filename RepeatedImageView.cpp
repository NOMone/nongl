#include <Nongl/RepeatedImageView.h>

using namespace Ngl;

void RepeatedImageView::draw(SpriteBatch *spriteBatch) {

    float unitWidth  = sprite.getScaledWidth ();
    float unitHeight = sprite.getScaledHeight();

    if (sprite.rotation) {

        // Rotate center about pivot,
        float centerX = sprite.getCenterX();
        float centerY = sprite.getCenterY();

        float cosAngle = cosf(sprite.rotation);
        float sinAngle = sinf(sprite.rotation);

        // Note: sprite center is different from the view center,
        float rotationPivotX = - (getCenterX() + rotationPivotXDisplacement - centerX);
        float rotationPivotY = - (getCenterY() + rotationPivotYDisplacement - centerY);

        float rotatedDisplacementX = (rotationPivotX * cosAngle) - (rotationPivotY * sinAngle);
        float rotatedDisplacementY = (rotationPivotY * cosAngle) + (rotationPivotX * sinAngle);

        float unitWidthCosAngle = unitWidth * cosAngle;
        float unitWidthSinAngle = unitWidth * sinAngle;

        float unitHeightCosAngle = unitHeight * cosAngle;
        float unitHeightSinAngle = unitHeight * sinAngle;

        float displacedCenterX = (centerX - rotationPivotX) + rotatedDisplacementX;
        float displacedCenterY = (centerY - rotationPivotY) + rotatedDisplacementY;

        // Draw,
        for (int32_t j=0; j<verticalRepeats; j++) {
            for (int32_t i=0; i<horizontalRepeats; i++) {

                sprite.setCenter(
                        displacedCenterX + (i*unitWidthCosAngle) - (j*unitHeightSinAngle),
                        displacedCenterY + (i*unitWidthSinAngle) + (j*unitHeightCosAngle));

                sprite.draw(spriteBatch);
            }
        }


        // Restore original center,
        sprite.setCenter(centerX, centerY);

    } else {

        float oldLeft   = getLeft  ();
        float oldBottom = getBottom();

        for (int32_t j=0; j<verticalRepeats; j++) {
            for (int32_t i=0; i<horizontalRepeats; i++) {
                sprite.x = oldLeft   + (unitWidth  * i);
                sprite.y = oldBottom + (unitHeight * j);
                sprite.draw(spriteBatch);
            }
        }

        sprite.x = oldLeft;
        sprite.y = oldBottom;
    }
}
