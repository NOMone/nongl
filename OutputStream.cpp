#include "OutputStream.h"
#include "InputStream.h"

int32_t Ngl::OutputStream::writeData(InputStream &inputStream) {

	std::vector<uint8_t> data;
	inputStream.readData(data);
	writeData(&data[0], data.size());

	return data.size();
}

int32_t Ngl::OutputStream::writeAllData(InputStream &inputStream) {

	// Call writeData() as many times as it takes until all data is consumed,
	int32_t dataSize=0;
	while (inputStream.hasData()) {
		dataSize += writeData(inputStream);
	}

	return dataSize;
}

