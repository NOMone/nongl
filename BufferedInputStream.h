#pragma once

#include "InputStream.h"
#include "ManagedObject.h"

#include <vector>

namespace Ngl {

	class BufferedInputStream : public InputStream {
		Ngl::ManagedPointer<InputStream> inputStream;
		std::vector<uint8_t> bufferedData;
		int32_t currentByteIndex=0;
		int32_t markReadLimit=0;
		bool marked=false, markReadLimitExceedFlag=false;

		bool discardBufferIfPossible();
		bool markReadLimitExceeded();

	public:

		inline BufferedInputStream(Ngl::ManagedPointer<InputStream> inputStream) : inputStream(inputStream) {}

		bool hasData();
		int32_t readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector);

		inline bool markSupported() { return true; }
		void mark(int32_t readLimit=0);
		void reset();

		uint8_t readByte();
		int16_t readInt16();
		int32_t readInt32();
		void readBlock(std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t sizeBytes);

		bool sourceHasData();
		int32_t buffer();
		int32_t getBufferedSize();
		void compact();
	};
}
