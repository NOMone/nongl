#if NONGL_FACEBOOK_SDK

#include <Nongl/Facebook.h>

#include <Nongl/SystemFacebookSdk.h>
#include <Nongl/ManagedObject.h>
#include <Nongl/HttpRequest.h>
#include <Nongl/Json/json.h>
#include <Nongl/DialogActivity.h>
#include <Nongl/OnScreenNotification.h>
#include <Nongl/Button.h>
#include <Nongl/TextView.h>
#include <Nongl/LinearLayout.h>
#include <Nongl/Theme.h>
#include <Nongl/SystemInterface.h>
#include <Nongl/SystemLog.h>

#include <functional>

using namespace Ngl;
using namespace JSON;

/////////////////////////////
// Helper classes
/////////////////////////////

namespace {

    ///////////////////////////////
    // Facebook Login Dialog
    ///////////////////////////////

    class FacebookLoginDialog : public DialogActivity, public AsyncRequest, public ButtonListener {

        TextBuffer appId, clientToken;

        TextView *centerText;
        LinearLayout *codeLayout;
        TextView *codeText;

        enum class State : int32_t {
            INITIAL,
            RECEIVED_CODE,
            WAITING_TO_POLL,
            WAITING_FOR_POLL_RESPONSE,
            RECEIVED_POLL_RESPONSE,
            LOGIN_FINISHED
        };
        State state = State::INITIAL;

        TextBuffer verificationUrl;
        float pollingIntervalMillis=0, timeToPollMillis=0, timeToExpireMillis=0;
        float elapsedTimeMillis=0;

        void advanceState() { state = static_cast<State>(static_cast<int32_t>(state) + 1); }
        bool performStateAction();

        void receivedCodeState();
        void stepTimers(float elapsedTimeMillis);
        void waitingToPollState(float elapsedTimeMillis);
        void receivedPollResponseState();

    public:

        Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > onResponseReceivedListener;
        bool success=false;
        TextBuffer listenerMessage;

        static const char *activityName;
        FacebookLoginDialog(
                const TextBuffer &appId,
                const TextBuffer &clientToken,
                const TextBuffer &permissions,
                const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener);

        bool onBackPressed();
        void buttonOnClick(Button *button);

        void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);

        void errorMessageAndFinish(const TextBuffer &message);
        void scheduleFinish(float timeToFinishMillis, bool success, const TextBuffer &message);
    };

    const char *FacebookLoginDialog::activityName = "NGL Facebook login dialog";

    FacebookLoginDialog::FacebookLoginDialog(
            const TextBuffer &appId,
            const TextBuffer &clientToken,
            const TextBuffer &permissions,
            const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener) :
                    DialogActivity(activityName),
                    appId(appId), clientToken(clientToken), onResponseReceivedListener(onResponseReceivedListener) {

        Theme &theme = Theme::getGlobalTheme();

        // Dimming view,
        ColorView *dimmingView = new ColorView(0, 0, rootLayout.getWidth(), rootLayout.getHeight(), 0x80000000);
        rootLayout.addView(dimmingView);

        // Dialog layout (including background),
        Layout *dialogLayout = theme.createDialogLayout(
                rootLayout.getWidth()*0.5f, rootLayout.getHeight()*0.5f);
        dialogLayout->setCenter(rootLayout.getWidth()*0.5f, rootLayout.getHeight()*0.5f);
        rootLayout.addView(dialogLayout);

        // Please wait text,
        centerText = theme.createText("Please wait...", dialogLayout->getWidth() - theme.getDialogLayoutPadding());
        centerText->setWidth(dialogLayout->getWidth() - theme.getDialogLayoutPadding());
        centerText->setCenter(dialogLayout->getWidth()*0.5f, dialogLayout->getHeight()*0.5f);
        dialogLayout->addView(centerText);

        // Code Layout,
        codeLayout = new LinearLayout(
                theme.getDialogLayoutPadding(), theme.getDialogLayoutPadding(),
                dialogLayout->getWidth () - 2*theme.getDialogLayoutPadding(),
                dialogLayout->getHeight() - 2*theme.getDialogLayoutPadding(),
                Orientation::VERTICAL, Gravity::CENTER, 20);
        codeLayout->setVisible(false);
        dialogLayout->addView(codeLayout);

        // Enter code text,
        TextView *enterCodeText = theme.createText("Please enter this code when asked to:", codeLayout->getWidth());

        // Code text,
        codeText = theme.createText("000", codeLayout->getWidth());
        codeText->setColorMask(0xff0000ff);
        codeText->setWidth(codeLayout->getWidth());

        // Login button,
        Button *loginButton = new Button(ButtonParams("Login in browser", 0.6484375f, codeLayout->getWidth()));
        loginButton->setListener(this);

        // Add them to the layout in reverse-order,
        codeLayout->addView(loginButton);
        codeLayout->addView(codeText);
        codeLayout->addView(enterCodeText);

        rootLayout.layout();

        // Create and send an HTTP request to use devices login,
        httpRequest.setUrl("https://graph.facebook.com/v2.6/device/login");
        httpRequest.setType(HttpRequestType::POST);

        httpRequest.addData("access_token", TextBuffer(appId).append('|').append(clientToken));
        httpRequest.addData("scope", permissions);

        sendRequestInParallel();
    }

    bool FacebookLoginDialog::onBackPressed() {

        finish();
        if (!onResponseReceivedListener.expired()) {
            (*onResponseReceivedListener)(FacebookLoginResponseType::CANCEL, "Cancelled by user");
        }
        return true;
    }

    void FacebookLoginDialog::buttonOnClick(Button *button) {
        javaBrowseUrl(verificationUrl);
    }

    void FacebookLoginDialog::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {

        this->elapsedTimeMillis = elapsedTimeMillis;
        performStateAction();

        DialogActivity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
    }

    void FacebookLoginDialog::errorMessageAndFinish(const TextBuffer &message) {

        state = State::LOGIN_FINISHED;

        codeLayout->setVisible(false);
        centerText->setText("Couldn't login to Facebook");
        centerText->setVisible(true);

        // Close the dialog and report to the listener,
        scheduleFinish(1500, false, message);
    }

    void FacebookLoginDialog::scheduleFinish(float timeToFinishMillis, bool success, const TextBuffer &message) {

        this->success = success;
        this->listenerMessage = message;

        // Finish the dialog after a moment,
        shared_ptr<Runnable> scheduledTask(Runnable::createRunnable([](void *activity)->bool {

            FacebookLoginDialog *dialog = (FacebookLoginDialog *) activity;
            dialog->finish();

            if (!dialog->onResponseReceivedListener.expired()) {
                if (dialog->success) {
                    (*dialog->onResponseReceivedListener)(FacebookLoginResponseType::SUCCESS, dialog->listenerMessage);
                } else {
                    (*dialog->onResponseReceivedListener)(FacebookLoginResponseType::ERROR, dialog->listenerMessage);
                }
            }
            return true;
        }, this));
        Nongl::scheduler.schedule(scheduledTask, timeToFinishMillis, this);
    }

    bool FacebookLoginDialog::performStateAction() {

        switch (state) {
            case State::RECEIVED_CODE: receivedCodeState(); break;
            case State::WAITING_TO_POLL: waitingToPollState(elapsedTimeMillis); break;
            case State::WAITING_FOR_POLL_RESPONSE: stepTimers(elapsedTimeMillis); break;
            case State::RECEIVED_POLL_RESPONSE: receivedPollResponseState(); break;

            default: break;
        }
        return false; // Don't let the request be deleted automatically.
    }

    void FacebookLoginDialog::receivedCodeState() {

        // Pad the response with 2 zeroes to allow parsing in place,
        requestResponse.push_back(0);
        requestResponse.push_back(0);

        JSON::Parser jsonParser;
        JSON::Value *json = jsonParser.parseInPlace((char *) &requestResponse[0], requestResponse.size());

        auto error = [&](const TextBuffer &message) {
            delete json;
            this->errorMessageAndFinish(message);
        };

        // Errors occurred?
        if (jsonParser.logErrors()) { error("Response parsing error (probably no response)"); return; }

        // Get required fields,
        std::string code, user_code, verification_uri;
        double expires_in, interval;
        try {
            code             = (*json)["code"            ].str();
            user_code        = (*json)["user_code"       ].str();
            verification_uri = (*json)["verification_uri"].str();
            expires_in       = (*json)["expires_in"      ].num();
            interval         = (*json)["interval"        ].num();
        } catch (const std::exception &e) {
            error("Missing or malformed response fields"); return;
        }

        // Update the UI to display the code and the verification URL,
        centerText->setVisible(false);
        codeLayout->setVisible(true);
        codeText->setText(user_code.c_str());

        JSON::decode(verification_uri);
        verificationUrl = verification_uri.c_str();

        // Keep the timing parameters,
        timeToExpireMillis = expires_in * 1000;
        pollingIntervalMillis = interval * 1000;
        timeToPollMillis = pollingIntervalMillis;

        // Set the new HTTP request,
        httpRequest.setUrl("https://graph.facebook.com/v2.6/device/login_status");
        httpRequest.setType(HttpRequestType::POST);

        httpRequest.resetData();
        httpRequest.addData("access_token", TextBuffer(appId).append('|').append(clientToken));
        httpRequest.addData("code", code.c_str());

        advanceState();
        delete json;
    }

    void FacebookLoginDialog::stepTimers(float elapsedTimeMillis) {

        timeToExpireMillis -= elapsedTimeMillis;
        if (timeToExpireMillis <= 0) {
            errorMessageAndFinish("Login period expired");
            return;
        }

        timeToPollMillis -= elapsedTimeMillis;
        if (timeToPollMillis <= 0) timeToPollMillis = 0;
    }

    void FacebookLoginDialog::waitingToPollState(float elapsedTimeMillis) {

        stepTimers(elapsedTimeMillis);

        if (timeToPollMillis == 0) {
            timeToPollMillis = pollingIntervalMillis;
            advanceState();
            sendRequestInParallel();
        }
    }

    void FacebookLoginDialog::receivedPollResponseState() {

        // Pad the response with 2 zeroes to allow parsing in place,
        requestResponse.push_back(0);
        requestResponse.push_back(0);

        JSON::Parser jsonParser;
        JSON::Value *json = jsonParser.parseInPlace((char *) &requestResponse[0], requestResponse.size());

        auto error = [&](const TextBuffer &message) {
            delete json;
            this->errorMessageAndFinish(message);
        };

        // Errors occurred?
        if (jsonParser.logErrors()) { error("Response parsing error (probably no response)"); return; }

        // Unexpected response?
        JSON::Object *responseObject = dynamic_cast<JSON::Object *>(json);
        if (!responseObject) { error("Unexpected response type"); return; }

        // Try again if errors occurred,
        try {
            responseObject->get("error");
            state = State::WAITING_TO_POLL;
            delete json;
            return ;
        } catch (const std::exception &e) {}

        // Get required fields,
        std::string access_token;
        try {
            access_token = responseObject->get("access_token").str();
        } catch (const std::exception &e) {
            errorMessageAndFinish("Missing or malformed response fields");
            return;
        }

        // Update the UI to reflect the login success,
        codeLayout->setVisible(false);
        centerText->setText("Login successful!");
        centerText->setVisible(true);

        // Close the dialog and report to the listener,
        scheduleFinish(1500, true, "Login successful");

        // Keep the access token,
        javaSetStringPreference(FACEBOOK_ACCESS_TOKEN_PREFERENCE, access_token.c_str());

        // Stop polling,
        advanceState();
        delete json;
    }
}

////////////////////////////////////
// Facebook
////////////////////////////////////

TextBuffer Facebook::appId;
TextBuffer Facebook::clientToken;

void Facebook::isLoggedIn(std::function<void(bool loggedIn, TextBuffer id, TextBuffer name)> listener) {

    // Check if a usable token already exists,

    // Prepare a listener for a simple "me" request,
    typedef void (ListenerType)(JSON::Value *response, const TextBuffer &errorMessage);
    auto responseListenerLambda = [=](JSON::Value *response, const TextBuffer &errorMessage) {

        // Request failed (no access token, not logged in),
        if (!response) {
            listener(false, "", "");
            return;
        }

        // Attempt to parse the response,
        try {
            TextBuffer id  ((*response)["id"  ].str().c_str());
            TextBuffer name((*response)["name"].str().c_str());

            // Logged in,
            listener(true, id, name);
        } catch (const std::exception &e) {

            // Not logged in,
            listener(false, "", "");
        }

        delete response;
    };

    shared_ptr<std::function<ListenerType> > responseListener;
    responseListener.manage(new std::function<ListenerType>(responseListenerLambda));

    // Perform the request,
    Facebook::me("id,name", responseListener);
}

void Facebook::login(
        const TextBuffer &appId,
        const TextBuffer &clientToken,
        const TextBuffer &permissions,
        const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener) {

    // Attempt sdk login first,
    // Create a response listener that uses devices login if sdk login is not supported,
    auto responseListenerLambda = [=](const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage) {

        switch (responseType) {
            case FacebookLoginResponseType::FACEBOOK_NOT_INTEGRATED:

                // Initiate the devices login,
                devicesLogin(appId, clientToken, permissions, onResponseReceivedListener);
                return;

            default:
                // Report the result (whatever it is) to the listener,
                if (!onResponseReceivedListener.expired()) {
                    (*onResponseReceivedListener)(responseType, errorMessage);
                }
                return;
        }
    };

    typedef void (ListenerType)(const FacebookLoginResponseType &, const TextBuffer &);
    shared_ptr<std::function<ListenerType> > responseListener;
    responseListener.manage(new std::function<ListenerType>(responseListenerLambda));

    // Attempt the sdk login,
    systemFacebookSdkLogInWithReadPermissions(appId, clientToken, permissions, responseListener);
}

void Facebook::logout() {

    // Invalidate the stored token (enough for devices login),
    javaSetStringPreference(FACEBOOK_ACCESS_TOKEN_PREFERENCE, "");

    // If additional action needed on this platform,
    systemFacebookSdkLogOut();
}

void Facebook::devicesLogin(
                const TextBuffer &appId,
                const TextBuffer &clientToken,
                const TextBuffer &permissions,
                const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener) {

    // Perform the devices login through the login dialog,
    Nongl::startActivity(new FacebookLoginDialog(appId, clientToken, permissions, onResponseReceivedListener));
}

void Facebook::performSimpleGraphApiRequest(
        const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
        Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onResponseReceivedListener) {

    // Check if access token exists,
    TextBuffer accessToken = javaGetStringPreference(FACEBOOK_ACCESS_TOKEN_PREFERENCE, "");
    if (accessToken.isEmpty()) {
        (*onResponseReceivedListener)(nullptr, "No access token");
        return;
    }

    // Add the access token and start an asynchronous request,
    HttpRequestData dataWithToken(data);
    dataWithToken.add("access_token", accessToken);
    new SimpleAsyncRequest(url, httpRequestType, dataWithToken, onResponseReceivedListener);
}

void Facebook::me(
        const TextBuffer &fields,
        Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onResponseReceivedListener) {

    Facebook::performSimpleGraphApiRequest(
            "https://graph.facebook.com/me",
            HttpRequestType::GET,
            HttpRequestData().add("fields", fields),
            onResponseReceivedListener);
}
#endif