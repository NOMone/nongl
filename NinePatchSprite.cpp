#include "NinePatchSprite.h"
#include <stdarg.h>
#include <stdlib.h>

/////////////////////////
// Nine patch data
/////////////////////////

void NinePatchData::setColumnWidths(int columnsCount, bool firstColumnHorizontallyStretchable, ...) {

	this->firstColumnHorizontallyStretchable = firstColumnHorizontallyStretchable;
	columnWidths.clear();

	va_list arguments;
	va_start (arguments, firstColumnHorizontallyStretchable);
	for (int i=0; i<columnsCount; i++) {
	  columnWidths.push_back(va_arg(arguments, int));
	}
	va_end(arguments);
}

void NinePatchData::setRowHeights(int rowsCount, bool firstRowVerticallyStretchable, ...) {

	this->firstRowVerticallyStretchable = firstRowVerticallyStretchable;
	rowHeights.clear();

	va_list arguments;
	va_start (arguments, firstRowVerticallyStretchable);
	for (int i=0; i<rowsCount; i++) {
	  rowHeights.push_back(va_arg(arguments, int));
	}
	va_end(arguments);
}

float NinePatchData::getMinWidth() const {
	float width=0;
	for (int i=0; i<columnWidths.size(); i++) {
		width += columnWidths[i];
	}
	return width;
}

float NinePatchData::getMinHeight() const {
	float height=0;
	for (int i=0; i<rowHeights.size(); i++) {
		height += rowHeights[i];
	}
	return height;
}

/////////////////////////
// Nine patch sprite
/////////////////////////

void NinePatchSprite::set(const NinePatchData *data) {

	this->x = this->y = 0;
	this->scaleX = this->scaleY = 1;
	this->rotation = 0;
	this->colorMask = 0xffffffff;

	this->textureRegion = data->textureRegion;

	this->columnsCount = (int32_t) data->columnWidths.size();
	this->rowsCount = (int32_t) data->rowHeights.size();

	this->firstColumnHorizontallyStretchable = data->firstColumnHorizontallyStretchable;
	this->firstRowVerticallyStretchable = data->firstRowVerticallyStretchable;

	// Allocate and setup regions,
	int regionsCount = columnsCount * rowsCount;
	textureRegions = (Ngl::TextureRegion **) malloc(sizeof(Ngl::TextureRegion *) * regionsCount);

	int virtualTextureLeft = textureRegion->x - textureRegion->offsetX;
	int virtualTextureTop = textureRegion->y - (textureRegion->originalHeight - (textureRegion->height + textureRegion->offsetY));
	int trimmedTextureLeft = textureRegion->x;
	int trimmedTextureTop = textureRegion->y;
	int trimmedTextureRight = trimmedTextureLeft + textureRegion->width;
	int trimmedTextureBottom = trimmedTextureTop + textureRegion->height;

	float currentY = virtualTextureTop;
	int regionIndex = 0;
	for (int j=0; j<rowsCount; j++) {

		bool skipRow = false;
		if ((currentY                       >= trimmedTextureBottom) ||
			(currentY + data->rowHeights[j] <  trimmedTextureTop   )) {
			skipRow = true;
		}

		float currentX = virtualTextureLeft;
		for (int i=0; i<columnsCount; i++) {

			if ((skipRow                                              ) ||
				(currentX                         >= trimmedTextureRight) ||
				(currentX + data->columnWidths[i] <  trimmedTextureLeft )) {
				textureRegions[regionIndex++] = 0;
				continue;
			}

			int regionX = currentX;
			int regionY = currentY;
			int regionWidth = data->columnWidths[i];
			int regionHeight = data->rowHeights[j];
			int regionOffsetX = 0;
			int regionOffsetY = 0;

			if (regionX < trimmedTextureLeft) {
				regionOffsetX = trimmedTextureLeft - regionX;
				regionX = trimmedTextureLeft;
				regionWidth -= regionOffsetX;
			}
			if (regionX + regionWidth > trimmedTextureRight) {
				regionWidth = trimmedTextureRight - regionX;
			}

			if (regionY < trimmedTextureTop) {
				regionHeight -= trimmedTextureTop - regionY;
				regionY = trimmedTextureTop;
			}
			if (regionY + regionHeight > trimmedTextureBottom) {
				regionOffsetY = regionY + regionHeight - trimmedTextureBottom;
				regionHeight -= regionOffsetY;
			}

			textureRegions[regionIndex++] = new Ngl::TextureRegion(
					textureRegion->texture,
					textureRegion->imageName,
					regionX, regionY,
					regionWidth, regionHeight,
					data->columnWidths[i], data->rowHeights[j],
					regionOffsetX, regionOffsetY,
					textureRegion->hasAlpha);

			currentX += data->columnWidths[i];
		}
		currentY += data->rowHeights[j];
	}

	// Calculate minimum and normal dimensions,
	// Width,
	normalWidth = minWidth = 0;
	bool stretchable = firstColumnHorizontallyStretchable;
	for (int i=0; i<columnsCount; i++) {
		normalWidth += data->columnWidths[i];
		if (!stretchable) minWidth += data->columnWidths[i];
		stretchable = !stretchable;
	}
	width = normalWidth;

	// Height,
	normalHeight = minHeight = 0;
	stretchable = firstRowVerticallyStretchable;
	for (int i=0; i<rowsCount; i++) {
		normalHeight += data->rowHeights[i];
		if (!stretchable) minHeight += data->rowHeights[i];
		stretchable = !stretchable;
	}
	height = normalHeight;
}

NinePatchSprite::~NinePatchSprite() {

	int regionsCount = columnsCount * rowsCount;
	for (int i=0; i<regionsCount; i++) {
		if (textureRegions[i]) {
			delete textureRegions[i];
		}
	}
	free(textureRegions);
}

void NinePatchSprite::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly) {

	// TODO: optimize by allocating sprites for regions and setting them only when modified.
	// TODO: use rotation.

	// Initialize sprite,
	sprite.setFromTextureRegion(0);
	sprite.z = getDepth();
	sprite.colorMask = colorMask;

	// Get stretchable dimensions,
	float stretchableWidth = width - minWidth;
	float stretchableHeight = height - minHeight;

	float widthStretchRatio = stretchableWidth / (normalWidth - minWidth);
	float heightStretchRatio = stretchableHeight / (normalHeight - minHeight);

	int currentRegion = 0;
	float currentTop = getTop();

	bool currentRowStretchable = firstRowVerticallyStretchable;
	for (int j=0; j<rowsCount; j++) {

		sprite.scaleY = (currentRowStretchable) ? scaleY * heightStretchRatio : scaleY;

		float currentLeft = getLeft();
		bool currentColumnStretchable = firstColumnHorizontallyStretchable;
		for (int i=0; i<columnsCount; i++) {

			if (textureRegions[currentRegion]) {

				sprite.scaleX = (currentColumnStretchable) ? scaleX * widthStretchRatio  : scaleX;

				sprite.setTextureRegion(textureRegions[currentRegion]);
				sprite.width  = textureRegions[currentRegion]->originalWidth ;
				sprite.height = textureRegions[currentRegion]->originalHeight;

				if (sprite.scaleX && sprite.scaleY &&
					(drawAllInOrder || (textureRegions[currentRegion]->hasAlpha == drawTransparentOnly))) {

					sprite.setLeft(currentLeft);
					sprite.setTop(currentTop);

					sprite.draw(spriteBatch);
				}

				// No, there should never be null texture regions that affect drawing. They
				// are only at the borders and their rows and columns will be skipped. This line
				// is ok,
				currentLeft += sprite.getScaledWidth();
			}

			currentColumnStretchable = !currentColumnStretchable;
			currentRegion++;
		}

		currentRowStretchable = !currentRowStretchable;

		// No, there should never be null texture regions that affect drawing. They
		// are only at the borders and their rows and columns will be skipped. This line
		// is ok,
		currentTop -= sprite.getScaledHeight();
	}
}
