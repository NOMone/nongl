#include "json.parserImplementation.h"

#include <sstream>
#include <stdexcept>

using namespace JSON;

std::string Item::toString() {
    std::stringstream stringStream;
    type(stringStream);
    return stringStream.str();
}

Value &Value::operator[](const std::string &key) {

    Object *object = dynamic_cast<Object *>(this);
    if (!object) throw std::runtime_error("JSON Value is not an Object to be indexed with string");

    return object->get(key);
}

Value &Value::operator[](int32_t index) {

    Array *array = dynamic_cast<Array *>(this);
    if (!array) throw std::runtime_error("JSON Value is not an Array to be indexed with integer");

    return array->get(index);
}

std::string Value::str() {

    String *string = dynamic_cast<String *>(this);
    if (!string) throw std::runtime_error("JSON Value is not a String to use str()");

    return string->value;
}

double Value::num() {

    Number *number = dynamic_cast<Number *>(this);
    if (!number) throw std::runtime_error("JSON Value is not a Number to use num()");

    return number->value;
}

Array &Value::arr() {

    Array *array = dynamic_cast<Array *>(this);
    if (!array) throw std::runtime_error("JSON Value is not an Array to use arr()");

    return *array;
}

void Null::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << "null";
}

void Boolean::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << (value ? "true" : "false");
}

void Number::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << value;
}

void String::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << '"' << value << '"';
}

Array::Array(Item *parent) : Value(parent) {}
Array::~Array() {
    for (int32_t i=(int32_t) values.size()-1; i>-1; i--) delete values[i];
}

void Array::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    int32_t count = (int32_t) values.size();
    int32_t beforeLastIndex = count - 1;
    std::string newIndentation = indentation + indentationStep;
    std::string extraLineFeed1="", extraLineFeed2="";
    if ((!values.empty()) && dynamic_cast<Object *>(values[0])){
        extraLineFeed1 = "\n" + newIndentation;
        extraLineFeed2 = "\n" + indentation;
    }
    stringStream << '[' << extraLineFeed1;
    for (int32_t i=0; i<beforeLastIndex; i++) {
        values[i]->type(stringStream, indentationStep, newIndentation);
        stringStream << ", " << extraLineFeed1;
    }
    if (count) values[beforeLastIndex]->type(stringStream, indentationStep, newIndentation);
    stringStream << extraLineFeed2 << ']';
}

Value &Array::get(int32_t index) {

    if ((index<0) || (index>=values.size())) {
        std::stringstream message;
        message << "JSON Array index " << index << " is out of range";
        throw std::runtime_error(message.str());
    }

    return *values[index];
}


int32_t Array::size() {
    return values.size();
}

Pair::Pair(Item *parent, const std::string &key) : Item(parent), key(key) {}

Pair::~Pair() {
    delete value;
}

void Pair::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    stringStream << '"' << key << "\": ";
    value->type(stringStream, indentationStep, indentation);
}

Object::Object(Item *parent) : Value(parent) {}

Object::~Object() {
    for (int32_t i=(int32_t) pairs.size()-1; i>-1; i--) delete pairs[i];
}

void Object::type(
        std::stringstream &stringStream,
        const std::string &indentationStep, const std::string &indentation) {
    int32_t count = (int32_t) pairs.size();
    int32_t beforeLastIndex = count - 1;
    std::string newIndentation = indentation + indentationStep;
    stringStream << "{\n";
    for (int32_t i=0; i<beforeLastIndex; i++) {
        stringStream << newIndentation;
        pairs[i]->type(stringStream, indentationStep, newIndentation);
        stringStream << ",\n";
    }
    if (count) {
        stringStream << newIndentation;
        pairs[beforeLastIndex]->type(stringStream, indentationStep, newIndentation);
    }
    stringStream << '\n' << indentation << "}";
}

Value &Object::get(const std::string &key) {

    for (int32_t i=(int32_t) pairs.size()-1; i>-1; i--) {
        if (pairs[i]->key == key) return *pairs[i]->value;
    }

    std::stringstream message;
    message << "JSON Object key \"" << key << "\" not found";
    throw std::runtime_error(message.str());
}

ParserImplementation::ParserImplementation() {}

Value *ParserImplementation::finishParsing() {

    // Keep the value on top of the stack,
    Value *parsedValue = nullptr;
    if (!itemsStack.empty()) {
        parsedValue = (Value *) itemsStack.top();
        itemsStack.pop();
    }

    // Delete any remaining values (which are only there due to errors in parsing),
    while (!itemsStack.empty()) {
        delete itemsStack.top();
        itemsStack.pop();
    }

    // Reset everything else,
    currentContainer = nullptr;
    stringTokens.clear();
    errors.clear();
    return parsedValue;
}

std::string JSON::decode(const std::string &text) {

    int32_t length = (int32_t) text.length();
    int32_t lastCharIndex = length-1;

    std::string outputText;
    outputText.reserve(length);

    for (int32_t i=0; i<length; i++) {

        if (text[i] == '\\') {
            if (i == lastCharIndex) {
                throw std::runtime_error("Encountered a lone '\\' at the end of text while decoding");
            }

            i++;
            switch(text[i]) {
                case '"' : outputText += '"' ; break;
                case '\\': outputText += '\\'; break;
                case '/' : outputText += '/' ; break;
                case 'b' : outputText += '\b'; break;
                case 'f' : outputText += '\f'; break;
                case 'n' : outputText += '\n'; break;
                case 'r' : outputText += '\r'; break;
                case 't' : outputText += '\t'; break;
                case 'u' : {
                    int32_t charCode;
                    if (sscanf(&text[i+1], "%04x", &charCode)) {

                        // Sorry, currently we don't have UTF-8 support,
                        outputText += '\254';

                        /*
                        // #include <locale>
                        wchar_t wc = charCode;
                        std::codecvt_utf8<wchar_t> conv;
                        const wchar_t *wnext;
                        char *next;
                        char cbuf[4] = {0}; // initialize the buffer to 0 to have a terminating null
                        std::mbstate_t state;
                        conv.out(state, &wc, &wc + 1, wnext, cbuf, cbuf+4, next);

                        outputText += cbuf;
                        */

                        i += 4;
                        break;
                    } else {
                        throw std::runtime_error("Encountered a malformed unicode character while decoding text");
                    }
                }
                default:
                    throw std::runtime_error("Encountered a non-supported escape character while decoding text");

            }
        } else {
            outputText += text[i];
        }
    }

    return outputText;
}
