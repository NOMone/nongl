#pragma once

#include "json.parserImplementation.h"

namespace JSON {

    class Parser {
        ParserImplementation parser;
        std::vector<std::string> errors;
    public:
        Value *parseInPlace(char *data, int32_t dataSize); // Will modify the data.
                                                           // data should end with 2 zeroes.
                                                           // dataSize should include the trailing zeroes.
                                                           // Remember to delete when finished.

        Value *parse(const char *data, int32_t dataSize); // data will remain intact.
                                                          // No zeroes required at the end of the data.
                                                          // Remember to delete when finished.

        bool logErrors();
    };
}
