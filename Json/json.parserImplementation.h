#pragma once

#include <stdint.h>
#include <string>
#include <vector>
#include <stack>
#include <iosfwd>

namespace JSON {

    class Array;

    class Item {
    public:
        Item *parent;
        Item(Item *parent) : parent(parent) {}
        virtual ~Item() {}
        virtual void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="") = 0;
        std::string toString();
    };

    class Value : public Item {
    public:
        Value(Item *parent) : Item(parent) {}
        Value &operator[](const std::string &key);
        Value &operator[](int32_t index);
        std::string str();
        double num();
        Array &arr();
    };

    class Null : public Value {
    public:
        Null(Item *parent) : Value(parent) {}
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
    };

    class Boolean : public Value {
    public:
        bool value;
        Boolean(Item *parent, bool value) : Value(parent), value(value) {}
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
    };

    class Number : public Value {
    public:
        double value;
        Number(Item *parent, double value) : Value(parent), value(value) {}
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
    };

    class String : public Value {
    public:
        std::string value;
        String(Item *parent, std::string value) : Value(parent), value(value) {}
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
    };

    class Array : public Value {
        friend class ParserImplementation;
        void addValueIgnoringParent(Value *value) {
            values.push_back(value);
        }
        void compactAndInverseArray() {
            // Compact and correct the array order,
            std::vector<Value *> orderedValues;
            int32_t valuesCount = (int32_t) values.size();
            int32_t frontIndex=0, backIndex=valuesCount-1;
            orderedValues.resize(valuesCount);
            while (frontIndex<=backIndex) {
                orderedValues[frontIndex] = values[ backIndex];
                orderedValues[ backIndex] = values[frontIndex];
                frontIndex++; backIndex--;
            }
            values.swap(orderedValues);
        }

    public:
        std::vector<Value *> values;
        Array(Item *parent);
        ~Array();
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
        Value &get(int32_t index);
        Value &operator[](int32_t index) { return get(index); }
        int32_t size();
    };

    class Pair : public Item {
    public:
        std::string key;
        Value *value=nullptr;
        Pair(Item *parent, const std::string &key);
        ~Pair();
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
    };

    class Object : public Value {
        friend class ParserImplementation;
        void addPairIgnoringParent(Pair *pair) {
            pairs.push_back(pair);
        }
        void compactAndInverseArray() {
            // Compact and correct the array order,
            std::vector<Pair *> orderedPairs;
            int32_t pairsCount = (int32_t) pairs.size();
            int32_t frontIndex=0, backIndex=pairsCount-1;
            orderedPairs.resize(pairsCount);
            while (frontIndex<=backIndex) {
                orderedPairs[frontIndex] = pairs[ backIndex];
                orderedPairs[ backIndex] = pairs[frontIndex];
                frontIndex++; backIndex--;
            }
            pairs.swap(orderedPairs);
        }

    public:
        std::vector<Pair *> pairs;
        Object(Item *parent);
        ~Object();
        void type(
                std::stringstream &stringStream,
                const std::string &indentationStep="  ", const std::string &indentation="");
        Value &get(const std::string &key);
        Value &operator[](const std::string &key) { return get(key); }
    };

    class ParserImplementation {
        std::vector<std::string> errors;
        std::vector<std::string> stringTokens;
        std::stack<Item *> itemsStack;
        Item *currentContainer=nullptr;
    public:
        int32_t lineNumber=1;

        ParserImplementation();

        void addError(std::string error) { errors.push_back(error); }
        std::vector<std::string> &getErrors() { return errors; }

        int32_t addStringToken(const char *str) {
            int32_t stringsCount = (int32_t) stringTokens.size();
            for (int32_t i=stringsCount-1; i>-1; i--) {
                if (stringTokens[i].empty()) stringTokens[i] = str;
                return i;
            }
            stringTokens.push_back(str);
            return stringsCount;
        }

        int32_t addStringTokenStripQoutes(const char *str) {
            int32_t tokenIndex = addStringToken(&str[1]);
            stringTokens[tokenIndex].pop_back();
            return tokenIndex;
        }

        std::string consumeStringToken(int32_t tokenIndex) {
            std::string str;
            str.swap(stringTokens[tokenIndex]);
            return str;
        }

        void pushNull() { itemsStack.push(new Null(currentContainer)); }
        void pushBoolean(bool value) { itemsStack.push(new Boolean(currentContainer, value)); }
        void pushNumber(double value) { itemsStack.push(new Number(currentContainer, value)); }
        void pushString(int32_t stringTokenIndex) {
            itemsStack.push(new String(currentContainer, consumeStringToken(stringTokenIndex)));
        }

        void pushEmptyArray() { itemsStack.push(new Array(currentContainer)); }
        void startArray() {
            currentContainer = new Array(currentContainer);
            itemsStack.push(currentContainer);
        }
        void endArray() {
            Array *array = (Array *) currentContainer;
            Item *currentItem = itemsStack.top();
            while (currentContainer!=currentItem) {
                array->addValueIgnoringParent((Value *) currentItem);
                itemsStack.pop();
                currentItem = itemsStack.top();
            }
            array->compactAndInverseArray();
            currentContainer = currentContainer->parent;
        }

        void pushEmptyObject() { itemsStack.push(new Object(currentContainer)); }
        void startObject() {
            currentContainer = new Object(currentContainer);
            itemsStack.push(currentContainer);
        }
        void endObject() {
            Object *object = (Object *) currentContainer;
            Item *currentItem = itemsStack.top();
            while (currentContainer!=currentItem) {
                object->addPairIgnoringParent((Pair *) currentItem);
                itemsStack.pop();
                currentItem = itemsStack.top();
            }
            object->compactAndInverseArray();
            currentContainer = currentContainer->parent;
        }

        void startPair(int32_t keyStringTokenIndex) {
            currentContainer = new Pair(currentContainer, consumeStringToken(keyStringTokenIndex));
            itemsStack.push(currentContainer);
        }
        void endPair() {
            ((Pair *) currentContainer)->value = (Value *) itemsStack.top();
            itemsStack.pop();
            currentContainer = currentContainer->parent;
        }

        Value *finishParsing();
    };

    std::string decode(const std::string &text);
}
