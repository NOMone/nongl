#include "json.h"

#include <Nongl/SystemLog.h>

using namespace JSON;

// Set the api prefix,
#define yy(name) JSON_ ## name
#define YY(name) JSON_ ## name

// Required declarations,
union YY(STYPE);
typedef YY(STYPE) YYSTYPE;
#include "json.lexer.h"

// Parser declarations,
int yy(parse) (yyscan_t scanner, JSON::ParserImplementation *parser);

Value *Parser::parseInPlace(char *data, int32_t dataSize) {

    yyscan_t scanner;

    // Create a pointer to scanner global data,
    yy(lex_init_extra)(&parser, &scanner);

    // Set Flex to read from buffer,
    yy(_scan_buffer)(data, dataSize, scanner);

    // Parse!
    yy(parse)(scanner, &parser);

    // Delete the scanner's global data,
    yy(lex_destroy)(scanner);

    // Keep the parser errors,
    errors.swap(parser.getErrors());

    // Return the parsed JSON,
    return parser.finishParsing();
}

Value *Parser::parse(const char *data, int32_t dataSize) {

    yyscan_t scanner;

    // Allocate memory for the mutable parsing buffer,
    char *tempData = (char *) malloc(dataSize+2); // Account for two additional zeroes.
    memcpy(tempData, data, dataSize);

    // Terminate the data with 2 zeroes,
    tempData[dataSize  ] = 0;
    tempData[dataSize+1] = 0;

    // Create a pointer to scanner global data,
    yy(lex_init_extra)(&parser, &scanner);

    // Set Flex to read from buffer,
    yy(_scan_buffer) (tempData, dataSize+2, scanner);

    // Parse!
    yy(parse)(scanner, &parser);

    // Delete the buffer,
    free(tempData);

    // Delete the scanner's global data,
    yy(lex_destroy)(scanner);

    // Keep the parser errors,
    errors.swap(parser.getErrors());

    // Return the parsed JSON,
    return parser.finishParsing();
}

bool Parser::logErrors() {

    for (int32_t i=0; i<errors.size(); i++) {
       LOGE("%s.", errors[i].c_str());
    }
    return !errors.empty();
}

#include <sstream>

void yy(error)(yyscan_t scanner, ParserImplementation *parser, const char *s) {

    // Commenting out this single liner since android NDK doesn't support std::to_string at the moment,
    // parser->addError(std::string("Parse error! Line ") + std::to_string(parser->lineNumber) + ": " + s);

    std::stringstream message;
    message << "Parse error! Line " << parser->lineNumber << ": " << s;
    parser->addError(message.str());
}
