#include "GestureRecorder.h"
#include "utils.h"
#include "ByteBuffer.h"
#include "TextUtils.h"
#include "File.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//////////////////////////////
// Gestures
//////////////////////////////

void TouchDownGesture::toString(char *outString) {
	sprintf(outString, "type: TouchDown\n time: %f\n pointerId: %d\n xy: %f, %f\n", time, pointerId, x, y);
}

void TouchDragGesture::toString(char *outString) {
	sprintf(outString, "type: TouchDrag\n time: %f\n pointerId: %d\n downXY: %f, %f\n xy: %f, %f\n maxDraggedDistanceCms: %f\n", time, pointerId, downX, downY, x, y, maxDraggedDistanceCms);
}

void TouchDragEndGesture::toString(char *outString) {
	sprintf(outString, "type: TouchDragEnd\n time: %f\n pointerId: %d\n downXY: %f, %f\n upXY: %f, %f\n maxDraggedDistanceCms: %f\n", time, pointerId, downX, downY, upX, upY, maxDraggedDistanceCms);
}

void TouchUpGesture::toString(char *outString) {
	sprintf(outString, "type: TouchUp\n time: %f\n pointerId: %d\n xy: %f, %f\n maxDraggedDistanceCms: %f\n", time, pointerId, x, y, maxDraggedDistanceCms);
}

void ClickGesture::toString(char *outString) {
	sprintf(outString, "type: Click\n time: %f\n pointerId: %d\n xy: %f, %f\n", time, pointerId, x, y);
}

void FlickGesture::toString(char *outString) {
	sprintf(outString, "type: Flick\n time: %f\n pointerId: %d\n xy: %f, %f\n magnitude: %f, %f\n", time, pointerId, x, y, xMagnitude, yMagnitude);
}

void ZoomGesture::toString(char *outString) {
	sprintf(outString, "type: Zoom\n time: %f\n xy: %f, %f\n ratio: %f\n", time, x, y, ratio);
}

void ZoomEndGesture::toString(char *outString) {
	sprintf(outString, "type: ZoomEnd\n time: %f\n xy: %f, %f\n ratio: %f\n", time, x, y, ratio);
}

//////////////////////////////
// Gesture recorder
//////////////////////////////

//////////////////////////////
// Gesture recording and saving
//////////////////////////////

GestureRecorder::GestureRecorder() {
	resetRecording();

	gestureListener = 0;
	resetReplaying();

	this->onReplayEndListener = 0;
	this->onReplayEndListenerData = 0;
}

GestureRecorder::~GestureRecorder() {
	removeRecordedGestures();
}

void GestureRecorder::updateRecording() {

	if (recordingRunning) {
		double currentTime = getTimeMillis();
		recordingTotalTimeMillis += currentTime - recordingLastTimeMillis;
		recordingLastTimeMillis = currentTime;
	}
}

void GestureRecorder::removeRecordedGestures() {
 
	int32_t gesturesCount = (int32_t) recordedGestures.size() - 1;
	for (; gesturesCount>-1; gesturesCount--) {
		delete (Gesture *) recordedGestures[gesturesCount];
	}

	recordedGestures.clear();
}

void GestureRecorder::resetRecording() {
	recordingTotalTimeMillis = 0;
	recordingLastTimeMillis = 0;
	recordingRunning = false;
	removeRecordedGestures();
}

void GestureRecorder::startRecording() {
	recordingLastTimeMillis = getTimeMillis();
	recordingRunning = true;
}

void GestureRecorder::pauseRecording() {
	updateRecording();
	recordingRunning = false;
}

void GestureRecorder::saveRecording(const Ngl::File &file) {

	updateRecording();

	Ngl::ByteBuffer data;

	char currentGestureString[2048];

	int32_t gesturesCount = (int32_t) recordedGestures.size();
	for (int i=0; i < gesturesCount; i++) {

		Gesture *currentGesture = (Gesture *) recordedGestures[i];
		currentGesture->toString(currentGestureString);

		data.pushData(currentGestureString, (int32_t) strlen(currentGestureString));
		data.pushChar('\n');
	}

	data.pushChar(0);

	file.write((char *) data.getData(), data.getDataSize(), false);
}

void GestureRecorder::scaleRecording(float scaleX, float scaleY) {

	int32_t gesturesCount = (int32_t) recordedGestures.size();
	for (int i=0; i < gesturesCount; i++) {
		Gesture *currentGesture = (Gesture *) recordedGestures[i];
		currentGesture->scale(scaleX, scaleY);
	}
}

//////////////////////////////
// Recording loading and replaying
//////////////////////////////

Gesture *GestureRecorder::createGesture(
		char *gestureType,
		float time,
		int pointerId,
		float x, float y,
		float downX, float downY,
		float upX, float upY,
		float maxDraggedDistanceCms,
		float xMagnitude, float yMagnitude,
		float ratio) {

	if (!strcasecmp(gestureType, "TouchDown"))
		return new TouchDownGesture(time, pointerId, x, y);
	else if (!strcasecmp(gestureType, "TouchDrag"))
		return new TouchDragGesture(time, pointerId, downX, downY, x, y, maxDraggedDistanceCms);
	else if (!strcasecmp(gestureType, "TouchDragEnd"))
		return new TouchDragEndGesture(time, pointerId, downX, downY, upX, upY, maxDraggedDistanceCms);
	else if (!strcasecmp(gestureType, "TouchUp"))
		return new TouchUpGesture(time, pointerId, x, y, maxDraggedDistanceCms);
	else if (!strcasecmp(gestureType, "Click"))
		return new ClickGesture(time, pointerId, x, y);
	else if (!strcasecmp(gestureType, "Flick"))
		return new FlickGesture(time, pointerId, x, y, xMagnitude, yMagnitude);
	else if (!strcasecmp(gestureType, "Zoom"))
		return new ZoomGesture(time, x, y, ratio);
	else if (!strcasecmp(gestureType, "ZoomEnd"))
		return new ZoomEndGesture(time, x, y, ratio);

	return 0;
}

bool GestureRecorder::loadRecording(const Ngl::File &file) {

	// Get recording data,
	std::vector<uint8_t> recordingData;
	int32_t dataSize = file.read(recordingData);

	// Remove any previous recording data,
	resetRecording();

	// Data we need to collect,
	char gestureTypeBuffer1[256];
	char gestureTypeBuffer2[256];
	char *currentGestureType = gestureTypeBuffer1;
	char *nextGestureType = gestureTypeBuffer2;

	float time=0;
	int pointerId;
	float x, y;
	float downX, downY;
	float upX, upY;
	float maxDraggedDistanceCms;
	float xMagnitude, yMagnitude;
	float ratio;

	// State information,
	float maxTime=0;

	char currentLine[1024];
	int currentLocation=0;

	bool processingGesture = false;

	do {

		int charsCount;
		int eof=0;
		do {

			// Check if end of file,
			if (currentLocation >= dataSize) {
				eof = 1;
				break;
			}

			// Read the next line,
			charsCount = readLine((char *) &recordingData[currentLocation], currentLine);
			currentLocation += charsCount;

		} while (strlen(trim(currentLine)) == 0);

		if (eof) {

			// No more processing, save any gesture that is currently being processed,
			if (processingGesture) {

				// Save this gesture,
				Gesture *newGesture = createGesture(
						currentGestureType,
						time,
						pointerId,
						x, y,
						downX, downY,
						upX, upY,
						maxDraggedDistanceCms,
						xMagnitude, yMagnitude,
						ratio);

				if (newGesture) {
					recordedGestures.push_back(newGesture);
					if (time > maxTime) maxTime = time;
				}
			}

			break;
		}

		// Check for new gesture,
		removeWhitespaces(currentLine);
		if (parseLine(currentLine, "type:", "type:%s", nextGestureType)) {

			if (processingGesture) {

				// Save this gesture,
				Gesture *newGesture = createGesture(
						currentGestureType,
						time,
						pointerId,
						x, y,
						downX, downY,
						upX, upY,
						maxDraggedDistanceCms,
						xMagnitude, yMagnitude,
						ratio);

				if (newGesture) {
					recordedGestures.push_back(newGesture);
					if (time > maxTime)
						maxTime = time;
				}
			}

			processingGesture = true;

			char *temp = nextGestureType;
			nextGestureType = currentGestureType;
			currentGestureType = temp;
		}
		else if (parseLine(currentLine, "time:"                 , "time:%f"                 , &time                   )) {}
		else if (parseLine(currentLine, "pointerId:"            , "pointerId:%d"            , &pointerId              )) {}
		else if (parseLine(currentLine, "xy:"                   , "xy:%f,%f"                , &x, &y                  )) {}
		else if (parseLine(currentLine, "downXY:"               , "downXY:%f,%f"            , &downX, &downY          )) {}
		else if (parseLine(currentLine, "upXY:"                 , "upXY:%f,%f"              , &upX, &upY              )) {}
		else if (parseLine(currentLine, "maxDraggedDistanceCms:", "maxDraggedDistanceCms:%f", &maxDraggedDistanceCms  )) {}
		else if (parseLine(currentLine, "magnitude:"            , "magnitude:%f,%f"         , &xMagnitude, &yMagnitude)) {}
		else if (parseLine(currentLine, "ratio:"                , "ratio:%f"                , &ratio                  )) {}
		else {
			//LOGE("Found undefined: %s", currentLine);
		}
	} while (1);

	recordingTotalTimeMillis = maxTime;

	return true;
}

void GestureRecorder::resetReplaying() {
	replayingTotalTimeMillis = 0;
	gestureToReplayIndex = 0;
	replayingRunning = 0;
}

void GestureRecorder::startReplaying() {
	replayingRunning = true;
}

void GestureRecorder::updateReplaying(float elapsedTimeMillis) {

	if (replayingRunning) {

		replayingTotalTimeMillis += elapsedTimeMillis;

		while ((gestureToReplayIndex < recordedGestures.size()) &&
			   (((Gesture *) recordedGestures[gestureToReplayIndex])->time <= replayingTotalTimeMillis)) {

			if (gestureListener) {
				((Gesture *) recordedGestures[gestureToReplayIndex])->setAndDispatchEvent(&touchEvent, gestureListener);
			}

			gestureToReplayIndex++;
		}

		// Call the on replay end listener,
		if (gestureToReplayIndex == recordedGestures.size()) {
			replayingRunning = false;
			if (onReplayEndListener) onReplayEndListener->run(onReplayEndListenerData);
		}
	}
}

void GestureRecorder::pauseReplaying() {
	updateReplaying(0);
	replayingRunning = false;
}

//////////////////////////////
// Gesture events
//////////////////////////////

bool GestureRecorder::onTouchEvent(const TouchEvent *event) {

	if (!recordingRunning)
		return false;

	updateRecording();

	Gesture *newGesture;

	switch (event->type) {
		case TouchEvent::TOUCH_DOWN:
			newGesture = new TouchDownGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::TOUCH_DRAG:
			newGesture = new TouchDragGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::TOUCH_DRAG_END:
			newGesture = new TouchDragEndGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::TOUCH_UP:
			newGesture = new TouchUpGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::CLICK:
			newGesture = new ClickGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::FLICK:
			newGesture = new FlickGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::ZOOM:
			newGesture = new ZoomGesture(recordingTotalTimeMillis, event);
			break;
		case TouchEvent::ZOOM_END:
			newGesture = new ZoomEndGesture(recordingTotalTimeMillis, event);
			break;

		default:
			break;
	}

	recordedGestures.push_back(newGesture);

	return false;
}
