#pragma once

#include <list>

class UniqueId {
	int currentId;
	int remainingIds;
	int maxId;

	std::list<int> usedIds;

	bool isIdOccupied(int id);
	bool findRemainingIds();

public:

	UniqueId(int maxId);

	int getNewUniqueId();
	void removeId(int id);
};
