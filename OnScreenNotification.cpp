#include <Nongl/OnScreenNotification.h>

#include <Nongl/Theme.h>
#include <Nongl/LinearLayout.h>
#include <Nongl/ImageView.h>
#include <Nongl/RotatingRaysView.h>
#include <Nongl/TextView.h>

using namespace Ngl;

///////////////////////////////
// Notification data
///////////////////////////////

OnScreenNotificationData::OnScreenNotificationData(
			const TextBuffer &text,
			const Ngl::TextureRegion *icon,
			float durationMillis, float delayMillis) {

	this->text.append(text);
	this->icon = icon;
	this->durationMillis = durationMillis;
	this->delayMillis = delayMillis;
}

OnScreenNotificationData &OnScreenNotificationData::setActivityName(const TextBuffer &activityName) {
    this->activityName.clear();
    this->activityName.append(activityName);
	return *this;
}

OnScreenNotificationData &OnScreenNotificationData::setDelayBeforeShowingDismissButton(float delayMillis, bool hasDismissButton) {
	this->delayBeforeShowingDismissButtonMillis = delayMillis;
	this->hasDismissButton = hasDismissButton;
	return *this;
}

OnScreenNotificationData &OnScreenNotificationData::setOnEndTask(
		const Ngl::ManagedPointer<Runnable> &onNotificationEndTask, void *onNotificationEndTaskData) {
	this->onNotificationEndTask = onNotificationEndTask;
	this->onNotificationEndTaskData = onNotificationEndTaskData;
	return *this;
}

OnScreenNotificationData &OnScreenNotificationData::setOnTouchedTask(
		const Ngl::ManagedPointer<Runnable> &onNotificationTouchedTask, void *onNotificationTouchedTaskData) {
	this->onNotificationTouchedTask = onNotificationTouchedTask;
	this->onNotificationTouchedTaskData = onNotificationTouchedTaskData;
	return *this;
}

///////////////////////////////
// Notification dialog
///////////////////////////////

#define PADDING_TOP 50
#define HORIZONTAL_PADDING 5

OnScreenNotification::OnScreenNotification(const OnScreenNotificationData &notificationData) :
        DialogActivity(notificationData.activityName.getConstText()) {

	setConsumesAllInputs(false);

    // Setup events and listeners,
	this->onNotificationEndTask = notificationData.onNotificationEndTask;
	this->onNotificationEndTaskData = notificationData.onNotificationEndTaskData;
    this->onNotificationTouchedTask.task = notificationData.onNotificationTouchedTask;
    this->onNotificationTouchedTask.taskData = notificationData.onNotificationTouchedTaskData;
    this->onDismissButtonTouchTask.notificationToDismiss = this;
    
    setOnSurfaceCreatedListener(notificationData.onActivitySurfaceCreatedListener);
    setOnResumeListener(notificationData.onActivityResumeListener);
    setOnDestroyListener(notificationData.onActivityDestroyListener);

	// Create UI,
	// rootLayout
	// -->dialogAlignmentLayout
	// -->-->dialogLayout
	// -->-->-->blackOverlayBackground
    // -->-->-->touchView
	// -->-->-->centeringLayout
	// -->-->-->-->dialogText
	// -->-->-->-->icon
    // -->-->-->dismissButtonLayout
    // -->-->-->-->dismissButtonCircle1
    // -->-->-->-->dismissButtonCircle2
    // -->-->-->-->dismissButtonX
    // -->-->-->-->dismissButtonTouchableView

    Theme &theme = Theme::getGlobalTheme();

    // Dialog icon,
    ImageView *icon=0;
    if (notificationData.icon) icon = new ImageView(notificationData.icon);
    
	// Dialog text,
    TextView *dialogText=0;
    if (!notificationData.text.isEmpty()) {
        float maxTextWidth = rootLayout.getWidth() - theme.getDialogLayoutPadding();
        if (icon) maxTextWidth -= (icon->getWidth() + HORIZONTAL_PADDING);
        dialogText = theme.createNotificationText(notificationData.text.getConstText(), maxTextWidth);
    }

    // Dialog layout,
	LinearLayout *dialogAlignmentLayout = rootLayout.addMatchingLinearLayout(Orientation::VERTICAL, Gravity::TOP_CENTER);
	dialogAlignmentLayout->setHeight(dialogAlignmentLayout->getHeight()-PADDING_TOP);

    // Get dialog dimensions to wrap content,
    float dialogLayoutWidth = 0;
    float dialogLayoutHeight = 0;
    if (dialogText) {
        dialogLayoutWidth  = dialogText->getWidth ();
        dialogLayoutHeight = dialogText->getHeight();
    }
    if (icon) {
        dialogLayoutWidth += icon->getWidth();
        if (dialogText) dialogLayoutWidth += HORIZONTAL_PADDING;
        
        dialogLayoutHeight = (dialogLayoutHeight > icon->getHeight()) ? dialogLayoutHeight : icon->getHeight();
    }

    dialogLayoutWidth  += theme.getDialogLayoutPadding();
	dialogLayoutHeight += theme.getDialogLayoutPadding();
    
    // Create dialog layout,
	dialogLayout = theme.createNotificationLayout(dialogLayoutWidth, dialogLayoutHeight);
	dialogAlignmentLayout->addView(dialogLayout);

    // Touchable view,
    TouchableView *touchView = new TouchableView(
            0, 0, dialogLayout->getWidth(), dialogLayout->getHeight(),
            &this->onNotificationTouchedTask);
    dialogLayout->addView(touchView);
    
    // Linear layout to arrange icon and text horizontally,
	LinearLayout *centeringLayout = dialogLayout->addMatchingLinearLayout(Orientation::HORIZONTAL, Gravity::CENTER);
	centeringLayout->setPadding(HORIZONTAL_PADDING);
    if (icon) centeringLayout->addView(icon);
	if (dialogText) centeringLayout->addView(dialogText);

	// Dismiss button,
	if (notificationData.hasDismissButton) {

		#define DISMISS_BUTTON_RADIUS 30

		// Layout to hold the button layers,
		dismissButtonLayout = new AbsoluteLayout(
				dialogLayout->getWidth() - DISMISS_BUTTON_RADIUS, dialogLayout->getHeight() - DISMISS_BUTTON_RADIUS,
				DISMISS_BUTTON_RADIUS*2, DISMISS_BUTTON_RADIUS*2);

		// Background,
		CircleView *dismissButtonCircle1 = new CircleView(DISMISS_BUTTON_RADIUS, DISMISS_BUTTON_RADIUS, DISMISS_BUTTON_RADIUS, 0xffbdbdbd, 20);
		CircleView *dismissButtonCircle2 = new CircleView(DISMISS_BUTTON_RADIUS, DISMISS_BUTTON_RADIUS, DISMISS_BUTTON_RADIUS-4, 0xff3434cb, 20);

		// X mark,
		TextView *dismissButtonX = new TextView(1, 3, dismissButtonLayout->getWidth(), dismissButtonLayout->getHeight(), Gravity::CENTER, 0xffffffff);
		dismissButtonX->setTextScale(0.5f, 0.5f);
		dismissButtonX->setText("x");

		// View to catch clicks,
		TouchableView *dismissButtonTouchableView = new TouchableView(
				-40, -40, dismissButtonLayout->getWidth()+80, dismissButtonLayout->getHeight()+80, &onDismissButtonTouchTask);

		// Add everything,
		dismissButtonLayout->addView(dismissButtonCircle1);
		dismissButtonLayout->addView(dismissButtonCircle2);
		dismissButtonLayout->addView(dismissButtonX);
		dismissButtonLayout->addView(dismissButtonTouchableView);
		dialogLayout->addView(dismissButtonLayout);

		// Schedule visibility,
		if (notificationData.delayBeforeShowingDismissButtonMillis > 0) {
			dismissButtonLayout->setEnabled(false);
			dismissButtonLayout->setVisible(false);

			Nongl::scheduler.schedule([] (void *data) -> bool {
				((OnScreenNotification *) data)->showDismissButton();
				return true;
			}, notificationData.delayBeforeShowingDismissButtonMillis, this, this);
		}
	}

	rootLayout.layout();

	// Entrance animation,
	Animation *entranceAnimation = new Animation();
	entranceAnimation->addComponent(
			AnimationTarget::Y,
			ElasticOutInterpolator(1000, notificationData.delayMillis).setDamping(1.0f),
			dialogLayout->getHeight()+PADDING_TOP, 0, true,
			0, 0, true);
	animationController.addAnimation(entranceAnimation);
	dialogLayout->attachToAnimation(entranceAnimation);
	entranceAnimation->start();
	dialogLayout->setBottom(dialogLayout->getBottom()+dialogLayout->getHeight()+PADDING_TOP);

	Nongl::scheduler.schedule(
			[](void *data) -> bool {
				((OnScreenNotification *) data)->dismiss();
				return true;
			}, notificationData.delayMillis+notificationData.durationMillis+250, this, this);
}

bool OnScreenNotification::OnNotificationTouchTask::run(void *data) {
    if ((!task.expired()) && (((TouchEvent *) data)->type == TouchEvent::TOUCH_DOWN)) task->run(taskData);
    return true;
}

bool OnScreenNotification::OnDismissButtonTouchTask::run(void *data) {
    if (((TouchEvent *) data)->type == TouchEvent::CLICK) notificationToDismiss->dismiss();
	return true;
}

const Animation *OnScreenNotification::getEntranceAnimation() {
	static Animation animation;
	return &animation;
}

const Animation *OnScreenNotification::getExitAnimation() {
	static Animation animation;
	return &animation;
}

void OnScreenNotification::setOnTouchedTask(Ngl::ManagedPointer<Runnable> onNotificationTouchedTask, void *onNotificationTouchedTaskData) {
    this->onNotificationTouchedTask.task = onNotificationTouchedTask;
    this->onNotificationTouchedTask.taskData = onNotificationTouchedTaskData;
}

void OnScreenNotification::dismiss() {

	if (dismissed) return;
	dismissed = true;

	// Exit animation,
	Animation *exitAnimation = new Animation();
	exitAnimation->addComponent(
			AnimationTarget::Y,
			ElasticInOutInterpolator(1000),
			0, 0, true,
			dialogLayout->getHeight()+PADDING_TOP+25, 0, true);
	animationController.addAnimation(exitAnimation);
	dialogLayout->attachToAnimation(exitAnimation);
	exitAnimation->start();

	// Finish the activity when the animation is over,
	Ngl::shared_ptr<Runnable> finishActivityTask(Runnable::createRunnable([](void *data) -> bool {
		((Activity *) data)->finish();
		return true;
	}, this));
	exitAnimation->setOnAnimationEndTask(finishActivityTask, this);

	// Call on end task,
    if (!onNotificationEndTask.expired()) {
        //onNotificationEndTask->run(onNotificationEndTaskData);
        Nongl::scheduler.schedule(onNotificationEndTask, 500, onNotificationEndTaskData);
    }
}

void OnScreenNotification::showDismissButton() {

	if (dismissButtonLayout) {
		dismissButtonLayout->setEnabled(true);
		dismissButtonLayout->setVisible(true);

		// Animation,
		Animation *animation = new Animation();
		animation->addComponent(
				AnimationTarget::COLOR_MASK, Interpolator(250),
				0, 0x00ffffff, false,
				0, 0xffffffff, true);
		dismissButtonLayout->attachToAnimation(animation);
		animationController.addAnimation(animation);
		animation->start();
	}
}

static std::deque<OnScreenNotificationData> queuedNotifications;
void OnScreenNotification::queueOnScreenNotification(const OnScreenNotificationData &notificationData) {

	if (notificationData.delayMillis) {

		// Queue the notification after the delay,
		class QueueNotificationDelayed : public Runnable {
			OnScreenNotificationData notificationData;
		public:

			QueueNotificationDelayed(const OnScreenNotificationData &notificationData) : notificationData(notificationData) {
				this->notificationData.delayMillis = 0;
			}
			bool run(void *data) {
				OnScreenNotification::queueOnScreenNotification(notificationData);
				return true;
			}
		};

		Nongl::scheduler.schedule(Ngl::shared_ptr<Runnable>(new QueueNotificationDelayed(notificationData)));

	} else {

		class ShowNextNotificationTask : public Runnable {
			Ngl::ManagedPointer<Runnable> runnable;
			void *data;
		public:
			ShowNextNotificationTask(const Ngl::ManagedPointer<Runnable> &runnable, void *data) : runnable(runnable), data(data) { }
			bool run(void *data) {
				if (!runnable.expired()) runnable->run(this->data);
				OnScreenNotification::showNextOnScreenNotification();
				return true;
			}
		};

		// Queue the notification,
		Ngl::shared_ptr<Runnable> showNextNotificationTask(new ShowNextNotificationTask(
				notificationData.onNotificationEndTask,
				notificationData.onNotificationEndTaskData));

		queuedNotifications.push_back(
				OnScreenNotificationData(notificationData).
				setOnEndTask(showNextNotificationTask));

		// If this is the only notification queued, show it immediately,
		if (queuedNotifications.size() == 1) {
			Activity *notificationDialog = new OnScreenNotification(queuedNotifications.front());
			Nongl::startActivity(notificationDialog);
			Nongl::setActivityAlwaysOnTop(notificationDialog);
		}
	}
}

void OnScreenNotification::queueOnScreenNotification(const char *text, const Ngl::TextureRegion *icon, float durationMillis, float delayMillis) {
	queueOnScreenNotification(OnScreenNotificationData(text, icon, durationMillis, delayMillis));
}

void OnScreenNotification::showNextOnScreenNotification() {

	// Remove last notification,
	if (!queuedNotifications.empty()) queuedNotifications.pop_front();

	// Show current notification,
	if (!queuedNotifications.empty()) {
		Activity *notificationDialog = new OnScreenNotification(queuedNotifications.front());
		Nongl::startActivity(notificationDialog);
		Nongl::setActivityAlwaysOnTop(notificationDialog);
	}
}

bool OnScreenNotification::notificationDisplayedOrQueued(const TextBuffer &notificationActivityName) {

    // Check if notification is already displayed,
    if (Nongl::getActivity(notificationActivityName.getConstText())) return true;

    // Check if notification is queued,
    for(auto i=queuedNotifications.begin(); i!=queuedNotifications.end(); i++) {
        if ((*i).activityName == notificationActivityName) return true;
    }

    // Not displayed nor queued,
    return false;
}
