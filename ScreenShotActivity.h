#pragma once

#include "DialogActivity.h"
#include "Button.h"

class FrameBuffer;
class PixelMap;

namespace Ngl {

    class ScreenShotActivity : public DialogActivity, ButtonListener {
        int screenShotWidth, screenShotHeight;
        FrameBuffer *frameBuffer=0;
        PixelMap *pixelMap;
        Button *captureButton;
    public:

        static const char *activityName;
        ScreenShotActivity(int screenShotWidth=0, int screenShotHeight=0);
        ~ScreenShotActivity();

        void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);

        void buttonOnClick(Button *button);
    };
}
