#include "AbsoluteLayout.h"
#include "DisplayManager.h"
#include "SpriteBatch.h"
#include "utils.h"

using namespace Ngl;

void AbsoluteLayout::init(float x, float y, float width, float height, bool deletesChildrenOnDestruction) {

	this->x = x;
	this->y = y;
	this->z = 0;
	this->width = width;
	this->height = height;
	this->scaleX = this->scaleY = 1;
	this->colorMask = 0xffffffff;
	this->deletesChildrenOnDestruction = deletesChildrenOnDestruction;
	this->clipping = false;
}

void AbsoluteLayout::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly) {

	// Check color mask,
	int oldColorMask = spriteBatch->colorMask;
	int newColorMask = multiplyColors(oldColorMask, colorMask);
	int colorMaskAlpha = newColorMask & 0xff000000;
	if (!colorMaskAlpha) return; // Totally transparent, nothing to draw here...

	// Flush anything previously drawn so it won't be affected by changes,
	spriteBatch->flush();

	// Save current settings,
	// Viewport transformation,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	displayManager->pushViewPortTransformation();

	// Clipping,
	bool clipped = clipping;   // Buffer the value of clipping,
	if (clipped) {
		displayManager->pushClipping();

		// Adjust clipping,
		displayManager->setClipping(getLeft(), getBottom(), getScaledWidth(), getScaledHeight());
	}

	// Adjust view port,
	displayManager->viewPortTransformation.transformationX = (displayManager->viewPortTransformation.transformationX + getLeft()) / getScaleX();
	displayManager->viewPortTransformation.transformationY = (displayManager->viewPortTransformation.transformationY + getBottom()) / getScaleY();
	displayManager->viewPortTransformation.transformationZ += z;
	displayManager->viewPortTransformation.transformationWidth /= getScaleX();
	displayManager->viewPortTransformation.transformationHeight /= getScaleY();

	// Adjust color mask,
	spriteBatch->colorMask = newColorMask;

	// Draw children,
	if (drawAllInOrder) {
		Layout::drawAllInOrder(spriteBatch);
	} else if (drawTransparentOnly) {
		if (colorMaskAlpha == 0xff000000) {
			Layout::drawTransparentParts(spriteBatch);
		} else {
			Layout::drawAllInOrder(spriteBatch);
		}
	} else {
		if (colorMaskAlpha == 0xff000000) Layout::drawOpaqueParts(spriteBatch);
	}

	// Flush everything before restoring old settings,
	spriteBatch->flush();

	// Restore settings,
	spriteBatch->colorMask = oldColorMask;

	displayManager->popViewPortTransformation();
	if (clipped) displayManager->popClipping();
}

Ngl::Rect AbsoluteLayout::transformParentRectToLocalRect(const Ngl::Rect &rect) {
	return Rect(
			(rect.left   - getLeft  ()) / scaleX,
			(rect.bottom - getBottom()) / scaleY,
			(rect.right  - getLeft  ()) / scaleX,
			(rect.top    - getBottom()) / scaleY);
}

void AbsoluteLayout::adjustDepths(float maxZ, float minZ) {

	this->z = maxZ;

	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (!childrenViewsCount) return ;
	if (childrenViewsCount==1) {
		childrenViews[0]->adjustDepths(0, minZ-maxZ);
	} else {

		int depthLayersCount = getDepthLayersCount();
		if (!depthLayersCount) return ;

		float depthRange = maxZ - minZ;
		float totalPadding = depthRange / 100;
		float childPadding = totalPadding / (childrenViewsCount-1);
		float layerRange = (depthRange - totalPadding) / depthLayersCount;

		maxZ = 0;   // Because depth is relative in this layout.

		for (int i=0; i<childrenViewsCount; i++) {
			float currentChildRange = layerRange * childrenViews[i]->getDepthLayersCount();
			childrenViews[i]->adjustDepths(maxZ, maxZ-currentChildRange);
			maxZ -= currentChildRange + childPadding;
		}
	}
}

bool AbsoluteLayout::onTouchEvent(const TouchEvent *event) {

	if (!isEnabled()) return false;

	// If clipping, check if hit within boundaries before sending to children,
	if (clipping) {
		if ((event->downX < getLeft  ()) || (event->downX >= getRight()) ||
			(event->downY < getBottom()) || (event->downY >= getTop  ())) {
			return false;
		}
	}

	TouchEvent transformedTouchEvent(*event);
	transformedTouchEvent.x -= getLeft();
	transformedTouchEvent.y -= getBottom();
	transformedTouchEvent.downX -= getLeft();
	transformedTouchEvent.downY -= getBottom();

	transformedTouchEvent.x /= scaleX;
	transformedTouchEvent.y /= scaleY;
	transformedTouchEvent.downX /= scaleX;
	transformedTouchEvent.downY /= scaleY;

	return Layout::onTouchEvent(&transformedTouchEvent);
}
