#include "GestureListenersGroup.h"

#include <stdint.h>

void GestureListenersGroup::addGestureListener(GestureListener *gestureListener) {
	gestureListeners.push_back(gestureListener);
}

void GestureListenersGroup::removeGestureListener(GestureListener *gestureListener) {

	for (int i=0; i<gestureListeners.size(); i++) {
		if (gestureListeners[i] == gestureListener) {
			gestureListeners.erase(gestureListeners.begin() + i);
			return ;
		}
	}
}

bool GestureListenersGroup::onTouchEvent(const TouchEvent *event) {

    int32_t gestureListenersCount = (int32_t) gestureListeners.size();
	bool consumed = false;
	for (int i=0; i<gestureListenersCount; i++) {
		consumed |= gestureListeners[i]->onTouchEvent(event);
	}
	return consumed;
}
