#include "Font.h"
#include "DisplayManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_FONT_NAME "NONGL.default.font"

Font *Font::defaultFont = 0;

Font::Font(const char *fontName) {

	this->characterSpacing = 0;

	DisplayManager *displayManager = DisplayManager::getSingleton();
	char *currentCharacterName = (char *) alloca(strlen(fontName) + 10);

	lineHeight = 0;
	for (int i=0; i<FONT_CHARACTERS_COUNT; i++) {
		sprintf(currentCharacterName, "%s.%d", fontName, i);
		characterRegions[i] = displayManager->getTextureRegion(currentCharacterName, false);
		if (characterRegions[i] && (characterRegions[i]->originalHeight > lineHeight)) {
			lineHeight = characterRegions[i]->originalHeight;
		}
	}
}

Font *Font::getDefaultFont() {
	if (!defaultFont) {
		defaultFont = new Font(DEFAULT_FONT_NAME);
		defaultFont->characterSpacing = 0;
	}
	return defaultFont;
}

Ngl::Texture *Font::getTexture() {

	for (int i=0; i<FONT_CHARACTERS_COUNT; i++) {
		if (characterRegions[i]) return characterRegions[i]->texture;
	}

	return 0;
}

int Font::getTextWidth(const char* text) {

	float maxLineWidth = 0;
	float currentLineWidth = 0;
	for (int i=0; text[i]; i++) {

		if (text[i]=='\n') {
			if (currentLineWidth > maxLineWidth) maxLineWidth = currentLineWidth;
		} else {
			currentLineWidth += getCharacterRegion(text[i])->originalWidth + characterSpacing;
		}
	}

	if (currentLineWidth > maxLineWidth) return currentLineWidth;
	return maxLineWidth;
}

int Font::getTextHeight(const char* text) {

	int linesCount=0;
	if (text[0]) linesCount = 1;

	for (int i=0; text[i]; i++) {
		if (text[i]=='\n') linesCount++;
	}

	return linesCount * getLineHeight();
}
