#pragma once

#include <stdint.h>
#include <type_traits>
#include <cassert>
#include <vector>
#include <stddef.h>

namespace Ngl {

	// TODO: try using header in multiple cpp files (inline issues).

	template <typename T>
	class shared_ptr;

	template <typename T>
	class weak_ptr;

	////////////////////////////
	// Managed pointer base
	////////////////////////////

	class ManagedPointerBase {
	protected:
		inline ManagedPointerBase() {}
		class ManagedObject {

			friend class ManagedPointerBase;
			template <typename AnyT> friend class managed_ptr;
			template <typename AnyT> friend class shared_ptr;
			template <typename AnyT> friend class weak_ptr;

			const void *object;
			bool expired;
			int sharedReferencesCount;
			int weakReferencesCount;
		public:
			inline ManagedObject(const void *object) {
				this->object = object;
				this->sharedReferencesCount = 0;
				this->weakReferencesCount = 0;
				this->expired = false;
			}
		};

		static inline ManagedObject *allocateAndKillNullObject() {
			static ManagedObject *nullObject = new ManagedPointerBase::ManagedObject(0);
			nullObject->expired = true;
			return nullObject;
		}

		static inline ManagedObject *getNullObject() { // To initialize or reset managed pointers.
			static ManagedObject *nullObject = allocateAndKillNullObject();
			return nullObject;
		}

		static shared_ptr<void *> nullPointer;  // So the null object would never be deleted until the end of the application.
	};

	////////////////////////////
	// Managed pointer
	////////////////////////////

	template <typename T>
	class managed_ptr : public ManagedPointerBase {

		template <typename AnyT> friend class managed_ptr;
		template <typename AnyT> friend class shared_ptr;
		template <typename AnyT> friend class weak_ptr;

	protected:
		ManagedObject *managedObject;

		inline managed_ptr() {}

		virtual void incrementCount() = 0;
		virtual void destruct() = 0;

	public:

		virtual inline ~managed_ptr() {}

		managed_ptr<T> &operator=(const managed_ptr<T> &rightHandSide) {
			if (this == &rightHandSide) return *this;
			destruct();
			managedObject = rightHandSide.managedObject;
			incrementCount();
			return *this;
		}

		template <typename OtherTypeT>
		managed_ptr<T> &operator=(const managed_ptr<OtherTypeT> &rightHandSide) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			destruct();
			managedObject = rightHandSide.managedObject;
			incrementCount();
			return *this;
		}

		inline bool operator==(const managed_ptr<T> &rightHandSide) const {
			return this->managedObject == rightHandSide.managedObject;
		}

		template <typename OtherTypeT>
		inline bool operator==(const managed_ptr<OtherTypeT> &rightHandSide) const {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");
			return this->managedObject == rightHandSide.managedObject;
		}

		inline bool operator!=(const managed_ptr<T> &rightHandSide) const {
			return this->managedObject != rightHandSide.managedObject;
		}

		template <typename OtherTypeT>
		inline bool operator!=(const managed_ptr<OtherTypeT> &rightHandSide) const {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");
			return this->managedObject != rightHandSide.managedObject;
		}

		inline T* operator->() const {
			return (T *) this->managedObject->object;
		}

		inline T& operator*() const {
			return *(T *) this->managedObject->object;
		}

		void die() {
			if (!managedObject->expired) {
				delete (T *) managedObject->object;
				managedObject->object = 0;
				managedObject->expired = true;
			}
		}

		void reset() {
			destruct();
			this->managedObject = getNullObject();
			incrementCount();
		}

		inline bool expired() const { return this->managedObject->expired; }
		inline T *get() const { return (T *) this->managedObject->object; }
	};

	////////////////////////////
	// Shared pointer
	////////////////////////////

	template <typename T>
	class shared_ptr : public managed_ptr<T> {

		template <typename AnyT> friend class shared_ptr;

		inline void incrementCount() { this->managedObject->sharedReferencesCount++; }

		void destruct() {
			this->managedObject->sharedReferencesCount--;
			if (!this->managedObject->sharedReferencesCount) {
				if (!this->managedObject->expired) {
					delete (T *) this->managedObject->object;
					this->managedObject->object = 0;
					this->managedObject->expired = true;
				}

				if (!this->managedObject->weakReferencesCount) delete this->managedObject;
			}
		}

	public:

		inline shared_ptr() {
			this->managedObject = ManagedPointerBase::getNullObject();
			this->managedObject->sharedReferencesCount++;
		}

		inline shared_ptr(T *object) {
			this->managedObject = new ManagedPointerBase::ManagedObject(object);
			this->managedObject->sharedReferencesCount++;
		}

		inline shared_ptr(const shared_ptr<T> &src) {
			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		inline shared_ptr(const managed_ptr<T> &src) {
			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		template <typename OtherTypeT>
		inline shared_ptr(const managed_ptr<OtherTypeT> &src) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		inline shared_ptr(shared_ptr<T> &&src) {
			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		inline shared_ptr(managed_ptr<T> &&src) {
			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		template <typename OtherTypeT>
		inline shared_ptr(managed_ptr<OtherTypeT> &&src) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			this->managedObject = src.managedObject;
			this->managedObject->sharedReferencesCount++;
		}

		inline ~shared_ptr() { destruct(); }

		// Included to enhance performance by avoiding parent calling virtual methods,
		shared_ptr<T> &operator=(const shared_ptr<T> &rightHandSide) {
			if (this == &rightHandSide) return *this;
			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->sharedReferencesCount++;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		shared_ptr<T> &operator=(const managed_ptr<T> &rightHandSide) {
			if (this == (shared_ptr<T> *) &rightHandSide) return *this;
			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->sharedReferencesCount++;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		template <typename OtherTypeT>
		shared_ptr<T> &operator=(const managed_ptr<OtherTypeT> &rightHandSide) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->sharedReferencesCount++;
			return *this;
		}

		inline shared_ptr<T> &operator=(shared_ptr<T> &&rightHandSide) {
			ManagedPointerBase::ManagedObject *temp = this->managedObject;
			this->managedObject = rightHandSide.managedObject;
			rightHandSide.managedObject = temp;
			return *this;
		}

		template <typename OtherTypeT>
		inline shared_ptr<T> &operator=(shared_ptr<OtherTypeT> &&rightHandSide) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			ManagedPointerBase::ManagedObject *temp = this->managedObject;
			this->managedObject = rightHandSide.managedObject;
			rightHandSide.managedObject = temp;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		void reset() {
			destruct();
			this->managedObject = ManagedPointerBase::getNullObject();
			this->managedObject->sharedReferencesCount++;
		}

		void manage(const T *object) {
			destruct();
			this->managedObject = new ManagedPointerBase::ManagedObject(object);
			this->managedObject->sharedReferencesCount++;
		}

		template <typename OtherTypeT>
		void manage(const OtherTypeT *object) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");
			destruct();
			this->managedObject = new ManagedPointerBase::ManagedObject(object);
			this->managedObject->sharedReferencesCount++;
		}
	};

	////////////////////////////
	// Weak pointer
	////////////////////////////

	template <typename T>
	class weak_ptr : public managed_ptr<T> {

		template <typename AnyT> friend class weak_ptr;

		inline void incrementCount() { this->managedObject->weakReferencesCount++; }

		inline void destruct() {
			this->managedObject->weakReferencesCount--;
			if ((!this->managedObject->weakReferencesCount) && (!this->managedObject->sharedReferencesCount)) {
				delete this->managedObject;
			}
		}

	public:

		inline weak_ptr() {
			this->managedObject = ManagedPointerBase::getNullObject();
			this->managedObject->weakReferencesCount++;
		}

		inline weak_ptr(const weak_ptr<T> &src) {
			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		inline weak_ptr(const managed_ptr<T> &src) {
			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		template <typename OtherTypeT>
		inline weak_ptr(const managed_ptr<OtherTypeT> &src) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		inline weak_ptr(weak_ptr<T> &&src) {
			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		inline weak_ptr(managed_ptr<T> &&src) {
			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		template <typename OtherTypeT>
		inline weak_ptr(managed_ptr<OtherTypeT> &&src) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			this->managedObject = src.managedObject;
			this->managedObject->weakReferencesCount++;
		}

		inline ~weak_ptr() { destruct(); }

		// Included to enhance performance by avoiding parent calling virtual methods,
		weak_ptr<T> &operator=(const weak_ptr<T> &rightHandSide) {
			if (this == &rightHandSide) return *this;
			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->weakReferencesCount++;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		weak_ptr<T> &operator=(const managed_ptr<T> &rightHandSide) {
			if (this == (weak_ptr<T> *) &rightHandSide) return *this;
			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->weakReferencesCount++;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		template <typename OtherTypeT>
		weak_ptr<T> &operator=(const managed_ptr<OtherTypeT> &rightHandSide) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			destruct();
			this->managedObject = rightHandSide.managedObject;
			this->managedObject->weakReferencesCount++;
			return *this;
		}

		inline weak_ptr<T> &operator=(weak_ptr<T> &&rightHandSide) {
			ManagedPointerBase::ManagedObject *temp = this->managedObject;
			this->managedObject = rightHandSide.managedObject;
			rightHandSide.managedObject = temp;
			return *this;
		}

		template <typename OtherTypeT>
		inline weak_ptr<T> &operator=(weak_ptr<OtherTypeT> &&rightHandSide) {
			static_assert(
					std::is_base_of<T, OtherTypeT>::value ||
					std::is_base_of<OtherTypeT, T>::value,
					"Pointer types must be compatible");

			ManagedPointerBase::ManagedObject *temp = this->managedObject;
			this->managedObject = rightHandSide.managedObject;
			rightHandSide.managedObject = temp;
			return *this;
		}

		// Included to enhance performance by avoiding parent calling virtual methods,
		void reset() {
			destruct();
			this->managedObject = ManagedPointerBase::getNullObject();
			this->managedObject->weakReferencesCount++;
		}
	};

	////////////////////////////
	// Managed Pointer
	////////////////////////////

	template <typename T>
	class ManagedPointer {

        template <typename AnyT> friend class ManagedPointer;

		shared_ptr<T> sharedPointer;
		weak_ptr<T> weakPointer;

		bool isShared;

	public:

		inline ManagedPointer() {
			isShared = false;
		}

		inline ManagedPointer(const std::nullptr_t) : ManagedPointer() {}

        template <typename AnyTypeT>
        inline ManagedPointer(const ManagedPointer<AnyTypeT> &src) {
            this->sharedPointer = src.sharedPointer;
            this->weakPointer   = src.weakPointer;
            this->isShared = src.isShared;
        }

		template <typename AnyTypeT>
		inline ManagedPointer(const shared_ptr<AnyTypeT> &src) {
			this->sharedPointer = src;
			isShared = true;
		}

		template <typename AnyTypeT>
		inline ManagedPointer(const weak_ptr<AnyTypeT> &src) {
			this->weakPointer = src;
			isShared = false;
		}

		template <typename AnyTypeT>
		ManagedPointer<T> &operator=(const shared_ptr<AnyTypeT> &rightHandSide) {
			set(rightHandSide);
			return *this;
		}

		template <typename AnyTypeT>
		ManagedPointer<T> &operator=(const weak_ptr<AnyTypeT> &rightHandSide) {
			set(rightHandSide);
			return *this;
		}

		template <typename AnyTypeT>
		inline void set(const shared_ptr<AnyTypeT> &src) {
			this->weakPointer.reset();
			this->sharedPointer = src;
			isShared = true;
		}

		template <typename AnyTypeT>
		inline void set(const weak_ptr<AnyTypeT> &src) {
			this->sharedPointer.reset();
			this->weakPointer = src;
			isShared = false;
		}

		void die() {
			if (isShared) {
				sharedPointer.die();
				isShared = false;
			} else {
				weakPointer.die();
			}
		}

		void reset() {
			sharedPointer.reset();
			weakPointer.reset();
			isShared = false;
		}

		inline T* operator->() const {
			if (isShared) {
				return (T *) sharedPointer.get();
			} else {
				return (T *) weakPointer.get();
			}
		}

		inline T& operator*() const {
			if (isShared) {
				return *(this->sharedPointer);
			} else {
				return *(this->weakPointer);
			}
		}

		inline bool expired() const {
			if (isShared) {
				return this->sharedPointer.expired();
			} else {
				return this->weakPointer.expired();
			}
		}

		inline T *get() const {
			if (isShared) {
				return (T *) this->sharedPointer.get();
			} else {
				return (T *) this->weakPointer.get();
			}
		}
	};

	////////////////////////////
	// Shared vector
	////////////////////////////

	template <typename T>
	class SharedVector {
		std::vector<shared_ptr<T> > stdVector;
	public:

		// Note: make sure killing items doesn't result in modifying the vector (killing
		// an item triggers its destructor. The destructor may modify the vector. It
		// shouldn't, if you want predictable behavior that is),
		void killAll() {
			int32_t count = (int32_t) size()-1;
			for (; count>-1; count--) (*this)[count].die();
		}

		// Removes all dead pointers from the vector,
		void collectDead() {

			auto beginLocation = stdVector.begin();
			auto currentLocation = beginLocation;
			auto endLocation = stdVector.end();

			// Find the first dead pointer,
			bool foundDead = false;
			while (currentLocation != endLocation) {
				if (currentLocation->expired()) {
					foundDead = true;
					break;
				}
				currentLocation++;
			}

			// Compact the list
			if (foundDead) {
				auto currentDestination = currentLocation++;
				while (currentLocation != endLocation) {
					if (!currentLocation->expired()) {
						*currentDestination = *currentLocation;
						currentDestination++;
					}
					currentLocation++;
				}
				stdVector.resize(currentDestination - beginLocation);
			}
		}

		template <typename AnyT>
		void push_back(const managed_ptr<AnyT> &managedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(managedPtr);
		}

		template <typename AnyT>
		void push_back(managed_ptr<AnyT> &&managedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(std::move(managedPtr));
		}

		inline shared_ptr<T> &at(size_t index) {
			return stdVector.at(index);
		}

	    inline shared_ptr<T> &operator[](size_t index) {
	    	return stdVector.operator [](index);
	    }

		inline auto size() const -> decltype(stdVector.size()) { return stdVector.size(); }
	};

	////////////////////////////
	// Weak vector
	////////////////////////////

	template <typename T>
	class WeakVector {
		std::vector<weak_ptr<T> > stdVector;
	public:

		// Note: make sure killing items doesn't result in modifying the vector (killing
		// an item triggers its destructor. The destructor may modify the vector. It
		// shouldn't, if you want predictable behavior that is),
		void killAll() {
			int32_t count = (int32_t) size()-1;
			for (; count>-1; count--) (*this)[count].die();
		}

		// Removes all dead pointers from the vector,
		void collectDead() {

			auto beginLocation = stdVector.begin();
			auto currentLocation = beginLocation;
			auto endLocation = stdVector.end();

			// Find the first dead pointer,
			bool foundDead = false;
			while (currentLocation != endLocation) {
				if (currentLocation->expired()) {
					foundDead = true;
					break;
				}
				currentLocation++;
			}

			// Compact the list,
			if (foundDead) {
				auto currentDestination = currentLocation++;
				while (currentLocation != endLocation) {
					if (!currentLocation->expired()) {
						*currentDestination = *currentLocation;
						currentDestination++;
					}
					currentLocation++;
				}
				stdVector.resize(currentDestination - beginLocation);
			}
		}

		template <typename AnyT>
		void push_back(const managed_ptr<AnyT> &managedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(managedPtr);
		}

		template <typename AnyT>
		void push_back(managed_ptr<AnyT> &&managedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(std::move(managedPtr));
		}

		inline weak_ptr<T> &at(size_t index) {
			return stdVector.at(index);
		}

	    inline weak_ptr<T> &operator[](size_t index) {
	    	return stdVector.operator [](index);
	    }

		inline auto size() const -> decltype(stdVector.size()) { return stdVector.size(); }
	};

	////////////////////////////
	// Managed vector
	////////////////////////////

	template <typename T>
	class ManagedVector {
		std::vector<ManagedPointer<T> > stdVector;
	public:

		// Note: make sure killing items doesn't result in modifying the vector (killing
		// an item triggers its destructor. The destructor may modify the vector. It
		// shouldn't, if you want predictable behavior that is),
		void killAll() {
			int count = size()-1;
			for (; count>-1; count--) (*this)[count].die();
		}

		// Removes all dead pointers from the vector,
		void collectDead() {

			auto beginLocation = stdVector.begin();
			auto currentLocation = beginLocation;
			auto endLocation = stdVector.end();

			// Find the first dead pointer,
			bool foundDead = false;
			while (currentLocation != endLocation) {
				if (currentLocation->expired()) {
					foundDead = true;
					break;
				}
				currentLocation++;
			}

			// Compact the list,
			if (foundDead) {
				auto currentDestination = currentLocation++;
				while (currentLocation != endLocation) {
					if (!currentLocation->expired()) {
						*currentDestination = *currentLocation;
						currentDestination++;
					}
					currentLocation++;
				}
				stdVector.resize(currentDestination - beginLocation);
			}
		}

        template <typename AnyT>
        void push_back(const ManagedPointer<AnyT> &managedPtr) {
            if (stdVector.size() == stdVector.capacity()) collectDead();
            stdVector.emplace_back(managedPtr);
        }

        template <typename AnyT>
        void push_back(ManagedPointer<AnyT> &&managedPtr) {
            if (stdVector.size() == stdVector.capacity()) collectDead();
            stdVector.emplace_back(std::move(managedPtr));
        }

		template <typename AnyT>
		void push_back(const shared_ptr<AnyT> &sharedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(sharedPtr);
		}

		template <typename AnyT>
		void push_back(shared_ptr<AnyT> &&sharedPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(std::move(sharedPtr));
		}

		template <typename AnyT>
		void push_back(const weak_ptr<AnyT> &weakPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(weakPtr);
		}

		template <typename AnyT>
		void push_back(weak_ptr<AnyT> &&weakPtr) {
			if (stdVector.size() == stdVector.capacity()) collectDead();
			stdVector.emplace_back(std::move(weakPtr));
		}

		inline ManagedPointer<T> &at(size_t index) {
			return stdVector.at(index);
		}

	    inline ManagedPointer<T> &operator[](size_t index) {
	    	return stdVector.operator [](index);
	    }

		inline auto size() const -> decltype(stdVector.size()) { return stdVector.size(); }
	};

	template <typename T>
	shared_ptr<T> mkshrd(T *t) {
		return shared_ptr<T>(t);
	}
}
