
#include "DraggableLayout.h"

#include "TouchState.h"
#include "utils.h"

#define BLOCK_CHILDREN_EVENTS_DRAG_DISTANCE_CMS 0.25f

DraggableLayout::DraggableLayout(float x, float y, float width, float height) :
	AbsoluteLayout(x, y, width, height) {

	blockChildrenTouchEvents = false;

	lastTouchX = 0;
	lastTouchY = 0;

	initialLeft = x;
	initialBottom = y;
}

bool DraggableLayout::onTouchEvent(const TouchEvent *event) {

	if (!isEnabled()) return false;

	// This can only be dragged by a single pointer,
	if (event->touchState->getPointersCount() > 1) {
		return AbsoluteLayout::onTouchEvent(event);
	}

	// Save initial location when a drag begins,
	if ((!touched) && (event->type == TouchEvent::TOUCH_DRAG)) {
		touched = true;
		initialLeft = getLeft();
		initialBottom = getBottom();
		lastTouchX = event->downX;
		lastTouchY = event->downY;
	}

	// If clipping, check if hit within boundaries before sending to children,
	if (clipping) {
		if ((event->downX < initialLeft  ) || (event->downX >= initialLeft   +  getWidth()) ||
			(event->downY < initialBottom) || (event->downY >= initialBottom + getHeight())) {
			return false;
		}
	}

	// If consumed by a child, then skip,
	if ((!blockChildrenTouchEvents) && AbsoluteLayout::onTouchEvent(event)) {
		return true;
	}

	// If not clipping, check if hit within boundaries after sending to children,
	if (!clipping) {
		if ((event->downX < initialLeft  ) || (event->downX >= initialLeft   +  getWidth()) ||
			(event->downY < initialBottom) || (event->downY >= initialBottom + getHeight())) {
			return false;
		}
	}

	// Event not consumed by a child,
	switch (event->type) {

		case TouchEvent::TOUCH_DRAG:
			if (event->maxDraggedDistanceCms > BLOCK_CHILDREN_EVENTS_DRAG_DISTANCE_CMS) blockChildrenTouchEvents = true;

			setLeft(getLeft() + (event->x - lastTouchX));
			setBottom(getBottom() + (event->y - lastTouchY));
			lastTouchX = event->x;
			lastTouchY = event->y;
			break;

		case TouchEvent::TOUCH_DRAG_END:
			touched = false;
			blockChildrenTouchEvents = false;
			break;

		default:
			return true;	// false if you don't want to consume all events.
	}

	return true;
}
