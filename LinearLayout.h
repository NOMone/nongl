#pragma once

#include "Layout.h"
#include "Gravity.h"

class LinearLayout : public Layout {

	float x, y;
	float width, height;

	Orientation::Value orientation;
	Gravity::Value gravity;

	float padding;

public:

	LinearLayout();
	LinearLayout(float x, float y, float width, float height, Orientation::Value orientation=Orientation::VERTICAL, Gravity::Value gravity=Gravity::CENTER, float padding=0);

	void set(float x, float y, float width, float height, Orientation::Value orientation=Orientation::VERTICAL, Gravity::Value gravity=Gravity::CENTER, float padding=0);

	virtual inline float getWidth() { return width; }
	virtual inline float getHeight() { return height; }
	virtual inline float getScaledWidth() { return width; }
	virtual inline float getScaledHeight() { return height; }
	virtual inline float getLeft() { return x; }
	virtual inline float getBottom() { return y; }
	virtual inline float getRight() { return x + width; }
	virtual inline float getTop() { return y + height; }
	virtual inline float getCenterX() { return x + (width * 0.5f); }
	virtual inline float getCenterY() { return y + (height * 0.5f); }

	inline Gravity::Value getGravity() { return gravity; }
	virtual inline Orientation::Value getOrientation() { return orientation; }

	virtual inline void setWidth(float width) { this->width = width; }
	virtual inline void setHeight(float height) { this->height = height; }
	virtual inline void setLeft(float left) { x = left; }
	virtual inline void setBottom(float bottom) { y = bottom; }
	virtual inline void setRight(float right) { x = right - width ; }
	virtual inline void setTop(float top) { y = top - height ; }
	virtual inline void setCenterX(float centerX) { x = centerX - (width * 0.5f); }
	virtual inline void setCenterY(float centerY) { y = centerY - (height * 0.5f); }
	virtual inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }

	inline void setGravity(Gravity::Value gravity) { this->gravity = gravity; }
	inline void setPadding(float padding) { this->padding = padding; }

	virtual void layout();

	void fitContents();
};
