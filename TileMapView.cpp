#include "TileMapView.h"

#include "TextUtils.h"
#include "SystemLog.h"
#include "DisplayManager.h"
#include "SpriteBatch.h"
#include "File.h"

#include <string.h>
#include <algorithm>
#include <stdexcept>

/////////////////////////////
// Tile set
/////////////////////////////

TileSet::TileSet(int width, int height, int tileWidth, int tileHeight, const char *regionNamesPrefix) {
	this->tileSetWidth = width;
	this->tileSetHeight = height;
	this->tileWidth = tileWidth;
	this->tileHeight = tileHeight;

	this->regionNamesPrefix = new char[strlen(regionNamesPrefix)+1];
	strcpy(this->regionNamesPrefix, regionNamesPrefix);
	tileTextureRegions.resize(width * height, 0);
}

TileSet::~TileSet() {
	delete[] regionNamesPrefix;
}

const Ngl::TextureRegion *TileSet::getTileTextureRegion(int tileIndex) {

    // Scream out loudly if the tileIndex is out of bounds,
    if (tileIndex >= tileTextureRegions.size()) throw std::runtime_error("Tried to use an invalid tile texture region.");

	// Try to retrieve the tile texture region from the cached regions,
	const Ngl::TextureRegion *textureRegion = tileTextureRegions[tileIndex];

	if (!textureRegion) {
		// Fetch the texture regions,
		// Reverse y of the tile index because the regions are numbered in ascending order where (0, 0) is top left,
		// while on map (0, 0) is bottom left,
		int reversedTileIndex = (tileIndex % tileSetWidth) + (((tileSetHeight-1) - (tileIndex / tileSetWidth)) * tileSetWidth);

		TextBuffer regionName;
		regionName.append(regionNamesPrefix).append('.').append(reversedTileIndex);

		textureRegion = tileTextureRegions[tileIndex] = DisplayManager::getSingleton()->getTextureRegion(regionName.getText(), false);

	    // If still no texture region was found, say something,
		if (!textureRegion) {
		    LOGE("Tile texture region: %s not found.", regionName.getConstText());
		}
	}

	return textureRegion;
}

/////////////////////////////
// Tile
/////////////////////////////

bool Tile::equals(const Tile *tile) {
	if ((this->tileIndex != tile->tileIndex) ||
		(this->tileSet != tile->tileSet)) {
		return false;
	}
	return true;
}

void Tile::setFrom(const Tile *tile) {
	if (tile) {
		this->tileSet = tile->tileSet;
		this->tileIndex = tile->tileIndex;
		this->displacementX = tile->displacementX;
		this->displacementY = tile->displacementY;
	} else {
		// TODO: maybe just use a memset for safety?
		this->tileSet = 0;
		this->tileIndex = 0;
		this->displacementX = 0;
		this->displacementY = 0;
	}
}

const Ngl::TextureRegion *Tile::getTextureRegion() {
	if (!tileSet) return 0;
	return tileSet->getTileTextureRegion(tileIndex);
}

/////////////////////////////
// Tile map layer
/////////////////////////////

TileMapLayer::TileMapLayer(int width, int height) {
	this->width = width;
	this->height = height;

	this->visible = true;

	Tile emptyTile;
	emptyTile.tileSet = 0;
	emptyTile.tileIndex = 0;
	emptyTile.displacementX = 0;
	emptyTile.displacementY = 0;
	tiles.resize(width * height, emptyTile);
}

void TileMapLayer::copyFrom(const TileMapLayer *sourceLayer) {
	for (int i=0; i<tiles.size(); i++) tiles[i].setFrom(&sourceLayer->tiles[i]);
}

bool TileMapLayer::getTileXY(const Tile *tile, int *outTileX, int *outTileY) {
	for (int i=0; i<tiles.size(); i++) {
		if (&tiles[i] == tile) {
			*outTileX = i % width;
			*outTileY = i / height;
			return true;
		}
	}
	return false;
}

void TileMapLayer::clear(Tile *clearTile) {
	for (int i=0; i<tiles.size(); i++) {
		tiles[i].setFrom(clearTile);
	}
}

/////////////////////////////
// Tile map
/////////////////////////////

TileMap::TileMap() : drawingSprite(0, 0, 0) {
	set(0, 0, 0, 0, 0);
}

TileMap::TileMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight, int layersCount) : drawingSprite(0, 0, 0) {
	set(mapWidth, mapHeight, tileWidth, tileHeight, layersCount);
}

TileMap::~TileMap() {
	for (int i=0; i<layers.size(); i++) delete layers[i];
}

void TileMap::set(int mapWidth, int mapHeight, int tileWidth, int tileHeight, int layersCount) {

	this->mapWidth  = mapWidth;
	this->mapHeight = mapHeight;
	this->tileWidth = tileWidth;
	this->tileHeight = tileHeight;

	this->activeRectangleLeft = this->activeRectangleBottom = 0;
	this->activeRectangleWidth = mapWidth;
	this->activeRectangleHeight = mapHeight;

	// Delete any previously allocated layers,
	for (int i=0; i<layers.size(); i++) delete layers[i];
	layers.clear();

	// Create new layers,
	layers.reserve(layersCount);
	for (; layersCount; layersCount--) {
		TileMapLayer *newTileMapLayer = new TileMapLayer(mapWidth, mapHeight);
		layers.push_back(newTileMapLayer);
	}

	// Default initialization,
	this->x = this->y = 0;
	this->minZ = this->maxZ = 0;
	this->scaleX = this->scaleY = 1;
	this->drawGrid = false;
}

void TileMap::set(const TileMap *sourceTileMap) {

	this->x = sourceTileMap->x;
	this->y = sourceTileMap->y;
	this->minZ = sourceTileMap->minZ;
	this->maxZ = sourceTileMap->maxZ;
	this->scaleX = sourceTileMap->scaleX;
	this->scaleY = sourceTileMap->scaleY;
	this->drawGrid = sourceTileMap->drawGrid;

	this->mapWidth  = sourceTileMap->mapWidth;
	this->mapHeight = sourceTileMap->mapHeight;
	this->tileWidth = sourceTileMap->tileWidth;
	this->tileHeight = sourceTileMap->tileHeight;

	// Delete any previously allocated layers,
	for (int i=0; i<layers.size(); i++) delete layers[i];
	layers.clear();

	// Create new layers,
	int32_t layersCount = (int32_t) sourceTileMap->layers.size();
	layers.reserve(layersCount);
	for (int32_t i=0; i<layersCount; i++) {
		TileMapLayer *newTileMapLayer = new TileMapLayer(mapWidth, mapHeight);
		layers.push_back(newTileMapLayer);

		// Copy layer,
		newTileMapLayer->copyFrom(sourceTileMap->layers[i]);
	}
}

int TileMap::getDepthLayersCount() {
	// TODO: fix for isometric (diagonal instead of vertical),
	return mapHeight;
}

void TileMap::draw(SpriteBatch *spriteBatch, bool drawOpaque, bool drawTransparent) {

	// Draw all layers one by one,
	// Calculate active rectangle boundaries,
	int activeRectangleLeft = this->activeRectangleLeft;
	int activeRectangleBottom = this->activeRectangleBottom;

	if (activeRectangleLeft < 0) activeRectangleLeft = 0;
	if (activeRectangleLeft > mapWidth) activeRectangleLeft = mapWidth;

	if (activeRectangleBottom < 0) activeRectangleBottom = 0;
	if (activeRectangleBottom > mapHeight) activeRectangleBottom = mapHeight;

	int activeRectangleRight = activeRectangleLeft + activeRectangleWidth;
	int activeRectangleTop = activeRectangleBottom + activeRectangleHeight;

	if (activeRectangleRight < 0) activeRectangleRight = 0;
	if (activeRectangleRight > mapWidth) activeRectangleRight = mapWidth;

	if (activeRectangleTop < 0) activeRectangleTop = 0;
	if (activeRectangleTop > mapHeight) activeRectangleTop = mapHeight;

	// Depth adjustment,
	// TODO: fix for isometric (diagonal instead of vertical),
	float zStep = (maxZ - minZ) / (layers.size() * mapHeight);
	int currentDepthLevel = 0;

	float scaledTileWidth = tileWidth * scaleX;
	float scaledTileHeigt = tileHeight * scaleY;

	drawingSprite.scaleX = scaleX;
	drawingSprite.scaleY = scaleY;
	for (int k=0; k<layers.size(); k++) {

		// If layer is not visible, skip layer,
		if (!layers[k]->getVisible()) {
			// Advance depth to avoid possible glitches,
			currentDepthLevel+= mapHeight;

			// Skip drawing,
			continue;
		}

		// Draw the current layer tile by tile,
		TileMapLayer *currentLayer = layers[k];

		// TODO: correct order for isometric maps,
		for (int j=activeRectangleTop-1; j>=activeRectangleBottom; j--) {

			drawingSprite.y = y + (j * scaledTileHeigt);

			// TODO: fix for isometric,
			drawingSprite.z = maxZ - (currentDepthLevel * zStep);

			for (int i=activeRectangleLeft; i<activeRectangleRight; i++) {

				drawingSprite.x = x + (i * scaledTileWidth);

				Tile *currentTile = currentLayer->getTile(i, j);
				drawingSprite.textureRegion = currentTile->getTextureRegion();
				if (drawingSprite.textureRegion) {
					if ((drawTransparent && drawingSprite.textureRegion->hasAlpha) ||
						(drawOpaque && !drawingSprite.textureRegion->hasAlpha)) {

						// Account for tile displacement,
						float oldX = drawingSprite.x;
						float oldY = drawingSprite.y;
						drawingSprite.x += currentTile->displacementX * scaleX;
						drawingSprite.y += currentTile->displacementY * scaleY;

						drawingSprite.width = drawingSprite.textureRegion->originalWidth;
						drawingSprite.height = drawingSprite.textureRegion->originalHeight;
						drawingSprite.draw(spriteBatch);

						drawingSprite.x = oldX;
						drawingSprite.y = oldY;
					}
				}
			}

			// Move one step in depth,
			currentDepthLevel++;
		}
	}

	// Draw grid,
	if (drawGrid) {
		// Vertical lines,
		for (int i=activeRectangleLeft; i<=activeRectangleRight; i++) {
			spriteBatch->drawLine(
					x + (i*tileWidth*scaleX), y, minZ,
					x + (i*tileWidth*scaleX), y+getScaledHeight(), minZ,
					0xff00ff00);
		}

		// Horizontal lines,
		for (int i=activeRectangleBottom; i<=activeRectangleTop; i++) {
			spriteBatch->drawLine(
					x, y + (i*tileHeight*scaleY), minZ,
					x+getScaledWidth(), y + (i*tileHeight*scaleY), minZ,
					0xff00ff00);
		}
	}
}

Tile *TileMap::getTileFromCoords(int layerIndex, float x, float y) {
	return layers[layerIndex]->getTile((x-this->x) / (tileWidth*scaleX), (y-this->y) / (tileHeight*scaleY));
}

bool TileMap::getTileLocation(const Tile *tile, int *outTileX, int *outTileY) {

	for (int i=0; i<layers.size(); i++) {
		if (layers[i]->getTileXY(tile, outTileX, outTileY)) return true;
	}
	return false;
}

void TileMap::getTileLocationFromCoords(float xCoord, float yCoord, int *outTileX, int *outTileY) {
	*outTileX = (xCoord-this->x) / (tileWidth*scaleX);
	*outTileY = (yCoord-this->y) / (tileHeight*scaleY);
}

void TileMap::pad(const std::vector<Tile> &layersPaddingTiles, int32_t horizontalPadding, int32_t verticalPadding) {

    if (layersPaddingTiles.size() != layers.size()) throw std::runtime_error("Tilemap padding layers count is different from the actual layers count.");

    int32_t newWidth  = getMapWidth () + (horizontalPadding<<1);
    int32_t newHeight = getMapHeight() + (verticalPadding  <<1);

    int32_t lastNotPaddingColumn = getMapWidth () + horizontalPadding - 1;
    int32_t lastNotPaddingRow    = getMapHeight() + verticalPadding   - 1;

    // For each layer,
    for (int32_t layerIndex=(int32_t) layers.size()-1; layerIndex>-1; layerIndex--) {

        // Create a new layer that's larger in dimensions,
        TileMapLayer *newLayer = new TileMapLayer(newWidth, newHeight);

        // Set the tiles in the new layer,
        TileMapLayer *currentLayer = layers[layerIndex];
        const Tile &paddingTile = layersPaddingTiles[layerIndex];

        for (int j=0; j<newHeight; j++) {
            for (int i=0; i<newWidth; i++) {

                Tile *newTile = newLayer->getTile(i, j);
                if ((i < horizontalPadding   ) ||
                    (j <   verticalPadding   ) ||
                    (i > lastNotPaddingColumn) ||
                    (j > lastNotPaddingRow   )) {

                    *newTile = paddingTile;
                } else {
                    *newTile = *currentLayer->getTile(i-horizontalPadding, j-verticalPadding);
                }
            }
        }

        // Delete the old layers and replace it with the new one,
        delete currentLayer;
        layers[layerIndex] = newLayer;
    }

    // Set the map new dimensions,
    mapWidth  = newWidth ;
    mapHeight = newHeight;
    setActiveRectangleLeft  ((activeRectangleLeft   - horizontalPadding >= 0) ? activeRectangleLeft   - horizontalPadding : 0);
    setActiveRectangleBottom((activeRectangleBottom - verticalPadding   >= 0) ? activeRectangleBottom - verticalPadding   : 0);

    setActiveRectangleWidth (newWidth );
    setActiveRectangleHeight(newHeight);
}


void TileMap::save(const Ngl::File &file, std::vector<TileSet *> *tileSets) {

	// Save map data,
	TextBuffer data;
	data.append("Map: ")
		.append(mapWidth).append(", ").append(mapHeight).append(", ")
		.append(tileWidth).append(", ").append(tileHeight).append(", ")
		.append((int) layers.size()).append('\n');

	// Get tilesets used,
	std::list<TileSet *> usedTileSetsList;
	if (tileSets) {
		usedTileSetsList.resize(tileSets->size());
		std::copy(tileSets->begin(), tileSets->end(), usedTileSetsList.begin());
	} else {

		// Get all the tilesets used in this map,
		for (int k=0; k<layers.size(); k++) {
			for (int j=0; j<mapHeight; j++) {
				for (int i=0; i<mapWidth; i++) {
					Tile *currentTile = getTile(k, i, j);
					if (currentTile->tileSet) {
						usedTileSetsList.push_front(currentTile->tileSet);
					}
				}
			}
			usedTileSetsList.sort();
			usedTileSetsList.unique();
		}
	}

	// Save tilesets used,
	// Move tileset to a vector for fast search,
	std::vector<TileSet *> usedTileSetsVector;
	usedTileSetsVector.resize(usedTileSetsList.size());
	std::copy(usedTileSetsList.begin(), usedTileSetsList.end(), usedTileSetsVector.begin());

	// Save tilesets,
	for (int i=0; i<usedTileSetsVector.size(); i++) {
		data.append("Tileset: ")
			.append(usedTileSetsVector[i]->getTileSetWidth()).append(", ").append(usedTileSetsVector[i]->getTileSetHeight()).append(", ")
			.append(usedTileSetsVector[i]->getTileWidth()).append(", ").append(usedTileSetsVector[i]->getTileHeight()).append(", ")
			.append(usedTileSetsVector[i]->getTileSetName()).append('\n');
	}

	// Save tiles,
	for (int k=0; k<layers.size(); k++) {
		for (int j=0; j<mapHeight; j++) {
			for (int i=0; i<mapWidth; i++) {
				Tile *currentTile = getTile(k, i, j);
				// If this is a non-empty tile,
				if (currentTile->tileSet) {
					//int tileSetIndex = std::lower_bound(usedTileSetsVector.begin(), usedTileSetsVector.end(), currentTile->tileSet) - usedTileSetsVector.begin();
					int32_t tileSetIndex = (int32_t) (std::find(usedTileSetsVector.begin(), usedTileSetsVector.end(), currentTile->tileSet) - usedTileSetsVector.begin());
					data.append("Tile: ").append(k).append(", ").append(i).append(", ").append(j).append(", ")
						.append(tileSetIndex).append(", ").append(currentTile->tileIndex)
						.append('\n');
				}
			}
		}
	}

	// Write file,
	file.write(data.getText(), data.getTextLength(), false);
}

TileMap *TileMap::load(const Ngl::File &file, std::vector<TileSet *> &outTileSets, bool loadTileSets) {

	// Load file,
	std::vector<uint8_t> fileContents;
	int32_t contentsSize = file.read(fileContents);

	char currentLine[1024];
	int currentLocation=0;

	TileMap *tileMap=0;
	int mapWidth, mapHeight, tileWidth, tileHeight, layersCount;
	int tileSetWidth, tileSetHeight, tileSetTileWidth, tileSetTileHeight;
	int tileX, tileY, tileLayer, tileTileSetIndex, tileIndex;
	char tileSetName[1024];

	do {

		int charsCount;
		do {
			// Check if end of file,
			if (currentLocation >= contentsSize) return tileMap;

			// Read the next line,
			charsCount = readLine((char *) &fileContents[currentLocation], currentLine);
			currentLocation += charsCount;
		} while (strlen(trim(currentLine)) == 0);

		removeWhitespaces(currentLine);
		if (parseLine(currentLine, "Map:", "Map:%d,%d,%d,%d,%d", &mapWidth, &mapHeight, &tileWidth, &tileHeight, &layersCount)) {
			tileMap = new TileMap(mapWidth, mapHeight, tileWidth, tileHeight, layersCount);
		} else if (parseLine(currentLine, "Tileset:", "Tileset:%d,%d,%d,%d,%s", &tileSetWidth, &tileSetHeight, &tileSetTileWidth, &tileSetTileHeight, tileSetName)) {
			if (loadTileSets) {
				TileSet *newTileSet = new TileSet(tileSetWidth, tileSetHeight, tileSetTileWidth, tileSetTileHeight, tileSetName);
				outTileSets.push_back(newTileSet);
			}
		} else if (parseLine(currentLine, "Tile:", "Tile:%d,%d,%d,%d,%d", &tileLayer, &tileX, &tileY, &tileTileSetIndex, &tileIndex)) {
			Tile *tile = tileMap->getTile(tileLayer, tileX, tileY);
			tile->tileSet = outTileSets[tileTileSetIndex];
			tile->tileIndex = tileIndex;
		} else {
			// Just ignore...
		}
	} while (1);

	return tileMap;
}

/////////////////////////////
// Tile map view
/////////////////////////////

TileMapView::TileMapView(TileMap *tileMap) : tileMap(tileMap) {
	this->x = this->y = 0;
	this->scaleX = tileMap->getScaleX();
	this->scaleY = tileMap->getScaleY();
	this->minZ = this->maxZ = 0;
}

void TileMapView::draw(SpriteBatch *spriteBatch, bool drawOpaque, bool drawTransparent) {

	// Buffer map properties,
	// This maybe wasteful in most cases, but since tilemaps are expensive anyway,
	// this overhead is negligable and serves well if the same tilemap is going to
	// be used with more than one TileMapVew...
	float oldLeft = tileMap->getLeft();
	float oldBottom = tileMap->getBottom();
	float oldMinDepth = tileMap->getMinDepth();
	float oldMaxDepth = tileMap->getMaxDepth();
	float oldScaleX = tileMap->getScaleX();
	float oldScaleY = tileMap->getScaleY();

	// Apply view transformation to the tilemap,
	tileMap->setLeft(x);
	tileMap->setBottom(y);
	tileMap->adjustDepths(maxZ, minZ);
	tileMap->setScale(scaleX, scaleY);

	// Draw,
	tileMap->draw(spriteBatch, drawOpaque, drawTransparent);

	// Restore the tilemap properties,
	tileMap->setLeft(oldLeft);
	tileMap->setBottom(oldBottom);
	tileMap->adjustDepths(oldMaxDepth, oldMinDepth);
	tileMap->setScale(oldScaleX, oldScaleY);
}

Tile *TileMapView::getTileFromCoords(int layerIndex, float x, float y) {
	return tileMap->getTileFromCoords(layerIndex, x, y);
}

