#pragma once

#include "TextureRegion.h"
#include "Sprite.h"

#include <vector>

class SpriteBatch;

/////////////////////////
// Nine patch data
/////////////////////////

class NinePatchData {
public:

	const Ngl::TextureRegion *textureRegion;

	std::vector<int> columnWidths;
	std::vector<int> rowHeights;

	bool firstColumnHorizontallyStretchable;
	bool firstRowVerticallyStretchable;

	void setColumnWidths(int columnsCount, bool firstColumnHorizontallyStretchable, ...);
	void setRowHeights(int rowsCount, bool firstRowVerticallyStretchable, ...);

	float getMinWidth() const;
	float getMinHeight() const;
};

/////////////////////////
// Nine patch sprite
/////////////////////////

class NinePatchSprite {

	const Ngl::TextureRegion *textureRegion;

	int columnsCount=0, rowsCount=0;
	Ngl::TextureRegion **textureRegions=nullptr;

	bool firstColumnHorizontallyStretchable;
	bool firstRowVerticallyStretchable;

	Sprite sprite;

	int minWidth, minHeight;
	int normalWidth, normalHeight;

	float x, y, z;
	float width, height;
	float rotation;

protected:
	void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool transparent);

public:

	float scaleX, scaleY;
	int colorMask;

	inline NinePatchSprite() {}
	inline NinePatchSprite(const NinePatchData *data) { set(data); }
	virtual ~NinePatchSprite();

	void set(const NinePatchData *data);

	inline Ngl::Texture *getTexture() { return textureRegion->texture; }
	inline Ngl::TextureRegion *getPatchRegion(int column, int row) { return textureRegions[(row*columnsCount)+column]; }

	inline float getWidth() { return width; }
	inline float getHeight() { return height; }
	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + (width * scaleX); }
	inline float getTop() { return y + (height * scaleY); }
	inline float getCenterX() { return x + (width * scaleX * 0.5f); }
	inline float getCenterY() { return y + (height * scaleY * 0.5f); }
	inline float getDepth() { return z; }
	inline float getRotation() { return rotation; }

	inline void setWidth(float width) { this->width = (width > minWidth) ? width : minWidth; }
	inline void setHeight(float height) { this->height = (height > minHeight) ? height : minHeight; }
	inline void setLeft(float left) { x = left; }
	inline void setBottom(float bottom) { y = bottom; }
	inline void setRight(float right) { x = right - (width * scaleX); }
	inline void setTop(float top) { y = top - (height * scaleY); }
	inline void setCenterX(float centerX) { x = centerX - (width * scaleX * 0.5f); }
	inline void setCenterY(float centerY) { y = centerY - (height * scaleY * 0.5f); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	inline void setDepth(float z) { this->z = z; }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }
	inline void setRotation(float angleRadians) { this->rotation = angleRadians; }
	inline void setColorMask(int colorMask) { this->colorMask = colorMask; }

	inline void drawOpaqueParts(SpriteBatch *spriteBatch) {
		if ((colorMask & 0xff000000) < 0xff000000) return;
		draw(spriteBatch, false, false);
	}
	inline void drawTransparentParts(SpriteBatch *spriteBatch) {
		if ((colorMask & 0xff000000) < 0xff000000) {
			draw(spriteBatch, true, false);
		} else {
			draw(spriteBatch, false, true);
		}
	}
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }

	void attachToAnimation(Animation *animation);
};
