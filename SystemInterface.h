#pragma once

#include <stdint.h>
#include "TextUtils.h"

// System side functions,
// Preferences,
void javaSetBooleanPreference(const char *key, bool value);
bool javaGetBooleanPreference(const char *key, bool defaultValue);
void javaSetIntPreference(const char *key, int32_t value);
int32_t javaGetIntPreference(const char *key, int32_t defaultValue);
void javaSetStringPreference(const char *key, const TextBuffer &value);
TextBuffer javaGetStringPreference(const char *key, const TextBuffer &defaultValue);

// Music,
int javaLoadMusicFromAssets(const char *filename);
void javaDeleteMusic(int musicIndex);
void javaPlayMusic(int musicIndex);
void javaMusicSetLooping(int musicIndex, bool looping);
void javaStopMusic(int musicIndex);
void javaPauseMusic(int musicIndex);
void javaResumeMusic(int musicIndex);
void javaFadeOutMusic(int musicIndex, int milliSeconds);

// Sound,
int32_t javaLoadSoundFromAssets(const char *fileName);
void javaDeleteSound(int soundIndex);
void javaPlaySound(int soundIndex, float rate=1, float leftVolume=1, float rightVolume=1);
void javaSoundSetLooping(int soundIndex, bool looping);
void javaStopSound(int soundIndex);
void javaPauseSound(int soundIndex);
void javaResumeSound(int soundIndex);
void javaMuteSounds(bool mute);

// Sockets,
int javaCreateSocket(const char *address, int port);
void javaCloseSocket(int socketId);
void javaSocketWrite(int socketId, const char *bytes, int sizeBytes);
void *javaSocketRead(int socketId);

// Misc,
bool javaUsesCoverageAa();
void javaLongToast(const char *toastText);
void javaShortToast(const char *toastText);
void javaBrowseUrl(const char *url, const char *alternativeUrl=0);
float javaGetDpi();
float javaGetTimeMillis();
void javaFinishActivity();
void javaPreventSleep(bool prevent);
void javaOpenOptionsMenu();
void javaShowVirtualKeyboard();
void javaHideVirtualKeyboard();
TextBuffer systemUrlEncode(const TextBuffer &text);
std::vector<uint8_t> systemHttpRequest(const TextBuffer &url, const TextBuffer &data, bool isPost);
std::vector<uint8_t> systemDownloadFile(const TextBuffer &url);
TextBuffer systemGetNetworkCountryCode();

void javaExit();  // Library users should never call this. Use javaFinishActivity() instead.
