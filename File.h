#pragma once

#include "InputStream.h"
#include "OutputStream.h"
#include "BufferedOutputStream.h"

#include "ManagedObject.h"
#include "TextUtils.h"

namespace Ngl {

	class File;

	/////////////////////////////////
	// File input stream
	/////////////////////////////////

	class FileInputStream : public InputStream {
		ManagedPointer<File> file;
		int32_t offsetInFile=0;
		int32_t markedOffset=-1;
		int32_t markReadLimit=0;
	public:

		inline FileInputStream(ManagedPointer<File> file) : file(file) {}

		// Whether this stream has (or may have) more data to deliver,
		bool hasData();

		// Reads the entire file (if possible),
		int32_t readData(std::vector<uint8_t> &outputVector, int32_t offset);

		inline bool markSupported() { return true; }
		void mark(int32_t readLimit=0);
		void reset();
	};

	/////////////////////////////////
	// File output stream
	/////////////////////////////////

	class UnbufferedFileOutputStream : public OutputStream {
		ManagedPointer<File> file;
		bool append;
	public:

		inline UnbufferedFileOutputStream(ManagedPointer<File> file, bool append=false) : file(file), append(append) {}
		void writeData(const void *data, int32_t sizeBytes);
	};

	class FileOutputStream : public OutputStream {
		BufferedOutputStream bufferedOutputStream;
	public:

		FileOutputStream(ManagedPointer<File> file, bool append=false, int32_t maxBufferSizeBytes=0);
		void writeData(const void *data, int32_t sizeBytes);
		void flush();
	};

	/////////////////////////////////
	// File
	/////////////////////////////////

	enum class FileLocation { ASSETS, SDCARD };

	class File {
		FileLocation location;
		TextBuffer fullPath;
	public:
		inline File(const TextBuffer &fullPath, FileLocation location=FileLocation::ASSETS) : location(location), fullPath(fullPath) {}
		inline File(FileLocation location, const TextBuffer &fullPath) : location(location), fullPath(fullPath) {}
		File(const File &parentDirectory, const TextBuffer &fileName);

	    bool operator==(const File &rightHandSide) const;

		bool exists() const;
		inline const TextBuffer &getFullPath() const { return fullPath; }
		inline FileLocation getLocation() const { return location; }
		int32_t getSizeBytes() const;
		void makeDir() const;
		void copy(const File &destination) const;

		int32_t read(std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile=0, int32_t maxReadSize=0) const;
		int32_t read(std::vector<uint8_t> &outputVector) const;
		int32_t write(const void *data, int32_t sizeBytes, bool append=false) const;
	};
}
