#include "FrameTimeTracker.h"
#include "utils.h"
#include <stdlib.h>

FrameTimeTracker::FrameTimeTracker(int totalHistoryLength, float minFrameDurationMillis) {

	this->lastFrameTimeMillis = 0;
	this->elapsedTimeMillis = 0;

	if (totalHistoryLength < 1) totalHistoryLength = 1;
	this->elapsedTimeMillisHistory = (double *) malloc(sizeof(double) * totalHistoryLength);
	this->totalHistoryLength = totalHistoryLength;
	this->usedHistoryLength = 0;
	this->historyIndex = 0;

	this->minFrameDurationMillis = minFrameDurationMillis;

	this->skipFrame = false;
}

FrameTimeTracker::~FrameTimeTracker() {
	free(elapsedTimeMillisHistory);
}

double FrameTimeTracker::newFrame() {

	// Calculate time since last frame,
	if (lastFrameTimeMillis == 0) {

		usedHistoryLength = 0;
		historyIndex = 0;
        elapsedTimeMillis = 0;
		lastFrameTimeMillis = getTimeMillis();
        
	} else if (skipFrame) {
		elapsedTimeMillis = 0;
		skipFrame = false;
		lastFrameTimeMillis = getTimeMillis();
        
    } else {
		double currentTime;
		double timeDifference;
		do {
			currentTime = getTimeMillis();
			timeDifference = currentTime - lastFrameTimeMillis;
			if (timeDifference < minFrameDurationMillis) {
				sleepMillis(minFrameDurationMillis-timeDifference);
			}
		} while (timeDifference < minFrameDurationMillis);

		elapsedTimeMillisHistory[historyIndex] = timeDifference;
		historyIndex = (historyIndex + 1) % totalHistoryLength;
		if (usedHistoryLength < totalHistoryLength)
			usedHistoryLength++;

		lastFrameTimeMillis = currentTime;
        
        // Calculate average elapsed time,
        elapsedTimeMillis = 0;
        if (usedHistoryLength) {
            for (int i=0; i<usedHistoryLength; i++) {
                elapsedTimeMillis += elapsedTimeMillisHistory[i];
            }
            elapsedTimeMillis /= usedHistoryLength;
        }
        
        return elapsedTimeMillis;
	}
    
    return 0;
}
