#pragma once

#include <vector>

#include "View.h"
#include "Gravity.h"

class LinearLayout;

class Layout : public View {

protected:
	std::vector<View *> childrenViews;
	int depthLayersCount=0;
public:

	bool deletesChildrenOnDestruction=true;
	bool drawOutOfBoundChildren=false;

	virtual ~Layout();

	virtual inline void addView(View *view) { view->setParent(this); childrenViews.push_back(view); }
	virtual inline void addView(View *view, int index) { view->setParent(this); childrenViews.insert(childrenViews.begin()+index, view); }
	virtual inline View *getView(int index) { return (childrenViews.size() > index) ? childrenViews[index] : 0; }
	virtual int32_t getViewIndex(View *view);
	virtual inline int32_t getChildViewsCount() { return (int32_t) childrenViews.size(); }
	virtual void removeView(View *view);
    virtual void removeView(int32_t index);
	virtual inline void onJustLayouted() { layout(); }
	virtual void layout();

	virtual Ngl::Rect transformParentRectToLocalRect(const Ngl::Rect &rect);
	virtual void setParentBounds(const Ngl::Rect &parentBounds);
	virtual void updateDepthLayersCount();
	virtual inline int getDepthLayersCount() { return depthLayersCount; }
	virtual void adjustDepths(float maxZ, float minZ);
	virtual void drawOpaqueParts(SpriteBatch *spriteBatch);
	virtual void drawTransparentParts(SpriteBatch *spriteBatch);
	virtual void drawAllInOrder(SpriteBatch *spriteBatch);
	virtual void update(float elapsedTimeMillis);

	virtual bool onTouchEvent(const TouchEvent *event);

	LinearLayout *addMatchingLinearLayout(Orientation::Value orientation, Gravity::Value gravity);
};
