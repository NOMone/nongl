#include "Button.h"

#include "TextView.h"
#include "TouchState.h"
#include "DisplayManager.h"
#include "SegmentedSprite.h"
#include "NinePatchSprite.h"
#include "Audio.h"
#include "utils.h"

#define MIN_SOUND_PITCH 0.8f
#define MAX_SOUND_PITCH 1.2f

///////////////////////////
// Button parameters
///////////////////////////

ButtonParams::ButtonParams() {
	x = y = 0;
	width = 0;
	height = 0;
	text = 0;
	rotation = 0;
	color = 0xffffffff;
	textColor = 0xff000000;
	textScaleX = textScaleY = 1;
	textPaddingX = 5;
	textPaddingY = 2;
	buttonUpTextureRegion = 0;
	buttonDownTextureRegion = 0;
	buttonUpSegmentedSpriteParams = 0;
	buttonDownSegmentedSpriteParams = 0;
	buttonUpNinePatchData = 0;
	buttonDownNinePatchData = 0;
}

ButtonParams::ButtonParams(const Ngl::TextureRegion *buttonUpTextureRegion) : ButtonParams() {
	this->buttonUpTextureRegion = buttonUpTextureRegion;
}

ButtonParams::ButtonParams(const char *text, float textScale, float maxWidth) : ButtonParams() {

	// Wrap text,
	this->text = text;
	this->textScaleX = this->textScaleY = textScale;
	this->width = TextSprite::getTextWidth(text, maxWidth, textScale);
	this->height = TextSprite::getTextHeight(text, this->width, textScale);

	// Extra padding,
	this->width  += this->textPaddingX * 2;
	this->height += this->textPaddingY * 2;
}

///////////////////////////
// Button
///////////////////////////

Sound *Button::clickSound = 0;

Button::Button(const ButtonParams &parameters) : Button(&parameters) {}
Button::Button(const ButtonParams *parameters) {
	if (parameters) {
		set(parameters);
	} else {
		ButtonParams defaultParameters;
		set(&defaultParameters);
	}
}

Button::~Button() {
	if (textView) delete textView;
}

Button::Button(float x, float y, const char *text) {

	ButtonParams params;
	params.x = x;
	params.y = y;
	params.text = text;

	set(&params);
}

void Button::set(const ButtonParams *parameters) {

	// Internal state initialization,
	this->textView = 0;
	this->consumesTouches = true;
	this->pressed = false;
	this->buttonListener = 0;
	this->touchState = 0;

	this->tag = 0;

	// Parameters initialization,
	this->x = parameters->x;
	this->y = parameters->y;
	this->width = parameters->width;
	this->height = parameters->height;
	this->rotation = parameters->rotation;
	this->color = parameters->color;
	this->textColor = parameters->textColor;
	this->textScaleX = parameters->textScaleX;
	this->textScaleY = parameters->textScaleY;
	this->textPaddingX = parameters->textPaddingX;
	this->textPaddingY = parameters->textPaddingY;

	// Set default parameters,
	this->maxZ = this->minZ = 0;
	this->scaleX = this->scaleY = 1;
	this->rotationPivotXDisplacement = this->rotationPivotYDisplacement = 0;
	this->colorMask = 0xffffffff;

	// TODO: should allow mixing sprite types...

	if (parameters->buttonUpSegmentedSpriteParams || parameters->buttonDownSegmentedSpriteParams) {

		// Segmented sprite initialization,
		const SegmentedSpriteParameters *downSpriteParams = parameters->buttonDownSegmentedSpriteParams;
		const SegmentedSpriteParameters *upSpriteParams   = parameters->buttonUpSegmentedSpriteParams  ;

		if (!downSpriteParams) downSpriteParams =   upSpriteParams;
		if (!upSpriteParams  )   upSpriteParams = downSpriteParams;

		if (width  == 0) width  = upSpriteParams->imageWidth ;
		if (height == 0) height = upSpriteParams->imageHeight;

		downSprite.init(downSpriteParams);
		upSprite  .init(  upSpriteParams);

	} else if (parameters->buttonUpTextureRegion || parameters->buttonDownTextureRegion) {

		// Regular sprite initialization,
		const Ngl::TextureRegion *downSpriteParams = parameters->buttonDownTextureRegion;
		const Ngl::TextureRegion *upSpriteParams   = parameters->buttonUpTextureRegion  ;

		if (!downSpriteParams) downSpriteParams =   upSpriteParams;
		if (!upSpriteParams  )   upSpriteParams = downSpriteParams;

		if (width  == 0) width  = upSpriteParams->originalWidth ;
		if (height == 0) height = upSpriteParams->originalHeight;

		downSprite.init(downSpriteParams);
		upSprite  .init(  upSpriteParams);

	} else {

		// Nine patch sprite initialization (default initialization),
		const NinePatchData *downSpriteParams = parameters->buttonDownNinePatchData;
		const NinePatchData *upSpriteParams   = parameters->buttonUpNinePatchData  ;

		// Since there is a default nine-patch button texture,
		if ((!downSpriteParams) && (!upSpriteParams)) {
			downSpriteParams = getDefaultNinePatchData();
			upSpriteParams   = downSpriteParams;
		} else if (!downSpriteParams) {
			downSpriteParams = upSpriteParams;
		} else if (!upSpriteParams) {
			upSpriteParams   = downSpriteParams;
		}

		// Set default width and height,
		if (!font) font = Font::getDefaultFont();
		if (width == 0) {
		    if (parameters->text) {
		        width = font->getTextWidth(parameters->text) + (textPaddingX*2);
		    } else {
		        width = upSpriteParams->getMinWidth();
		    }
		}
        if (height == 0) {
            if (parameters->text) {
                height = font->getTextHeight(parameters->text) + (textPaddingX*2);
            } else {
                height = upSpriteParams->getMinHeight();
            }
        }

		downSprite.init(downSpriteParams, width, height);
		upSprite  .init(  upSpriteParams, width, height);
	}

	currentSprite = &upSprite;

	if (parameters->text) {
		textView = new TextView(
				x + textPaddingX, y + textPaddingY,
				width - (textPaddingX*2), height - (textPaddingY*2),
				Gravity::CENTER, textColor);
		if (font) textView->setFont(font);
		textView->setText(parameters->text);
	}

	// Animation initialization,
	this->localAnimationScaleX = this->localAnimationScaleY = 1;
	this->localAnimationX = this->localAnimationY = 0;

	ElasticOutInterpolator elasticOutInterpolator;
	elasticOutInterpolator.set(1000);

	Interpolator linearInterpolator;
	linearInterpolator.set(100);

	onPressAnimation.addComponent(AnimationTarget::SCALE_X, elasticOutInterpolator, 1, 0, true, 1.25f, 0, false);
	onPressAnimation.addComponent(AnimationTarget::SCALE_Y, elasticOutInterpolator, 1, 0, true, 1.25f, 0, false);
	onPressAnimation.addComponent(AnimationTarget::X, elasticOutInterpolator, 0, 0, true, -0.125f, 0, false);
	onPressAnimation.addComponent(AnimationTarget::Y, elasticOutInterpolator, 0, 0, true, -0.125f, 0, false);
	onPressAnimation.addComponent(AnimationTarget::COLOR_MASK, linearInterpolator, 0, 0xffffffff, true, 0, 0xff808080, false);

	onReleaseAnimation.addComponent(AnimationTarget::SCALE_X, elasticOutInterpolator, 1, 0, true, 1, 0, false);
	onReleaseAnimation.addComponent(AnimationTarget::SCALE_Y, elasticOutInterpolator, 1, 0, true, 1, 0, false);
	onReleaseAnimation.addComponent(AnimationTarget::X, elasticOutInterpolator, 0, 0, true, 0, 0, false);
	onReleaseAnimation.addComponent(AnimationTarget::Y, elasticOutInterpolator, 0, 0, true, 0, 0, false);
	onReleaseAnimation.addComponent(AnimationTarget::COLOR_MASK, linearInterpolator, 0, 0xffffffff, true, 0, 0xffffffff, false);
}

const NinePatchData *Button::getDefaultNinePatchData() {

	static NinePatchData defaultNinePatchData;
	static bool ninePatchInitialized = false;
	if (!ninePatchInitialized) {
		defaultNinePatchData.textureRegion = DisplayManager::getSingleton()->getTextureRegion("NONGL.roundCorners");
		defaultNinePatchData.setColumnWidths(3, false, 11, 1, 11);
		defaultNinePatchData.setRowHeights(3, false, 10, 1, 10);
		ninePatchInitialized = true;
	}

	return &defaultNinePatchData;
}

void Button::drawOpaqueParts(SpriteBatch *spriteBatch) {
	if (isVisible()) currentSprite->drawOpaqueParts(spriteBatch);
}

void Button::drawTransparentParts(SpriteBatch *spriteBatch) {
	if (isVisible()) {
		currentSprite->drawTransparentParts(spriteBatch);
		//spriteBatch->setLineThickness(2);
		//spriteBatch->drawLine(currentSprite->getCenterX(), currentSprite->getCenterY(), -0.5f, currentSprite->getCenterX() + rotationPivotXDisplacement, currentSprite->getCenterY() + rotationPivotYDisplacement, -0.5f, 0xff00ff00);
		if (textView) textView->drawTransparentParts(spriteBatch);
	}
}

void Button::drawAllInOrder(SpriteBatch *spriteBatch) {
	if (isVisible()) {
		currentSprite->drawAllInOrder(spriteBatch);
		if (textView) textView->drawTransparentParts(spriteBatch);
	}
}

void Button::update(float elapsedTimeMillis) {

	// Update animation,
	onPressAnimation.update(elapsedTimeMillis);
	onReleaseAnimation.update(elapsedTimeMillis);

	if (!isVisible()) return;

	// Check pressed state,
	//if (isEnabled() && isTouched()) {
	if (isEnabled() && pressed) {
		currentSprite = &downSprite;
	} else {
		currentSprite = &upSprite;
	}

	// Set new attributes,
	currentSprite->setLeft(x + localAnimationX);
	currentSprite->setBottom(y + localAnimationY);
	currentSprite->setDepth(maxZ);
	currentSprite->setScale(
			(width  / currentSprite->getWidth() ) * scaleX * localAnimationScaleX,
			(height / currentSprite->getHeight()) * scaleY * localAnimationScaleY);
	currentSprite->setRotation(rotation);
	currentSprite->setColorMask(multiplyColors(color, colorMask));

	if (textView) {
		textView->z = minZ;
		textView->width = width - (textPaddingX*2);
		textView->height = height - (textPaddingY*2);
		textView->scaleX = scaleX * localAnimationScaleX;
		textView->scaleY = scaleY * localAnimationScaleY;
		textView->textColor = multiplyColors(textColor, colorMask);
		textView->textScaleX = textScaleX;
		textView->textScaleY = textScaleY;
		textView->setFont(font);
		// TODO: textView->rotation = rotation;
	}

	// Set sprite and text locations,
	if (rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {
		float cosAngle = cosf(rotation);
		float sinAngle = sinf(rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle) ;
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		float newCenterX = currentSprite->getCenterX() + rotationPivotXDisplacement + rotatedDisplacementX;
		float newCenterY = currentSprite->getCenterY() + rotationPivotYDisplacement + rotatedDisplacementY;

		currentSprite->setCenter(newCenterX, newCenterY);
		if (textView) textView->setCenter(newCenterX, newCenterY);

	} else {
		if (textView) {
			textView->setLeft(x + textPaddingX + localAnimationX);
			textView->setBottom(y + textPaddingY + localAnimationY);
		}
	}
}

bool Button::onTouchEvent(const TouchEvent *event) {

	touchState = event->touchState;

	if (!isEnabled()) return false;

	/*
	if ((event->type == TouchEvent::TOUCH_DOWN) ||
		(event->type == TouchEvent::TOUCH_DRAG) ||
		(event->type == TouchEvent::TOUCH_UP)) {

		return testHit(event->x, event->y);
	}
	*/

	switch (event->type) {

		case TouchEvent::CLICK:
			if (isEnabled() && testHit(event->x, event->y))	{
				if (clickSound) clickSound->play(randomFloatRange(MIN_SOUND_PITCH, MAX_SOUND_PITCH));
				if (buttonListener) buttonListener->buttonOnClick(this);

				return true;
			}
			break;

		case TouchEvent::TOUCH_DOWN:
			if (consumesTouches && isEnabled() && testHit(event->x, event->y))	{
				pressingPointerId = event->pointerId;
				pressed = true;
				attachToLocalAnimation(&onPressAnimation);
				onPressAnimation.start();
				onReleaseAnimation.reset();
				if (buttonListener) buttonListener->buttonOnPressed(this);
				return true;
			}
			break;

		case TouchEvent::TOUCH_UP:
			if (pressed && (pressingPointerId == event->pointerId)) {
				pressed = false;
				attachToLocalAnimation(&onReleaseAnimation);
				onReleaseAnimation.start();
				onPressAnimation.reset();
				if (buttonListener) buttonListener->buttonOnReleased(this);
				return true;
			}
			break;

		default:
			break;
	}

	return false;
}

void Button::attachToAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += x;
				currentComponent->setTargetAddress(&x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += y;
				currentComponent->setTargetAddress(&y);
				break;

			case AnimationTarget::YAW:
				currentComponent->setTargetAddress(&rotation);
				break;

			default:
				break;
		}
	}
}

void Button::setTextureRegions(const Ngl::TextureRegion *buttonUpRegion, const Ngl::TextureRegion *buttonDownRegion, bool resize) {

	if ((!buttonUpRegion) && (!buttonDownRegion)) {

		// Use default nine-patch,
		const NinePatchData *defaultNinePatch = getDefaultNinePatchData();
		upSprite  .setNinePatch(defaultNinePatch);
		downSprite.setNinePatch(defaultNinePatch);
		if (resize) {
			this->width  = upSprite.getWidth();
			this->height = upSprite.getHeight();
		}
		return ;
	} else if (!buttonDownRegion) {
		buttonDownRegion = buttonUpRegion;
	} else if (!buttonUpRegion) {
		buttonUpRegion   = buttonDownRegion;
	}

	if (resize) {
		this->width  = buttonUpRegion->originalWidth ;
		this->height = buttonUpRegion->originalHeight;
	}

	upSprite  .setTextureRegion(buttonUpRegion  );
	downSprite.setTextureRegion(buttonDownRegion);
}

bool Button::testHit(float x, float y) {
	if ((getLeft() <= x) &&
		 (getBottom() <= y) &&
		 (getRight() > x) &&
		 (getTop() > y)) {

		return true;
	}
	return false;
}

bool Button::isTouched() {

	if (touchState == 0) {
		return false;
	} else {

		// Get untransformed coordinates,
		float left, bottom;
		float right, top;
		DisplayManager *displayManager = DisplayManager::getSingleton();
		displayManager->getDesignCoords(getLeft (), getBottom(), &left , &bottom);
		displayManager->getDesignCoords(getRight(), getTop   (), &right, &top   );

		return touchState->isRectangleTouched(left, bottom, right, top);
	}
}

void Button::attachToLocalAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::const_iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::const_iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * getScaledWidth() * localAnimationScaleX;
				currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * getScaledWidth() * localAnimationScaleX;
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += localAnimationX;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   += localAnimationX;
				currentComponent->setTargetAddress(&localAnimationX);
			break;

			case AnimationTarget::Y:
				currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * getScaledHeight() * localAnimationScaleY;
				currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * getScaledHeight() * localAnimationScaleY;
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += localAnimationY;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   += localAnimationY;
				currentComponent->setTargetAddress(&localAnimationY);
			break;

			case AnimationTarget::SCALE_X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * localAnimationScaleX;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * localAnimationScaleX;
				currentComponent->setTargetAddress(&localAnimationScaleX);
				break;

			case AnimationTarget::SCALE_Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * localAnimationScaleY;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * localAnimationScaleY;
				currentComponent->setTargetAddress(&localAnimationScaleY);
				break;

			case AnimationTarget::COLOR_MASK:
				currentComponent->initialValueInt = currentComponent->initialValueIntBackup;
				currentComponent->finalValueInt   = currentComponent->finalValueIntBackup  ;
				if (currentComponent->initialValueRelative) currentComponent->initialValueInt = multiplyColors(currentComponent->initialValueInt, colorMask);
				if (currentComponent->  finalValueRelative) currentComponent->finalValueInt   = multiplyColors(currentComponent->finalValueInt  , colorMask);
				currentComponent->setTargetAddress(&colorMask, true);
				break;

			default:
				break;
		}
	}
}
