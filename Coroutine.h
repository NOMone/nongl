#pragma once

#include <Nongl/ManagedObject.h>

#include <vector>
#include <functional>

namespace Ngl {

    class Coroutine {

        struct AliveToken {};
        Ngl::shared_ptr<AliveToken> aliveToken{new AliveToken()};

        std::vector<std::function<bool()> > functions;

        int32_t currentFunctionIndex=0;
        float timeToSleepMillis=0;
        bool sleepingIndefinitely=false;

    public:

        void add(const std::function<bool()> &function);
        void sleep(float durationMillis) { sleepingIndefinitely = false; timeToSleepMillis = durationMillis; }
        void sleepIndefinitely() { sleepingIndefinitely = true; timeToSleepMillis = 0; }
        void awake() { sleepingIndefinitely = false; timeToSleepMillis = 0; }
        void next() { currentFunctionIndex++; }
        void run();

        int32_t getCurrentStepIndex() { return currentFunctionIndex; }
    };
}
