#pragma once

#include <Nongl/TextUtils.h>
#include <Nongl/ManagedObject.h>

#include <vector>
#include <functional>

class Activity;


namespace JSON {
    class Value;
}

namespace Ngl {

    class HttpRequest;

    class HttpRequestData {
        friend class HttpRequest;
        TextBuffer dataText;
    public:
        HttpRequestData() {}
        HttpRequestData(const HttpRequestData &src) {
            this->dataText = src.dataText;
        }

        HttpRequestData &reset() { dataText = TextBuffer(); }
        HttpRequestData &add(const TextBuffer &name, const TextBuffer &value);
        HttpRequestData &encodeAndAdd(const TextBuffer &name, const TextBuffer &value);

        TextBuffer toString() const { return dataText; }
    };

    enum class HttpRequestType { POST, GET };

    class HttpRequest {
        TextBuffer url;
        HttpRequestData data;
        HttpRequestType type;
    public:

        void setType(HttpRequestType type) { this->type = type; }
        void setUrl(const TextBuffer &url) { this->url = url; }
        void setData(const HttpRequestData &data) { this->data = data; }
        void resetData() { this->data.reset(); }
        void addData(const TextBuffer &name, const TextBuffer &value) { data.add(name, value); }
        void encodeAndAddData(const TextBuffer &name, const TextBuffer &value) { data.encodeAndAdd(name, value); }
        std::vector<uint8_t> send();
    };

    class AsyncRequest {

    protected:
        HttpRequest httpRequest;
        std::vector<uint8_t> requestResponse;

        struct AliveToken {};
        Ngl::shared_ptr<AliveToken> aliveToken{new AliveToken()};

        virtual void advanceState()=0;
        void pollStateChanges(Activity *activity=nullptr);

        void sendRequestInParallel();
        void sendRequest();

    public:
        virtual ~AsyncRequest() {}
        virtual bool performStateAction()=0;
    };

    ///////////////////////////////
    // Simple Async Request
    ///////////////////////////////

    class SimpleAsyncRequest : public AsyncRequest {

        Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onJsonResponseReceivedListener;
        Ngl::ManagedPointer<std::function<void(std::vector<uint8_t> &response, const TextBuffer &errorMessage)> > onPlainResponseReceivedListener;

        enum class State : int32_t { INITIAL, RECEIVED_RESPONSE };
        State state = State::INITIAL;

        void advanceState() { state = static_cast<State>(static_cast<int32_t>(state) + 1); }
        bool performStateAction();
        void receivedResponseState();
        SimpleAsyncRequest(const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data);

    public:
        SimpleAsyncRequest(
                const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
                Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onJsonResponseReceivedListener);
        SimpleAsyncRequest(
                const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
                Ngl::ManagedPointer<std::function<void(std::vector<uint8_t> &response, const TextBuffer &errorMessage)> > onPlainResponseReceivedListener);
    };
}
