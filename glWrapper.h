#if __APPLE__

    #include "TargetConditionals.h"

    #if TARGET_OS_IPHONE

        #define VBO_ENABLED true
        #define VERTEX_ARRAYS_ENABLED true

        #define LOWP "lowp"
        #define MEDIUMP "mediump"
        #define PRECISION_MEDIUMP_FLOAT "precision mediump float;\n"

        #include <OpenGLES/ES2/gl.h>

    #elif TARGET_OS_MAC

        #define VBO_ENABLED true
        #define VERTEX_ARRAYS_ENABLED false

        #define LOWP
        #define MEDIUMP
        #define PRECISION_MEDIUMP_FLOAT

        #include <OpenGL/gl.h>
        #include <OpenGL/glext.h>

    #endif

#elif defined(DESKTOP)

    #define VBO_ENABLED true
    #define VERTEX_ARRAYS_ENABLED false

    #define LOWP ""
    #define MEDIUMP ""
    #define PRECISION_MEDIUMP_FLOAT ""

    #include <GL/glew.h>
    #include <GL/gl.h>

#elif defined(__ANDROID__)

    #define VBO_ENABLED true
    #define VERTEX_ARRAYS_ENABLED true

	#define LIB_KTX_ENABLED true
	#define KTX_OPENGL_ES2 1
	#define SUPPORT_SOFTWARE_ETC_UNPACK 0

    #define LOWP "lowp"
    #define MEDIUMP "mediump"
    #define PRECISION_MEDIUMP_FLOAT "precision mediump float;\n"
    #define PRECISION_LOWP_FLOAT "precision lowp float;\n"

    #include <GLES2/gl2.h>

#elif defined(EMSCRIPTEN)

    #define VBO_ENABLED true
    #define VERTEX_ARRAYS_ENABLED false

    #define LOWP "lowp"
    #define MEDIUMP "mediump"
    #define PRECISION_MEDIUMP_FLOAT "precision mediump float;\n"

    #include <GLES2/gl2.h>

#endif
