#include "View.h"

#include "SpriteBatch.h"
#include "utils.h"

using namespace Ngl;

/////////////////////////////
// View animation listener
/////////////////////////////

void ViewAnimationListener::onAnimationComponentApplied(AnimationTarget::Target target) {

	switch (target) {
		case AnimationTarget::X: view->setLeft(x); break;
		case AnimationTarget::Y: view->setBottom(y); break;
		case AnimationTarget::SCALE_X: view->setScale(scaleX, view->getScaleY()); break;
		case AnimationTarget::SCALE_Y: view->setScale(view->getScaleX(), scaleY); break;

		case AnimationTarget::SCALE_ABOUT_CENTER: {
			float centerX = view->getCenterX();
			float centerY = view->getCenterY();
			view->setScale(scaleAboutCenter, scaleAboutCenter);
			view->setCenter(centerX, centerY);
			break;
		}

		case AnimationTarget::COLOR_MASK: view->setColorMask(colorMask); break;

		default:
			break;
	}
}

/////////////////////////////
// View
/////////////////////////////

Rect View::getBoundingRect() {
	return Rect(getLeft(), getBottom(), getRight(), getTop());
}

bool View::withinParentBounds() {
	return parentBounds.intersects({getLeft(), getBottom(), getRight(), getTop()});
}

void View::attachToAnimation(Animation *animation) {

	if (!this->animationListener) this->animationListener = new ViewAnimationListener(this);
	animation->setAnimationListener(this->animationListener);
	ViewAnimationListener *animationListener = (ViewAnimationListener *) this->animationListener;

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += getLeft();
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += getLeft();
				currentComponent->setTargetAddress(&animationListener->x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += getBottom();
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += getBottom();
				currentComponent->setTargetAddress(&animationListener->y);
				break;

			case AnimationTarget::SCALE_X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * getScaleX();
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * getScaleX();
				currentComponent->setTargetAddress(&animationListener->scaleX);
				break;

			case AnimationTarget::SCALE_Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * getScaleY();
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * getScaleY();
				currentComponent->setTargetAddress(&animationListener->scaleY);
				break;

			case AnimationTarget::SCALE_ABOUT_CENTER:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat = currentComponent->initialValueFloatBackup * getScaleX();
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   = currentComponent->finalValueFloatBackup   * getScaleX();
				currentComponent->setTargetAddress(&animationListener->scaleAboutCenter);
				break;

			case AnimationTarget::COLOR_MASK:
				currentComponent->initialValueInt = currentComponent->initialValueIntBackup;
				currentComponent->finalValueInt   = currentComponent->finalValueIntBackup  ;
				if (currentComponent->initialValueRelative) currentComponent->initialValueInt = multiplyColors(currentComponent->initialValueInt, getColorMask());
				if (currentComponent->  finalValueRelative) currentComponent->finalValueInt   = multiplyColors(currentComponent->finalValueInt  , getColorMask());
				currentComponent->setTargetAddress(&animationListener->colorMask, true);
				break;

			default:
				break;
		}
	}
}

/////////////////////////////
// Color view
/////////////////////////////

void ColorView::drawOpaqueParts(SpriteBatch *spriteBatch) {
	if ((color & 0xff000000) == 0xff000000) {
		spriteBatch->drawFilledRect(getLeft(), getBottom(), getScaledWidth(), getScaledHeight(), z, color);
	}
}

void ColorView::drawTransparentParts(SpriteBatch *spriteBatch) {
	int viewAlpha = color & 0xff000000;
	if ((viewAlpha != 0xff000000) && (viewAlpha != 0)) {
		spriteBatch->drawFilledRect(getLeft(), getBottom(), getScaledWidth(), getScaledHeight(), z, color);
	}
}

void ColorView::drawAllInOrder(SpriteBatch *spriteBatch) {
	if ((color & 0xff000000) != 0) {
		spriteBatch->drawFilledRect(getLeft(), getBottom(), getScaledWidth(), getScaledHeight(), z, color);
	}
}

/////////////////////////////
// Touchable view
/////////////////////////////

bool TouchableView::testHit(float x, float y) {
	bool notTouched = (x < getLeft()) || (x >= getRight()) || (y < getBottom()) || (y >= getTop());
	return !notTouched;
}

bool TouchableView::onTouchEvent(const TouchEvent *event) {
    
	if (discardOutOfBounds && (!testHit(event->downX, event->downY))) return false;
    if (onTouchTask) return onTouchTask->run((void *) event);

    return false;
}
