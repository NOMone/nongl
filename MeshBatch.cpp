#include "MeshBatch.h"

#include "Mesh.h"
#include "glWrapper.h"
#include "SystemLog.h"
#include "DisplayManager.h"
using namespace Ngl;

#include <cstring>

#define MAX_VERTICES_COUNT 65535

// TODO: maybe implement these one day?
//#define DRAW_WIRE_FRAME
//#define DRAW_WIRE_FRAME_ONLY

MeshBatch::MeshBatch(bool vboPowered /*false*/, bool isStatic /*false*/) {

	this->vboPowered = VBO_ENABLED && vboPowered;
	this->isStatic = isStatic;
}

MeshBatch::~MeshBatch() {

	if (verticesVboId) {
		glDeleteBuffers(1, (GLuint *) &verticesVboId);
		glDeleteBuffers(1, (GLuint *) &indicesVboId);
	}
}

void MeshBatch::prepareForBatch(const shared_ptr<Material> &material, int32_t verticesCount, int32_t indicesCount) {

	// Check if a flush should be performed due to material changing,
	// TODO: also check the material version, maybe it was modified,
	if (this->material != material) {
		flush();
		this->material = material;
		flushesDueToMaterialChangeCount++;
	}

	// Check if vertices limit was reached,
	int32_t newBatchedVerticesCount = batchedVerticesCount + verticesCount;
	if (newBatchedVerticesCount > MAX_VERTICES_COUNT) {
		LOGE("Warning: mesh batch flushed due to overflow...");
		flush();
	}

	// Resize the vectors if needed,
	// Vertices,
	int32_t requiredVerticesBufferSize = material->getFormatedVerticesDataSizeBytes(newBatchedVerticesCount);
	int32_t pushCount = requiredVerticesBufferSize - (int32_t) verticesData.size();
	for (; pushCount > 0; pushCount--) verticesData.push_back(0);

	// Indices,
	int32_t requiredIndicesBufferSize = batchedIndicesCount + indicesCount;
	pushCount = requiredIndicesBufferSize - (int32_t) indices.size();
	for (; pushCount > 0; pushCount--) indices.push_back(0);

	// Mark vbos as dirty,
	vbosModified = true;
}

void MeshBatch::batch(
		const shared_ptr<Material> &material,
		const uint8_t *formattedVerticesData, int32_t verticesCount,
		const uint16_t *indices, int32_t indicesCount) {

	// Prepare buffers for batching,
	prepareForBatch(material, verticesCount, indicesCount);

	// Batch the data,
	// Vertices,
	int32_t verticesToBatchSize = material->getFormatedVerticesDataSizeBytes(verticesCount);
	memcpy(&verticesData[batchedVerticesSizeBytes], formattedVerticesData, verticesToBatchSize);

	// Indices,
	for (int32_t i=0; i<indicesCount; i++) {

		// Copy and offset the vertex indices,
		// TODO: remove this after renaming...
		this->indices[batchedIndicesCount+i] = indices[i] + batchedVerticesCount;
	}

	// Update batched counts,
	batchedVerticesSizeBytes += verticesToBatchSize;
	batchedVerticesCount += verticesCount;
	batchedIndicesCount += indicesCount;

	// Mark vbos as dirty,
	vbosModified = true;
}

void MeshBatch::clear() {
	batchedVerticesSizeBytes = batchedVerticesCount = batchedIndicesCount = 0;
}

void MeshBatch::onVbosDiscarded() {
	verticesVboId = indicesVboId = 0;
}

void MeshBatch::adjustAndPrepareVbos() {

	#define SHORT_BYTES 2

	if ((!verticesVboId) || (verticesData.capacity() > verticesVboSizeBytes)) {

		if (verticesVboId) glDeleteBuffers(1, (GLuint *) &verticesVboId);
		verticesVboSizeBytes = (int32_t) verticesData.capacity();

		// Prepare vertices VBO,
		glGenBuffers(1, (GLuint *) &verticesVboId);
		glBindBuffer(GL_ARRAY_BUFFER, verticesVboId);
		glBufferData(GL_ARRAY_BUFFER, verticesVboSizeBytes, (void *) &verticesData[0], (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW
	} else if (vbosModified) {
		glBindBuffer(GL_ARRAY_BUFFER, verticesVboId);
		//glBufferData(GL_ARRAY_BUFFER, vertexDataSize, (void *) &vertexData[0], GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferData(GL_ARRAY_BUFFER, verticesVboSizeBytes, 0, (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW
		glBufferSubData(GL_ARRAY_BUFFER, 0, batchedVerticesSizeBytes, (void *) &verticesData[0]);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, verticesVboId);
	}

	if ((!indicesVboId) || (indices.capacity() > indicesVboSizeShorts)) {

		if (indicesVboId) glDeleteBuffers(1, (GLuint *) &indicesVboId);
		indicesVboSizeShorts = (int32_t) indices.capacity();

		// Prepare the indices VBO,
		glGenBuffers(1, (GLuint *) &indicesVboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesVboSizeShorts * SHORT_BYTES, (void *) &indices[0], (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
	} else if (vbosModified) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboId);
		//glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize * SHORT_BYTES, (void *) &indices[0], GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesVboSizeShorts * SHORT_BYTES, 0, (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, batchedIndicesCount * SHORT_BYTES, (void *) &indices[0]);
	} else {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVboId);
	}

	vbosModified = false;
}

void MeshBatch::flush() {
	draw();
	clear();
}

void MeshBatch::changeBlendMode(bool enabled) {

	if (this->blendEnabled != enabled) {
		flush();
		this->blendEnabled = enabled;
	}
}

void MeshBatch::draw() {

	// TODO: one line check gl errors...

	// If we should skip drawing entirely,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	if (displayManager->isEntirelyClipped()) return;

	// If nothing drawn, return,
	if (indices.empty()) return;

	// If no material specified or material expired, crash!
	if (material.expired()) {
    	throw std::runtime_error("Can't draw mesh batch without a valid material.");
	}

	// If VBO based, adjust VBOs,
	if ((!VERTEX_ARRAYS_ENABLED) || vboPowered) {

		adjustAndPrepareVbos();
		material->prepareMaterial(0);

		glDrawElements(GL_TRIANGLES, batchedIndicesCount, GL_UNSIGNED_SHORT, 0);

		// TODO: how wrapping these methods into Nongl methods to avoid unnecessary call overhead?
		// TODO: how about we move these two lines to the else statement? Unbind only when arrays are
		// going to be used?
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	} else {

		material->prepareMaterial(&verticesData[0]);
		glDrawElements(GL_TRIANGLES, batchedIndicesCount, GL_UNSIGNED_SHORT, (void *) &indices[0]);
	}
}
