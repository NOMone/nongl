#pragma once

#include <vector>

class Sprite;
class SpriteBatch;
class TextureRegion;

class SegmentedSpriteParameters {
public:
	int columnsCount, rowsCount;
	const char *regionPrefix;
	float imageWidth, imageHeight;

	inline SegmentedSpriteParameters() {}
	SegmentedSpriteParameters(
			int columnsCount, int rowsCount,
			const char *regionPrefix,
			float imageWidth, float imageHeight);

	void set(
			int columnsCount, int rowsCount,
			const char *regionPrefix,
			float imageWidth, float imageHeight);
};

class SegmentedSprite {

	int columnsCount, rowsCount;
	std::vector<Sprite *> regionSprites;

	int segmentWidth, segmentHeight;

	float x, y, z;
	float width, height;
	float scaleX, scaleY;
	float rotation;
	int colorMask;

	bool modified;

	bool hasOpaqueSegments, hasTransparentSegments;

protected:
	void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool transparent);

public:

	inline SegmentedSprite() {}
	SegmentedSprite(const SegmentedSpriteParameters *params);
	SegmentedSprite(int columnsCount, int rowsCount, const char *regionPrefix, float imageWidth, float imageHeight);
	~SegmentedSprite();

	void set(int columnsCount, int rowsCount, const char *regionPrefix, float imageWidth, float imageHeight);

	inline float getWidth() { return width; }
	inline float getHeight() { return height; }
	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + (width * scaleX); }
	inline float getTop() { return y + (height * scaleY); }
	inline float getCenterX() { return x + (width * scaleX * 0.5f); }
	inline float getCenterY() { return y + (height * scaleY * 0.5f); }
	inline float getDepth() { return z; }
	inline float getScaleX() { return scaleX; }
	inline float getScaleY() { return scaleY; }
	inline float getRotation() { return rotation; }

	inline void setLeft(float left) { x = left; modified = true; }
	inline void setBottom(float bottom) { y = bottom; modified = true; }
	inline void setRight(float right) { x = right - (width * scaleX); modified = true; }
	inline void setTop(float top) { y = top - (height * scaleY); modified = true; }
	inline void setCenterX(float centerX) { x = centerX - (width * scaleX * 0.5f); modified = true; }
	inline void setCenterY(float centerY) { y = centerY - (height * scaleY * 0.5f); modified = true; }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); modified = true; }
	inline void setDepth(float z) { this->z = z; modified = true; }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; modified = true; }
	inline void setRotation(float rotation) { this->rotation = rotation; modified = true; }
	inline void setColorMask(int colorMask) { this->colorMask = colorMask; modified = true; }

	inline void drawOpaqueParts(SpriteBatch *spriteBatch) { if (hasOpaqueSegments) draw(spriteBatch, false, false); }
	inline void drawTransparentParts(SpriteBatch *spriteBatch) { if (hasTransparentSegments) draw(spriteBatch, false, true); }
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }
};
