#include "PixelMap.h"

#include <Nongl/LodePng/LodePng.h>
#include <Nongl/StbImage/StbImage.h>

#include <Nongl/SystemLog.h>
#include <Nongl/TextUtils.h>
#include <Nongl/Texture.h>
#include <Nongl/TextureRegion.h>
#include <Nongl/DisplayManager.h>
#include <Nongl/FrameBuffer.h>
#include <Nongl/Nongl.h>
#include <Nongl/File.h>
#include <Nongl/utils.h>
#include <Nongl/glWrapper.h>

#include <stdexcept>

PixelMap::PixelMap(int width, int height) {
	this->width = width;
	this->height = height;
	data.resize(width*height, 0);
}

PixelMap::PixelMap(const Ngl::File &file, PixelMapFileType fileType, bool flipImageVertically) {

    switch(fileType) {
        case PixelMapFileType::PNG:         setFromPng       (file, flipImageVertically); return;
        case PixelMapFileType::JPG:         setFromJpg       (file, flipImageVertically); return;
        case PixelMapFileType::AUTO_DETECT: setFromAutoDetect(file, flipImageVertically); return;
    }
}

PixelMap::PixelMap(const std::vector<uint8_t> &imageData, PixelMapFileType fileType, bool flipImageVertically) {

    switch(fileType) {
        case PixelMapFileType::PNG:         setFromPng       (imageData, flipImageVertically); return;
        case PixelMapFileType::JPG:         setFromJpeg      (imageData, flipImageVertically); return;
        case PixelMapFileType::AUTO_DETECT: setFromAutoDetect(imageData, flipImageVertically); return;
    }
}

void PixelMap::setFromPng(const Ngl::File &pngFile, bool flipImageVertically) {

	// Load file,
	std::vector<uint8_t> fileData;
	pngFile.read(fileData);

	try {
	    setFromPng(fileData, flipImageVertically);
	} catch (const std::exception &e) {
        throw std::runtime_error(
                TextBuffer(e.what())
                .append(", file: ")
                .append(pngFile.getFullPath())
                .getConstText());
	}
}

void PixelMap::setFromPng(const std::vector<uint8_t> &pngData, bool flipImageVertically) {

	// Decode png,
    unsigned char* buffer;
    unsigned error = lodepng_decode_memory(
            &buffer,
            (uint32_t *) &width, (uint32_t *) &height,
            &pngData[0], pngData.size(), LCT_RGBA, 8);

    if (buffer && !error) {

        // No errors, set data from buffer,
        data.assign(
                 (int32_t *) buffer,
                ((int32_t *) buffer) + (width*height));
        free(buffer);

    } else {
        throw std::runtime_error(
                TextBuffer("Couldn't create PixelMap, couldn't decode PNG Data")
                .append(", error: ").append((int32_t) error)
                .getConstText());
    }

    if (!flipImageVertically) flipVertically();
}

void PixelMap::setFromJpg (const Ngl::File &jpgFile, bool flipImageVertically) {
    setFromAutoDetect(jpgFile, flipImageVertically);
}

void PixelMap::setFromJpeg(const std::vector<uint8_t> &jpegData, bool flipImageVertically) {
    setFromAutoDetect(jpegData, flipImageVertically);
}

void PixelMap::setFromAutoDetect(const Ngl::File &imageFile, bool flipImageVertically) {

    // Load file,
    std::vector<uint8_t> fileData;
    imageFile.read(fileData);

    try {
        setFromJpeg(fileData, flipImageVertically);
    } catch (const std::exception &e) {
        throw std::runtime_error(
                TextBuffer(e.what())
                .append(", file: ")
                .append(imageFile.getFullPath())
                .getConstText());
    }
}

void PixelMap::setFromAutoDetect(const std::vector<uint8_t> &imageData, bool flipImageVertically) {

    // Note: not thread-safe ...?
    stbi_set_flip_vertically_on_load(flipImageVertically);

    int componentsPerPixel;
    uint8_t *buffer = stbi_load_from_memory(
            &imageData[0], (int32_t) imageData.size(),
            &width, &height,
            &componentsPerPixel, 4);

    if (buffer) {

        // Fill data from buffer,
        data.assign(
                 (int32_t *) buffer,
                ((int32_t *) buffer) + (width*height));

        stbi_image_free(buffer);

    } else {
        throw std::runtime_error(
                TextBuffer("Couldn't create PixelMap, couldn't decode JPEG data")
                .append(", error: ").append(stbi_failure_reason())
                .getConstText());
    }
}

void PixelMap::flipVertically() {

    int32_t topPointer = 0;
    int32_t bottomPointer = width * (height-1);
    int32_t halfHeight = height >> 1; // Works for even and odd.

    for (int32_t j=0; j<halfHeight; j++) {
        for (int32_t i=0; i<width; i++) {

            int32_t temp = data[topPointer+i];
            data[topPointer+i] = data[bottomPointer+i];
            data[bottomPointer+i] = temp;
        }
        topPointer += width;
        bottomPointer -= width;
    }
}

void PixelMap::drawPixelMap(
		const PixelMap &sourcePixelMap,
		int sourceLeft, int sourceBottom, int destinationLeft, int destinationBottom) {

	int sourceRight = sourceLeft   + sourcePixelMap.width ;
	int sourceTop   = sourceBottom + sourcePixelMap.height;
	int destinationRight = destinationLeft   + sourcePixelMap.width ;
	int destinationTop   = destinationBottom + sourcePixelMap.height;

	// Completely outside source,
	if ((sourceLeft   >= sourcePixelMap.width ) ||
		(sourceBottom >= sourcePixelMap.height) ||
		(sourceRight  <= 0) ||
		(sourceTop    <= 0)) {
		return ;
	}

	// Source clipping,
	// Lower bound,
	if (sourceLeft < 0) {
		destinationLeft -= sourceLeft;
		sourceLeft = 0;
	}

	if (sourceBottom < 0) {
		destinationBottom -= sourceBottom;
		sourceBottom = 0;
	}

	// Upper bound,
	if (sourceRight > sourcePixelMap.width) {
		destinationRight -= (sourceRight - sourcePixelMap.width);
		sourceRight = sourcePixelMap.width;
	}

	if (sourceTop > sourcePixelMap.height) {
		destinationTop -= (sourceTop - sourcePixelMap.height);
		sourceTop = sourcePixelMap.height;
	}

	// Completely outside destination,
	if ((destinationLeft   >=  width) ||
		(destinationBottom >= height) ||
		(destinationRight  <= 0) ||
		(destinationTop    <= 0)) {
		return ;
	}

	// Destination clipping,
	// Lower bound,
	if (destinationLeft < 0) {
		sourceLeft -= destinationLeft;
		destinationLeft = 0;
	}

	if (destinationBottom < 0) {
		sourceBottom -= destinationBottom;
		destinationBottom = 0;
	}

	// Upper bound,
	if (destinationRight > width) {
		sourceRight -= (destinationRight - width);
		destinationRight = width;
	}

	if (destinationTop > height) {
		sourceTop -= (destinationTop - height);
		destinationTop = height;
	}

	// Make sure the source coordinates are still valid,
	if ((sourceLeft   >= sourceRight) ||
		(sourceBottom >= sourceTop  ) ||
		(sourceRight  <= 0          ) ||
		(sourceTop    <= 0          ) ||
		(sourceLeft   >= sourcePixelMap.width ) ||
		(sourceBottom >= sourcePixelMap.height)) {

		return ;
	}

	// Do the blit,
	int blitWidth  = destinationRight - destinationLeft  ;
	int blitHeight = destinationTop   - destinationBottom;

	int sourceIndex      = sourceLeft      + (sourceBottom      * sourcePixelMap.width);
	int destinationIndex = destinationLeft + (destinationBottom *                width);

	for (; blitHeight; blitHeight--) {
		for (int i=0; i<blitWidth; i++) {
			data[destinationIndex+i] = sourcePixelMap.data[sourceIndex+i];
		}
		destinationIndex += width;
		sourceIndex += sourcePixelMap.width;
	}
}


void PixelMap::drawPixelMapStretch(
		const PixelMap &sourcePixelMap,
		int sourceLeft, int sourceBottom, int sourceWidth, int sourceHeight,
		int destinationLeft, int destinationBottom, int destinationWidth, int destinationHeight) {

	int sourceRight = sourceLeft   + sourceWidth ;
	int sourceTop   = sourceBottom + sourceHeight;
	int destinationRight = destinationLeft   + destinationWidth ;
	int destinationTop   = destinationBottom + destinationHeight;

	// Completely outside source,
	if ((sourceLeft   >= sourcePixelMap.width ) ||
		(sourceBottom >= sourcePixelMap.height) ||
		(sourceRight  <= 0) ||
		(sourceTop    <= 0)) {
		return ;
	}

	// Source clipping,
	float destinationPerSourceWidthRatio  = destinationWidth  / (float) sourceWidth ;
	float destinationPerSourceHeightRatio = destinationHeight / (float) sourceHeight;

	// Lower bound,
	if (sourceLeft < 0) {
		destinationLeft -= sourceLeft * destinationPerSourceWidthRatio;
		sourceLeft = 0;
	}

	if (sourceBottom < 0) {
		destinationBottom -= sourceBottom * destinationPerSourceHeightRatio;
		sourceBottom = 0;
	}

	// Upper bound,
	if (sourceRight > sourcePixelMap.width) {
		destinationRight -= (sourceRight - sourcePixelMap.width) * destinationPerSourceWidthRatio;
		sourceRight = sourcePixelMap.width;
	}

	if (sourceTop > sourcePixelMap.height) {
		destinationTop -= (sourceTop - sourcePixelMap.height) * destinationPerSourceHeightRatio;
		sourceTop = sourcePixelMap.height;
	}

	// Completely outside destination,
	if ((destinationLeft   >=  width) ||
		(destinationBottom >= height) ||
		(destinationRight  <= 0) ||
		(destinationTop    <= 0)) {
		return ;
	}

	// Destination clipping,
	float sourcePerDestinationWidthRatio  = 1 / destinationPerSourceWidthRatio ;
	float sourcePerDestinationHeightRatio = 1 / destinationPerSourceHeightRatio;

	// Lower bound,
	if (destinationLeft < 0) {
		sourceLeft -= destinationLeft * sourcePerDestinationWidthRatio;
		destinationLeft = 0;
	}

	if (destinationBottom < 0) {
		sourceBottom -= destinationBottom * sourcePerDestinationHeightRatio;
		destinationBottom = 0;
	}

	// Upper bound,
	if (destinationRight > width) {
		sourceRight -= (destinationRight - width) * sourcePerDestinationWidthRatio;
		destinationRight = width;
	}

	if (destinationTop > height) {
		sourceTop -= (destinationTop - height) * sourcePerDestinationHeightRatio;
		destinationTop = height;
	}

	// Make sure the source coordinates are still valid,
	if ((sourceLeft   >= sourceRight) ||
		(sourceBottom >= sourceTop  ) ||
		(sourceRight  <= 0          ) ||
		(sourceTop    <= 0          ) ||
		(sourceLeft   >= sourcePixelMap.width ) ||
		(sourceBottom >= sourcePixelMap.height)) {

		return ;
	}

	// Do the blit,
	sourceWidth  = sourceRight - sourceLeft  ;
	sourceHeight = sourceTop   - sourceBottom;
	destinationWidth  = destinationRight - destinationLeft  ;
	destinationHeight = destinationTop   - destinationBottom;

	float sourceStepX = sourceWidth  / (float) destinationWidth ;
	float sourceStepY = sourceHeight / (float) destinationHeight;

	int destinationIndex = destinationLeft + (destinationBottom * width);

	float currentSourceY = sourceBottom;
	for (; destinationHeight; destinationHeight--) {
		float currentSourceX = sourceLeft + (sourcePixelMap.width * int(currentSourceY));
		for (int i=0; i<destinationWidth; i++) {
			data[destinationIndex+i] = sourcePixelMap.data[(int) currentSourceX];
			currentSourceX += sourceStepX;
		}

		destinationIndex += width;
		currentSourceY += sourceStepY;
	}
}

void PixelMap::clear(int clearColor) {

	int pixelsCount = (int32_t) data.size();
	for (int i=0; i<pixelsCount; i++) {
		data[i] = clearColor;
	}
}


void PixelMap::readFromFrameBuffer(int frameBufferX, int frameBufferY) {
	glReadPixels(frameBufferX, frameBufferY, width, height, GL_RGBA, GL_UNSIGNED_BYTE, &data[0]);
	checkGlError("PixelMap::readFromFrameBuffer, glReadPixels");
}

void PixelMap::readFromTextureRegion(const Ngl::TextureRegion *textureRegion, int xInRegion, int yInRegion) {

	// Since glGetTexImage() is not supported in OpenGL ES 2.0/3.0,
	// this will be a little convoluted,

	// Create framebuffer using the texture,
	FrameBuffer frameBuffer(textureRegion->texture, false);

	// Bind frame buffer,
	FrameBuffer *oldFrameBuffer = Nongl::getBoundFrameBuffer();
	Nongl::bindFrameBuffer(&frameBuffer);

	// Read from frame buffer,
	int remainingRegionWidth = textureRegion->originalWidth - xInRegion;
	int remainingRegionHeight = textureRegion->originalHeight - yInRegion;

	// Maybe the texture region white-space is trimmed, make sure to check if offset exists,
	if ((remainingRegionWidth < width) ||
		(remainingRegionHeight < height) ||
		(xInRegion < 0) ||
		(yInRegion < 0) ||
		(textureRegion->offsetX) ||
		(textureRegion->offsetY) ||
		(textureRegion->originalWidth  - textureRegion->width ) ||
		(textureRegion->originalHeight - textureRegion->height)) {

		// Account for offset,
		// X-axis,
		int srcLeft = xInRegion - textureRegion->offsetX;
		int srcRight = srcLeft + width;
		int paddingLeft = 0;
		if (srcLeft < 0) {
			paddingLeft = -srcLeft;
			srcLeft = 0;
		}
		int paddingRight = 0;
		if (srcRight > textureRegion->width) {
			paddingRight = srcRight - textureRegion->width;
			srcRight = textureRegion->width;
		}

		// Y-axis,
		// offsetY alignment is different from offsetX,
		int correctlyAlignedYOffset = textureRegion->originalHeight - (textureRegion->height + textureRegion->offsetY);
		int srcBottom = yInRegion - correctlyAlignedYOffset;
		int srcTop = srcBottom + height;
		int paddingBottom = 0;
		if (srcBottom < 0) {
			paddingBottom = -srcBottom;
			srcBottom = 0;
		}
		int paddingTop = 0;
		if (srcTop > textureRegion->height) {
			paddingTop = srcTop - textureRegion->height;
			srcTop = textureRegion->height;
		}

		// Read in a smaller pixel map first,
		clear(0);
		int srcWidth = srcRight - srcLeft;
		int srcHeight = srcTop - srcBottom;
		if ((srcWidth > 0) && (srcHeight > 0)) {
			PixelMap tempPixelMap(srcWidth, srcHeight);
			tempPixelMap.readFromFrameBuffer(textureRegion->x + srcLeft, textureRegion->y + srcBottom);
			drawPixelMap(tempPixelMap, 0, 0, paddingLeft, paddingBottom);
		}

	} else {
		readFromFrameBuffer(textureRegion->x + xInRegion, textureRegion->y + yInRegion);
	}

	// Bind the original frame buffer,
	Nongl::bindFrameBuffer(oldFrameBuffer);
}

Ngl::Texture *PixelMap::createTexture() {
	Ngl::Texture *texture = DisplayManager::getSingleton()->createTexture(width, height, GL_LINEAR, GL_LINEAR);
	texture->setTextureRegion(0, 0, width, height, (char *) &data[0]);
	return texture;
}

void PixelMap::saveToFile(const Ngl::File &pngFile, bool flipImageVertically) {

	// TODO: NO NEED TO CHANGE INTO VECTOR!!!!

	// Change vector type for compatibility with LodePng library,
	int scanlineSize = width*4;
	int uncompressedImageSize = scanlineSize*height;
	std::vector<unsigned char> image;
	image.resize(uncompressedImageSize, 0);

	// Might as well flip upside-down while doing so (because PNGs default (0, 0) is
	// at top-left, while Nongl/OpenGl default (0, 0) is at bottom-left,
	int currentDestRowStartIndex;
	int scanLineFeed;
	if (flipImageVertically) {
		currentDestRowStartIndex = uncompressedImageSize - scanlineSize;
		scanLineFeed = -scanlineSize;
	} else {
		currentDestRowStartIndex = 0;
		scanLineFeed = scanlineSize;
	}

	int currentSrcIndex = 0;
	for (int j=0; j<height; j++) {

		int currentDestRowEndIndex = currentDestRowStartIndex + scanlineSize;
		for (int i=currentDestRowStartIndex; i<currentDestRowEndIndex; i+=4) {
			int currentPixel = data[currentSrcIndex++];
			image[i  ] =  currentPixel & 0x000000ff       ;
			image[i+1] = (currentPixel & 0x0000ff00) >>  8;
			image[i+2] = (currentPixel & 0x00ff0000) >> 16;
			image[i+3] = (currentPixel & 0xff000000) >> 24;
		}

		currentDestRowStartIndex += scanLineFeed;
	}

	// Allocate vector for png output,
	std::vector<unsigned char> png;

	// Create PNG,
	unsigned error = lodepng::encode(png, image, width, height, LCT_RGBA, 8);
	if (error) {
		LOGE("Encoding to PNG in pixel map saving error: %s", lodepng_error_text(error));
		return ;
	}

	// Save file,
	pngFile.write(&png[0], (int32_t) png.size(), false);
}

