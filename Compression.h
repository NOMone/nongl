#pragma once

#include "InputStream.h"
#include "OutputStream.h"
#include "BufferedOutputStream.h"
#include "BufferedInputStream.h"
#include "ManagedObject.h"

#include <vector>
#include <cstdint>

namespace Ngl {

	////////////////////////////////////////////////
	// Miniz compression
	////////////////////////////////////////////////

	std::vector<uint8_t> miniZCompress(const void *rawData, int32_t rawDataSize);

	// compressedData: compressed data (output from miniZCompress),
	void miniZUncompress(const void *compressedBlock, std::vector<uint8_t> &outputVector, int32_t offsetInVector);

	////////////////////////////////////////////////
	// Compressing Input Stream
	////////////////////////////////////////////////

	class CompressingInputStream : public InputStream {
		Ngl::ManagedPointer<InputStream> inputStream;
	public:

		CompressingInputStream(ManagedPointer<InputStream> inputStream);

		virtual bool hasData();

		// Reads some (or all) data available from input stream. Returns data size,
		virtual int32_t readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector);

		virtual bool markSupported() { return false; }
	};

	////////////////////////////////////////////////
	// Compressing Output Stream
	////////////////////////////////////////////////

	class CompressingOutputStream : public OutputStream {
		Ngl::BufferedOutputStream bufferedOutputStream;
	public:

		CompressingOutputStream(Ngl::ManagedPointer<OutputStream> outputStream, int32_t maxBufferSizeBytes=0);

		// Writes all data to the output stream,
		virtual void writeData(const void *data, int32_t sizeBytes);

		// Flushes any data that might be buffered and not written to the stream yet,
		virtual void flush();
	};

	////////////////////////////////////////////////
	// Uncompressing Input Stream
	////////////////////////////////////////////////

	class UncompressingInputStream : public InputStream {
		BufferedInputStream bufferedInputStream;
	public:

		UncompressingInputStream(ManagedPointer<InputStream> inputStream);

		virtual bool hasData();

		// Reads some (or all) data available from input stream. Returns data size,
		virtual int32_t readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector);

		virtual bool markSupported() { return false; }
	};

	////////////////////////////////////////////////
	// Uncompressing Output Stream
	////////////////////////////////////////////////

	class UncompressingOutputStream : public OutputStream {
		Ngl::ManagedPointer<OutputStream> outputStream;
		std::vector<uint8_t> buffer;
		int32_t blockBytesRemaining=0;
	public:

		UncompressingOutputStream(Ngl::ManagedPointer<OutputStream> outputStream);

		// Writes all data to the output stream,
		virtual void writeData(const void *data, int32_t sizeBytes);

		virtual void flush();
	};
}
