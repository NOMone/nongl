#pragma once

#if NONGL_FACEBOOK_SDK

class TextBuffer;

namespace Ngl {
    template <typename T>
    class ManagedPointer;
}

#include <functional>

/////////////////////////////
// API
/////////////////////////////

#define FACEBOOK_ACCESS_TOKEN_PREFERENCE "Facebook access token"

namespace Ngl {
    enum class FacebookLoginResponseType {UNKNOWN, NOT_RECEIVED, FACEBOOK_NOT_INTEGRATED, SUCCESS, CANCEL, ERROR};
}

// Takes a "," separated permissions list,
void systemFacebookSdkLogInWithReadPermissions(
        const TextBuffer &appId,
        const TextBuffer &clientToken,
        const TextBuffer &permissions,
        const Ngl::ManagedPointer<std::function<void(const Ngl::FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener);

void systemFacebookSdkLogOut();

#endif
