#include "Texture.h"
#include "TextureRegion.h"
#include "InputStream.h"
#include "glWrapper.h"

Ngl::Texture::Texture() {}
Ngl::Texture::~Texture() {}

void Ngl::Texture::setTextureRegion(int x, int y, int width, int height, const char *data) {
    
    glTexSubImage2D(
            GL_TEXTURE_2D,
            0,
            x,
            y,
            width,
            height,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            data);
}

Ngl::TextureRegion *Ngl::Texture::createTextureRegion(const TextBuffer &regionName) {
    return new Ngl::TextureRegion(
    		this,
    		regionName,
    		0, 0, width, height,
    		width, height,
    		0, 0,
    		true);
}
