#include "ToggleButton.h"

#include "Audio.h"
#include "utils.h"

#define MIN_SOUND_PITCH 0.8f
#define MAX_SOUND_PITCH 1.2f

void ToggleButtonOnToggledListener::toggleButtonOnToggled(ToggleButton *toggleButton, bool pressed) {}

ToggleButton::ToggleButton(const ButtonParams *parameters) : Button(parameters) {
	this->toggleButtonOnToggledListener = 0;
	this->toggled = false;
}

ToggleButton::ToggleButton(float x, float y, const char *text) : Button(x, y, text) {
	this->toggleButtonOnToggledListener = 0;
	this->toggled = false;
}

bool ToggleButton::onTouchEvent(const TouchEvent *event) {

	// TODO: redo this method?
	touchState = event->touchState;

	if (!isEnabled())
		return false;

	/*
	if ((event->type == TouchEvent::TOUCH_DOWN) ||
		(event->type == TouchEvent::TOUCH_DRAG) ||
		(event->type == TouchEvent::TOUCH_UP)) {
		return testHit(event->x, event->y);
	}
	*/

	if (isEnabled() && (event->type == TouchEvent::TOUCH_UP)) {
		attachToLocalAnimation(&onReleaseAnimation);
		onReleaseAnimation.start();
		onPressAnimation.reset();
	}

	if (isEnabled() && testHit(event->x, event->y)) {

		if (event->type == TouchEvent::TOUCH_DOWN) {
			attachToLocalAnimation(&onPressAnimation);
			onPressAnimation.start();
			onReleaseAnimation.reset();
		}

		if (event->type != TouchEvent::CLICK)
			return false;

		toggled = !toggled;

		if (buttonListener)
			buttonListener->buttonOnClick(this);

		if (toggleButtonOnToggledListener)
			toggleButtonOnToggledListener->toggleButtonOnToggled(this, toggled);

		if (clickSound)
			clickSound->play(randomFloatRange(MIN_SOUND_PITCH, MAX_SOUND_PITCH));

		return true;
	}

	return false;
}

void ToggleButton::update(float elapsedTimeMillis) {

	bool originalPressed = pressed;

	pressed |= toggled;
	Button::update(elapsedTimeMillis);
	pressed = originalPressed;
}
