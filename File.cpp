
#include "File.h"
#include "SystemFileServices.h"

#include <stdexcept>

/////////////////////////////////
// File Input Stream
/////////////////////////////////

bool Ngl::FileInputStream::hasData() {
	if (file.expired()) throw std::runtime_error("Attempt to get expired file size.");
	return (file->getSizeBytes() > offsetInFile);
}

int32_t Ngl::FileInputStream::readData(std::vector<uint8_t> &outputVector, int32_t offset) {

	if (file.expired()) throw std::runtime_error("Attempt to read from an expired file.");
	int32_t readBytesCount = systemReadFile(*file, outputVector, offset, offsetInFile);
	offsetInFile += readBytesCount;

	return readBytesCount;
}

void Ngl::FileInputStream::mark(int32_t readLimit) {
	markedOffset = offsetInFile;
	markReadLimit = readLimit;
}

void Ngl::FileInputStream::reset() {

	if (markedOffset == -1) throw std::runtime_error("Can't reset() without mark().");
	if (markReadLimit && (markedOffset + markReadLimit > offsetInFile)) {
		throw std::runtime_error("Read limit exceeded, can't reset.");
	}

	offsetInFile = markedOffset;
	markedOffset=-1;
	markReadLimit=0;
}

/////////////////////////////////
// File output stream
/////////////////////////////////

void Ngl::UnbufferedFileOutputStream::writeData(const void *data, int32_t sizeBytes) {

	if (file.expired()) throw std::runtime_error("Attempt to write to an expired file.");

	int32_t readBytesCount = systemWriteFile(*file, data, sizeBytes, append);
	append = true;

	if (readBytesCount != sizeBytes) throw std::runtime_error("Couldn't write all data to file.");
}

Ngl::FileOutputStream::FileOutputStream(ManagedPointer<File> file, bool append, int32_t maxBufferSizeBytes) :
		bufferedOutputStream(
				Ngl::shared_ptr<Ngl::OutputStream>(new Ngl::UnbufferedFileOutputStream(file, append)),
				maxBufferSizeBytes) {
	// Just a filler comment.
}

void Ngl::FileOutputStream::writeData(const void *data, int32_t sizeBytes) {
	bufferedOutputStream.writeData(data, sizeBytes);
}

void Ngl::FileOutputStream::flush() { bufferedOutputStream.flush(); }

/////////////////////////////////
// File
/////////////////////////////////

Ngl::File::File(const File &parentDirectory, const TextBuffer &fileName) {
	this->location = parentDirectory.location;
	this->fullPath = TextBuffer(parentDirectory.fullPath).append('/').append(fileName);
}

bool Ngl::File::operator==(const File &rightHandSide) const {
	return (location == rightHandSide.location) && (fullPath == rightHandSide.fullPath);
}

bool Ngl::File::exists() const {
	return systemFileExists(*this);
}

int32_t Ngl::File::getSizeBytes() const {
	return systemGetFileSize(*this);
}

void Ngl::File::makeDir() const {
	systemMakeDir(*this);
}

void Ngl::File::copy(const File &destination) const {
	FileInputStream srcStream(shared_ptr<File>(new File(*this)));
	FileOutputStream destStream(shared_ptr<File>(new File(destination)));

	destStream.writeAllData(srcStream);
}

int32_t Ngl::File::read(std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) const {
	return systemReadFile(*this, outputVector, offsetInVector, offsetInFile, maxReadSize);
}

int32_t Ngl::File::read(std::vector<uint8_t> &outputVector) const {
	return systemReadFile(*this, outputVector, outputVector.size());
}

int32_t Ngl::File::write(const void *data, int32_t sizeBytes, bool append) const {
	return systemWriteFile(*this, data, sizeBytes, append);
}
