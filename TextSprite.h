#pragma once

#include "Font.h"
#include "Sprite.h"
#include "SpriteBatch.h"

#include <vector>

class TextSprite {

	Font *font;

	int textLength;
	std::vector<char> text;

	Sprite characterSprite;

	float width, height;

public:

	struct LengthAndWidth {
		int length;
		float width;
	};

	float x, y, z;
	float scaleX, scaleY;
	int colorMask;

	TextSprite(const char *text=0, Font *font=0);

	TextSprite *clone();

	void setFont(Font *font) {
		if (font) {
			this->font = font;
			if (!text.empty()) setText(&text[0]);
		}
	}
	inline Font *getFont() { return font; }

	inline Ngl::Texture *getTexture() { return font->getTexture(); }

	void setText(const char *text);

	// Computes length and width based on non-scaled character regions.
	static LengthAndWidth getLengthAndWidthToLastFittingWordEnd(const char *text, float width, const Font *font);
	static float getWidthToLastFittingWordEnd(const char *text, float width, const Font *font);
	static int getLengthToLastFittingWordEnd(const char *text, float width, const Font *font);
	int getLengthToLastFittingWordEnd(const char *text, float width);

	// Returns maximum text width if no limits were imposed on width.
	static float getTextWidth(const char *text, float maxAllowedWidth, float scale=1.0f, const Font *font=0);
	static float getTextHeight(const char *text, float width, float scale=1.0f, const Font *font=0);
	static float getTextLinesCount(const char *text, float width, float scale=1.0f, const Font *font=0);

	void draw(SpriteBatch *spriteBatch);

	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + (width * scaleX); }
	inline float getTop() { return y + (height * scaleY); }
	inline float getCenterX() { return x + (width * scaleX * 0.5f); }
	inline float getCenterY() { return y + (height * scaleY * 0.5f); }
	inline float getDepth() { return z; }

	inline void setLeft(float left) { x = left; }
	inline void setBottom(float bottom) { y = bottom; }
	inline void setRight(float right) { x = right - (width * scaleX); }
	inline void setTop(float top) { y = top - (height * scaleY); }
	inline void setCenterX(float centerX) { x = centerX - (width * scaleX * 0.5f); }
	inline void setCenterY(float centerY) { y = centerY - (height * scaleY * 0.5f); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	inline void setDepth(float z) { this->z = z; }
};
