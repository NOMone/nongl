#pragma once

#include <Nongl/DialogActivity.h>
#include <Nongl/Gravity.h>

class LinearLayout;
class NinePatchView;
class ImageView;
class TextView;
class NinePatchData;

namespace Ngl {

    //////////////////////////////
    // Text animation
    //////////////////////////////

    class TextAnimation {
        char *text;
        char *visibleText;
        char *formattedText;
        int32_t currentChar;
        int32_t currentVisibleChar;
        float charDelay;
        float delay;
        float remainingTime;

        void reset();
        void pushVisibleChar(char character);
        bool processEscapeSequence(const char *text);

    public:

        inline TextAnimation() { reset(); }
        inline ~TextAnimation() {
            if (text) {
                delete[] text;
                delete[] visibleText;
                if (formattedText) delete[] formattedText;
            }
        }

        void removeEscapeSequencesFromText(char *text);
        void update(float elapsedTimeMillis);
        const char *getText();
        void setText(const char *text, const char *formattedText=0);

        inline bool hasFinished() { return !(text[currentChar]); }
        void finish();
    };

    //////////////////////////////
    // Speech dialog
    //////////////////////////////

    class SpeechDialog : public DialogActivity, public Runnable {

        LinearLayout *dialogPositionLayout;
        NinePatchView *dialogBackground;
        ImageView *characterImage;
        TextView *dialogTextView;
        LinearLayout *portraitAndTextLayout;

        Runnable *onDialogDismissedListener;
        void *onDialogDismissedListenerData;

        TextAnimation textAnimation;

        bool readyForInput;

        void setupDialogAnimations();
        void sendToBack();

    public:

        static const char *activityName;
        bool autoSkip;
        bool backPressed;
        Animation entranceAnimation, exitAnimation;

        SpeechDialog();

        const Animation *getEntranceAnimation() { return &entranceAnimation; }
        const Animation *getExitAnimation() { return &exitAnimation; }

        void set(const Ngl::TextureRegion *characterPicture, const NinePatchData *background, Gravity::Value gravity, const char *text, int textColor);
        void setGravity(Gravity::Value gravity);
        void setBackground(const NinePatchData *background);
        void setCharacterPortrait(const Ngl::TextureRegion *characterPortrait);
        void setText(const char *text);
        void setTextColor(int textColor);
        void setTextScale(float scaleX, float scaleY);

        void updateLayout();

        inline void setOnDialogDismissedListener(Runnable *onDialogDismissedListener, void *onDialogDismissedListenerData=0) {
            this->onDialogDismissedListener = onDialogDismissedListener;
            this->onDialogDismissedListenerData = onDialogDismissedListenerData;
        }

        void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);

        bool onBackPressed();
        bool onTouchEvent(const TouchEvent *event);

        void onShowed();

        bool run(void *data);
    };
}
