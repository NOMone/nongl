#pragma once

#include <vector>
#include <stdint.h>

class TextBuffer;

namespace Ngl {
	class Texture;
	class TextureRegion;
	class File;
}

enum class PixelMapFileType { PNG, JPG, AUTO_DETECT };

class PixelMap {

	int width=0, height=0;
	std::vector<int32_t> data;

public:

	PixelMap(int width, int height);
	PixelMap(const Ngl::File                 &file, PixelMapFileType fileType, bool flipImageVertically);
    PixelMap(const std::vector<uint8_t> &imageData, PixelMapFileType fileType, bool flipImageVertically);

    // TODO: add generic auto detect load ...
	void setFromPng(const Ngl::File            &pngFile, bool flipImageVertically);
    void setFromPng(const std::vector<uint8_t> &pngData, bool flipImageVertically);

	void setFromJpg (const Ngl::File             &jpgFile, bool flipImageVertically);
    void setFromJpeg(const std::vector<uint8_t> &jpegData, bool flipImageVertically);

    void setFromAutoDetect(const Ngl::File            &imageFile, bool flipImageVertically);
    void setFromAutoDetect(const std::vector<uint8_t> &imageData, bool flipImageVertically);

	void flipVertically();

	inline int getWidth() { return width; }
	inline int getHeight() { return height; }

	inline std::vector<int> &getData() { return data; }

	inline int getPixel(int x, int y) { return data[(y*width)+x]; }
	inline void setPixel(int x, int y, int color) { data[(y*width)+x] = color; }
	void drawPixelMap(const PixelMap &sourcePixelMap, int sourceLeft, int sourceBottom, int destinationLeft, int destinationBottom);
	void drawPixelMapStretch(
			const PixelMap &sourcePixelMap,
			int sourceLeft, int sourceBottom, int sourceWidth, int sourceHeight,
			int destinationsLeft, int destinationBottom, int destinationWidth, int destinationHeight);

	void clear(int clearColor);
	void readFromFrameBuffer(int frameBufferX, int frameBufferY);
	void readFromTextureRegion(const Ngl::TextureRegion *textureRegion, int xInRegion, int yInRegion);

	Ngl::Texture *createTexture();

	void saveToFile(const Ngl::File &pngFile, bool flipImageVertically);
};
