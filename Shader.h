#pragma once

class Shader {

public:

	int programId;

	inline Shader() {}
	Shader(const char *vertexShaderCode, const char *fragmentShaderCode);
	virtual ~Shader();

	// This should never be called more than once.
	void setShaderCode(const char *vertexShaderCode, const char *fragmentShaderCode);

	virtual bool use() = 0;	// Returns false if shader is already in use.
	virtual void release() = 0;
};
