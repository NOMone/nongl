#include "Preferences.h"

#include "TextUtils.h"
#include "ByteBuffer.h"
#include "OutputStream.h"
#include "BufferedInputStream.h"
#include "SystemLog.h"

#include <stdexcept>

/////////////////////////
// Preference
/////////////////////////

namespace NglImpDetails {

	// Note that changing the order of the type ids invalidates all existing
	// preferences (pay attentions to preferences saved on disk),
	enum class PreferenceTypeId : uint8_t {BOOL, INT, STRING};

	class Preference {
	public:
		virtual ~Preference() {}

		virtual bool operator==(const Preference &rightHandSide) const = 0;
		bool operator!=(const Preference &rightHandSide) const {
			return !operator==(rightHandSide);
		}

		virtual void save(Ngl::OutputStream &outputStream) = 0;
		virtual TextBuffer toString() = 0;
	};

	class BoolPreference : public Preference {
	public:
		bool value;
		BoolPreference(bool value) : value(value) {}

		bool operator==(const Preference &rightHandSide) const {
			if (!dynamic_cast<const BoolPreference *>(&rightHandSide)) return false;
			return value == ((BoolPreference &) rightHandSide).value;
		}

		void save(Ngl::OutputStream &outputStream) {
			Ngl::ByteBuffer data(2);
			data.pushByte((uint8_t) PreferenceTypeId::BOOL);
			data.pushByte(value ? 1 : 0);
			outputStream.writeData(data.getData(), data.getDataSize());
		}

		static bool load(Ngl::BufferedInputStream &bufferedInputStream) {
			return bufferedInputStream.readByte();
		}

		TextBuffer toString() { return TextBuffer().append(value); }
	};

	class IntPreference : public Preference {
	public:
		int32_t value;
		IntPreference(int32_t value) : value(value) {}

		bool operator==(const Preference &rightHandSide) const {
			if (!dynamic_cast<const IntPreference *>(&rightHandSide)) return false;
			return value == ((IntPreference &) rightHandSide).value;
		}

		void save(Ngl::OutputStream &outputStream) {
			Ngl::ByteBuffer data(5);
			data.pushByte((uint8_t) PreferenceTypeId::INT);
			data.pushInt(value);
			outputStream.writeData(data.getData(), data.getDataSize());
		}

		static int32_t load(Ngl::BufferedInputStream &bufferedInputStream) {
			return bufferedInputStream.readInt32();
		}

		TextBuffer toString() { return TextBuffer().append(value); }
	};

	class StringPreference : public Preference {
	public:
		TextBuffer value;
		StringPreference(const TextBuffer &value) : value(value) {}

		bool operator==(const Preference &rightHandSide) const {
			if (!dynamic_cast<const StringPreference *>(&rightHandSide)) return false;
			return value == ((StringPreference &) rightHandSide).value;
		}

		void save(Ngl::OutputStream &outputStream) {
			PreferenceTypeId typeId = PreferenceTypeId::STRING;
			int32_t stringLength = value.getTextLength();
			outputStream.writeData(&typeId, 1);
			outputStream.writeData(&stringLength, 4);
			outputStream.writeData(value.getConstText(), stringLength);
		}

		static TextBuffer load(Ngl::BufferedInputStream &bufferedInputStream) {
			return TextBuffer(bufferedInputStream, bufferedInputStream.readInt32());
		}

		TextBuffer toString() { return TextBuffer(value); }
	};
}

/////////////////////////
// Preferences
/////////////////////////

template <typename TPreference, typename T>
void Ngl::Preferences::setPreference(const TextBuffer &key, T value) {

	// Delete any existing preference (deleting null is ok, and the standard guarantees that
	// scaler template types are initialized to 0,
	NglImpDetails::Preference * &existingPreference = preferences[key];

	// If this key was used before, just update the preference,
	if (existingPreference) {
		// Make sure the existing preference and the new value are of matching types,
		TPreference *typedPreference = dynamic_cast<TPreference *>(existingPreference);
		if (typedPreference) {
			typedPreference->value = value;
		} else {
			throw std::runtime_error("Attempt to set an existing preference to a new type.");
		}
	} else {

		// They key is new, make a new preference for it,
		existingPreference = new TPreference(value);
	}
}

template <typename TPreference, typename T>
T Ngl::Preferences::getPreference(const TextBuffer &key, T defaultValue) {

	auto iterator = preferences.find(key);

	// If the preference is not found, return the default value,
	if (iterator == preferences.end()) return defaultValue;

	// Make sure that the found preference's type matches the new value type,
	TPreference *typedPreference = dynamic_cast<TPreference *>(iterator->second);
	if (typedPreference) {
		return typedPreference->value;
	} else {
		throw std::runtime_error("Attempt to get an existing preference as a new type.");
	}
}

void Ngl::Preferences::setBoolPreference(const TextBuffer &key, bool value) {
	setPreference<NglImpDetails::BoolPreference, bool>(key, value);
}

bool Ngl::Preferences::getBoolPreference(const TextBuffer &key, bool defaultValue) {
	return getPreference<NglImpDetails::BoolPreference, bool>(key, defaultValue);
}

void Ngl::Preferences::setIntPreference(const TextBuffer &key, int32_t value) {
	setPreference<NglImpDetails::IntPreference, int32_t>(key, value);
}

int32_t Ngl::Preferences::getIntPreference(const TextBuffer &key, int32_t defaultValue) {
	return getPreference<NglImpDetails::IntPreference, int32_t>(key, defaultValue);
}

void Ngl::Preferences::setStringPreference(const TextBuffer &key, const TextBuffer &value) {
	setPreference<NglImpDetails::StringPreference, TextBuffer>(key, value);
}

TextBuffer Ngl::Preferences::getStringPreference(const TextBuffer &key, const TextBuffer &defaultValue) {
	return getPreference<NglImpDetails::StringPreference, TextBuffer>(key, defaultValue);
}

void Ngl::Preferences::write(OutputStream &outputStream) {

	// Save all preferences, one by one,
	auto iterator = preferences.begin();

	while (iterator != preferences.end()) {

		// Write key,
		const TextBuffer &key = (*iterator).first;
		int16_t keySize = (int16_t) key.getTextLength();
		outputStream.writeData(&keySize, 2);
		outputStream.writeData(key.getConstText(), keySize);

		// Write value,
		(*iterator).second->save(outputStream);
		iterator++;
	}
}

void Ngl::Preferences::read(InputStream &inputStream) {

	// If you ever have too many types, consider using a std::map as described
	// in this article: http://www.parashift.com/c++-faq/serialize-inherit-no-ptrs.html

	BufferedInputStream bufferedInputStream(Ngl::shared_ptr<InputStream>(new InputStreamWrapper(inputStream)));

	while (bufferedInputStream.hasData()) {

		// Read key,
		int16_t keySize = bufferedInputStream.readInt16();
		TextBuffer key(bufferedInputStream, keySize);

		// Read value,
		NglImpDetails::PreferenceTypeId typeId = (NglImpDetails::PreferenceTypeId) bufferedInputStream.readByte();
		switch (typeId) {
			case NglImpDetails::PreferenceTypeId::BOOL:
				setBoolPreference(key, NglImpDetails::BoolPreference::load(bufferedInputStream));
				break;

			case NglImpDetails::PreferenceTypeId::INT:
				setIntPreference(key, NglImpDetails::IntPreference::load(bufferedInputStream));
				break;

			case NglImpDetails::PreferenceTypeId::STRING:
				setStringPreference(key, NglImpDetails::StringPreference::load(bufferedInputStream));
				break;

			default:
				throw std::runtime_error(TextBuffer("Unknown preference type id: ").append((uint8_t) typeId).getConstText());
				break;
		}
	}
}

void Ngl::Preferences::log() {

	LOGE("=======================");
	LOGE("Logging preferences...");

	// Log all preferences, one by one,
	auto iterator = preferences.begin();

	while (iterator != preferences.end()) {
		LOGE("%s: %s", (*iterator).first.getConstText(), (*iterator).second->toString().getConstText());
		iterator++;
	}

	LOGE("=======================");
}

bool Ngl::Preferences::isIdenticalTo(const Preferences &rightHandSide) {

	//Implement == in Preference and derivatives ...

	// They are not identical if they don't contain the same number of keys,
	if (preferences.size() != rightHandSide.preferences.size()) return false;

	// Test all preferences, one by one,
	auto iterator1 = preferences.begin();
	auto iterator2 = rightHandSide.preferences.begin();

	while (iterator1 != preferences.end()) {
		if ((  iterator1->first   !=   iterator2->first  ) ||
			(*(iterator1->second) != *(iterator2->second))) {
			return false;
		}

		iterator1++;
		iterator2++;
	}

	// They are identical,
	return true;
}
