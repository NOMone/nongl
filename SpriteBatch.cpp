#include "SpriteBatch.h"

#include "DisplayManager.h"
#include "Quad.h"
#include "Nongl.h"
#include "SystemLog.h"
#include "glWrapper.h"
#include "Texture.h"
#include "Shaders/PlainShader.h"
#include "Shaders/SeparateAlphaShader.h"

//#define DRAW_WIRE_FRAME
//#define DISABLE_SPRITES_DRAWING
//#define DISABLE_QUADS_DRAWING

#define MAX_VERTICES 65536
#define VERTEX_FLOATS 6

#define SPRITE_VERTICES 4
#define SPRITE_FLOATS (SPRITE_VERTICES * VERTEX_FLOATS)
#define SPRITE_INDICES 6

#define QUAD_VERTICES 4
#define QUAD_VERTICES_FLOATS (QUAD_VERTICES * VERTEX_FLOATS)
#define QUAD_INDICES 6
#define QUAD_INDICES_SHORTS 6

#define HALF_PI 1.570796326795f

SpriteBatch::SpriteBatch(int initialSpritesCapacity /*0*/, bool vboPowered /*false*/, bool isStatic /*false*/) :
		filledRectangleSprite(Nongl::whiteBlockTextureRegion), lineSprite(Nongl::whiteBlockTextureRegion) {

	this->shader = 0;
	this->separateAlphaShader = 0;

	this->texture = 0;

	this->flushesDueToTextureChangeCount = 0;
	this->colorMask = 0xffffffff;

	this->vboPowered = VBO_ENABLED && vboPowered;
	this->isStatic = isStatic;
	this->verticesVBO = this->indicesVBO = 0;
	this->verticesVBOSize = this->indicesVBOSize = 0;
	this->vbosModified = false;

	this->verticesCount = 0;
	this->vertexDataSize = 0;
	this->indicesSize = 0;
	vertexData.reserve(initialSpritesCapacity * SPRITE_FLOATS);
	indices.reserve(initialSpritesCapacity * SPRITE_INDICES);

	this->lineSpriteWidthReciprocal = 1 / this->lineSprite.width;
	setLineThickness(1);
}

SpriteBatch::~SpriteBatch() {

	if (verticesVBO) {
		glDeleteBuffers(1, &verticesVBO);
		glDeleteBuffers(1, &indicesVBO);
	}
}

void SpriteBatch::setTexture(Ngl::Texture *texture) {

	// Check if a flush should be performed due to texture changing,
	if (this->texture != texture) {
		flush();
		this->texture = texture;
		flushesDueToTextureChangeCount++;
	}
}

void SpriteBatch::prepareForDraw(Ngl::Texture *texture, int vertexFloatsCount, int indicesShortsCount) {

	// Check if a flush should be performed due to texture changing,
	if (this->texture != texture) {
		flush();
		this->texture = texture;
		flushesDueToTextureChangeCount++;
	}

	// Check if vertex limit was reached,
	int requiredVertexBufferSize = vertexDataSize + vertexFloatsCount;
	if (requiredVertexBufferSize > MAX_VERTICES * VERTEX_FLOATS) {
		LOGE("Warning: flushed due to overflow...");
		flush();
	}

	// Resize the vectors if needed,
	// Vertex date,
	int32_t pushCount = requiredVertexBufferSize - (int32_t) vertexData.size();
	for (; pushCount > 0; pushCount--) vertexData.push_back(0);

	// Indices,
	int requiredIndicesBufferSize = indicesSize + indicesShortsCount;
	pushCount = requiredIndicesBufferSize - (int32_t) indices.size();
	for (; pushCount > 0; pushCount--) indices.push_back(0);

	// Mark vbos as dirty,
	vbosModified = true;
}

void SpriteBatch::drawSprite(Sprite *sprite) {

#ifdef DRAW_WIRE_FRAME
	if (sprite != lineSprite) {
		if (sprite->textureRegion->hasAlpha) {
			sprite->drawWireFrame(this, 0xff0000ff);
		} else {
			sprite->drawWireFrame(this, 0xff00ff00);
		}
	}
#endif

#ifdef DISABLE_SPRITES_DRAWING
	if (sprite == lineSprite)
#endif
	{

		// TODO: refactor to use prepareForDraw...

		// Check if a flush should be performed due to texture changing,
		Ngl::Texture *spriteTexture = sprite->getTexture();
		if (texture != spriteTexture) {
			flush();
			texture = spriteTexture;
			flushesDueToTextureChangeCount++;
		}

		// Check if vertex limit was reached,
		int requiredVertexBufferSize = vertexDataSize + SPRITE_FLOATS;
		if (requiredVertexBufferSize > MAX_VERTICES * VERTEX_FLOATS) {
			LOGE("Warning: flushed due to overflow...");
			flush();
			drawSprite(sprite);
		} else {
			// Resize the vectors if needed,
			// Vertex date,
			int32_t pushCount = requiredVertexBufferSize - (int32_t) vertexData.size();
			for (; pushCount > 0; pushCount--) {
				vertexData.push_back(0);
			}

			// Indices,
			pushCount = indicesSize + SPRITE_INDICES - (int32_t) indices.size();
			for (; pushCount > 0; pushCount--) {
				indices.push_back(0);
			}

			// Adding the sprite data,
			sprite->draw(
					(float *) &vertexData[vertexDataSize],
					(unsigned short *) &indices[indicesSize],
					verticesCount);

			verticesCount += SPRITE_VERTICES;
			vertexDataSize = requiredVertexBufferSize;
			indicesSize += SPRITE_INDICES;

			// Mark vbos as dirty,
			vbosModified = true;
		}
	}
}

void SpriteBatch::drawQuad(Quad *quad) {

#ifdef DRAW_WIRE_FRAME
	if (quad->textureRegion->hasAlpha) {
		quad->drawWireFrame(this, 0xff0000ff);
	} else {
		quad->drawWireFrame(this, 0xff00ff00);
	}
#endif

#ifndef DISABLE_QUADS_DRAWING

	// Prepare buffers for drawing,
	prepareForDraw(quad->getTexture(), QUAD_VERTICES_FLOATS, QUAD_INDICES_SHORTS);

	// Adding the quad data,
	quad->draw(
			(float *) &vertexData[vertexDataSize],
			(unsigned short *) &indices[indicesSize],
			verticesCount);

	// Count the added vertices and faces,
	vertexDataSize += QUAD_VERTICES_FLOATS;
	indicesSize += QUAD_INDICES_SHORTS;
	verticesCount += QUAD_VERTICES;

#endif
}

void SpriteBatch::drawRect(float left, float bottom, float width, float height, float depth, int color, bool fillCorners) {
	if (fillCorners) {
		drawLine(left      -halfLineThickness, bottom                         , depth, left+width+halfLineThickness, bottom                         , depth, color);
		drawLine(left+width                  , bottom       +halfLineThickness, depth, left+width                  , bottom+height-halfLineThickness, depth, color);
		drawLine(left+width+halfLineThickness, bottom+height                  , depth, left      -halfLineThickness, bottom+height                  , depth, color);
		drawLine(left                        , bottom+height-halfLineThickness, depth, left                        , bottom       +halfLineThickness, depth, color);
	} else {
		drawLine(left      , bottom       , depth, left+width, bottom       , depth, color);
		drawLine(left+width, bottom       , depth, left+width, bottom+height, depth, color);
		drawLine(left+width, bottom+height, depth, left      , bottom+height, depth, color);
		drawLine(left      , bottom+height, depth, left      , bottom       , depth, color);
	}
}

void SpriteBatch::drawFilledRect(float left, float bottom, float width, float height, float depth, int color) {
	filledRectangleSprite.setLeft(left);
	filledRectangleSprite.setBottom(bottom);
	filledRectangleSprite.colorMask = color;
	filledRectangleSprite.scaleX = width / filledRectangleSprite.width;
	filledRectangleSprite.scaleY = height / filledRectangleSprite.height;
	filledRectangleSprite.z = depth;

	drawSprite(&filledRectangleSprite);
}

void SpriteBatch::setLineThickness(float lineThickness) {
	this->lineThickness = lineThickness;
	halfLineThickness = lineThickness * 0.5f;
	lineSprite.scaleY = lineThickness / lineSprite.height;
}

void SpriteBatch::drawLine(float x1, float y1, float z1, float x2, float y2, float z2, int color) {

	// TODO: refactor to use quad instead of sprite so that each point can have separate depth...

	float xDifference = x2-x1;
	float yDifference = y2-y1;

	if (xDifference == 0) {
		lineSprite.rotation = HALF_PI;
		lineSprite.setBottom(y1 + (yDifference * 0.5f));
		lineSprite.scaleX = yDifference * lineSpriteWidthReciprocal;
		if (lineSprite.scaleX < 0) lineSprite.scaleX = -lineSprite.scaleX;
	} else if (yDifference == 0) {
		lineSprite.rotation = 0;
		lineSprite.setBottom(y1);
		lineSprite.scaleX = xDifference * lineSpriteWidthReciprocal;
		if (lineSprite.scaleX < 0) lineSprite.scaleX = -lineSprite.scaleX;
	} else {

		float length = sqrtf((xDifference*xDifference) + (yDifference*yDifference));
		lineSprite.scaleX = length * lineSpriteWidthReciprocal;
		lineSprite.rotation = atanf(yDifference / xDifference);
	}

	lineSprite.setCenter((x1 + x2) * 0.5f, (y1 + y2) * 0.5f);
	lineSprite.colorMask = color;

	lineSprite.z = z2;

	drawSprite(&lineSprite);
}

void SpriteBatch::clear() {
	verticesCount = vertexDataSize = indicesSize = 0;
}

void SpriteBatch::onVBOsDiscarded() {
	verticesVBO = indicesVBO = 0;
}

void SpriteBatch::adjustVBOs() {

	#define FLOAT_BYTES 4
	#define SHORT_BYTES 2

	if ((!verticesVBO) || (vertexData.capacity() > verticesVBOSize)) {

		if (verticesVBO) {
			glDeleteBuffers(1, &verticesVBO);
		}
		verticesVBOSize = (int32_t) vertexData.capacity();

		// Prepare vertices VBO,
		glGenBuffers(1, &verticesVBO);
		glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
		glBufferData(GL_ARRAY_BUFFER, verticesVBOSize * FLOAT_BYTES, (void *) &vertexData[0], (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW
	} else if (vbosModified) {
		glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
		//glBufferData(GL_ARRAY_BUFFER, vertexDataSize * FLOAT_BYTES, (void *) &vertexData[0], GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferData(GL_ARRAY_BUFFER, verticesVBOSize * FLOAT_BYTES, 0, (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STATIC_DRAW, GL_STREAM_DRAW, GL_DYNAMIC_DRAW
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertexDataSize * FLOAT_BYTES, (void *) &vertexData[0]);
	} else {
		glBindBuffer(GL_ARRAY_BUFFER, verticesVBO);
	}

	if ((!indicesVBO) || (indices.capacity() > indicesVBOSize)) {

		if (indicesVBO) {
			glDeleteBuffers(1, &indicesVBO);
		}

		indicesVBOSize = (int32_t) indices.capacity();

		// Prepare the indices VBO,
		glGenBuffers(1, &indicesVBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesVBOSize * SHORT_BYTES, (void *) &indices[0], (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
	} else if (vbosModified) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
		//glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize * SHORT_BYTES, (void *) &indices[0], GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesVBOSize * SHORT_BYTES, 0, (isStatic) ? GL_STATIC_DRAW : GL_DYNAMIC_DRAW); // GL_STREAM_DRAW ?
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indicesSize * SHORT_BYTES, (void *) &indices[0]);
	} else {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVBO);
	}

	vbosModified = false;
}

void SpriteBatch::flush() {
	draw();
	clear();
}

void SpriteBatch::draw() {

	// TODO: one line check gl errors...

	// If we should skip drawing entirely,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	if (displayManager->isEntirelyClipped()) {
		return;
	}

	// If nothing drawn, return,
	if (!indicesSize)
		return;

	// If VBO based, adjust VBOs,
	if ((!VERTEX_ARRAYS_ENABLED) || vboPowered) {
		adjustVBOs();
	}

	// Get texture id for drawing,
	int texId = displayManager->prepareTexture(texture);

	// Decide which shader to use based on blend state and availability of alpha texture,
	GLboolean blendEnabled;
	//glGetBooleanv(GL_BLEND, &blendEnabled);  // Crashes on Mali400 devices (probably driver issue).
	//blendEnabled = glIsEnabled(GL_BLEND);    // Works nicely.
	//checkGlError("glIsEnabled(GL_BLEND)");
	blendEnabled = Nongl::getBlendEnabled();

	Ngl::Texture *alphaTexture = texture->getAlphaTexture();
	if (blendEnabled && alphaTexture) {

		int alphaTexId = displayManager->prepareTexture(alphaTexture);

		SeparateAlphaShader *shaderToUse = (separateAlphaShader) ? separateAlphaShader : Nongl::getDefaultSeparateAlphaShader();
		shaderToUse->use();

		if ((!VERTEX_ARRAYS_ENABLED) || vboPowered) {
			shaderToUse->setParams(0, colorMask, texId, alphaTexId);
			glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_SHORT, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		} else {
			shaderToUse->setParams((float *) &vertexData[0], colorMask, texId, alphaTexId);
			glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_SHORT, (void *) &indices[0]);
		}

	} else {
		PlainShader *shaderToUse = (shader) ? shader : Nongl::getDefaultPlainShader();
		shaderToUse->use();

		if ((!VERTEX_ARRAYS_ENABLED) || vboPowered) {
			shaderToUse->setParams(0, colorMask, texId);
			glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_SHORT, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		} else {
			shaderToUse->setParams((float *) &vertexData[0], colorMask, texId);
			glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_SHORT, (void *) &indices[0]);
		}
	}
}
