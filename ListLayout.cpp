#include "ListLayout.h"

#define PADDING 0

ListLayout::ListLayout() {
	set(0, 0, 0, 100, 100);
}

ListLayout::ListLayout(float x, float y, float z, float width, float height, OnListItemSelectedListener *onListItemSelectedListener, Orientation::Value orientation) {
	set(x, y, z, width, height, onListItemSelectedListener);
}

void ListLayout::set(float x, float y, float z, float width, float height, OnListItemSelectedListener *onListItemSelectedListener, Orientation::Value orientation) {

	// TODO: refactor to remove z from constructor...
	this->onListItemSelectedListener = onListItemSelectedListener;
	ScrollableLayout::set(x, y, width, height, width, height);
	linearLayout.set(0, 0, width, height, orientation, Gravity::CENTER, PADDING);
	linearLayout.deletesChildrenOnDestruction = true;

	this->maxSelectedItemsCount = 1;

	addView(&linearLayout);
}

void ListLayout::addItem(ListItem *item) {

	items.push_back(item);
	linearLayout.addView(item->getView());
	item->setListView(this);

	if (linearLayout.getOrientation() == Orientation::HORIZONTAL) {
		item->setRecommendedMaxHeight(getScaledHeight());
	} else {
		item->setRecommendedMaxWidth(getScaledWidth());
	}
}

void ListLayout::layout() {

	linearLayout.fitContents();
	linearLayout.layout();

	if (linearLayout.getScaledWidth() > getWidth()) {
		setScrollWidth(linearLayout.getScaledWidth());
	} else {
		setScrollWidth(getWidth());
	}

	if (linearLayout.getScaledHeight() > getHeight()) {
		setScrollHeight(linearLayout.getScaledHeight());
	} else {
		setScrollHeight(getHeight());
	}

	linearLayout.setCenterX(getScrollWidth() * 0.5f);
	linearLayout.setCenterY(getScrollHeight() * 0.5f);

	ScrollableLayout::layout();
}

void ListLayout::onListItemSelected(ListItem *item) {

	if (item->isSelected()) {

		// Deselect item,
		item->setSelected(false);
		selectedItems.remove(item);

	} else {

		// Add item to selected items,
		if (selectedItems.size() == maxSelectedItemsCount) {
			selectedItems.front()->setSelected(false);
			selectedItems.erase(selectedItems.begin());
		}
		selectedItems.push_back(item);
		item->setSelected(true);

		// Notify listeners of item selection,
		if (onListItemSelectedListener) {
			onListItemSelectedListener->onListItemSelected(item);
		}
	}
}

/////////////////////////////
// Common list items.
/////////////////////////////

TextListItem::TextListItem(const char *text, float width, float height, int color, int selectedBackColor, float backColorDepthOffset) {
	set(0, 0, width, height);
	setText(text);
	textColor = color;
	this->selectedBackColor = selectedBackColor;
	this->backColorDepthOffset = backColorDepthOffset;
}

void TextListItem::fixHeight() {
	float textHeight = getTextLinesCount() * getFont()->lineHeight;
	height = textHeight;
}

void TextListItem::setRecommendedMaxWidth(float recommendedMaxWidth) {
	this->width = recommendedMaxWidth;
	fixHeight();
}

void TextListItem::setRecommendedMaxHeight(float recommendedMaxHeight) {}

void TextListItem::drawTransparentParts(SpriteBatch *spriteBatch) {
	if (selected) {
		spriteBatch->drawFilledRect(getLeft(), getBottom(), getScaledWidth(), getScaledHeight(), getDepth() + backColorDepthOffset, selectedBackColor);
		int oldTextColor = textColor;
		textColor = (textColor & (0xff000000)) | ((~textColor) & (0x00ffffff));
		TextView::drawTransparentParts(spriteBatch);
		textColor = oldTextColor;
	} else {
		TextView::drawTransparentParts(spriteBatch);
	}
}

bool TextListItem::onTouchEvent(const TouchEvent *event) {
	if (event->type == TouchEvent::CLICK) {
		if ((event->downX < getLeft()) || (event->downX >= getRight()) ||
			(event->downY < getBottom()) || (event->downY >= getTop())) {
			return false;
		}

		parentListView->onListItemSelected(this);
		return true;
	}
	return false;
}
