#pragma once

class Sprite;
class SegmentedSprite;
class SegmentedSpriteParameters;
class NinePatchSprite;
class NinePatchData;

class SpriteBatch;
class Animation;

namespace Ngl {
	class TextureRegion;
}

class AbstractSprite {

	Sprite *sprite;
	SegmentedSprite *segmentedSprite;
	NinePatchSprite *ninePatchSprite;

	float width, height;
	float x, y, z;
	float scaleX, scaleY;
	float rotation;
	float rotationPivotXDisplacement, rotationPivotYDisplacement;
	int colorMask;

	bool modified;

	void reset();

public:

	inline AbstractSprite() {}
	inline AbstractSprite(const Ngl::TextureRegion *textureRegion) { init(textureRegion); }
	inline AbstractSprite(const SegmentedSpriteParameters *params) { init(params); }
	inline AbstractSprite(const NinePatchData *data, float width, float height) { init(data, width, height); }

	~AbstractSprite();

	void init(const Ngl::TextureRegion *textureRegion);
	void init(const SegmentedSpriteParameters *params);
	void init(const NinePatchData *data, float width, float height);

	inline float getWidth() { return width; }
	inline float getHeight() { return height; }
	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + getScaledWidth(); }
	inline float getTop() { return y + getScaledHeight();  }
	inline float getCenterX() { return x + (0.5f * getScaledWidth()); }
	inline float getCenterY() { return y + (0.5f * getScaledHeight()); }
	inline float getDepth() { return z; }
	inline float getScaleX() { return scaleX; }
	inline float getScaleY() { return scaleY; }
	inline float getRotation() { return rotation; }

	inline void setLeft(float left) { this->x = left; modified = true; }
	inline void setBottom(float bottom) { this->y = bottom; modified = true; }
	inline void setRight(float right) { this->x = right - getScaledWidth(); modified = true; }
	inline void setTop(float top) { this->y = top - getScaledHeight(); modified = true; }
	inline void setCenterX(float centerX) { this->x = centerX - (0.5f * getScaledWidth()); modified = true; }
	inline void setCenterY(float centerY) { this->y = centerY - (0.5f * getScaledHeight()); modified = true; }
	inline void setCenter(float centerX, float centerY) { this->x = centerX - (0.5f * getScaledWidth()); this->y = centerY - (0.5f * getScaledHeight()); modified = true; }
	inline void setDepth(float z) { this->z = z; modified = true; }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; modified = true; }

	inline void setRotation(float angleRadians) { this->rotation = angleRadians; modified = true; }
	inline void setRotationPivotDisplacementFromCenter(float xDisplacement, float yDisplacement) {
		this->rotationPivotXDisplacement = xDisplacement;
		this->rotationPivotYDisplacement = yDisplacement;
		modified = true;
	}

	inline void setColorMask(int colorMask) { this->colorMask = colorMask; modified = true; }

	inline void adjustDepths(float maxZ, float minZ) { z = maxZ; modified = true; }

	void drawOpaqueParts(SpriteBatch *spriteBatch);
	void drawTransparentParts(SpriteBatch *spriteBatch);
	void drawAllInOrder(SpriteBatch *spriteBatch);

	void updateSprite();
	void updateSegmentedSprite();
	void updateNinePatchSprite();

	void attachToAnimation(Animation *animation);

	void setTextureRegion(const Ngl::TextureRegion *textureRegion);
	void setNinePatch(const NinePatchData *ninePatchData);
};
