#pragma once

#include <Nongl/DialogActivity.h>
#include <Nongl/TextUtils.h>

#include <functional>

namespace Ngl {

	///////////////////////////////
	// Notification dialog data
	///////////////////////////////

	class OnScreenNotificationData {
	public:
		TextBuffer activityName;
		TextBuffer text;
		const Ngl::TextureRegion *icon=0;
		float durationMillis=500, delayMillis=0;

		bool hasDismissButton=false;
		float delayBeforeShowingDismissButtonMillis=0;

		Ngl::ManagedPointer<Runnable> onNotificationEndTask;
		void *onNotificationEndTaskData=0;

		Ngl::ManagedPointer<Runnable> onNotificationTouchedTask;
		void *onNotificationTouchedTaskData=0;

		std::function<void(bool)> onActivityResumeListener;
        std::function<void()> onActivitySurfaceCreatedListener;
        std::function<void()> onActivityDestroyListener;

		OnScreenNotificationData() = default;
		OnScreenNotificationData(
				const TextBuffer &text,
				const Ngl::TextureRegion *icon=0,
				float durationMillis=500, float delayMillis=0);

		OnScreenNotificationData &setActivityName(const TextBuffer &activityName);
		OnScreenNotificationData &setDelayBeforeShowingDismissButton(float delayMillis, bool hasDismissButton=true);
		OnScreenNotificationData &setOnEndTask(const Ngl::ManagedPointer<Runnable> &onNotificationEndTask, void *onNotificationEndTaskData=0);
		OnScreenNotificationData &setOnTouchedTask(const Ngl::ManagedPointer<Runnable> &onNotificationTouchedTask, void *onNotificationTouchedTaskData=0);
	};

	///////////////////////////////
	// Notification dialog
	///////////////////////////////

	class OnScreenNotification : public DialogActivity {

		Layout *dialogLayout;
		AbsoluteLayout *dismissButtonLayout=0;
		Ngl::ManagedPointer<Runnable> onNotificationEndTask;
		void *onNotificationEndTaskData;

		class OnNotificationTouchTask : public Runnable {
		public:
			Ngl::ManagedPointer<Runnable> task;
			void *taskData;
			bool run(void *data);
		} onNotificationTouchedTask;

		class OnDismissButtonTouchTask : public Runnable {
		public:
			OnScreenNotification *notificationToDismiss;
			bool run(void *data);
		} onDismissButtonTouchTask;

		bool dismissed=false;
	public:

		OnScreenNotification(const OnScreenNotificationData &notificationData);

		const Animation *getEntranceAnimation();
		const Animation *getExitAnimation();

		void setOnTouchedTask(Ngl::ManagedPointer<Runnable> onNotificationTouchedTask, void *onNotificationTouchedTaskData=0);
		void dismiss();
		void showDismissButton();

		static void queueOnScreenNotification(const OnScreenNotificationData &notificationData);
		static void queueOnScreenNotification(const char *text, const Ngl::TextureRegion *icon=0, float durationMillis=1000, float delayMillis=0);
		static void showNextOnScreenNotification();

		static bool notificationDisplayedOrQueued(const TextBuffer &notificationActivityName);
	};
}
