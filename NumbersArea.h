#ifndef NUMBERS_AREA_H
#define NUMBERS_AREA_H

#include "SpriteBatch.h"
#include "Sprite.h"
#include "TextureRegion.h"

class NumbersArea {

	const Ngl::TextureRegion *numbersRegions[10];
	Sprite digitSprite;

public:

	float centerX, centerY;
	float width, height;
	float scale;
	int colorMask;

	NumbersArea(float centerX, float centerY, float width, float height, float scale);

	void setNumbersRegions(const Ngl::TextureRegion **numbersRegions);
	inline void setColorMask(int colorMask) { this->colorMask = colorMask; }

	void draw(int number, SpriteBatch *spriteBatch);
};

#endif
