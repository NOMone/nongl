#pragma once

#include "Animation.h"
#include "GestureListener.h"
#include "Rect.h"

class View;
class SpriteBatch;

class ViewAnimationListener : public AnimationListener {
	View *view;
public:
	float x, y;
	float scaleX, scaleY;
	float scaleAboutCenter;
	int colorMask;

	inline ViewAnimationListener(View *targetView) { this->view = targetView; }
	virtual void onAnimationComponentApplied(AnimationTarget::Target target);
};

class View : public GestureListener {

protected:

	bool enabled;
	bool visible;
	View *parent;
	Ngl::Rect parentBounds;
	AnimationListener *animationListener;

public:

	inline View() { parent = 0; animationListener = 0; enabled = true; visible = true; }
	inline ~View() { if (animationListener) delete animationListener; }

	inline View *getParent() { return parent; }
	inline void setParent(View *parent) { this->parent = parent; }

	virtual inline void setEnabled(bool enabled) { this->enabled = enabled; }
	virtual inline bool isEnabled() { return (parent) ? parent->isEnabled() && enabled : enabled; }

	virtual inline void setVisible(bool visible) { this->visible = visible; }
	virtual inline bool isVisible() { return visible; }

	virtual inline float getWidth() { return 0; }
	virtual inline float getHeight() { return 0; }
	virtual inline float getScaledWidth() { return 0; }
	virtual inline float getScaledHeight() { return 0; }
	virtual inline float getLeft() { return 0; }
	virtual inline float getBottom() { return 0; }
	virtual inline float getRight() { return 0; }
	virtual inline float getTop() { return 0;  }
	virtual inline float getCenterX() { return 0; }
	virtual inline float getCenterY() { return 0; }
	virtual Ngl::Rect getBoundingRect();
	virtual inline float getScaleX() { return 1; }
	virtual inline float getScaleY() { return 1; }
	virtual inline int getColorMask() { return 0xffffffff; }
    virtual inline float getDepth() { return -1; } // The depth you use for perspective,

	virtual inline void setWidth(float width) {}
	virtual inline void setHeight(float height) {}
	virtual inline void setLeft(float left) {}
	virtual inline void setBottom(float bottom) {}
	virtual inline void setRight(float right) {}
	virtual inline void setTop(float top) {}
	virtual inline void setCenterX(float centerX) {}
	virtual inline void setCenterY(float centerY) {}
	virtual inline void setCenter(float centerX, float centerY) {}
	virtual inline void setScale(float scaleX, float scaleY) {}
	virtual inline void setColorMask(int colorMask) {}
    virtual inline void setDepth(float depth) {} // The depth you use for perspective,

	virtual inline void onJustLayouted() {}
	virtual bool withinParentBounds();
	virtual void setParentBounds(const Ngl::Rect &parentBounds) { this->parentBounds = parentBounds; }
	virtual inline void updateDepthLayersCount() {}
	virtual inline int getDepthLayersCount() { return 1; }
	virtual inline void adjustDepths(float maxZ, float minZ) {} // Note: you are free to using both maxZ and minZ inclusively.
	virtual inline void drawOpaqueParts(SpriteBatch *spriteBatch) {}
	virtual inline void drawTransparentParts(SpriteBatch *spriteBatch) {}
	virtual inline void drawAllInOrder(SpriteBatch *spriteBatch) {}

	virtual inline void update(float elapsedTimeMillis) {}

	virtual inline bool onTouchEvent(const TouchEvent *event) { return false; }

	virtual void attachToAnimation(Animation *animation);
};

class ColorView : public View {

	float x, y, z, width, height;
	int color;

public:

	inline ColorView() {}
	inline ColorView(float x, float y, float width, float height, int color) { set(x, y, width, height, color); }

	inline void set(float x, float y, float width, float height, int color) {
		this->x = x;
		this->y = y;
		this->z = 0;
		this->width = width;
		this->height = height;
		this->color = color;
	}

	virtual inline float getWidth() { return width; }
	virtual inline float getHeight() { return height; }
	virtual inline float getScaledWidth() { return width; }
	virtual inline float getScaledHeight() { return height; }
	virtual inline float getLeft() { return x; }
	virtual inline float getBottom() { return y; }
	virtual inline float getRight() { return x+width; }
	virtual inline float getTop() { return y+height;  }
	virtual inline float getCenterX() { return x+(width*0.5f);  }
	virtual inline float getCenterY() { return y+(height*0.5f);  }

	virtual inline void setWidth(float width) { this->width = width; }
	virtual inline void setHeight(float height) { this->height = height; }
	virtual inline void setLeft(float left) { x = left; }
	virtual inline void setBottom(float bottom) { y = bottom; }
	virtual inline void setRight(float right) { x = right-width; }
	virtual inline void setTop(float top) { y = top-height; }
	virtual inline void setCenterX(float centerX) { x = centerX-(width*0.5f); }
	virtual inline void setCenterY(float centerY) { y = centerY-(height*0.5f); }
	virtual inline void setCenter(float centerX, float centerY) { x = centerX-(width*0.5f); y = centerY-(height*0.5f); }

	virtual inline void adjustDepths(float maxZ, float minZ) { this->z = maxZ; }

	virtual void drawOpaqueParts(SpriteBatch *spriteBatch);
	virtual void drawTransparentParts(SpriteBatch *spriteBatch);
	virtual void drawAllInOrder(SpriteBatch *spriteBatch);
};

class TouchableView : public ColorView {
    Runnable *onTouchTask;
    bool discardOutOfBounds;
public:
    
	inline TouchableView(
            float x, float y,
            float width, float height,
            Runnable *onTouchTask,
            bool discardOutOfBounds=true,
            int color=0x00000000) : ColorView(x, y, width, height, color) {
        this->onTouchTask = onTouchTask;
        this->discardOutOfBounds = discardOutOfBounds;
    }
    
	bool testHit(float x, float y);
    virtual bool onTouchEvent(const TouchEvent *event);
};
