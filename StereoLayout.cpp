#include <Nongl/StereoLayout.h>
#include <Nongl/utils.h>

using namespace Ngl;

StereoLayout::StereoLayout(float x, float y, float width, float height, bool deletesChildrenOnDesruction) :
    AbsoluteLayout(x, y, width, height, false) {

    // Create two absolute layouts, left and right,
    // Left,
    leftLayout = new AbsoluteLayout(0, 0, width*0.5f, height, false);
    leftLayout->clipping = true;
    AbsoluteLayout::addView(leftLayout);

    // Right,
    rightLayout = new AbsoluteLayout(leftLayout->getWidth(), 0, width-(leftLayout->getWidth()), height, false);
    rightLayout->clipping = true;
    AbsoluteLayout::addView(rightLayout);
}

StereoLayout::~StereoLayout() {
    int32_t childrenCount = rightLayout->getChildViewsCount();
    for (int i=childrenCount-1; i>-1; i--) {
        removeView(i);
    }
}


void StereoLayout::draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly) {

    // TODO: make them into properties,
    float eyeOffset = 50;

    // If not stereo, then it's a regular draw,
    if (!stereo) {
        AbsoluteLayout::draw(spriteBatch, drawAllInOrder, drawTransparentOnly);
        return ;
    }

    // Apply perspective,
    // Adjust positions,
    float centerX = rightLayout->getWidth () * 0.5f;
    float centerY = rightLayout->getHeight() * 0.5f;
    int32_t childrenCount = rightLayout->getChildViewsCount();

    // Right eye,
    oldPositions.clear();
    for (int32_t i=0; i<childrenCount; i++) {

        View *view = rightLayout->getView(i);
        oldPositions.emplace_back(view->getLeft(), view->getBottom(), view->getScaleX(), view->getScaleY());

        float depth = view->getDepth();
        if (depth == -1) continue; // This view is very far.

        float ratio = 1/depth;
        view->setScale(view->getScaleX()*ratio, view->getScaleY()*ratio);
        view->setColorMask(rgba(255*ratio, 255*ratio, 255*ratio, 255));
        view->setLeft  (centerX + (view->getLeft  ()+eyeOffset-centerX)*ratio);
        view->setBottom(centerY + (view->getBottom()          -centerY)*ratio);
    }

    // Draw right eye,
    leftLayout->setVisible(false);
    AbsoluteLayout::draw(spriteBatch, drawAllInOrder, drawTransparentOnly);

    // Restore,
    for (int32_t i=0; i<childrenCount; i++) {
        View *view = rightLayout->getView(i);
        Position &position = oldPositions[i];
        view->setLeft  (position.x);
        view->setBottom(position.y);
    }

    // Left eye,
    for (int32_t i=0; i<childrenCount; i++) {

        View *view = leftLayout->getView(i);
        float depth = view->getDepth();
        if (depth == -1) continue; // This view is very far.

        // Left eye,
        float ratio = 1/depth;
        view->setLeft  (centerX + (view->getLeft  ()-(eyeOffset+centerX))*ratio);
        view->setBottom(centerY + (view->getBottom()           -centerY) *ratio);
    }

    // Draw left eye,
    rightLayout->setVisible(false);
    leftLayout->setVisible(true);
    AbsoluteLayout::draw(spriteBatch, drawAllInOrder, drawTransparentOnly);
    rightLayout->setVisible(true);

    // Restore,
    for (int32_t i=0; i<childrenCount; i++) {
        View *view = rightLayout->getView(i);
        Position &position = oldPositions[i];
        view->setScale(position.scaleX, position.scaleY);
        view->setLeft  (position.x);
        view->setBottom(position.y);
    }
}

void StereoLayout::setStereo(bool stereo) {

    if (this->stereo==stereo) return;

    this->stereo = stereo;
    if (stereo) {
        rightLayout->setVisible(true);
        leftLayout ->setWidth(getWidth()*0.5f);
    } else {
        rightLayout->setVisible(false);
        leftLayout ->setWidth(getWidth());
    }
}

void StereoLayout::sort() {

    int32_t childrenCount = rightLayout->getChildViewsCount();
    for (int32_t i=0; i<childrenCount; i++) {

        View *view1 = getView(i);
        if (view1->getDepth()==-1) continue;
        for (int32_t j=i+1; j<childrenCount; j++) {
            View *view2 = getView(j);
            if (view2->getDepth()==-1) continue;
            if (view1->getDepth() < view2->getDepth()) {
                removeView(i);
                addView(view2, i);
                removeView(j);
                addView(view1, j);
                View *tempView = view1;
                view1 = view2;
                view2 = view1;
            }
        }
    }
}
