#include "ImageView.h"
#include "utils.h"

void ImageViewAnimationListener::onAnimationComponentApplied(AnimationTarget::Target target) {

	if (target == AnimationTarget::SCALE_ABOUT_CENTER) {
		float centerX = imageView->getCenterX();
		float centerY = imageView->getCenterY();
		imageView->setScale(scaleAboutCenter, scaleAboutCenter);
		imageView->setCenter(centerX, centerY);
	}
}

void ImageView::draw(SpriteBatch *spriteBatch) {

	if (sprite.rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(sprite.rotation);
		float sinAngle = sinf(sprite.rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		sprite.setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);

		// Draw,
		sprite.draw(spriteBatch);

		// Restore original position,
		sprite.setCenter(centerX, centerY);

	} else {

		// Just draw,
		sprite.draw(spriteBatch);
	}
}

void ImageView::drawOpaqueParts(SpriteBatch *spriteBatch) {
	if (sprite.textureRegion && !sprite.textureRegion->hasAlpha) draw(spriteBatch);
}

void ImageView::drawTransparentParts(SpriteBatch *spriteBatch) {
	if (sprite.textureRegion && sprite.textureRegion->hasAlpha) draw(spriteBatch);
}

void ImageView::drawAllInOrder(SpriteBatch *spriteBatch) {
	if (sprite.textureRegion) draw(spriteBatch);
}

void ImageView::setupAnimationListener(Animation *animation) {
	if (!animationListener) animationListener = (AnimationListener *) new ImageViewAnimationListener(this);
	animation->setAnimationListener(animationListener);
}

void ImageView::attachToAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += sprite.x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += sprite.x;
				currentComponent->setTargetAddress(&sprite.x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += sprite.y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += sprite.y;
				currentComponent->setTargetAddress(&sprite.y);
				break;

			case AnimationTarget::YAW:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += sprite.rotation;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += sprite.rotation;
				currentComponent->setTargetAddress(&sprite.rotation);
				break;

			case AnimationTarget::COLOR_MASK:
				if (currentComponent->initialValueRelative) currentComponent->initialValueInt = multiplyColors(currentComponent->initialValueInt, sprite.colorMask);
				if (currentComponent->  finalValueRelative) currentComponent->finalValueInt   = multiplyColors(currentComponent->finalValueInt  , sprite.colorMask);
				currentComponent->setTargetAddress(&sprite.colorMask, true);
				break;

			case AnimationTarget::SCALE_X:
				currentComponent->setTargetAddress(&sprite.scaleX);
				break;

			case AnimationTarget::SCALE_Y:
				currentComponent->setTargetAddress(&sprite.scaleY);
				break;

			case AnimationTarget::SCALE_ABOUT_CENTER:
				setupAnimationListener(animation);
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat *= sprite.scaleX;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat   *= sprite.scaleX;
				currentComponent->setTargetAddress(&((ImageViewAnimationListener *) animationListener)->scaleAboutCenter);
				break;

			default:
				break;
		}
	}
}
