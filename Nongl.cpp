#include "Nongl.h"

#include "SystemEvents.h"
#include "SystemLog.h"
#include "SystemInterface.h"
#include "Activity.h"
#include "Shaders/PlainShader.h"
#include "Shaders/SeparateAlphaShader.h"
#include "FrameBuffer.h"
#include "AudioManager.h"
#include "GestureDetector.h"
#include "glWrapper.h"
#include "DisplayManager.h"
#include "File.h"
#include "utils.h"

#include "../Src/Config.h"

#include <string.h>

using namespace Ngl;

/////////////////////////////////////////////////
// Static initializations
/////////////////////////////////////////////////

static bool initialized = false;

bool Nongl::mainCalled = false;
int Nongl::pausedInternallyCount = 0;
bool Nongl::pausedExternally = true;
bool Nongl::exitRecieved = false;

GestureDetector *Nongl::gestureDetector = 0;
GestureListener *Nongl::gestureListener = 0;
bool Nongl::discardTouchEventsTillFrameDraw = true;

PlainShader *Nongl::plainShader = 0;
SeparateAlphaShader *Nongl::separateAlphaShader = 0;
Shader *Nongl::lastUsedShader = 0;

int32_t Nongl::clearColor = 0x00000000;

bool Nongl::glBlendEnabled = true;
bool Nongl::glDepthTestEnabled = true;

FrameBuffer *Nongl::defaultFrameBuffer = 0;
FrameBuffer *Nongl::boundFrameBuffer = 0;

AudioManager *Nongl::audioManager = 0;


int Nongl::contextLostCount = 0;

std::vector<Activity *> Nongl::activities;
std::vector<Activity *> Nongl::alwaysOnTopActivities;

int Nongl::topActivityStackIndex=0;
int Nongl::bottomActivityStackIndex=-1;

Ngl::TextureRegion *Nongl::_whiteBlockTextureRegion=0;
Ngl::TextureRegion * const & Nongl::whiteBlockTextureRegion = Nongl::_whiteBlockTextureRegion;

Animation Nongl::defaultActivityEntranceAnimation;
Animation Nongl::defaultActivityExitAnimation;
Animation Nongl::defaultDialogEntranceAnimation;
Animation Nongl::defaultDialogExitAnimation;

FrameTimeTracker Nongl::frameTimeTracker;
float Nongl::maxAllowedFrameDurationMillis=40.0f;

Scheduler Nongl::scheduler;

/////////////////////////////////////////////////
// Life cycle
/////////////////////////////////////////////////

void Nongl::initialize() {

	// Gesture detector,
	class NonglGestureListener : public GestureListener {
		public: bool onTouchEvent(const TouchEvent *event) { return Nongl::onTouchEvent(event); }
	};
	gestureListener = new NonglGestureListener();
	gestureDetector = new GestureDetector(gestureListener);

	// Audio manager,
	audioManager = new AudioManager();

	// Default frame buffer,
	defaultFrameBuffer = new FrameBuffer();
	boundFrameBuffer = defaultFrameBuffer;

	// Assets,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	if (!TextBuffer(TEXTURE_PACK).isEmpty()) displayManager->loadTextureAtlas(Ngl::File(Ngl::FileLocation::ASSETS, TEXTURE_PACK));
	displayManager->setMaximumLoadedTexturesCount(6);

	// Default assets,
	// White block,
	const Ngl::TextureRegion *rawWhiteBlock = displayManager->getTextureRegion("NONGL.whiteBox");
	if (rawWhiteBlock) {
		_whiteBlockTextureRegion = new Ngl::TextureRegion(
				rawWhiteBlock->texture,
				rawWhiteBlock->imageName,
				rawWhiteBlock->x + 1, rawWhiteBlock->y + 1,
				rawWhiteBlock->width - 2, rawWhiteBlock->height - 2,
				rawWhiteBlock->originalWidth - 2, rawWhiteBlock->originalHeight -2,
				rawWhiteBlock->offsetX, rawWhiteBlock->offsetY,
				rawWhiteBlock->hasAlpha);
	}

	// Default Animations,
	Interpolator linearAnimationInterpolator;

	// Activity entrance animation,
	// Black,
	linearAnimationInterpolator.set(250, 0);
	defaultActivityEntranceAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			linearAnimationInterpolator,
			0, 0x00000000, false,
			0, 0x00000000, false);

	// Fade in,
	linearAnimationInterpolator.set(250, 250);
	defaultActivityEntranceAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			linearAnimationInterpolator,
			0, 0xff000000, false,
			0, 0xffffffff, false);

	// Activity exit animation,
	// Fade out,
	linearAnimationInterpolator.set(250, 0);
	defaultActivityExitAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			linearAnimationInterpolator,
			0, 0xffffffff, false,
			0, 0xff000000, false);

	// Dialog entrance,
	// Alpha,
	linearAnimationInterpolator.set(250, 0);
	defaultDialogEntranceAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			linearAnimationInterpolator,
			0, 0x00ffffff, false,
			0, 0xffffffff, false);

	// Dialog exit animation,
	// Alpha,
	defaultDialogExitAnimation.addComponent(
			AnimationTarget::COLOR_MASK,
			linearAnimationInterpolator,
			0, 0xffffffff, false,
			0, 0x00ffffff, false);

	// Mark as initialized,
	initialized = true;
}

void Nongl::destroy() {

	// Delete all living activities,
	for (int i=(int)activities.size()-1; i>-1; i--) delete activities[i];

	// Delete default resources,
	delete gestureDetector;
	delete gestureListener;
	delete audioManager;
	delete defaultFrameBuffer;
	delete DisplayManager::getSingleton();

	if (plainShader) delete plainShader;
	if (separateAlphaShader) delete separateAlphaShader;

	delete _whiteBlockTextureRegion;

	// Terminate the application,
	javaExit();
}

void Nongl::onSurfaceChanged(int width, int height) {

	// Notify gesture detector,
	gestureDetector->onSurfaceChanged();

	// Fix viewport,
	defaultFrameBuffer->width = width;
	defaultFrameBuffer->height = height;
	if (boundFrameBuffer == defaultFrameBuffer) {
		DisplayManager::getSingleton()->onDisplayChanged(width, height);
	}

	// Notify all activities,
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->onSurfaceChanged(width, height);
	}
}

void Nongl::onSurfaceCreated() {

	// Denote context lost so that resources are reloaded again,
	contextLostCount++;

	// Initialize NONGL,
	if (!initialized) initialize();

	// Set default opengl parameters,
	// Depth test,
	if (glDepthTestEnabled) {
		glEnable(GL_DEPTH_TEST);
	} else {
		glDisable(GL_DEPTH_TEST);
	}
	glDepthFunc(GL_LEQUAL); //GL_LESS); // Must be GL_LESS for drawing opaque parts in reverse order to work.

	// Enable alpha blending,
	if (glBlendEnabled) {
		glEnable(GL_BLEND);
	} else {
		glDisable(GL_BLEND);
	}
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Performance and quality tuning,
	// Back face culling,
	//glDisable(GL_CULL_FACE);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	// Dithering,
	glEnable(GL_DITHER);

	// Scissoring,
	glEnable(GL_SCISSOR_TEST);

	// Notify all activities,
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->onSurfaceCreated();
	}
}

void Nongl::discardOpenGlObjects() {

	// TODO: reset frame buffers...?

	// Reset textures,
	DisplayManager::getSingleton()->destroyAllTextures();

	// Reset shaders,
	lastUsedShader = 0;

	if (plainShader) {
		delete plainShader;
		plainShader = 0;
	}

	if (separateAlphaShader) {
		delete separateAlphaShader;
		separateAlphaShader = 0;
	}
}

void Nongl::tickTheClock() {
	frameTimeTracker.newFrame();
}

void Nongl::onNewFrame() {

	// If first frame, call main and resume,
	if (!mainCalled) {
		main();
        mainCalled = true;
        Nongl::onResume(true);
	}

	// Tick the clock,
	Nongl::tickTheClock();

	// Dispatch events,
	systemDispatchEvents();

    // If application paused, neglect elapsed time,
    if (pausedInternallyCount || pausedExternally) {
        frameTimeTracker.skipNextFrame();
        frameTimeTracker.newFrame();
    }
    
	// Allow touch events,
	discardTouchEventsTillFrameDraw = false;

	// Update the default scheduler,
	scheduler.updateAndDispatch(getFrameTime());

	// Sort activities,
	sortActivities();

	// Draw all active activities,
	onDrawFrame();

	// Terminate application if dead or exit signal received,
	bool applicationDied = (scheduler.getTasksCount()==0) && (activities.size()==0);
	if (applicationDied || exitRecieved) destroy();
}

void Nongl::onDrawFrame() {

	// Clear whole screen,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	// TODO: is this glScissor redundant (applyClipping should do it)?
	glScissor(0, 0, displayManager->getDisplayWidth(), displayManager->getDisplayHeight());
	clearScreen(clearColor, getDepthTestEnabled());
	// TODO: is this glScissor redundant (Activity::onBeforeDraw does it)?
	displayManager->applyClipping(false);

	// Get active activities count,
	int activeActivitiesCount = 0;
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		Activity *activity = *iterator;
		if (activity->isActive()) {
			activeActivitiesCount++;
		}
	}

	// Draw all active activities,
	if (activeActivitiesCount) {
		float depthPadding = 0.01f / activeActivitiesCount;
		float currentDepth = 0.99f;
		float paddedDepthStep = (0.98f + depthPadding) / activeActivitiesCount;
		float unpaddedDepthStep = paddedDepthStep - depthPadding;

		iterator = activities.begin();
		endIterator = activities.end();
		for (; iterator!=endIterator; ++iterator) {
			Activity *activity = *iterator;
			if (activity->isActive()) {
				float frameTime = activity->getFrameTime();
				activity->onBeforeDrawFrame(frameTime);
				activity->onDrawFrame(frameTime, currentDepth, currentDepth-unpaddedDepthStep);
				currentDepth -= paddedDepthStep;
			}
		}
	}
}

void Nongl::onPause(bool external) {

	auto logReceivedMessage = [external] (bool actionTaken) {
		if (actionTaken) {
			LOGE("Application paused. External: %s. Paused: externally: %s, internally: %d",
					external ? "yes" : "no", pausedExternally ? "yes" : "no", pausedInternallyCount);
		} else {
			LOGE("Received application pause. External: %s. Paused: externally: %s, internally: %d",
					external ? "yes" : "no", pausedExternally ? "yes" : "no", pausedInternallyCount);
		}
	};

	// Don't pause if not already paused,
	if (external) {
		if (pausedExternally) {
			logReceivedMessage(false);
			return ;
		}
		pausedExternally = true;
	} else {
		pausedInternallyCount++;

		// If already paused, don't pause again,
		if (pausedInternallyCount > 1) {
			logReceivedMessage(false);
			return ;
		}
	}

	logReceivedMessage(true);

	// Pause all activities,
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->onPause(external);
	}
}

void Nongl::onResume(bool external) {

	auto logReceivedMessage = [external] (bool actionTaken) {
		if (actionTaken) {
			LOGE("Application resumed. External: %s. Paused: externally: %s, internally: %d",
					external ? "yes" : "no", pausedExternally ? "yes" : "no", pausedInternallyCount);
		} else {
			LOGE("Received application resume. External: %s. Paused: externally: %s, internally: %d",
					external ? "yes" : "no", pausedExternally ? "yes" : "no", pausedInternallyCount);
		}
	};

    // If there's nothing to resume, just return,
	if (external) {

		// The external resume before main is delayed until main is called. Don't
		// even report it,
		if (!mainCalled) return ;

	    if (!pausedExternally) {
	    	logReceivedMessage(false);
	    	return;
	    }

		pausedExternally = false;
	} else {
		if (!pausedInternallyCount) {
	    	logReceivedMessage(false);
			return ;
		}

		pausedInternallyCount--;

		// If still paused, don't resume yet,
		if (pausedInternallyCount) {
			logReceivedMessage(false);
			return ;
		}
	}

	logReceivedMessage(true);
    
    if (gestureDetector) {
    	discardTouchEventsTillFrameDraw = true;
    	gestureDetector->cancelAllGestures();
    }

	frameTimeTracker.newFrame();

	// Resume all activities,
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->onResume(external);
	}
}

bool Nongl::onBackPressed() {

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if (activity->isInteractive() && activity->onBackPressed()) return true;
			if (activity->isConsumesAllInputs()) return !activity->isBuffersUnconsumedInputs();
		}
	};

	return false;
}

//////////////////////////////////
// Input events
//////////////////////////////////

bool Nongl::onMenuPressed() {

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if (activity->isInteractive() && activity->onMenuPressed()) return true;
			if (activity->isConsumesAllInputs()) return !activity->isBuffersUnconsumedInputs();
		}
	};

	return false;
}

void Nongl::onMenuItemSelected(int itemId) {

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if ((activity->isInteractive() && activity->onMenuItemSelected(itemId)) ||
				 activity->isConsumesAllInputs()) {
				break;	// Consumed.
			}
		}
	};
}

bool Nongl::onTouchDown(int pointerId, float x, float y) {

	if (discardTouchEventsTillFrameDraw) return true;

	// Transform x and y to design coordinates,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	x *= displayManager->designWidth ;
	y *= displayManager->designHeight;

	return gestureDetector->onTouchDown(pointerId, x, y);
}

bool Nongl::onTouchDrag(int pointerId, float x, float y) {

	if (discardTouchEventsTillFrameDraw) return true;

	// Transform x and y to design coordinates,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	x *= displayManager->designWidth ;
	y *= displayManager->designHeight;

	return gestureDetector->onTouchDrag(pointerId, x, y);
}

bool Nongl::onTouchUp(int pointerId, float x, float y) {

	if (discardTouchEventsTillFrameDraw) return true;

	// Transform x and y to design coordinates,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	x *= displayManager->designWidth ;
	y *= displayManager->designHeight;

	return gestureDetector->onTouchUp(pointerId, x, y);
}

bool Nongl::onTouchCancel(int pointerId, float x, float y) {

	if (discardTouchEventsTillFrameDraw) return true;

	// Transform x and y to design coordinates,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	x *= displayManager->designWidth ;
	y *= displayManager->designHeight;

	return gestureDetector->onTouchCancel(pointerId, x, y);
}

bool Nongl::onScroll(float x, float y, float xOffset, float yOffset) {

	if (discardTouchEventsTillFrameDraw) return true;

	// Transform x and y to design coordinates,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	x *= displayManager->designWidth ;
	y *= displayManager->designHeight;

	return gestureDetector->onScroll(x, y, xOffset, yOffset);
}

bool Nongl::onTouchEvent(const TouchEvent *event) {

	// Unlike onTouchDown/Drag/Up/Scroll, this event is called from the
	// gesture detector and not the native interface.

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if (activity->isInteractive() && activity->gestureListenersGroup.onTouchEvent(event)) return true;
			if (activity->isConsumesAllInputs()) return !activity->isBuffersUnconsumedInputs();
		}
	};

	return false;
}

bool Nongl::onKeyDown(int keyCode) {

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if (activity->isInteractive() && activity->onKeyDown(keyCode)) return true;
			if (activity->isConsumesAllInputs()) return !activity->isBuffersUnconsumedInputs();
		}
	};

	return false;
}

bool Nongl::onKeyUp(int keyCode) {

	// Notify all active activities back to front until consumed,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;
		if (activity->isActive()) {
			if (activity->isInteractive() && activity->onKeyUp(keyCode)) return true;
			if (activity->isConsumesAllInputs()) return !activity->isBuffersUnconsumedInputs();
		}
	};

	return false;
}

/////////////////////////////////////////////////
// Opengl state
/////////////////////////////////////////////////

void Nongl::setBlendEnabled(bool enabled) {

	if (Nongl::glBlendEnabled != enabled) {
		Nongl::glBlendEnabled = enabled;
		if (enabled) {
			glEnable(GL_BLEND);
		} else {
			glDisable(GL_BLEND);
		}
	}
}

void Nongl::setDepthTestEnabled(bool enabled) {

	if (Nongl::glDepthTestEnabled != enabled) {
		Nongl::glDepthTestEnabled = enabled;
		if (enabled) {
			glEnable(GL_DEPTH_TEST);
		} else {
			glDisable(GL_DEPTH_TEST);
		}
	}
}

void Nongl::bindFrameBuffer(FrameBuffer *frameBuffer) {

	if (!frameBuffer) frameBuffer = defaultFrameBuffer;
	if (boundFrameBuffer == frameBuffer) return ;

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->getId());
	if (frameBuffer->hasDepthBuffer()) glBindRenderbuffer(GL_RENDERBUFFER, frameBuffer->getDepthBufferId());

	boundFrameBuffer = frameBuffer;

	DisplayManager *displayManager = DisplayManager::getSingleton();
	displayManager->onDisplayChanged(frameBuffer->getWidth(), frameBuffer->getHeight());
	displayManager->applyClipping(false);
}

/////////////////////////////////////////////////
// Default assets
/////////////////////////////////////////////////

PlainShader *Nongl::getDefaultPlainShader() {

	if (!plainShader)
		plainShader = new PlainShader();

	return plainShader;
}

SeparateAlphaShader *Nongl::getDefaultSeparateAlphaShader() {

	if (!separateAlphaShader)
		separateAlphaShader = new SeparateAlphaShader();

	return separateAlphaShader;
}

/////////////////////////////////////////////////
// Activities
/////////////////////////////////////////////////

// Returns true if activity should be hidden according to the activities above it.
bool Nongl::getActivityHiddenState(Activity *activity) {

	// Iterate back to front,
	bool showingActivities = true;

	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *currentActivity = *iterator;

		if (showingActivities) {
			if (currentActivity == activity) return false;
			if (currentActivity->isHidesBelow()) {
				showingActivities = false;
			}
		} else if (currentActivity == activity) {
			return true;
		}
	}

	return true;
}

void Nongl::updateActivitiesHiddenState(Activity *activityToSkip) {

	// Iterate back to front,
	bool showingActivities = true;

	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *activity = *iterator;

		if (showingActivities) {
			if (activity!=activityToSkip) activity->show();
			if (activity->isHidesBelow()) {
				showingActivities = false;
			}
		} else {
			if (activity!=activityToSkip) activity->hide();
		}
	}
}

void Nongl::hideAllActivities(Activity *exception) {

	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		if ((*iterator) != exception) {
			(*iterator)->hide(false);
		}
	}
}

void Nongl::sortActivities() {

	// Nothing to sort,
	if (activities.empty()) return ;

	// One activity can't be unsorted,
	if (activities.size() == 1) {

		// Make sure it's visible,
		activities[0]->show();
		return ;
	}

	// Bring all always on front activities to front,
	bringAlwaysOnTopActivitiesToFront();

	// Sort activities based on their stack indices,
	// Iterate back to front,
	std::vector<Activity *>::iterator iterator, beginIterator, beforeEndIterator;
	beforeEndIterator = activities.end() - 1;
	beginIterator = activities.begin();
	iterator = beforeEndIterator;
	while (iterator != beginIterator) {

		--iterator;

		// Float this activity to the top if needed,
		std::vector<Activity *>::iterator bubbleIterator = iterator;
		while (bubbleIterator != beforeEndIterator) {

			Activity *currentActivity = *bubbleIterator;
			bubbleIterator++;
			Activity *activityAbove = *bubbleIterator;

			if ((activityAbove->getActivityStackIndex() < currentActivity->getActivityStackIndex()) &&
				//(!currentActivity->isAnimating()) &&
				((!activityAbove->isActive()) || (!activityAbove->isAnimating())) ) {

				// Swap activities,
				*bubbleIterator = currentActivity;
				*(bubbleIterator-1) = activityAbove;

				// Settle activities hiding below states,
				currentActivity->setTemporarilyNotHidingBelow(false);
				activityAbove  ->setTemporarilyNotHidingBelow(false);
			} else {
				break;
			}
		}
	}

	// Update activities,
	updateActivitiesHiddenState();
}

Activity *Nongl::getActivity(const char *activityName) {

	// Search for the activity,
	std::vector<Activity *>::iterator iterator, endIterator;
	iterator = activities.begin();
	endIterator = activities.end();
	for (; iterator!=endIterator; ++iterator) {
		Activity *activity = *iterator;
		if (!strcmp(activity->getName(), activityName)) {
			return activity;
		}
	}

	return 0;
}

Activity *Nongl::getFrontActivity() {
	if (activities.empty()) return 0;
	return activities.back();
}

void Nongl::startActivity(Activity *activity, bool scheduleOnNextFrame) {

	if (scheduleOnNextFrame) {

		scheduler.schedule([] (void *data) -> bool {
			Activity *activity = (Activity *) data;
			Nongl::startActivity(activity, false);
			return true;
		}, 0, activity);

		return ;
	}

	activity->setActivityStackIndex(topActivityStackIndex++);
	activities.push_back(activity);
	//activities.insert(activities.begin(), activity);
}

void Nongl::bringActivityToFront(Activity *activity, bool scheduleOnNextFrame) {

	// Schedule bringing activity to front to next frame beginning,
	if (scheduleOnNextFrame) {
		scheduler.schedule([] (void *data) -> bool {
			Nongl::bringActivityToFront((Activity *) data, false);
			return true;
		}, 0, activity);
	} else {
		activity->setActivityStackIndex(topActivityStackIndex++);
	}
}

Activity *Nongl::bringActivityToFront(const char *activityName, bool scheduleOnNextFrame) {

	// Put to front,
	Activity *activity = getActivity(activityName);
	if (activity) bringActivityToFront(activity, scheduleOnNextFrame);

	return activity;
}

void Nongl::sendActivityToBack(Activity *activity, bool scheduleOnNextFrame) {

	// Schedule sending activity to back to next frame beginning,
	if (scheduleOnNextFrame) {
		scheduler.schedule([] (void *data) -> bool {
			Nongl::sendActivityToBack((Activity *) data, false);
			return true;
		}, 0, activity);
	} else {

		activity->setTemporarilyNotHidingBelow(true);

		// Set new stack index for the activity so that sorting should put it at the
		// very beginning when the hiding animation finishes,
		activity->setActivityStackIndex(bottomActivityStackIndex--);
	}
}

Activity *Nongl::sendActivityToBack(const char *activityName, bool scheduleOnNextFrame) {

	// Send to back,
	Activity *activity = getActivity(activityName);
	if (activity) sendActivityToBack(activity, scheduleOnNextFrame);

	return activity;
}

void Nongl::setActivityAlwaysOnTop(Activity *activity, bool alwaysOnTop) {

	if (alwaysOnTop) {
		setActivityAlwaysOnTop(activity, false);
		alwaysOnTopActivities.push_back(activity);
	} else {

		// Remove from the always on top list,
		auto iterator = alwaysOnTopActivities.begin();
		auto endIterator = alwaysOnTopActivities.end();

		while (iterator != endIterator) {
			if (*iterator == activity) {
				alwaysOnTopActivities.erase(iterator);
				return ;
			}
			++iterator;
		}
	}
}

void Nongl::bringAlwaysOnTopActivitiesToFront() {

	auto iterator = alwaysOnTopActivities.begin();
	auto endIterator = alwaysOnTopActivities.end();
	while (iterator != endIterator) {
		bringActivityToFront(*iterator, false);
		++iterator;
	}
}

void Nongl::removeActivity(Activity *activity) {

	std::vector<Activity *>::iterator iterator = activities.begin();

	while (iterator!=activities.end()) {
		Activity *currentActivity = *iterator;
		if (activity == currentActivity) {
			iterator = activities.erase(iterator);
		} else {
			++iterator;
		}
	};

	// Remove from always on top list,
	setActivityAlwaysOnTop(activity, false);

	// Remove all related scheduler tasks,
	scheduler.removeActivityTasks(activity);
}

bool Nongl::isActivityInputBlocked(const Activity *activity) {

	if ((!activity->isActive()) || (!activity->isInteractive())) return true;

	// Check all activities from top to bottom,
	std::vector<Activity *>::iterator iterator, beginIterator;
	iterator = activities.end();
	beginIterator = activities.begin();
	while (iterator != beginIterator) {
		--iterator;
		Activity *currentActivity = *iterator;
		if (currentActivity == activity) return false;
		if (currentActivity->isConsumesAllInputs()) return true;
	};

	return false;
}
