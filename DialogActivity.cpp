#include "Nongl.h"

#include "DialogActivity.h"

DialogActivity::DialogActivity(const char *name) : Activity(name) {
	setHidesBelow(false);
}

const Animation *DialogActivity::getEntranceAnimation() {
	return Nongl::getDefaultDialogEntranceAnimation();
}

const Animation *DialogActivity::getExitAnimation() {
	return Nongl::getDefaultDialogExitAnimation();
}

