#pragma once

#include "ManagedObject.h"

#include <math.h>

class Runnable;

#define PI 3.14159265359f
#define TWO_PI 6.283185307f

class Interpolator {

	float delay;
	float duration;
	float currentDuration;
	bool running;
	bool inverted;

	Ngl::ManagedPointer<Runnable> onInterpolationEndListener;

public:

	Interpolator();
	inline Interpolator(float duration, float delay=0) { set(duration, delay); }
	virtual inline ~Interpolator() {}

	virtual inline Interpolator *clone() const { return new Interpolator(*this); }

	void set(float duration, float delay=0);
	inline void reset() { running = false; currentDuration = 0; }
	inline void start() { running = true; }
	inline void restart() { currentDuration = 0; running = true; }
	inline void finish() { currentDuration = duration + delay; }
	inline void pause() { running = false; }
	inline void invertRemainingTime() { currentDuration = (duration + delay) - currentDuration; }
	inline void setInverted(bool inverted) { this->inverted = inverted; }
	inline bool isRunning() { return running; }
	inline bool isDelayed() { return currentDuration < delay; }
	inline bool hasFinished() { return currentDuration >= duration + delay; }
	inline bool isInverted() { return inverted; }
	inline float getTotalDuration() { return duration + delay; }
	virtual inline float getRatio() {
		float ratio = (currentDuration < delay) ? 0 : (currentDuration - delay) / duration;
		return inverted ? (1-ratio) : ratio;
	}
	inline float interpolate(float startValue, float endValue) { return startValue + ((endValue - startValue) * getRatio()); }
	int interpolateColors(int startColor, int endColor);

	inline void setOnInterpolationEndListener(Ngl::ManagedPointer<Runnable> onInterpolationEndListener) { this->onInterpolationEndListener = onInterpolationEndListener; }
	inline Ngl::ManagedPointer<Runnable> getOnInterpolationEndListener() { return onInterpolationEndListener; }

	// Returns true when finished.
	bool update(float elapsedTime);
};

class EaseInEaseOutInterpolator : public Interpolator {
public:
	inline EaseInEaseOutInterpolator(float duration=0, float delay=0) : Interpolator(duration, delay) {}
	virtual inline Interpolator *clone() const { return new EaseInEaseOutInterpolator(*this); }
	inline float getRatio() { return 0.5f - 0.5f * cosf(PI * Interpolator::getRatio()); }
};

class ElasticOutInterpolator : public Interpolator {
public:
	float damping;
	inline ElasticOutInterpolator(float duration=0, float delay=0) : Interpolator(duration, delay), damping(0) {}
	virtual inline Interpolator *clone() const { return new ElasticOutInterpolator(*this); }
	float getRatio();
	inline ElasticOutInterpolator &setDamping(float damping) { this->damping = damping; return *this; }
};

class ElasticInOutInterpolator : public Interpolator {
public:
	inline ElasticInOutInterpolator(float duration=0, float delay=0) : Interpolator(duration, delay) {}
	virtual inline Interpolator *clone() const { return new ElasticInOutInterpolator(*this); }
	float getRatio();
};

class CycleInterpolator : public Interpolator {
	float cyclesCount;
	bool allowNegative, damping;
public:
	inline CycleInterpolator(float duration=0, float delay=0, float cyclesCount=1, bool allowNegative=false, bool damping=false) :
				Interpolator(duration, delay),
				cyclesCount(cyclesCount), allowNegative(allowNegative), damping(damping) {}
	virtual inline Interpolator *clone() const { return new CycleInterpolator(*this); }
	float getRatio();
};
