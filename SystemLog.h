#pragma once

#include "../Src/Config.h"

void logI(const char *logTag, const char *format, ...);
void logW(const char *logTag, const char *format, ...);
void logE(const char *logTag, const char *format, ...);

inline void dummyFunction() {}

#define LOG_TAG "Nongl"
#define LOGI(format, ...) ((VERBOSE) ? logI(LOG_TAG, format, ##__VA_ARGS__) : dummyFunction())
#define LOGW(format, ...) ((VERBOSE) ? logW(LOG_TAG, format, ##__VA_ARGS__) : dummyFunction())
#define LOGE(format, ...) ((VERBOSE) ? logE(LOG_TAG, format, ##__VA_ARGS__) : dummyFunction())
