#include "Gravity.h"

void Gravity::getGravityComponents(Gravity::Value gravity, VerticalGravity::Value *outVerticalGravity, HorizontalGravity::Value *outHorizontalGravity) {

	switch (gravity) {
		case Gravity::TOP_LEFT:
			*outVerticalGravity = VerticalGravity::TOP;
			*outHorizontalGravity = HorizontalGravity::LEFT;
			return ;
		case Gravity::TOP_CENTER:
			*outVerticalGravity = VerticalGravity::TOP;
			*outHorizontalGravity = HorizontalGravity::CENTER;
			return ;
		case Gravity::TOP_RIGHT:
			*outVerticalGravity = VerticalGravity::TOP;
			*outHorizontalGravity = HorizontalGravity::RIGHT;
			return ;

		case Gravity::CENTER_LEFT:
			*outVerticalGravity = VerticalGravity::CENTER;
			*outHorizontalGravity = HorizontalGravity::LEFT;
			return ;
		case Gravity::CENTER:
			*outVerticalGravity = VerticalGravity::CENTER;
			*outHorizontalGravity = HorizontalGravity::CENTER;
			return ;
		case Gravity::CENTER_RIGHT:
			*outVerticalGravity = VerticalGravity::CENTER;
			*outHorizontalGravity = HorizontalGravity::RIGHT;
			return ;

		case Gravity::BOTTOM_LEFT:
			*outVerticalGravity = VerticalGravity::BOTTOM;
			*outHorizontalGravity = HorizontalGravity::LEFT;
			return ;
		case Gravity::BOTTOM_CENTER:
			*outVerticalGravity = VerticalGravity::BOTTOM;
			*outHorizontalGravity = HorizontalGravity::CENTER;
			return ;
		case Gravity::BOTTOM_RIGHT:
			*outVerticalGravity = VerticalGravity::BOTTOM;
			*outHorizontalGravity = HorizontalGravity::RIGHT;
			return ;
	}
}

VerticalGravity::Value Gravity::getVerticalGravity(Gravity::Value gravity) {

	switch (gravity) {
		case Gravity::TOP_LEFT:
		case Gravity::TOP_CENTER:
		case Gravity::TOP_RIGHT:
			return VerticalGravity::TOP;

		case Gravity::CENTER_LEFT:
		case Gravity::CENTER:
		case Gravity::CENTER_RIGHT:
			return VerticalGravity::CENTER;

		//case Gravity::BOTTOM_LEFT:
		//case Gravity::BOTTOM_CENTER:
		//case Gravity::BOTTOM_RIGHT:
		default:
			return VerticalGravity::BOTTOM;
	}
}

HorizontalGravity::Value Gravity::getHorizontalGravity(Gravity::Value gravity) {

	switch (gravity) {

		case Gravity::TOP_RIGHT:
		case Gravity::CENTER_RIGHT:
		case Gravity::BOTTOM_RIGHT:
			return HorizontalGravity::RIGHT;

		case Gravity::TOP_CENTER:
		case Gravity::CENTER:
		case Gravity::BOTTOM_CENTER:
			return HorizontalGravity::CENTER;

		//case Gravity::TOP_LEFT:
		//case Gravity::CENTER_LEFT:
		//case Gravity::BOTTOM_LEFT:
		default:
			return HorizontalGravity::LEFT;
	}
}

Gravity::Value Gravity::getGravityFromComponents(VerticalGravity::Value verticalGravity, HorizontalGravity::Value horizontalGravity) {

	switch (verticalGravity) {
		case VerticalGravity::TOP:
			switch (horizontalGravity) {
				case HorizontalGravity::RIGHT: return Gravity::TOP_RIGHT;
				case HorizontalGravity::CENTER: return Gravity::TOP_CENTER;
				case HorizontalGravity::LEFT:
				default: return Gravity::TOP_LEFT;
			}
			break;

		case VerticalGravity::CENTER:
			switch (horizontalGravity) {
				case HorizontalGravity::RIGHT: return Gravity::CENTER_RIGHT;
				case HorizontalGravity::CENTER: return Gravity::CENTER;
				case HorizontalGravity::LEFT:
				default: return Gravity::CENTER_LEFT;
			}
			break;

		case VerticalGravity::BOTTOM:
			switch (horizontalGravity) {
				case HorizontalGravity::RIGHT: return Gravity::BOTTOM_RIGHT;
				case HorizontalGravity::CENTER: return Gravity::BOTTOM_CENTER;
				case HorizontalGravity::LEFT:
				default: return Gravity::BOTTOM_LEFT;
			}
			break;
	}
}
