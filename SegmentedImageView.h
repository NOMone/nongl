#pragma once

#include "SegmentedSprite.h"
#include "View.h"

class SegmentedImageView : public View {

	SegmentedSprite sprite;

	float x, y;
	float scaleX, scaleY;
	float rotation;
	float rotationPivotXDisplacement, rotationPivotYDisplacement;

	bool modified;

public:

	SegmentedImageView(int columnsCount, int rowsCount, const char *regionPrefix, float imageWidth, float imageHeight);

	inline float getScaledWidth() { return sprite.getWidth() * scaleX; }
	inline float getScaledHeight() { return sprite.getHeight() * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + getScaledWidth(); }
	inline float getTop() { return y + getScaledHeight();  }
	inline float getCenterX() { return x + (0.5f * getScaledWidth()); }
	inline float getCenterY() { return y + (0.5f * getScaledHeight()); }
	inline float getDepth() { return sprite.getDepth(); }
	inline float getScaleX() { return scaleX; }
	inline float getScaleY() { return scaleY; }

	inline void setLeft(float left) { this->x = left; modified = true; }
	inline void setBottom(float bottom) { this->y = bottom; modified = true; }
	inline void setRight(float right) { this->x = right - getScaledWidth(); modified = true; }
	inline void setTop(float top) { this->y = top - getScaledHeight(); modified = true; }
	inline void setCenterX(float centerX) { this->x = centerX - (0.5f * getScaledWidth()); modified = true; }
	inline void setCenterY(float centerY) { this->y = centerY - (0.5f * getScaledHeight()); modified = true; }
	inline void setCenter(float centerX, float centerY) { this->x = centerX - (0.5f * getScaledWidth()); this->y = centerY - (0.5f * getScaledHeight()); modified = true; }
	inline void setDepth(float z) { sprite.setDepth(z); }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; modified = true; }

	inline void setRotation(float angleRadians) { this->rotation = angleRadians; modified = true; }
	inline void setRotationPivotDisplacementFromCenter(float xDisplacement, float yDisplacement) {
		this->rotationPivotXDisplacement = xDisplacement;
		this->rotationPivotYDisplacement = yDisplacement;
		modified = true;
	}

	inline void adjustDepths(float maxZ, float minZ) { sprite.setDepth(maxZ); }

	void drawOpaqueParts(SpriteBatch *spriteBatch);
	void drawTransparentParts(SpriteBatch *spriteBatch);
	void drawAllInOrder(SpriteBatch *spriteBatch);

	void updateSprite();
	void attachToAnimation(Animation *animation);
};
