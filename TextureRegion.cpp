#include "TextureRegion.h"
#include "Texture.h"

Ngl::TextureRegion::TextureRegion(
			Texture *texture,
			const TextBuffer &imageName,
			int x, int y,
			int width, int height,
			int originalWidth, int originalHeight,
			int offsetX, int offsetY,
			bool hasAlpha) {

	this->texture = texture;
	this->imageName = imageName;
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->originalWidth = originalWidth;
	this->originalHeight = originalHeight;
	this->offsetX = offsetX;
	this->offsetY = offsetY;
	this->hasAlpha = hasAlpha;

	float textureWidth = texture->getWidth();
	float textureHeight = texture->getHeight();
	u1 = x / textureWidth;
	v1 = y / textureHeight;
	u2 = (x + width) / textureWidth;
	v2 = (y + height) / textureHeight;
}
