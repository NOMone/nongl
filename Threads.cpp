#include <Nongl/Threads.h>

Runnable *Runnable::createRunnable(std::function<bool(void *data)> runnable, Activity *activity) {

    class Wrapper : public Runnable {
        std::function<bool(void *data)> runnable;
    public:
        Wrapper(std::function<bool(void *data)> runnable, Activity *activity) :
            Runnable(activity), runnable(runnable) {}

        bool run(void *data) {
            return runnable(data);
        }
    };

    return new Wrapper(runnable, activity);
}
