#include "NumbersArea.h"

NumbersArea::NumbersArea(float centerX, float centerY, float width, float height, float scale) :
	centerX(centerX), centerY(centerY), width(width), height(height), scale(scale)
{
	colorMask = 0xffffffff;
}

void NumbersArea::setNumbersRegions(const Ngl::TextureRegion **numbersRegions)
{
	for (int i=0; i<10; i++)
		this->numbersRegions[i] = numbersRegions[i];
}

void NumbersArea::draw(int number, SpriteBatch *spriteBatch)
{
	// Calculate the number size, if it fits in area dimensions then just draw it. Otherwise,
	// apply more scaling,

	// Get the individual digits and number total dimensions,
	int digits[20];
	int digitsCount = 0;
	float totalWidth = 0;
	float maxHeight = 0;
	int temp = number;
	do
	{
		digits[digitsCount] = temp % 10;
		temp /= 10;

		totalWidth += numbersRegions[digits[digitsCount]]->originalWidth;
		if (numbersRegions[digits[digitsCount]]->originalHeight > maxHeight)
			maxHeight = numbersRegions[digits[digitsCount]]->originalHeight;

		digitsCount++;
	} while (temp);

	// Find the final scale,
	float finalScale = scale;
	if ((totalWidth  * scale > width ) ||
		(maxHeight * scale > height))
	{
		float widthScale = width / totalWidth;
		float heightScale = height / maxHeight;
		finalScale = (widthScale < heightScale) ? widthScale : heightScale;
	}

	// Draw,
	// Prepare the sprite,
	float finalWidth = totalWidth * finalScale;
	float finalHeight = maxHeight * finalScale;

	digitSprite.setFromTextureRegion(numbersRegions[digits[0]]);
	digitSprite.scaleX = digitSprite.scaleY = finalScale;
	digitSprite.setBottom(centerY - (finalHeight * 0.5f));
	digitSprite.colorMask = colorMask;

	// Do the drawing,
	float leftX = centerX - (finalWidth * 0.5f);
	for (digitsCount--; digitsCount>-1; digitsCount--)
	{
		digitSprite.textureRegion = numbersRegions[digits[digitsCount]];
		digitSprite.width = numbersRegions[digits[digitsCount]]->originalWidth;
		digitSprite.height = numbersRegions[digits[digitsCount]]->originalHeight;
		digitSprite.setLeft(leftX);

		spriteBatch->drawSprite(&digitSprite);

		leftX += digitSprite.getScaledWidth();
	}
}
