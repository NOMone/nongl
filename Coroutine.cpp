#include <Nongl/Coroutine.h>
#include <Nongl/Nongl.h>

using namespace Ngl;

void Coroutine::add(const std::function<bool()> &function) {
    functions.push_back(function);
}

void Coroutine::run() {

    Ngl::weak_ptr<AliveToken> weakAliveToken(aliveToken);

    RunnableFunction *runnable = new RunnableFunction([&, weakAliveToken](void *)mutable->bool {

        // If the coroutine is dead, stop running immediately,
        if (weakAliveToken.expired()) return true;

        // If the coroutine is finished, stop running,
        if (currentFunctionIndex >= functions.size()) return true;

        // If sleeping indefinitely, continue sleeping,
        if (sleepingIndefinitely) return false;

        // If sleeping for a certain period, continue to sleep,
        if (timeToSleepMillis) {
            timeToSleepMillis -= Nongl::getFrameTime();
            if (timeToSleepMillis <= 0) timeToSleepMillis = 0;

            // Continue the coroutine,
            return false;
        }

        // Execute the next coroutine step,
        if (functions[currentFunctionIndex]()) currentFunctionIndex++;

        // Continue running the coroutine,
        return false;
    });

    // Schedule the runnable,
    Ngl::shared_ptr<Runnable> sharedRunnable(runnable);
    Nongl::scheduler.schedule(sharedRunnable, 0);
}
