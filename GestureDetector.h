#pragma once

#include "GestureListener.h"
#include "TouchState.h"

class GestureDetector {
	GestureListener *gestureListener;

	TouchState touchState;
	TouchEvent touchEvent;

	int pointersDownCount;
	bool singlePointerGesture;

	// TODO: move these to the touch state. Make them per pointer (finger).
	double touchDownTime;
	float touchDownX, touchDownY;
	float maxDraggedDistanceCms;
	double flickStartTime;
	float flickStartX, flickStartY;
	float lastZoomRatio;

	int pointer1Id, pointer2Id;
	float pointer1TouchDownX, pointer1TouchDownY;
	float pointer1CurrentX, pointer1CurrentY;
	float pointer2TouchDownX, pointer2TouchDownY;
	float pointer2CurrentX, pointer2CurrentY;

	void init(GestureListener *gestureListener=0);

public:

	GestureDetector();
	GestureDetector(GestureListener *gestureListener);

	inline void reset() { init(gestureListener); }

	void setGestureListener(GestureListener *gestureListener);

	void cancelAllGestures();
	void onSurfaceChanged();

	void doLostTouchUpMitigation(int concernedPointerId);

	// TODO: gesture detector should ALWAYS consume events. No need for return values.
	bool onTouchDown(int pointerId, float x, float y);
	bool onTouchDrag(int pointerId, float x, float y);
	bool onTouchUp(int pointerId, float x, float y);
	bool onTouchCancel(int pointerId, float x, float y);
	bool onScroll(float x, float y, float xOffset, float yOffset);
};
