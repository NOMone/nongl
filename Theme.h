#pragma once

#include "Gravity.h"

#include <queue>

class View;
class Layout;
class TextView;

namespace Ngl {

	class Theme {
	public:

		virtual ~Theme();

		static Theme &getGlobalTheme();

		virtual float getDialogLayoutPadding() { return 0; }
		virtual View *createDialogBackground(float width, float height)=0;
		virtual Layout *createDialogLayout(float width, float height);
		virtual TextView *createText(const char *text, float maxWidth)=0;

		virtual View *createNotificationBackground(float width, float height) { return createDialogBackground(width, height); }
		virtual Layout *createNotificationLayout(float width, float height);
		virtual TextView *createNotificationText(const char *text, float maxWidth) { return createText(text, maxWidth); }
	};

	class WhiteTheme : public Theme {
	public:
		float getDialogLayoutPadding() { return 30; }
		View *createDialogBackground(float width, float height);
		TextView *createText(const char *text, float maxWidth);

		View *createNotificationBackground(float width, float height);
		Layout *createNotificationLayout(float width, float height);
		TextView *createNotificationText(const char *text, float maxWidth);
	};

	class CandypedeTheme : public WhiteTheme {
	public:
        View *createDialogBackground(float width, float height);
        View *createNotificationBackground(float width, float height);
        TextView *createNotificationText(const char *text, float maxWidth);
	};
}
