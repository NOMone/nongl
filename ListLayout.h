#pragma once

#include "ScrollableLayout.h"
#include "LinearLayout.h"
#include "TextView.h"

#include <vector>
#include <list>

/////////////////////////////
// List item.
/////////////////////////////

class ListLayout;
class ListItem {
protected:
	ListLayout *parentListView;
	bool selected;
public:
	inline ListItem() : parentListView(0), selected(false) {}
	virtual inline ~ListItem() {}

	inline void setListView(ListLayout *parentListView) { this->parentListView = parentListView; }

	virtual inline void setRecommendedMaxWidth(float recommendedMaxWidth) {}
	virtual inline void setRecommendedMaxHeight(float recommendedMaxHeight) {}
	virtual inline void setSelected(bool selected) { this->selected = selected; }
	virtual inline bool isSelected() { return selected; }

	virtual View *getView()=0;
};

/////////////////////////////
// On item selected listener.
/////////////////////////////

class OnListItemSelectedListener {
public:
	inline virtual ~OnListItemSelectedListener() {}
	inline virtual void onListItemSelected(ListItem *item) {}
};

/////////////////////////////
// List layout.
/////////////////////////////

class ListLayout : protected ScrollableLayout, public OnListItemSelectedListener {
	LinearLayout linearLayout;
	OnListItemSelectedListener *onListItemSelectedListener;

	std::vector<ListItem *> items;
	std::list<ListItem *> selectedItems;
	int maxSelectedItemsCount;

public:
	ListLayout();
	ListLayout(float x, float y, float z, float width, float height, OnListItemSelectedListener *onListItemSelectedListener=0, Orientation::Value orientation=Orientation::VERTICAL);
	void set(float x, float y, float z, float width, float height, OnListItemSelectedListener *onListItemSelectedListener=0, Orientation::Value orientation=Orientation::VERTICAL);

	inline void setMaxSelectedItemsCount(int maxSelectedItemsCount) { this->maxSelectedItemsCount = maxSelectedItemsCount; }
	inline View *getView() { return (View *) this; }

	void addItem(ListItem *item);

	void layout();

	void onListItemSelected(ListItem *item);
};

/////////////////////////////
// Common list items.
/////////////////////////////

class TextListItem : public TextView, public ListItem {
	void fixHeight();
	int selectedBackColor;
	float backColorDepthOffset;
public:
	TextListItem(const char *text, float width, float height, int color, int selectedBackColor, float backColorDepthOffset=0.001f);
	void setRecommendedMaxWidth(float recommendedMaxWidth);
	void setRecommendedMaxHeight(float recommendedMaxHeight);
	inline View *getView() { return this; }

	void drawTransparentParts(SpriteBatch *spriteBatch);
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { drawTransparentParts(spriteBatch); }

	bool onTouchEvent(const TouchEvent *event);
};

