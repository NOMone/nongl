#pragma once

#include "SpriteBatch.h"
#include "TextureRegion.h"
#include "Point3D.h"

class QuadPoint : public Point3D {
public:
	int colorMask;
	inline QuadPoint() { this->colorMask = 0xffffffff; }
};

// CONVEX quads ONLY.
class Quad {
public:

	// Quad points order:
	// ==================
	//
	//  4                      3
	//   \--------------------/
	//    \                  /
	//     \ Arbitrary Quad /
	//      \              /
	//       \------------/
	//        1          2
	//
	enum pointPosition { BOTTOM_LEFT_POINT=0, BOTTOM_RIGHT_POINT, TOP_RIGHT_POINT, TOP_LEFT_POINT };
	QuadPoint points[4];

	float x, y, z;
	float scaleX, scaleY, scaleZ;   //
	float rotation;                 // Scale, rotation and flip are around (0, 0, 0) before translation.
	bool flipX, flipY;              //
	int colorMask;
	const Ngl::TextureRegion *textureRegion;

	Quad();
	Quad(const Ngl::TextureRegion *textureRegion, const QuadPoint *points=0);
	void set(const Ngl::TextureRegion *textureRegion, const QuadPoint *points=0);

	void resetPoints();
	void setPoints(const QuadPoint *points);

	inline void setTextureRegion(const Ngl::TextureRegion *textureRegion) { this->textureRegion = textureRegion; }
	inline Ngl::Texture *getTexture() { return textureRegion->texture; }

	void roll(float angleRadian);
	void pitch(float angleRadian);
	void yaw(float angleRadian);

	// Note: this method ignores texture region offset totally (trimming). To avoid trimming a certain region
	// while packing, add two almost 100% transparent pixels to opposite corners of the region.
	void draw(float *vertexData, unsigned short *indices, int verticesOffset);
	void drawWireFrame(SpriteBatch *spriteBatch, int color);
	inline void draw(SpriteBatch *spriteBatch) { spriteBatch->drawQuad(this); }
};
