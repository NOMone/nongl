
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := natives
APP_MODULES := stlport_shared.so
#APP_MODULES := gnustl_shared.so

LOCAL_CFLAGS := -DKTX_OPENGL_ES2=1 -DSUPPORT_SOFTWARE_ETC_UNPACK=0 -DLIB_KTX_ENABLED
LOCAL_CPPFLAGS += -std=c++11

SRC_FILES = \
	$(wildcard ../Nongl/libktx/*.c) \
	$(wildcard ../Nongl/MiniZ/*.c) \
	$(wildcard ../Nongl/PicoPng/*.cpp) \
	$(wildcard ../Nongl/*.cpp) \
	$(wildcard ../src/*.cpp) \

#Append ../ to all files. It seems that "LOCAL_PATH := $(call my-dir)" forces compiler to work inside "jni", while
#the make utility resides in the project root folder. 
TEMP = $(SRC_FILES:%.cpp=../%.cpp) 
SOURCES = $(TEMP:%.c=../%.c)

LOCAL_SRC_FILES := $(SOURCES) 
      
LOCAL_LDLIBS := -lGLESv2 -ldl -llog

include $(BUILD_SHARED_LIBRARY)
