package com.nomone.nongl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewGroup;

public class NonglActivity extends Activity {
	
	private int layoutId, rootLayoutId;
	
	static final String RENDERER_PREFERENCE = "Renderer name";
	
	private String rendererName;
	private GLSurfaceView glView;
	private boolean renderContinuously = true;

	private boolean fullColor = false;
	private int depthBufferBits = 16;
	private int samplesCount = 0; 
	
	private NONGLConfigChooser nonglConfigChooser;
	
	private boolean paused = false;
	
	public NonglActivity(int layoutId, int rootLayoutId) {
		this.layoutId = layoutId;
		this.rootLayoutId = rootLayoutId;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remove window title,
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        //		WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Common.context = getApplicationContext(); 
        
        setContentView(layoutId);
        
        // Create and add WebView to the layout,
    	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        setupGlSurfaceView(sharedPreferences.getString(RENDERER_PREFERENCE, null));
    }
    
    private void setupGlSurfaceView(String rendererName) {
    	
    	boolean justGetRenderer;
    	if (rendererName == null) {
    		justGetRenderer = true;
    	} else {
    		justGetRenderer = false;

    		Utils.logW("Renderer: " + rendererName);
			
    		// If mali400 or adreno, enable MSAA 4x,
    		if (rendererName.contains("Mali-400")) {
    			if (samplesCount < 4) {
    				Utils.logW("Enabled MSAA 4x because it wouldn't hurt!");
    				samplesCount = 4;
    			}
    		}	
    	}
    	
        glView = new SuperGLSurfaceView(this);
        glView.setEGLContextClientVersion(2);         
        glView.setEGLConfigChooser(nonglConfigChooser = new NONGLConfigChooser(fullColor, depthBufferBits, samplesCount));
        //glView.setEGLConfigChooser(false); // Disable z-buffer.
        glView.setRenderer(new NonglRenderer(justGetRenderer));
        glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
         
        ViewGroup rootLayout = (ViewGroup) findViewById(rootLayoutId);
        rootLayout.addView(glView, 0);    	
    }
    
    public void setRenderModeContinuously(boolean renderContinously) {
    	this.renderContinuously = renderContinously;
    	if (renderContinously) glView.requestRender();
    }
    
    public boolean usesCoverageAa() {
    	
        if (nonglConfigChooser.usesCoverageAa())
            return true;
        
        return false;
    }
    
    @Override
    protected void onResume() {
    
		synchronized (NONGLNatives.nativeSyncLock) {
			Utils.logE("resume");
	    	paused = false;
	    	Common.currentActivity = this;
	    	super.onResume();
	    	NONGLNatives.nativeOnResume();
	    	
    		glView.requestRender();
		}
    }
    
    @Override
    protected void onPause() {
		synchronized (NONGLNatives.nativeSyncLock) {
			Utils.logE("pause");
	    	paused = true;
	    	NONGLNatives.nativeOnPause();
	    	Common.currentActivity = null;
	    	super.onPause();
		}
    }
    
    @Override
    public void onBackPressed() {
    	NONGLNatives.nativeOnBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			NONGLNatives.nativeOnMenuPressed();
		} else if ((keyCode == KeyEvent.KEYCODE_BACK) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_UP) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_MUTE)) {
			return super.onKeyDown(keyCode, event);
    	} else {
        	// TODO: how about non-unicode keys?
			NONGLNatives.nativeOnKeyDown(event.getUnicodeChar());
    		//android.util.Log.e("Key", "" + keyCode);
    	}
		
		return true;
    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

    	if ((keyCode == KeyEvent.KEYCODE_MENU) || (keyCode == KeyEvent.KEYCODE_BACK)) {
			return super.onKeyUp(keyCode, event);
    	} else {
			NONGLNatives.nativeOnKeyUp(event.getUnicodeChar());
    	}
	
		return true;
    }

    public void onActivityFinished() {}

	class SuperGLSurfaceView extends GLSurfaceView {

		/** Multi-touch hassle. */
		static final int ACTION_DOWN = 0;
		static final int ACTION_DRAG = 1;
		static final int ACTION_UP   = 2;

		/** Maximum number of pointers (fingers) are supported. */
		private static final int MAX_POINTERS_COUNT = 20;

		/** Used to determine which finger is associated with the touch move events. */ 
		private PointerState[] pointersState;

		private long lastDragTime;
		private static final long MIN_INTER_DRAG_TIME_MILLIS = 16;
		
		public SuperGLSurfaceView(Context context) {
			super(context);
		}
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			
			if (pointersState == null) {
				pointersState = new PointerState[MAX_POINTERS_COUNT];
				// Initialize pointers state data structures, 
				for (int i=0; i<pointersState.length; i++) {
					pointersState[i] = new PointerState();
				}
			}
			
			int action = event.getAction();
			
			// Limit dispateched events,
			long currentTime = System.currentTimeMillis();			
			if ((action & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_MOVE) {
				if ((currentTime - lastDragTime) < MIN_INTER_DRAG_TIME_MILLIS) {
					return true;				
				} else {
					lastDragTime = currentTime;
				} 
			}
			
			// Since Android 2.0, a new set of action were introduced, like ACTION_POINTER_DOWN and
			// ACTION_POINTER_UP. These actions has parameters encoded inside the action value itself.
			// So, in order to be able to identify the action regardless of its parameters, we have
			// to use a mask,
			switch (action & MotionEvent.ACTION_MASK) {
			
			case MotionEvent.ACTION_DOWN: {

				// An action of this type is generated only if one pointer is pressed. When
				// more than one pointer is pressed, ACTION_POINTER_DOWN is generated instead, 
				
				// The ID of this pointer,
				int pointerId = event.getPointerId(0);
				
				action = ACTION_DOWN;
				float x = event.getX();
				float y = event.getY();
				normalizeAndReportTouch(action, pointerId, x, y);
				
				break;
			}

			case MotionEvent.ACTION_MOVE: {

				action = ACTION_DRAG;
				
				// An action of this type is generated if any number of pointers is pressed
				// (including one pointer only). Unfortunately, there is no way that I know
				// of to directly determine the specific pointer which generated this event,
				// so we need some sort of workaround,

				// We are going to check all the currently down pointers for the ones whose
				// positions were changed,
				int numPointers = event.getPointerCount();
				for (int i = 0; i < numPointers; i++) {

					float x = event.getX(i);
					float y = event.getY(i);

					// The ID of this pointer,
					int pointerId = event.getPointerId(i);

					// Discard pointers above the max allowed,
					if (pointerId >= MAX_POINTERS_COUNT)
						continue;
					
					// If this is the pointer that moved,
					if ((pointersState[pointerId].x != x) ||
						(pointersState[pointerId].y != y)) {
						
						normalizeAndReportTouch(action, pointerId, x, y);
					}
				}

				break;
			}

			case MotionEvent.ACTION_UP: {

				// An action of this type is generated only if one pointer is pressed. When
				// more than one pointer is pressed, ACTION_POINTER_UP is generated instead, 

				// The ID of this pointer,
				int pointerId = event.getPointerId(0);
				
				action = ACTION_UP;			
				float x = event.getX();
				float y = event.getY();
				normalizeAndReportTouch(action, pointerId, x, y);
				
				break;
			}

			case MotionEvent.ACTION_POINTER_DOWN: {

				// Extract the index of the pointer,
				final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				final int pointerId = event.getPointerId(pointerIndex);
				
				action = ACTION_DOWN;
				float x = event.getX(pointerIndex);
				float y = event.getY(pointerIndex);
				normalizeAndReportTouch(action, pointerId, x, y);
				break;
			}

			case MotionEvent.ACTION_POINTER_UP: {

				// Extract the index of the pointer,
				final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				final int pointerId = event.getPointerId(pointerIndex);
				
				action = ACTION_UP;
				float x = event.getX(pointerIndex);
				float y = event.getY(pointerIndex);
				normalizeAndReportTouch(action, pointerId, x, y);
				break;
			}
			
			}

			// Mark this event as consumed, no further processing by the system is required,
			return true;
		}

		private void normalizeAndReportTouch(int action, int pointerId, float x, float y) {
			
			// Normalize touch,
			int width = getWidth();
			int height = getHeight();
			x /= width;
			y = (height - y) / height;

			// Report touch to native,
			switch (action) {
			case ACTION_DOWN:
				NONGLNatives.nativeOnTouchDown(pointerId, x, y);
				break;
				
			case ACTION_DRAG:
				NONGLNatives.nativeOnTouchDrag(pointerId, x, y);
				break;
				
			case ACTION_UP:
				NONGLNatives.nativeOnTouchUp(pointerId, x, y);
				break;

			default:
				break;
			}
		}
		
		/** Some data about a pointer (finger). */ 
		class PointerState {
			float x, y;
		}
	}
	
    class NonglRenderer implements Renderer {

		private boolean justGetRenderer;
		NonglRenderer(boolean justGetRenderer) {
			this.justGetRenderer = justGetRenderer;
		}

    	
		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			
			if (justGetRenderer) {
				rendererName = gl.glGetString(GL10.GL_RENDERER);
				
				Utils.logW("vendor: " + gl.glGetString(GL10.GL_VENDOR));
				Utils.logW("version: " + gl.glGetString(GL10.GL_VERSION));
				 
				runOnUiThread(new Runnable() {
					@Override 
					public void run() {
						ViewGroup rootLayout = (ViewGroup) findViewById(rootLayoutId);
				        rootLayout.removeView(glView);
				        
			        	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(NonglActivity.this);
			    		Editor preferencesEditor = sharedPreferences.edit(); 
			    		preferencesEditor.putString(RENDERER_PREFERENCE, rendererName);
			    		preferencesEditor.commit();

				        setupGlSurfaceView(rendererName);
					}
				});		        
		        return ;
			}
				
			/*
		     String s = GLES20.glGetString(GLES20.GL_EXTENSIONS);
		     if (s.contains("GL_COMPRESSED_RGBA8_ETC2_EAC") ||
	    		 s.contains("GL_OES_compressed_ETC2_RGBA8_texture")) {
		    	 Log.e("sddsf", "found!");
		     } else {
		    	 Log.e("sddsf", s);
		     }
		    */
			
			// Notify native side to reload textures,
			NONGLNatives.nativeOnSurfaceCreated();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			if (justGetRenderer) 
				return;

			NONGLNatives.nativeOnSurfaceChanged(width, height);
		}

		@Override
		public void onDrawFrame(GL10 gl) {

			if (justGetRenderer) 
				return;

			synchronized (NONGLNatives.nativeSyncLock) {

				if (paused)
					return;

				NONGLNatives.nativeOnDrawFrame();
				
				if (renderContinuously) glView.requestRender();
			}
		}

    }
}
