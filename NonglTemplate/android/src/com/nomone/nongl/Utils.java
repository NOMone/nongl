package com.nomone.nongl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;

public class Utils {

	public static int nextPot(int value) {
		
		if ((value & (value - 1)) == 0)
				return value;
		
		for (int i=0; i<31; i++) {
			if ((1<<i) >= value)
				return 1<<i;
		}
		return 0;
	}
	
	public static boolean isApplicationIstalledByPackageName(Context context, String packageName) {
		
		List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);
		if (packages != null && packageName != null) {
			for (PackageInfo packageInfo : packages) {
				if (packageName.equals(packageInfo.packageName)) {
					return true;
				}
			}
		}
		
		return false;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		return (cm.getActiveNetworkInfo() != null) &&
			    cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}
	
	public static void copyTextToClipboard(Context context, String textToCopy) {

		/*
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
		    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		    clipboard.setText(MARKET_URL);
		} else {
		    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", MARKET_URL);
		    clipboard.setPrimaryClip(clip);
		}
		*/

		// Use reflection to avoid verification errors,
		if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			
			try {
				// Get clipboard manager,
				Object clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE);
				
				// Invoke setText on clipboardManager,
				Method setTextMethod = clipboardManager.getClass().getMethod("setText", new Class[] {CharSequence.class});
				setTextMethod.invoke(clipboardManager, textToCopy);
			} catch (Exception e) {}
			
		} else {
			
			try {
				// Get clipboard manager,
				Object clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE);

				// Invoke newPlaintText on ClipData and get a clipData instance, 
				Class<?> clipDataClass = Class.forName("android.content.ClipData");
				Method newPlaintTextMethod = clipDataClass.getMethod("newPlainText", new Class[] {CharSequence.class, CharSequence.class}); 
				Object clipData = newPlaintTextMethod.invoke(null, "Copied Text", textToCopy);
				
				// Invoke setPrimaryClip on clipboardManager,
				Method setPrimaryClipMethod = clipboardManager.getClass().getMethod("setPrimaryClip", new Class[] {clipDataClass});
				setPrimaryClipMethod.invoke(clipboardManager, clipData);
			} catch (Exception e) {}
		}
	}
	
	public static void setBackgroundDrawable(View view, Drawable drawable) {
	
		/*
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			viewToDelete.setBackground(null);								
		} else {
			viewToDelete.setBackgroundDrawable(null);
		}
		*/
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			try {
				Method setBackgroundMethod = view.getClass().getMethod("setBackground", new Class[] {Drawable.class});
				setBackgroundMethod.invoke(view, drawable);
			} catch (Exception e) {}
		} else {
			try {
				Method setBackgroundDrawableMethod = view.getClass().getMethod("setBackgroundDrawable", new Class[] {Drawable.class});
				setBackgroundDrawableMethod.invoke(view, drawable);
			} catch (Exception e) {}
		}
	}

	private static BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
	public static void getImageBounds(String imagePath, Point outDimensions) {
		
		synchronized (bitmapOptions) {
			bitmapOptions.inJustDecodeBounds = true;
			bitmapOptions.outWidth = bitmapOptions.outHeight = 0;
			try {
				BitmapFactory.decodeFile(imagePath, bitmapOptions);
			} catch (Exception e) {}
			outDimensions.x = bitmapOptions.outWidth;
			outDimensions.y = bitmapOptions.outHeight;	
		}
	}

	public static void getImageBoundsFromAssets(Context context, String imagePath, Point outDimensions) {
		
		synchronized (bitmapOptions) {
			bitmapOptions.inJustDecodeBounds = true;
			bitmapOptions.outWidth = bitmapOptions.outHeight = 0;
			
			InputStream inputStream=null;
			try {
				inputStream = context.getAssets().open(imagePath);
				BitmapFactory.decodeStream(inputStream, null, bitmapOptions);
			} catch (Exception e) {
			} finally {
	            try {
	            	if (inputStream != null) {
	            		inputStream.close();
	            	}
	            } catch (IOException e) {}
	        }				
			
			outDimensions.x = bitmapOptions.outWidth;
			outDimensions.y = bitmapOptions.outHeight;	
		}
	}

	public static Bitmap loadClosestBitmap(String imagePath, Point requiredDimensions) {
		return loadClosestBitmap(imagePath, null, requiredDimensions);
	}
	
	private static Point actualDimensions = new Point();
	private static BitmapFactory.Options bitmapOptions2 = new BitmapFactory.Options();
	private static final int MAX_BITMAP_SIZE_BYTES = 4200000; 
	public static Bitmap loadClosestBitmap(String imagePath, Point originalDimensions, Point requiredDimensions) {
		
		// Get actual dimensions,
		int actualWidth, actualHeight;
		if (originalDimensions == null) {
			synchronized (actualDimensions) {
				getImageBounds(imagePath, actualDimensions);
				actualWidth = actualDimensions.x;
				actualHeight = actualDimensions.y;
			}
		} else {
			actualWidth = originalDimensions.x;
			actualHeight = originalDimensions.y;
		}
		
		// Calculate sampleSize,
		int xSampleSize = actualWidth  / requiredDimensions.x;
		int ySampleSize = actualHeight / requiredDimensions.y;
		int sampleSize = (xSampleSize < ySampleSize) ? xSampleSize : ySampleSize;

		// Put upper limit for image size,
		if (sampleSize > 0) {
			while ((actualWidth / sampleSize) * (actualHeight / sampleSize) * 4 > MAX_BITMAP_SIZE_BYTES) {
				sampleSize ++;
			}
		}
		
		// Load bitmap using sample size,
		bitmapOptions2.inScaled = false;
		bitmapOptions2.inSampleSize = sampleSize; 
		Bitmap loadedBitmap = BitmapFactory.decodeFile(imagePath, bitmapOptions2);
	
		return loadedBitmap;
	}
	
	private static Rect srcRect = new Rect();
	private static Rect destRect = new Rect();
	private static Paint paint = new Paint();
	private static Canvas canvas = new Canvas();
	public static Bitmap loadScaledBitmap(String imagePath, Point requiredDimensions) {
		
		Bitmap loadedBitmap = loadClosestBitmap(imagePath, requiredDimensions);
		
		if ((loadedBitmap.getWidth() == requiredDimensions.x) &&
			(loadedBitmap.getHeight() == requiredDimensions.y)) {
			
			return loadedBitmap;
			 
		} else {
				
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(loadedBitmap, requiredDimensions.x, requiredDimensions.y, true);
			if (scaledBitmap == null) {
				
				scaledBitmap = Bitmap.createBitmap(requiredDimensions.x, requiredDimensions.y, Bitmap.Config.ARGB_8888);
				synchronized (canvas) {
					canvas.setBitmap(scaledBitmap); 
					srcRect.set(0, 0, loadedBitmap.getWidth(), loadedBitmap.getHeight());
					destRect.set(0, 0, requiredDimensions.x, requiredDimensions.y);
					paint.setFilterBitmap(true);
					paint.setDither(true);
					canvas.drawBitmap(loadedBitmap, srcRect, destRect, paint);
				}
			}
			loadedBitmap.recycle();
			return scaledBitmap;
		} 
	}

	private static Rect srcRect2 = new Rect();
	private static Rect destRect2 = new Rect();
	private static Paint paint2 = new Paint();
	private static Canvas canvas2 = new Canvas();
	public static Bitmap createBitmapBestFitPadding(Bitmap srcBitmap, Point requiredDimensions, Drawable background) {

		// Check if nothing needs to be done,
		int srcBitmapWidth = srcBitmap.getWidth();
		int srcBitmapHeight = srcBitmap.getHeight();
		if ((srcBitmapWidth == requiredDimensions.x) &&
			(srcBitmapHeight == requiredDimensions.y)) {
			return srcBitmap;
		}

		// Calculate best fit with padding,
		float scaleX = requiredDimensions.x / (float) srcBitmapWidth;
		float scaleY = requiredDimensions.y / (float) srcBitmapHeight;
		float scale = (scaleX < scaleY) ? scaleX : scaleY;
		
		int scaledWidth = (int) (srcBitmapWidth * scale);
		int scaledHeight = (int) (srcBitmapHeight * scale);
		
		// Create padded bitmap,
		Bitmap paddedBitmap = Bitmap.createBitmap(requiredDimensions.x, requiredDimensions.y, Bitmap.Config.ARGB_8888);
		synchronized (canvas2) {
			
			canvas2.setBitmap(paddedBitmap);
			
			// Draw background,
			background.setBounds(0, 0, requiredDimensions.x, requiredDimensions.y);
			background.draw(canvas2);
			
			// Draw image,
			srcRect2.set(0, 0, srcBitmap.getWidth(), srcBitmap.getHeight());
			destRect2.set(
					(int) ((requiredDimensions.x - scaledWidth) * 0.5f),
					(int) ((requiredDimensions.y - scaledHeight) * 0.5f),
					(int) ((requiredDimensions.x + scaledWidth) * 0.5f),
					(int) ((requiredDimensions.y + scaledHeight) * 0.5f));
			paint2.setFilterBitmap(true);
			paint2.setDither(true);
			canvas2.drawBitmap(srcBitmap, srcRect2, destRect2, paint2);
		}
		
		return paddedBitmap;
	}

	private static Point originalDimensions = new Point();
	private static Point scaleDimensions = new Point();
	public static Bitmap loadBitmapBestFitPadding(String imagePath, Point requiredDimensions, Drawable background) {
		
		// Load the closest down-sampled version of the image,
		Bitmap closestBitmap;
		synchronized (originalDimensions) {
			
			// Get image dimensions,
			getImageBounds(imagePath, originalDimensions);
			int originalWidth = originalDimensions.x;
			int originalHeight = originalDimensions.y;
		
			// Calculate best fit with padding,
			float scaleX = requiredDimensions.x / (float) originalWidth;
			float scaleY = requiredDimensions.y / (float) originalHeight;
			float scale = (scaleX < scaleY) ? scaleX : scaleY;
			
			int scaledWidth = (int) (originalWidth * scale);
			int scaledHeight = (int) (originalHeight * scale);
			
			// load scaled image,
			synchronized (scaleDimensions) {
				scaleDimensions.x = scaledWidth;
				scaleDimensions.y = scaledHeight;
				closestBitmap = loadClosestBitmap(imagePath, originalDimensions, scaleDimensions);
			}
		}
		
		// Pad,
		Bitmap paddedBitmap = createBitmapBestFitPadding(closestBitmap, requiredDimensions, background);
		if (paddedBitmap != closestBitmap) {
			closestBitmap.recycle();
		}
		return paddedBitmap;
	}

	private static Rect srcRect3 = new Rect();
	private static Rect destRect3 = new Rect();
	private static Paint paint3 = new Paint();
	private static Canvas canvas3 = new Canvas();
	public static Bitmap createBitmapBestFitCropping(Bitmap srcBitmap, Point requiredDimensions) {

		// Check if nothing needs to be done,
		int srcBitmapWidth = srcBitmap.getWidth();
		int srcBitmapHeight = srcBitmap.getHeight();
		if ((srcBitmapWidth == requiredDimensions.x) &&
			(srcBitmapHeight == requiredDimensions.y)) {
			return srcBitmap;
		}
		
		// Calculate best fit cropping,
		float scaleX = requiredDimensions.x / (float) srcBitmapWidth;
		float scaleY = requiredDimensions.y / (float) srcBitmapHeight;
		float scale = (scaleX > scaleY) ? scaleX : scaleY;
			
		scaleX /= scale;
		scaleY /= scale;

		int srcWidth = (int) (srcBitmapWidth * scaleX);
		int srcHeight = (int) (srcBitmapHeight * scaleY);

		// Create cropped bitmap,
		Bitmap croppedBitmap = Bitmap.createBitmap(requiredDimensions.x, requiredDimensions.y, Bitmap.Config.ARGB_8888);
		synchronized (canvas3) {
			canvas3.setBitmap(croppedBitmap);
			
			// Draw image,
			srcRect3.set(
					(int) ((srcBitmapWidth - srcWidth) * 0.5f),
					(int) ((srcBitmapHeight - srcHeight) * 0.5f),
					(int) ((srcBitmapWidth + srcWidth) * 0.5f),
					(int) ((srcBitmapHeight + srcHeight) * 0.5f));
			destRect3.set(0, 0, requiredDimensions.x, requiredDimensions.y);
			paint3.setFilterBitmap(true);
			paint3.setDither(true);
			canvas3.drawBitmap(srcBitmap, srcRect3, destRect3, paint3);
		}
		
		return croppedBitmap;
	}

	private static Point originalDimensions2 = new Point();
	private static Point scaleDimensions2 = new Point();
	public static Bitmap loadBitmapBestFitCropping(String imagePath, Point requiredDimensions) {
		
		// Load the closest down-sampled version of the image,
		Bitmap closestBitmap;
		synchronized (originalDimensions2) {
			
			getImageBounds(imagePath, originalDimensions2);
			int originalWidth = originalDimensions2.x;
			int originalHeight = originalDimensions2.y;
		
			// Calculate best fit resizing,
			float scaleX = requiredDimensions.x / (float) originalWidth;
			float scaleY = requiredDimensions.y / (float) originalHeight;
			float scale = (scaleX > scaleY) ? scaleX : scaleY;
			
			int scaledWidth = (int) (originalWidth * scale);
			int scaledHeight = (int) (originalHeight * scale);
			
			// load scaled image,
			synchronized (scaleDimensions2) {
				scaleDimensions2.x = scaledWidth;
				scaleDimensions2.y = scaledHeight;
				closestBitmap = loadClosestBitmap(imagePath, originalDimensions2, scaleDimensions2);
			}
		}
		
		// Crop the image,
		Bitmap croppedBitmap = createBitmapBestFitCropping(closestBitmap, requiredDimensions);
		if (croppedBitmap != closestBitmap) {
			closestBitmap.recycle();
		}
		
		return croppedBitmap;
	}

	public static void previewImage(Context context, String filename) {
		
		final Context finalContext = context; 
		
		MediaScannerConnection.OnScanCompletedListener onScanCompletedListener;
		onScanCompletedListener = new MediaScannerConnection.OnScanCompletedListener() {
			
			@Override
			public void onScanCompleted(String path, Uri uri) {
				
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(uri, "image/*");
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				finalContext.startActivity(intent);
			}
		};
		
		File f = new File(filename);
		if (f.isFile()) {
			MediaScannerConnection.scanFile(
					context,
					new String[] { f.toString() },
					null,
					onScanCompletedListener);
		}						

		/*
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://" + imagePaths[finalImageIndex]), "image/*");
		startActivity(intent);
		*/
	}
	
	public static Bitmap loadBitmapFromAssets(Context context, String filename) {
		
		Bitmap bitmap = null;		
		InputStream inputStream=null;
		
		try {
			inputStream = context.getAssets().open(filename);
			
			/*
			// We need to flip the textures vertically:
		    Matrix flip = new Matrix();
		    flip.postScale(1f, -1f);
		    
		    // This will tell the BitmapFactory to not scale based on the device's pixel density,
		    BitmapFactory.Options opts = new BitmapFactory.Options();
		    opts.inScaled = false;
		    
		    // Load up, and flip the texture:
		    Bitmap temp = BitmapFactory.decodeStream(inputStream, null, opts);
			bitmap = Bitmap.createBitmap(temp, 0, 0, temp.getWidth(), temp.getHeight(), flip, true);
		    temp.recycle();
		    */
			
			bitmap = BitmapFactory.decodeStream(inputStream);
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            try {
            	if (inputStream != null) {
            		inputStream.close();
            	}
            } catch (IOException e) {
                // Ignore.
            }
        }				
		
        return bitmap;
	}

	private static String adjustRawFileName(String fileName) {
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
		fileName = fileName.replace('/', '_');
		return fileName.toLowerCase();
	}

	public static byte[] loadFileFromRaw(Context context, String fileName) {

		fileName = adjustRawFileName(fileName);		
		Resources resources = context.getResources(); 
		int resourceId = resources.getIdentifier(fileName, "raw", context.getPackageName());
		
		if (resourceId == 0) {
			return null;
		}

		byte[] fileContents = null;		
		InputStream inputStream=null;
		ByteArrayOutputStream baos=null;
		
		try {
			
			inputStream = resources.openRawResource(resourceId);
			
		    baos = new ByteArrayOutputStream(inputStream.available() + 16);
		    byte[] buffer = new byte[65536];
		    int length; 
		    while ((length = inputStream.read(buffer)) != -1) {
		        baos.write(buffer, 0, length);
		    }
		    fileContents = baos.toByteArray();
		    
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (baos != null) 
				try { baos.close(); } catch (IOException e) {}
		    
			if (inputStream != null) 
				try { inputStream.close(); } catch (IOException e) {}
        }				
		
        return fileContents;
	}
	
	public static boolean rawFileExists(Context context, String fileName) {

		fileName = adjustRawFileName(fileName);		
		int resourceId = context.getResources().getIdentifier(fileName, "raw", context.getPackageName());
        return resourceId != 0;
	}
	
	public static byte[] loadFileFromAssets(Context context, String filename) {
		
		byte[] fileContents = null;		
		InputStream inputStream=null;
		ByteArrayOutputStream baos=null;
		
		try {
			
			inputStream = context.getAssets().open(filename);
			
		    baos = new ByteArrayOutputStream(inputStream.available() + 16);
		    byte[] buffer = new byte[65536];
		    int length; 
		    while ((length = inputStream.read(buffer)) != -1) {
		        baos.write(buffer, 0, length);
		    }
		    fileContents = baos.toByteArray();
		    
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (baos != null) 
				try { baos.close(); } catch (IOException e) {}
		    
			if (inputStream != null) 
				try { inputStream.close(); } catch (IOException e) {}
        }				
		
        return fileContents;
	}

	public static boolean assetsFileExists(Context context, String filename) {
		
		boolean exists = true;
		InputStream inputStream=null;
		try {
			inputStream = context.getAssets().open(filename);
		} catch (Exception e) {
			exists = false;
		} finally {		    
			if (inputStream != null) 
				try { inputStream.close(); } catch (IOException e) {}
        }				
		
        return exists;
	}

	public static File getApplicationFile(Context context, String filename) {
		
		String extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
		File file = new File(extStorageDirectory + "/Android/data/" + context.getPackageName() + "/" + filename);
		
		return file;
	}

	public static byte[] loadApplicationFile(Context context, String filename) {
		
		byte[] fileContents = null;		
		InputStream inputStream=null;
		ByteArrayOutputStream baos=null;
		
		try {
			File file = getApplicationFile(context, filename);
			inputStream = new FileInputStream(file);

		    baos = new ByteArrayOutputStream();
		    byte[] buffer = new byte[1024];
		    int length;
		    while ((length = inputStream.read(buffer)) != -1) {
		        baos.write(buffer, 0, length);
		    }
		    
		    fileContents = baos.toByteArray();
		    
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (baos != null) 
				try { baos.close(); } catch (IOException e) {}
		    
			if (inputStream != null) 
				try { inputStream.close(); } catch (IOException e) {}
        }				
		
        return fileContents;
	}

	public static File createApplicationFile(Context context, String filename) {
		
		String extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
		File file = new File(extStorageDirectory + "/Android/data/" + context.getPackageName() + "/" + filename);
		File parentDirectory = file.getParentFile();
		if (parentDirectory != null)
			parentDirectory.mkdirs();
		
		return file;
	}

	/** Creates a file that should be automatically removed on application uninstall. */
	public static boolean createApplicationFile(Context context, String filename, String data) {
		
		File file = createApplicationFile(context, filename);
		
        PrintStream printStream = null;
        boolean success = true;
        try {
        	printStream = new PrintStream(file); 
        	printStream.print(data);
        	printStream.flush();        	
        } catch(Exception e) {
        	success = false;
        	Utils.logE("File creation error: " + filename);
            e.printStackTrace();
        } finally {
        	if (printStream != null) {
        		try { printStream.close(); } catch (Exception e) {}        		
        	}
        }		
        
        return success;
	}

	/** Creates a file that should be automatically removed on application uninstall. */
	public static boolean createApplicationFile(Context context, String filename, byte[] data) {
		
		File file = createApplicationFile(context, filename);

    	FileOutputStream fileOutputStream = null;
        boolean success = true;
        try {
        	fileOutputStream = new FileOutputStream(file);
        	fileOutputStream.write(data);
        	fileOutputStream.flush();
        } catch(Exception e) {
        	success = false;
        	Utils.logE("File creation error: " + filename);
            e.printStackTrace();
        } finally {
        	if (fileOutputStream != null) {
        		try { fileOutputStream.close(); } catch (Exception e) {}        		
        	}
        }		
        
        return success;
	}

	public static boolean applicationFileExists(Context context, String filename) {
		File file = getApplicationFile(context, filename);
		return file.exists();
	}

	public static void browseUrl(Context context, String url, String alternativeUrl) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		
		try {
			context.startActivity(intent);
		} catch (Exception e) {
			if (alternativeUrl != null) {
				intent.setData(Uri.parse(alternativeUrl));
				try { context.startActivity(intent); } catch (Exception ex) {}
			}
		}
	}

	private static final boolean VERBOSE = true;
	private static final String LOG_TAG = "Nongl";
	public static void logW(String message) { if (VERBOSE) Log.w(LOG_TAG, message); }
	public static void logE(String message) { if (VERBOSE) Log.e(LOG_TAG, message); }
}
