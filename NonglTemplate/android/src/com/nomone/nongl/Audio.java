package com.nomone.nongl;


public abstract class Audio {

	public abstract void play();
	public abstract void stop();
	public abstract void pause();
	public abstract void resume();
	public abstract void setLooping(boolean loopingEnabled);
	public abstract boolean isPlaying() ;	
}
