package com.nomone.nongl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.opengl.ETC1Util;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.nomone.nongl.SocketsClient.ByteArrayBuffer;

public class NONGLNatives {

	static {
		System.loadLibrary("stlport_shared");
		System.loadLibrary("natives");
	}

	public static Object nativeSyncLock = new Object();
			
	public static boolean javaUsesCoverageAa() {
		
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null)
			return activity.usesCoverageAa();
		
		return false;
	}

	public static boolean javaLoadCompressedTextureMipMapsFromAssets(int textureId, String filename, String suffix) {
		
		int mipMapLevel = 0;
		filename += "_mip_";
		String currentFileName = filename + mipMapLevel + suffix;
		while (Utils.assetsFileExists(Common.context, currentFileName + ".pkm")) {
		
			if (!javaLoadCompressedTextureFromAssets(textureId, currentFileName, mipMapLevel))
				return false;
			
			mipMapLevel++;
			currentFileName = filename + mipMapLevel + suffix;
		}
			
		return true;
	}

	public static boolean javaLoadCompressedTextureFromAssets(int textureId, String filename, int mipMapLevel) {
		
		filename += ".pkm";
		
		InputStream inputStream=null;
		boolean success = true;
		
		try {
			inputStream = Common.context.getAssets().open(filename);
			ETC1Util.loadTexture(GLES20.GL_TEXTURE_2D, mipMapLevel, 0, GLES20.GL_RGB, GLES20.GL_UNSIGNED_BYTE, inputStream);

			int error = GLES20.glGetError(); 
			while (error != 0) {
				Utils.logE("Gl error (before or at) loading ETC1 texture: " + error);
				error = GLES20.glGetError();
			}
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		} finally {
            try {
            	if (inputStream != null) {
            		inputStream.close();
            	}
            } catch (IOException e) {
                // Ignore.
            }
        }				
				
		return success;
		/*
		GLES20.glCompressedTexImage2D(
				, 
				0, 
				GLES20.GL_COMPRESSED_RGB, width, height, border, imageSize, data)
		*/
	}
	
	private static final int TEXTURE_FILTER_NEAREST = 0;
	private static final int TEXTURE_FILTER_LINEAR = 1;
	private static final int TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST = 2;
	private static final int TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR = 3;
	private static final int TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST = 4;
	private static final int TEXTURE_FILTER_LINEAR_MIPMAP_LINEAR = 5;

	public static int javaLoadTextureFromAssets(int textureId, String filename, String suffix, int minTextureFilterType, int magTextureFilterType) {
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);

		// TODO: remove and do in native?
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE); //GL_REPEAT);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE); //GL_REPEAT);

		// Select the correct texture filters,
		boolean generateMipMaps = false;

		switch (minTextureFilterType) {
		case TEXTURE_FILTER_NEAREST: minTextureFilterType = GLES20.GL_NEAREST; break;
		case TEXTURE_FILTER_LINEAR: minTextureFilterType = GLES20.GL_LINEAR; break;
		case TEXTURE_FILTER_NEAREST_MIPMAP_NEAREST: minTextureFilterType = GLES20.GL_NEAREST_MIPMAP_NEAREST; generateMipMaps = true; break;
		case TEXTURE_FILTER_NEAREST_MIPMAP_LINEAR: minTextureFilterType = GLES20.GL_NEAREST_MIPMAP_LINEAR; generateMipMaps = true; break;
		case TEXTURE_FILTER_LINEAR_MIPMAP_NEAREST: minTextureFilterType = GLES20.GL_LINEAR_MIPMAP_NEAREST; generateMipMaps = true; break;
		case TEXTURE_FILTER_LINEAR_MIPMAP_LINEAR: minTextureFilterType = GLES20.GL_LINEAR_MIPMAP_LINEAR; generateMipMaps = true; break;
		default: break;
		}
		
		switch (magTextureFilterType) {
		case TEXTURE_FILTER_NEAREST: magTextureFilterType = GLES20.GL_NEAREST; break;
		case TEXTURE_FILTER_LINEAR: magTextureFilterType = GLES20.GL_LINEAR; break;
		default: break;
		}

		// Apply texture filter,
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, minTextureFilterType);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, magTextureFilterType);

		// Check and load ".pkm" texture instead if possible,
		String extensionLessFileName = filename.substring(0, filename.length() - 4);
		String fileNameExtension = filename.substring(filename.length() - 4);

		if (ETC1Util.isETC1Supported()) {
			if (generateMipMaps) {
			 	if (Utils.assetsFileExists(Common.context, extensionLessFileName + "_mip_0" + suffix + ".pkm") &&
		 		    javaLoadCompressedTextureMipMapsFromAssets(textureId, extensionLessFileName, suffix)) {
			 		return 1;
			 	}
			} else {
			 	if (Utils.assetsFileExists(Common.context, extensionLessFileName + suffix + ".pkm")) {
			 		if (javaLoadCompressedTextureFromAssets(textureId, extensionLessFileName + suffix, 0))
			 			return 1;
			 	} else if (Utils.assetsFileExists(Common.context, extensionLessFileName + "_mip_0" + suffix + ".pkm")) {
			 		if (javaLoadCompressedTextureFromAssets(textureId, extensionLessFileName + "_mip_0" + suffix, 0))
			 			return 1;
			 	}
 			}
		} 

		// Load uncompressed texture,
		Bitmap textureBitmap = null;
		try {
    		textureBitmap = Utils.loadBitmapFromAssets(Common.context, extensionLessFileName + suffix + fileNameExtension);
		} catch (Throwable e) {
			e.printStackTrace();			
		}
	
		if (textureBitmap != null) {
			// Report and discard all previous errors,
			int error = GLES20.glGetError(); 
			while (error != 0) {
				Utils.logE("Gl error before texImage2D: " + error);
				error = GLES20.glGetError();
			}

			// Transfer the texture,
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textureBitmap, 0);
			textureBitmap.recycle();
			if (GLES20.glGetError() != 0) {
				return 0;
			}
	
			// Generate mip maps,
			if (generateMipMaps)
				GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
			
			return 1; 
	    } else {
	    	Utils.logE("Couldn't load texture " + filename);
	    }
		
		return 0;
	}

	public static int javaGetTextureDimensionsFromAssets(String filename) {
		
		Point dimensions = new Point();
		Utils.getImageBoundsFromAssets(Common.context, filename, dimensions);
		
		int packedDimensions = dimensions.x | (dimensions.y << 16);
		return packedDimensions;
	}

	public static byte[] javaLoadFileFromRaw(String filename) {
		return Utils.loadFileFromRaw(Common.context, filename);
	}

	public static byte[] javaLoadFileFromAssets(String filename) {
		return Utils.loadFileFromAssets(Common.context, filename);
	}

	public static byte[] javaLoadApplicationFile(String filename) {
		return Utils.loadApplicationFile(Common.context, filename);
	}
	
	public static String javaLoadTextFileFromAssets(String filename) {
		byte[] bytes = Utils.loadFileFromAssets(Common.context, filename);
		if (bytes != null)
			return new String(bytes);
		return null;
	}

	public static String javaLoadTextApplicationFile(String filename) {
		byte[] bytes = Utils.loadApplicationFile(Common.context, filename);
		if (bytes != null)
			return new String(bytes);
		return null;
	}
	
	public static boolean javaCreateApplicationFile(String filename, String data) {
		return Utils.createApplicationFile(Common.context, filename, data);
	}

	public static boolean javaCreateApplicationFile(String filename, byte[] data) {
		return Utils.createApplicationFile(Common.context, filename, data);
	}

	public static boolean javaRawFileExists(String filename) {
		return Utils.rawFileExists(Common.context, filename);
	}

	public static boolean javaAssetsFileExists(String filename) {
		return Utils.assetsFileExists(Common.context, filename);
	}
	
	public static boolean javaApplicationFileExists(String filename) {
		return Utils.applicationFileExists(Common.context, filename);
	}
	
	public static void javaLongToast(String toastText) {
		final String finalToastText = toastText;
		Handler mainHandler = new Handler(Common.context.getMainLooper());
		Runnable myRunnable = new Runnable() {				
			@Override public void run() {
				Toast.makeText(Common.context, finalToastText, Toast.LENGTH_LONG).show();
			}
		};
		mainHandler.post(myRunnable);
	}
	
	public static void javaShortToast(String toastText) {
		final String finalToastText = toastText;
		Handler mainHandler = new Handler(Common.context.getMainLooper());
		Runnable myRunnable = new Runnable() {				
			@Override public void run() {
				Toast.makeText(Common.context, finalToastText, Toast.LENGTH_SHORT).show();
			}
		};
		mainHandler.post(myRunnable);
	}
	
	public static void javaYesNoDialog(int dialogId, String dialogTitle, String dialogMessage, String yesButtonText, String noButtonText) {
		
		// In order to use this method, SuperApplication.currentActivity must be set in you activity's onResume method. Note
		// that you should nullify it in the onPause method to avoid memory leaks,
	    
		final int finalDialogId = dialogId;
		final String finalDialogTitle = dialogTitle;
		final String finalDialogMessage = dialogMessage;
		final String finalYesButtonText = yesButtonText;
		final String finalNoButtonText = noButtonText;

		Common.currentActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				AlertDialog alertDialog = new AlertDialog.Builder(Common.currentActivity).create();
				alertDialog.setCancelable(false);
				alertDialog.setTitle(finalDialogTitle);
				alertDialog.setMessage(finalDialogMessage);
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, finalYesButtonText,
						new DialogInterface.OnClickListener() {
							@Override public void onClick(DialogInterface dialog, int which) { 
								nativeOnYesNoDialogResult(finalDialogId, true); 
							}
						});
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, finalNoButtonText,
						new DialogInterface.OnClickListener() {
							@Override public void onClick(DialogInterface dialog, int which) {
								nativeOnYesNoDialogResult(finalDialogId, false); 
							}
						});
				alertDialog.show();
			}
		});
	}
	
	public static void javaBrowseUrl(String url, String alternativeUrl) {
		if (Common.currentActivity != null)
			Utils.browseUrl(Common.currentActivity, url, alternativeUrl);
	}

	//////////////////////////////////////
	// Preferences
	//////////////////////////////////////

	static private SharedPreferences sharedPreferences;
	public static void javaSetBooleanPreference(String key, boolean value) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);
		
		Editor preferencesEditor = sharedPreferences.edit(); 
		preferencesEditor.putBoolean(key, value);
		preferencesEditor.commit();
	}
	
	public static boolean javaGetBooleanPreference(String key, boolean defaultValue) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		return sharedPreferences.getBoolean(key, defaultValue);
	}

	public static void javaSetIntPreference(String key, int value) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);
		
		Editor preferencesEditor = sharedPreferences.edit(); 
		preferencesEditor.putInt(key, value);
		preferencesEditor.commit();
	}
	
	public static int javaGetIntPreference(String key, int defaultValue) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		return sharedPreferences.getInt(key, defaultValue);
	}
	
	//////////////////////////////////////
	// Music
	//////////////////////////////////////
	
	private static ArrayList<Music> loadedMusics = new ArrayList<Music>();
	public static int javaLoadMusicFromAssets(String filename) {
		
		Music music;
		try {
			music = new Music(Common.context, filename);
			loadedMusics.add(music);
			return loadedMusics.size() - 1;
		} catch (Exception e) {}
		
		return -1;
	}
	
	public static void javaDeleteMusic(int musicIndex) {
		loadedMusics.get(musicIndex).stop();
		loadedMusics.set(musicIndex, null);
	}
	
	public static void javaPlayMusic(int musicIndex) {
		loadedMusics.get(musicIndex).play();
	}
	
	public static void javaMusicSetLooping(int musicIndex, boolean looping) {
		loadedMusics.get(musicIndex).setLooping(looping);
	}

	public static void javaStopMusic(int musicIndex) {
		loadedMusics.get(musicIndex).stop();
	}

	public static void javaPauseMusic(int musicIndex) {
		loadedMusics.get(musicIndex).pause();
	}

	public static void javaResumeMusic(int musicIndex) {
		loadedMusics.get(musicIndex).resume();
	}

	public static void javaFadeOutMusic(int musicIndex, int milliSeconds) {
		loadedMusics.get(musicIndex).fadeOut(milliSeconds);
	}
	
	//////////////////////////////////////
	// Sound
	//////////////////////////////////////

	private static ArrayList<Sound> loadedSounds = new ArrayList<Sound>();
	public static int javaLoadSoundFromAssets(String filename) {

		try {
			Sound sound = new Sound(Common.context, filename);
			loadedSounds.add(sound);
			return loadedSounds.size() - 1;
		} catch (Exception e) {}
		
		return -1;
	}

	public static void javaDeleteSound(int soundIndex) {		
		Sound sound = loadedSounds.get(soundIndex);
		sound.stop();
		sound.release();
		loadedSounds.set(soundIndex, null);
	}

	public static void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {
		loadedSounds.get(soundIndex).play(rate, leftVolume, rightVolume);
	}

	public static void javaStopSound(int soundIndex) {
		loadedSounds.get(soundIndex).stop();
	}

	public static void javaPauseSound(int soundIndex) {
		loadedSounds.get(soundIndex).pause();
	}

	public static void javaResumeSound(int soundIndex) {
		loadedSounds.get(soundIndex).resume();
	}

	public static void javaSoundSetLooping(int soundIndex, boolean looping) {
		loadedSounds.get(soundIndex).setLooping(looping);
	}

	public static void javaMuteSounds(boolean mute) {
		Sound.mute(mute);
	}
	
	//////////////////////////////////////
	// Sockets
	//////////////////////////////////////
	 
	private static ArrayList<SocketsClient> socketsClients = new ArrayList<SocketsClient>();
	public static int javaCreateSocket(String address, int port) {

		try {
			SocketsClient socketsClient = new SocketsClient(address, port);

			int i;
			for (i=0; i<socketsClients.size(); i++) {
				if (socketsClients.get(i) == null)
					break;
			}
			
			if (i<socketsClients.size()) {
				socketsClients.set(i, socketsClient);
			} else {
				socketsClients.add(socketsClient);
			}
			
			return i; 
		} catch (Exception e) {}
		
		return -1;
	}

	public static void javaCloseSocket(int socketIndex) {
		socketsClients.get(socketIndex).close();
		socketsClients.set(socketIndex, null);
	}
	
	public static void javaSocketWrite(int socketIndex, byte[] bytes) {
		socketsClients.get(socketIndex).write(bytes);
	}
	
	private static ByteArrayBuffer readBuffer = new ByteArrayBuffer();
	/** @return The read data size in bytes in the first 4 bytes, followed
	 * by the actual bytes read. */
	public static byte[] javaSocketRead(int socketIndex) {
		
		readBuffer.reset();
		int readBytesCount = 0;
		try { readBytesCount = socketsClients.get(socketIndex).recieve(readBuffer); } catch (Exception e) {}
		
		if (readBytesCount == 0)
			return null;
		
		return readBuffer.data;
	}
		
	//////////////////////////////////////
	// Misc
	//////////////////////////////////////
	
	public static float javaGetDpi() {
		return Common.context.getResources().getDisplayMetrics().xdpi;
	}

	static long startTime = System.currentTimeMillis();
	public static float javaGetTimeMillis() {
		//return System.nanoTime() / 1000000.0f;
		return System.currentTimeMillis() - startTime;
	}

	public static void javaExit() {
		System.exit(0);
	}
	
	public static void javaFinishActivity() {
		if (Common.currentActivity != null) {
			Common.currentActivity.onActivityFinished();
			Common.currentActivity.finish();
		}
	}

	public static Runnable openOptionsMenuRunnable = new Runnable() {
		@Override public void run() { Common.currentActivity.openOptionsMenu(); }
	};
	
	public static void javaOpenOptionsMenu() {
		if (Common.currentActivity != null) {
			Common.currentActivity.runOnUiThread(openOptionsMenuRunnable);
		}
	}

	public static void javaShowVirtualKeyboard() {
        InputMethodManager imm = (InputMethodManager) Common.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public static void javaHideVirtualKeyboard() {
        InputMethodManager imm = (InputMethodManager) Common.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}

	public static native void nativeOnPause();
	public static native void nativeOnResume();
	public static native void nativeOnSurfaceCreated();
	public static native void nativeOnSurfaceChanged(int width, int height);
	public static native void nativeOnDrawFrame();
	public static native void nativeOnTouchDown(int pointerId, float x, float y);
	public static native void nativeOnTouchDrag(int pointerId, float x, float y);
	public static native void nativeOnTouchUp(int pointerId, float x, float y);
	public static native void nativeOnBackPressed();
	public static native void nativeOnMenuPressed();
	public static native void nativeOnKeyDown(int keyCode);
	public static native void nativeOnKeyUp(int keyCode);
	public static native void nativeOnYesNoDialogResult(int dialogId, boolean yes);
	public static native void nativeOnMenuItemSelected(int itemId);
}
