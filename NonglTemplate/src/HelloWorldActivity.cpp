#include "HelloWorldActivity.h"

#include "../Nongl/TextView.h"

const char *HelloWorldActivity::activityName = "Hello world activity";

HelloWorldActivity::HelloWorldActivity() : Activity(activityName) {

	// Construct GUI,
	DisplayManager *displayManager = DisplayManager::getSingleton();

	// White background,
	ColorView *background = new ColorView(0, 0, displayManager->designWidth, displayManager->designHeight, 0xffffffff);
	rootLayout.addView(background);

	// Hello world,
	TextView *textView = new TextView(
			0, 0, displayManager->designWidth, displayManager->designHeight,
			Gravity::CENTER, 0xff000000);
	textView->setText("besm Allah, hello world :)");
	textView->setTextScale(2, 2);
	rootLayout.addView(textView);

	rootLayout.layout();
}

bool HelloWorldActivity::onBackPressed() {
	Nongl::exit();
	return true;
}
