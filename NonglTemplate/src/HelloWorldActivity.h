#pragma once

#include "../Nongl/Activity.h"

class HelloWorldActivity : public Activity {
public:

	static const char *activityName;
	HelloWorldActivity();

	bool onBackPressed();
};

