#include "../Nongl/Nongl.h"

#include "HelloWorldActivity.h"

void Nongl::main() {

	//DisplayManager::getSingleton()->loadTextureAtlasFromAssets("CombinedPack.txt");
	DisplayManager::getSingleton()->setMaximumLoadedTexturesCount(6);

	Activity *newActivity = new HelloWorldActivity();
	startActivity(newActivity);
}
