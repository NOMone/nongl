#include "Activity.h"
#include "Nongl.h"
#include "Threads.h"
#include "DisplayManager.h"
#include "SpriteBatch.h"

#include <string.h>

#define MAX_ALLOWED_FRAME_DURATION_MILLIS 40.0f //66.66f

Activity::Activity(const char *name, const Ngl::ActivityStyle &style) {

	activityStackIndex = 0;
	finishing = false;
	active = false;
	interactive = false;
	paused = false;
	hidesBelow = true;
	temporarilyNotHidingBelow = false;
	consumesAllInputs = true;
	buffersUnConsumedInputs = false;
	justShowed = false;

	maxAllowedFrameDurationMillis = MAX_ALLOWED_FRAME_DURATION_MILLIS;

	entranceAnimation = exitAnimation = 0;

	// Copy name,
	if (name) {
		this->name = new char[strlen(name)+1];
		strcpy(this->name, name);
	} else {
		this->name = 0;
	}

	// Start animation controller task,
	Nongl::scheduler.schedule([](void *data) -> bool {
		Activity *activity = (Activity *) data;
		activity->animationController.update(activity->getFrameTime());
		return false;
	}, 0, this, this);

	// Setup root layout,
	gestureListenersGroup.addGestureListener(&rootLayout);
	rootLayout.clipping = true;
	setStyle(style);

	// Let the activity receive gestures,
	gestureListenersGroup.addGestureListener(this);
}

Activity::~Activity() {

    if (onDestroyListener) onDestroyListener();

	if (name) delete[] name;

	if (entranceAnimation) delete entranceAnimation;
	if (exitAnimation) delete exitAnimation;

	Nongl::removeActivity(this);
}

void Activity::setStyle(const Ngl::ActivityStyle &activityStyle) {
	this->style = activityStyle;
	refreshStyle();
}

void Activity::refreshStyle() {

	rootLayout.clipping = style.clip;

	DisplayManager *displayManager = DisplayManager::getSingleton();

	float windowWidth  = displayManager->getDisplayWidth ();
	float windowHeight = displayManager->getDisplayHeight();
	float designWidth  = displayManager->designWidth ;
	float designHeight = displayManager->designHeight;

	float activityDesignWidth, activityDesignHeight;
	bool preserveAspectRatio;

	if (windowWidth < windowHeight) {
		// Portrait,
		preserveAspectRatio = style.preservePortraitAspectRatio;
		activityDesignWidth  = (style.portraitWidth ) ? style.portraitWidth  : designWidth ;
		activityDesignHeight = (style.portraitHeight) ? style.portraitHeight : designHeight;
	} else {
		// Landscape,
		preserveAspectRatio = style.preserveLandscapeAspectRatio;
		activityDesignWidth  = (style.landscapeWidth ) ? style.landscapeWidth  : designWidth ;
		activityDesignHeight = (style.landscapeHeight) ? style.landscapeHeight : designHeight;
	}

	if (style.followRealDimensions) {

		rootLayout.setScale(designWidth / windowWidth, designHeight / windowHeight);

		if (preserveAspectRatio) {

			float windowWidthHeightRatio = windowWidth / windowHeight;
			float designWidthHeightRatio = activityDesignWidth / activityDesignHeight;

			if (windowWidthHeightRatio < designWidthHeightRatio) {

				// Y padding is necessary,
				rootLayout.setWidth (windowWidth);
				rootLayout.setHeight(activityDesignHeight * windowWidth / activityDesignWidth);
				rootLayout.setLeft(0);
				rootLayout.setBottom(style.center ? (designHeight - rootLayout.getScaledHeight()) * 0.5f : 0);
			} else {

				// X padding is necessary,
				rootLayout.setWidth (activityDesignWidth * windowHeight / activityDesignHeight);
				rootLayout.setHeight(windowHeight);
				rootLayout.setLeft(style.center ? (designWidth - rootLayout.getScaledWidth()) * 0.5f : 0);
				rootLayout.setBottom(0);
			}

		} else {

			// Follow real dimensions but don't preserve aspect ratio,
			rootLayout.setLeft(0);
			rootLayout.setBottom(0);
			rootLayout.setWidth (windowWidth );
			rootLayout.setHeight(windowHeight);
		}
	} else {

		// Don't follow real dimensions,
		rootLayout.setWidth (activityDesignWidth);
		rootLayout.setHeight(activityDesignHeight);

		if (preserveAspectRatio) {

			float windowWidthHeightRatio = windowWidth / windowHeight;
			float designWidthHeightRatio = activityDesignWidth / activityDesignHeight;

			if (windowWidthHeightRatio < designWidthHeightRatio) {

				// Y padding is necessary,
				rootLayout.setScale(
						designWidth / activityDesignWidth,
						(designHeight * windowWidthHeightRatio) / (activityDesignHeight * designWidthHeightRatio));
				rootLayout.setLeft(0);
				rootLayout.setBottom(style.center ? (designHeight - rootLayout.getScaledHeight()) * 0.5f : 0);
			} else {

				// X padding is necessary,
				rootLayout.setScale(
						(designWidth * designWidthHeightRatio) / (activityDesignWidth * windowWidthHeightRatio),
						designHeight / activityDesignHeight);
				rootLayout.setLeft(style.center ? (designWidth - rootLayout.getScaledWidth()) * 0.5f : 0);
				rootLayout.setBottom(0);
			}
		} else {

			// Don't follow real dimensions and don't preserve aspect ratio,
			rootLayout.setLeft(0);
			rootLayout.setBottom(0);
			rootLayout.setScale(designWidth / activityDesignWidth, designHeight / activityDesignHeight);
		}
	}
}

void Activity::setInteractive(bool interactive) {
	if (this->interactive != interactive) {
		this->interactive = interactive;
	}
}

float Activity::getFrameTime() {

	float elapsedTimeMillis = Nongl::getFrameTime();
	if (elapsedTimeMillis > maxAllowedFrameDurationMillis) {
		elapsedTimeMillis = maxAllowedFrameDurationMillis;
	}

	return elapsedTimeMillis;
}

const Animation *Activity::getEntranceAnimation() {
	return Nongl::getDefaultActivityEntranceAnimation();
}

const Animation *Activity::getExitAnimation() {
	return Nongl::getDefaultActivityExitAnimation();
}

void Activity::attachToAnimation(Animation *animation) {

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = *iterator;
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) {
					currentComponent->initialValueFloat *= DisplayManager::getSingleton()->designWidth;
				}
				if (currentComponent->finalValueRelative) {
					currentComponent->finalValueFloat *= DisplayManager::getSingleton()->designWidth;
				}
				currentComponent->setTargetAddress(&DisplayManager::getSingleton()->viewPortTransformation.transformationX);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) {
					currentComponent->initialValueFloat *= DisplayManager::getSingleton()->designHeight;
				}
				if (currentComponent->finalValueRelative) {
					currentComponent->finalValueFloat *= DisplayManager::getSingleton()->designHeight;
				}
				currentComponent->setTargetAddress(&DisplayManager::getSingleton()->viewPortTransformation.transformationY);
				break;

			case AnimationTarget::COLOR_MASK:
				currentComponent->setTargetAddress(&spriteBatch.colorMask, true);
				break;

			default:
				break;
		}
	}
}

bool Activity::isAnimating() {

	if (entranceAnimation && (!entranceAnimation->hasFinished()))
		return true;

	if (exitAnimation && (!exitAnimation->hasFinished()))
		return true;

	return false;
}

class OnActivityHiddenTask : public Runnable {
	Activity *activityToDelete;
	bool finishActivity;
public:
	OnActivityHiddenTask(Activity *activityToDelete, bool finishActivity) : Runnable() {
		this->activityToDelete = activityToDelete;
		this->finishActivity = finishActivity;
	}

	bool run(void *data) {
		activityToDelete->onHidden();
		activityToDelete->setActive(false);
		if (finishActivity) {
			Nongl::scheduler.schedule([] (void *data) -> bool {
				delete (Activity *) data;
				return true;
			}, 0, activityToDelete);
		}

		return true;
	}
};

void Activity::hide(bool finishActivity) {

	if (isFinishing())
		return ;

	if (!isActive() && finishActivity) {
		Nongl::scheduler.schedule([] (void *data) -> bool {
			delete (Activity *) data;
			return true;
		}, 0, this);
		return ;
	}

	if (!isActive() || (exitAnimation && !finishActivity)) {
		return ;
	}

	setInteractive(false);
	setFinishing(finishActivity);

	// Run exit animation,
	if (entranceAnimation) {
		delete entranceAnimation;
		entranceAnimation = 0;
	}

	if (exitAnimation) {
		delete exitAnimation;
	}

	Ngl::shared_ptr<OnActivityHiddenTask> onHiddenTask(new OnActivityHiddenTask(this, finishActivity));

	if (justShowed) {
		// No need for exit animation,
		onHiddenTask->run(0);
	} else {
		exitAnimation = getExitAnimation()->clone();
		attachToAnimation(exitAnimation);
		exitAnimation->setOnAnimationEndTask(onHiddenTask);
		exitAnimation->start();
	}

	onHide();
}

void Activity::show() {

	if (isFinishing() || entranceAnimation)
		return ;

	setActive(true);

	// Run entrance animation,
	if (exitAnimation) {
		delete exitAnimation;
		exitAnimation = 0;
	}

	if (entranceAnimation) {
		delete entranceAnimation;
	}

	entranceAnimation = getEntranceAnimation()->clone();
	attachToAnimation(entranceAnimation);

	Ngl::shared_ptr<Runnable> onActivityShowedTask(Runnable::createRunnable([] (void *data) -> bool {
		Activity *activity = (Activity *) data;
		activity->setInteractive(true);
		activity->onShowed();
		return true;
	}, this));

	entranceAnimation->setOnAnimationEndTask(onActivityShowedTask, this);
	entranceAnimation->start();

	justShowed = true;
	onShow();
}

void Activity::finish() {
	setHidesBelow(false);
	hide(true);
}

void Activity::setOnSurfaceCreatedListener(std::function<void()> onSurfaceCreatedListener) {
    this->onSurfaceCreatedListener = onSurfaceCreatedListener;
}

void Activity::setOnResumeListener(std::function<void(bool)> onResumeListener) {
    this->onResumeListener = onResumeListener;
}

void Activity::setOnDestroyListener(std::function<void()> onDestroyListener) {
    this->onDestroyListener = onDestroyListener;
}

void Activity::onResume(bool externalResume) {
    if (onResumeListener) onResumeListener(externalResume);
}

void Activity::onSurfaceCreated() {
    spriteBatch.onVBOsDiscarded();
    if (onSurfaceCreatedListener) onSurfaceCreatedListener();
}

void Activity::onBeforeDrawFrame(float elapsedTimeMillis) {

	//LOGE("Activity %s drawn", name);
	justShowed = false;

	if (entranceAnimation) {
		entranceAnimation->update(elapsedTimeMillis);
	} else if (exitAnimation) {
		exitAnimation->update(elapsedTimeMillis);
	}
	DisplayManager::getSingleton()->applyClipping(false);
}

void Activity::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {

	if ((spriteBatch.colorMask & 0xff000000) == 0x00000000) return ;

	rootLayout.setParentBounds(rootLayout.getBoundingRect());
	rootLayout.updateDepthLayersCount();
	rootLayout.adjustDepths(maxZ, minZ);
    rootLayout.update(elapsedTimeMillis);
	if ((spriteBatch.colorMask & 0xff000000) == 0xff000000) {
		spriteBatch.changeBlendMode(false);
		rootLayout.drawOpaqueParts(&spriteBatch);
		spriteBatch.changeBlendMode(true);
		rootLayout.drawTransparentParts(&spriteBatch);
	} else {
		spriteBatch.changeBlendMode(true);
		rootLayout.drawAllInOrder(&spriteBatch);
	}
}
