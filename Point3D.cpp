#include "Point3D.h"

#include <math.h>

void Point3D::roll(float angleRadian) {

	float cosAngle = cosf(angleRadian);
	float sinAngle = sinf(angleRadian);

	float newZ;
	newZ = (z * cosAngle) - (y * sinAngle);
	y    = (y * cosAngle) + (z * sinAngle);
	z = newZ;
}

void Point3D::pitch(float angleRadian) {

	float cosAngle = cosf(angleRadian);
	float sinAngle = sinf(angleRadian);

	float newX;
	newX = (x * cosAngle) - (z * sinAngle);
	z    = (z * cosAngle) + (x * sinAngle);
	x = newX;
}

void Point3D::yaw(float angleRadian) {

	float cosAngle = cosf(angleRadian);
	float sinAngle = sinf(angleRadian);

	float newX;
	newX = (x * cosAngle) - (y * sinAngle);
	y    = (y * cosAngle) + (x * sinAngle);
	x = newX;
}
