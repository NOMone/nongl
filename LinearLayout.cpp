#include "LinearLayout.h"

// TODO: Use view transformation instead of just adding left and right...

LinearLayout::LinearLayout() {
	set(0, 0, 100, 100);
}

LinearLayout::LinearLayout(float x, float y, float width, float height, Orientation::Value orientation, Gravity::Value gravity, float padding) {
	set(x, y, width, height, orientation, gravity, padding);
}

void LinearLayout::set(float x, float y, float width, float height, Orientation::Value orientation, Gravity::Value gravity, float padding) {

	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->orientation = orientation;
	this->gravity = gravity;
	this->padding = padding;
}

void LinearLayout::layout() {

    // TODO: use view transformations instead... (?)
    
	int childrenViewsCount = (int) childrenViews.size();

	// Get vertical and horizontal gravities,
	VerticalGravity::Value verticalGravity;
	HorizontalGravity::Value horizontalGravity;
	Gravity::getGravityComponents(gravity, &verticalGravity, &horizontalGravity);

	// Do the layouting,
	// Vertical orientation,
	if (orientation == Orientation::VERTICAL) {

        // Calculate layout bottom,
		float currentBottom;
		switch (verticalGravity) {
			case VerticalGravity::BOTTOM:
				currentBottom = getBottom();
				break;
			case VerticalGravity::CENTER:
				currentBottom = getScaledHeight() - (padding * (childrenViewsCount - 1));
				for (int i=0; i<childrenViewsCount; i++) {
					currentBottom -= childrenViews[i]->getScaledHeight();
				}
				currentBottom *= 0.5f;
				currentBottom += getBottom();
				break;
			case VerticalGravity::TOP:
				currentBottom = getTop() - (padding * (childrenViewsCount - 1));
				for (int i=0; i<childrenViewsCount; i++) {
					currentBottom -= childrenViews[i]->getScaledHeight();
				}
				break;
		}

        // Do the vertical layouting,
		switch (horizontalGravity) {
			case HorizontalGravity::LEFT: {
				float left = getLeft();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setLeft(left);
					childrenViews[i]->setBottom(currentBottom);
					currentBottom += childrenViews[i]->getScaledHeight() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
			case HorizontalGravity::CENTER: {
				float centerX = getCenterX();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setCenterX(centerX);
					childrenViews[i]->setBottom(currentBottom);
					currentBottom += childrenViews[i]->getScaledHeight() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
			case HorizontalGravity::RIGHT: {
				float right = getRight();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setRight(right);
					childrenViews[i]->setBottom(currentBottom);
					currentBottom += childrenViews[i]->getScaledHeight() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
		}

	} else {

		// Horizontal orientation,
        
        // Calculate layout left,
		float currentLeft;
		switch (horizontalGravity) {
			case HorizontalGravity::LEFT:
				currentLeft = getLeft();
				break;
			case HorizontalGravity::CENTER:
				currentLeft = getScaledWidth() - (padding * (childrenViewsCount - 1));
				for (int i=0; i<childrenViewsCount; i++) {
					currentLeft -= childrenViews[i]->getScaledWidth();
				}
				currentLeft *= 0.5f;
				currentLeft += getLeft();
				break;
			case HorizontalGravity::RIGHT:
				currentLeft = getRight() - (padding * (childrenViewsCount - 1));
				for (int i=0; i<childrenViewsCount; i++) {
					currentLeft -= childrenViews[i]->getScaledWidth();
				}
				break;
		}

        // Do the horizontal layouting,
		switch (verticalGravity) {
			case VerticalGravity::BOTTOM: {
				float bottom = getBottom();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setBottom(bottom);
					childrenViews[i]->setLeft(currentLeft);
					currentLeft += childrenViews[i]->getScaledWidth() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
			case VerticalGravity::CENTER: {
				float centerY = getCenterY();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setCenterY(centerY);
					childrenViews[i]->setLeft(currentLeft);
					currentLeft += childrenViews[i]->getScaledWidth() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
			case VerticalGravity::TOP: {
				float top = getTop();
				for (int i=0; i<childrenViewsCount; i++) {
					childrenViews[i]->setTop(top);
					childrenViews[i]->setLeft(currentLeft);
					currentLeft += childrenViews[i]->getScaledWidth() + padding;
					childrenViews[i]->onJustLayouted();
				}
				break;
			}
		}

	}
}

void LinearLayout::fitContents() {

	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (orientation == Orientation::VERTICAL) {

		float maxWidth = 0;
		float totalHeight = 0;
		for (int i=0; i<childrenViewsCount; i++) {
			if (childrenViews[i]->getScaledWidth() > maxWidth) maxWidth = childrenViews[i]->getScaledWidth();
			totalHeight += childrenViews[i]->getScaledHeight();
		}
		totalHeight += padding * (childrenViewsCount - 1);
		setWidth(maxWidth);
		setHeight(totalHeight);

	} else {

		float maxHeight = 0;
		float totalWidth = 0;
		for (int i=0; i<childrenViewsCount; i++) {
			if (childrenViews[i]->getScaledHeight() > maxHeight) maxHeight = childrenViews[i]->getScaledHeight();
			totalWidth += childrenViews[i]->getScaledWidth();
		}
		totalWidth += padding * (childrenViewsCount - 1);
		setWidth(totalWidth);
		setHeight(maxHeight);
	}
}
