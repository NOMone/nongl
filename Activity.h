#pragma once

#include <Nongl/Animation.h>
#include <Nongl/AbsoluteLayout.h>
#include <Nongl/SpriteBatch.h>
#include <Nongl/GestureListenersGroup.h>

#include <functional>

namespace Ngl {
	class ActivityStyle {
	public:
		float portraitWidth=0, portraitHeight=0;    // When set to 0, they follow the design
		float landscapeWidth=0, landscapeHeight=0;  // dimensions, unless followRealDimensions is set.

		bool followRealDimensions=false;  // Indicates that the activity's dimensions should follow
									      // the real size of the window.

		bool preservePortraitAspectRatio=false;  // Works by introducing padding when necessary, whether
		bool preserveLandscapeAspectRatio=false; // followRealDimensions is set or not.

		bool clip=true;
		bool center=true;
	};
}

class Activity : public GestureListener {

	char *name;
	int activityStackIndex;
	bool finishing;
	bool active;                           // Visible.
	bool interactive;                      // Responds to user inputs.
	bool paused;                           // getTime returns 0.
	bool hidesBelow;                       // Anything below this activity shouldn't be drawn.
	bool temporarilyNotHidingBelow;        // Anything below this activity can be drawn until sorted in position.
	bool consumesAllInputs;                // Doesn't allow inputs to propagate downwards even if not interactive.
	bool buffersUnConsumedInputs;   // Requires inputs to be buffered if not consumed immediately.
	bool justShowed;                       // No frames were drawn since show yet.

	float maxAllowedFrameDurationMillis;

	Ngl::ActivityStyle style;
	Animation *entranceAnimation, *exitAnimation;

	std::function<void(bool)> onResumeListener;
    std::function<void()> onSurfaceCreatedListener;
	std::function<void()> onDestroyListener;

public:

	SpriteBatch spriteBatch;
	AnimationController animationController;
	AbsoluteLayout rootLayout;
	GestureListenersGroup gestureListenersGroup;

	Activity(const char *name, const Ngl::ActivityStyle &style=Ngl::ActivityStyle());
	virtual ~Activity();

	void setStyle(const Ngl::ActivityStyle &activityStyle);
	void refreshStyle();

	inline int getActivityStackIndex() { return activityStackIndex; }
	inline void setActivityStackIndex(int activityStackIndex) { this->activityStackIndex = activityStackIndex; }

	inline bool isFinishing() { return finishing; }
	inline void setFinishing(bool finishing) { this->finishing = finishing; }

	// Denotes whether this application is hidden or not,
	inline bool isActive() const { return active; }
	inline void setActive(bool active) { this->active = active; }

	inline bool isInteractive() const { return interactive; }
	void setInteractive(bool interactive);

	inline bool isPaused() { return paused; }
	inline void setPaused(bool paused) { this->paused = paused; }

	inline bool isTemporarilyNotHidingBelow() { return temporarilyNotHidingBelow; }
	inline void setTemporarilyNotHidingBelow(bool enabled) { this->temporarilyNotHidingBelow = enabled; }

	inline bool isHidesBelow() { return hidesBelow && (!temporarilyNotHidingBelow); }
	inline void setHidesBelow(bool hidesBelow) { this->hidesBelow = hidesBelow; }

	inline bool isConsumesAllInputs() { return consumesAllInputs; }
	inline void setConsumesAllInputs(bool consumesAllInputs) { this->consumesAllInputs = consumesAllInputs; }

	// Buffers only is consumesAllInputs is set,
	inline bool isBuffersUnconsumedInputs() { return buffersUnConsumedInputs; }
	inline void setBuffersUnconsumedInputs(bool buffersUnconsumedInputs) { this->buffersUnConsumedInputs = buffersUnconsumedInputs; }

	inline const char *getName() { return name; }

	virtual float getFrameTime();

	virtual const Animation *getEntranceAnimation();
	virtual const Animation *getExitAnimation();
	void attachToAnimation(Animation *animation);
	bool isAnimating();

	void hide(bool finishActivity=false);		// Library method. Should never be called by the application.
	void show();								// Library method. Should never be called by the application.
	void finish();

	void setOnResumeListener(std::function<void(bool)> onResumeListener);
    void setOnSurfaceCreatedListener(std::function<void()> onSurfaceCreatedListener);
    void setOnDestroyListener(std::function<void()> onDestroyListener);

	virtual void onPause(bool externalPause) {}
	virtual void onResume(bool externalResume);
	virtual void onSurfaceCreated();
	virtual void onSurfaceChanged(int width, int height) { refreshStyle(); };
	virtual void onBeforeDrawFrame(float elapsedTimeMillis);
	virtual void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);
	inline virtual bool onKeyDown(int keyCode) { return false; }
	inline virtual bool onKeyUp(int keyCode) { return false; }
	inline virtual bool onBackPressed() { return false; }
	inline virtual bool onMenuPressed() { return false; }
	inline virtual bool onMenuItemSelected(int itemId) { return false; }
	inline virtual void onShow() {}
	inline virtual void onHide() {}
	inline virtual void onShowed() {}
	inline virtual void onHidden() {}

	inline virtual bool onTouchEvent(const TouchEvent *event) { return false; }
};

