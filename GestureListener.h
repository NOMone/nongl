#pragma once

class TouchState;

class TouchEvent {
public:

	enum TouchEventType { TOUCH_DOWN, TOUCH_DRAG, TOUCH_DRAG_END, TOUCH_UP, SCROLL, CLICK, FLICK, ZOOM, ZOOM_END};

	TouchEventType type;
	TouchState *touchState;

	int pointerId;
	float x, y;
	float downX, downY;
	float maxDraggedDistanceCms;
	float xMagnitude, yMagnitude;
	float ratio;

	void setAsTouchDown(int pointerId, float x, float y);
	void setAsTouchDrag(int pointerId, float downX, float downY, float x, float y, float maxDraggedDistanceCms);
	void setAsTouchDragEnd(int pointerId, float downX, float downY, float upX, float upY, float maxDraggedDistanceCms);
	void setAsTouchUp(int pointerId, float x, float y, float maxDraggedDistanceCms);
	void setAsScroll(float x, float y, float xOffset, float yOffset);
	void setAsClick(int pointerId, float x, float y);
	void setAsFlick(int pointerId, float x, float y, float xMagnitude, float yMagnitude);
	void setAsZoom(float x, float y, float ratio);
	void setAsZoomEnd(float x, float y, float ratio);
};

class GestureListener {

public:
	virtual inline ~GestureListener() {}

	virtual bool onTouchEvent(const TouchEvent *event) = 0;
};
