#include "Layout.h"

#include "LinearLayout.h"

using namespace Ngl;

Layout::~Layout() {

	// Delete all children if needed,
	if (deletesChildrenOnDestruction) {
		int32_t childrenViewsCount = (int32_t) childrenViews.size();
		for (int32_t i=0; i<childrenViewsCount; i++) {
			delete childrenViews[i];
		}
	}
}

int32_t Layout::getViewIndex(View *view) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int32_t i=0; i<childrenViewsCount; i++) {
		if (childrenViews[i] == view) {
			return i;
		}
	}
	return -1;
}

void Layout::removeView(View *view) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int32_t i=0; i<childrenViewsCount; i++) {
		if (childrenViews[i] == view) {
			childrenViews.erase(childrenViews.begin()+i);
			return;
		}
	}
}

void Layout::removeView(int32_t index) {
    childrenViews.erase(childrenViews.begin()+index);
}

void Layout::layout() {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int i=0; i<childrenViewsCount; i++) {
		childrenViews[i]->onJustLayouted();
	}
}

Ngl::Rect Layout::transformParentRectToLocalRect(const Ngl::Rect &rect) {
	return rect;
}

void Layout::setParentBounds(const Ngl::Rect &parentBounds) {

	this->parentBounds = parentBounds;

	Rect bounds(getLeft(), getBottom(), getRight(), getTop());
	auto intersectionResult = bounds.intersect(parentBounds);
	if (!intersectionResult.intersecting) intersectionResult.intersection.clear();

	Rect intersection = transformParentRectToLocalRect(intersectionResult.intersection);
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int i=0; i<childrenViewsCount; i++) childrenViews[i]->setParentBounds(intersection);
}

void Layout::updateDepthLayersCount() {

	depthLayersCount = 0;
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int i=0; i<childrenViewsCount; i++) {
		childrenViews[i]->updateDepthLayersCount();
		depthLayersCount += childrenViews[i]->getDepthLayersCount();
	}
}

void Layout::adjustDepths(float maxZ, float minZ) {

	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (!childrenViewsCount) return ;
	if (childrenViewsCount==1) {
		childrenViews[0]->adjustDepths(maxZ, minZ);
	} else {

		int depthLayersCount = getDepthLayersCount();
		if (!depthLayersCount) return ;

		float depthRange = maxZ - minZ;
		float totalPadding = depthRange / 100;
		float childPadding = totalPadding / (childrenViewsCount-1);
		float layerRange = (depthRange - totalPadding) / depthLayersCount;

		for (int i=0; i<childrenViewsCount; i++) {
			float currentChildRange = layerRange * childrenViews[i]->getDepthLayersCount();
			childrenViews[i]->adjustDepths(maxZ, maxZ-currentChildRange);
			maxZ -= currentChildRange + childPadding;
		}
	}
}

void Layout::drawOpaqueParts(SpriteBatch *spriteBatch) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (drawOutOfBoundChildren) {
		for (int i=childrenViewsCount-1;i>-1; i--) {
			if (childrenViews[i]->isVisible()) childrenViews[i]->drawOpaqueParts(spriteBatch);
		}
	} else {
		for (int i=childrenViewsCount-1;i>-1; i--) {
			View *currentChild = childrenViews[i];
			if (currentChild->isVisible() && currentChild->withinParentBounds()) currentChild->drawOpaqueParts(spriteBatch);
		}
	}
}

void Layout::drawTransparentParts(SpriteBatch *spriteBatch) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (drawOutOfBoundChildren) {
		for (int i=0 ;i<childrenViewsCount; i++) {
			if (childrenViews[i]->isVisible()) childrenViews[i]->drawTransparentParts(spriteBatch);
		}
	} else {
		for (int i=0 ;i<childrenViewsCount; i++) {
			View *currentChild = childrenViews[i];
			if (currentChild->isVisible() && currentChild->withinParentBounds()) currentChild->drawTransparentParts(spriteBatch);
		}
	}
}

void Layout::drawAllInOrder(SpriteBatch *spriteBatch) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	if (drawOutOfBoundChildren) {
		for (int i=0 ;i<childrenViewsCount; i++) {
			if (childrenViews[i]->isVisible()) childrenViews[i]->drawAllInOrder(spriteBatch);
		}
	} else {
		for (int i=0 ;i<childrenViewsCount; i++) {
			View *currentChild = childrenViews[i];
			if (currentChild->isVisible() && currentChild->withinParentBounds()) currentChild->drawAllInOrder(spriteBatch);
		}
	}
}

void Layout::update(float elapsedTimeMillis) {
	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int i=0 ;i<childrenViewsCount; i++) {
		childrenViews[i]->update(elapsedTimeMillis);
	}
}

bool Layout::onTouchEvent(const TouchEvent *event) {

	if (!isEnabled()) return false;

	int32_t childrenViewsCount = (int32_t) childrenViews.size();
	for (int32_t i=childrenViewsCount-1; i>-1; i--) {
		if (childrenViews[i]->onTouchEvent(event)) return true;
	}
	return false;
}

////////////////////////////////////
// Convenience helper functions
////////////////////////////////////

LinearLayout *Layout::addMatchingLinearLayout(Orientation::Value orientation, Gravity::Value gravity) {
	LinearLayout *linearLayout = new LinearLayout(
			0, 0,
			getWidth(), getHeight(),
			orientation, gravity);
	addView(linearLayout);
	return linearLayout;
}
