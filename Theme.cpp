#include "Theme.h"

#include "AbsoluteLayout.h"
#include "LinearLayout.h"
#include "NinePatchView.h"
#include "DisplayManager.h"
#include "TextView.h"

using namespace Ngl;

//////////////////////////////////////////
// Theme
//////////////////////////////////////////

Theme::~Theme() {}

Theme &Theme::getGlobalTheme() {

	// There should be a valid current theme at all times,
	static ManagedPointer<Theme> currentTheme;
	if (currentTheme.expired()) currentTheme.set(mkshrd(new CandypedeTheme()));
	return *currentTheme;
}

Layout *Theme::createDialogLayout(float width, float height) {

	// Dialog layout,
	AbsoluteLayout *dialogLayout = new AbsoluteLayout(0, 0, width, height);
	dialogLayout->addView(createDialogBackground(width, height));

	return dialogLayout;
}

Layout *Theme::createNotificationLayout(float width, float height) {

	// Notification layout,
	AbsoluteLayout *notificationLayout = new AbsoluteLayout(0, 0, width, height);
	notificationLayout->addView(createNotificationBackground(width, height));

	return notificationLayout;
}

//////////////////////////////////////////
// White theme
//////////////////////////////////////////

View *WhiteTheme::createDialogBackground(float width, float height) {

	NinePatchData dialogNinePatchData;
	dialogNinePatchData.textureRegion = DisplayManager::getSingleton()->getTextureRegion("NONGL.dialogBackground");
	dialogNinePatchData.setColumnWidths(3, false, 24, 4, 24);
	dialogNinePatchData.setRowHeights  (3, false, 24, 4, 24);

	NinePatchView *dialogBackground = new NinePatchView(&dialogNinePatchData);
	dialogBackground->setWidth(width);
	dialogBackground->setHeight(height);
	dialogBackground->getPatchRegion(1, 1)->hasAlpha = false;

	return dialogBackground;
}

TextView *WhiteTheme::createText(const char *text, float maxWidth) {

	float textScale = 0.6484375f;
	float width = TextSprite::getTextWidth(text, maxWidth, textScale);

	TextView *textView = new TextView(0, 0, width, 10, Gravity::CENTER, 0xffffffff);
	textView->setText(text);
	textView->setTextScale(textScale, textScale);
	textView->setHeight(textView->getTextHeight());

	return textView;
}

View *WhiteTheme::createNotificationBackground(float width, float height) {

	// Notification background,
	// TODO: should be just a nine-patch view. Check the next todo,
	AbsoluteLayout *backgroundLayout = new AbsoluteLayout(0, 0, width, height);

	// White layer,
	NinePatchData dialogNinePatchData;
	dialogNinePatchData.textureRegion = DisplayManager::getSingleton()->getTextureRegion("NONGL.roundCorners");
	dialogNinePatchData.setColumnWidths(3, false, 11, 1, 11);
	dialogNinePatchData.setRowHeights  (3, false, 10, 1, 10);

	NinePatchView *dialogBackground = new NinePatchView(&dialogNinePatchData);
	dialogBackground->setWidth(width);
	dialogBackground->setHeight(height);
	dialogBackground->getPatchRegion(1, 1)->hasAlpha = false;

	backgroundLayout->addView(dialogBackground);

	// Black with border layer,
	// TODO: should be removed for better performance. Just combine both
	// layers in a single nine-patch...
	backgroundLayout->addView(createDialogBackground(width, height));

	return backgroundLayout;
}

Layout *WhiteTheme::createNotificationLayout(float width, float height) {

	// Notification layout,
	AbsoluteLayout *notificationLayout = new AbsoluteLayout(0, 0, width, height);
	notificationLayout->addView(createNotificationBackground(width, height));

	return notificationLayout;
}

TextView *WhiteTheme::createNotificationText(const char *text, float maxWidth) {

	float textScale = 0.45f;
	float width = TextSprite::getTextWidth(text, maxWidth, textScale);

	TextView *textView = new TextView(0, 0, width, 10, Gravity::CENTER_LEFT, 0xffffffff);
	textView->setText(text);
	textView->setTextScale(textScale, textScale);
	textView->setHeight(textView->getTextHeight());

	return textView;
}

View *CandypedeTheme::createDialogBackground(float width, float height) {

    NinePatchData dialogNinePatchData;
    dialogNinePatchData.textureRegion = DisplayManager::getSingleton()->getTextureRegion("UI.dialogBackground");
    dialogNinePatchData.setColumnWidths(3, false, 27, 3, 27);
    dialogNinePatchData.setRowHeights  (3, false, 33, 3, 33);

    NinePatchView *dialogBackground = new NinePatchView(&dialogNinePatchData);
    dialogBackground->setWidth(width);
    dialogBackground->setHeight(height);
    //dialogBackground->getPatchRegion(1, 1)->hasAlpha = false;

    return dialogBackground;
}

View *CandypedeTheme::createNotificationBackground(float width, float height) {

    // Notification background,
    // TODO: should be just a nine-patch view. Check the next todo,
    AbsoluteLayout *backgroundLayout = new AbsoluteLayout(0, 0, width, height);

    // White layer,
    NinePatchData dialogNinePatchData;
    dialogNinePatchData.textureRegion = DisplayManager::getSingleton()->getTextureRegion("NONGL.roundCorners");
    dialogNinePatchData.setColumnWidths(3, false, 11, 1, 11);
    dialogNinePatchData.setRowHeights  (3, false, 10, 1, 10);

    NinePatchView *dialogBackground = new NinePatchView(&dialogNinePatchData);
    dialogBackground->setWidth(width);
    dialogBackground->setHeight(height);
    dialogBackground->getPatchRegion(1, 1)->hasAlpha = false;

    backgroundLayout->addView(dialogBackground);

    // Black with border layer,
    // TODO: should be removed for better performance. Just combine both
    // layers in a single nine-patch...
    backgroundLayout->addView(WhiteTheme::createDialogBackground(width, height));
    backgroundLayout->addView(createDialogBackground(width, height));

    return backgroundLayout;
}

TextView *CandypedeTheme::createNotificationText(const char *text, float maxWidth) {
    TextView *notificationText = WhiteTheme::createNotificationText(text, maxWidth);
    //notificationText->setColorMask(0xff222222);
    return notificationText;
}
