#pragma once

#include <stdint.h>

namespace Ngl {

	class InputStream;

	class OutputStream {
	public:
		virtual inline ~OutputStream() { }

		// Writes all data to the output stream,
		virtual void writeData(const void *data, int32_t sizeBytes) = 0;

		// Writes some (or all) data available to output stream. Returns
		// data size,
		virtual int32_t writeData(InputStream &inputStream);

		// Blocks until input stream hasData() is no longer true, writes
		// all data possible. Returns data size,
		int32_t writeAllData(InputStream &inputStream);

		// Flushes any data that might be buffered and not written to the
		// stream yet,
		virtual inline void flush() {}
	};
}
