
#include <Nongl/HttpRequest.h>
#include <Nongl/SystemInterface.h>
#include <Nongl/Nongl.h>
#include <Nongl/Json/json.h>

#include <thread>
#include <functional>

using namespace Ngl;

/////////////////////////
// HTTP Request
/////////////////////////

HttpRequestData &HttpRequestData::add(const TextBuffer &name, const TextBuffer &value) {
    if (dataText.isEmpty()) {
        dataText.append(name).append('=').append(value);
    } else {
        dataText.append('&').append(name).append('=').append(value);
    }
    return *this;
}

HttpRequestData &HttpRequestData::encodeAndAdd(const TextBuffer &name, const TextBuffer &value) {
    if (dataText.isEmpty()) {
        dataText.append(name).append('=').append(systemUrlEncode(value));
    } else {
        dataText.append('&').append(name).append('=').append(systemUrlEncode(value));
    }
    return *this;
}

std::vector<uint8_t> HttpRequest::send() {
    return systemHttpRequest(url, data.dataText, type == HttpRequestType::POST);
}

///////////////////////////////
// Async Request
///////////////////////////////

void AsyncRequest::pollStateChanges(Activity *activity) {

    // Check for state changes every frame,
    shared_ptr<Runnable> scheduledTask(Runnable::createRunnable([](void *asyncRequest)->bool {

        AsyncRequest *request = (AsyncRequest *) asyncRequest;

        if (request->performStateAction()) {
            // The request reached it's ending state,
            delete request;
            return true;
        }

        // Keep polling,
        return false;
    }, activity));
    Nongl::scheduler.schedule(scheduledTask, 0, this);
}

void AsyncRequest::sendRequestInParallel() {

    // Send request in a parallel thread (if not HTML5 build),
    #ifndef EMSCRIPTEN
        std::thread loginThread([] (AsyncRequest *asyncRequest)
    #endif
    {
            asyncRequest->sendRequest();
    }
    #ifndef EMSCRIPTEN
        , this);
        loginThread.detach();
    #endif
}

void AsyncRequest::sendRequest() {

    // Keep a copy of the alive token,
    Ngl::weak_ptr<AliveToken> aliveToken = this->aliveToken;

    // Perform the request (which should take some time, during which
    // the login activity may die,
    std::vector<uint8_t> requestResponse = httpRequest.send();

    // Make sure the activity is alive before proceeding,
    if (aliveToken.expired()) return;

    // Move to the next state,
    requestResponse.swap(this->requestResponse);

    advanceState();
}

///////////////////////////////
// Simple Async Request
///////////////////////////////

bool SimpleAsyncRequest::performStateAction() {

    switch (state) {
        case State::RECEIVED_RESPONSE: {
            receivedResponseState();
            return true;
        }
        default: return false;
    }
}

void SimpleAsyncRequest::receivedResponseState() {

    // The request was initialized with a plain data listener,
    if (!onPlainResponseReceivedListener.expired()) {
        (*onPlainResponseReceivedListener)(requestResponse, "");
        return ;
    }

    // If the request was initialized with JSON data listener and
    // it has already expired, then there is no point in going
    // through the trouble of parsing,
    if (onJsonResponseReceivedListener.expired()) return ;

    // Pad the response with 2 zeroes to allow parsing in place,
    requestResponse.push_back(0);
    requestResponse.push_back(0);

    JSON::Parser jsonParser;
    JSON::Value *json = jsonParser.parseInPlace((char *) &requestResponse[0], requestResponse.size());

    auto error = [&](const TextBuffer &message) {
        delete json;
        (*onJsonResponseReceivedListener)(nullptr, message);
    };

    // Errors occurred?
    if (jsonParser.logErrors()) { error("Response parsing error (probably no response)"); return; }

    // Unexpected response?
    JSON::Object *responseObject = dynamic_cast<JSON::Object *>(json);
    if (!responseObject) { error("Unexpected response type"); return; }

    // Tell the listener about the whole thing!
    (*onJsonResponseReceivedListener)(json, "Success!");
}

SimpleAsyncRequest::SimpleAsyncRequest(const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data) {
    httpRequest.setUrl(url);
    httpRequest.setType(httpRequestType);
    httpRequest.setData(data);

    sendRequestInParallel();
    pollStateChanges();
}

SimpleAsyncRequest::SimpleAsyncRequest(
        const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
        Ngl::ManagedPointer<std::function<void(JSON::Value *response, const TextBuffer &errorMessage)> > onJsonResponseReceivedListener) :
            SimpleAsyncRequest(url, httpRequestType, data) {

    this->onJsonResponseReceivedListener = onJsonResponseReceivedListener;
}

SimpleAsyncRequest::SimpleAsyncRequest(
        const TextBuffer &url, HttpRequestType httpRequestType, const HttpRequestData &data,
        Ngl::ManagedPointer<std::function<void(std::vector<uint8_t> &response, const TextBuffer &errorMessage)> > onPlainResponseReceivedListener) :
            SimpleAsyncRequest(url, httpRequestType, data) {

    this->onPlainResponseReceivedListener = onPlainResponseReceivedListener;
}
