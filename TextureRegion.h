#pragma once

#include "TextUtils.h"

namespace Ngl {

	class Texture;

	class TextureRegion {

	public:

		Texture *texture;

		TextBuffer imageName;
		int x, y;
		int width, height;
		int originalWidth, originalHeight;
		int offsetX, offsetY;
		bool hasAlpha;

		float u1, v1, u2, v2;

		TextureRegion(
				Texture *texture,
				const TextBuffer &imageName,
				int x, int y,
				int width, int height,
				int originalWidth, int originalHeight,
				int offsetX, int offsetY,
				bool hasAlpha);
	};
}
