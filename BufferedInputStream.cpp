// TODO: throw exceptions if inputStream expired...

#include "BufferedInputStream.h"
#include "TextUtils.h"

#include <stdexcept>

bool Ngl::BufferedInputStream::discardBufferIfPossible() {
	if ((!marked) || markReadLimitExceeded()) {
		bufferedData.clear();
		currentByteIndex = 0;
		return true;
	}
	return false;
}

bool Ngl::BufferedInputStream::markReadLimitExceeded() {
	return markReadLimitExceedFlag = markReadLimitExceedFlag || (marked && markReadLimit && (currentByteIndex > markReadLimit));
}

bool Ngl::BufferedInputStream::hasData() {

	if ((currentByteIndex < bufferedData.size()) ||
		 inputStream->hasData()) {
		return true;
	}
	return false;
}

int32_t Ngl::BufferedInputStream::readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) {

	// Return remaining buffered data if any,
	int32_t remainingDataSize = bufferedData.size() - currentByteIndex;
	if (remainingDataSize) {
		outputVector.insert(outputVector.begin()+offsetInVector, bufferedData.begin()+currentByteIndex, bufferedData.end());
		currentByteIndex += remainingDataSize;
		return remainingDataSize;
	}

	// Discard buffer and read from input stream to output directly,
	discardBufferIfPossible();
	return inputStream->readData(outputVector, offsetInVector);
}

void Ngl::BufferedInputStream::mark(int32_t readLimit) {

	bufferedData = std::vector<uint8_t>(bufferedData.begin() + currentByteIndex, bufferedData.end());
	currentByteIndex = 0;

	marked = true;
	markReadLimit = readLimit;
	markReadLimitExceedFlag = false;
}

void Ngl::BufferedInputStream::reset() {

	if (!marked) throw std::runtime_error("Can't reset() without mark().");
	if (markReadLimitExceeded()) throw std::runtime_error("Read limit exceeded, can't reset.");

	marked = false;
	markReadLimit = 0;
	currentByteIndex = 0;
}

uint8_t Ngl::BufferedInputStream::readByte() {

	// Read from buffered data if possible,
	int32_t bufferedDataSize = bufferedData.size();
	if (currentByteIndex < bufferedDataSize) return bufferedData[currentByteIndex++];

	// Discard current buffer and read more data,
	discardBufferIfPossible();

	// Block until enough data is available,
	if (inputStream->hasData()) {
		while (!inputStream->readData(bufferedData));
		return bufferedData[currentByteIndex++];
	}
	throw std::runtime_error("Couldn't read a byte, no data available.");
}

int16_t Ngl::BufferedInputStream::readInt16() {

	// Read from buffered data if possible,
	int32_t remainingDataSize = bufferedData.size() - currentByteIndex;
	if (remainingDataSize > 1) {
		int16_t value =  bufferedData[currentByteIndex  ] |
						(bufferedData[currentByteIndex+1] << 8);
		currentByteIndex += 2;
		return value;
	}

	// Cache the remaining data,
	uint8_t remainingData[1];
	for (int32_t i=0; i<remainingDataSize; i++) remainingData[i] = bufferedData[currentByteIndex+i];

	// Discard current buffer and restore cached data,
	if (discardBufferIfPossible()) {
		for (int32_t i=0; i<remainingDataSize; i++) bufferedData.push_back(remainingData[i]);
	}

	// Read more data from the input stream,
	// Block until enough data is avaiable,
	while ((remainingDataSize < 2) && inputStream->hasData()) {
		remainingDataSize += inputStream->readData(bufferedData);
	}

	// Enough data available,
	if (remainingDataSize > 1) {
		int16_t value =  bufferedData[currentByteIndex  ] |
						(bufferedData[currentByteIndex+1] << 8);
		currentByteIndex += 2;
		return value;
	}

	throw std::runtime_error("Couldn't read a short, not enough data available.");
}

int32_t Ngl::BufferedInputStream::readInt32() {

	// Read from buffered data if possible,
	int32_t remainingDataSize = bufferedData.size() - currentByteIndex;
	if (remainingDataSize > 3) {
		int32_t value =  bufferedData[currentByteIndex  ]        |
						(bufferedData[currentByteIndex+1] << 8)  |
						(bufferedData[currentByteIndex+2] << 16) |
						(bufferedData[currentByteIndex+3] << 24);
		currentByteIndex += 4;
		return value;
	}

	// Cache the remaining data,
	uint8_t remainingData[3];
	for (int32_t i=0; i<remainingDataSize; i++) remainingData[i] = bufferedData[currentByteIndex+i];

	// Discard current buffer and restore cached data,
	if (discardBufferIfPossible()) {
		for (int32_t i=0; i<remainingDataSize; i++) bufferedData.push_back(remainingData[i]);
	}

	// Read more data from the input stream,
	// Block until enough data is avaiable,
	while ((remainingDataSize < 4) && inputStream->hasData()) {
		remainingDataSize += inputStream->readData(bufferedData);
	}

	// Enough data available,
	if (remainingDataSize > 3) {
		int32_t value =  bufferedData[currentByteIndex  ]        |
						(bufferedData[currentByteIndex+1] << 8)  |
						(bufferedData[currentByteIndex+2] << 16) |
						(bufferedData[currentByteIndex+3] << 24);
		currentByteIndex += 4;
		return value;
	}

	throw std::runtime_error("Couldn't read an int, not enough data available.");
}

void Ngl::BufferedInputStream::readBlock(std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t sizeBytes) {

	// Read from buffered data if possible,
	int32_t remainingDataSize = bufferedData.size() - currentByteIndex;
	if (remainingDataSize >= sizeBytes) {
		outputVector.insert(
				outputVector.begin()+offsetInVector,
				bufferedData.begin()+currentByteIndex, bufferedData.begin()+currentByteIndex+sizeBytes);
		currentByteIndex += sizeBytes;
		return;
	}

	// Cache the remaining data,
	uint8_t remainingData[remainingDataSize];
	for (int32_t i=0; i<remainingDataSize; i++) remainingData[i] = bufferedData[currentByteIndex+i];

	// Discard current buffer and restore cached data,
	if (discardBufferIfPossible()) {
		for (int32_t i=0; i<remainingDataSize; i++) bufferedData.push_back(remainingData[i]);
	}

	// Read more data from the input stream,
	// Block until enough data is avaiable,
	while ((remainingDataSize < sizeBytes) && inputStream->hasData()) {
		remainingDataSize += inputStream->readData(bufferedData);
	}

	// Enough data available,
	if (remainingDataSize >= sizeBytes) {
		outputVector.insert(
				outputVector.begin()+offsetInVector,
				bufferedData.begin()+currentByteIndex, bufferedData.begin()+currentByteIndex+sizeBytes);
		currentByteIndex += sizeBytes;
		return;
	}

	throw std::runtime_error(
			TextBuffer("Couldn't read ").
			append(sizeBytes).append(" bytes. Not enough data available.").getConstText());
}

bool Ngl::BufferedInputStream::sourceHasData() {
	return inputStream->hasData();
}

int32_t Ngl::BufferedInputStream::buffer() {
	return inputStream->readData(bufferedData);
}

int32_t Ngl::BufferedInputStream::getBufferedSize() {
	return bufferedData.size() - currentByteIndex;
}

void Ngl::BufferedInputStream::compact() {
	discardBufferIfPossible();
	bufferedData.shrink_to_fit();
	//std::vector<uint8_t>(bufferedData).swap(bufferedData);
}
