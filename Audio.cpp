#include "Audio.h"

#include "Nongl.h"
#include "Threads.h"

void Sound::play(Sound *sound, float delayMillis, float rate, float leftVolume, float rightVolume, Activity *activity) {

	if (!delayMillis) {
		sound->play();
	} else {
		class PlayLaterTask : public Runnable {
			Sound *sound;
			float rate, leftVolume, rightVolume;
		public:
			PlayLaterTask(Sound *sound, float rate, float leftVolume, float rightVolume, Activity *activity) : Runnable(activity) {
				this->sound = sound;
				this->rate = rate;
				this->leftVolume = leftVolume;
				this->rightVolume = rightVolume;
			}

			bool run(void *data) {
				sound->play(rate, leftVolume, rightVolume);
				return true;
			}
		};

		Ngl::shared_ptr<PlayLaterTask> playLaterTask(new PlayLaterTask(sound, rate, leftVolume, rightVolume, activity));
		Nongl::scheduler.schedule(playLaterTask, delayMillis);
	}
}
