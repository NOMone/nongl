#pragma once

#include "AbsoluteLayout.h"

class DraggableLayout : public AbsoluteLayout {

	bool blockChildrenTouchEvents;
	bool touched = false;
	float lastTouchX, lastTouchY;
	float initialLeft, initialBottom;

public:

	DraggableLayout(float x, float y, float width, float height);
	virtual bool onTouchEvent(const TouchEvent *event);
};
