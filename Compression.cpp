#include "Compression.h"

#include "MiniZ/MiniZ.h"
#include "TextUtils.h"
#include "SystemLog.h"

#include <stdexcept>
#include <cstring>

using namespace Ngl;

////////////////////////////////////////////////
// Miniz compression
////////////////////////////////////////////////

std::vector<uint8_t> Ngl::miniZCompress(const void *rawData, int32_t rawDataSize) {

	// Prepare output vector,
	std::vector<uint8_t> outputVector;
	outputVector.resize(rawDataSize+8);

	// Compress,
	long compressedDataSize = outputVector.size();
	int compressionStatus = mz_compress2(
			(unsigned char *) &outputVector[8],
			(unsigned long int *) &compressedDataSize,
			(unsigned char *) rawData,
			(unsigned long int) rawDataSize,
			9);

	// If compression failed,
	if (compressionStatus != MZ_OK) {
		throw std::runtime_error(TextBuffer("MiniZ compression failed (").append(compressionStatus).append(')').getConstText());
	}

	// Compact vector to remove unneeded extra bytes,
	outputVector.resize(compressedDataSize+8);

	// Set the first 4 bytes to uncompressed data size,
	for (int32_t i=0; i<4; i++) outputVector[i] = (rawDataSize >> (i<<3)) & 255;

	// Set the second 4 bytes to compressed data size,
	for (int32_t i=0; i<4; i++) outputVector[4+i] = (compressedDataSize >> (i<<3)) & 255;

	return outputVector;
}

void Ngl::miniZUncompress(const void *compressedBlock, std::vector<uint8_t> &outputVector, int32_t offsetInVector) {

	// Get compressed and uncompressed sizes,
	unsigned long uncompressedDataSize = 0;
	memcpy(&uncompressedDataSize, compressedBlock, 4);

	unsigned long compressedDataSize = 0;
	memcpy(&compressedDataSize, ((uint8_t *) compressedBlock) + 4, 4);

	unsigned char *compressedDataStart = ((uint8_t *) compressedBlock) + 8;

	// Allocate storage for uncompressed data,
	int uncompressionStatus;
	int32_t initialOutputVectorSize = (int32_t) outputVector.size();

	if (offsetInVector == initialOutputVectorSize) {

		// Uncompress into output vector directly,
		outputVector.resize(initialOutputVectorSize + uncompressedDataSize);

		// Uncompress,
		uncompressionStatus = mz_uncompress(
				&outputVector[initialOutputVectorSize],   // destination.
				&uncompressedDataSize,                    // destination size.
				compressedDataStart,                      // source.
				compressedDataSize);                      // source size.
	} else {

		// Uncompress into a temporary vector then insert it,
		std::vector<uint8_t> uncompressedData;
		uncompressedData.resize(uncompressedDataSize);

		// Uncompress,
		uncompressionStatus = mz_uncompress(
				&uncompressedData[0],                     // destination.
				&uncompressedDataSize,                    // destination size.
				compressedDataStart,                      // source.
				compressedDataSize);                      // source size.

		// Insert into output vector,
		outputVector.insert(outputVector.begin()+offsetInVector, uncompressedData.begin(), uncompressedData.end());
	}

	// If uncompression failed,
	if (uncompressionStatus != MZ_OK) {
		throw std::runtime_error(TextBuffer("MiniZ uncompression failed (").append(uncompressionStatus).append(')').getConstText());
	}
}

////////////////////////////////////////////////
// Compressing Input Stream
////////////////////////////////////////////////

CompressingInputStream::CompressingInputStream(ManagedPointer<InputStream> inputStream) : inputStream(inputStream) {}

bool CompressingInputStream::hasData() {
	if (inputStream.expired()) throw std::runtime_error("Attempt to check available data from expired input stream.");
	return inputStream->hasData();
}

int32_t CompressingInputStream::readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) {

	if (inputStream.expired()) throw std::runtime_error("Attempt to read data from expired input stream.");

	// Read all data possible,
	std::vector<uint8_t> rawData;
	inputStream->readAllData(rawData);

	if (!rawData.size()) return 0;

	// Compress data,
	std::vector<uint8_t> compressedData = miniZCompress(&rawData[0], rawData.size());

	// Insert compressed data into output vector,
	outputVector.insert(outputVector.begin()+offsetInVector, compressedData.begin(), compressedData.end());

	return compressedData.size();
}

////////////////////////////////////////////////
// UnbBuffered Compressing Output Stream
////////////////////////////////////////////////

class UnbufferedCompressingOutputStream : public OutputStream {
	ManagedPointer<OutputStream> outputStream;
public :
	UnbufferedCompressingOutputStream(ManagedPointer<OutputStream> outputStream);
	void writeData(const void *data, int32_t sizeBytes);
};

UnbufferedCompressingOutputStream::UnbufferedCompressingOutputStream(ManagedPointer<OutputStream> outputStream) : outputStream(outputStream) {}

void UnbufferedCompressingOutputStream::writeData(const void *data, int32_t sizeBytes) {

	if (outputStream.expired()) throw std::runtime_error("Attempt to write data to an expired output stream.");

	if (!sizeBytes) return ;

	// Compress and write data,
	std::vector<uint8_t> compressedData = miniZCompress(data, sizeBytes);
	outputStream->writeData(&compressedData[0], compressedData.size());
}

////////////////////////////////////////////////
// Compressing Output Stream
////////////////////////////////////////////////

CompressingOutputStream::CompressingOutputStream(Ngl::ManagedPointer<OutputStream> outputStream, int32_t maxBufferSizeBytes) :
		bufferedOutputStream(
				shared_ptr<OutputStream>(new UnbufferedCompressingOutputStream(outputStream)),
				maxBufferSizeBytes) {}

void CompressingOutputStream::writeData(const void *data, int32_t sizeBytes) {
	bufferedOutputStream.writeData(data, sizeBytes);
}

void CompressingOutputStream::flush() {
	bufferedOutputStream.flush();
}

////////////////////////////////////////////////
// Uncompressing Input Stream
////////////////////////////////////////////////

UncompressingInputStream::UncompressingInputStream(ManagedPointer<InputStream> inputStream) :
		bufferedInputStream(inputStream) {}

bool UncompressingInputStream::hasData() {

	// Check if there is enough data for a compressed block header,
	int32_t bufferedSize = bufferedInputStream.getBufferedSize();
	while ((bufferedSize < 8) && bufferedInputStream.sourceHasData()) {
		bufferedSize += bufferedInputStream.buffer();
	}

	if (bufferedSize < 8) return false;

	// Get compressed block size,
	// Peek at the compressed data size without removing them from the stream,
	bufferedInputStream.mark(8);
	bufferedInputStream.readInt32();
	int32_t compressedDataSize = bufferedInputStream.readInt32();
	bufferedInputStream.reset();

	// Make sure there is enough data for one block,
	int32_t blockSize = 8 + compressedDataSize;
	while ((bufferedSize < blockSize) && bufferedInputStream.sourceHasData()) {
		bufferedSize += bufferedInputStream.buffer();
	}

	if (bufferedSize < blockSize) return false;

	return true;
}

int32_t UncompressingInputStream::readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) {

	// Peek at the uncompressed and compressed data size without removing them from the stream,
	bufferedInputStream.mark(8);
	int32_t uncompressedDataSize = bufferedInputStream.readInt32();
	int32_t compressedDataSize = bufferedInputStream.readInt32();
	bufferedInputStream.reset();

	// Read compressed data block,
	std::vector<uint8_t> compressedData(compressedDataSize + 8);
	bufferedInputStream.readBlock(compressedData, 0, compressedDataSize + 8);

	// Uncompress block,
	miniZUncompress(&compressedData[0], outputVector, offsetInVector);

	return uncompressedDataSize;
}

////////////////////////////////////////////////
// Uncompressing Output Stream
////////////////////////////////////////////////

UncompressingOutputStream::UncompressingOutputStream(Ngl::ManagedPointer<OutputStream> outputStream) :
		outputStream(outputStream) {}

void UncompressingOutputStream::writeData(const void *data, int32_t sizeBytes) {

	int32_t currentDataOffset = 0;

	while (sizeBytes) {
		if (blockBytesRemaining) {
			if (sizeBytes < blockBytesRemaining) {

				// Just buffer the new data,
				buffer.insert(
						buffer.end(),
						((const char *) data) + currentDataOffset,
						((const char *) data) + currentDataOffset + sizeBytes);

				// Updates bytes remaining,
				blockBytesRemaining -= sizeBytes;

				// All data consumed, return.
				return ;
			} else {

				// Complete block, uncompress and write to output stream,
				buffer.insert(
						buffer.end(),
						((const char *) data) + currentDataOffset,
						((const char *) data) + currentDataOffset + blockBytesRemaining);
				currentDataOffset += blockBytesRemaining;

				// Uncompress and write data,
				std::vector<uint8_t> uncompressedData;
				miniZUncompress(&buffer[0], uncompressedData, 0);
				outputStream->writeData(&uncompressedData[0], uncompressedData.size());

				// Reset buffer and remaining bytes,
				buffer.clear();
				sizeBytes -= blockBytesRemaining;
				blockBytesRemaining = 0;
			}
		} else {

			// Expecting a new block,
			// Read compression header (uncompressed and compressed sizes),
			int32_t headerBytesRemaining = 8 - buffer.size();
			if (sizeBytes < headerBytesRemaining) {

				// Just insert whatever bytes available and return,
				buffer.insert(
						buffer.end(),
						((const char *) data) + currentDataOffset,
						((const char *) data) + currentDataOffset + sizeBytes);
				return ;
			} else {

				// Complete header size and start processing block,
				buffer.insert(
						buffer.end(),
						((const char *) data) + currentDataOffset,
						((const char *) data) + currentDataOffset + headerBytesRemaining);
				currentDataOffset += headerBytesRemaining;
				sizeBytes -= headerBytesRemaining;

				blockBytesRemaining = buffer[4] | (buffer[5]<<8) | (buffer[6]<<16) | (buffer[7]<<24);
			}
		}
	}
}

void UncompressingOutputStream::flush() {
	LOGE("Flushing and UncompressingOutputStream does nothing.");
}
