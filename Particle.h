#ifndef PARTICLE_H
#define PARTICLE_H

#include "TextureRegion.h"
#include "Sprite.h"
#include "SpriteBatch.h"

class Particle {
public:

	Sprite sprite;
	float delay;
	float lifeTime;
	float gravityX, gravityY;

	float velocityX, velocityY;
	float angularVelocity;

	Particle(
			const Ngl::TextureRegion *textureRegion,
			float lifeTime,
			float emmitterX, float emmitterY,
			float minVelocityX, float maxVelocityX,
			float minVelocityY, float maxVelocityY,
			float gravityX=0, float gravityY=0,
			float minAngularVelocity=0, float maxAngularVelocity=0,
			float minScale=1, float maxScale=1,
			float minDelay=0, float maxDelay=0);

	// returns true when dead,
	bool update(float elapsedTimeMillis);

	inline void draw(SpriteBatch *spriteBatch) { sprite.draw(spriteBatch); }
};

#endif
