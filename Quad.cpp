#include "Quad.h"
#include "TextureRegion.h"
#include "utils.h"

#include <math.h>
#include <string.h>

Quad::Quad() {

	textureRegion = 0;
	resetPoints();

	// Default parameters,
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->scaleX = 1;
	this->scaleY = 1;
	this->scaleZ = 1;
	this->rotation = 0;
	this->flipX = 0;
	this->flipY = 0;
	this->colorMask = 0xffffffff;
}

Quad::Quad(const Ngl::TextureRegion *textureRegion, const QuadPoint *points) {
	set(textureRegion, points);
}

void Quad::set(const Ngl::TextureRegion *textureRegion, const QuadPoint *points) {

	this->textureRegion = textureRegion;

	// Copy the points,
	if (points) {
		setPoints(points);
	} else  {
		this->points[TOP_RIGHT_POINT].x = textureRegion->width  * 0.5f;
		this->points[TOP_RIGHT_POINT].y = textureRegion->height * 0.5f;
		this->points[TOP_RIGHT_POINT].z = 0;
		this->points[TOP_RIGHT_POINT].colorMask = 0xffffffff;

		this->points[BOTTOM_RIGHT_POINT].x =  this->points[TOP_RIGHT_POINT].x;
		this->points[BOTTOM_RIGHT_POINT].y = -this->points[TOP_RIGHT_POINT].y;
		this->points[BOTTOM_RIGHT_POINT].z =  0;
		this->points[BOTTOM_RIGHT_POINT].colorMask = 0xffffffff;

		this->points[TOP_LEFT_POINT].x = -this->points[TOP_RIGHT_POINT].x;
		this->points[TOP_LEFT_POINT].y =  this->points[TOP_RIGHT_POINT].y;
		this->points[TOP_LEFT_POINT].z =  0;
		this->points[TOP_LEFT_POINT].colorMask = 0xffffffff;

		this->points[BOTTOM_LEFT_POINT].x = -this->points[TOP_RIGHT_POINT].x;
		this->points[BOTTOM_LEFT_POINT].y = -this->points[TOP_RIGHT_POINT].y;
		this->points[BOTTOM_LEFT_POINT].z =  0;
		this->points[BOTTOM_LEFT_POINT].colorMask = 0xffffffff;
	}

	// Default parameters,
	this->x = 0;
	this->y = 0;
	this->z = 0;
	this->scaleX = 1;
	this->scaleY = 1;
	this->scaleZ = 1;
	this->rotation = 0;
	this->flipX = 0;
	this->flipY = 0;
	this->colorMask = 0xffffffff;
}

void Quad::resetPoints() {
	for (int i=0; i<4; i++) {
		points[i].x = points[i].y = points[i].z = 0;
		points[i].colorMask = 0xffffffff;
	}
}

void Quad::setPoints(const QuadPoint *points) {
	for (int i=0; i<4; i++) {
		this->points[i] = points[i];
	}
}

void Quad::roll(float angleRadian) {
	for (int i=0; i<4; i++) points[i].roll(angleRadian);
}

void Quad::pitch(float angleRadian) {
	for (int i=0; i<4; i++) points[i].pitch(angleRadian);
}

void Quad::yaw(float angleRadian) {
	for (int i=0; i<4; i++) points[i].yaw(angleRadian);
}


#define UPPER_LEFT 0
#define BOTTOM_LEFT 1
#define UPPER_RIGHT 2
#define BOTTOM_RIGHT 3

#define UPPER_LEFT_X 0
#define UPPER_LEFT_Y 1
#define UPPER_LEFT_Z 2
#define UPPER_LEFT_TEX_X 3
#define UPPER_LEFT_TEX_Y 4
#define UPPER_LEFT_COLOR_MASK 5

#define BOTTOM_LEFT_X 6
#define BOTTOM_LEFT_Y 7
#define BOTTOM_LEFT_Z 8
#define BOTTOM_LEFT_TEX_X 9
#define BOTTOM_LEFT_TEX_Y 10
#define BOTTOM_LEFT_COLOR_MASK 11

#define UPPER_RIGHT_X 12
#define UPPER_RIGHT_Y 13
#define UPPER_RIGHT_Z 14
#define UPPER_RIGHT_TEX_X 15
#define UPPER_RIGHT_TEX_Y 16
#define UPPER_RIGHT_COLOR_MASK 17

#define BOTTOM_RIGHT_X 18
#define BOTTOM_RIGHT_Y 19
#define BOTTOM_RIGHT_Z 20
#define BOTTOM_RIGHT_TEX_X 21
#define BOTTOM_RIGHT_TEX_Y 22
#define BOTTOM_RIGHT_COLOR_MASK 23

void Quad::draw(float *vertexData, unsigned short *indices, int verticesOffset) {

	int flipsCount = 0;
	float cosAngle, sinAngle;

	if (!rotation) {

		// Vertices,
		if (flipX) {
			flipsCount++;
			vertexData[UPPER_RIGHT_X ] = x - (points[TOP_RIGHT_POINT   ].x * scaleX);
			vertexData[BOTTOM_RIGHT_X] = x - (points[BOTTOM_RIGHT_POINT].x * scaleX);
			vertexData[UPPER_LEFT_X  ] = x - (points[TOP_LEFT_POINT    ].x * scaleX);
			vertexData[BOTTOM_LEFT_X ] = x - (points[BOTTOM_LEFT_POINT ].x * scaleX);
		} else {
			vertexData[UPPER_RIGHT_X ] = x + (points[TOP_RIGHT_POINT   ].x * scaleX);
			vertexData[BOTTOM_RIGHT_X] = x + (points[BOTTOM_RIGHT_POINT].x * scaleX);
			vertexData[UPPER_LEFT_X  ] = x + (points[TOP_LEFT_POINT    ].x * scaleX);
			vertexData[BOTTOM_LEFT_X ] = x + (points[BOTTOM_LEFT_POINT ].x * scaleX);
		}

		if (flipY) {
			flipsCount++;
			vertexData[UPPER_RIGHT_Y ] = y - (points[TOP_RIGHT_POINT   ].y * scaleY);
			vertexData[BOTTOM_RIGHT_Y] = y - (points[BOTTOM_RIGHT_POINT].y * scaleY);
			vertexData[UPPER_LEFT_Y  ] = y - (points[TOP_LEFT_POINT    ].y * scaleY);
			vertexData[BOTTOM_LEFT_Y ] = y - (points[BOTTOM_LEFT_POINT ].y * scaleY);
		} else {
			vertexData[UPPER_RIGHT_Y ] = y + (points[TOP_RIGHT_POINT   ].y * scaleY);
			vertexData[BOTTOM_RIGHT_Y] = y + (points[BOTTOM_RIGHT_POINT].y * scaleY);
			vertexData[UPPER_LEFT_Y  ] = y + (points[TOP_LEFT_POINT    ].y * scaleY);
			vertexData[BOTTOM_LEFT_Y ] = y + (points[BOTTOM_LEFT_POINT ].y * scaleY);
		}

	} else {

		// Rotation is non-zero,
		// Vertices,
		cosAngle = cosf(rotation);
		sinAngle = sinf(rotation);

		// TODO: code size optimization (remove duplicates)...
		if (flipX) {
			float constant1 = scaleX * cosAngle;
			float constant2 = scaleY * sinAngle;
			flipsCount++;
			vertexData[UPPER_RIGHT_X ] = x - ((points[TOP_RIGHT_POINT   ].x * constant1) - (points[TOP_RIGHT_POINT   ].y * constant2));
			vertexData[BOTTOM_RIGHT_X] = x - ((points[BOTTOM_RIGHT_POINT].x * constant1) - (points[BOTTOM_RIGHT_POINT].y * constant2));
			vertexData[UPPER_LEFT_X  ] = x - ((points[TOP_LEFT_POINT    ].x * constant1) - (points[TOP_LEFT_POINT    ].y * constant2));
			vertexData[BOTTOM_LEFT_X ] = x - ((points[BOTTOM_LEFT_POINT ].x * constant1) - (points[BOTTOM_LEFT_POINT ].y * constant2));
		} else {
			float constant1 = scaleX * cosAngle;
			float constant2 = scaleY * sinAngle;
			vertexData[UPPER_RIGHT_X ] = x + ((points[TOP_RIGHT_POINT   ].x * constant1) - (points[TOP_RIGHT_POINT   ].y * constant2));
			vertexData[BOTTOM_RIGHT_X] = x + ((points[BOTTOM_RIGHT_POINT].x * constant1) - (points[BOTTOM_RIGHT_POINT].y * constant2));
			vertexData[UPPER_LEFT_X  ] = x + ((points[TOP_LEFT_POINT    ].x * constant1) - (points[TOP_LEFT_POINT    ].y * constant2));
			vertexData[BOTTOM_LEFT_X ] = x + ((points[BOTTOM_LEFT_POINT ].x * constant1) - (points[BOTTOM_LEFT_POINT ].y * constant2));
		}

		if (flipY) {
			float constant1 = scaleY * cosAngle;
			float constant2 = scaleX * sinAngle;
			flipsCount++;
			vertexData[UPPER_RIGHT_Y ] = y - ((points[TOP_RIGHT_POINT   ].y * constant1) + (points[TOP_RIGHT_POINT   ].x * constant2));
			vertexData[BOTTOM_RIGHT_Y] = y - ((points[BOTTOM_RIGHT_POINT].y * constant1) + (points[BOTTOM_RIGHT_POINT].x * constant2));
			vertexData[UPPER_LEFT_Y  ] = y - ((points[TOP_LEFT_POINT    ].y * constant1) + (points[TOP_LEFT_POINT    ].x * constant2));
			vertexData[BOTTOM_LEFT_Y ] = y - ((points[BOTTOM_LEFT_POINT ].y * constant1) + (points[BOTTOM_LEFT_POINT ].x * constant2));
		} else {
			float constant1 = scaleY * cosAngle;
			float constant2 = scaleX * sinAngle;
			vertexData[UPPER_RIGHT_Y ] = y + ((points[TOP_RIGHT_POINT   ].y * constant1) + (points[TOP_RIGHT_POINT   ].x * constant2));
			vertexData[BOTTOM_RIGHT_Y] = y + ((points[BOTTOM_RIGHT_POINT].y * constant1) + (points[BOTTOM_RIGHT_POINT].x * constant2));
			vertexData[UPPER_LEFT_Y  ] = y + ((points[TOP_LEFT_POINT    ].y * constant1) + (points[TOP_LEFT_POINT    ].x * constant2));
			vertexData[BOTTOM_LEFT_Y ] = y + ((points[BOTTOM_LEFT_POINT ].y * constant1) + (points[BOTTOM_LEFT_POINT ].x * constant2));
		}
	}

	vertexData[UPPER_RIGHT_Z ] = z + (points[TOP_RIGHT_POINT   ].z * scaleZ);
	vertexData[BOTTOM_RIGHT_Z] = z + (points[BOTTOM_RIGHT_POINT].z * scaleZ);
	vertexData[UPPER_LEFT_Z  ] = z + (points[TOP_LEFT_POINT    ].z * scaleZ);
	vertexData[BOTTOM_LEFT_Z ] = z + (points[BOTTOM_LEFT_POINT ].z * scaleZ);

	// Texture coordinates,
	vertexData[UPPER_LEFT_TEX_X ] = vertexData[BOTTOM_LEFT_TEX_X ] = textureRegion->u1;
	vertexData[UPPER_RIGHT_TEX_X] = vertexData[BOTTOM_RIGHT_TEX_X] = textureRegion->u2;
	vertexData[UPPER_LEFT_TEX_Y ] = vertexData[UPPER_RIGHT_TEX_Y ] = textureRegion->v1;
	vertexData[BOTTOM_LEFT_TEX_Y] = vertexData[BOTTOM_RIGHT_TEX_Y] = textureRegion->v2;

	// Color mask,
	if (colorMask == 0xffffffff) {
		((unsigned int *) vertexData)[UPPER_LEFT_COLOR_MASK  ] = points[TOP_LEFT_POINT    ].colorMask;
		((unsigned int *) vertexData)[BOTTOM_LEFT_COLOR_MASK ] = points[BOTTOM_LEFT_POINT ].colorMask;
		((unsigned int *) vertexData)[UPPER_RIGHT_COLOR_MASK ] = points[TOP_RIGHT_POINT   ].colorMask;
		((unsigned int *) vertexData)[BOTTOM_RIGHT_COLOR_MASK] = points[BOTTOM_RIGHT_POINT].colorMask;
	} else {
		((unsigned int *) vertexData)[UPPER_LEFT_COLOR_MASK  ] = multiplyColors(colorMask, points[TOP_LEFT_POINT    ].colorMask);
		((unsigned int *) vertexData)[BOTTOM_LEFT_COLOR_MASK ] = multiplyColors(colorMask, points[BOTTOM_LEFT_POINT ].colorMask);
		((unsigned int *) vertexData)[UPPER_RIGHT_COLOR_MASK ] = multiplyColors(colorMask, points[TOP_RIGHT_POINT   ].colorMask);
		((unsigned int *) vertexData)[BOTTOM_RIGHT_COLOR_MASK] = multiplyColors(colorMask, points[BOTTOM_RIGHT_POINT].colorMask);
	}

	// Indices,
	// Preserve counter-clockwise order,
	if (flipsCount&1) {
		indices[0] = verticesOffset + UPPER_RIGHT;
		indices[1] = indices[4] = verticesOffset + BOTTOM_RIGHT;
		indices[2] = indices[3] = verticesOffset + UPPER_LEFT;
		indices[5] = verticesOffset + BOTTOM_LEFT;
	} else {
		indices[0] = verticesOffset + UPPER_LEFT;
		indices[1] = indices[4] = verticesOffset + BOTTOM_LEFT;
		indices[2] = indices[3] = verticesOffset + UPPER_RIGHT;
		indices[5] = verticesOffset + BOTTOM_RIGHT;
	}
}

void Quad::drawWireFrame(SpriteBatch *spriteBatch, int color) {

	int flipsCount = 0;
	float cosAngle, sinAngle;

	float upperRightX, upperRightY;
	float bottomRightX, bottomRightY;
	float upperLeftX, upperLeftY;
	float bottomLeftX, bottomLeftY;

	if (!rotation) {

		// Vertices,
		if (flipX) {
			flipsCount++;
			upperRightX  = x - (points[TOP_RIGHT_POINT   ].x * scaleX);
			bottomRightX = x - (points[BOTTOM_RIGHT_POINT].x * scaleX);
			upperLeftX   = x - (points[TOP_LEFT_POINT    ].x * scaleX);
			bottomLeftX  = x - (points[BOTTOM_LEFT_POINT ].x * scaleX);
		} else {
			upperRightX  = x + (points[TOP_RIGHT_POINT   ].x * scaleX);
			bottomRightX = x + (points[BOTTOM_RIGHT_POINT].x * scaleX);
			upperLeftX   = x + (points[TOP_LEFT_POINT    ].x * scaleX);
			bottomLeftX  = x + (points[BOTTOM_LEFT_POINT ].x * scaleX);
		}

		if (flipY) {
			flipsCount++;
			upperRightY  = y - (points[TOP_RIGHT_POINT   ].y * scaleY);
			bottomRightY = y - (points[BOTTOM_RIGHT_POINT].y * scaleY);
			upperLeftY   = y - (points[TOP_LEFT_POINT    ].y * scaleY);
			bottomLeftY  = y - (points[BOTTOM_LEFT_POINT ].y * scaleY);
		} else {
			upperRightY  = y + (points[TOP_RIGHT_POINT   ].y * scaleY);
			bottomRightY = y + (points[BOTTOM_RIGHT_POINT].y * scaleY);
			upperLeftY   = y + (points[TOP_LEFT_POINT    ].y * scaleY);
			bottomLeftY  = y + (points[BOTTOM_LEFT_POINT ].y * scaleY);
		}

	} else {

		// Rotation is non-zero,
		// Vertices,
		cosAngle = cosf(rotation);
		sinAngle = sinf(rotation);

		if (flipX) {
			flipsCount++;
			upperRightX  = x - ((points[TOP_RIGHT_POINT   ].x * scaleX * cosAngle) - (points[TOP_RIGHT_POINT   ].y * scaleY * sinAngle));
			bottomRightX = x - ((points[BOTTOM_RIGHT_POINT].x * scaleX * cosAngle) - (points[BOTTOM_RIGHT_POINT].y * scaleY * sinAngle));
			upperLeftX   = x - ((points[TOP_LEFT_POINT    ].x * scaleX * cosAngle) - (points[TOP_LEFT_POINT    ].y * scaleY * sinAngle));
			bottomLeftX  = x - ((points[BOTTOM_LEFT_POINT ].x * scaleX * cosAngle) - (points[BOTTOM_LEFT_POINT ].y * scaleY * sinAngle));
		} else {
			upperRightX  = x + ((points[TOP_RIGHT_POINT   ].x * scaleX * cosAngle) - (points[TOP_RIGHT_POINT   ].y * scaleY * sinAngle));
			bottomRightX = x + ((points[BOTTOM_RIGHT_POINT].x * scaleX * cosAngle) - (points[BOTTOM_RIGHT_POINT].y * scaleY * sinAngle));
			upperLeftX   = x + ((points[TOP_LEFT_POINT    ].x * scaleX * cosAngle) - (points[TOP_LEFT_POINT    ].y * scaleY * sinAngle));
			bottomLeftX  = x + ((points[BOTTOM_LEFT_POINT ].x * scaleX * cosAngle) - (points[BOTTOM_LEFT_POINT ].y * scaleY * sinAngle));
		}

		if (flipY) {
			flipsCount++;
			upperRightY  = y - ((points[TOP_RIGHT_POINT   ].y * scaleY * cosAngle) + (points[TOP_RIGHT_POINT   ].x * scaleX * sinAngle));
			bottomRightY = y - ((points[BOTTOM_RIGHT_POINT].y * scaleY * cosAngle) + (points[BOTTOM_RIGHT_POINT].x * scaleX * sinAngle));
			upperLeftY   = y - ((points[TOP_LEFT_POINT    ].y * scaleY * cosAngle) + (points[TOP_LEFT_POINT    ].x * scaleX * sinAngle));
			bottomLeftY  = y - ((points[BOTTOM_LEFT_POINT ].y * scaleY * cosAngle) + (points[BOTTOM_LEFT_POINT ].x * scaleX * sinAngle));
		} else {
			upperRightY  = y + ((points[TOP_RIGHT_POINT   ].y * scaleY * cosAngle) + (points[TOP_RIGHT_POINT   ].x * scaleX * sinAngle));
			bottomRightY = y + ((points[BOTTOM_RIGHT_POINT].y * scaleY * cosAngle) + (points[BOTTOM_RIGHT_POINT].x * scaleX * sinAngle));
			upperLeftY   = y + ((points[TOP_LEFT_POINT    ].y * scaleY * cosAngle) + (points[TOP_LEFT_POINT    ].x * scaleX * sinAngle));
			bottomLeftY  = y + ((points[BOTTOM_LEFT_POINT ].y * scaleY * cosAngle) + (points[BOTTOM_LEFT_POINT ].x * scaleX * sinAngle));
		}
	}

	// Indices,
	// Preserve counter-clockwise order,
	if (flipsCount&1) {
		// Face 1,
		spriteBatch->drawLine(upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, color);
		spriteBatch->drawLine(upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, color);

		// Face 2,
		spriteBatch->drawLine(upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, color);
	} else {
		// Face 1,
		spriteBatch->drawLine(upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, color);
		spriteBatch->drawLine(upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, upperLeftX, upperLeftY, points[TOP_LEFT_POINT].z, color);

		// Face 2,
		spriteBatch->drawLine(upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, color);
		spriteBatch->drawLine(bottomLeftX, bottomLeftY, points[BOTTOM_LEFT_POINT].z, bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, color);
		spriteBatch->drawLine(bottomRightX, bottomRightY, points[BOTTOM_RIGHT_POINT].z, upperRightX, upperRightY, points[TOP_RIGHT_POINT].z, color);
	}
}

