#pragma once

#include "View.h"
#include "NinePatchSprite.h"

class NinePatchView : public View {

	NinePatchSprite sprite;
	float rotationPivotXDisplacement, rotationPivotYDisplacement;

	void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool transparent);

public:

	inline NinePatchView() {}
	inline NinePatchView(const NinePatchData *ninePatchData) { init(ninePatchData); }

	inline void init(const NinePatchData *ninePatchData) {
		this->rotationPivotXDisplacement = this->rotationPivotYDisplacement = 0;
		sprite.set(ninePatchData);
	}

	inline float getWidth() { return sprite.getWidth(); }
	inline float getHeight() { return sprite.getHeight(); }
	inline float getScaledWidth() { return sprite.getScaledWidth(); }
	inline float getScaledHeight() { return sprite.getScaledHeight(); }
	inline float getLeft() { return sprite.getLeft(); }
	inline float getBottom() { return sprite.getBottom(); }
	inline float getRight() { return sprite.getRight(); }
	inline float getTop() { return sprite.getTop();  }
	inline float getCenterX() { return sprite.getCenterX();  }
	inline float getCenterY() { return sprite.getCenterY();  }

	inline void setWidth(float width) { sprite.setWidth(width); }
	inline void setHeight(float height) { sprite.setHeight(height); }
	inline void setLeft(float left) { sprite.setLeft(left); }
	inline void setBottom(float bottom) { sprite.setBottom(bottom); }
	inline void setRight(float right) { sprite.setRight(right); }
	inline void setTop(float top) { sprite.setTop(top); }
	inline void setCenterX(float centerX) { sprite.setCenterX(centerX); }
	inline void setCenterY(float centerY) { sprite.setCenterY(centerY); }
	inline void setCenter(float centerX, float centerY) { sprite.setCenter(centerX, centerY); }
	inline void setScale(float scaleX, float scaleY) { sprite.scaleX = scaleX; sprite.scaleY = scaleY; }
	inline void setRotation(float angleRadians) { sprite.setRotation(angleRadians); }
	inline void setRotationPivotDisplacementFromCenter(float xDisplacement, float yDisplacement) {
		this->rotationPivotXDisplacement = xDisplacement;
		this->rotationPivotYDisplacement = yDisplacement;
	}
	inline void setColorMask(int colorMask) { sprite.setColorMask(colorMask); }

	inline void adjustDepths(float maxZ, float minZ) { sprite.setDepth(maxZ); }

	inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, false); }
	inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }

	inline void attachToAnimation(Animation *animation) { sprite.attachToAnimation(animation); }

	inline Ngl::TextureRegion *getPatchRegion(int column, int row) { return sprite.getPatchRegion(column, row); }
};
