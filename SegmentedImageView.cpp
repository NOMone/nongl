#include "SegmentedImageView.h"

SegmentedImageView::SegmentedImageView(
		int columnsCount, int rowsCount,
		const char *regionPrefix,
		float imageWidth, float imageHeight) {

	this->x = this->y = 0;
	this->scaleX = this->scaleY = 1;
	this->rotation = 0;
	this->rotationPivotXDisplacement = this->rotationPivotYDisplacement = 0;
	this->modified = false;
	sprite.set(columnsCount, rowsCount, regionPrefix, imageWidth, imageHeight);
}

void SegmentedImageView::drawOpaqueParts(SpriteBatch *spriteBatch) {
	if (modified) { updateSprite(); modified = false; }
	sprite.drawOpaqueParts(spriteBatch);
}

void SegmentedImageView::drawTransparentParts(SpriteBatch *spriteBatch) {
	if (modified) { updateSprite(); modified = false; }
	sprite.drawTransparentParts(spriteBatch);
}

void SegmentedImageView::drawAllInOrder(SpriteBatch *spriteBatch) {
	if (modified) { updateSprite(); modified = false; }
	sprite.drawAllInOrder(spriteBatch);
}

void SegmentedImageView::updateSprite() {

	sprite.setScale(scaleX, scaleY);
	sprite.setRotation(rotation);

	if (rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(rotation);
		float sinAngle = sinf(rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		sprite.setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);
	} else {
		sprite.setLeft(x);
		sprite.setBottom(y);
	}
}

void SegmentedImageView::attachToAnimation(Animation *animation) {

	animation->setOnUpdatedFlagTarget(&modified);

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += x;
				currentComponent->setTargetAddress(&x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += y;
				currentComponent->setTargetAddress(&y);
				break;

			case AnimationTarget::YAW:
				currentComponent->setTargetAddress(&rotation);
				break;

			default:
				break;
		}
	}
}
