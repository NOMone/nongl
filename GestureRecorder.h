#ifndef GESTURE_RECORDER_H
#define GESTURE_RECORDER_H

#include "Threads.h"
#include "GestureListener.h"
#include <vector>

namespace Ngl {
	class File;
}

//////////////////////////////
// Gestures
//////////////////////////////

class Gesture
{
public:
	double time;

	inline Gesture(double time) : time(time) {}
	inline virtual ~Gesture() {}

	virtual void scale(float scaleX, float scaleY) = 0;
	virtual void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) = 0;
	virtual void toString(char *outString) = 0;
};

class TouchDownGesture : public Gesture
{
public:
	int pointerId;
	float x, y;

	inline TouchDownGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), x(touchEvent->x), y(touchEvent->y) {}

	inline TouchDownGesture(double time, int pointerId, float x, float y) :
			Gesture(time), pointerId(pointerId), x(x), y(y) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsTouchDown(pointerId, x, y);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class TouchDragGesture : public Gesture
{
public:
	int pointerId;
	float downX, downY;
	float x, y;
	float maxDraggedDistanceCms;

	inline TouchDragGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), downX(touchEvent->downX), downY(touchEvent->downY), x(touchEvent->x), y(touchEvent->y), maxDraggedDistanceCms(touchEvent->maxDraggedDistanceCms) {}

	inline TouchDragGesture(double time, int pointerId, float downX, float downY, float x, float y, float maxDraggedDistanceCms) :
			Gesture(time), pointerId(pointerId), downX(downX), downY(downY), x(x), y(y), maxDraggedDistanceCms(maxDraggedDistanceCms) {}

	inline void scale(float scaleX, float scaleY) { downX*=scaleX; downY*=scaleY; x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsTouchDrag(pointerId, downX, downY, x, y, maxDraggedDistanceCms);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class TouchDragEndGesture : public Gesture
{
public:
	int pointerId;
	float downX, downY;
	float upX, upY;
	float maxDraggedDistanceCms;

	inline TouchDragEndGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), downX(touchEvent->downX), downY(touchEvent->downY), upX(touchEvent->x), upY(touchEvent->y), maxDraggedDistanceCms(touchEvent->maxDraggedDistanceCms) {}

	inline TouchDragEndGesture(double time, int pointerId, float downX, float downY, float upX, float upY, float maxDraggedDistanceCms) :
			Gesture(time), pointerId(pointerId), downX(downX), downY(downY), upX(upX), upY(upY), maxDraggedDistanceCms(maxDraggedDistanceCms) {}

	inline void scale(float scaleX, float scaleY) { downX*=scaleX; downY*=scaleY; upX*=scaleX; upY*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsTouchDragEnd(pointerId, downX, downY, upX, upY, maxDraggedDistanceCms);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class TouchUpGesture : public Gesture
{
public:
	int pointerId;
	float x, y;
	float maxDraggedDistanceCms;

	inline TouchUpGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), x(touchEvent->x), y(touchEvent->y), maxDraggedDistanceCms(touchEvent->maxDraggedDistanceCms) {}

	inline TouchUpGesture(double time, int pointerId, float x, float y, float maxDraggedDistanceCms) :
			Gesture(time), pointerId(pointerId), x(x), y(y), maxDraggedDistanceCms(maxDraggedDistanceCms) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsTouchUp(pointerId, x, y, maxDraggedDistanceCms);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class ClickGesture : public Gesture
{
public:
	int pointerId;
	float x, y;

	inline ClickGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), x(touchEvent->x), y(touchEvent->y) {}

	inline ClickGesture(double time, int pointerId, float x, float y) :
			Gesture(time), pointerId(pointerId), x(x), y(y) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsClick(pointerId, x, y);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class FlickGesture : public Gesture
{
public:
	int pointerId;
	float x, y;
	float xMagnitude, yMagnitude;

	inline FlickGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), pointerId(touchEvent->pointerId), x(touchEvent->x), y(touchEvent->y), xMagnitude(touchEvent->xMagnitude), yMagnitude(touchEvent->yMagnitude) {}

	inline FlickGesture(double time, int pointerId, float x, float y, float xMagnitude, float yMagnitude) :
			Gesture(time), pointerId(pointerId), x(x), y(y), xMagnitude(xMagnitude), yMagnitude(yMagnitude) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsFlick(pointerId, x, y, xMagnitude, yMagnitude);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class ZoomGesture : public Gesture
{
public:
	float x, y;
	float ratio;

	inline ZoomGesture(double time, const TouchEvent *touchEvent) :
			Gesture(time), x(touchEvent->x), y(touchEvent->y), ratio(touchEvent->ratio) {}

	inline ZoomGesture(double time, float x, float y, float ratio) :
			Gesture(time), x(x), y(y), ratio(ratio) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsZoom(x, y, ratio);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

class ZoomEndGesture : public Gesture
{
public:
	float x, y;
	float ratio;

	inline ZoomEndGesture(double time, const TouchEvent *touchEvent) :
				Gesture(time), x(touchEvent->x), y(touchEvent->y), ratio(touchEvent->ratio) {}

	inline ZoomEndGesture(double time, float x, float y, float ratio) :
			Gesture(time), x(x), y(y), ratio(ratio) {}

	inline void scale(float scaleX, float scaleY) { x*=scaleX; y*=scaleY; }

	inline void setAndDispatchEvent(TouchEvent *touchEvent, GestureListener *gestureListener) {
		touchEvent->setAsZoomEnd(x, y, ratio);
		gestureListener->onTouchEvent(touchEvent);
	}
	void toString(char *outString);
};

//////////////////////////////
// Gesture recorder
//////////////////////////////

class GestureRecorder : public GestureListener {

	std::vector<Gesture *> recordedGestures;

	double recordingTotalTimeMillis;
	double recordingLastTimeMillis;
	bool recordingRunning;

	void updateRecording();

	GestureListener *gestureListener;
	double replayingTotalTimeMillis;
	int gestureToReplayIndex;
	bool replayingRunning;
	Runnable *onReplayEndListener;
	void *onReplayEndListenerData;

	TouchEvent touchEvent;

public:

	GestureRecorder();
	~GestureRecorder();

	void removeRecordedGestures();
	void resetRecording();
	void startRecording();
	void pauseRecording();
	void saveRecording(const Ngl::File &file);
	void scaleRecording(float scaleX, float scaleY);

	static Gesture *createGesture(
			char *gestureType,
			float time,
			int pointerId,
			float x, float y,
			float downX, float downY,
			float upX, float upY,
			float maxDraggedDistanceCms,
			float xMagnitude, float yMagnitude,
			float ratio);


	bool loadRecording(const Ngl::File &file);
	inline void setGestureListener(GestureListener *gestureListener) { this->gestureListener = gestureListener; }
	void resetReplaying();
	void startReplaying();
	void updateReplaying(float elapsedTimeMillis);
	void pauseReplaying();
	inline bool isReplaying() { return replayingRunning; }

	inline void setOnReplayEndListener(Runnable *onReplayEndListener, void *onReplayEndListenerData=0) {
		this->onReplayEndListener = onReplayEndListener;
		this->onReplayEndListenerData = onReplayEndListenerData;
	}

	virtual bool onTouchEvent(const TouchEvent *event);
};

#endif
