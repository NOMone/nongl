#pragma once

#include "TextUtils.h"

#include <stdint.h>

namespace Ngl {

	class Texture;
	class Vertex;
	class MestBatch;

	class Material {
		TextBuffer name;
	public:
		Material(TextBuffer name) : name(name) {}
		virtual ~Material() {}

		virtual Material *clone() const =0;

		void setName(const TextBuffer &name) { this->name = name; }
		const TextBuffer &getName() { return name; }

		virtual void setTexture(Texture *texture) {}
		virtual void setColor(int32_t color) {}

		virtual int32_t getFormatedVerticesDataSizeBytes(int32_t verticesCount)=0;
		virtual void getFormatedVerticesData(const Vertex *vertices, int32_t vertexSizeBytes, int32_t verticesCount, uint8_t *formattedVerticesData)=0;

		virtual void prepareMaterial(const uint8_t *verticesData) {}
	};
}
