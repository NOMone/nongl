#pragma once

#include <vector>
#include <stdint.h>

class TextBuffer;

namespace Ngl {
	class Texture;
	class TextureRegion;
	class File;
	enum class FileLocation;
}

class ViewPort {
public:

	/** Size and location of the current viewport in terms of design units. */
	int x, y;
	int width, height;
};

class ViewPortTransformation {
public:

	/** Scaling and translation of renderable objects within the viewport. */
	float transformationX, transformationY, transformationZ;
	float transformationWidth, transformationHeight;
};

class Clipping {
public:

	/** Size and location of the clipping window in terms transformed viewport. */
	float x, y;
	float width, height;
};

class DisplayManager {

	friend class Nongl;

	// So this can never be created manually,
	DisplayManager();
	static DisplayManager *singleton;

	int maximumLoadedTexturesCount;
	int loadedTexturesCount;

	std::vector<Ngl::Texture *> textures;

	bool etc1SupportTested;
	bool GLEW_OES_compressed_ETC1_RGB8_texture_supported;

	bool pvrtcSupportTested;
	bool GL_IMG_texture_compression_pvrtc_supported;

	/** Actual render area dimensions. */
	int displayWidth, displayHeight;

	std::vector<ViewPort> viewPortStack;
	std::vector<ViewPortTransformation> viewPortTransformationStack;
	std::vector<Clipping> clippingStack, physicalClippingStack;

	bool entirelyClipped;

	void applyViewPort();
	void destroyAllTextures();
	void loadTexture(Ngl::Texture *texture);

	int32_t getTextureRegionIndex(const char *name, int32_t *insertIndex=0);

public:

	/** Virtual dimensions that abstract the actual display dimensions. */
	float designWidth, designHeight;

	/** Library users should never alter these. */
	ViewPort viewPort;
	ViewPortTransformation viewPortTransformation;
	Clipping clipping, physicalClipping;

	std::vector<Ngl::TextureRegion *> textureRegions;

	~DisplayManager();

	static DisplayManager *getSingleton();

	void setDesignDimensions(float width, float height);

	void onDisplayChanged(int displayWidth, int displayHeight);

	void setViewPort(int x, int y, int width, int height);
	void pushViewPort();
	void popViewPort();

	void setViewPortTransformation(float x, float y, float z, float width, float height);
	void resetViewPortTransformation();
	void pushViewPortTransformation();
	void popViewPortTransformation();

	void setClipping(float x, float y, float width, float height, bool withinCurrentClipping=true);
	void pushClipping();
	void popClipping();
	void applyClipping(bool withinCurrentClipping);

	inline bool isEntirelyClipped() { return entirelyClipped; }
	inline float getDisplayWidth() { return displayWidth; }
	inline float getDisplayHeight() { return displayHeight; }

	void getDesignCoords(float inTransformedX, float inTransformedY, float *outDesignX, float *outDesignY);
	void getScreenCoords(float inTransformedX, float inTransformedY, float *outScreenX, float *outScreenY);
	float pixelsToCms(float lengthPixels);

	// TODO: should be marked as private. No library users should ever use this.
	Ngl::Texture *addTexture(const TextBuffer &textureFileName, Ngl::FileLocation fileLocation, int textureMinFilterType, int textureMagFilterType);
	Ngl::Texture *createTexture(int width, int height, int textureMinFilterType, int textureMagFilterType);
    void deleteTexture(Ngl::Texture *texture);
	int32_t getTexturesCount();
	Ngl::Texture *getTexture(int32_t textureIndex);
	void setMaximumLoadedTexturesCount(int maximumLoadedTexturesCount);
	int32_t loadTextureAtlas(const Ngl::File &packFile);
	bool releaseLeastRecentlyUsedTexture();
	int32_t getTextureRegionsCount();
	const Ngl::TextureRegion *getTextureRegion(int index);
	const Ngl::TextureRegion *getTextureRegion(const char *name, bool verbose=true);
	void addTextureRegion(Ngl::TextureRegion *textureRegion);

	int prepareTexture(Ngl::Texture *texture);

	bool isEtc1Supported();
	bool isPvrtcSupported();
};
