#include "FrameBuffer.h"
#include "Texture.h"
#include "Nongl.h"
#include "SystemLog.h"
#include "DisplayManager.h"
#include "glWrapper.h"

FrameBuffer::FrameBuffer() {
	// The default frame buffer,
}

void FrameBuffer::create(Ngl::Texture *texture, int width, int height, bool createDepthBuffer) {

	DisplayManager *displayManager = DisplayManager::getSingleton();

	// Set texture,
	if (!texture) texture = displayManager->createTexture(width, height, GL_LINEAR, GL_LINEAR);
	this->texture = texture;
	displayManager->prepareTexture(texture);

	// Dimensions,
	this->width = texture->getWidth();
	this->height = texture->getHeight();

	// Store the currently set frame buffer object,
	FrameBuffer *oldFrameBuffer = Nongl::getBoundFrameBuffer();

	// Create frame buffer,
	glGenFramebuffers(1, &id);

	// Attach texture to color attachment 0,
	Nongl::bindFrameBuffer(this);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->getId(), 0);
    
	// Create depth buffer if required,
	depthBufferCreated = createDepthBuffer;
	if (createDepthBuffer) {

		// Create render buffer,
		glGenRenderbuffers(1, &depthBufferId);
		glBindRenderbuffer(GL_RENDERBUFFER, depthBufferId);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, this->width, this->height);

		// Attach the render buffer to depth attachment point,
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBufferId);

		// Clear the depth buffer,
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	// check FBO status
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if(status != GL_FRAMEBUFFER_COMPLETE) {
		LOGE("Failed to create frame buffer: %08x", status);
	}

	// Restore original render target,
	Nongl::bindFrameBuffer(oldFrameBuffer);
}

FrameBuffer::~FrameBuffer() {
	if (depthBufferCreated) glDeleteRenderbuffers(1, &depthBufferId);
	if (id != 0) glDeleteFramebuffers(1, &id);
}
