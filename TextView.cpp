#include "TextView.h"
#include "utils.h"
#include "TextUtils.h"

#include <string.h>

TextView::TextView() {
	set(0, 0, 100, 100);
}

TextView::TextView(float x, float y, float width, float height, Gravity::Value gravity, int textColor) : textSprite() {
	set(x, y, width, height, gravity, textColor);
}

TextView::TextView(const char *text, float maxWidth, float textScale, Gravity::Value gravity) :
		TextView(0 ,0, TextSprite::getTextWidth(text, maxWidth, textScale), 10, gravity, 0xffffffff) {
	setText(text);
	setTextScale(textScale, textScale);
	setHeight(getTextHeight());
}

TextView *TextView::clone() {

	TextView *newTextView = new TextView();

	newTextView->setText(&text[0]);

	newTextView->verticalGravity = verticalGravity;
	newTextView->horizontalGravity = horizontalGravity;

	newTextView->x = x;
	newTextView->y = y;
	newTextView->z = z;
	newTextView->width = width;
	newTextView->height = height;
	newTextView->scaleX = scaleX;
	newTextView->scaleY = scaleY;
	newTextView->textScaleX = textScaleX;
	newTextView->textScaleY = textScaleY;

	newTextView->textColor = textColor;
	newTextView->colorMask = colorMask;

	return newTextView;
}

void TextView::wrapText(float maxWidth) {
	setWidth(TextSprite::getTextWidth(getText(), maxWidth, textScaleX, getFont()));
	setHeight(getTextHeight());
}

void TextView::set(float x, float y, float width, float height, Gravity::Value gravity, int textColor) {
	this->x = x;
	this->y = y;
	this->z = 0;
	this->width = width;
	this->height = height;
	setGravity(gravity);
	this->textColor = textColor;
	this->colorMask = 0xffffffff;

	this->scaleX = this->scaleY = 1;
	this->textScaleX = this->textScaleY = 1;
	this->textLength = 0;
}

void TextView::setText(const char *text) {

	if (!text) {
		textLength = 0;
		return ;
	}

	// Allocate memory for new text and copy it,
	textLength = (int32_t) strlen(text);
	this->text.reserve(textLength+1);
	strcpy(&(this->text[0]), text);
}

void TextView::drawTransparentParts(SpriteBatch *spriteBatch) {

	float top;
	int linesCount;

	switch (verticalGravity) {
		case VerticalGravity::CENTER:
			linesCount = getTextLinesCount();
			top = getTop() - (getScaledHeight() - (linesCount * getFont()->lineHeight * scaleY * textScaleY)) * 0.5f;
			break;

		case VerticalGravity::BOTTOM:
			linesCount = getTextLinesCount();
			top = getTop() - (getScaledHeight() - (linesCount * getFont()->lineHeight * scaleY * textScaleY));
			break;

		default:
			top = getTop();
			break;
	}

	drawText(top, spriteBatch);
}

int TextView::drawText(float topY, SpriteBatch *spriteBatch, bool draw) {

	if (!textLength)
		return 0;

	textSprite.colorMask = multiplyColors(textColor, colorMask);
	textSprite.scaleX = scaleX * textScaleX;
	textSprite.scaleY = scaleY * textScaleY;
	textSprite.z = getDepth();

	// Support multiple lines based on '\n' chars,
	int currentChar = 0;
	int totalLinesCount = 0;
	while (textLength > currentChar) {

		int currentLineStartIndex = currentChar;
		while(text[currentChar] && (text[currentChar] != '\n')) {
			currentChar++;
		}

		char oldCurrentChar = text[currentChar];
		text[currentChar] = 0;
		int linesCount;
		if (draw) {
			linesCount = breakTextLinePreservingWordsAndDraw(&text[currentLineStartIndex], topY, spriteBatch);
		} else {
			linesCount = breakTextLinePreservingWordsAndGetLinesCount(&text[currentLineStartIndex]);
		}
		text[currentChar] = oldCurrentChar;

		if ((!linesCount) && (oldCurrentChar == '\n')) linesCount++;

		currentChar++;
		topY -= textSprite.getScaledHeight() * linesCount;
		totalLinesCount += linesCount;
	}

	return totalLinesCount;
}

void TextView::getFormattedText(const char *text, std::vector<char> &formattedText) {

	int currentLineStartIndex = 0;
	int lineEndCharIndex;
	do {
		int lineLength = textSprite.getLengthToLastFittingWordEnd(&text[currentLineStartIndex], width / textScaleX);
		if (!lineLength) {
			// Included for a day when empty spaces are omitted.
			formattedText.push_back(0);
			return;
		}

		// Copy text,
		for (int i=0; i<lineLength; i++) formattedText.push_back(text[currentLineStartIndex+i]);

		lineEndCharIndex = currentLineStartIndex + lineLength;
		currentLineStartIndex = lineEndCharIndex;

		if (text[lineEndCharIndex]) {
			if (text[lineEndCharIndex] != '\n') {
				formattedText.push_back('\n');
			}
		} else {
			formattedText.push_back(0);
		}
	} while (text[lineEndCharIndex]);
}

int TextView::breakTextLinePreservingWordsAndGetLinesCount(char *text) {

	int linesCount = 0;
	int currentLineStartIndex = 0;
	int lineEndCharIndex;
	do {
		int lineLength = textSprite.getLengthToLastFittingWordEnd(&text[currentLineStartIndex], width / textScaleX);
		if (!lineLength) return linesCount;

		lineEndCharIndex = currentLineStartIndex + lineLength;
		currentLineStartIndex = lineEndCharIndex;
		linesCount++;
	} while (text[lineEndCharIndex]);
	return linesCount;
}

int TextView::breakTextLinePreservingWordsAndDraw(char *text, float topY, SpriteBatch *spriteBatch) {

	textSprite.scaleX = scaleX * textScaleX;
	textSprite.scaleY = scaleY * textScaleY;

	int linesCount = 0;
	int currentLineStartIndex = 0;
	int lineEndCharIndex;
	TextBuffer trimmedText;
	do {
		int lineLength = textSprite.getLengthToLastFittingWordEnd(&text[currentLineStartIndex], width / textScaleX);
		if (!lineLength) return linesCount;

		lineEndCharIndex = currentLineStartIndex + lineLength;
		char oldEndChar = text[lineEndCharIndex];
		text[lineEndCharIndex] = 0;
		trimmedText.clear().append(&text[currentLineStartIndex]);
		text[lineEndCharIndex] = oldEndChar;

		trimmedText.trimEnd();
		textSprite.setText(trimmedText.getText());

		// Set horizontal alignment,
		switch (horizontalGravity) {
			case HorizontalGravity::CENTER:
				textSprite.setCenterX(getCenterX());
				break;
			case HorizontalGravity::RIGHT:
				textSprite.setRight(getRight());
				break;
			default:
				textSprite.setLeft(getLeft());
				break;
		}

		// Draw,
		textSprite.setTop(topY);
		textSprite.draw(spriteBatch);

		topY -= textSprite.getScaledHeight();
		currentLineStartIndex = lineEndCharIndex;
		linesCount++;
	} while (text[lineEndCharIndex]);

	return linesCount;
}
