#ifndef TEXT_UTILS_H
#define TEXT_UTILS_H

#include <vector>
#include <stdint.h>

////////////////////////////////////////////////
// Text buffer
////////////////////////////////////////////////

namespace Ngl {
	class InputStream;
	class BufferedInputStream;
}

class TextBuffer {
	std::vector<uint8_t> text;
public:

	inline TextBuffer(int capacity=0) { text.reserve(capacity+1); text.push_back(0); }

	inline TextBuffer(const TextBuffer &source) {
		text.reserve(source.text.size());
		text.push_back(0);
		append(source);
	}

	TextBuffer(const TextBuffer &source, int32_t maxLength);
	TextBuffer(const TextBuffer &source, int32_t sourceStartIndex, int32_t maxLength);

	inline TextBuffer(char character) {
		text.push_back(0);
		append(character);
	}

	inline TextBuffer(const char *source) {
		text.push_back(0);
		append(source);
	}

	// Use only when you know that the stream doesn't contain any nulls.
	TextBuffer(Ngl::InputStream &inputStream);

	// Use only when you know that the stream doesn't contain any nulls up to "length" characters.
	inline TextBuffer(Ngl::BufferedInputStream &bufferedInputStream, int32_t length) {
		text.reserve(length+1);
		text.push_back(0);
		append(bufferedInputStream, length);
	}

	// TODO: implement move constructor.

    bool operator==(const TextBuffer &rightHandSide) const;
    bool operator==(const char *rightHandSide) const;
    bool operator!=(const TextBuffer &rightHandSide) const;
    bool operator!=(const char *rightHandSide) const;

    bool operator> (const TextBuffer &rightHandSide) const;
    bool operator>=(const TextBuffer &rightHandSide) const;
	bool operator< (const TextBuffer &rightHandSide) const;
    bool operator<=(const TextBuffer &rightHandSide) const;

	// TODO: overload "+" operator instead of using append. keep append though.

    char &operator[](int32_t index) { return (char &) text[index];	}

    inline operator char *() { return getText(); }
    inline operator const char *() const { return getConstText(); }

	inline TextBuffer &append(char character) { text[text.size()-1] = character; text.push_back(0); return *this; }
	TextBuffer &append(const char *string);
	TextBuffer &append(const char *string, int length); // Use only when you know that "string" doesn't contain any nulls up to "length" characters.
	TextBuffer &append(Ngl::BufferedInputStream &bufferedInputStream, int32_t length); // Use only when you know that the stream doesn't contain any nulls.

	inline TextBuffer &append(const TextBuffer &buffer) { append((const char *) &buffer.text[0]); return *this; }
	inline TextBuffer &append(const TextBuffer &buffer, int32_t maxLength);
	inline TextBuffer &append(bool flag) { return flag ? append("true") : append("false"); }
	TextBuffer &append(int number);
	TextBuffer &append(float number);
	TextBuffer &append(double number);
    
    inline TextBuffer &prepend(char character) {  text.insert(text.begin(), character); return *this; }
	TextBuffer &prepend(const char *string);
	inline TextBuffer &prepend(const TextBuffer &buffer) { prepend((const char *) &buffer.text[0]); return *this; }
	inline TextBuffer &prepend(bool flag) { return flag ? prepend("true") : prepend("false"); }
	TextBuffer &prepend(int number);
	TextBuffer &prepend(float number);

	inline TextBuffer & clear() { text.clear(); text.push_back(0); return *this; }

	TextBuffer &removeWhiteSpaces();
	TextBuffer &trimFront();
	TextBuffer &trimEnd();
	TextBuffer &trim();
	TextBuffer &truncate(int32_t startIndex);

	bool startsWith(const TextBuffer &textToMatch) const;
	int32_t getTextIndex(const TextBuffer &textToFind) const;
	int32_t getTextIndex(int32_t startIndex, const TextBuffer &textToFind) const;

	inline char *getText() { return (char *) &text[0]; }
	inline const char *getConstText() const { return (const char *) &text[0]; }
	inline int32_t getTextLength() const { return (int32_t) text.size()-1; }
	inline bool isEmpty() const { return text.size() == 1; }
};

////////////////////////////////////////////////
// Text parsing utilities
////////////////////////////////////////////////

bool isWhiteSpace(char character);
int readLine(const char *src, char *outLine);
char *trimFront(char *text);
char *trimEnd(char *text);
char *trim(char *text);
char *removeWhitespaces(char *text);
int testSignature(const char *text, const char *signature);
bool parseLine(const char *lineText, const char *signature, const char *format, ...);

int stringCompare(const char *string1, const char *string2);

#endif
