#pragma once

#include <Nongl/Shader.h>

class SeparateAlphaShader : public Shader {

	int positionLoc;
	int colorMaskLoc;
	int textureCoordLoc;
	int textureLoc;
	int alphaTextureLoc;

	int globalTranslateLoc;
	int globalScaleLoc;
	int globalColorMaskLoc;

public:

	SeparateAlphaShader();

	bool use();
	void release();

	void setParams(float *vertexData, int globalColorMask, int textureId, int alphaTextureId);
};
