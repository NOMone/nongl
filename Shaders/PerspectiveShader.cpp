#include "PerspectiveShader.h"
#include <Nongl/glWrapper.h>
using namespace Ngl;

namespace {
	const char *plainPerspectiveVertexShaderCode =
		"attribute vec4 position;\n"
		"attribute vec4 colorMask;\n"
		"attribute vec2 textureCoord;\n"
		"uniform vec4 globalScale;\n"
		"uniform vec4 globalTranslate;\n"
		"uniform vec4 globalColorMask;\n"
		"uniform float zScale;\n"
		"varying " LOWP " vec4 pixelColorMask;\n"
		"varying " MEDIUMP " vec3 pixelTextureCoord;\n"
		"varying " MEDIUMP " vec2 pixelCoord;\n"
		"void main() {\n"

		"	 const mat4 projection = mat4(\n"
		"	        vec4(    1.0, 0.0, 0.0, 0.0),\n"
		"	        vec4(    0.0, 1.0, 0.0, 0.0),\n"
		"	        vec4(    0.0, 0.0, 0.00005, 0.0012),\n"
		"	        vec4(    0.0, 0.0, 0.0, 1.0)\n"
		"	    );\n"

		"  gl_Position = (position + globalTranslate) * globalScale;\n"
		//"  gl_Position.z -= 1000.0;"
		"  gl_Position = projection * gl_Position;"
		"  gl_Position.z += 0.035;\n"


		//"  pixelCoord = gl_Position.xy;\n"
		"  pixelColorMask = colorMask * globalColorMask;\n"
		//"  pixelColorMask = vec4(vec3(0.0+(gl_Position.z*1.0)), 1.0);\n"
		"  pixelTextureCoord.xy = textureCoord;\n"
		"}\n";

	const char *plainPerspectiveFragmentShaderCode =
		PRECISION_MEDIUMP_FLOAT
		"varying " LOWP " vec4 pixelColorMask;\n"
		"varying " MEDIUMP " vec3 pixelTextureCoord;\n"
		"varying " MEDIUMP " vec2 pixelCoord;\n"
		"uniform " LOWP " sampler2D texture;\n"
		"void main() {\n"
		//"  float distance = 1.5-length(pixelCoord);\n"
		"  gl_FragColor = texture2D(texture, pixelTextureCoord.xy) * pixelColorMask;\n"
		//"  gl_FragColor.rgb *= distance;\n"
		"}\n";
}

PerspectiveShader::PerspectiveShader() : PlainShader(plainPerspectiveVertexShaderCode, plainPerspectiveFragmentShaderCode) {}

bool PerspectiveShader::use() {

	if (!PlainShader::use()) return false;

	// TODO: is retrieving attributes and uniforms really necessary with every use? How about once when code compiled?

	// Get uniforms' locations,
	zScaleLoc = glGetUniformLocation(programId, "zScale");
	return true;
}

void PerspectiveShader::release() {
	PlainShader::release();
}

void PerspectiveShader::setParams(float *vertexData, int globalColorMask, int textureId) {
	PlainShader::setParams(vertexData, globalColorMask, textureId);
	glUniform1f(zScaleLoc, zScale);
}

void PerspectiveShader::refresh() {
	setShaderCode(plainPerspectiveVertexShaderCode, plainPerspectiveFragmentShaderCode);
}

void PerspectiveShader::setExtraParams(float zScale) {
	this->zScale = zScale;
}
