
#include "PerspectiveMaterial.h"

#include "PerspectiveShader.h"
#include <Nongl/Vertex.h>
#include <Nongl/Vectors.h>
#include <Nongl/Texture.h>
#include <Nongl/DisplayManager.h>
#include <Nongl/glWrapper.h>

using namespace Ngl;

PerspectiveMaterial::PerspectiveMaterial(const TextBuffer &name) : Material(name) {}

PerspectiveMaterial::PerspectiveMaterial(const shared_ptr<PerspectiveShader> &shader, Texture *texture, const TextBuffer &name) : Material(name) {
	this->shader = shader;
	this->texture = texture;
}

Material *PerspectiveMaterial::clone() const {
	PerspectiveMaterial *newPerspectiveMaterial = new PerspectiveMaterial();
	*newPerspectiveMaterial = *this;
	return newPerspectiveMaterial;
}

void PerspectiveMaterial::getFormatedVerticesData(const Vertex *vertices, int32_t vertexSizeBytes, int32_t verticesCount, uint8_t *formattedVertexData) {

#define FORMATTED_VERTEX_SIZE_FLOATS 6

	float *formattedVerticesDataFloat = (float *) formattedVertexData;
	int32_t formattedVerticesDataSizeFloats = verticesCount * FORMATTED_VERTEX_SIZE_FLOATS;

	const uint8_t *currentVertexByte = (uint8_t *) vertices;
	const Vertex *currentVertex;
	Vec2 currentTextureCoordinates;
	Vec3 currentXYZ;
	int32_t currentColor;

	for (int32_t i=0; i<formattedVerticesDataSizeFloats; i+=FORMATTED_VERTEX_SIZE_FLOATS) {

		currentVertex = (const Vertex *) currentVertexByte;
		currentXYZ = currentVertex->getXYZ(TexturedVertex::defaultXYZ);
		currentTextureCoordinates = currentVertex->getTextureCoordinates(TexturedVertex::defaultTextureCoordinates);
		currentColor = currentVertex->getColor(TexturedVertex::defaultColor);

		// TODO: remove hard coded values,
		formattedVerticesDataFloat[i  ] = currentXYZ.x;
		formattedVerticesDataFloat[i+1] = currentXYZ.y;
		formattedVerticesDataFloat[i+2] = currentXYZ.z - 1500;
		formattedVerticesDataFloat[i+3] = currentTextureCoordinates.x;
		formattedVerticesDataFloat[i+4] = currentTextureCoordinates.y;
		formattedVerticesDataFloat[i+5] = *((float *) &currentColor);

		currentVertexByte += vertexSizeBytes;
	}
}

void PerspectiveMaterial::prepareMaterial(const uint8_t *verticesData) {

	// Get texture id for drawing,
	DisplayManager *displayManager = DisplayManager::getSingleton();
	int texId = displayManager->prepareTexture(texture);

	shader->use();
	shader->setParams(0, color, texId);
}
