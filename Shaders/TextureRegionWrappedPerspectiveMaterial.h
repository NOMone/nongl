#pragma once

#include <Nongl/Material.h>
#include <Nongl/ManagedObject.h>

namespace Ngl {

	class TextureRegionWrappedPerspectiveShader;
	class Texture;

	class TextureRegionWrappedPerspectiveMaterial : public Material {
		shared_ptr<TextureRegionWrappedPerspectiveShader> shader;
		Texture *texture;
		int32_t color=0xffffffff;
	public:

		TextureRegionWrappedPerspectiveMaterial(const TextBuffer &name="");
		TextureRegionWrappedPerspectiveMaterial(const shared_ptr<TextureRegionWrappedPerspectiveShader> &shader, Texture *texture, const TextBuffer &name="");

		Material *clone() const;

		void setTexture(Texture *texture) { this->texture = texture; }
		void setColor(int32_t color) { this->color = color; }

		int32_t getFormatedVerticesDataSizeBytes(int32_t verticesCount) { return verticesCount*40; }
		void getFormatedVerticesData(const Vertex *vertices, int32_t vertexSizeBytes, int32_t verticesCount, uint8_t *formattedVerticesData);
		void prepareMaterial(const uint8_t *verticesData);
	};
}
