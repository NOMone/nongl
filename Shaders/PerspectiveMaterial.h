#pragma once

#include <Nongl/Material.h>
#include <Nongl/ManagedObject.h>

namespace Ngl {

	class PerspectiveShader;
	class Texture;

	class PerspectiveMaterial : public Material {
		shared_ptr<PerspectiveShader> shader;
		Texture *texture;
		int32_t color=0xffffffff;
	public:

		PerspectiveMaterial(const TextBuffer &name="");
		PerspectiveMaterial(const shared_ptr<PerspectiveShader> &shader, Texture *texture, const TextBuffer &name="");

		Material *clone() const;

		void setTexture(Texture *texture) { this->texture = texture; }
		void setColor(int32_t color) { this->color = color; }

		int32_t getFormatedVerticesDataSizeBytes(int32_t verticesCount) { return verticesCount*24; }
		void getFormatedVerticesData(const Vertex *vertices, int32_t vertexSizeBytes, int32_t verticesCount, uint8_t *formattedVerticesData);
		void prepareMaterial(const uint8_t *verticesData);
	};
}
