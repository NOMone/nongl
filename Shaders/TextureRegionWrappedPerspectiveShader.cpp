#include "TextureRegionWrappedPerspectiveShader.h"
#include <Nongl/DisplayManager.h>
#include <Nongl/glWrapper.h>
using namespace Ngl;

namespace {

	const char *vertexShaderCode =
		"attribute vec4 position;\n"
		"attribute vec4 colorMask;\n"
		"attribute vec2 textureCoord;\n"
		"uniform vec4 globalScale;\n"
		"uniform vec4 globalTranslate;\n"
		"uniform vec4 globalColorMask;\n"
		"uniform float zScale;\n"
		"varying " LOWP " vec4 pixelColorMask;\n"
		"varying " MEDIUMP " vec3 pixelTextureCoord;\n"
		"varying " MEDIUMP " vec2 pixelCoord;\n"
		"void main() {\n"
		"  float z;\n"
		"  z = position.z * 0.0003;\n"
		"  gl_Position = (position + globalTranslate) * globalScale;\n"
		"  gl_Position.z = globalTranslate.z + (z * zScale);\n"
		"  z += 1.0;\n"
		//"  z = 1.0;\n"
		"  gl_Position.xy /= z;\n"
		"  pixelCoord = gl_Position.xy;\n"
		"  pixelColorMask = colorMask * globalColorMask;\n"
		"  pixelTextureCoord.xy = textureCoord / z;\n"
		"  pixelTextureCoord.z = 1.0 / z;\n"
		"}\n";

	const char *fragmentShaderCode =
		PRECISION_MEDIUMP_FLOAT
		"varying " LOWP " vec4 pixelColorMask;\n"
        "varying " MEDIUMP " vec3 pixelTextureCoord;\n"
		"varying " MEDIUMP " vec2 pixelCoord;\n"
        "uniform " LOWP " sampler2D texture;\n"
        "void main() {\n"
        "  float distance = 1.5-length(pixelCoord);\n"
        "  gl_FragColor = texture2DProj(texture, pixelTextureCoord) * pixelColorMask;\n"
        "  gl_FragColor.rgb *= distance;\n"
        "}\n";
}

TextureRegionWrappedPerspectiveShader::TextureRegionWrappedPerspectiveShader() : Shader(vertexShaderCode, fragmentShaderCode) {}

bool TextureRegionWrappedPerspectiveShader::use() {

	if (!Shader::use()) return false;

	// TODO: is retrieving attributes really necessary with every use? How about once when code compiled?

	// Get attributes' locations,
	positionLoc = glGetAttribLocation(programId, "position");
	colorMaskLoc = glGetAttribLocation(programId, "colorMask");
	textureCoordLoc = glGetAttribLocation(programId, "textureCoord");

	// Get uniforms' locations,
	//mvpMatrixLoc = glGetUniformLocation(programId, "mvpMatrix");
	globalTranslateLoc = glGetUniformLocation(programId, "globalTranslate");
	globalScaleLoc = glGetUniformLocation(programId, "globalScale");
	globalColorMaskLoc = glGetUniformLocation(programId, "globalColorMask");
	zScaleLoc = glGetUniformLocation(programId, "zScale");

	// Textures,
	textureLoc = glGetUniformLocation(programId, "texture");
	glUniform1i(textureLoc, 0);

	return true;
}

void TextureRegionWrappedPerspectiveShader::release() {
    glDisableVertexAttribArray(positionLoc);
    glDisableVertexAttribArray(colorMaskLoc);
    glDisableVertexAttribArray(textureCoordLoc);
}

void TextureRegionWrappedPerspectiveShader::setParams(float *vertexData, int globalColorMask, int textureId) {

	// Bind textures,
	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    // Uniforms,
    DisplayManager *displayManager = DisplayManager::getSingleton();
    glUniform4f(globalTranslateLoc,
    		displayManager->viewPortTransformation.transformationX - (displayManager->viewPortTransformation.transformationWidth * 0.5f),
    		displayManager->viewPortTransformation.transformationY - (displayManager->viewPortTransformation.transformationHeight * 0.5f),
    		displayManager->viewPortTransformation.transformationZ,
    		0);
    glUniform4f(globalScaleLoc,
    		2.0f / displayManager->viewPortTransformation.transformationWidth,
    		2.0f / displayManager->viewPortTransformation.transformationHeight,
    		1, 1);
    glUniform4f(globalColorMaskLoc,
    		( globalColorMask        & 255) / 255.0f,
    		((globalColorMask >> 8 ) & 255) / 255.0f,
    		((globalColorMask >> 16) & 255) / 255.0f,
    		((globalColorMask >> 24) & 255) / 255.0f);

	glUniform1f(zScaleLoc, zScale);

    // Attributes,
    #define FLOAT_BYTES 4
	#define COLOR_MASK_BYTES 4
	#define BYTE_STRIDE (9 * FLOAT_BYTES) + COLOR_MASK_BYTES
    glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, BYTE_STRIDE, vertexData);
    glEnableVertexAttribArray(positionLoc);
    glVertexAttribPointer(textureCoordLoc, 2, GL_FLOAT, GL_FALSE, BYTE_STRIDE, &vertexData[3]);
    glEnableVertexAttribArray(textureCoordLoc);
    glVertexAttribPointer(colorMaskLoc, 4, GL_UNSIGNED_BYTE, GL_TRUE, BYTE_STRIDE, &vertexData[5]);
    glEnableVertexAttribArray(colorMaskLoc);

    // TODO: add remaining attributes...
}

void TextureRegionWrappedPerspectiveShader::refresh() {
	setShaderCode(vertexShaderCode, fragmentShaderCode);
}

void TextureRegionWrappedPerspectiveShader::setExtraParams(float zScale) {
	this->zScale = zScale;
}
