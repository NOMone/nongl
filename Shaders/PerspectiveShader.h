#pragma once

#include "PlainShader.h"

namespace Ngl {

	class PerspectiveShader : public PlainShader {

		int zScaleLoc;
		float zScale;

	public:

		PerspectiveShader();

		bool use();
		void release();
		void setParams(float *vertexData, int globalColorMask, int textureId);

		void refresh();
		void setExtraParams(float zScale);
	};
}
