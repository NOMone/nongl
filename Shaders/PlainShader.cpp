#include "PlainShader.h"
#include <Nongl/DisplayManager.h>
#include <Nongl/glWrapper.h>

static const char vertexShaderCode[] =
        "attribute vec4 position;\n"
		"attribute vec4 colorMask;\n"
        "attribute vec2 textureCoord;\n"
		"uniform vec4 globalScale;\n"
		"uniform vec4 globalTranslate;\n"
		"uniform vec4 globalColorMask;\n"
		"varying " LOWP " vec4 pixelColorMask;\n"
        "varying " MEDIUMP " vec2 pixelTextureCoord;\n"
        "void main() {\n"
        "  gl_Position = (position + globalTranslate) * globalScale;\n"
		"  pixelColorMask = colorMask * globalColorMask;\n"
        "  pixelTextureCoord = textureCoord;\n"
        "}\n";

		// Counter clockwise rotation:
		//"  gl_Position.yxzw = (position + globalTranslate) * globalScale;\n"
		//"  gl_Position.xy = ((gl_Position.xy + vec2(1.0)) * vec2(800.0/1280.0, 1280.0/800.0)) - vec2(1.0);\n"
		//"  gl_Position.x = 0.0 - gl_Position.x;\n"

static const char fragmentShaderCode[] =
		PRECISION_MEDIUMP_FLOAT
		"varying " LOWP " vec4 pixelColorMask;\n"
        "varying " MEDIUMP " vec2 pixelTextureCoord;\n"
        "uniform " LOWP " sampler2D texture;\n"
        "void main() {\n"
        "  gl_FragColor = texture2D(texture, pixelTextureCoord) * pixelColorMask;\n"
        "}\n";

PlainShader::PlainShader(bool initialize) : Shader() {
	if (initialize) setShaderCode(vertexShaderCode, fragmentShaderCode);
}

PlainShader::PlainShader(const char *newVertexShaderCode, const char *newFragmentShaderCode) : Shader() {
	if (!newVertexShaderCode) newVertexShaderCode = vertexShaderCode;
	if (!newFragmentShaderCode) newFragmentShaderCode = fragmentShaderCode;
	setShaderCode(newVertexShaderCode, newFragmentShaderCode);
}

bool PlainShader::use() {

	if (!Shader::use()) return false;

	// TODO: is retrieving attributes really necessary with every use? How about once when code compiled?

	// Get attributes' locations,
	positionLoc = glGetAttribLocation(programId, "position");
	colorMaskLoc = glGetAttribLocation(programId, "colorMask");
	textureCoordLoc = glGetAttribLocation(programId, "textureCoord");

	// Get uniforms' locations,
	//mvpMatrixLoc = glGetUniformLocation(programId, "mvpMatrix");
	globalTranslateLoc = glGetUniformLocation(programId, "globalTranslate");
	globalScaleLoc = glGetUniformLocation(programId, "globalScale");
	globalColorMaskLoc = glGetUniformLocation(programId, "globalColorMask");

	// Textures,
	textureLoc = glGetUniformLocation(programId, "texture");
	glUniform1i(textureLoc, 0);

	return true;
}

void PlainShader::release() {
    glDisableVertexAttribArray(positionLoc);
    glDisableVertexAttribArray(colorMaskLoc);
    glDisableVertexAttribArray(textureCoordLoc);
}

void PlainShader::setParams(float *vertexData, int globalColorMask, int textureId) {

	// Bind textures,
	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    // Uniforms,
    DisplayManager *displayManager = DisplayManager::getSingleton();
    glUniform4f(globalTranslateLoc,
    		displayManager->viewPortTransformation.transformationX - (displayManager->viewPortTransformation.transformationWidth * 0.5f),
    		displayManager->viewPortTransformation.transformationY - (displayManager->viewPortTransformation.transformationHeight * 0.5f),
    		displayManager->viewPortTransformation.transformationZ,
    		0);
    glUniform4f(globalScaleLoc,
    		2.0f / displayManager->viewPortTransformation.transformationWidth,
    		2.0f / displayManager->viewPortTransformation.transformationHeight,
    		1, 1);
    glUniform4f(globalColorMaskLoc,
    		( globalColorMask        & 255) / 255.0f,
    		((globalColorMask >> 8 ) & 255) / 255.0f,
    		((globalColorMask >> 16) & 255) / 255.0f,
    		((globalColorMask >> 24) & 255) / 255.0f);

    // Attributes,
    #define FLOAT_BYTES 4
	#define BYTE_STRIDE (5 * FLOAT_BYTES) + 4
    glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, BYTE_STRIDE, vertexData);
    glEnableVertexAttribArray(positionLoc);
    glVertexAttribPointer(textureCoordLoc, 2, GL_FLOAT, GL_FALSE, BYTE_STRIDE, &vertexData[3]);
    glEnableVertexAttribArray(textureCoordLoc);
    glVertexAttribPointer(colorMaskLoc, 4, GL_UNSIGNED_BYTE, GL_TRUE, BYTE_STRIDE, &vertexData[5]);
    glEnableVertexAttribArray(colorMaskLoc);
}

