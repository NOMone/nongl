#pragma once

#include <Nongl/Shader.h>

namespace Ngl {

	class TextureRegionWrappedPerspectiveShader : public Shader {

		int positionLoc;
		int colorMaskLoc;
		int textureCoordLoc;
		int textureLoc;

		int globalTranslateLoc;
		int globalScaleLoc;
		int globalColorMaskLoc;

		int zScaleLoc;
		float zScale;

	public:

		TextureRegionWrappedPerspectiveShader();

		bool use();
		void release();
		void setParams(float *vertexData, int globalColorMask, int textureId);

		void refresh();
		void setExtraParams(float zScale);
	};
}
