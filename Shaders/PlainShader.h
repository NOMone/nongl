#pragma once

#include <Nongl/Shader.h>

class PlainShader : public Shader {

	int positionLoc;
	int colorMaskLoc;
	int textureCoordLoc;
	int textureLoc;

	int globalTranslateLoc;
	int globalScaleLoc;
	int globalColorMaskLoc;

public:

	PlainShader(bool initialize=true);
	PlainShader(const char *newVertexShaderCode, const char *newFragmentShaderCode=0);

	virtual bool use();
	virtual void release();

	virtual void setParams(float *vertexData, int globalColorMask, int textureId);
};
