#include "AbstractSprite.h"
#include "Sprite.h"
#include "SegmentedSprite.h"
#include "NinePatchSprite.h"

AbstractSprite::~AbstractSprite() {
	if (sprite) delete sprite;
	if (segmentedSprite) delete segmentedSprite;
	if (ninePatchSprite) delete ninePatchSprite;
}

void AbstractSprite::init(const Ngl::TextureRegion *textureRegion) {
	reset();
	sprite = new Sprite(textureRegion);
	width  = sprite->width ;
	height = sprite->height;
}

void AbstractSprite::init(const SegmentedSpriteParameters *params) {
	reset();
	segmentedSprite = new SegmentedSprite(params);
	width  = segmentedSprite->getWidth ();
	height = segmentedSprite->getHeight();
}

void AbstractSprite::init(const NinePatchData *data, float width, float height) {
	reset();
	ninePatchSprite = new NinePatchSprite(data);
	ninePatchSprite->setWidth(width);
	ninePatchSprite->setHeight(height);
	this->width  = ninePatchSprite->getWidth ();
	this->height = ninePatchSprite->getHeight();
}

void AbstractSprite::reset() {
	sprite = 0;
	segmentedSprite = 0;
	ninePatchSprite = 0;

	x = y = z = 0;
	scaleX = scaleY = 1;
	rotation = 0;
	rotationPivotXDisplacement = rotationPivotYDisplacement = 0;
	colorMask = 0xffffffff;

	modified = true;
}

void AbstractSprite::drawOpaqueParts(SpriteBatch *spriteBatch) {
	if (sprite) {
		if (sprite->textureRegion->hasAlpha) return;
		if (modified) { updateSprite(); modified = false; }
		sprite->draw(spriteBatch);
	} else if (segmentedSprite) {
		if (modified) { updateSegmentedSprite(); modified = false; }
		segmentedSprite->drawOpaqueParts(spriteBatch);
	} else if (ninePatchSprite) {
		if (modified) { updateNinePatchSprite(); modified = false; }
		ninePatchSprite->drawOpaqueParts(spriteBatch);
	}
}

void AbstractSprite::drawTransparentParts(SpriteBatch *spriteBatch) {
	if (sprite) {
		if (!sprite->textureRegion->hasAlpha) return;
		if (modified) { updateSprite(); modified = false; }
		sprite->draw(spriteBatch);
	} else if (segmentedSprite){
		if (modified) { updateSegmentedSprite(); modified = false; }
		segmentedSprite->drawTransparentParts(spriteBatch);
	} else if (ninePatchSprite) {
		if (modified) { updateNinePatchSprite(); modified = false; }
		ninePatchSprite->drawTransparentParts(spriteBatch);
	}
}

void AbstractSprite::drawAllInOrder(SpriteBatch *spriteBatch) {
	if (sprite) {
		if (modified) { updateSprite(); modified = false; }
		sprite->draw(spriteBatch);
	} else if (segmentedSprite) {
		if (modified) { updateSegmentedSprite(); modified = false; }
		segmentedSprite->drawAllInOrder(spriteBatch);
	} else if (ninePatchSprite) {
		if (modified) { updateNinePatchSprite(); modified = false; }
		ninePatchSprite->drawAllInOrder(spriteBatch);
	}
}

void AbstractSprite::updateSprite() {

	sprite->z = z;
	sprite->width  = width ;
	sprite->height = height;
	sprite->scaleX = scaleX;
	sprite->scaleY = scaleY;
	sprite->rotation = rotation;
	sprite->colorMask = colorMask;

	if (rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(rotation);
		float sinAngle = sinf(rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		sprite->setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);
	} else {
		sprite->setLeft(x);
		sprite->setBottom(y);
	}
}

void AbstractSprite::updateSegmentedSprite() {

	segmentedSprite->setDepth(z);
	//segmentedSprite->setWidth (width );    // Segmented sprite doesn't support changing dimensions.
	//segmentedSprite->setHeight(height);    // Segmented sprite doesn't support changing dimensions.
	segmentedSprite->setScale(scaleX, scaleY);
	segmentedSprite->setRotation(rotation);
	segmentedSprite->setColorMask(colorMask);

	if (rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(rotation);
		float sinAngle = sinf(rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		segmentedSprite->setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);
	} else {
		segmentedSprite->setLeft(x);
		segmentedSprite->setBottom(y);
	}
}

void AbstractSprite::updateNinePatchSprite() {

	ninePatchSprite->setDepth(z);
	ninePatchSprite->setWidth (width );
	ninePatchSprite->setHeight(height);
	ninePatchSprite->setScale(scaleX, scaleY);
	ninePatchSprite->setRotation(rotation);
	ninePatchSprite->setColorMask(colorMask);

	if (rotation && (rotationPivotXDisplacement || rotationPivotYDisplacement)) {

		float centerX = getCenterX();
		float centerY = getCenterY();

		float cosAngle = cosf(rotation);
		float sinAngle = sinf(rotation);

		float rotatedDisplacementX =    (rotationPivotYDisplacement * sinAngle) - (rotationPivotXDisplacement * cosAngle);
		float rotatedDisplacementY = - ((rotationPivotYDisplacement * cosAngle) + (rotationPivotXDisplacement * sinAngle));

		// Adjust position,
		ninePatchSprite->setCenter(
				centerX + rotationPivotXDisplacement + rotatedDisplacementX,
				centerY + rotationPivotYDisplacement + rotatedDisplacementY);
	} else {
		ninePatchSprite->setLeft(x);
		ninePatchSprite->setBottom(y);
	}
}

void AbstractSprite::attachToAnimation(Animation *animation) {

	animation->setOnUpdatedFlagTarget(&modified);

	std::vector<AnimationComponent *>::iterator iterator = animation->components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = animation->components.end();
	for (; iterator!=endIterator; ++iterator) {
		AnimationComponent *currentComponent = (*iterator);
		switch (currentComponent->target) {

			case AnimationTarget::X:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += x;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += x;
				currentComponent->setTargetAddress(&x);
				break;

			case AnimationTarget::Y:
				if (currentComponent->initialValueRelative) currentComponent->initialValueFloat += y;
				if (currentComponent->  finalValueRelative) currentComponent->finalValueFloat += y;
				currentComponent->setTargetAddress(&y);
				break;

			case AnimationTarget::YAW:
				currentComponent->setTargetAddress(&rotation);
				break;

			default:
				break;
		}
	}
}

void AbstractSprite::setTextureRegion(const Ngl::TextureRegion *textureRegion) {

	if (sprite) {
		sprite->setTextureRegion(textureRegion);
		sprite->width = textureRegion->originalWidth;
		sprite->height = textureRegion->originalHeight;
	} else if (segmentedSprite) {
		delete segmentedSprite;
		segmentedSprite = 0;
		sprite = new Sprite(textureRegion);
	} else if (ninePatchSprite) {
		delete ninePatchSprite;
		ninePatchSprite = 0;
		sprite = new Sprite(textureRegion);
	}

	width  = textureRegion->originalWidth ;
	height = textureRegion->originalHeight;
	modified = true;
}

void AbstractSprite::setNinePatch(const NinePatchData *ninePatchData) {

	if (sprite) {
		delete sprite;
		sprite = 0;
	} else if (segmentedSprite) {
		delete segmentedSprite;
		segmentedSprite = 0;
	} else if (ninePatchData) {
		delete ninePatchSprite;
		ninePatchSprite = 0;
	}

	ninePatchSprite = new NinePatchSprite(ninePatchData);

	width  = ninePatchSprite->getWidth ();
	height = ninePatchSprite->getHeight();
	modified = true;
}
