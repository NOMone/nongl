#include "EntitySystem.h"
#include "SystemLog.h"

#include <string.h>
#include <stdexcept>

using namespace Ngl;

//////////////////////
// Attributes
//////////////////////

Ngl::Attributes::~Attributes() {

	// Delete all remaining attributes,
	for (int32_t i=(int32_t) attributes.size()-1; i>-1; i--) {
		delete attributes[i];
	}
}

void Ngl::Attributes::addAttribute(Attribute *attribute) {

	int attributeId = attribute->id;
	int i;
	for (i=0; i<attributes.size(); i++) {
		if (attributeId <= attributes[i]->id) break;
	}

	if (attributeId == attributes[i]->id) {
		throw std::runtime_error("Attribute with the same id was added before.");
	}

	attributes.insert(attributes.begin() + i, attribute);
}

Ngl::Attribute *Ngl::Attributes::getAttribute(int attributeId) {

	// Binary search,
	int32_t upperBound = (int32_t) attributes.size() - 1;
	if (upperBound < 0) return 0;   // No attributes found at all.

	int32_t lowerBound = 0;
	int32_t currentIndex = (lowerBound + upperBound) >> 1;

	do {

		if (attributes[currentIndex]->id == attributeId) {
			// Attribute found,
			return attributes[currentIndex];
		}

		if (attributes[currentIndex]->id > attributeId) {
			// Search below,
			upperBound = currentIndex - 1;
		} else {
			// Search above,
			lowerBound = currentIndex + 1;
		}

		if (lowerBound > upperBound) {
			// Search exhausted, attribute not found,
			return 0;
		}

		currentIndex = (lowerBound + upperBound) >> 1;
	} while (true);

	return 0;  // This should never be reached.
}

//////////////////////
// System
//////////////////////

// Nothing here!

//////////////////////
// Component
//////////////////////

// We'll have to rely on the library user to provide the managing pointer (?)...
Component::Component(Entity *parentEntity, shared_ptr<Component> &managingPointer) : parentEntity(_parentEntity) {
	this->_parentEntity = parentEntity;
	this->attributes = 0;
	managingPointer.manage(this);
	parentEntity->addComponent(managingPointer);
	parentEntity->entitiesManager->onComponentCreated<Component>(managingPointer);
}

Component::~Component() {
	parentEntity->entitiesManager->onComponentDeleted(this);
	delete attributes;
}

//////////////////////
// Entity
//////////////////////

Entity::Entity(EntitiesManager *entitiesManager) :
		entitiesManager(_entitiesManager),
		components(_components) {

	this->_entitiesManager = entitiesManager;
}

Entity::~Entity() {

	// Delete all remaining components,
	_components.killAll();
}

//////////////////////
// Entities manager
//////////////////////

EntitiesManager::~EntitiesManager() {

	entities.collectDead();
	LOGE("Entities manager created %d entities in its life-time and had %d entities when destructed.", createdEntitiesCount, entities.size());

	// Delete any remaining entities,
	entities.killAll();

	LOGE("Entities manager destructed gracefully.");
}

shared_ptr<Entity> EntitiesManager::createEntity() {
	shared_ptr<Entity> newEntity(new Entity(this));
	addEntity(newEntity);
	return newEntity;
}

int EntitiesManager::getAttributeId(const char *attributeName) {

	// Search for the attribute,
	int32_t attributesCount = (int32_t) attributeNames.size();
	for (int32_t i=0; i<attributesCount; i++) {
		if (!strcmp(attributeName, attributeNames[i].getText())) {
			return i;
		}
	}

	// Attribute not found, add a new one,
	attributeNames.emplace_back(attributeName);

	return attributesCount;
}

void EntitiesManager::allocateSystemsVector(int componentTypeId) {
	componentTypeId++;	// Because vector size = last index + 1.
	while (componentTypeId > listeningSystems.size()) {
		SharedVector<System> newVector;
		listeningSystems.push_back(newVector);
	}
}
