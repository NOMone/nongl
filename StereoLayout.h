#pragma once

#include <Nongl/AbsoluteLayout.h>

#include <vector>

namespace Ngl {

    struct Position {
        float x, y, scaleX, scaleY;
        Position(float x, float y, float scaleX, float scaleY) : x(x), y(y), scaleX(scaleX), scaleY(scaleY) {}
    };

    class StereoLayout : public AbsoluteLayout {

        AbsoluteLayout *leftLayout, *rightLayout;
        bool stereo=true;

        std::vector<Position> oldPositions;

        virtual void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly);

    public:

        StereoLayout(float x, float y, float width, float height, bool deletesChildrenOnDesruction=true);
        ~StereoLayout();

        float getEffectiveWidth() { return leftLayout->getWidth(); }

        void setStereo(bool stereo=true);
        void setScale(float scaleX, float scaleY) {
            rightLayout->setScale(scaleX, scaleY);
            leftLayout ->setScale(scaleX, scaleY);
        }

        virtual void addView(View *view) { addView(view, rightLayout->getChildViewsCount()); }
        virtual void addView(View *view, int index) {
            rightLayout->addView(view, index);
            leftLayout ->addView(view, index);
        }

        virtual View *getView(int index) { return rightLayout->getView(index); }

        virtual void removeView(View *view) {
            rightLayout->removeView(view);
            leftLayout ->removeView(view);
        }

        virtual void removeView(int32_t index) {
            rightLayout->removeView(index);
            leftLayout ->removeView(index);
        }

        void sort();
    };
}
