#pragma once

class Point3D {
public:
	float x, y, z;

	inline Point3D() : x(0), y(0), z(0) {}
	inline Point3D(float x, float y, float z) : x(x), y(y), z(z) {}

	inline void set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }

	void roll(float angleRadian);
	void pitch(float angleRadian);
	void yaw(float angleRadian);
};
