#pragma once

#include <cstdint>

namespace Ngl {
	class InputStream;
}

#define PI 3.14159265359f
#define HALF_PI 1.570796326795f
#define DEGREE_TO_RADIAN_CONST 0.01745329252f
#define RADIAN(angleInDegrees) ((angleInDegrees) * DEGREE_TO_RADIAN_CONST)

// Opengl,
void clearScreen(unsigned int clearColor, bool clearDepth);
void clearDepth();
bool checkGlError(const char* op);
int loadShader(int shaderType, const char *code);
int createProgram(const char *vertexShaderCode, const char *fragmentShaderCode);

void loadPngTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType);
void loadKtxTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType, bool miniZCompressed = false);
void loadPvrTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType, bool miniZCompressed = false);

// Random,
float randomFloat(void);
int randomInteger(int max);  // Note: max can't be generated. The largest number possible is max - 1.
inline float randomFloatRange(float minValue, float maxValue) { return minValue + (randomFloat() * (maxValue - minValue)); }
inline int randomIntegerRange(int minValue, int maxValue) { return minValue + randomInteger(1+maxValue-minValue); } // Note: max can be generated.

// System,
double getTimeMillis();
void sleepMillis(float milliseconds);

// Math,
inline float absolute(float value) { return (value > 0) ? value : -value; }
float magnitude(float x, float y);
float distance(float x1, float y1, float x2, float y2);

// Color manipulation,
// (Needs testing)
unsigned int rgba(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha);

// Multiplies colors with sufficient precision (tolerance = 1).
unsigned int multiplyColors(unsigned int color1, unsigned int color2);

// Language,
template<class T>
inline T removeReferenceFromType(T &a) {}
