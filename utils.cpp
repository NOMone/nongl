#include "utils.h"

#include "SystemInterface.h"
#include "SystemLog.h"
#include "glWrapper.h"
#include "Compression.h"

#include "LodePng/LodePng.h"

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <vector>
#include <stdexcept>

/////////////////////////////
// Opengl
/////////////////////////////

#define GL_COVERAGE_BUFFER_BIT_NV 0x8000

// Clears the screen with the specified color,
void clearScreen(unsigned int clearColor, bool clearDepth) {

	glClearColor(
			((clearColor >> 16) & 255) / 255.0f,
			((clearColor >>  8) & 255) / 255.0f,
			((clearColor      ) & 255) / 255.0f,
			((clearColor >> 24)      ) / 255.0f );

	int mask = GL_COLOR_BUFFER_BIT;
	if (javaUsesCoverageAa()) mask |= GL_COVERAGE_BUFFER_BIT_NV;
	if (clearDepth) mask |= GL_DEPTH_BUFFER_BIT;

	glClear(mask);
}

// Clears the depth buffer,
void clearDepth() {
	glClear(GL_DEPTH_BUFFER_BIT);
}

// Checks for OpenGL errors,
bool checkGlError(const char* op) {

#if VERBOSE
#if CHECK_GL_ERRORS
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGE("after %s() glError (0x%x)\n", op, error);
        return true;
    }
#endif
#endif

    return false;
}

// Loads a shader (vertex or fragment) and returns its id,
int loadShader(int shaderType, const char *code) {
    
	int shader = glCreateShader(shaderType);

	if (shader != 0) {
		int32_t codeLength = (int32_t) strlen(code);
		glShaderSource(shader, 1, &code, &codeLength);
		glCompileShader(shader);

		int compiled[1];
		glGetShaderiv(shader, GL_COMPILE_STATUS, compiled);

		if (compiled[0] == 0) {
			LOGE("Could not compile shader");

			char errorInfo[1024];
			int infoSize;
			glGetShaderInfoLog(shader, 1024, &infoSize, errorInfo);
			LOGE("%s", errorInfo);

			glDeleteShader(shader);
			return 0;
        }
    }

    return shader;
}

// Creates a shader program (both vertex and fragment) and returns its id,
int createProgram(const char *vertexShaderCode, const char *fragmentShaderCode) {

	int vertexShader = loadShader(GL_VERTEX_SHADER, vertexShaderCode);
	if (vertexShader == 0)
		LOGE("Could not load vertex shader.");

	int pixelShader = loadShader(GL_FRAGMENT_SHADER, fragmentShaderCode);
	if (pixelShader == 0)
		LOGE("Could not load fragment shader.");

	int program = glCreateProgram();
	if (!program)
	{
		LOGE("Could not load create shader program.");
		return 0;
	}
	else
	{
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");

        glAttachShader(program, pixelShader);
		checkGlError("glAttachShader");

		glLinkProgram(program);
		int linkStatus[1];
		glGetProgramiv(program, GL_LINK_STATUS, linkStatus);

		if (linkStatus[0] != GL_TRUE)
		{
			LOGE("Could not laod link program.");

			char errorInfo[1024];
			int infoSize;
			glGetProgramInfoLog(program, 1024, &infoSize, errorInfo);
			LOGE("%s", errorInfo);

            glDeleteProgram(program);
			return 0;
        }
    }

	return program;
}

void loadPngTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType) {

	std::vector<unsigned char> pixels;
	unsigned int width, height;

	// PicoPNG decode,
	// decodePNG(pixels, width, height, textureData, textureDataSize, true);

	// LodePNG decode,
	unsigned error = lodepng::decode(pixels, width, height, (unsigned char *) textureData, textureDataSize);
	if (error) {
		throw std::runtime_error(TextBuffer("Couldn't load png texture (").append(lodepng_error_text(error)).append(").").getConstText());
	}

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //GL_REPEAT);

	// Apply texture filter,
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinFilterType);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagFilterType);

	// Upload texture data,
	glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGBA,
        GLsizei(width),
        GLsizei(height),
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        &pixels[0]);

   	// Generate mipmaps,
	switch (textureMinFilterType) {
	case GL_NEAREST_MIPMAP_NEAREST:
	case GL_NEAREST_MIPMAP_LINEAR:
	case GL_LINEAR_MIPMAP_NEAREST:
	case GL_LINEAR_MIPMAP_LINEAR:
		glGenerateMipmap(GL_TEXTURE_2D);
		break;
	default:
		break;
	}
}

#ifdef LIB_KTX_ENABLED

	#include "libktx/ktx.h"

	void loadKtxTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType, bool miniZCompressed) {

		// If MiniZ compressed,
		std::vector<uint8_t> uncompressedTextureData;
		if (miniZCompressed) {

			// Uncompress,
			try {
				Ngl::miniZUncompress(textureData, uncompressedTextureData, 0);
				textureData = &uncompressedTextureData[0];
				textureDataSize = uncompressedTextureData.size();
			} catch (const std::runtime_error &e) {
				throw std::runtime_error(
						TextBuffer("Couldn't load ktx (.jet) texture file (uncompression failed: ").
						append(e.what()).append(')').getConstText());
			}
		}

		// Bind texture,
		glBindTexture(GL_TEXTURE_2D, textureId);

		// Set texture parameters,
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //GL_REPEAT);

		// Apply texture filter,
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinFilterType);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagFilterType);

		// Load ktx file,
		GLenum target;

		KTX_error_code result = ktxLoadTextureM(
				textureData, textureDataSize,
				(unsigned int *) &textureId, &target,
				0, 0, 0, 0, 0);

		if(result != KTX_SUCCESS) {
			throw std::runtime_error(TextBuffer("Couldn't load ktx texture (").append(result).append(").").getConstText());
		}
	}

#else // #ifdef LIB_KTX_ENABLED
	void loadKtxTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType, bool miniZCompressed) {}
#endif

struct PVRHeaderV3 {
    unsigned int       mVersion;
    unsigned int       mFlags;
    unsigned long long mPixelFormat;
    unsigned int       mColourSpace;
    unsigned int       mChannelType;
    unsigned int       mHeight;
    unsigned int       mWidth;
    unsigned int       mDepth;
    unsigned int       mNumSurfaces;
    unsigned int       mNumFaces;
    unsigned int       mMipmapCount;
    unsigned int       mMetaDataSize;
};

void loadPvrTexture(unsigned int textureId, const void *textureData, int32_t textureDataSize, int textureMinFilterType, int textureMagFilterType, bool miniZCompressed) {

	// If MiniZ compressed,
	std::vector<uint8_t> uncompressedTextureData;
	if (miniZCompressed) {

		// Uncompress,
		try {
			Ngl::miniZUncompress(textureData, uncompressedTextureData, 0);
			textureData = &uncompressedTextureData[0];
			textureDataSize = uncompressedTextureData.size();
		} catch (const std::runtime_error &e) {
			throw std::runtime_error(
					TextBuffer("Couldn't load pvr (.xmf) texture file (uncompression failed: ").
					append(e.what()).append(')').getConstText());
		}
	}

    // Bind texture,
	glBindTexture(GL_TEXTURE_2D, textureId);

	// Set texture parameters,
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //GL_REPEAT);

	// Apply texture filter,
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinFilterType);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagFilterType);

	// Load pvr file,
    // Grab the header
    PVRHeaderV3 header;
	unsigned int *currentPosition = (unsigned int *) textureData;

	header.mVersion = *currentPosition++;
    header.mFlags = *currentPosition++;
    header.mPixelFormat = *((unsigned long long *) currentPosition); currentPosition+=2;
    header.mColourSpace = *currentPosition++;
    header.mChannelType = *currentPosition++;
    header.mHeight = *currentPosition++;
    header.mWidth = *currentPosition++;
    header.mDepth = *currentPosition++;
    header.mNumSurfaces = *currentPosition++;
    header.mNumFaces = *currentPosition++;
    header.mMipmapCount = *currentPosition++;
    header.mMetaDataSize = *currentPosition++;

    // Determine the format,
    GLenum format;
    GLuint bitsPerPixel;

    #define GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG   0x8C00
    #define GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG   0x8C01
    #define GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG  0x8C02
    #define GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG  0x8C03

    switch(header.mPixelFormat) {
        case 0:
            // PVRTC 2bpp RGB
            format = GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
            bitsPerPixel = 2;
            break;

        case 1:
            // PVRTC 2bpp RGBA
            format = GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
            bitsPerPixel = 2;
            break;

        case 2:
            // PVRTC 4bpp RGB
            format = GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
            bitsPerPixel = 4;
            break;

        case 3:
            // PVRTC 4bpp RGBA
            format = GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
            bitsPerPixel = 4;
            break;

        default:
			throw std::runtime_error("Couldn't load pvr texture. Unknown format.");
    }

    // Load the texture,
    char *mipMapData = ((char *) textureData) + (52 + header.mMetaDataSize);
    unsigned int mipMapWidth = header.mWidth;
    unsigned int mipMapHeight = header.mHeight;

    unsigned int currentMipMapLevel = 0;
    do {

        // Determine size (width * height * bbp / 8),
        // pixelDataSize must be at least two blocks (4x4 pixels for 4bpp, 8x4 pixels for 2bpp), so min size is 32,
        unsigned int pixelDataSize = (mipMapWidth * mipMapHeight * bitsPerPixel) >> 3;
        pixelDataSize = (pixelDataSize < 32) ? 32 : pixelDataSize;

        // Upload texture data for this mipmap level,
        glCompressedTexImage2D(
        		GL_TEXTURE_2D,
        		currentMipMapLevel,
        		format,
        		mipMapWidth, mipMapHeight,
        		0, pixelDataSize, mipMapData);
        checkGlError("glCompressedTexImage2D");

        // Next mips is half the size with a min of 1,
        mipMapWidth = mipMapWidth >> 1;
        mipMapWidth = (mipMapWidth == 0) ? 1 : mipMapWidth;

        mipMapHeight = mipMapHeight >> 1;
        mipMapHeight = (mipMapHeight == 0) ? 1 : mipMapHeight;

        // Move to next mip,
        mipMapData += pixelDataSize;
        currentMipMapLevel++;
    } while(currentMipMapLevel < header.mMipmapCount);
}

/////////////////////////////
// Random numbers
/////////////////////////////

float randomFloat(void) {
    static bool firstTime=true;
    if (firstTime) {
        srand(time(NULL));
        firstTime = false;
    }
	return (float)rand()/(float)RAND_MAX;
}

int randomInteger(int max) {
	return rand() % max;
}

/////////////////////////////
// System utils
/////////////////////////////

#ifdef TARGET_OS_MAC
#include <mach/clock.h>
#include <mach/mach.h>
double getTimeMillis(void) {
    
    clock_serv_t cclock;
    mach_timespec_t mts;
    
    host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    
    return ((double) mts.tv_sec*1000) + ((double) mts.tv_nsec/1000000);
}
#else
double getTimeMillis(void) {
	//return javaGetTimeMillis();

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return ((double) now.tv_sec*1000) + ((double) now.tv_nsec/1000000);
}
#endif

void sleepMillis(float milliseconds) {
	usleep((int) (milliseconds * 1000));
}

/////////////////////////////
// Math
/////////////////////////////

float magnitude(float x, float y) {
	return sqrtf((x*x) + (y*y));
}

float distance(float x1, float y1, float x2, float y2) {
	float xDifference = x1-x2;
	float yDifference = y1-y2;
	return sqrtf((xDifference*xDifference) + (yDifference*yDifference));
}

/////////////////////////////
// Colors
/////////////////////////////

unsigned int rgba(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) {
	return red | (green<<8) | (blue << 16) | (alpha << 24);
}

unsigned int multiplyColors(unsigned int color1, unsigned int color2) {

	return rgba(
			( ((color1 & 0x000000ff)       +1) *  (color2 & 0x000000ff)       ) >> 8,
			((((color1 & 0x0000ff00) >>  8)+1) * ((color2 & 0x0000ff00) >>  8)) >> 8,
			((((color1 & 0x00ff0000) >> 16)+1) * ((color2 & 0x00ff0000) >> 16)) >> 8,
			((((color1 & 0xff000000) >> 24)+1) * ((color2 & 0xff000000) >> 24)) >> 8);
}
