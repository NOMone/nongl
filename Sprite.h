#pragma once

#include "TextureRegion.h"
#include "Animation.h"

class Animation;
class Sprite;
class SpriteBatch;

/////////////////////////////////
// Sprite animation listener
/////////////////////////////////

class SpriteAnimationListener : public AnimationListener {
	Sprite *sprite;
public:
	float scaleAboutCenter;

	inline SpriteAnimationListener(Sprite *targetSprite) { this->sprite = targetSprite; }
	void onAnimationComponentApplied(AnimationTarget::Target target);
};

/////////////////////////////////
// Sprite
/////////////////////////////////

class Sprite {

	SpriteAnimationListener *animationListener;
	void setupAnimationListener(Animation *animation);

public:

	float width, height;
	float x, y, z;
	float scaleX, scaleY;
	float rotation;
	bool flipX, flipY;
	int colorMask;
	const Ngl::TextureRegion *textureRegion;

	Sprite();
	Sprite(const Sprite &spriteToClone);
	Sprite(const Ngl::TextureRegion *textureRegion);
	Sprite(float width, float height, Ngl::TextureRegion *textureRegion);
	~Sprite();

	void clone(const Sprite *spriteToClone);

	void setFromTextureRegion(const Ngl::TextureRegion *textureRegion);
	void set(
			float width, float height,
			float x, float y, float z,
			float scaleX, float scaleY,
			float rotation,
			bool flipX, bool flipY,
			int colorMask,
			const Ngl::TextureRegion *textureRegion);

	void setTextureRegion(const Ngl::TextureRegion *textureRegion);

	inline Ngl::Texture *getTexture() { return textureRegion->texture; }

	bool hit(float x, float y);
	bool hit(float x, float y, float rightTolerance, float leftTolerance=0, float topTolerance=0, float bottomTolerance=0);
	float getDistance(float x, float y);

	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + (width * scaleX); }
	inline float getTop() { return y + (height * scaleY); }
	inline float getCenterX() { return x + (width * scaleX * 0.5f); }
	inline float getCenterY() { return y + (height * scaleY * 0.5f); }

	inline void setLeft(float left) { x = left; }
	inline void setBottom(float bottom) { y = bottom; }
	inline void setRight(float right) { x = right - (width * scaleX); }
	inline void setTop(float top) { y = top - (height * scaleY); }
	inline void setCenterX(float centerX) { x = centerX - (width * scaleX * 0.5f); }
	inline void setCenterY(float centerY) { y = centerY - (height * scaleY * 0.5f); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }

	void draw(float *vertexData, unsigned short *indices, int verticesOffset);
	void drawWireFrame(SpriteBatch *spriteBatch, int color);
	void draw(SpriteBatch *spriteBatch);

	void attachToAnimation(Animation *animation);
};


class SpritesGroup {
	std::list<Sprite *> sprites;
public:

	bool deletesChildrenOnDestruction;
	bool drawsFullyTransparentSprites;

	inline SpritesGroup() {
		deletesChildrenOnDestruction = true;
		drawsFullyTransparentSprites = false;
	}
	inline ~SpritesGroup() { deleteAllSprites(); }

	inline void add(Sprite *sprite) { sprites.push_back(sprite); }
	void deleteAllSprites();

	void draw(SpriteBatch *spriteBatch);
	void draw(SpriteBatch *spriteBatch, float zOffset, float zScale);
};
