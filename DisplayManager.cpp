#include "DisplayManager.h"

#include "../Src/Config.h"
#include "SystemInterface.h"
#include "SystemLog.h"
#include "utils.h"
#include "TextUtils.h"
#include "glWrapper.h"
#include "Texture.h"
#include "TextureRegion.h"
#include "File.h"

//#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <stdexcept>

////////////////////////////////////////////////
// Static initializations
////////////////////////////////////////////////

DisplayManager *DisplayManager::singleton = 0;

////////////////////////////////////////////////
// Construction and destruction
////////////////////////////////////////////////

DisplayManager::DisplayManager() {

	designWidth  = DESIGN_WIDTH;
	designHeight = DESIGN_HEIGHT;

	viewPort.width = DESIGN_WIDTH;
	viewPort.height = DESIGN_HEIGHT;
	viewPort.x = 0;
	viewPort.y = 0;

	viewPortTransformation.transformationWidth = DESIGN_WIDTH;
	viewPortTransformation.transformationHeight = DESIGN_HEIGHT;
	viewPortTransformation.transformationX = 0;
	viewPortTransformation.transformationY = 0;
	viewPortTransformation.transformationZ = 0;

	clipping.x = 0;
	clipping.y = 0;
	clipping.width = DESIGN_WIDTH;
	clipping.height = DESIGN_HEIGHT;

	entirelyClipped = false;

	maximumLoadedTexturesCount = 0;
	loadedTexturesCount = 0;

	etc1SupportTested = false;
	pvrtcSupportTested = false;
}

DisplayManager::~DisplayManager() {

	// Free all the allocated memory,

	// Delete all textures,
	int32_t texturesCount = (int32_t) textures.size();
	for (int i=0; i<texturesCount; i++) {
		if (textures[i]->loaded) glDeleteTextures(1, &textures[i]->id);
		delete textures[i];
	}

	// Texture regions,
	int32_t textureRegionsCount = (int32_t) textureRegions.size();
	for (int i=0; i<textureRegionsCount; i++) delete textureRegions[i];

	LOGE("DisplayManager destructed");
}

DisplayManager *DisplayManager::getSingleton() {
	if (singleton == 0) {
		singleton = new DisplayManager();
	}
	return singleton;
}

////////////////////////////////////////////////
// Viewport management,
////////////////////////////////////////////////

void DisplayManager::setDesignDimensions(float width, float height) {

	if (viewPortStack.size() || viewPortTransformationStack.size() || clippingStack.size()) {
		throw std::runtime_error("Can't changed design dimensions deep into drawing.");
	}

	if ((designWidth == width) && (designHeight == height)) return ;

	designWidth = width;
	designHeight = height;

	this->viewPort.width = width;
	this->viewPort.height = height;
	this->clipping.width = width;
	this->clipping.height = height;
	this->viewPortTransformation.transformationWidth = width;
	this->viewPortTransformation.transformationHeight = height;

	applyViewPort();
}

void DisplayManager::onDisplayChanged(int displayWidth, int displayHeight) {
	this->displayWidth = displayWidth;
	this->displayHeight = displayHeight;

	this->physicalClipping.width = displayWidth;
	this->physicalClipping.height = displayHeight;

	applyViewPort();
}

void DisplayManager::setViewPort(int x, int y, int width, int height) {
	viewPort.width = width;
	viewPort.height = height;
	viewPort.x = x;
	viewPort.y = y;

	applyViewPort();
}

void DisplayManager::applyViewPort() {

	float widthRatio = displayWidth / designWidth;
	float heightRatio = displayHeight / designHeight;

	glViewport(
			viewPort.x * widthRatio, viewPort.y * heightRatio,
			viewPort.width * widthRatio, viewPort.height * heightRatio);
}

void DisplayManager::pushViewPort() {
	viewPortStack.push_back(viewPort);
}

void DisplayManager::popViewPort() {

	ViewPort poppedViewPort = viewPortStack.back();
	viewPortStack.pop_back();

	// Just return if nothing changed,
	if ((viewPort.x      ==      poppedViewPort.x) &&
		(viewPort.y      ==      poppedViewPort.y) &&
		(viewPort.width  ==  poppedViewPort.width) &&
		(viewPort.height == poppedViewPort.height)) {
		return ;
	}

	viewPort = poppedViewPort;
	applyViewPort();
}

void DisplayManager::setViewPortTransformation(float x, float y, float z, float width, float height) {

	// If nothing changed, just return,
	if ((viewPortTransformation.transformationX      ==      x) &&
		(viewPortTransformation.transformationY      ==      y) &&
		(viewPortTransformation.transformationZ      ==      z) &&
		(viewPortTransformation.transformationWidth  ==  width) &&
		(viewPortTransformation.transformationHeight == height)) {
		return ;
	}

	viewPortTransformation.transformationX = x;
	viewPortTransformation.transformationY = y;
	viewPortTransformation.transformationZ = z;
	viewPortTransformation.transformationWidth = width;
	viewPortTransformation.transformationHeight = height;
}

void DisplayManager::resetViewPortTransformation() {
	setViewPortTransformation(0, 0, 0, designWidth, designHeight);
}

void DisplayManager::pushViewPortTransformation() {
	viewPortTransformationStack.push_back(viewPortTransformation);
}

void DisplayManager::popViewPortTransformation() {

	ViewPortTransformation poppedViewPortTransformation = viewPortTransformationStack.back();
	viewPortTransformationStack.pop_back();

	// If nothing changed, just return,
	if ((viewPortTransformation.transformationX      ==     poppedViewPortTransformation.transformationX) &&
		(viewPortTransformation.transformationY      ==      poppedViewPortTransformation.transformationY) &&
		(viewPortTransformation.transformationZ      ==      poppedViewPortTransformation.transformationZ) &&
		(viewPortTransformation.transformationWidth  ==  poppedViewPortTransformation.transformationWidth) &&
		(viewPortTransformation.transformationHeight == poppedViewPortTransformation.transformationHeight)) {
		return ;
	}

	viewPortTransformation = poppedViewPortTransformation;
}

void DisplayManager::setClipping(float x, float y, float width, float height, bool withinCurrentClipping) {

	// If nothing changed, just return,
	if ((clipping.x      ==      x) &&
		(clipping.y      ==      y) &&
		(clipping.width  ==  width) &&
		(clipping.height == height)) {
		return ;
	}

	clipping.x = x;
	clipping.y = y;
	clipping.width = width;
	clipping.height = height;

	applyClipping(withinCurrentClipping);
}

void DisplayManager::pushClipping() {
	clippingStack.push_back(clipping);
	physicalClippingStack.push_back(physicalClipping);
}

void DisplayManager::popClipping() {

	clipping = clippingStack.back();
	clippingStack.pop_back();

	physicalClipping = physicalClippingStack.back();
	physicalClippingStack.pop_back();

	entirelyClipped = (!physicalClipping.width) || (!physicalClipping.height);

	glScissor(physicalClipping.x, physicalClipping.y, physicalClipping.width, physicalClipping.height);
	if (checkGlError("popClipping => glScissor()")) {
		LOGE("glScissor(%f, %f, %f, %f)", physicalClipping.x, physicalClipping.y, physicalClipping.width, physicalClipping.height);
	}
}

void DisplayManager::applyClipping(bool withinCurrentClipping) {

	// Convert from view transformed coordinates to screen coordinates,
	float clippingLeft, clippingBottom, clippingRight, clippingTop;
	getScreenCoords(clipping.x, clipping.y, &clippingLeft, &clippingBottom);
	getScreenCoords(clipping.x + clipping.width, clipping.y + clipping.height, &clippingRight, &clippingTop);

	// Clip against screen boundaries,
	if (clippingLeft   <             0) clippingLeft   =             0;
	if (clippingBottom <             0) clippingBottom =             0;
	if (clippingRight  >  displayWidth) clippingRight  =  displayWidth;
	if (clippingTop    > displayHeight) clippingTop    = displayHeight;

	// Clip against existing clipping,
	if (withinCurrentClipping) {
		if (clippingLeft   < physicalClipping.x                          ) clippingLeft   = physicalClipping.x                          ;
		if (clippingBottom < physicalClipping.y                          ) clippingBottom = physicalClipping.y                          ;
		if (clippingRight  > physicalClipping.x + physicalClipping.width ) clippingRight  = physicalClipping.x + physicalClipping.width ;
		if (clippingTop    > physicalClipping.y + physicalClipping.height) clippingTop    = physicalClipping.y + physicalClipping.height;
	}

	entirelyClipped = (clippingRight <= clippingLeft) || (clippingTop <= clippingBottom);

	// Update current clipping,
	if (entirelyClipped) {
		physicalClipping.x = 0;
		physicalClipping.y = 0;
		physicalClipping.width  = 0;
		physicalClipping.height = 0;
	} else {
		physicalClipping.x = clippingLeft;
		physicalClipping.y = clippingBottom;
		physicalClipping.width  = clippingRight - physicalClipping.x;
		physicalClipping.height = clippingTop   - physicalClipping.y;
	}

	// Set clipping,
	glScissor(physicalClipping.x, physicalClipping.y, physicalClipping.width, physicalClipping.height);
	if (checkGlError("applyClipping => glScissor()")) {
		LOGE("glScissor(%f, %f, %f, %f)", physicalClipping.x, physicalClipping.y, physicalClipping.width, physicalClipping.height);
	}
}

void DisplayManager::getDesignCoords(float inTransformedX, float inTransformedY, float *outDesignX, float *outDesignY) {

	// Reverse transform first,
	float x = (inTransformedX + viewPortTransformation.transformationX) * (designWidth  / viewPortTransformation.transformationWidth );
	float y = (inTransformedY + viewPortTransformation.transformationY) * (designHeight / viewPortTransformation.transformationHeight);

	// Now do the viewport mapping things,
	*outDesignX = (x * viewPort.width  / designWidth ) + viewPort.x;
	*outDesignY = (y * viewPort.height / designHeight) + viewPort.y;
}

void DisplayManager::getScreenCoords(float inTransformedX, float inTransformedY, float *outScreenX, float *outScreenY) {

	// Convert from view transformed coordinates to design coordinates,
	float designX, designY;
	getDesignCoords(inTransformedX, inTransformedY, &designX, &designY);

	// Now convert design coordinates into screen coordinates,
	*outScreenX = designX * (displayWidth  /  designWidth);
	*outScreenY = designY * (displayHeight / designHeight);
}

float DisplayManager::pixelsToCms(float lengthPixels) {
	float dotsPerCm = javaGetDpi() * 0.393700787f; // (1 / 2.54f);
	return lengthPixels / dotsPerCm;
}

////////////////////////////////////////////////
// Texture management,
////////////////////////////////////////////////

int32_t DisplayManager::getTexturesCount() {
	return textures.size();
}

Ngl::Texture *DisplayManager::getTexture(int32_t textureIndex) {
	return textures[textureIndex];
}

void DisplayManager::setMaximumLoadedTexturesCount(int maximumLoadedTexturesCount) {
	this->maximumLoadedTexturesCount = maximumLoadedTexturesCount;
}

Ngl::Texture *DisplayManager::addTexture(const TextBuffer &textureFileName, Ngl::FileLocation fileLocation, int textureMinFilterType, int textureMagFilterType) {

	// Check if texture file added before,
	class PreviousTexture {
	public:
		Ngl::File file;
		Ngl::Texture *texture;
		PreviousTexture(const Ngl::File &file, Ngl::Texture *texture) : file(file), texture(texture) {}
	};
	static std::vector<PreviousTexture> previouslyAddedTextures;

	Ngl::File textureFile(fileLocation, textureFileName);
	for (int32_t i=0; i<previouslyAddedTextures.size(); i++) {
		if (previouslyAddedTextures[i].file == textureFile) {
			LOGE("Texture: %s already added before.", textureFile.getFullPath().getConstText());
			return previouslyAddedTextures[i].texture;
		}
	}

	// Create a new texture,
	Ngl::Texture *newTexture = new Ngl::Texture();
	textures.push_back(newTexture);
	previouslyAddedTextures.emplace_back(textureFile, newTexture);

	// Set its attributes,
	newTexture->name = textureFileName;
	newTexture->minFilter = textureMinFilterType;
	newTexture->magFilter = textureMagFilterType;
	newTexture->alphaTexture = 0;

	newTexture->loaded = false;
	newTexture->id = 0;
	newTexture->lastUsed = 0;
	newTexture->releasable = true;

	// Set texture source and format,
	auto checkAndSetFile = [newTexture, fileLocation, &textureFileName] (const TextBuffer &extension, Ngl::TextureFormat textureFormat) -> bool {

		Ngl::shared_ptr<Ngl::File> textureFile(new Ngl::File(fileLocation, TextBuffer(textureFileName).append(extension)));
		if (textureFile->exists()) {
			newTexture->source = Ngl::shared_ptr<Ngl::InputStream>(new Ngl::FileInputStream(textureFile));
			newTexture->sourceFormat = textureFormat;
			return true;
		}
		return false;
	};

	if        (isPvrtcSupported() && checkAndSetFile(".xmf", Ngl::TextureFormat::XMF)) {
	} else if (isPvrtcSupported() && checkAndSetFile(".pvr", Ngl::TextureFormat::PVR)) {
	} else if (isEtc1Supported () && checkAndSetFile(".jet", Ngl::TextureFormat::JET)) {
	} else if (isEtc1Supported () && checkAndSetFile(".ktx", Ngl::TextureFormat::KTX)) {
	} else if (                      checkAndSetFile(".png", Ngl::TextureFormat::PNG)) {
	} else {

		// Nothing worked :(,
		delete newTexture;
		textures.pop_back();
		previouslyAddedTextures.pop_back();
		throw std::runtime_error(TextBuffer("Couldn't add texture: ").append(textureFileName).getConstText());
	}

	return newTexture;
}

Ngl::Texture *DisplayManager::createTexture(int width, int height, int textureMinFilterType, int textureMagFilterType) {

	Ngl::Texture *newTexture = new Ngl::Texture();
	textures.push_back(newTexture);

	// Properties,
	newTexture->width = width;
	newTexture->height = height;
	newTexture->minFilter = textureMinFilterType;
	newTexture->magFilter = textureMagFilterType;
	newTexture->alphaTexture = 0;
	newTexture->releasable = false;
	newTexture->loaded = true;
	newTexture->lastUsed = 0;

	// Create the texture,
	glGenTextures(1, &newTexture->id);

	glBindTexture(GL_TEXTURE_2D, newTexture->id);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //GL_REPEAT);

	// Apply texture filter,
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureMinFilterType);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureMagFilterType);

	// Upload texture data,
	glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGBA,
        GLsizei(width),
        GLsizei(height),
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        0);

   	// Generate mipmaps,
	switch (textureMinFilterType) {
	case GL_NEAREST_MIPMAP_NEAREST:
	case GL_NEAREST_MIPMAP_LINEAR:
	case GL_LINEAR_MIPMAP_NEAREST:
	case GL_LINEAR_MIPMAP_LINEAR:
		glGenerateMipmap(GL_TEXTURE_2D);
		break;
	default:
		break;
	}
    
    loadedTexturesCount++;

	return newTexture;
}

void DisplayManager::deleteTexture(Ngl::Texture *texture) {

    // Unload texture,
    if (texture->loaded) {
        glDeleteTextures(1, &texture->id);
        texture->loaded = false;
        loadedTexturesCount--;
    }

    // Delete texture,
	auto iterator = textures.begin();
	while (iterator!=textures.end()) {
		Ngl::Texture *currentTexture = *iterator;
		if (texture == currentTexture) {
			iterator = textures.erase(iterator);
            delete currentTexture;
            return ;
		} else {
			++iterator;
		}
	};
}

bool DisplayManager::releaseLeastRecentlyUsedTexture() {

	// Find the least recently used (oldest) releasable texture,
	Ngl::Texture *leastRecentlyUsedTexture=0;
	int leastRecentlyUsedAge = -1;
	int32_t count = (int32_t) textures.size();
	for (int i=0; i<count; i++) {

		Ngl::Texture *currentTexture = textures[i];
		if ( currentTexture->releasable &&
			 currentTexture->loaded &&
			(currentTexture->lastUsed > leastRecentlyUsedAge)) {

			leastRecentlyUsedTexture = currentTexture;
			leastRecentlyUsedAge = currentTexture->lastUsed;
		}
	}

	// If no images loaded,
	if (!leastRecentlyUsedTexture) return 0;

	// Release the texture,
	glDeleteTextures(1, &leastRecentlyUsedTexture->id);
	glFinish();

	leastRecentlyUsedTexture->loaded = false;
	leastRecentlyUsedTexture->lastUsed = 0;

	loadedTexturesCount--;

	if (!leastRecentlyUsedTexture->name.isEmpty()) {
		LOGE("Released texture: %s", leastRecentlyUsedTexture->name.getText());
	} else {
		LOGE("Released a texture with no file");
	}

	return 1;
}

void DisplayManager::destroyAllTextures() {

	// Delete all textures,
	int32_t texturesCount = (int32_t) textures.size();
	for (int i=0; i<texturesCount; i++) {
		if (textures[i]->loaded) {
			// TODO: call texture save listener...
			glDeleteTextures(1, &textures[i]->id);
			textures[i]->loaded = false;
		}
	}

	loadedTexturesCount = 0;
}

bool DisplayManager::isEtc1Supported(void) {

#ifdef LIB_KTX_ENABLED

	if (etc1SupportTested) return GLEW_OES_compressed_ETC1_RGB8_texture_supported;

	const int ETC1_RGB8_OES = 0x8D64;

	GLint num = 0;
 	GLEW_OES_compressed_ETC1_RGB8_texture_supported = false;

	glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &num);
	if (num > 0) {

		GLint* formats = (GLint*)calloc(num, sizeof(GLint));
		glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, formats);

		for (int index = 0; index < num; index++) {
			switch(formats[index]) {
			case ETC1_RGB8_OES:
				GLEW_OES_compressed_ETC1_RGB8_texture_supported = true;
				break;
			}
		}

		free(formats);
	}

	etc1SupportTested = true;


#else	//#ifdef LIB_KTX_ENABLED
	GLEW_OES_compressed_ETC1_RGB8_texture_supported = false;
#endif

	return GLEW_OES_compressed_ETC1_RGB8_texture_supported;
}

bool DisplayManager::isPvrtcSupported() {

	if (pvrtcSupportTested) return GL_IMG_texture_compression_pvrtc_supported;

    const GLubyte* pExtensions = glGetString(GL_EXTENSIONS);
    GL_IMG_texture_compression_pvrtc_supported = strstr((char*) pExtensions, "GL_IMG_texture_compression_pvrtc") != 0;

    pvrtcSupportTested = true;
    return GL_IMG_texture_compression_pvrtc_supported;
}

void DisplayManager::loadTexture(Ngl::Texture *texture) {

	std::vector<uint8_t> textureData;
	auto readTextureData = [&textureData, texture] () {
		texture->source->mark();
		try {
			texture->source->readData(textureData);
		} catch (const std::runtime_error &e) {
			texture->source->reset();
			throw e;
		}
		texture->source->reset();
	};

	switch (texture->sourceFormat) {
		case Ngl::TextureFormat::RAW:
			// TODO: implement raw format loading (width, height, uncompressed data),
			throw std::runtime_error("Couldn't load texture (raw format loading not implemented yet).");

		case Ngl::TextureFormat::XMF:
			readTextureData();
			loadPvrTexture(texture->id, &textureData[0], textureData.size(), texture->minFilter, texture->magFilter, true);
			texture->alphaTexture = 0; // No need for alpha texture.
			break;

		case Ngl::TextureFormat::PVR:
			readTextureData();
			loadPvrTexture(texture->id, &textureData[0], textureData.size(), texture->minFilter, texture->magFilter, false);
			texture->alphaTexture = 0; // No need for alpha texture.
			break;

		case Ngl::TextureFormat::JET:
			readTextureData();
			loadKtxTexture(texture->id, &textureData[0], textureData.size(), texture->minFilter, texture->magFilter, true);
			break;

		case Ngl::TextureFormat::KTX:
			readTextureData();
			loadKtxTexture(texture->id, &textureData[0], textureData.size(), texture->minFilter, texture->magFilter, false);
			break;

		case Ngl::TextureFormat::PNG:
			readTextureData();
			loadPngTexture(texture->id, &textureData[0], textureData.size(), texture->minFilter, texture->magFilter);
			break;

		default:
			throw std::runtime_error("Couldn't load texture (unimplemented/unknown format).");
			break;
	}
}

int DisplayManager::prepareTexture(Ngl::Texture *texture) {

	// Age all textures,
	int count = (int) textures.size();
	for (int i=0; i<count; i++) textures[i]->lastUsed++;

	// Mark the required texture as most recent,
	texture->lastUsed = 0;

	// If texture is already loaded, do nothing,
	if (texture->loaded) return texture->id;

	// Force maximum number of loaded textures (and make place for one extra texture),
	if (maximumLoadedTexturesCount) {
		while ((loadedTexturesCount>=maximumLoadedTexturesCount) && releaseLeastRecentlyUsedTexture());
	}

	// Load the texture,
	GLuint newTextureId;
	glGenTextures(1, &newTextureId);
	texture->id = newTextureId;

	try {
		loadTexture(texture);
		texture->loaded = true;
		loadedTexturesCount++;

		if (texture->name.isEmpty()) {
			LOGE("Loaded nameless texture");
		} else {
			LOGE("Loaded texture: %s", texture->name.getText());
		}
	} catch (const std::runtime_error &e) {

		// If loading failed,
		LOGE("%s", e.what());
		glDeleteTextures(1, &newTextureId);
		texture->id = 0;
	}

	return texture->id;
}

////////////////////////////////////////////////
// Texture regions management,
////////////////////////////////////////////////

int32_t DisplayManager::getTextureRegionsCount() {
	return (int32_t) textureRegions.size();
}

const Ngl::TextureRegion *DisplayManager::getTextureRegion(int index) {
	return textureRegions[index];
}

int32_t DisplayManager::getTextureRegionIndex(const char *name, int *insertIndex) {

	// Binary search using the name,
	int32_t lowBoundaryIndex = 0;
	int32_t highBoundaryIndex = (int32_t) textureRegions.size() - 1;
	int32_t currentIndex;
	while (lowBoundaryIndex <= highBoundaryIndex) {

		currentIndex = (lowBoundaryIndex + highBoundaryIndex) >> 1;

		int comparisonResult = strcmp(name, textureRegions[currentIndex]->imageName);

		// Found,
		if (!comparisonResult) {
			if (insertIndex) *insertIndex = currentIndex+1;
			return currentIndex; // Found.
		}

		if (comparisonResult < 0) {
			// Search below,
			highBoundaryIndex = currentIndex - 1;
		} else {
			// Search above,
			lowBoundaryIndex = currentIndex + 1;
		}
	}

	// Location in which this should have been found,
	if (insertIndex) *insertIndex = lowBoundaryIndex;

	// Not found,
	return -1;
}

const Ngl::TextureRegion *DisplayManager::getTextureRegion(const char *name, bool verbose) {

	int regionIndex = getTextureRegionIndex(name);
	if (regionIndex != -1) return textureRegions[regionIndex];

	if (verbose) LOGE("Texture region %s not found", name);
	return 0;
}

////////////////////////////////////////////////
// Texture atlas loading utilities
////////////////////////////////////////////////

static bool parseTextureFilename(const char *atlasFilename, const char *lineText, TextBuffer &textureFileName) {

	int32_t length = (int32_t) strlen(lineText);

	// The file name should be at least one character, a dot and
	// a three characters extension,
	if (length < 5) return false;

	// Test against the supported extensions,
	const char *extension = &lineText[length-3];
	if (!strcasecmp(extension, "png") || !strcasecmp(extension, "jpg")) {

		// Extract path of the pack file,
		int32_t slashIndex;
		for (slashIndex=(int32_t) strlen(atlasFilename); slashIndex>-1; slashIndex--) {
			if (atlasFilename[slashIndex] == '/') break;
		}

		textureFileName.clear();
		if (slashIndex > -1) textureFileName.append(atlasFilename, slashIndex+1);

		// Add the texture file name (without extension),
		textureFileName.append(lineText, length-4);

		return true;
	}

	return false;
}

static bool parseImageName(const char *lineText, TextBuffer &outImageName) {

	// Check if contains colons, otherwise it's an image name,
	int32_t length = (int32_t) strlen(lineText);
	for (; length>-1; length--) {
		if (lineText[length] == ':') return false;
	}

	outImageName.clear().append(lineText);
	return true;
}

static bool parseTextureFilterType(const char *lineText, int *minFilterType, int *magFilterType) {

	char filterType[50];
	if (!parseLine(lineText, "filter:", "filter:%s", filterType)) return false;

	// TODO: optimize by breaking into two parts, min and max then applying appropriate filters,
	if (testSignature(filterType, "Nearest,Nearest")) {
		*minFilterType = GL_NEAREST;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "Nearest,Linear")) {
		*minFilterType = GL_NEAREST;
		*magFilterType = GL_LINEAR;
	} else if (testSignature(filterType, "Linear,Nearest")) {
		*minFilterType = GL_LINEAR;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "Linear,Linear")) {
		*minFilterType = GL_LINEAR;
		*magFilterType = GL_LINEAR;
	} else if (testSignature(filterType, "MipMapNearestNearest,Nearest")) {
		*minFilterType = GL_NEAREST_MIPMAP_NEAREST;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "MipMapNearestNearest,Linear")) {
		*minFilterType = GL_NEAREST_MIPMAP_NEAREST;
		*magFilterType = GL_LINEAR;
	} else if (testSignature(filterType, "MipMapNearestLinear,Nearest")) {
		*minFilterType = GL_NEAREST_MIPMAP_LINEAR;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "MipMapNearestLinear,Linear")) {
		*minFilterType = GL_NEAREST_MIPMAP_LINEAR;
		*magFilterType = GL_LINEAR;
	} else if (testSignature(filterType, "MipMapLinearNearest,Nearest")) {
		*minFilterType = GL_LINEAR_MIPMAP_NEAREST;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "MipMapLinearNearest,Linear")) {
		*minFilterType = GL_LINEAR_MIPMAP_NEAREST;
		*magFilterType = GL_LINEAR;
	} else if (testSignature(filterType, "MipMapLinearLinear,Nearest")) {
		*minFilterType = GL_LINEAR_MIPMAP_LINEAR;
		*magFilterType = GL_NEAREST;
	} else if (testSignature(filterType, "MipMapLinearLinear,Linear")) {
		*minFilterType = GL_LINEAR_MIPMAP_LINEAR;
		*magFilterType = GL_LINEAR;
	}

	return true;
}

////////////////////////////////////////////////
// Texture atlas loading
////////////////////////////////////////////////

int32_t DisplayManager::loadTextureAtlas(const Ngl::File &packFile) {

	std::vector<uint8_t> packContents;
	int32_t packSize = packFile.read(packContents);
	Ngl::FileLocation packFileLocation = packFile.getLocation();

	char currentLine[1024];
	int currentLocation=0;

	// Data we need to collect,
	TextBuffer textureFilename;
	Ngl::Texture *texture;
	int textureWidth, textureHeight;

	TextBuffer imageName;
	int x, y;
	int width, height;
	int originalWidth, originalHeight;
	int offsetX, offsetY;
	int tempInteger;
	bool hasAlpha;

	std::vector<TextBuffer> supportedTextureFileExtensions(3);
	supportedTextureFileExtensions.push_back(TextBuffer(".png"));
	if (isEtc1Supported()) {
		supportedTextureFileExtensions.push_back(TextBuffer(".ktx"));
		supportedTextureFileExtensions.push_back(TextBuffer(".jet"));
	}
	if (isPvrtcSupported()) {
		supportedTextureFileExtensions.push_back(TextBuffer(".pvr"));
		supportedTextureFileExtensions.push_back(TextBuffer(".xmf"));
	}

	int processingTextureRegion = 0;
	TextBuffer newImageName;

	do {

		int charsCount;
		int eof=0;
		do {
			// Check if end of file,
			if (currentLocation >= packSize) {
				eof = 1;
				break;
			}

			// Read the next line,
			charsCount = readLine((char *) &packContents[currentLocation], currentLine);
			currentLocation += charsCount;
		} while (strlen(trim(currentLine)) == 0);

		if (eof) {
			// No more processing, save any texture region that is currently being processed,
			if (processingTextureRegion) {

				// Save this texture region,
				Ngl::TextureRegion *newTextureRegion = new Ngl::TextureRegion(
						texture,
						imageName,
						x, y,
						width, height,
						originalWidth, originalHeight,
						offsetX, offsetY,
						hasAlpha);
				addTextureRegion(newTextureRegion);
			}
			break;
		}

		// Check for image file,
		if (parseTextureFilename(packFile.getFullPath(), currentLine, textureFilename)) {

			if (processingTextureRegion) {

				// Save this texture region,
				Ngl::TextureRegion *newTextureRegion = new Ngl::TextureRegion(
						texture,
						imageName,
						x, y,
						width, height,
						originalWidth, originalHeight,
						offsetX, offsetY,
						hasAlpha);
				addTextureRegion(newTextureRegion);
				processingTextureRegion = 0;
			}

			// Add texture,
			texture = addTexture(textureFilename, packFileLocation,  GL_NEAREST, GL_NEAREST);

			// Add alpha texture (if any),
			TextBuffer alphaTextureFileName(textureFilename);
			alphaTextureFileName.append("_alpha");

			TextBuffer alphaTextureFullFileName;
			for (int i=0; i<supportedTextureFileExtensions.size(); i++) {
				alphaTextureFullFileName.clear().append(alphaTextureFileName).append(supportedTextureFileExtensions[i]);
				if (Ngl::File(packFileLocation, alphaTextureFullFileName).exists()) {
					texture->alphaTexture = addTexture(alphaTextureFileName, packFileLocation, GL_NEAREST, GL_NEAREST);
				}
			}

		} else if (parseImageName(currentLine, newImageName)) {

			if (processingTextureRegion) {

				// Save this texture region,
				Ngl::TextureRegion *newTextureRegion = new Ngl::TextureRegion(
						texture,
						imageName,
						x, y,
						width, height,
						originalWidth, originalHeight,
						offsetX, offsetY,
						hasAlpha);
				addTextureRegion(newTextureRegion);
			}
			imageName = newImageName;
			processingTextureRegion = 1;

		} else if (parseTextureFilterType(removeWhitespaces(currentLine), &texture->minFilter, &texture->magFilter)) {
			Ngl::Texture *alphaTexture = texture->getAlphaTexture();
			if (alphaTexture) {
				alphaTexture->minFilter = texture->minFilter;
				alphaTexture->magFilter = texture->magFilter;
			}
		} else if (parseLine(currentLine, "dimensions:", "dimensions:%d,%d", &textureWidth, &textureHeight)) {
			texture->width = textureWidth;
			texture->height = textureHeight;
		} else if (parseLine(currentLine, "xy:", "xy:%d,%d", &x, &y)) {
		} else if (parseLine(currentLine, "size:", "size:%d,%d", &width, &height)) {
		} else if (parseLine(currentLine, "orig:", "orig:%d,%d", &originalWidth, &originalHeight)) {
		} else if (parseLine(currentLine, "offset:", "offset:%d,%d", &offsetX, &offsetY)) {
		} else if (parseLine(currentLine, "hasAlpha:", "hasAlpha:%d", &tempInteger)) {
			hasAlpha = tempInteger;
		} else {
			//... attributes
			//LOGE("Found undefined: %s", currentLine);
		}
	} while (1);

	return 1;
}

void DisplayManager::addTextureRegion(Ngl::TextureRegion *textureRegion) {

	int regionIndex, insertIndex;
	regionIndex = getTextureRegionIndex(textureRegion->imageName, &insertIndex);

	if (regionIndex == -1) {

		// Region not found, insert new,
		textureRegions.insert(textureRegions.begin() + insertIndex, textureRegion);
	} else {

		// Region found, overwrite,
		*textureRegions[regionIndex] = *textureRegion;
		delete textureRegion;
	}
}
