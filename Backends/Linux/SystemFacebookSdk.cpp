
#if NONGL_FACEBOOK_SDK

#include <Nongl/SystemFacebookSdk.h>
#include <Nongl/TextUtils.h>
#include <Nongl/ManagedObject.h>

#include <functional>

using namespace Ngl;

void systemFacebookSdkLogInWithReadPermissions(
        const TextBuffer &appId,
        const TextBuffer &clientToken,
        const TextBuffer &permissions,
        const Ngl::ManagedPointer<std::function<void(const FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener) {

    if (onResponseReceivedListener.expired()) return;
    (*onResponseReceivedListener)(
            FacebookLoginResponseType::FACEBOOK_NOT_INTEGRATED,
            "Facebook integration not implemented in Linux");
}

void systemFacebookSdkLogOut() {

}

#endif
