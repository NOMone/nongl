
#include "../../SystemInterface.h"
#include "../../SystemEvents.h"
#include "../../SystemLog.h"
#include "../../Nongl.h"
#include "../../TextUtils.h"
#include "../../ByteBuffer.h"
#include "../../File.h"

#include "../../../Src/Config.h"

#include <string>
#include <math.h>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include <GL/glew.h>
#include "GLFW/glfw3.h"

#ifndef WINDOW_WIDTH
    #define WINDOW_WIDTH (DESIGN_WIDTH * 0.5f)
#endif
#ifndef WINDOW_HEIGHT
    #define WINDOW_HEIGHT (DESIGN_HEIGHT * 0.5f)
#endif

int windowWidth = WINDOW_WIDTH;
int windowHeight = WINDOW_HEIGHT;

float dpi;

GLFWwindow* window;

int is_dragging = 0;

void onWindowResized(GLFWwindow* window, int width, int height) {
	windowWidth = width;
	windowHeight = height;
	Nongl::onSurfaceChanged(windowWidth, windowHeight);
}

int init_gl() {
 
	// Initialize GLFW,
    if (glfwInit() != GL_TRUE) {
        LOGE("glfwInit() failed\n");
        return GL_FALSE;
    }

	// Create window,
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	
    window = glfwCreateWindow(windowWidth, windowHeight, APPLICATION_TITLE, NULL, NULL);
    if (!window) {
    	LOGE("glfwCreateWindow() failed\n");
        return GL_FALSE;
    }
    glfwMakeContextCurrent(window);

    // Put the window in the center of the screen,
	GLFWmonitor *primaryMonitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* videoMode = glfwGetVideoMode(primaryMonitor);
	glfwSetWindowPos(
			window,
			0.5f * (videoMode->width  - windowWidth ),
			0.5f * (videoMode->height - windowHeight));

    // Setup v-sync,
    glfwSwapInterval(1);

	// Initialize GLEW,
	glewExperimental = GL_TRUE;
 	GLenum err = glewInit();
 	if (err != GLEW_OK) {
		LOGE("GLEW init error: %s", glewGetErrorString(err));
	}

	// Initialize OpenGL state,
    glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);

    // Set window resized callback,
    glfwSetWindowSizeCallback(window, onWindowResized);

	// Get screen dpi,
	int widthMM, heightMM;
	glfwGetMonitorPhysicalSize(primaryMonitor, &widthMM, &heightMM);

	if ((videoMode->width == 0) || (widthMM == 0)) {
		dpi = 96;
	} else {
		dpi = videoMode->width / (widthMM / 25.4f);
	}

    return GL_TRUE;
}

void do_frame() {

	Nongl::onNewFrame();

    glfwSwapBuffers(window);
    glfwPollEvents();
}
 
void shutdown_gl() {

 	glfwDestroyWindow(window);
	glfwTerminate();
}

bool initAudio() {

	SDL_Init(SDL_INIT_AUDIO);
	Mix_Init(MIX_INIT_OGG | MIX_INIT_MP3);

	int ret = Mix_OpenAudio(0, 0, 0, 0); // we ignore all these..
	if (ret) {
		LOGE("couldn't open audio.");
	}	

	Mix_AllocateChannels(32);
	
	return ret;
}
 
void shutDownAudio() {
	// Force quit sdl audio,
	while(Mix_Init(0)) 	Mix_Quit();
	Mix_CloseAudio();
}

void onCursorPositionChanged(GLFWwindow *window, double x, double y) {

	double x_pos, y_pos;
	glfwGetCursorPos(window, &x_pos, &y_pos);
	float normalized_x =      x_pos /  windowWidth ;
	float normalized_y = 1 - (y_pos / windowHeight);

	if (is_dragging) {
		systemEventTouchDrag(0, normalized_x, normalized_y);
	}
}

void onMouseButtonEvent(GLFWwindow *, int button, int action, int mods) {
	
	if (button == GLFW_MOUSE_BUTTON_1) {
      
		double x_pos, y_pos;
		glfwGetCursorPos(window, &x_pos, &y_pos);
		float normalized_x =      x_pos /  windowWidth ;
		float normalized_y = 1 - (y_pos / windowHeight);

		if (action == GLFW_PRESS) {
			is_dragging = 1;            
			systemEventTouchDown(0, normalized_x, normalized_y);
		} else if (action == GLFW_RELEASE) {
			is_dragging = 0;
			systemEventTouchUp(0, normalized_x, normalized_y);
		}  
	}    
}

void onScrollEvent(GLFWwindow *window, double xOffset, double yOffset) {

	double cursorX, cursorY;
	glfwGetCursorPos(window, &cursorX, &cursorY);
	float normalizedX =      cursorX /  windowWidth ;
	float normalizedY = 1 - (cursorY / windowHeight);

	systemEventScroll(normalizedX, normalizedY, (float) xOffset, (float) yOffset);
}

static bool capsLock = false;
void onKeyEvent(GLFWwindow *window, int key, int scanCode, int action, int mods) {

	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_ESCAPE) {
			systemEventBackPressed();
		} else {			

			// TODO: get rid of GLFW for its poor input support.
			if (key == GLFW_KEY_CAPS_LOCK) capsLock = !capsLock;

			if ((key >= 'A') && (key <= 'Z')) {			
				bool capital = capsLock;
				if ((mods & GLFW_MOD_SHIFT) != 0) capital = !capital;
			
				if (!capital) key = tolower(key);
			}
			
			systemEventKeyDown(key);
		}

	} else if (action == GLFW_RELEASE) {

		if (key == GLFW_KEY_ESCAPE) {
		} else {			

			if ((key >= 'A') && (key <= 'Z')) {			
				bool capital = capsLock;
				if ((mods & GLFW_MOD_SHIFT) != 0) capital = !capital;
			
				if (!capital) key = tolower(key);
			}
			
			systemEventKeyUp(key);
		}
	
	} else if (action == GLFW_REPEAT) {
	
	}
}

void initInput() {
	glfwSetMouseButtonCallback(window, onMouseButtonEvent); 	
	glfwSetCursorPosCallback(window, onCursorPositionChanged);
	glfwSetScrollCallback(window, onScrollEvent);

	glfwSetKeyCallback(window, onKeyEvent);
}

static bool exitApplication = false;
int main() {
	
	// Initialize audio,
	initAudio();
			
	// Initialize OpenGL,
    if (init_gl() == GL_TRUE) {

		// Initialize graphics,
    	Nongl::onSurfaceCreated();
    	Nongl::onSurfaceChanged(windowWidth, windowHeight);
        		
	 	// Initialize input,
		initInput();

		// Main loop,
	    while (!glfwWindowShouldClose(window) && !exitApplication) {
	    	do_frame();
	    }

	    // Clean up if not done yet,
	    if (!exitApplication) {
	    	Nongl::onPause(true);
	    	Nongl::destroy();
	    }
    }
 
    shutdown_gl();
	shutDownAudio();

    return 0;
}

/////////////////////////////////////
// Preferences
/////////////////////////////////////

#define PREFERENCES_FILE "preferences"

class PreferencesManager {
    
	struct Preference {
        char *key;
        char *value;
        int valueSize;
    };
    
    std::vector<Preference> preferences;
public:
    
    PreferencesManager() { loadPreferences(); }
	~PreferencesManager() {
		for (int i=0; i<preferences.size(); i++) {
			free(preferences[i].key);
			free(preferences[i].value);
		}
	}
    
    void savePreferences() {
        
        Ngl::ByteBuffer fileData;
        
        // Write key-value pairs count,
        int preferencesCount = preferences.size();
        fileData.pushInt(preferencesCount);
        
        // Write key-value pairs,
        for (int i=0; i<preferencesCount; i++) {
            
            fileData.pushInt(strlen(preferences[i].key)+1);                    // Write key size.
            fileData.pushString(preferences[i].key);                           // Write key.
            fileData.pushInt(preferences[i].valueSize);                        // Write value size.
            fileData.pushData(preferences[i].value, preferences[i].valueSize); // Write value.
        }
        
        // Save data to file,
        Ngl::File(Ngl::FileLocation::SDCARD, PREFERENCES_FILE).write(fileData.getData(), fileData.getDataSize(), false);
    }
    
    void loadPreferences() {

        // Read preferences file,
    	Ngl::File preferencesFile(Ngl::FileLocation::SDCARD, PREFERENCES_FILE);
        if (!preferencesFile.exists()) {
            LOGE("Preferences file not found.");
            return ;
        }

    	std::vector<uint8_t> data;
        int32_t dataSize = preferencesFile.read(data);

        // Read key-value pairs count,
        unsigned char *currentByte = &data[0];
        int preferencesCount = *((int *) currentByte);
        currentByte += 4;
        
        // Read preferences,
        for (int i=0; i<preferencesCount; i++) {
            
            preferences.push_back(Preference());
            Preference &preference = preferences.back();
            
            // Read key,
            int keySize = *((int *) currentByte);
            currentByte += 4;
            
            preference.key = (char *) malloc(keySize);
            memcpy(preference.key, currentByte, keySize);
            currentByte += keySize;
            
            // Read value,
            preference.valueSize = *((int *) currentByte);
            currentByte += 4;
            
            preference.value = (char *) malloc(preference.valueSize);
            memcpy(preference.value, currentByte, preference.valueSize);
            currentByte += preference.valueSize;
        }
    }
    
    void addPreference(const char *key, const char *value, int valueSizeBytes=0) {
        
        // Default to zero terminated string,
        if (!valueSizeBytes) {
            valueSizeBytes = strlen(value) + 1;
        }
        
        // Update the preference if key is existing,
        for (int i=0; i<preferences.size(); i++) {
            if (strcmp(preferences[i].key, key) == 0) {
                free(preferences[i].value);
                preferences[i].value = (char *) malloc(valueSizeBytes);
                memcpy(preferences[i].value, value, valueSizeBytes);
                preferences[i].valueSize = valueSizeBytes;
                savePreferences();
                return;
            }
        }
        
        // Add a new preference,
        preferences.push_back(Preference());
        Preference &newPreference = preferences.back();
        
        newPreference.key = (char *) malloc(strlen(key)+1);
        strcpy(newPreference.key, key);
        
        newPreference.value = (char *) malloc(valueSizeBytes);
        memcpy(newPreference.value, value, valueSizeBytes);
        newPreference.valueSize = valueSizeBytes;
        
        savePreferences();
    }
    
    const char *getPreference(const char *key) {
        
        for (int i=0; i<preferences.size(); i++) {
            if (strcmp(preferences[i].key, key) == 0) {
                return preferences[i].value;
            }
        }
        return 0;
    }
};

// Instance of the preferences manager,
static PreferencesManager preferencesManager;

void javaSetBooleanPreference(const char *key, bool value) {
	
	if (value) {
		preferencesManager.addPreference(key, "true");
	} else {
		preferencesManager.addPreference(key, "false");
	}
}

bool javaGetBooleanPreference(const char *key, bool defaultValue) {
    
	const char *valueString = preferencesManager.getPreference(key);
	if (valueString) {
		if (strcmp(valueString, "true") == 0) {
			return true;
		} else {
			return false;
		}
	}
    
	return defaultValue;
}

void javaSetIntPreference(const char *key, int value) {
    
	char valueString[12];
	sprintf(valueString, "%d", value);
	
	preferencesManager.addPreference(key, valueString);
}

int javaGetIntPreference(const char *key, int defaultValue) {
    
	const char *valueString = preferencesManager.getPreference(key);
    
	if (valueString) {
		int value;
		sscanf(valueString, "%d", &value);
		return value;
	}
	
	return defaultValue;
}

void javaSetStringPreference(const char *key, const TextBuffer &value) {
    preferencesManager.addPreference(key, value);
}

TextBuffer javaGetStringPreference(const char *key, const TextBuffer &defaultValue) {

    const char *value = preferencesManager.getPreference(key);

    if (value) return value;
    return defaultValue;
}

/////////////////////////////////////
// Music
/////////////////////////////////////

std::vector<Mix_Music *> musics;

int javaLoadMusicFromAssets(const char *filename) { 

    TextBuffer completePath;
    completePath.append("assets/").append(filename).append(".ogg");
    
	Mix_Music *music = Mix_LoadMUS(completePath.getText());
	if (!music) {
	 	LOGE("Mix_LoadMUS: %s, %s\n", completePath.getText(), Mix_GetError());
	 	return -1;
	}
	
	musics.push_back(music);

	return musics.size()-1; 
}

void javaDeleteMusic(int musicIndex) {
	Mix_FreeMusic(musics[musicIndex]);
	musics[musicIndex] = 0;
}

void javaPlayMusic(int musicIndex) {
	if (Mix_PlayMusic(musics[musicIndex], 1) == -1) {
	 	LOGE("Mix_PlayMusic: %s\n", Mix_GetError());
 	}
}

void javaMusicSetLooping(int musicIndex, bool looping) {

	int loops = (looping) ? -1 : 1;
	if (Mix_PlayMusic(musics[musicIndex], loops) == -1) {
	 	LOGE("Mix_PlayMusic (looping): %s\n", Mix_GetError());
 	}
}

void javaStopMusic(int musicIndex) {
	Mix_HaltMusic();
}

void javaPauseMusic(int musicIndex) {
	Mix_PauseMusic();
}

void javaResumeMusic(int musicIndex) {
	Mix_ResumeMusic();
}

void javaFadeOutMusic(int musicIndex, int milliSeconds) {
	Mix_FadeOutMusic(milliSeconds);
}

/////////////////////////////////////
// Sound
/////////////////////////////////////

bool muteSounds = false;
std::vector<Mix_Chunk *> sounds;

int javaLoadSoundFromAssets(const char *filename) { 

    TextBuffer completePath;
    completePath.append("assets/").append(filename).append(".ogg");
    
	Mix_Chunk *sound = Mix_LoadWAV(completePath.getText());
	if (!sound) {
	 	LOGE("Mix_LoadWAV: %s, %s\n", completePath.getText(), Mix_GetError());
	 	return -1;
	}
	
	sounds.push_back(sound);
	
	return sounds.size()-1; 
}

void javaDeleteSound(int soundIndex) {
	Mix_FreeChunk(sounds[soundIndex]);
	sounds[soundIndex] = 0;
}

void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {

	if (muteSounds) return ;
	
	int channel = Mix_PlayChannel(-1, sounds[soundIndex], 0);
    if (channel == -1) {    
	 	LOGE("Mix_PlayChannel: %s\n", Mix_GetError());
 	} else {
	 	Mix_SetPanning(channel, leftVolume*255, rightVolume*255);
 	}
}

void javaStopSound(int soundIndex) {
	LOGE("incomplete function 20 called");
}

void javaPauseSound(int soundIndex) {
	LOGE("incomplete function 21 called");
}

void javaResumeSound(int soundIndex) {
	LOGE("incomplete function 22 called");
}

void javaSoundSetLooping(int soundIndex, bool looping) {
	LOGE("incomplete function 23 called");
}

void javaMuteSounds(bool mute) {
	muteSounds = mute;
}

/////////////////////////////////////
// Sockets
/////////////////////////////////////

int javaCreateSocket(const char *address, int port) { 
	LOGE("incomplete function 25 called");
	return 0; 
}

void javaCloseSocket(int socketId) {
	LOGE("incomplete function 26 called");
}

void javaSocketWrite(int socketId, const char *bytes, int sizeBytes) {
	LOGE("incomplete function 27 called");
}

void *javaSocketRead(int socketIndex) { 
	LOGE("incomplete function 28 called");
	return 0; 
}

/////////////////////////////////////
// Misc
/////////////////////////////////////

bool javaUsesCoverageAa() { return false; }

void javaLongToast(const char *toastText) {
	LOGE("incomplete function 4 called");
}

void javaShortToast(const char *toastText) {
	LOGE("incomplete function 5 called");
}

#include <unistd.h> //<cstdlib.h>
void javaBrowseUrl(const char *url, const char *alternativeUrl) {

	const char *browser = "xdg-open";
    
	char *args[3];
	args[0] = (char *) browser;
	args[1] = (char *) url;
	args[2] = 0;

	pid_t pid = fork();
	if(!pid) {
		execvp(browser, args);
		exit(0);
	}
}

float javaGetDpi() {
	return dpi;
}

float javaGetTimeMillis() { 
	LOGE("incomplete function 29 called");
	return 1; 
}

void javaFinishActivity() {
	Nongl::scheduler.schedule([](void *) -> bool {
		Nongl::onPause(true);
		Nongl::exit();
		return true;
	});
}

void javaPreventSleep(bool prevent) {}

void javaOpenOptionsMenu() {
	LOGE("incomplete function 31 called");
}

void javaShowVirtualKeyboard() {}
void javaHideVirtualKeyboard() {}

#include <curl/curl.h>

TextBuffer systemUrlEncode(const TextBuffer &text) {

    TextBuffer encodedData;

    CURL *curl;
    curl = curl_easy_init();
    if (curl) {
        char *urlEncodedData = curl_easy_escape(curl, text.getConstText(), text.getTextLength());
        encodedData = urlEncodedData;
        curl_free(urlEncodedData);

        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }

    return encodedData;
}

std::vector<uint8_t> systemHttpRequest(const TextBuffer &url, const TextBuffer &data, bool isPost) {

    // The true signature: typedef size_t (*CurlWriteFunctionPointer)(void*, size_t, size_t, void*);
    typedef size_t (*CurlWriteFunctionPointer)(const uint8_t *, size_t, size_t, std::vector<uint8_t> *);

    auto writeDataFunction = [] (const uint8_t *dataToWrite, size_t size, size_t nmemb, std::vector<uint8_t> *dataDestination) -> size_t {
        int bytesCount = size * nmemb;
        dataDestination->insert(dataDestination->end(), dataToWrite, dataToWrite + bytesCount);
        return bytesCount;
    };

    std::vector<uint8_t> response;

    CURL *curl;
    CURLcode result;

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (CurlWriteFunctionPointer) writeDataFunction);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

        if (isPost) {
            curl_easy_setopt(curl, CURLOPT_URL, url.getConstText());
            curl_easy_setopt(curl, CURLOPT_POST, 1);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.getConstText());
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, data.getTextLength());
        } else {
            curl_easy_setopt(curl, CURLOPT_POST, 0);

            if (data.isEmpty()) {
                curl_easy_setopt(curl, CURLOPT_URL, url.getConstText());
            } else {
                TextBuffer urlWithParameters(url);
                urlWithParameters.append('?').append(data);
                curl_easy_setopt(curl, CURLOPT_URL, urlWithParameters.getConstText());
            }
        }

        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        result = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if(result != CURLE_OK) LOGE("curl_easy_perform() failed: %s\n", curl_easy_strerror(result ));
    }

    return response;
}

std::vector<uint8_t> systemDownloadFile(const TextBuffer &url) {

	// The true signature: typedef size_t (*CurlWriteFunctionPointer)(void*, size_t, size_t, void*);
	typedef size_t (*CurlWriteFunctionPointer)(const uint8_t *, size_t, size_t, std::vector<uint8_t> *);

	auto writeDataFunction = [] (const uint8_t *dataToWrite, size_t size, size_t nmemb, std::vector<uint8_t> *dataDestination) -> size_t {
		int bytesCount = size * nmemb;
		dataDestination->insert(dataDestination->end(), dataToWrite, dataToWrite + bytesCount);
	    return bytesCount;
	};

	std::vector<uint8_t> downloadedFile;

	CURL *curl;
    CURLcode result;

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.getConstText());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, (CurlWriteFunctionPointer) writeDataFunction);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &downloadedFile);
        //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        result = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }

    // TODO: do something with result...

    return downloadedFile;
}

TextBuffer systemGetNetworkCountryCode() {

	// Get ip data from online api,
	std::vector<uint8_t> jsonData = systemDownloadFile("http://ip-api.com/json");
	if (!jsonData.size()) return TextBuffer();

	// Null terminate the json string,
	jsonData.push_back(0);

	// Search for "countryCode",
	std::string str((const char *) &jsonData[0]);
	std::size_t index = str.find("\"countryCode\"");
	if (index != std::string::npos) {
		index += sizeof("\"countryCode\":\"") - 1;
		return TextBuffer().
				append((char) toupper(jsonData[index  ])).
				append((char) toupper(jsonData[index+1]));
	}

	return TextBuffer();
}

void javaExit() {
	exitApplication = true;
}
