
#if NONGL_GAME_SERVICES

#include "../../../SystemGameServices.h"
#include "../../../SystemLog.h"
#include "../../../Nongl.h"

#include "NativeInterface.h"

#include <stdexcept>

//////////////////////////////
// Internal functions
//////////////////////////////

void javaGameServicesResetSignInState() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesResetSignInState", "()V");
    jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method);
}

bool javaGameServicesAchievementsDidLoad() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesAchievementsDidLoad", "()Z");
    bool value = jniState.env->CallStaticBooleanMethod(jniState.interfaceClass, method);

	return value;
}

int javaGameServicesGetLoadedAchievementsCount() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesGetLoadedAchievementsCount", "()I");
	int value = jniState.env->CallStaticIntMethod(jniState.interfaceClass, method);

	return value;
}

TextBuffer javaGameServicesGetLoadedAchievementId(int achievementIndex) {

	JniState jniState;

	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesGetLoadedAchievementId", "(I)Ljava/lang/String;");
	jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method, achievementIndex);

	// Check for null,
	if (jString == 0) {
		TextBuffer errorMessage;
		errorMessage.append("javaGameServicesGetLoadedAchievementId(").append(achievementIndex).append(") failed.");
		throw std::runtime_error(errorMessage.getConstText());
	}

	// Copy the output string,
	const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
	TextBuffer outputText(nativeString);

	// Release the output string,
	jniState.env->ReleaseStringUTFChars(jString, nativeString);

	return outputText;
}

float javaGameServicesGetLoadedAchievementPercentCompleted(int achievementIndex) {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesGetLoadedAchievementPercentCompleted", "(I)F");
	float value = jniState.env->CallStaticFloatMethod(jniState.interfaceClass, method, achievementIndex);

	return value;
}

//////////////////////////////
// System game services
//////////////////////////////

void systemGameServicesConnect() {}

void systemGameServicesShowSignInDialog(float delayMillis) {

	if (delayMillis > 0) {
		javaGameServicesResetSignInState();
		Nongl::scheduler.schedule([](void *)->bool {
			systemGameServicesShowSignInDialog(0);
			return true;
		}, delayMillis);
		return ;
	}

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesShowSignInDialog", "()V");
	jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method);
}

bool systemGameServicesIsSignInComplete() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesIsSignInComplete", "()Z");
    bool value = jniState.env->CallStaticBooleanMethod(jniState.interfaceClass, method);

	return value;
}

bool systemGameServicesIsUserSignedIn() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesIsUserSignedIn", "()Z");
    bool value = jniState.env->CallStaticBooleanMethod(jniState.interfaceClass, method);

	return value;
}

void systemGameServicesShowServicesDialog() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesShowServicesDialog", "()V");
	jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method);
}

void systemGameServicesSubmitAchievement(const char *achievementIdentifier, float percent, bool showBanner) {
	// This method is to avoid default initializing onSubmittedTask in header file, and hance having
	// to include ManagedObject.h,
	systemGameServicesSubmitAchievement(achievementIdentifier, percent, showBanner, nullptr);
}

void systemGameServicesSubmitAchievement(
		const char *achievementIdentifier, float percent, bool showBanner,
		const Ngl::ManagedPointer<Runnable> &onSubmittedTask, void *onSubmittedTaskData) {

	// TODO: use the rest of the parameters...
	JniState jniState;
    jstring jstr = jniState.env->NewStringUTF(achievementIdentifier);
    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesSubmitAchievement", "(Ljava/lang/String;Z)V");
    jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method, jstr, showBanner);
    jniState.env->DeleteLocalRef(jstr);

	// TODO: implement a real way of tracking the result before calling
	// onSubmittedTask...
	Nongl::scheduler.schedule(onSubmittedTask, 0, onSubmittedTaskData);
}

void systemGameServicesResetAchievements() {
	LOGE("systemGameServicesResetAchievements() does nothing in Google Play Games Services.");
}

void systemGameServicesGetAchievements(const Ngl::ManagedPointer<Runnable> &onAchievementsLoadedTask) {

	// Trigger loading achievements,
	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGameServicesLoadAchievements", "()V");
	jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method);

	// Constantly pull the java end for the results,
	class AchievementsLoadedPollingTask : public Runnable {
		Ngl::ManagedPointer<Runnable> onAchievementsLoadedTask;
	public:
		AchievementsLoadedPollingTask(const Ngl::ManagedPointer<Runnable> &onAchievementsLoadedTask) : Runnable(0) {
			this->onAchievementsLoadedTask = onAchievementsLoadedTask;
		}

		bool run(void *) {

			// If achievements finally loaded,
			if (javaGameServicesAchievementsDidLoad()) {

				// Collect achievements,
				std::vector<Achievement> achievements;
				int achievementsCount = javaGameServicesGetLoadedAchievementsCount();
				for (int i=0; i<achievementsCount; i++) {
				    achievements.emplace_back(
				    		javaGameServicesGetLoadedAchievementId(i),
				    		javaGameServicesGetLoadedAchievementPercentCompleted(i));
				}

				// Report to the on loaded task,
				if (!onAchievementsLoadedTask.expired()) onAchievementsLoadedTask->run(&achievements);

				return true;
			}
			return false;
		}
	};

	Ngl::shared_ptr<Runnable> achievementsLoadedPollingTask(new AchievementsLoadedPollingTask(onAchievementsLoadedTask));
	Nongl::scheduler.schedule(achievementsLoadedPollingTask);
}

#endif
