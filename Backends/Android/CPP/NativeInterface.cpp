#include "NativeInterface.h"

#include "../../../SystemEvents.h"
#include "../../../SystemLog.h"
#include "../../../Nongl.h"

//////////////////////////////////////////////
// Native side functions declarations
//////////////////////////////////////////////

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogI)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogW)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogE)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeReleaseOpenGlObjects)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnPause)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnResume)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnSurfaceCreated)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnSurfaceChanged)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint width, jint height);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnDrawFrame)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchDown)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchDrag)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchUp)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchCancel)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnScroll)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jfloat x, jfloat y, jfloat xOffset, jfloat yOffset);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnBackPressed)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnMenuPressed)(JNI_FUNCTION_SIGNATURE_PREFIX);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnKeyDown)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint keyCode);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnKeyUp)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint keyCode);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnYesNoDialogResult)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint dialogId, jboolean yes);
JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnMenuItemSelected)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint itemId);

//////////////////////////////////////////////
// Native side functions implementations
//////////////////////////////////////////////

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogI)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message) {

	if (message == 0) return ;
	const char *nativeString = env->GetStringUTFChars(message, 0);
	LOGI("%s", nativeString);
	env->ReleaseStringUTFChars(message, nativeString);
}

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogW)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message) {

	if (message == 0) return ;
	const char *nativeString = env->GetStringUTFChars(message, 0);
	LOGW("%s", nativeString);
	env->ReleaseStringUTFChars(message, nativeString);
}

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeLogE)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jstring message) {

	if (message == 0) return ;
	const char *nativeString = env->GetStringUTFChars(message, 0);
	LOGE("%s", nativeString);
	env->ReleaseStringUTFChars(message, nativeString);
}

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeReleaseOpenGlObjects)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	Nongl::discardOpenGlObjects();
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnPause)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	Nongl::onPause(true);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnResume)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	Nongl::onResume(true);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnSurfaceCreated)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	Nongl::onSurfaceCreated();
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnSurfaceChanged)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint width, jint height) {
	Nongl::onSurfaceChanged(width, height);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnDrawFrame)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	Nongl::onNewFrame();
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchDown)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y) {
	systemEventTouchDown(pointerId, x, y);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchDrag)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y) {
	systemEventTouchDrag(pointerId, x, y);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchUp)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y) {
	systemEventTouchUp(pointerId, x, y);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnTouchCancel)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint pointerId, jfloat x, jfloat y) {
	systemEventTouchCancel(pointerId, x, y);
}

JNI_CALL_PREFIX JNIEXPORT void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnScroll)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jfloat x, jfloat y, jfloat xOffset, jfloat yOffset) {
	systemEventScroll(x, y, xOffset, yOffset);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnBackPressed)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	systemEventBackPressed();
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnMenuPressed)(JNI_FUNCTION_SIGNATURE_PREFIX) {
	systemEventMenuPressed();
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnKeyDown)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint keyCode) {
	systemEventKeyDown(keyCode);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnKeyUp)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint keyCode) {
	systemEventKeyUp(keyCode);
}

JNI_CALL_PREFIX void JNICALL JNI_FUNCTION_NAME_PREFIX(nativeOnMenuItemSelected)(JNI_FUNCTION_SIGNATURE_PREFIX_WITH_COMMA jint itemId) {
	Nongl::onMenuItemSelected(itemId);
}
