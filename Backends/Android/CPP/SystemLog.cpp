#include "../../../SystemLog.h"

#include <stdarg.h>
#include <stdio.h>
#include <vector>
#include <android/log.h>

#define LOG_IMPLEMENTATION(logLevel) \
	static std::vector<char> message; \
	va_list arguments; \
	va_start(arguments, format); \
	int textLength = vsnprintf(&message[0], message.size(), format, arguments) + 1; \
	va_end(arguments); \
	if (textLength > message.size()) { \
		message.resize(textLength); \
		va_start(arguments, format); \
		vsnprintf(&message[0], textLength, format, arguments); \
		va_end(arguments); \
	} \
	__android_log_print(logLevel, LOG_TAG, "%s", &message[0]);

void logI(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION(ANDROID_LOG_INFO); }
void logW(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION(ANDROID_LOG_WARN); }
void logE(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION(ANDROID_LOG_ERROR); }
