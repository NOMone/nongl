// TODO: revise this file to release all java objects that are not explicitly released,

#include "NativeInterface.h"
#include "../../../SystemInterface.h"

#include <string.h>
#include <string>
#include <stdexcept>

static const char packageName[] = NATIVES_CLASS;
static JavaVM *currentJvm=0;
static jclass interfaceClass;

JniState::JniState() {

	this->interfaceClass = ::interfaceClass;
	shouldDetachThread = false;
	if (currentJvm->GetEnv((void **)&env, JNI_VERSION_1_6) == JNI_EDETACHED) {
		currentJvm->AttachCurrentThread(&env, nullptr);
		shouldDetachThread = true;
	}
}

JniState::~JniState() {
    if (shouldDetachThread) currentJvm->DetachCurrentThread();
}

extern "C" JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {

	currentJvm = jvm;

	JNIEnv *env;
	currentJvm->GetEnv((void **)&env, JNI_VERSION_1_6);
	interfaceClass = env->FindClass(packageName);
	interfaceClass = (jclass) env->NewGlobalRef(interfaceClass);

	//env->RegisterNatives(clazz, sMethods, 1);

	return JNI_VERSION_1_6;
}

/////////////////////////////////////
// Preferences
/////////////////////////////////////

void javaSetBooleanPreference(const char *key, bool value) {

	JniState jniState;
	jstring keyString = jniState.env->NewStringUTF(key);
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaSetBooleanPreference", "(Ljava/lang/String;Z)V");
	jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method, keyString, value);

	// Release the input strings,
	jniState.env->DeleteLocalRef(keyString);
}

bool javaGetBooleanPreference(const char *key, bool defaultValue) {

	JniState jniState;

	jstring keyString = jniState.env->NewStringUTF(key);
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGetBooleanPreference", "(Ljava/lang/String;Z)Z");
	bool value = jniState.env->CallStaticBooleanMethod(jniState.interfaceClass, method, keyString, defaultValue);

	// Release the input strings,
	jniState.env->DeleteLocalRef(keyString);

	return value;
}

void javaSetIntPreference(const char *key, int32_t value) {

	JniState jniState;

	jstring keyString = jniState.env->NewStringUTF(key);
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaSetIntPreference", "(Ljava/lang/String;I)V");
	jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method, keyString, value);

	// Release the input strings,
	jniState.env->DeleteLocalRef(keyString);
}

int32_t javaGetIntPreference(const char *key, int32_t defaultValue) {

	JniState jniState;

	jstring keyString = jniState.env->NewStringUTF(key);
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGetIntPreference", "(Ljava/lang/String;I)I");
	int32_t value = jniState.env->CallStaticIntMethod(jniState.interfaceClass, method, keyString, defaultValue);

	// Release the input strings,
	jniState.env->DeleteLocalRef(keyString);

	return value;
}

void javaSetStringPreference(const char *key, const TextBuffer &value) {

    JniState jniState;

    jstring   keyString = jniState.env->NewStringUTF(key  );
    jstring valueString = jniState.env->NewStringUTF(value);

    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaSetStringPreference", "(Ljava/lang/String;Ljava/lang/String;)V");
    jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method, keyString, valueString);

    // Release the input strings,
    jniState.env->DeleteLocalRef(  keyString);
    jniState.env->DeleteLocalRef(valueString);
}

TextBuffer javaGetStringPreference(const char *key, const TextBuffer &defaultValue) {

    JniState jniState;

    jstring          keyString = jniState.env->NewStringUTF(key         );
    jstring defaultValueString = jniState.env->NewStringUTF(defaultValue);

    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGetStringPreference", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
    jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method, keyString, defaultValueString);

    // Release the input strings,
    jniState.env->DeleteLocalRef(         keyString);
    jniState.env->DeleteLocalRef(defaultValueString);

    // Check for null,
    if (jString == 0) return TextBuffer();

    // Copy the output string,
    const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
    TextBuffer preferenceValue(nativeString);

    // Release the output string,
    jniState.env->ReleaseStringUTFChars(jString, nativeString);

    return preferenceValue;
}

/////////////////////////////////////
// Music
/////////////////////////////////////

int javaLoadMusicFromAssets(const char *filename) {

	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jstring filenameString = currentEnv->NewStringUTF(filename);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaLoadMusicFromAssets", "(Ljava/lang/String;)I");
    int musicId = currentEnv->CallStaticIntMethod(clazz, method, filenameString);

    // Release the input string,
    currentEnv->DeleteLocalRef(filenameString);

    return musicId;
}

void javaDeleteMusic(int musicIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaDeleteMusic", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex);
}

void javaPlayMusic(int musicIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaPlayMusic", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex);
}

void javaMusicSetLooping(int musicIndex, bool looping) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaMusicSetLooping", "(IZ)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex, looping);
}

void javaStopMusic(int musicIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaStopMusic", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex);
}

void javaPauseMusic(int musicIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaPauseMusic", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex);
}

void javaResumeMusic(int musicIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaResumeMusic", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex);
}

void javaFadeOutMusic(int musicIndex, int milliSeconds) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaFadeOutMusic", "(II)V");
    currentEnv->CallStaticVoidMethod(clazz, method, musicIndex, milliSeconds);
}

/////////////////////////////////////
// Sound
/////////////////////////////////////

int javaLoadSoundFromAssets(const char *filename)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jstring filenameString = currentEnv->NewStringUTF(filename);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaLoadSoundFromAssets", "(Ljava/lang/String;)I");
    int soundId = currentEnv->CallStaticIntMethod(clazz, method, filenameString);

    // Release the input string,
    currentEnv->DeleteLocalRef(filenameString);

    return soundId;
}

void javaDeleteSound(int soundIndex) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaDeleteSound", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex);
}

void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaPlaySound", "(IFFF)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex, rate, leftVolume, rightVolume);
}

void javaSoundSetLooping(int soundIndex, bool looping) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaSoundSetLooping", "(IZ)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex, looping);
}

void javaStopSound(int soundIndex)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaStopSound", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex);
}

void javaPauseSound(int soundIndex)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaPauseSound", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex);
}

void javaResumeSound(int soundIndex)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaResumeSound", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, soundIndex);
}

void javaMuteSounds(bool mute)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaMuteSounds", "(Z)V");
    currentEnv->CallStaticVoidMethod(clazz, method, mute);
}

/////////////////////////////////////
// Sockets
/////////////////////////////////////

int javaCreateSocket(const char *address, int port) {

	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jstring addressString = currentEnv->NewStringUTF(address);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaCreateSocket", "(Ljava/lang/String;I)I");
    int socketId = currentEnv->CallStaticIntMethod(clazz, method, addressString, port);

    // Release the input string,
    currentEnv->DeleteLocalRef(addressString);

    return socketId;
}

void javaCloseSocket(int socketId) {

	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaCloseSocket", "(I)V");
    currentEnv->CallStaticVoidMethod(clazz, method, socketId);
}

void javaSocketWrite(int socketId, const char *bytes, int sizeBytes) {

	JNIEnv *currentEnv;
	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);

	// Create a java byte array for the data,
	jbyteArray javaByteArray = currentEnv->NewByteArray(sizeBytes);

	// Fill the array,
	currentEnv->SetByteArrayRegion(javaByteArray, 0, sizeBytes, (const jbyte *) bytes);

	// Get and call the method,
	jclass clazz = currentEnv->FindClass(packageName);
	jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaSocketWrite", "(I[B)V");
    currentEnv->CallStaticVoidMethod(clazz, method, socketId, javaByteArray);

    // Release the byte array,
    currentEnv->DeleteLocalRef(javaByteArray);
}

void *javaSocketRead(int socketIndex) {

	JNIEnv *currentEnv;
	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaSocketRead", "(I)[B");
    jbyteArray javaByteArray = (jbyteArray) currentEnv->CallStaticObjectMethod(clazz, method, socketIndex);

    // Check for null,
    if (javaByteArray == 0)
    	return 0;

    // Copy the array,
    int arrayLength = currentEnv->GetArrayLength(javaByteArray);
    signed char *outputArray = (signed char *) malloc(arrayLength);
    currentEnv->GetByteArrayRegion(javaByteArray, 0, arrayLength, outputArray);

    // Release the java objects,
    currentEnv->DeleteLocalRef(clazz);
    currentEnv->DeleteLocalRef(javaByteArray);

    return outputArray;
}

/////////////////////////////////////
// Misc
/////////////////////////////////////

bool javaUsesCoverageAa() {

	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaUsesCoverageAa", "()Z");
    return currentEnv->CallStaticBooleanMethod(clazz, method);
}

void javaLongToast(const char *toastText)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jstring toastTextString = currentEnv->NewStringUTF(toastText);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaLongToast", "(Ljava/lang/String;)V");
    currentEnv->CallStaticVoidMethod(clazz, method, toastTextString);

    // Release the input string,
    currentEnv->DeleteLocalRef(toastTextString);
}

void javaShortToast(const char *toastText)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);

    jstring toastTextString = currentEnv->NewStringUTF(toastText);

    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaShortToast", "(Ljava/lang/String;)V");
    currentEnv->CallStaticVoidMethod(clazz, method, toastTextString);

    // Release the input string,
    currentEnv->DeleteLocalRef(toastTextString);
}

void javaBrowseUrl(const char *url, const char *alternativeUrl)
{
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
	jclass clazz = currentEnv->FindClass(packageName);

	jstring urlString = currentEnv->NewStringUTF(url);
	jstring alternativeUrlString = currentEnv->NewStringUTF(alternativeUrl);

	jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaBrowseUrl", "(Ljava/lang/String;Ljava/lang/String;)V");
	currentEnv->CallStaticVoidMethod(clazz, method, urlString, alternativeUrlString);

	// Release the input strings,
	currentEnv->DeleteLocalRef(urlString);
	currentEnv->DeleteLocalRef(alternativeUrlString);
}

float javaGetDpi() {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaGetDpi", "()F");
    return currentEnv->CallStaticFloatMethod(clazz, method);
}

float javaGetTimeMillis() {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaGetTimeMillis", "()F");
    return currentEnv->CallStaticFloatMethod(clazz, method);
}

void javaFinishActivity() {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaFinishActivity", "()V");
    currentEnv->CallStaticVoidMethod(clazz, method);
}

void javaPreventSleep(bool prevent) {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
	jclass clazz = currentEnv->FindClass(packageName);
	jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaPreventSleep", "(Z)V");
	currentEnv->CallStaticVoidMethod(clazz, method, prevent);
}

void javaOpenOptionsMenu() {
	JNIEnv *currentEnv;
	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaOpenOptionsMenu", "()V");
    currentEnv->CallStaticVoidMethod(clazz, method);
}

void javaShowVirtualKeyboard() {
	JNIEnv *currentEnv;
	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaShowVirtualKeyboard", "()V");
    currentEnv->CallStaticVoidMethod(clazz, method);
}

void javaHideVirtualKeyboard() {
	JNIEnv *currentEnv;
	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaHideVirtualKeyboard", "()V");
    currentEnv->CallStaticVoidMethod(clazz, method);
}

TextBuffer systemUrlEncode(const TextBuffer &text) {

    JniState jniState;

    // Create jstring objects for string inputs,
    jstring textJString = jniState.env->NewStringUTF(text.getConstText());

    // Get the method id,
    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaUrlEncode", "(Ljava/lang/String;)Ljava/lang/String;");

    // Call the method,
    jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method, textJString);

    // Release the input string,
    jniState.env->DeleteLocalRef(textJString);

    // Check for null,
    if (jString == 0) return TextBuffer();

    // Copy the output string,
    const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
    TextBuffer encodedText(nativeString);

    // Release the output string,
    jniState.env->ReleaseStringUTFChars(jString, nativeString);

    return encodedText;
}

std::vector<uint8_t> systemHttpRequest(const TextBuffer &url, const TextBuffer &data, bool isPost) {

    JniState jniState;

    // Create jstring objects for string inputs,
    jstring  urlJString = jniState.env->NewStringUTF( url.getConstText());
    jstring dataJString = jniState.env->NewStringUTF(data.getConstText());

    // Get the method id,
    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaHttpRequest", "(Ljava/lang/String;Ljava/lang/String;Z)[B");

    // Call the method,
    jbyteArray responseJArray = (jbyteArray) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method, urlJString, dataJString, isPost);

    // Release the input strings,
    jniState.env->DeleteLocalRef( urlJString);
    jniState.env->DeleteLocalRef(dataJString);

    // Check for null,
    std::vector<uint8_t> responseData;
    if (responseJArray == 0) return responseData;

    // Get the array size,
    int sizeBytes = jniState.env->GetArrayLength(responseJArray);
    responseData.resize(sizeBytes);

    // Copy the array,
    jniState.env->GetByteArrayRegion(responseJArray, 0, sizeBytes, (jbyte *) &responseData[0]);

    // Release the array,
    jniState.env->DeleteLocalRef(responseJArray);

    return responseData;
}

std::vector<uint8_t> systemDownloadFile(const TextBuffer &url) {

    JniState jniState;

	// Create a String object,
    jstring jstr = jniState.env->NewStringUTF(url.getConstText());

    // Get the method id,
    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaDownloadFile", "(Ljava/lang/String;)[B");

    // Call the method,
    jbyteArray jarray = (jbyteArray) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method, jstr);

    // Release the input string,
    jniState.env->DeleteLocalRef(jstr);

    // Check for null,
    std::vector<uint8_t> fileData;
    if (jarray == 0) return fileData;

    // Get the array size,
    int sizeBytes = jniState.env->GetArrayLength(jarray);
	fileData.resize(sizeBytes);

    // Copy the array,
	jniState.env->GetByteArrayRegion(jarray, 0, sizeBytes, (jbyte *) &fileData[0]);

    // Release the array,
	jniState.env->DeleteLocalRef(jarray);

    return fileData;
}

static TextBuffer getNetworkCountryCode() {

	// Get ip data from online api,
	std::vector<uint8_t> jsonData = systemDownloadFile("http://ip-api.com/json");
	if (!jsonData.size()) return TextBuffer();

	// Null terminate the json string,
	jsonData.push_back(0);

	// Search for "countryCode",
	std::string str((const char *) &jsonData[0]);
	std::size_t index = str.find("\"countryCode\"");
	if (index != std::string::npos) {
		index += sizeof("\"countryCode\":\"") - 1;
		return TextBuffer().
				append((char) toupper(jsonData[index  ])).
				append((char) toupper(jsonData[index+1]));
	}

	return TextBuffer();
}

static TextBuffer javeGetNetworkCountryCode() {

	JniState jniState;

	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGetNetworkCountryCode", "()Ljava/lang/String;");
	jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method);

	// Check for null,
	if (jString == 0) return TextBuffer();

	// Copy the output string,
	const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
	TextBuffer countryCode(nativeString);

	// Release the output string,
	jniState.env->ReleaseStringUTFChars(jString, nativeString);

	return countryCode;
}

#define NETWORK_COUNTRY_CODE_INT_PREFERENCE "Nongl network country code"

TextBuffer systemGetNetworkCountryCode() {

	TextBuffer onlineNetworkCountryCode = getNetworkCountryCode();
	if (!onlineNetworkCountryCode.isEmpty()) {

		// Save online country code to a preference,
	    int32_t countryCodeInteger=0;
	    int32_t textLength = onlineNetworkCountryCode.getTextLength();
	    for (int32_t i=0; i<textLength; i++) {
	    	countryCodeInteger |= onlineNetworkCountryCode[i] << (8*i);
	    }
	    javaSetIntPreference(NETWORK_COUNTRY_CODE_INT_PREFERENCE, countryCodeInteger);
		return onlineNetworkCountryCode;
	} else {

		// Get online country code from prefrence,
		int32_t countryCodeInteger = javaGetIntPreference(NETWORK_COUNTRY_CODE_INT_PREFERENCE, 0);
		if (countryCodeInteger) {
			TextBuffer countryCode;
			while (countryCodeInteger) {
				countryCode.append((char) (countryCodeInteger & 255));
				countryCodeInteger >>= 8;
			}
			return countryCode;
		}

		// No online country code was ever retrieved, just fallback to SIM and network methods,
		return javeGetNetworkCountryCode();
	}
}

void javaExit() {
	JNIEnv *currentEnv;

	currentJvm->GetEnv((void **)&currentEnv, JNI_VERSION_1_6);
    jclass clazz = currentEnv->FindClass(packageName);
    jmethodID method = currentEnv->GetStaticMethodID(clazz, "javaExit", "()V");
    currentEnv->CallStaticVoidMethod(clazz, method);
}
