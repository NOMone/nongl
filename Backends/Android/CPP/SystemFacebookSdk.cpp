
#if NONGL_FACEBOOK_SDK

#include <Nongl/SystemFacebookSdk.h>
#include <Nongl/TextUtils.h>
#include <Nongl/ManagedObject.h>
#include <Nongl/Threads.h>
#include <Nongl/Nongl.h>
#include <Nongl/SystemInterface.h>
#include "NativeInterface.h"

#include <functional>

using namespace Ngl;

//////////////////////////////
// Internal functions
//////////////////////////////

static void javaFacebookSdkLogInWithReadPermissions(const TextBuffer &permissions) {

    JniState jniState;
    jstring jstr = jniState.env->NewStringUTF(permissions.getConstText());
    jmethodID method = jniState.env->GetStaticMethodID(
            jniState.interfaceClass,
            "javaFacebookSdkLogInWithReadPermissions", "(Ljava/lang/String;)V");
    jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method, jstr);
    jniState.env->DeleteLocalRef(jstr);
}

static FacebookLoginResponseType javaFacebookSdkGetLoginResponseType() {

    JniState jniState;
    jmethodID method = jniState.env->GetStaticMethodID(
            jniState.interfaceClass,
            "javaFacebookSdkGetLoginResponseType", "()I");
    int32_t responseTypeInt = jniState.env->CallStaticIntMethod(jniState.interfaceClass, method);

    switch (responseTypeInt) {
        case 0: return FacebookLoginResponseType::UNKNOWN;
        case 1: return FacebookLoginResponseType::NOT_RECEIVED;
        case 2: return FacebookLoginResponseType::FACEBOOK_NOT_INTEGRATED;
        case 3: return FacebookLoginResponseType::SUCCESS;
        case 4: return FacebookLoginResponseType::CANCEL;
        case 5: return FacebookLoginResponseType::ERROR;
    }
    return FacebookLoginResponseType::UNKNOWN;
}

static TextBuffer javaFacebookSdkGetLoginErrorMessage() {

    JniState jniState;

    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaFacebookSdkGetLoginErrorMessage", "()Ljava/lang/String;");
    jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method);

    // Check for null,
    if (jString == 0) return TextBuffer();

    // Copy the output string,
    const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
    TextBuffer errorMessage(nativeString);

    // Release the output string,
    jniState.env->ReleaseStringUTFChars(jString, nativeString);

    return errorMessage;
}

static TextBuffer javaFacebookSdkGetCurrentAccessToken() {

    JniState jniState;

    jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaFacebookSdkGetCurrentAccessToken", "()Ljava/lang/String;");
    jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method);

    // Check for null,
    if (jString == 0) return TextBuffer();

    // Copy the output string,
    const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
    TextBuffer accessToken(nativeString);

    // Release the output string,
    jniState.env->ReleaseStringUTFChars(jString, nativeString);

    return accessToken;
}

static void javaFacebookSdkLogOut() {

    JniState jniState;
    jmethodID method = jniState.env->GetStaticMethodID(
            jniState.interfaceClass,
            "javaFacebookSdkLogOut", "()V");
    jniState.env->CallStaticVoidMethod(jniState.interfaceClass, method);
}

//////////////////////////////
// System Facebook SDK
//////////////////////////////

// Note: appId and clientToken parameters are not used in android login. Instead,
// appId is specified in the manifest,
void systemFacebookSdkLogInWithReadPermissions(
        const TextBuffer &appId,
        const TextBuffer &clientToken,
        const TextBuffer &permissions,
        const Ngl::ManagedPointer<std::function<void(const Ngl::FacebookLoginResponseType &responseType, const TextBuffer &errorMessage)> > &onResponseReceivedListener) {

    javaFacebookSdkLogInWithReadPermissions(permissions);

    typedef void (ListenerType)(const FacebookLoginResponseType &, const TextBuffer &);

    // Constantly pull the java end for the login response,
    class LoginResponsePollingTask : public Runnable {
        ManagedPointer<std::function<ListenerType> > onResponseReceivedListener;
    public:
        LoginResponsePollingTask(const ManagedPointer<std::function<ListenerType> > &onResponseReceivedListener) : Runnable(0) {
            this->onResponseReceivedListener = onResponseReceivedListener;
        }

        bool run(void *) {

            // Terminate immediately of listener expired,
            if (onResponseReceivedListener.expired()) return true;

            // If response finally received,
            FacebookLoginResponseType loginResponse = javaFacebookSdkGetLoginResponseType();
            if (loginResponse != FacebookLoginResponseType::NOT_RECEIVED) {

                // Persist access token if login successful,
                if (loginResponse == FacebookLoginResponseType::SUCCESS) {
                    javaSetStringPreference(FACEBOOK_ACCESS_TOKEN_PREFERENCE, javaFacebookSdkGetCurrentAccessToken());
                }

                // Report to the listener,
                (*onResponseReceivedListener)(
                        loginResponse,
                        javaFacebookSdkGetLoginErrorMessage());

                return true;
            }
            return false;
        }
    };

    Ngl::shared_ptr<Runnable> loginResponsePollingTask(new LoginResponsePollingTask(onResponseReceivedListener));
    Nongl::scheduler.schedule(loginResponsePollingTask);
}

void systemFacebookSdkLogOut() {
    javaFacebookSdkLogOut();
}

#endif
