
#include "../../../SystemFileServices.h"
#include "../../../TextUtils.h"
#include "../../../File.h"

#include "NativeInterface.h"

#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include <sys/stat.h>
#include <stdexcept>

//////////////////////////////
// Internal functions
//////////////////////////////

static AAssetManager *javaGetAssetsManager() {

	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaGetAssetsManager", "()Landroid/content/res/AssetManager;");
	jobject jObject = jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method);

	// Check for null,
	if (jObject == 0) {
		throw std::runtime_error("javaGetAssetsManager() failed.");
	}

	AAssetManager *androidAssetManager = AAssetManager_fromJava(jniState.env, jObject);

	return androidAssetManager;
}

static TextBuffer javaCreateAndGetApplicationExternalStorageAddress() {

	static bool alreadyRetrieved = false;
	static TextBuffer storageAddress;

	if (alreadyRetrieved) return storageAddress;

	// Retrieve the storage address,
	JniState jniState;
	jmethodID method = jniState.env->GetStaticMethodID(jniState.interfaceClass, "javaCreateAndGetApplicationExternalStorageAddress", "()Ljava/lang/String;");
	jstring jString = (jstring) jniState.env->CallStaticObjectMethod(jniState.interfaceClass, method);

	// Check for null,
	if (jString == 0) throw std::runtime_error("javaCreateAndGetApplicationExternalStorageAddress() failed.");

	// Copy the output string,
	const char *nativeString = jniState.env->GetStringUTFChars(jString, 0);
	storageAddress.append(nativeString);

	// Release the output string,
	jniState.env->ReleaseStringUTFChars(jString, nativeString);

	// Mark the storage address as already retrieved to save jni calls in the future,
	alreadyRetrieved = true;

	return storageAddress;
}

static TextBuffer getApplicationExternalStorageFileAddress(const TextBuffer &filePath) {
	return javaCreateAndGetApplicationExternalStorageAddress().append('/').append(filePath);
}

static bool assetFileExists(const Ngl::File &file) {

	AAssetManager *androidAssetManager = javaGetAssetsManager();
    AAsset *fileHandle = AAssetManager_open(androidAssetManager, file.getFullPath(), AASSET_MODE_UNKNOWN);
    if (fileHandle) {
        AAsset_close(fileHandle);
        return true;
    }
    return false;
}

static bool sdCardFileExists(const Ngl::File &file) {

	TextBuffer fileFullPath = getApplicationExternalStorageFileAddress(file.getFullPath());
   	struct stat st;
    return stat(fileFullPath, &st) == 0;
}

static int32_t getAssetFileSize(const Ngl::File &file) {

	AAssetManager *androidAssetManager = javaGetAssetsManager();
    AAsset *fileHandle = AAssetManager_open(androidAssetManager, file.getFullPath(), AASSET_MODE_UNKNOWN);
    if (!fileHandle) {
    	throw std::runtime_error(TextBuffer("Couldn't open file to get size: ").append(file.getFullPath()).getConstText());
    }

    int32_t fileSize = (int32_t) AAsset_getLength(fileHandle);
	AAsset_close(fileHandle);
	return fileSize;
}

static int32_t getSdCardFileSize(const Ngl::File &file) {

	TextBuffer fileFullPath = getApplicationExternalStorageFileAddress(file.getFullPath());
	struct stat fileState;
	if (stat(fileFullPath, &fileState) != 0) {
		throw std::runtime_error(TextBuffer("Couldn't get file size (doesn't exist): ").append(fileFullPath).getConstText());
	}
	return (int32_t) fileState.st_size;
}

static int32_t readAssetFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) {

	// Open file,
	AAssetManager *androidAssetManager = javaGetAssetsManager();
    AAsset *fileHandle = AAssetManager_open(androidAssetManager, file.getFullPath(), AASSET_MODE_UNKNOWN);
    if (!fileHandle) {
    	throw std::runtime_error(TextBuffer("Couldn't open file to read: ").append(file.getFullPath()).getConstText());
    }

    // Get file size,
    int32_t fileSize = (int32_t) AAsset_getLength(fileHandle);

	// Force upper limit of read size,
	int32_t readSize = fileSize - offsetInFile;
	if (maxReadSize && (readSize > maxReadSize)) readSize = maxReadSize;

	// Forward to the offset,
	if (offsetInFile) {
		if (AAsset_seek(fileHandle, offsetInFile, SEEK_SET) < 0) {
			throw std::runtime_error(TextBuffer("Couldn't seek in file for reading: ").append(file.getFullPath()).getConstText());
		}
	}

	// Prepare the output vector to hold the file data,
	int32_t readBytesCount;
	if (offsetInVector == outputVector.size()) {

		// Adjust the original vector to hold the new data,
		int32_t originalVectorSize = outputVector.size();
		outputVector.resize(originalVectorSize + readSize);

		// Read the data into the output vector directly,
		readBytesCount = AAsset_read(fileHandle, &outputVector[originalVectorSize], readSize);
		if (readBytesCount < 0) {
			throw std::runtime_error(TextBuffer("Couldn't read from file: ").append(file.getFullPath()).getConstText());
		}

		// Fit the vector to the data,
		int32_t unreadBytesCount = readSize - readBytesCount;
		if (unreadBytesCount) outputVector.resize(originalVectorSize + readBytesCount);

	} else {

		// Read into a temporary vector first,
		std::vector<uint8_t> tempVector;
		tempVector.resize(readSize);

		// Read the data into the temporary vector,
		readBytesCount = AAsset_read(fileHandle, &tempVector[0], readSize);
		if (readBytesCount < 0) {
			throw std::runtime_error(TextBuffer("Couldn't read from file: ").append(file.getFullPath()).getConstText());
		}

		// Fit the vector to the data,
		int32_t unreadBytesCount = readSize - readBytesCount;
		if (unreadBytesCount) tempVector.resize(readBytesCount);

		// Insert temp vector into the output vector,
		outputVector.insert(outputVector.begin()+offsetInVector, tempVector.begin(), tempVector.end());
	}

	// Close the file,
	AAsset_close(fileHandle);

	return readBytesCount;
}

static int32_t readSdCardFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) {

	// Find the file final location,
	TextBuffer fileFullPath = getApplicationExternalStorageFileAddress(file.getFullPath());

	// Get file size to prepare the output vector,
	struct stat fileState;
	if (stat(fileFullPath, &fileState) != 0) {
		throw std::runtime_error(TextBuffer("Couldn't read file (doesn't exist): ").append(fileFullPath).getConstText());
	}
	int32_t fileSize = fileState.st_size;

	// Force upper limit of read size,
	int32_t readSize = fileSize - offsetInFile;
	if (maxReadSize && (readSize > maxReadSize)) readSize = maxReadSize;

	// Open the file,
	FILE *fileHandle = fopen(fileFullPath, "rb");
	if (!fileHandle) {
		throw std::runtime_error(TextBuffer("Couldn't open file for reading: ").append(fileFullPath).getConstText());
	}

	// Forward to the offset,
	if (offsetInFile) {
		if (fseek (fileHandle, offsetInFile, SEEK_SET)) {
			throw std::runtime_error(TextBuffer("Couldn't seek in file for reading: ").append(fileFullPath).getConstText());
		}
	}

	// Prepare the output vector to hold the file data,
	int32_t readBytesCount;
	if (offsetInVector == outputVector.size()) {

		// Adjust the original vector to hold the new data,
		int32_t originalVectorSize = outputVector.size();
		outputVector.resize(originalVectorSize + readSize);

		// Read the data into the output vector directly,
		readBytesCount = fread(&outputVector[originalVectorSize], 1, readSize, fileHandle);

		// Fit the vector to the data,
		int32_t unreadBytesCount = readSize - readBytesCount;
		if (unreadBytesCount) outputVector.resize(originalVectorSize + readBytesCount);

	} else {

		// Read into a temporary vector first,
		std::vector<uint8_t> tempVector;
		tempVector.resize(readSize);

		// Read the data into the temporary vector,
		readBytesCount = fread(&tempVector[0], 1, readSize, fileHandle);

		// Fit the vector to the data,
		int32_t unreadBytesCount = readSize - readBytesCount;
		if (unreadBytesCount) tempVector.resize(readBytesCount);

		// Insert temp vector into the output vector,
		outputVector.insert(outputVector.begin()+offsetInVector, tempVector.begin(), tempVector.end());
	}

	// Close the file,
	fclose(fileHandle);

	return readBytesCount;
}

//////////////////////////////
// System file services
//////////////////////////////

bool Ngl::systemFileExists(const Ngl::File &file) {

	// Different file locations need different treatments,
	Ngl::FileLocation fileLocation = file.getLocation();
	switch (fileLocation) {
		case Ngl::FileLocation::ASSETS: return assetFileExists(file);
		case Ngl::FileLocation::SDCARD: return sdCardFileExists(file);
		default:
			throw std::runtime_error(
					TextBuffer("File location not supported (only assets or sdcard supported): ").
					append(file.getFullPath()).getConstText());
	}
}

int32_t Ngl::systemGetFileSize(const Ngl::File &file) {

	// Different file locations need different treatments,
	Ngl::FileLocation fileLocation = file.getLocation();
	switch (fileLocation) {
		case Ngl::FileLocation::ASSETS: return getAssetFileSize(file);
		case Ngl::FileLocation::SDCARD: return getSdCardFileSize(file);
		default:
			throw std::runtime_error(
					TextBuffer("File location not supported (only assets or sdcard supported): ").
					append(file.getFullPath()).getConstText());
	}
}

void Ngl::systemMakeDir(const Ngl::File &file) {

	// Different file locations need different treatments,
	Ngl::FileLocation fileLocation = file.getLocation();
	switch (fileLocation) {

		case Ngl::FileLocation::ASSETS:
			// Writing to assets not supported,
			throw std::runtime_error(TextBuffer("Can't make directories in assets: ").append(file.getFullPath()).getConstText());

		case Ngl::FileLocation::SDCARD: {
			// Find the file final location,
			TextBuffer directoryFullPath = getApplicationExternalStorageFileAddress(file.getFullPath());
			if (mkdir(directoryFullPath, 0777)) {
				throw std::runtime_error(TextBuffer("Couldn't make directory: ").append(directoryFullPath).getConstText());
			}
			break;
		}
		default:
			throw std::runtime_error(
					TextBuffer("File location not supported (only assets or sdcard supported): ").
					append(file.getFullPath()).getConstText());
	}
}

int32_t Ngl::systemReadFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) {

	// Different file locations need different treatments,
	Ngl::FileLocation fileLocation = file.getLocation();
	switch (fileLocation) {
		case Ngl::FileLocation::ASSETS: return readAssetFile(file, outputVector, offsetInVector, offsetInFile, maxReadSize);
		case Ngl::FileLocation::SDCARD: return readSdCardFile(file, outputVector, offsetInVector, offsetInFile, maxReadSize);
		default:
			throw std::runtime_error(
					TextBuffer("File location not supported (only assets or sdcard supported): ").
					append(file.getFullPath()).getConstText());
	}
}

int32_t Ngl::systemWriteFile(const Ngl::File &file, const void *data, int32_t sizeBytes, bool append) {

	// Writing to assets not supported,
	if (file.getLocation() == Ngl::FileLocation::ASSETS) {
		throw std::runtime_error(TextBuffer("Can't write files to assets: ").append(file.getFullPath()).getConstText());
	}

	// Find the file final location,
	TextBuffer fileFullPath = getApplicationExternalStorageFileAddress(file.getFullPath());

	// Open file,
	FILE *fileHandle;
	if (append) {
		fileHandle = fopen(fileFullPath, "ab");
		if (!fileHandle) {
			throw std::runtime_error(TextBuffer("Couldn't open file for appending: ").append(fileFullPath).getConstText());
		}
	} else {
		fileHandle = fopen(fileFullPath, "wb");
		if (!fileHandle) {
			throw std::runtime_error(TextBuffer("Couldn't open file for writing: ").append(fileFullPath).getConstText());
		}
	}

	// Write data to the file,
	int32_t writtenBytesCount = fwrite(data, 1, sizeBytes, fileHandle);
	fclose(fileHandle);

	return writtenBytesCount;
}
