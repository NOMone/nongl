package com.nomone.nongl;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class GooglePlayGameServicesWrapperStub extends GooglePlayGameServicesWrapperBase {

    public GooglePlayGameServicesWrapperStub(Activity parentActivity, boolean autoStartSignInFlow) {
    	super(parentActivity, autoStartSignInFlow);
    }
    public void init(Activity parentActivity, boolean autoStartSignInFlow) {}
    public void onCreate(Bundle savedInstanceState) {}
    public void resetSignInState() {}
    public boolean isSignedIn() { return false; }
    public void onStart() {}
    public void onStop() {}
    public void showAchievements() {}
    public void showLeaderboards() {}
    public void unlockAchievement(String achievementId, boolean showPopUp) {}
    public void getAchievements(ArrayList<NonglAchievement> outAchievements, AtomicBoolean outAchievementsLoadedFlag) {}
    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) { return false; }
    public String getPlayerDisplayName() { return null; }
    
    public void signIn() {}
    public void signOut() {}
    public boolean isSignInComplete() { return false; }
}