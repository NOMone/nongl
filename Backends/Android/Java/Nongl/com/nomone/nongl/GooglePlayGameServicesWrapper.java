package com.nomone.nongl;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.plus.Plus;

public class GooglePlayGameServicesWrapper extends GooglePlayGameServicesWrapperBase implements 
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	// Parent activity,
	Activity parentActivity=null;
	
    // Client used to interact with Google APIs
    private GoogleApiClient mGoogleApiClient=null;

    // Are we currently resolving a connection failure?
    private boolean resolvingConnectionFailure = false;
    private int resolvingConnectionFailureAttemptsCount = 0;
        
    // Automatically start the sign-in flow when the Activity starts
    private boolean mAutoStartSignInFlow = false;

    // Sign in process completed, whether succeeded or failed,
    private boolean signInComplete = true;
    
    // Another variable for signing in status that can't be reset manually,
    private boolean signingIn = false;

    // request codes we use when invoking an external activity
    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;

    public GooglePlayGameServicesWrapper(Activity parentActivity, boolean autoStartSignInFlow) {
    	super(parentActivity, autoStartSignInFlow);
    }
    
    public void init(Activity parentActivity, boolean autoStartSignInFlow) {
    	this.parentActivity = parentActivity;
    	this.mAutoStartSignInFlow = autoStartSignInFlow;    	
    }
    
    public void onCreate(Bundle savedInstanceState) {

    	// MUST be called by parent activity.
    	
        // Create the Google API Client with access to Plus and Games,
    	int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(parentActivity); 
    	if (result == ConnectionResult.SUCCESS) {
	        mGoogleApiClient = new GoogleApiClient.Builder(parentActivity)
	                .addConnectionCallbacks(this)
	                .addOnConnectionFailedListener(this)
	                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
	                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
	                .build();
    	} else {
    		GooglePlayServicesUtil.getErrorDialog(result, parentActivity, RC_UNUSED);
    	}
    }
    
    public void resetSignInState() {
    	signInComplete = false;
    }
    
    public boolean isSignedIn() {
        return (mGoogleApiClient != null && mGoogleApiClient.isConnected());
    }

    public void onStart() {

    	// MUST be called by parent activity.

    	if (mGoogleApiClient != null) {

	    	// If already signing in, do nothing,
	    	if (signingIn) return;
    	
	    	NONGLNatives.nativeLogE("GPGS=> onStart(): connecting");
	        if (mAutoStartSignInFlow) {
	        	signingIn = true;
	        	signInComplete = false;
	        	mGoogleApiClient.connect();
	        }
    	} else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't connect. Api client not present.");
    	}
    }

    public void onStop() {
    	
    	// MUST be called by parent activity.

    	if (mGoogleApiClient != null) {

	    	// Stops while signing in are un-intentional and should be ignored,
	    	if (signingIn) return;

	    	NONGLNatives.nativeLogE("GPGS=> onStop(): disconnecting");
	        if (mGoogleApiClient.isConnected()) {
	            mGoogleApiClient.disconnect();
	        }
    	} else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't disconnect. Api client not present.");
    	}
    }

    public void showAchievements() {
        if (isSignedIn()) {
            parentActivity.startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), RC_UNUSED);
        } else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't show achievements, not signed in.");
            //BaseGameUtils.makeSimpleDialog(this, getString(R.string.leaderboards_not_available)).show();
        }
    }

    public void showLeaderboards() {
    	
        if (isSignedIn()) {
            parentActivity.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient), RC_UNUSED);
        } else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't show leaderboards, not signed in.");
            //BaseGameUtils.makeSimpleDialog(this, getString(R.string.leaderboards_not_available)).show();
        }
    }

    void unlockAchievement(String achievementId, boolean showPopUp) {
        
    	if (isSignedIn()) {

        	// Doesn't work on Andoird 2.3,
        	if (showPopUp) {
        		Games.setViewForPopups(mGoogleApiClient, parentActivity.getWindow().getDecorView().getRootView());
        	} else {
        		View invisibleView = new View(parentActivity); 
        		invisibleView.setVisibility(View.GONE);
        		Games.setViewForPopups(mGoogleApiClient, invisibleView);
        	}
            Games.Achievements.unlockImmediate(mGoogleApiClient, achievementId);
            
        } else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't unlock achievement, not signed in.");
        }
    }

    public static class NonglAchievement {
    	public String identifier;
    	public float percentCompleted; 
    }
    
    public void getAchievements(ArrayList<NonglAchievement> outAchievements, AtomicBoolean outAchievementsLoadedFlag) {
    	
        if (isSignedIn()) {
        	
        	final ArrayList<NonglAchievement> outAchievementsFinal = outAchievements;
        	final AtomicBoolean outAchievementsLoadedFlagFinal = outAchievementsLoadedFlag;
        	
        	PendingResult<Achievements.LoadAchievementsResult> pendingResult;
        	pendingResult = Games.Achievements.load(mGoogleApiClient, true);
        	pendingResult.setResultCallback(new ResultCallback<Achievements.LoadAchievementsResult>() {
				@Override public void onResult(LoadAchievementsResult loadAchievementsResult) {
					
					// Whenever result arrives,
					if (loadAchievementsResult.getStatus().getStatusCode() == GamesStatusCodes.STATUS_OK) {
						AchievementBuffer achievementBuffer = loadAchievementsResult.getAchievements();
                        for (Achievement achievement : achievementBuffer) {
                        	NonglAchievement newAchievement = new NonglAchievement();
                        	newAchievement.identifier = achievement.getAchievementId();
                        	if (achievement.getType() == Achievement.TYPE_INCREMENTAL) {
                            	newAchievement.percentCompleted = 100.0f * achievement.getCurrentSteps() / achievement.getTotalSteps();                        		
                        	} else {
                        		newAchievement.percentCompleted = 0;
                        		if (achievement.getState() == Achievement.STATE_UNLOCKED) newAchievement.percentCompleted = 100;                         		
                        	}
                        	outAchievementsFinal.add(newAchievement);
                        }
                        achievementBuffer.close();
                        achievementBuffer.release();
					}
					
					outAchievementsLoadedFlagFinal.set(true);
				}
			});
        	 
        } else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't get achievements, not signed in.");
        }
    }
    
    // Returns true if consumed,
    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {

    	// MUST be called by parent activity.

    	if (requestCode == RC_SIGN_IN) {
            resolvingConnectionFailure = false;
            if (resultCode == Activity.RESULT_OK) {
            	if (mGoogleApiClient != null) mGoogleApiClient.connect();
            } 
            return true;
        }
    	
    	// Not consumed,
    	return false;
    }

    String getPlayerDisplayName() {
    	
        if (isSignedIn()) {
            Player p = Games.Players.getCurrentPlayer(mGoogleApiClient);
            if (p == null) {
            	NONGLNatives.nativeLogE("GPGS=> mGamesClient.getCurrentPlayer() is NULL!");
                return "???";
            } else {
                return p.getDisplayName();
            }
        } else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't get player display name, not signed in.");
        	return null;
        }
    }
    
    @Override
    public void onConnected(Bundle bundle) {

    	// Library users should not call this.

    	resolvingConnectionFailureAttemptsCount = 0;
    	resolvingConnectionFailure = false;
    	NONGLNatives.nativeLogE("GPGS=> onConnected(): connected to Google APIs");
    	
    	// TODO: do something here .. ?
    	signingIn = false;
        signInComplete = true;
    }

    @Override
    public void onConnectionSuspended(int i) {

    	// Library users should not call this.

    	NONGLNatives.nativeLogE("GPGS=> onConnectionSuspended(): attempting to connect");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    	
    	// Library users should not call this.

		if (resolvingConnectionFailure) {
			NONGLNatives.nativeLogE("GPGS=> onConnectionFailed(): already resolving");
			return;
		}

		if (resolvingConnectionFailureAttemptsCount < 1) {
			NONGLNatives.nativeLogE("GPGS=> onConnection failed(): attempting to resolve");
			resolvingConnectionFailureAttemptsCount++;
			resolvingConnectionFailure = true;
			if (BaseGameUtils.resolveConnectionFailure(parentActivity, mGoogleApiClient, connectionResult, RC_SIGN_IN, "Couldn't sign in to Play Games.")) {
				return ;
			}
		}
			
        // Sign-in failed,
		resolvingConnectionFailureAttemptsCount = 0;
		resolvingConnectionFailure = false;
    	NONGLNatives.nativeLogE("GPGS=> Connection failed.");
    	
        // TODO: do something (show sign-in button on main menu)?
    	signingIn = false;
        signInComplete = true;
    }

    public void signIn() {
    	
    	if (mGoogleApiClient != null) {
    		
	    	if (isSignedIn()) {
	    		signInComplete = true;
	    		return ;
	    	}
	    
	        // start the sign-in flow
	    	signingIn = true;
	        signInComplete = false;
	        mGoogleApiClient.connect();
    	} else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't sign in. Api client not present.");
    	}
    }

    public void signOut() {

    	if (mGoogleApiClient != null) {

	    	if (!isSignedIn()) return ;
	
	        Games.signOut(mGoogleApiClient);
	        if (mGoogleApiClient.isConnected()) {
	        	signingIn = false;
	            signInComplete = true;
	            mGoogleApiClient.disconnect();
	        }
    	} else {
        	NONGLNatives.nativeLogE("GPGS=> Couldn't sign out. Api client not present.");
    	}
    }
    
    public boolean isSignInComplete() {
    	return signInComplete;
    }
}
