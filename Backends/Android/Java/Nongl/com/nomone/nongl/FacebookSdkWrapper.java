package com.nomone.nongl;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.facebook.CallbackManager;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Collection;

/**
 * Created by raslanove on 8/21/16.
 */
public class FacebookSdkWrapper extends FacebookSdkWrapperBase {

    CallbackManager callbackManager;

    private FacebookLoginResponse loginResponse = new FacebookLoginResponse();

    @Override
    public void sdkInitialize(Application app, Context context) {

        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(context);
        AppEventsLogger.activateApp(app);

        // Create and register a call-back manager,
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override public void onSuccess(LoginResult loginResult) {
                loginResponse.type = FacebookLoginResponse.Type.SUCCESS;
            }

            @Override public void onCancel() {
                loginResponse.type = FacebookLoginResponse.Type.CANCEL;
            }

            @Override public void onError(FacebookException error) {
                loginResponse.type = FacebookLoginResponse.Type.ERROR;
                loginResponse.errorMessage = error.getMessage();
            }
        });
    }

    @Override
    public void CallbackManager_onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void LoginManager_logInWithReadPermissions(Activity activity, Collection<String> permissions) {
        loginResponse.type = FacebookLoginResponse.Type.NOT_RECEIVED;
        LoginManager.getInstance().logInWithReadPermissions(activity, permissions);
    }

    @Override public FacebookLoginResponse getLoginResponse() {
        return loginResponse;
    }

    @Override
    public void LoginManager_logOut() {
        LoginManager.getInstance().logOut();
    }

    @Override
    public String getCurrentAccessToken() {
        return AccessToken.getCurrentAccessToken().getToken();
    }
}
