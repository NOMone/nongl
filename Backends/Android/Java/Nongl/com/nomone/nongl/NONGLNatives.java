package com.nomone.nongl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.nomone.nongl.GooglePlayGameServicesWrapperBase.NonglAchievement;
import com.nomone.nongl.SocketsClient.ByteArrayBuffer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

public class NONGLNatives {

	static {
		// Used to need this when I used ndk-build, since I used
		// a shared stl instead of a static one,
		//System.loadLibrary("c++_shared");
		System.loadLibrary("natives");
	}

	public static Object nativeSyncLock = new Object();
				
	///////////////////////////////////////////////////
	// File system
	///////////////////////////////////////////////////
	
	public static AssetManager javaGetAssetsManager() {
		return Common.assetManager;
	}
	
	public static String javaCreateAndGetApplicationExternalStorageAddress() {
		
		String extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
		String address = extStorageDirectory + "/Android/data/" + Common.context.getPackageName();

		// Try to make directory if it doesn't already exist,	
		if ((new File(address)).mkdirs()) return address;

		// Failed to create directory. Probably no external storage installed. Switch
		// to internal storage,
		address = Common.context.getFilesDir().getAbsolutePath();
			
		// Try to make directory if it doesn't already exist,	
		if ((new File(address)).mkdirs()) return address;

		// Failed to create directory. Log the error,
		nativeLogE("Couldn't create directory: " + address);
		
		return address;
	}

	//////////////////////////////////////
	// Preferences
	//////////////////////////////////////

	static private SharedPreferences sharedPreferences;
	public static void javaSetBooleanPreference(String key, boolean value) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);
		
		Editor preferencesEditor = sharedPreferences.edit(); 
		preferencesEditor.putBoolean(key, value);
		preferencesEditor.commit();
	}
	
	public static boolean javaGetBooleanPreference(String key, boolean defaultValue) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		return sharedPreferences.getBoolean(key, defaultValue);
	}

	public static void javaSetIntPreference(String key, int value) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);
		
		Editor preferencesEditor = sharedPreferences.edit(); 
		preferencesEditor.putInt(key, value);
		preferencesEditor.commit();
	}
	
	public static int javaGetIntPreference(String key, int defaultValue) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		return sharedPreferences.getInt(key, defaultValue);
	}

	public static void javaSetStringPreference(String key, String value) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		Editor preferencesEditor = sharedPreferences.edit();
		preferencesEditor.putString(key, value);
		preferencesEditor.commit();
	}

	public static String javaGetStringPreference(String key, String defaultValue) {
		if (sharedPreferences == null)
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Common.context);

		return sharedPreferences.getString(key, defaultValue);
	}

	//////////////////////////////////////
	// Music
	//////////////////////////////////////
	
	private static ArrayList<Music> loadedMusics = new ArrayList<Music>();
	public static int javaLoadMusicFromAssets(String filename) {

		Music music;
		try {
			music = new Music(Common.context, filename + ".ogg");
			loadedMusics.add(music);
			return loadedMusics.size() - 1;
		} catch (Exception e) {}
		
		return -1;
	}
	
	public static void javaDeleteMusic(int musicIndex) {
		loadedMusics.get(musicIndex).stop();
		loadedMusics.set(musicIndex, null);
	}
	
	public static void javaPlayMusic(int musicIndex) {
		loadedMusics.get(musicIndex).play();
	}
	
	public static void javaMusicSetLooping(int musicIndex, boolean looping) {
		loadedMusics.get(musicIndex).setLooping(looping);
	}

	public static void javaStopMusic(int musicIndex) {
		loadedMusics.get(musicIndex).stop();
	}

	public static void javaPauseMusic(int musicIndex) {
		loadedMusics.get(musicIndex).pause();
	}

	public static void javaResumeMusic(int musicIndex) {
		loadedMusics.get(musicIndex).resume();
	}

	public static void javaFadeOutMusic(int musicIndex, int milliSeconds) {
		loadedMusics.get(musicIndex).fadeOut(milliSeconds);
	}
	
	//////////////////////////////////////
	// Sound
	//////////////////////////////////////

	private static ArrayList<Sound> loadedSounds = new ArrayList<Sound>();
	public static int javaLoadSoundFromAssets(String filename) {

		try {
			Sound sound = new Sound(Common.context, filename + ".ogg");
			loadedSounds.add(sound);
			return loadedSounds.size() - 1;
		} catch (Exception e) {}
		
		return -1;
	}

	public static void javaDeleteSound(int soundIndex) {		
		Sound sound = loadedSounds.get(soundIndex);
		sound.stop();
		sound.release();
		loadedSounds.set(soundIndex, null);
	}

	public static void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {
		loadedSounds.get(soundIndex).play(rate, leftVolume, rightVolume);
	}

	public static void javaStopSound(int soundIndex) {
		loadedSounds.get(soundIndex).stop();
	}

	public static void javaPauseSound(int soundIndex) {
		loadedSounds.get(soundIndex).pause();
	}

	public static void javaResumeSound(int soundIndex) {
		loadedSounds.get(soundIndex).resume();
	}

	public static void javaSoundSetLooping(int soundIndex, boolean looping) {
		loadedSounds.get(soundIndex).setLooping(looping);
	}

	public static void javaMuteSounds(boolean mute) {
		Sound.mute(mute);
	}
	
	//////////////////////////////////////
	// Sockets
	//////////////////////////////////////
	 
	private static ArrayList<SocketsClient> socketsClients = new ArrayList<SocketsClient>();
	public static int javaCreateSocket(String address, int port) {

		try {
			SocketsClient socketsClient = new SocketsClient(address, port);

			int i;
			for (i=0; i<socketsClients.size(); i++) {
				if (socketsClients.get(i) == null)
					break;
			}
			
			if (i<socketsClients.size()) {
				socketsClients.set(i, socketsClient);
			} else {
				socketsClients.add(socketsClient);
			}
			
			return i; 
		} catch (Exception e) {}
		
		return -1;
	}

	public static void javaCloseSocket(int socketIndex) {
		socketsClients.get(socketIndex).close();
		socketsClients.set(socketIndex, null);
	}
	
	public static void javaSocketWrite(int socketIndex, byte[] bytes) {
		socketsClients.get(socketIndex).write(bytes);
	}
	
	private static ByteArrayBuffer readBuffer = new ByteArrayBuffer();
	/** @return The read data size in bytes in the first 4 bytes, followed
	 * by the actual bytes read. */
	public static byte[] javaSocketRead(int socketIndex) {
		
		readBuffer.reset();
		int readBytesCount = 0;
		try { readBytesCount = socketsClients.get(socketIndex).recieve(readBuffer); } catch (Exception e) {}
		
		if (readBytesCount == 0)
			return null;
		
		return readBuffer.data;
	}
		
	//////////////////////////////////////
	// Misc
	//////////////////////////////////////
	
	public static boolean javaUsesCoverageAa() {
		
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null)
			return activity.usesCoverageAa();
		
		return false;
	}

	public static void javaLongToast(String toastText) {
		final String finalToastText = toastText;
		Handler mainHandler = new Handler(Common.context.getMainLooper());
		Runnable myRunnable = new Runnable() {				
			@Override public void run() {
				Toast.makeText(Common.context, finalToastText, Toast.LENGTH_LONG).show();
			}
		};
		mainHandler.post(myRunnable);
	}
	
	public static void javaShortToast(String toastText) {
		final String finalToastText = toastText;
		Handler mainHandler = new Handler(Common.context.getMainLooper());
		Runnable myRunnable = new Runnable() {				
			@Override public void run() {
				Toast.makeText(Common.context, finalToastText, Toast.LENGTH_SHORT).show();
			}
		};
		mainHandler.post(myRunnable);
	}
		
	public static void javaBrowseUrl(String url, String alternativeUrl) {
		if (Common.currentActivity != null)
			Utils.browseUrl(Common.currentActivity, url, alternativeUrl);
	}

	public static float javaGetDpi() {
		return Common.context.getResources().getDisplayMetrics().xdpi;
	}

	static long startTime = System.currentTimeMillis();
	public static float javaGetTimeMillis() {
		//return System.nanoTime() / 1000000.0f;
		return System.currentTimeMillis() - startTime;
	}

	public static void javaExit() {
		System.exit(0);
	}
	
	public static void javaFinishActivity() {
		if (Common.currentActivity != null) {
			Common.currentActivity.finish();
		}
	}

	public static void javaPreventSleep(boolean prevent) {
		if (Common.currentActivity != null) {
			Common.currentActivity.preventSleep(prevent);
		}
	}
	
	public static Runnable openOptionsMenuRunnable = new Runnable() {
		@Override public void run() { Common.currentActivity.openOptionsMenu(); }
	};
	
	public static void javaOpenOptionsMenu() {
		if (Common.currentActivity != null) {
			Common.currentActivity.runOnUiThread(openOptionsMenuRunnable);
		}
	}

	public static void javaShowVirtualKeyboard() {
        InputMethodManager imm = (InputMethodManager) Common.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public static void javaHideVirtualKeyboard() {
        InputMethodManager imm = (InputMethodManager) Common.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}

	// Get the method id,
	public static String javaUrlEncode(String text) {
		String charset = "UTF-8";  // java.nio.charset.StandardCharsets.UTF_8.name();
		try {
			return URLEncoder.encode(text, charset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] javaHttpRequest(String url, String data, boolean isPost) {

		// If GET, append the data to the url,
		if ((!isPost) && (!"".equals(data))) url = url + '?' + data;

		try {
			URL urlObject = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();

			if (isPost) {
				conn.setDoOutput(true); // Triggers POST.
				OutputStream outputStream = conn.getOutputStream();
				outputStream.write(data.getBytes());
				outputStream.close();
			}

			InputStream inputStream;
			if (conn.getResponseCode() >= 400) { //HttpStatus.SC_BAD_REQUEST) {
				inputStream = conn.getErrorStream();
			} else {
				inputStream = conn.getInputStream();
			}

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int readBytesCount;
			byte[] response = new byte[16384];

			while ((readBytesCount = inputStream.read(response, 0, response.length)) != -1) {
				buffer.write(response, 0, readBytesCount);
			}
			inputStream.close();
			buffer.flush();

			return buffer.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static byte[] javaDownloadFile(String url) {
		
		try {
			
			// If you are using https, make sure to import java.net.HttpsURLConnection,
			URL urlObject = new URL(url);

			HttpURLConnection conn = (HttpURLConnection) urlObject.openConnection();
			conn.setRequestMethod("GET");

			InputStream inputStream = conn.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			int readBytesCount;
			byte[] data = new byte[16384];

			while ((readBytesCount = inputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, readBytesCount);
			}
			inputStream.close();
			buffer.flush();

			return buffer.toByteArray();
		} catch (Exception e) {}
		
		return null;
	}

	public static String javaGetNetworkCountryCode() {
		return Utils.getNetworkCountryCode(Common.context);
	}
	
	//////////////////////////////////////
	// Game services
	//////////////////////////////////////
	
	public static void javaGameServicesResetSignInState() {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) activity.gameServicesResetSignInState();		
	}
	
	public static void javaGameServicesShowSignInDialog() {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) activity.gameServicesShowSignInDialog(); 
	}

	public static boolean javaGameServicesIsSignInComplete() {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) return activity.gameServicesIsSignInComplete();
		return false;
	}
	
	public static boolean javaGameServicesIsUserSignedIn() {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) return activity.gameServicesIsUserSignedIn();
		return false;
	}
	
	public static void javaGameServicesShowServicesDialog() {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) activity.gameServicesShowServicesDialog(); 
	}
	
	public static void javaGameServicesSubmitAchievement(String achievementId, boolean showBanner) {
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) activity.gameServicesSubmitAchievement(achievementId, showBanner); 		
	}

	static ArrayList<NonglAchievement> loadedAchievements;
	static AtomicBoolean achievementsLoadedFlag;
    public static void javaGameServicesLoadAchievements() {
    	
		NonglActivity activity = (NonglActivity) Common.currentActivity;
		if (activity != null) {
			loadedAchievements = new ArrayList<NonglAchievement>();
			achievementsLoadedFlag = new AtomicBoolean(false);
			activity.gameServicesGetAchievements(loadedAchievements, achievementsLoadedFlag); 		
		}
    }
    
    public static boolean javaGameServicesAchievementsDidLoad() {
    	if (achievementsLoadedFlag != null) return achievementsLoadedFlag.get();
    	return false;
    }
    
    public static int javaGameServicesGetLoadedAchievementsCount() {
    	if (loadedAchievements != null) return loadedAchievements.size();
    	return 0;
    }
    
    public static String javaGameServicesGetLoadedAchievementId(int achievementIndex) {
    	if (loadedAchievements != null) return loadedAchievements.get(achievementIndex).identifier;
    	return null;
    }
    
    public static float javaGameServicesGetLoadedAchievementPercentCompleted(int achievementIndex) {
    	if (loadedAchievements != null) return loadedAchievements.get(achievementIndex).percentCompleted;
    	return 0;
    }

	//////////////////////////////////////
	// Facebook sdk
	//////////////////////////////////////

	public static void javaFacebookSdkLogInWithReadPermissions(String permissions) {
		 Common.facebookSdkWrapper.LoginManager_logInWithReadPermissions(
				 Common.currentActivity,
				 Arrays.<String>asList(permissions.split(",")));
	}

	public static int javaFacebookSdkGetLoginResponseType() {
		switch (Common.facebookSdkWrapper.getLoginResponse().type) {
			case UNKNOWN: return 0;
			case NOT_RECEIVED: return 1;
			case FACEBOOK_NOT_INTEGRATED: return 2;
			case SUCCESS: return 3;
			case CANCEL: return 4;
			case ERROR: return 5;
		}
		return 0; // Unknown.
	}

	public static String javaFacebookSdkGetLoginErrorMessage() {
		return Common.facebookSdkWrapper.getLoginResponse().errorMessage;
	}

	public static void javaFacebookSdkLogOut() {

		Common.facebookSdkWrapper.LoginManager_logOut();
	}

	public static String javaFacebookSdkGetCurrentAccessToken() {
		return Common.facebookSdkWrapper.getCurrentAccessToken();
	}

	//////////////////////////////////////
	// Native functions
	//////////////////////////////////////

	public static native void nativeLogI(String message);
	public static native void nativeLogW(String message);
	public static native void nativeLogE(String message);
	public static native void nativeReleaseOpenGlObjects();
	public static native void nativeOnPause();
	public static native void nativeOnResume();
	public static native void nativeOnSurfaceCreated();
	public static native void nativeOnSurfaceChanged(int width, int height);
	public static native void nativeOnDrawFrame();
	public static native void nativeOnTouchDown(int pointerId, float x, float y);
	public static native void nativeOnTouchDrag(int pointerId, float x, float y);
	public static native void nativeOnTouchUp(int pointerId, float x, float y);
	public static native void nativeOnTouchCancel(int pointerId, float x, float y);					
	public static native void nativeOnScroll(float x, float y, float xOffset, float yOffset);
	public static native void nativeOnBackPressed();
	public static native void nativeOnMenuPressed();
	public static native void nativeOnKeyDown(int keyCode);
	public static native void nativeOnKeyUp(int keyCode);
	public static native void nativeOnMenuItemSelected(int itemId);
}
