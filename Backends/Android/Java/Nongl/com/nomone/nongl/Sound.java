package com.nomone.nongl;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;

public class Sound extends Audio {

	private static final int MAX_STREAMS = 8;
	
	private static SoundPool soundPool;
	private static AudioManager audioManager;
	
	private int soundId, streamId;
	
	private boolean mutable = true;

	private static boolean muted = false;
	
	public Sound(Context context, String filename) throws Exception {
		
		if (soundPool == null) {			
			soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, 0);
			audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);			
		}
		
		AssetFileDescriptor fileDescriptor;
		fileDescriptor = context.getAssets().openFd(filename);
		soundId = soundPool.load(fileDescriptor, 1);
	}
	
	public static void mute(boolean mute) {
		muted = mute;
	}

	public void release() {
		soundPool.unload(soundId);		
	}

	public void play() {
		
		if (muted && mutable)
			return;
		
		float streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		streamVolume = streamVolume / audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		
		streamId = soundPool.play(soundId, streamVolume, streamVolume, 1, 0, 1f);
	}
	
	public void play(float rate, float leftVolume, float rightVolume) {

		if (muted && mutable)
			return;
		
		float streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		streamVolume = streamVolume / audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		streamId = soundPool.play(
				soundId, 
				streamVolume*leftVolume, 
				streamVolume*rightVolume, 1, 0, rate);
	}

	public void stop() {
		soundPool.stop(streamId);
	}

	public void pause() {
		soundPool.pause(streamId);
	}
	
	public void resume() {
		soundPool.resume(streamId);
	}
	
	public void setLooping(boolean loopingEnabled) {
		
		mutable = !loopingEnabled;
		
		float streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		streamVolume = streamVolume / audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		streamId = soundPool.play(soundId, streamVolume, streamVolume, 1, -1, 1f);
	}
	
	public boolean isPlaying() {
		return false;
	}
}
