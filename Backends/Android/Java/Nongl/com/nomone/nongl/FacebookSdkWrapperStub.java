package com.nomone.nongl;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import java.util.Collection;

/**
 * Created by raslanove on 8/21/16.
 */
public class FacebookSdkWrapperStub extends FacebookSdkWrapperBase {

    FacebookLoginResponse loginResponse = new FacebookLoginResponse();
    FacebookSdkWrapperStub() {
        loginResponse.type = FacebookLoginResponse.Type.FACEBOOK_NOT_INTEGRATED;
        loginResponse.errorMessage = "Facebook SDK not integrated";
    }

    @Override public void sdkInitialize(Application app, Context context) {}
    @Override public void CallbackManager_onActivityResult(int requestCode, int resultCode, Intent data) {}

    @Override public void LoginManager_logInWithReadPermissions(Activity activity, Collection<String> permissions) {}
    @Override public FacebookLoginResponse getLoginResponse() {
        return loginResponse;
    }
    @Override public void LoginManager_logOut() {}

    @Override public String getCurrentAccessToken() { return null; }
}
