package com.nomone.nongl;

import android.app.Application;

/**
 * Created by raslanove on 8/21/16.
 */
public class NonglApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Select the appropriate facebook sdk wrapper,
        try {
            Common.facebookSdkWrapper = (FacebookSdkWrapperBase) Class.forName("com.nomone.nongl.FacebookSdkWrapper").newInstance();
        } catch (Exception e) {
            Common.facebookSdkWrapper = new FacebookSdkWrapperStub();
        }

        // Initialize the SDK before executing any other operations,
        Common.facebookSdkWrapper.sdkInitialize(this, getApplicationContext());
    }
}
