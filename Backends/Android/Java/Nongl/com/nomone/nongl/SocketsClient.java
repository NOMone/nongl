/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nomone.nongl;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 *
 * @author raslanove
 */
public class SocketsClient {

    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
 
    private ByteArrayBuffer dataShelf = new ByteArrayBuffer();
    private ByteArrayBuffer sendBuffer = new ByteArrayBuffer();
    
    private WritingThread writingThread;

    private Object lockObject = new Object();
    private boolean dataToSendAvailable = false;
    
    public SocketsClient(String ip, int port) throws Exception {
        
		socket = new Socket(ip, port);
		dataOutputStream = new DataOutputStream(socket.getOutputStream());
		dataInputStream = new DataInputStream(socket.getInputStream());
        
        writingThread = new WritingThread();
        writingThread.start();            
    }    

    /** 
     * Adds to the byffer: The bytes read count (4 bytes) followed by the bytes read.
     * @return number of bytes read. */
    public int recieve(ByteArrayBuffer buffer) throws IOException {
    	
    	// Save the old writing position,
    	int oldBytesCount = buffer.bytesCount;
    	
    	// Reserve space for size variable,
    	buffer.put((int) 0);
    	
    	// Read the data,
    	int readBytesCount = buffer.put(dataInputStream);

    	if (readBytesCount == 0) {
    		buffer.bytesCount = oldBytesCount;
    		return 0;
    	}
    	
    	// Save the new writing position,
    	int newBytesCount = buffer.bytesCount;
    	
    	// Restore the old writing position to modify the size variable,
    	buffer.bytesCount = oldBytesCount;
    	
    	// Modify the size variable,
    	buffer.put(readBytesCount);
    	
    	// Restore the new writing position,
    	buffer.bytesCount = newBytesCount;
    	
    	return readBytesCount;
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (dataInputStream != null) {
            try {
                dataInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (dataOutputStream != null) {
            try {
                dataOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
        
    public void write(int value) {
        
        synchronized(lockObject) {
            dataShelf.put(value);
            dataToSendAvailable = true;
            lockObject.notify();
        }
    }

    public void write(byte[] bytes) {

        synchronized(lockObject) {
            dataShelf.put(bytes);
            dataToSendAvailable = true;
            lockObject.notify();
        }
    }

    public void write(String value) {

        synchronized(lockObject) {
            dataShelf.put(value.getBytes());
            dataShelf.put((byte) 0);
            dataToSendAvailable = true;
            lockObject.notify();
        }
    }

    private class WritingThread extends Thread {
        
        boolean shouldRun = true;
        
        @Override
        public void run() {
            
            while (shouldRun) {
                
                synchronized(lockObject) {
          
                    while (dataToSendAvailable == false) {
                        try { lockObject.wait(); } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    
                    // Find all data that's needed to be sent, copy them into
                    // send buffers,                        
                    sendBuffer.reset();
                    sendBuffer.put(dataShelf.data, 0, dataShelf.bytesCount);
                    dataShelf.reset();                        
                    
                    dataToSendAvailable = false;                    
                }
                
                // Send data,
                try {                        
                    dataOutputStream.write(
                            sendBuffer.data, 
                            0, sendBuffer.bytesCount);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class ByteArrayBuffer {

        byte[] data = new byte[4];
        int bytesCount = 0;
        int readPointer = 0;

        private void doubleCapacity() {
        	byte[] newArray = new byte[data.length << 1];
            System.arraycopy(data, 0, newArray, 0, data.length);
            data = newArray;
        }
        
        public void put(byte value) {

            if (bytesCount == data.length) {
                doubleCapacity();
            }

            data[bytesCount++] = value;
        }

        public void put(int value) {

            if (bytesCount + 3 >= data.length) {
                byte[] newArray = new byte[data.length << 1];
                System.arraycopy(data, 0, newArray, 0, data.length);
                data = newArray;
            }

            data[bytesCount++] = (byte)  value        ;
            data[bytesCount++] = (byte) (value >>> 8 );
            data[bytesCount++] = (byte) (value >>> 16);
            data[bytesCount++] = (byte) (value >>> 24);
        }
        
        public void put(float value) {

            int bits = Float.floatToIntBits(value);
            if (bytesCount + 3 >= data.length) {
                byte[] newArray = new byte[data.length << 1];
                System.arraycopy(data, 0, newArray, 0, data.length);
                data = newArray;
            }

            data[bytesCount++] = (byte)  bits       ;
            data[bytesCount++] = (byte) (bits >> 8 );
            data[bytesCount++] = (byte) (bits >> 16);
            data[bytesCount++] = (byte) (bits >> 24);
        }
    
        public void put(byte[] bytes) {
            put(bytes, 0, bytes.length);
        }
        
        public void put(ByteArrayBuffer buffer) {
            put(buffer.data, 0, buffer.bytesCount);
        }
                
        public void put(byte[] bytes, int offset, int length) {
            
            int shiftsCount=0;
            while (bytesCount + length >= (data.length<<shiftsCount)) {
                shiftsCount++;
            }
            
            if (shiftsCount != 0) {
                byte[] newArray = new byte[data.length << shiftsCount];
                System.arraycopy(data, 0, newArray, 0, data.length);
                data = newArray;
            }
            
            System.arraycopy(bytes, offset, data, bytesCount, length);
            bytesCount += length;
        }
        
        /** @return The number of bytes read from the stream. */
        public int put(InputStream inputStream) throws IOException {
        	
        	int streamBytesCount = inputStream.available();
        	if (streamBytesCount == 0) {
        		return 0;
        	}
        	
            int shiftsCount=0;
            while (bytesCount + streamBytesCount >= (data.length<<shiftsCount)) {
                shiftsCount++;
            }
            
            if (shiftsCount != 0) {
                byte[] newArray = new byte[data.length << shiftsCount];
                System.arraycopy(data, 0, newArray, 0, data.length);
                data = newArray;
            }
            
            int actualReadBytesCount = inputStream.read(data, bytesCount, streamBytesCount);
            bytesCount += actualReadBytesCount;
            
            return actualReadBytesCount;
        }
        
        /** @return The actual number of bytes read from the stream (maybe less than requested). */
        public int put(InputStream inputStream, int requestedBytesCount) throws IOException {
        	
        	int streamBytesCount = inputStream.available();
        	if (streamBytesCount == 0) {
        		return 0;
        	}
        	
        	if (requestedBytesCount > streamBytesCount)
        		requestedBytesCount = streamBytesCount;
        	
            int shiftsCount=0;
            while (bytesCount + requestedBytesCount >= (data.length<<shiftsCount)) {
                shiftsCount++;
            }
            
            if (shiftsCount != 0) {
                byte[] newArray = new byte[data.length << shiftsCount];
                System.arraycopy(data, 0, newArray, 0, data.length);
                data = newArray;
            }
            
            int actualReadBytesCount = inputStream.read(data, bytesCount, requestedBytesCount);
            bytesCount += actualReadBytesCount;
            
            return actualReadBytesCount;
        }

        public void reset() {
            bytesCount = 0;
            readPointer = 0;
        }

        public void resetReadPointer() {
            readPointer = 0;
        }
        
        public byte getByte() {
            return data[readPointer++];
        }
        
        public int getInt() {
            int value = data[readPointer++] & 0x000000ff;
            value |= (data[readPointer++] & 0x000000ff) << 8;
            value |= (data[readPointer++] & 0x000000ff) << 16;
            value |= (data[readPointer++] & 0x000000ff) << 24;           
            return value;
        }

        public float getFloat() {

            int value = data[readPointer++] & 0x000000ff;
            value |= (data[readPointer++] & 0x000000ff) << 8;
            value |= (data[readPointer++] & 0x000000ff) << 16;
            value |= (data[readPointer++] & 0x000000ff) << 24;           

            return Float.intBitsToFloat(value);
        }
    }
}
