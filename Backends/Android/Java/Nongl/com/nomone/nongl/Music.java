package com.nomone.nongl;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

public class Music extends Audio {

	static final ExpandingMediaPlayersArray mediaPlayers = new ExpandingMediaPlayersArray();

	static final int SOUND_END_TOLERANCE_MILLIS = 50;
	AssetFileDescriptor fileDescriptor;
	
	public Music(Context context, String filename) throws Exception {
		fileDescriptor = context.getAssets().openFd(filename);
	}
	
	public void play() {
		
		FileDescriptor musicFileDescriptor = fileDescriptor.getFileDescriptor(); 
		//FileDescriptor mediaPlayerFileDescriptor;
		
		MediaPlayer mediaPlayer;
		int mediaPlayerIndex = mediaPlayers.getMediaPlayer(this);
		
		if (mediaPlayerIndex == -1) {
			mediaPlayerIndex = mediaPlayers.requestIdleMediaPlayer(this); 
		}
		
		mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayerIndex);
		//mediaPlayerFileDescriptor = mediaPlayers.getFileDescriptor(mediaPlayerIndex);
		
		try {
			//if (musicFileDescriptor != mediaPlayerFileDescriptor) {
				mediaPlayer.reset();
				mediaPlayer.setDataSource(
						musicFileDescriptor,
						fileDescriptor.getStartOffset(),
						fileDescriptor.getDeclaredLength());
				mediaPlayer.prepare();
				mediaPlayers.setFileDescriptor(mediaPlayerIndex, musicFileDescriptor);
			//} else {
				//mediaPlayer.prepare();
				//mediaPlayer.seekTo(0);
			//}
			mediaPlayer.start();
		} catch (IllegalArgumentException e) {
			NONGLNatives.nativeLogW("Unable to play music queue do to exception: " + e.getMessage());
		} catch (IllegalStateException e) {
			NONGLNatives.nativeLogW("Unable to play music queue do to exception: " + e.getMessage());
		} catch (IOException e) {
			NONGLNatives.nativeLogW("Unable to play music queue do to exception: " + e.getMessage());
		}
	}

	public void setLooping(boolean loopingEnabled) {
		
		MediaPlayer mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayers.getMediaPlayer(this));
		if (mediaPlayer != null) {
			mediaPlayer.setLooping(loopingEnabled);
		}		
	}

	public void stop() {
		MediaPlayer mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayers.getMediaPlayer(this));
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
		}
	}

	public void pause() {
		MediaPlayer mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayers.getMediaPlayer(this));
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
			}
		}
	}
	
	public void resume() {

		MediaPlayer mediaPlayer;
		int mediaPlayerIndex = mediaPlayers.getMediaPlayer(this);
		
		if (mediaPlayerIndex == -1) {
			return; 
		}
			
		mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayerIndex);
		try {
			mediaPlayer.start();
		} catch (Exception e) {}		
	}
	
	public void fadeOut(int milliSeconds) {

		final MediaPlayer mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayers.getMediaPlayer(this));
		if (mediaPlayer != null) {
			
			final Timer timer = new Timer(true);
			
	        TimerTask timerTask = new TimerTask() {
	        	float fadeDurationMillis;
	        	long startTime;
	            
	        	TimerTask init(int milliSeconds) {
	        		fadeDurationMillis = milliSeconds;
	        		startTime = System.nanoTime();
	        		return this;
	        	}
	        	
	        	@Override public void run() {
	        		// Fade out,
	        		float linearRatio = ((System.nanoTime() - startTime) / 1000000.0f) / fadeDurationMillis;	        		
	        		if (linearRatio >= 1) {
	                    timer.cancel();
	                    timer.purge();
	                    linearRatio = 1;
	        		}

	        		float log = 1.0f - (float) (Math.log(linearRatio * 10) / Math.log(10));
	        		if (log > 1) log = 1;
	        		mediaPlayer.setVolume(log, log);
	            }
	        }.init(milliSeconds);

	        // Start timer,
	        timer.schedule(timerTask, 0, 16);
		}
	}
	
	public boolean isPlaying() {

		MediaPlayer mediaPlayer = mediaPlayers.getMediaPlayer(mediaPlayers.getMediaPlayer(this));
		if (mediaPlayer != null) {
			return mediaPlayer.isPlaying();
		}		
		return false;
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		fileDescriptor.close(); 
		super.finalize();
	}
	
	static class ExpandingMediaPlayersArray {
		
		public MediaPlayer[] mediaPlayers = new MediaPlayer[1];
		public Music[] mediaPlayerUsers = new Music[1]; 
		public FileDescriptor[] fileDescriptors = new FileDescriptor[1];
		public int count = 0;
		
		int lastIdlePlayerIndex = 0;
		
		public void add(MediaPlayer mediaPlayer, Music mediaPlayerUser, FileDescriptor fileDescriptor) {
			
			// Expand the array if needed,
			if (count == mediaPlayers.length) {
				MediaPlayer[] newMediaPlayerArray = new MediaPlayer[mediaPlayers.length << 1];
				Music[] newMediaPlayerUsersArray = new Music[mediaPlayers.length << 1];
				FileDescriptor[] newFileDescriptorsArray = new FileDescriptor[mediaPlayers.length << 1];
				for (int i=0; i<mediaPlayers.length; i++) {
					newMediaPlayerArray[i] = mediaPlayers[i];
					newMediaPlayerUsersArray[i] = mediaPlayerUsers[i];
					newFileDescriptorsArray[i] = fileDescriptors[i];
				}
				mediaPlayers = newMediaPlayerArray;
				mediaPlayerUsers = newMediaPlayerUsersArray;
				fileDescriptors = newFileDescriptorsArray; 
			}
			
			// Now add the media player,
			mediaPlayers[count] = mediaPlayer;
			mediaPlayerUsers[count] = mediaPlayerUser;
			fileDescriptors[count] = fileDescriptor;
			count++;
		}
		
		public int requestIdleMediaPlayer(Music mediaPlayerUser) {
			
			for (int i=0; i<count; i++) {
				
				lastIdlePlayerIndex = (lastIdlePlayerIndex + 1) % count;
				
				if (mediaPlayers[lastIdlePlayerIndex].isPlaying() == false) {
					int currentPosition = mediaPlayers[lastIdlePlayerIndex].getCurrentPosition();
					if ((currentPosition == 0) ||
						(mediaPlayers[lastIdlePlayerIndex].getDuration() - currentPosition <= SOUND_END_TOLERANCE_MILLIS)) {
						
						mediaPlayerUsers[i] = mediaPlayerUser;
						return i;						
					}
				}
			}
			
			MediaPlayer mediaPlayer = new MediaPlayer();  
			add(mediaPlayer, mediaPlayerUser, null);
			
			return count - 1;
		}
		
		public MediaPlayer getMediaPlayer(int index) {
			if (index < 0)
				return null;
			return mediaPlayers[index];
		}
		
		public FileDescriptor getFileDescriptor(int index) {
			return fileDescriptors[index];
		}
		
		public void setFileDescriptor(int mediaPlayerIndex, FileDescriptor fileDescriptor) {
			fileDescriptors[mediaPlayerIndex] = fileDescriptor;
		}
		
		public int getMediaPlayer(Music mediaPlayerUser) {
			
			for (int i=0; i<count; i++) {
				if (mediaPlayerUsers[i] == mediaPlayerUser) { 
					return i;
				}
			}
			
			return -1;
		}

	}
	
	public static void forceReset() {
		
		for (int i=0; i<mediaPlayers.count; i++) {
			mediaPlayers.fileDescriptors[i] = null;
		}
	}
}
