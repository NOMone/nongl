package com.nomone.nongl;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import java.util.Collection;

/**
 * Created by raslanove on 8/21/16.
 */
public abstract class FacebookSdkWrapperBase {

    static class FacebookLoginResponse {
        public enum Type {UNKNOWN, NOT_RECEIVED, FACEBOOK_NOT_INTEGRATED, SUCCESS, CANCEL, ERROR}
        public Type type = Type.NOT_RECEIVED;
        public String errorMessage=null;
    }

    public abstract void sdkInitialize(Application app, Context context);
    public abstract void CallbackManager_onActivityResult(int requestCode, int resultCode, Intent data);

    public abstract void LoginManager_logInWithReadPermissions(Activity activity, Collection<String> permissions);
    public abstract FacebookLoginResponse getLoginResponse();
    public abstract void LoginManager_logOut();

    public abstract String getCurrentAccessToken();
}
