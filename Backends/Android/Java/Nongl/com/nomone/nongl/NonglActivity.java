package com.nomone.nongl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.nomone.nongl.GooglePlayGameServicesWrapperBase.NonglAchievement;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public abstract class NonglActivity extends Activity {
	
	private int layoutId, rootLayoutId;

	public static class NonglApplicationConfig {
		public boolean googlePlayGamesServicesAutoSignIn;
		public boolean fullColor;
		public int depthBufferBits;
		public int samplesCount;

		public NonglApplicationConfig() {
			this.googlePlayGamesServicesAutoSignIn = false;
			this.fullColor = false;
			this.depthBufferBits = 16;
			this.samplesCount = 0; 			
		}
		public NonglApplicationConfig(boolean googlePlayGamesServicesAutoSignIn, boolean fullColor, int depthBufferBits, int samplesCount) {
			this.googlePlayGamesServicesAutoSignIn = googlePlayGamesServicesAutoSignIn;
			this.fullColor = fullColor;
			this.depthBufferBits = depthBufferBits;
			this.samplesCount = samplesCount; 
		}
	}	
	private NonglApplicationConfig config;

	private GooglePlayGameServicesWrapperBase googlePlayGameServicesWrapper=null; 
	
	static final String RENDERER_PREFERENCE = "Renderer name";
	
	private String rendererName;
	private GLSurfaceView glView;
	private boolean renderContinuously = true;
	
	private NONGLConfigChooser nonglConfigChooser;
	
	public NonglActivity(int layoutId, int rootLayoutId) {
		this.layoutId = layoutId;
		this.rootLayoutId = rootLayoutId;
		
		config = getConfig();
	}
	
	protected abstract NonglApplicationConfig getConfig();
	
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
	// Interface methods
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
	
	public void preventSleep(boolean prevent) {
		final boolean preventFinal = prevent;
		glView.post(new Runnable() {
			@Override public void run() {
				glView.setKeepScreenOn(preventFinal);
			}
		});
	}

	public void gameServicesShowSignInDialog() {
    	googlePlayGameServicesWrapper.signIn();
	}
	
	public boolean gameServicesIsSignInComplete() {
    	return googlePlayGameServicesWrapper.isSignInComplete();
	}

	public void gameServicesResetSignInState() {
    	googlePlayGameServicesWrapper.resetSignInState();
	}

	public boolean gameServicesIsUserSignedIn() {
    	return googlePlayGameServicesWrapper.isSignedIn();
	}

	public void gameServicesShowServicesDialog() {
    	googlePlayGameServicesWrapper.showAchievements();
	}
	
	public void gameServicesSubmitAchievement(String achievementId, boolean showBanner) {
    	googlePlayGameServicesWrapper.unlockAchievement(achievementId, showBanner);
	}
	
    public void gameServicesGetAchievements(ArrayList<NonglAchievement> outAchievements, AtomicBoolean outAchievementsLoadedFlag) {
    	googlePlayGameServicesWrapper.getAchievements(outAchievements, outAchievementsLoadedFlag);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
	// Application events
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remove window title,
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        //		WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Common.context = getApplicationContext();
        Common.assetManager = getAssets();

        setContentView(layoutId);
        
        // Preferences (TODO: maybe should be removed in the future?),
    	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	
    	// Opengl,
        setupGlSurfaceView(sharedPreferences.getString(RENDERER_PREFERENCE, null));
        
        // Games services,
    	try {
    		googlePlayGameServicesWrapper = (GooglePlayGameServicesWrapperBase) Class.forName("com.nomone.nongl.GooglePlayGameServicesWrapper").newInstance();
    	} catch (Exception e) {
    		googlePlayGameServicesWrapper = new GooglePlayGameServicesWrapperStub(this, config.googlePlayGamesServicesAutoSignIn);
    	}
    	googlePlayGameServicesWrapper.onCreate(savedInstanceState);
    }

    /*
    @Override protected void onStart() {
    	super.onStart();
    	if (googlePlayGameServicesWrapper!=null) googlePlayGameServicesWrapper.onStart();
    }

    @Override protected void onStop() {
    	super.onStop();
    	if (googlePlayGameServicesWrapper!=null) googlePlayGameServicesWrapper.onStop();    	
    }
    */
    
    @Override protected void onDestroy() {
    	super.onDestroy();    	
    }
    
    @Override protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	googlePlayGameServicesWrapper.onActivityResult(requestCode, resultCode, data);
		Common.facebookSdkWrapper.CallbackManager_onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    protected void onResume() {
		NONGLNatives.nativeLogE("resume");

    	googlePlayGameServicesWrapper.onStart();

    	super.onResume();
		glView.onResume();
    	Common.currentActivity = this;

		synchronized (NONGLNatives.nativeSyncLock) {			
	    	NONGLNatives.nativeOnResume();
    		glView.requestRender();
		}
    }
    
    @Override
    protected void onPause() {
		
    	NONGLNatives.nativeLogE("pause");

    	googlePlayGameServicesWrapper.onStop();

    	Common.currentActivity = null;

    	glView.onPause();
    	super.onPause();

    	synchronized (NONGLNatives.nativeSyncLock) {			
			NONGLNatives.nativeOnPause();
			// TODO: Get rid of the GlSurfaceView, use plain SurfaceView and do EGLcontext handling yourself.
	    	glView.queueEvent(new Runnable() { 
				@Override public void run() { NONGLNatives.nativeReleaseOpenGlObjects(); }
			});
		}
    }
    
    @Override
    public void onBackPressed() {
    	NONGLNatives.nativeOnBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

    	//glView.requestRender();	// To prevent a bug where application freezes after waking on a phone call (couldn't reproduce).
    	
		if (keyCode == KeyEvent.KEYCODE_MENU) {			
			NONGLNatives.nativeOnMenuPressed();
		} else if ((keyCode == KeyEvent.KEYCODE_BACK) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_UP) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) ||
					(keyCode == KeyEvent.KEYCODE_VOLUME_MUTE)) {
			return super.onKeyDown(keyCode, event);
    	} else {
        	// TODO: how about non-unicode keys?
			NONGLNatives.nativeOnKeyDown(event.getUnicodeChar());
    		//android.util.Log.e("Key", "" + keyCode);
    	}
		
		return true;
    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

    	if ((keyCode == KeyEvent.KEYCODE_MENU) || (keyCode == KeyEvent.KEYCODE_BACK)) {
			return super.onKeyUp(keyCode, event);
    	} else {
			NONGLNatives.nativeOnKeyUp(event.getUnicodeChar());
    	}
	
		return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    // Opengl setup
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    private void setupGlSurfaceView(String rendererName) {
    	
    	boolean justGetRenderer;
    	if (rendererName == null) {
    		justGetRenderer = true;
    	} else {
    		justGetRenderer = false;

    		NONGLNatives.nativeLogW("Renderer: " + rendererName);
			
    		// If mali400 or adreno, enable MSAA 4x,
    		if (rendererName.contains("Mali-400")) {
    			if (config.samplesCount < 4) {
    				NONGLNatives.nativeLogW("Enabled MSAA 4x because it wouldn't hurt!");
    				config.samplesCount = 4;
    			}
    		}	
    	}
    	
        glView = new SuperGLSurfaceView(this);
        glView.setEGLContextClientVersion(2);         
        glView.setEGLConfigChooser(nonglConfigChooser = new NONGLConfigChooser(config.fullColor, config.depthBufferBits, config.samplesCount));
        //glView.setEGLConfigChooser(false); // Disable z-buffer.
        glView.setRenderer(new NonglRenderer(justGetRenderer));
        glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
         
        ViewGroup rootLayout = (ViewGroup) findViewById(rootLayoutId);
        rootLayout.addView(glView, 0);    	
    }
    
    public void setRenderModeContinuously(boolean renderContinously) {
    	this.renderContinuously = renderContinously;
    	if (renderContinously) glView.requestRender();
    }
    
    public boolean usesCoverageAa() {
    	
        if (nonglConfigChooser.usesCoverageAa())
            return true;
        
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    // SuperGLSurfaceView
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    
	class SuperGLSurfaceView extends GLSurfaceView {

		/** Maximum number of pointers (fingers) are supported. */
		private static final int MAX_POINTERS_COUNT = 20;

		/** Used to determine which finger is associated with the touch move events. */ 
		private PointerState[] pointersState;

		private long lastDragTime;
		private static final long MIN_INTER_DRAG_TIME_MILLIS = 16;
		
		public SuperGLSurfaceView(Context context) {
			super(context);
		}
		
		@Override
		public boolean onTouchEvent(MotionEvent event) {
		
			//requestRender(); // To prevent a bug where application freezes after waking on a phone call (couldn't reproduce).

			if (pointersState == null) {

				// Initialize pointers state data structures,
				pointersState = new PointerState[MAX_POINTERS_COUNT];
				for (int i=0; i<pointersState.length; i++) {
					pointersState[i] = new PointerState();
				}
			}
			
			int action = event.getAction();
			
			// Limit dispatched events,
			long currentTime = System.currentTimeMillis();			
			if ((action & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_MOVE) {
				if ((currentTime - lastDragTime) < MIN_INTER_DRAG_TIME_MILLIS) {
					return true;				
				} else {
					lastDragTime = currentTime;
				} 
			}
			
			// Since Android 2.0, a new set of action were introduced, like ACTION_POINTER_DOWN and
			// ACTION_POINTER_UP. These actions has parameters encoded inside the action value itself.
			// So, in order to be able to identify the action regardless of its parameters, we have
			// to use a mask,
			switch (action & MotionEvent.ACTION_MASK) {
			
				case MotionEvent.ACTION_DOWN: {
					// An action of this type is generated only if one pointer is pressed. When
					// more than one pointer is pressed, ACTION_POINTER_DOWN is generated instead, 
					NONGLNatives.nativeOnTouchDown(
							event.getPointerId(0), 
							normalizeX(event.getX()), normalizeY(event.getY()));					
				} 
				break;
	
				case MotionEvent.ACTION_MOVE: {
	
					// An action of this type is generated if any number of pointers is pressed
					// (including one pointer only). Unfortunately, there is no way that I know
					// of to directly determine the specific pointer which generated this event,
					// so we need some sort of workaround,
	
					// We are going to check all the currently down pointers for the ones whose
					// positions were changed,
					int numPointers = event.getPointerCount();
					for (int i = 0; i < numPointers; i++) {
	
						float x = event.getX(i);
						float y = event.getY(i);
	
						// The ID of this pointer,
						int pointerId = event.getPointerId(i);
	
						// Discard pointers above the max allowed,
						if (pointerId >= MAX_POINTERS_COUNT)
							continue;
						
						// If this is the pointer that moved,
						if ((pointersState[pointerId].x != x) ||
							(pointersState[pointerId].y != y)) {
				
							NONGLNatives.nativeOnTouchDrag(pointerId, normalizeX(x), normalizeY(y));
						}
					}
				}
				break;
	
				case MotionEvent.ACTION_UP: {
	
					// An action of this type is generated only if one pointer is pressed. When
					// more than one pointer is pressed, ACTION_POINTER_UP is generated instead, 
					NONGLNatives.nativeOnTouchUp(
							event.getPointerId(0), 
							normalizeX(event.getX()), normalizeY(event.getY()));					
				}
				break;
	
				case MotionEvent.ACTION_POINTER_DOWN: {
	
					// Extract the index of the pointer,
					final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
					NONGLNatives.nativeOnTouchDown(
							event.getPointerId(pointerIndex), 
							normalizeX(event.getX(pointerIndex)), normalizeY(event.getY(pointerIndex)));					
				}
				break;
	
				case MotionEvent.ACTION_POINTER_UP: {
	
					// Extract the index of the pointer,
					final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
					NONGLNatives.nativeOnTouchUp(
							event.getPointerId(pointerIndex), 
							normalizeX(event.getX(pointerIndex)), normalizeY(event.getY(pointerIndex)));					
					break;
				}
				
				case MotionEvent.ACTION_SCROLL: {

					// This is supported only in api level 12 and above,
					if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB_MR1) break; 
					
					// TODO: use reflection,
					// TODO: test,
					/*
					int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
					NONGLNatives.nativeOnScroll(
							normalizeX(event.getX(pointerIndex)), normalizeY(event.getY(pointerIndex)), 
							event.getAxisValue(MotionEvent.AXIS_HSCROLL), event.getAxisValue(MotionEvent.AXIS_VSCROLL));
					*/

					// TODO: remove these commented lines if not needed,
					/*
					PointerCoords pointerCoords;
					event.getPointerCoords(pointerIndex, pointerCoords);
					NONGLNatives.nativeOnScroll(
							pointerCoords.x, pointerCoords.y, 
							event.getAxisValue(MotionEvent.AXIS_HSCROLL), event.getAxisValue(MotionEvent.AXIS_VSCROLL));
					*/
					break;
				}
				
				case MotionEvent.ACTION_CANCEL: {

					// Extract the index of the pointer,
					final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;

					NONGLNatives.nativeOnTouchCancel(
							event.getPointerId(pointerIndex), 
							normalizeX(event.getX(pointerIndex)), normalizeY(event.getY(pointerIndex)));					
					break; 
				}
				
			}

			// Mark this event as consumed, no further processing by the system is required,
			return true;
		}

		private float normalizeX(float x) {
			int width = getWidth();
			return x / width;
		}

		private float normalizeY(float y) {
			int height = getHeight();
			return (height - y) / height;
		}
		
		/** Some data about a pointer (finger). */ 
		class PointerState {
			float x, y;
		}
	}

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    // NonglRenderer
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    class NonglRenderer implements Renderer {

		private boolean justGetRenderer;
		NonglRenderer(boolean justGetRenderer) {
			this.justGetRenderer = justGetRenderer;
		}

    	
		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config) {
			
			if (justGetRenderer) {
				rendererName = gl.glGetString(GL10.GL_RENDERER);
				
				NONGLNatives.nativeLogW("vendor: " + gl.glGetString(GL10.GL_VENDOR));
				NONGLNatives.nativeLogW("version: " + gl.glGetString(GL10.GL_VERSION));
				 
				runOnUiThread(new Runnable() {
					@Override 
					public void run() {
						ViewGroup rootLayout = (ViewGroup) findViewById(rootLayoutId);
				        rootLayout.removeView(glView);
				        
			        	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(NonglActivity.this);
			    		Editor preferencesEditor = sharedPreferences.edit(); 
			    		preferencesEditor.putString(RENDERER_PREFERENCE, rendererName);
			    		preferencesEditor.commit();

				        setupGlSurfaceView(rendererName);
					}
				});		        
		        return ;
			}
				
			/*
		     String s = GLES20.glGetString(GLES20.GL_EXTENSIONS);
		     if (s.contains("GL_COMPRESSED_RGBA8_ETC2_EAC") ||
	    		 s.contains("GL_OES_compressed_ETC2_RGBA8_texture")) {
		    	 Log.e("sddsf", "found!");
		     } else {
		    	 Log.e("sddsf", s);
		     }
		    */
			
			// Notify native side to reload textures,
			NONGLNatives.nativeOnSurfaceCreated();
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) {

			if (justGetRenderer) 
				return;

			NONGLNatives.nativeOnSurfaceChanged(width, height);
		}

		@Override
		public void onDrawFrame(GL10 gl) {

			if (justGetRenderer) 
				return;

			synchronized (NONGLNatives.nativeSyncLock) {

				NONGLNatives.nativeOnDrawFrame();
				
				if (renderContinuously) glView.requestRender();
			}
		}

    }
}
