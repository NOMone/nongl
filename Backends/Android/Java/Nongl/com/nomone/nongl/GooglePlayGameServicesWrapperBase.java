package com.nomone.nongl;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public abstract class GooglePlayGameServicesWrapperBase {
	
    public GooglePlayGameServicesWrapperBase(Activity parentActivity, boolean autoStartSignInFlow) {
    	init(parentActivity, autoStartSignInFlow);
    }
    public abstract void init(Activity parentActivity, boolean autoStartSignInFlow);    
    public abstract void onCreate(Bundle savedInstanceState);
    public abstract void resetSignInState();
    public abstract boolean isSignedIn();
    public abstract void onStart();
    public abstract void onStop();
    public abstract void showAchievements();
    public abstract void showLeaderboards();
    public abstract void unlockAchievement(String achievementId, boolean showPopUp);
    public static class NonglAchievement {
    	public String identifier;
    	public float percentCompleted; 
    }
    public abstract void getAchievements(ArrayList<NonglAchievement> outAchievements, AtomicBoolean outAchievementsLoadedFlag);
    public abstract boolean onActivityResult(int requestCode, int resultCode, Intent intent);
    public abstract String getPlayerDisplayName();
    
    public abstract void signIn();
    public abstract void signOut();
    public abstract boolean isSignInComplete();
}
