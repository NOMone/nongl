package com.nomone.nongl;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.opengl.GLSurfaceView.EGLConfigChooser;

public class NONGLConfigChooser implements EGLConfigChooser {
	
    private boolean fullColor;
    private int samplesCount;
    private int depthBufferBits;

    private int[] returnValue;
    private boolean usesNVidiaCoverageAa = false;

    public NONGLConfigChooser(boolean fullColor, int depthBufferBits, int samplesCount) {

    	this.fullColor = fullColor;
    	this.depthBufferBits = depthBufferBits;
    	this.samplesCount = samplesCount;
    	
    	returnValue = new int[1];
    }
    
    @Override
    public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
    	
    	// Try to find the required configuration,
        int currentSamplesCount = samplesCount;
        int numConfigs;
        int[] configSpec;
        do {
            configSpec = new int[] {
                    EGL10.EGL_RED_SIZE, 5,		// Minimum size. OpenGL should return the highest bit depths first (i.e: if 
                    EGL10.EGL_GREEN_SIZE, 6,	// full color is supported, it's returned first).
                    EGL10.EGL_BLUE_SIZE, 5,		//
                    EGL10.EGL_DEPTH_SIZE, depthBufferBits, 
                    // Requires that setEGLContextClientVersion(2) is called on the view.
                    EGL10.EGL_RENDERABLE_TYPE, 4 /* EGL_OPENGL_ES2_BIT */,
                    EGL10.EGL_SAMPLE_BUFFERS, (samplesCount > 0) ? 1 : 0,
                    EGL10.EGL_SAMPLES, currentSamplesCount,
                    EGL10.EGL_NONE
            };

	        if (!egl.eglChooseConfig(display, configSpec, null, 0, returnValue)) {
	        	NONGLNatives.nativeLogW("Initial eglChooseConfig failed at MSAA " + currentSamplesCount + "x");
	        }        
        	
	        numConfigs = returnValue[0];
        	        
	        // If no configurations returned and multi-sampling enabled, try nVidia multi-sampling,
	    	if ((numConfigs <= 0) && (samplesCount > 0)) {
	
	    		NONGLNatives.nativeLogW("Couldn't find EGL configuration with MSAA " + currentSamplesCount + "x.");
	
	    		// Probably it's nVidia Tegra2 issue. Try to create a coverage multi-sampling 
	    		// configuration, for the nVidia Tegra2. See the EGL_NV_coverage_sample documentation,
	
	            final int EGL_COVERAGE_BUFFERS_NV = 0x30E0;
	            final int EGL_COVERAGE_SAMPLES_NV = 0x30E1;
	            
	    		configSpec = new int[] {
	                    EGL10.EGL_RED_SIZE, 5, 
	                    EGL10.EGL_GREEN_SIZE, 6,
	                    EGL10.EGL_BLUE_SIZE, 5,
	                    EGL10.EGL_DEPTH_SIZE, depthBufferBits, 
	                    EGL10.EGL_RENDERABLE_TYPE, 4,
	                    
	                    EGL_COVERAGE_BUFFERS_NV, 1 /* true */,
	                    EGL_COVERAGE_SAMPLES_NV, currentSamplesCount,  // always 5 in practice on tegra 2
	                    
	                    EGL10.EGL_NONE
	            };
	            
	            if (!egl.eglChooseConfig(display, configSpec, null, 0, returnValue)) {
	            	NONGLNatives.nativeLogW("Attempting nVidia coverage sampling "+ currentSamplesCount + "x eglChooseConfig failed.");
	            }
	            
	            numConfigs = returnValue[0];
	            
	            if (numConfigs > 0) {
	            	usesNVidiaCoverageAa = true;
	            }
	    	}
	    	
	    	currentSamplesCount--;
        } while ((numConfigs <= 0) && (currentSamplesCount > 1));

    	
    	// If multi-sampling didn't work at all,
        if ((numConfigs <= 0) && (samplesCount > 0)) {

        	NONGLNatives.nativeLogW("Couldn't find EGL configuration with nVidia coverage sampling. Trying no multi-sampling at all.");

    		// We have tried normal multi-sampling and nVidia multi-sampling. Time to try without multi-sampling at all,
        	samplesCount = 0;
            configSpec = new int[]{
                    EGL10.EGL_RED_SIZE, 5,
                    EGL10.EGL_GREEN_SIZE, 6,
                    EGL10.EGL_BLUE_SIZE, 5,
                    EGL10.EGL_DEPTH_SIZE, depthBufferBits, 
                    EGL10.EGL_RENDERABLE_TYPE, 4 /* EGL_OPENGL_ES2_BIT */,
                    EGL10.EGL_NONE
            };

            if (!egl.eglChooseConfig(display, configSpec, null, 0, returnValue)) {
            	NONGLNatives.nativeLogW("Attemping no multi-sampling at all eglChooseConfig failed.");
            } else {
            	NONGLNatives.nativeLogW("Attemping no multi-sampling at all worked.");
            }
            
            numConfigs = returnValue[0];
        }
        
        // Nothing worked out,
        if (numConfigs <= 0) {
			throw new IllegalArgumentException("No suitable EGL config found.");
        }

        // Configuration(s) found!
        // Get all matching configurations.
        EGLConfig[] configs = new EGLConfig[numConfigs];
        if (!egl.eglChooseConfig(display, configSpec, configs, numConfigs, returnValue)) {
            throw new IllegalArgumentException("Actual configurations retreival using eglChooseConfig failed");
        }
        
        // Return the closest configuration,
        if (fullColor) {
        	return configs[0];
        } else {
   
            // eglChooseConfigs returns configurations with higher RGB bit depth
            // first: Even though we might have asked for rgb565 configurations, 
            // rgb888 configurations are considered "better" and are returned first.
            // We need to explicitly filter the data returned by eglChooseConfig,

	        int index = -1;
	        for (int i = 0; i < configs.length; ++i) {
	            if (findConfigAttrib(egl, display, configs[i], EGL10.EGL_RED_SIZE, 0) == 5) {
	                index = i;
	                break;
	            }
	        }
	        
	        if (index == -1) {
	        	NONGLNatives.nativeLogW("Did not find 16bit color config, using first.");
	            return configs[0];
	        } else {
	        	return configs[index];
	        }
        }
    }

    private int findConfigAttrib(EGL10 egl, EGLDisplay display, EGLConfig config, int attribute, int defaultValue) {
        if (egl.eglGetConfigAttrib(display, config, attribute, returnValue)) {
            return returnValue[0];
        }
        return defaultValue;
    }

    public boolean usesCoverageAa() {
        return usesNVidiaCoverageAa;
    }
}