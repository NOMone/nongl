//
//  AppDelegate.m
//  White
//
//  Created by Omar El Sayyed on 8/20/14.
//  Copyright (c) 2014 Omar El Sayyed. All rights reserved.
//

#import "AppDelegate.h"
#include "../../SystemLog.h"
#include "../../Nongl.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    LOGE("Application will resign active");
    Nongl::onPause(true);
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    LOGE("Application did enter background");
    Nongl::onPause(true);
    Nongl::discardOpenGlObjects();
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    LOGE("Application will enter foreground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    LOGE("Application did become active");
    Nongl::onResume(true);
    Nongl::onSurfaceCreated();
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    LOGE("Application will terminate");
    Nongl::destroy();
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    LOGE("Application received memory warning");
    Nongl::discardOpenGlObjects();
    Nongl::onSurfaceCreated();
}

@end
