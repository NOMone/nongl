
#include "Nongl/SystemFileServices.h"
#include "Nongl/TextUtils.h"
#include "Nongl/File.h"

#include <sys/stat.h>
#include <stdexcept>

static bool fileExists(const char *filename) {
   	struct stat st;
    return stat(filename, &st) == 0;
}

static TextBuffer prependAssetsDirectory(const TextBuffer &path) {
    
    static char assetsDirectory[1024];
    static bool assetsDirectoryRetrieved = false;

    // Get the assets directory,
    if (!assetsDirectoryRetrieved) {
        CFURLRef assetsDirectoryUrl = CFBundleCopyResourcesDirectoryURL(CFBundleGetMainBundle());
        CFURLGetFileSystemRepresentation(assetsDirectoryUrl, true, (UInt8 *) assetsDirectory, sizeof(assetsDirectory));
        CFRelease(assetsDirectoryUrl);
        assetsDirectoryRetrieved = true;
    }
    
    // Append the file to it,
    return TextBuffer(assetsDirectory).append("/assets/").append(path);
}

static TextBuffer prependApplicationSdcardDirectory(const TextBuffer &path) {
 
    static char sdcardDirectory[1024];
    static bool sdcardDirectoryRetrieved = false;

    // Get the sdcard directory,
    if (!sdcardDirectoryRetrieved) {
        NSString *appSupportDir = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
        NSString *appBundleID = [[NSBundle mainBundle] bundleIdentifier];
        NSString *appDirectory = [appSupportDir stringByAppendingPathComponent:appBundleID];
        
        strcpy(sdcardDirectory, [appDirectory UTF8String]);
        sdcardDirectoryRetrieved = true;
        
        // If folder doesn't exist yet, create it,
        if (!fileExists(sdcardDirectory)) {
            NSError *error;
            if (![[NSFileManager defaultManager] createDirectoryAtPath:appDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
                NSLog(@"Create directory error: %@", error);
            }
        }
    }
    
    // Append the file to it,
    return TextBuffer(sdcardDirectory).append("/").append(path);
}

static TextBuffer getFileFullPath(const Ngl::File &file) {
    
    // Find the file final location,
    Ngl::FileLocation fileLocation = file.getLocation();
    switch (fileLocation) {
        case Ngl::FileLocation::ASSETS: return prependAssetsDirectory(file.getFullPath());
        case Ngl::FileLocation::SDCARD: return prependApplicationSdcardDirectory(file.getFullPath());
        default:
            throw std::runtime_error(
                                     TextBuffer("File location not supported (only assets or sdcard): ").
                                     append(file.getFullPath()).getConstText());
    }
}

bool Ngl::systemFileExists(const Ngl::File &file) {
    return fileExists(getFileFullPath(file));
}

int32_t Ngl::systemGetFileSize(const Ngl::File &file) {
    
    // Find the file final location,
    TextBuffer fileFullPath = getFileFullPath(file);
    
    // Get file size,
    struct stat fileState;
    if (stat(fileFullPath, &fileState) != 0) {
        throw std::runtime_error(TextBuffer("Couldn't get file size (doesn't exist): ").append(fileFullPath).getConstText());
    }
    return (int32_t) fileState.st_size;
}

void Ngl::systemMakeDir(const Ngl::File &file) {
    
    // Writing to assets not supported (for consistency with other platforms),
    if (file.getLocation() == Ngl::FileLocation::ASSETS) {
        throw std::runtime_error(TextBuffer("Can't make directories in assets: ").append(file.getFullPath()).getConstText());
    }
    
    // Find the file final location,
    TextBuffer directoryFullPath = getFileFullPath(file);
    if (mkdir(directoryFullPath, 0777)) {
        throw std::runtime_error(TextBuffer("Couldn't make directory: ").append(directoryFullPath).getConstText());
    }
}

int32_t Ngl::systemReadFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) {
    
    // Find the file final location,
    TextBuffer fileFullPath = getFileFullPath(file);
    
    // Get file size to prepare the output vector,
    struct stat fileState;
    if (stat(fileFullPath, &fileState) != 0) {
        throw std::runtime_error(TextBuffer("Couldn't read file (doesn't exist): ").append(fileFullPath).getConstText());
    }
    int32_t fileSize = (int32_t) fileState.st_size;
    
    // Force upper limit of read size,
    int32_t readSize = fileSize - offsetInFile;
    if (maxReadSize && (readSize > maxReadSize)) readSize = maxReadSize;
    
    // Open the file,
    FILE *fileHandle = fopen(fileFullPath, "rb");
    if (!fileHandle) {
        throw std::runtime_error(TextBuffer("Couldn't open file for reading: ").append(fileFullPath).getConstText());
    }
    
    // Forward to the offset,
    if (offsetInFile) {
        if (fseek (fileHandle, offsetInFile, SEEK_SET)) {
            throw std::runtime_error(TextBuffer("Couldn't seek in file for reading: ").append(fileFullPath).getConstText());
        }
    }
    
    // Prepare the output vector to hold the file data,
    int32_t readBytesCount;
    if (offsetInVector == outputVector.size()) {
        
        // Adjust the original vector to hold the new data,
        int32_t originalVectorSize = (int32_t) outputVector.size();
        outputVector.resize(originalVectorSize + readSize);
        
        // Read the data into the output vector directly,
        readBytesCount = (int32_t) fread(&outputVector[originalVectorSize], 1, readSize, fileHandle);
        
        // Fit the vector to the data,
        int32_t unreadBytesCount = readSize - readBytesCount;
        if (unreadBytesCount) outputVector.resize(originalVectorSize + readBytesCount);
        
    } else {
        
        // Read into a temporary vector first,
        std::vector<uint8_t> tempVector;
        tempVector.resize(readSize);
        
        // Read the data into the temporary vector,
        readBytesCount = (int32_t) fread(&tempVector[0], 1, readSize, fileHandle);
        
        // Fit the vector to the data,
        int32_t unreadBytesCount = readSize - readBytesCount;
        if (unreadBytesCount) tempVector.resize(readBytesCount);
        
        // Insert temp vector into the output vector,
        outputVector.insert(outputVector.begin()+offsetInVector, tempVector.begin(), tempVector.end());
    }
    
    // Close the file,
    fclose(fileHandle);
    
    return readBytesCount;
}

int32_t Ngl::systemWriteFile(const Ngl::File &file, const void *data, int32_t sizeBytes, bool append) {
    
    // Writing to assets not supported (for consistency with other platforms),
    if (file.getLocation() == Ngl::FileLocation::ASSETS) {
        throw std::runtime_error(TextBuffer("Can't write files to assets: ").append(file.getFullPath()).getConstText());
    }
    
    // Find the file final location,
    TextBuffer fileFullPath = getFileFullPath(file);
    
    // Open file,
    FILE *fileHandle;
    if (append) {
        fileHandle = fopen(fileFullPath, "ab");
        if (!fileHandle) {
            throw std::runtime_error(TextBuffer("Couldn't open file for appending: ").append(fileFullPath).getConstText());
        }
    } else {
        fileHandle = fopen(fileFullPath, "wb");
        if (!fileHandle) {
            throw std::runtime_error(TextBuffer("Couldn't open file for writing: ").append(fileFullPath).getConstText());
        }
    }
    
    // Write data to the file,
    int32_t writtenBytesCount = (int32_t) fwrite(data, 1, sizeBytes, fileHandle);
    fclose(fileHandle);
    
    return writtenBytesCount;
}
