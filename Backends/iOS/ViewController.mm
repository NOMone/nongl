#import "ViewController.h"
#include "../../Nongl.h"
#include "../../SystemEvents.h"
#include "../../SystemLog.h"

@interface ViewController () {
}

@property (strong, nonatomic) EAGLContext *context;

- (void)setupGL;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Remove status bar,
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        // iOS 7
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    } else {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    
    // Initialize OpenGL,
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat16;
    view.drawableColorFormat = GLKViewDrawableColorFormatRGBA8888;
    //view.drawableMultisample = GLKViewDrawableMultisample4X;
    
    view.userInteractionEnabled = YES;
    view.multipleTouchEnabled = true;
    
    self.preferredFramesPerSecond = 60;
   
    [self setupGL];

    // Set view controller for game services,
    #if NONGL_GAME_SERVICES
        setGameCenterViewController(self);
    #endif
}

- (void)dealloc
{
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    Nongl::onSurfaceCreated();
}

- (NSUInteger)supportedInterfaceOrientations
{
    //return (UIInterfaceOrientationMaskAll);
    return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortraitUpsideDown);
}

static CGPoint getNormalizedPoint(UIView* view, CGPoint locationInView)
{
    const float normalizedX = locationInView.x / view.bounds.size.width;
    const float normalizedY = 1.0f - (locationInView.y / view.bounds.size.height);
    return CGPointMake(normalizedX, normalizedY);
}

#include <vector>
static std::vector<UITouch *> touchesTable;
int addTouch(UITouch *touch) {
    
    // Find an emtpy slot for the new touch,
    int touchesCount = (int) touchesTable.size();
    for (int i=0; i<touchesCount; i++) {
        if (!touchesTable[i]) {
            touchesTable[i] = touch;
            return i;
        }
    }
    
    // No empty slots found, add a new one,
    touchesTable.push_back(touch);
    return touchesCount;
}

int removeTouch(UITouch *touch) {

    // Empty the touch slot in the table,
    int touchId = getTouchId(touch);
    if (touchId != -1) touchesTable[touchId] = 0;
    return touchId;
}

int getTouchId(UITouch *touch) {

    // Find the touch in the table,
    int touchesCount = (int) touchesTable.size();
    for (int i=0; i<touchesCount; i++) {
        if (touchesTable[i] == touch) return i;
    }
    
    // Touch not found. Should never be reached,
    return -1;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    for (UITouch *touch in touches) {
        CGPoint locationInView = [touch locationInView:self.view];
        CGPoint normalizedPoint = getNormalizedPoint(self.view, locationInView);
        systemEventTouchDown(addTouch(touch), normalizedPoint.x, normalizedPoint.y);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
   
    for (UITouch *touch in touches) {
        CGPoint locationInView = [touch locationInView:self.view];
        CGPoint normalizedPoint = getNormalizedPoint(self.view, locationInView);
        int touchId = getTouchId(touch);
        if (touchId != -1) systemEventTouchDrag(touchId, normalizedPoint.x, normalizedPoint.y);
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    for (UITouch *touch in touches) {
        CGPoint locationInView = [touch locationInView:self.view];
        CGPoint normalizedPoint = getNormalizedPoint(self.view, locationInView);
        int touchId = removeTouch(touch);
        if (touchId != -1) systemEventTouchUp(touchId, normalizedPoint.x, normalizedPoint.y);
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    
    for (UITouch *touch in touches) {
        CGPoint locationInView = [touch locationInView:self.view];
        CGPoint normalizedPoint = getNormalizedPoint(self.view, locationInView);
        int touchId = removeTouch(touch);
        if (touchId != -1) systemEventTouchCancel(touchId, normalizedPoint.x, normalizedPoint.y);
    }
}

static bool onSurfaceChangedCalled = false;
static float lastWidth, lastHeight;
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{

    float scale = [[UIScreen mainScreen] scale];
    float width = rect.size.width*scale;
    float height = rect.size.height*scale;
    if ((!onSurfaceChangedCalled) ||
        (lastWidth != width) || (lastHeight != height)) {
        
        // Note: screen dimensions might be swapped based on orientation.
        // TODO: Test on newer devices when updating Xcode.
        LOGE("Screen dimensions: %f, %f", rect.size.width*scale, rect.size.height*scale);
        Nongl::onSurfaceChanged(width, height);

        onSurfaceChangedCalled = true;
        lastWidth = width;
        lastHeight = height;
    }

    Nongl::onNewFrame();
}

/////////////////
// Game Center
/////////////////

#if NONGL_GAME_SERVICES

- (void)authenticateGameCenterLocalPlayer
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];

    setLastGameCenterAuthenticationComplete(false);
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error) {
        if (viewController != nil) {
            setGameCenterAuthenticationDialog(viewController);
        } else {
            setLastGameCenterAuthenticationComplete(true);
            Nongl::onResume(false);
        }
    };
}

- (void)showGameCenter
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    
    if (gameCenterController != nil) {
        Nongl::onPause(false);
        gameCenterController.gameCenterDelegate = self;
        [self presentViewController: gameCenterController animated: YES completion:nil];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    Nongl::onResume(false);
    [self dismissViewControllerAnimated:YES completion:nil];
}

#endif

@end
