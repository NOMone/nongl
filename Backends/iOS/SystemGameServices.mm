#if NONGL_GAME_SERVICES

#include "../../SystemGameServices.h"

#include "../../SystemLog.h"
#include "../../Nongl.h"

#import "ViewController.h"

/////////////////////////////
// API
/////////////////////////////

static ViewController *viewController;

void systemGameServicesConnect() {
    [viewController authenticateGameCenterLocalPlayer];
}

bool showGameCenterAuthenticationDialog();
void systemGameServicesShowSignInDialog(float delayMillis) {
    
    class ShowGameCenterAuthenticationDialogTask : public Runnable {
        bool run(void *data) {
            if (systemGameServicesIsSignInComplete() || showGameCenterAuthenticationDialog()) {
                return true;
            }
            return false;
        }
    };
    
    Nongl::scheduler.schedule(Ngl::shared_ptr<Runnable>(new ShowGameCenterAuthenticationDialogTask()), delayMillis);
}

static bool lastGameCenterAuthenticationComplete = false;
bool systemGameServicesIsSignInComplete() {
    return lastGameCenterAuthenticationComplete;
}

bool systemGameServicesIsUserSignedIn() {
    return [GKLocalPlayer localPlayer].isAuthenticated;
}

void systemGameServicesShowServicesDialog() {
    [viewController showGameCenter];
}

void systemGameServicesSubmitAchievement(const char *achievementIdentifier, float percent, bool showBanner) {
    systemGameServicesSubmitAchievement(achievementIdentifier, percent, showBanner, nullptr);
}

void systemGameServicesSubmitAchievement(const char *achievementIdentifier, float percent, bool showBanner, const Ngl::ManagedPointer<Runnable> &onSubmittedTask, void *onSubmittedTaskData) {

    NSString* identifier = [NSString stringWithCString:achievementIdentifier encoding:NSASCIIStringEncoding];
    GKAchievement *achievement = [[GKAchievement alloc] initWithIdentifier: identifier];
    
    if (achievement) {
        achievement.percentComplete = percent;
        achievement.showsCompletionBanner = showBanner;

        Ngl::ManagedPointer<Runnable> onSubmittedTaskCopy = onSubmittedTask;
        [achievement reportAchievementWithCompletionHandler:^(NSError *error) {
            if (!onSubmittedTaskCopy.expired()) onSubmittedTaskCopy->run(onSubmittedTaskData);
            if (error != nil) {
                LOGE("Error in reporting achievements: %s", [[error localizedDescription] UTF8String]);
                //NSLog(@"Error in reporting achievements: %@", error);
            }
        }];
    }
}

void systemGameServicesResetAchievements() {
    
    [GKAchievement resetAchievementsWithCompletionHandler:^(NSError *error) {
     if (error != nil) LOGE("Error in resetting achievements");
     }];
}

void systemGameServicesGetAchievements(const Ngl::ManagedPointer<Runnable> &onAchievementsLoadedTask) {

    Ngl::ManagedPointer<Runnable> onAchievementsLoadedTaskCopy = onAchievementsLoadedTask;
    [GKAchievement loadAchievementsWithCompletionHandler:^(NSArray *achievements, NSError *error) {
        
        if (error != nil) LOGE("Error in loading achievements: %s", [[error localizedDescription] UTF8String]);
        
        std::vector<Achievement> achievementsVector;
        if (achievements != nil) {
            for (GKAchievement* achievement in achievements) {
                achievementsVector.emplace_back([achievement.identifier UTF8String], (float) achievement.percentComplete);
            }
        }
        if (!onAchievementsLoadedTaskCopy.expired()) onAchievementsLoadedTaskCopy->run(&achievementsVector);
    }];
}

/////////////////////////////
// Internal functions
/////////////////////////////

void setGameCenterViewController(ViewController *vc) {
    viewController = vc;
}

void setLastGameCenterAuthenticationComplete(bool complete) {
    lastGameCenterAuthenticationComplete = complete;
}

static UIViewController *gameCenterAuthenticationDialog = 0;
void setGameCenterAuthenticationDialog(UIViewController *vc) {
    gameCenterAuthenticationDialog = vc;
}

bool showGameCenterAuthenticationDialog() {
    if (gameCenterAuthenticationDialog) {
        Nongl::onPause(false);
        [viewController presentViewController: gameCenterAuthenticationDialog animated: YES completion:nil];
        gameCenterAuthenticationDialog = 0;
        return true;
    }
    return false;
}

#endif