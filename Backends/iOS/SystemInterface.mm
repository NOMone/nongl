#include "Nongl/SystemInterface.h"

#include "Nongl/SystemLog.h"
#include "Nongl/TextUtils.h"
#include "Nongl/ByteBuffer.h"
#include "Nongl/File.h"
#include "Nongl/utils.h"

#import <AVFoundation/AVFoundation.h>
#include <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>

#include <sys/utsname.h>
#include <vector>

/////////////////////////////////////
// Preferences
/////////////////////////////////////

// Check:
// http://stackoverflow.com/questions/3566140/how-do-i-save-user-preferences-for-my-iphone-app
// For a maybe better alternative?

#define PREFERENCES_FILE "preferences"

class PreferencesManager {
    
    struct Preference {
        char *key;
        char *value;
        int valueSize;
    };
    
    std::vector<Preference> preferences;
public:
    
    PreferencesManager() { loadPreferences(); }
    ~PreferencesManager() {
        for (int i=0; i<preferences.size(); i++) {
            free(preferences[i].key);
            free(preferences[i].value);
        }
    }
    
    void savePreferences() {
        
        Ngl::ByteBuffer fileData;
        
        // Write key-value pairs count,
        int preferencesCount = (int32_t) preferences.size();
        fileData.pushInt(preferencesCount);
        
        // Write key-value pairs,
        for (int i=0; i<preferencesCount; i++) {
            
            fileData.pushInt((int32_t) strlen(preferences[i].key)+1);          // Write key size.
            fileData.pushString(preferences[i].key);                           // Write key.
            fileData.pushInt(preferences[i].valueSize);                        // Write value size.
            fileData.pushData(preferences[i].value, preferences[i].valueSize); // Write value.
        }
        
        // Save data to file,
        Ngl::File(Ngl::FileLocation::SDCARD, PREFERENCES_FILE).write(fileData.getData(), fileData.getDataSize(), false);
    }
    
    void loadPreferences() {
        
        // Read preferences file,
        Ngl::File preferencesFile(Ngl::FileLocation::SDCARD, PREFERENCES_FILE);
        if (!preferencesFile.exists()) {
            LOGE("Preferences file not found.");
            return ;
        }

        std::vector<uint8_t> data;
        preferencesFile.read(data);
        
        // Read key-value pairs count,
        unsigned char *currentByte = &data[0];
        int preferencesCount = *((int *) currentByte);
        currentByte += 4;
        
        // Read preferences,
        for (int i=0; i<preferencesCount; i++) {
            
            preferences.push_back(Preference());
            Preference &preference = preferences.back();
            
            // Read key,
            int keySize = *((int *) currentByte);
            currentByte += 4;
            
            preference.key = (char *) malloc(keySize);
            memcpy(preference.key, currentByte, keySize);
            currentByte += keySize;
            
            // Read value,
            preference.valueSize = *((int *) currentByte);
            currentByte += 4;
            
            preference.value = (char *) malloc(preference.valueSize);
            memcpy(preference.value, currentByte, preference.valueSize);
            currentByte += preference.valueSize;
        }
    }
    
    void addPreference(const char *key, const char *value, int valueSizeBytes=0) {
        
        // Default to zero terminated string,
        if (!valueSizeBytes) {
            valueSizeBytes = ((int32_t) strlen(value)) + 1;
        }
        
        // Update the preference if key is existing,
        for (int i=0; i<preferences.size(); i++) {
            if (strcmp(preferences[i].key, key) == 0) {
                free(preferences[i].value);
                preferences[i].value = (char *) malloc(valueSizeBytes);
                memcpy(preferences[i].value, value, valueSizeBytes);
                preferences[i].valueSize = valueSizeBytes;
                savePreferences();
                return;
            }
        }
        
        // Add a new preference,
        preferences.push_back(Preference());
        Preference &newPreference = preferences.back();
        
        newPreference.key = (char *) malloc(strlen(key)+1);
        strcpy(newPreference.key, key);
        
        newPreference.value = (char *) malloc(valueSizeBytes);
        memcpy(newPreference.value, value, valueSizeBytes);
        newPreference.valueSize = valueSizeBytes;
        
        savePreferences();
    }
    
    const char *getPreference(const char *key) {
        
        for (int i=0; i<preferences.size(); i++) {
            if (strcmp(preferences[i].key, key) == 0) {
                return preferences[i].value;
            }
        }
        return 0;
    }
};

// Instance of the preferences manager,
static PreferencesManager preferencesManager;

void javaSetBooleanPreference(const char *key, bool value) {
    
    if (value) {
        preferencesManager.addPreference(key, "true");
    } else {
        preferencesManager.addPreference(key, "false");
    }
}

bool javaGetBooleanPreference(const char *key, bool defaultValue) {
    
    const char *valueString = preferencesManager.getPreference(key);
    if (valueString) {
        if (strcmp(valueString, "true") == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    return defaultValue;
}

void javaSetIntPreference(const char *key, int value) {
    
    char valueString[12];
    sprintf(valueString, "%d", value);
    
    preferencesManager.addPreference(key, valueString);
}

int javaGetIntPreference(const char *key, int defaultValue) {
    
    const char *valueString = preferencesManager.getPreference(key);
    
    if (valueString) {
        int value;
        sscanf(valueString, "%d", &value);
        return value;
    }
    
    return defaultValue;
}

/////////////////////////////////////
// Music
/////////////////////////////////////

@interface NonglIOSAudioPlayer : AVAudioPlayer
@property double startTime;
@property double fadeDurationMillis;

- (void) continueFadeOut;
- (void) fadeOut:(int) milliSeconds;
@end

@implementation NonglIOSAudioPlayer

-(void) continueFadeOut {

    // Fade out,
    float linearRatio = (getTimeMillis() - self.startTime) / self.fadeDurationMillis;
    if (linearRatio >= 1) {
        [self stop];
        self.currentTime = 0;
        self.volume = 1.0;
        //[self prepareToPlay];
        return ;
    }
    
    float log = 1.0f - (logf(linearRatio * 10) / logf(10));
    if (log > 1) log = 1;
    self.volume = log;
    
    [self performSelector:@selector(continueFadeOut) withObject:nil afterDelay:0.016];
}

- (void) fadeOut:(int) milliSeconds {
    self.startTime = getTimeMillis();
    self.fadeDurationMillis = milliSeconds;
    [self performSelector:@selector(continueFadeOut) withObject:nil afterDelay:0.016];
}
@end

class IOSMusic {
    NonglIOSAudioPlayer *audioPlayer;
public:
    IOSMusic(const Ngl::File &file) {
        
        // Append the correct extension to the filename,
        TextBuffer filePathWithExtension = file.getFullPath();
        filePathWithExtension.append(".m4a");
        
        // Get file path url,
        NSString *nsPath = [NSString stringWithCString:filePathWithExtension.getText() encoding:NSASCIIStringEncoding];
        NSURL *pathURL = [NSURL fileURLWithPath : nsPath];
        
        // Create audio player for the file,
        audioPlayer = [[NonglIOSAudioPlayer alloc] initWithContentsOfURL:pathURL error:nil];
    }
    
    ~IOSMusic() {
        // ?
        audioPlayer = 0;
    }
    
    void play() {
        [audioPlayer play];
    }
    
    void stop() {
        [audioPlayer stop];
        audioPlayer.currentTime = 0;
        audioPlayer.volume = 1.0;
        [audioPlayer prepareToPlay];
    }
    
    void setLooping(bool looping) {
        if (looping) {
            audioPlayer.numberOfLoops = -1; //infinite loop
        } else {
            audioPlayer.numberOfLoops = 0; //infinite loop
        }
    }
    
    void pause() {
        [audioPlayer pause];
    }
    
    void fadeOut(int milliSeconds) {
        [audioPlayer fadeOut:milliSeconds];
    }
};

std::vector<IOSMusic *> musics;

int javaLoadMusicFromAssets(const char *fileName) {
    musics.push_back(new IOSMusic(Ngl::File(Ngl::FileLocation::ASSETS, fileName)));
    return (int) musics.size() - 1;
}

void javaDeleteMusic(int musicIndex) {
    delete musics[musicIndex];
    musics[musicIndex] = 0;
}

void javaPlayMusic(int musicIndex) {
    musics[musicIndex]->play();
}

void javaMusicSetLooping(int musicIndex, bool looping) {
    musics[musicIndex]->setLooping(looping);
}

void javaStopMusic(int musicIndex) {
    musics[musicIndex]->stop();
}

void javaPauseMusic(int musicIndex) {
    musics[musicIndex]->pause();
}

void javaResumeMusic(int musicIndex) {
    musics[musicIndex]->play();
}

void javaFadeOutMusic(int musicIndex, int milliSeconds) {
    musics[musicIndex]->fadeOut(milliSeconds);
}

/////////////////////////////////////
// Sound
/////////////////////////////////////

class IOSSound {
    SystemSoundID soundId;
public:
    IOSSound(const Ngl::File &file) {
        
        // Append the correct extension to the filename,
        TextBuffer filePathWithExtension = file.getFullPath();
        filePathWithExtension.append(".m4a");
        
        // Get file path url,
        NSString *nsPath = [NSString stringWithCString:filePathWithExtension.getText() encoding:NSASCIIStringEncoding];
        NSURL *pathURL = [NSURL fileURLWithPath : nsPath];
        
        // Create system sound id for the file,
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &soundId);
    }

    ~IOSSound() {
        // Must be done AFTER the sound is done playing (?),
        AudioServicesDisposeSystemSoundID(soundId);
    }
    
    void play() {
        AudioServicesPlaySystemSound(soundId);
    }
};

std::vector<IOSSound *> sounds;

int javaLoadSoundFromAssets(const char *fileName) {
    sounds.push_back(new IOSSound(Ngl::File(Ngl::FileLocation::ASSETS, fileName)));
    return (int32_t) sounds.size()-1;
}

void javaDeleteSound(int soundIndex) {
    delete sounds[soundIndex];
    sounds[soundIndex] = 0;
}

static bool muteSounds = false;
void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {
    if (!muteSounds) sounds[soundIndex]->play();
}

void javaStopSound(int soundIndex) {
    LOGE("Incomplete function 26 called");
}

void javaPauseSound(int soundIndex) {
    LOGE("Incomplete function 27 called");
}

void javaResumeSound(int soundIndex) {
	LOGE("incomplete function 28 called");
}

void javaSoundSetLooping(int soundIndex, bool looping) {
	LOGE("incomplete function 29 called");
}

void javaMuteSounds(bool mute) {
    muteSounds = mute;
}

/////////////////////////////////////
// Sockets
/////////////////////////////////////

int javaCreateSocket(const char *address, int port) { 
    LOGE("Incomplete function 31 called");
	return 0;
}

void javaCloseSocket(int socketId) {
    LOGE("Incomplete function 32 called");
}

void javaSocketWrite(int socketId, const char *bytes, int sizeBytes) {
    LOGE("Incomplete function 33 called");
}

void *javaSocketRead(int socketIndex) { 
    LOGE("Incomplete function 34 called");
	return 0;
}

/////////////////////////////////////
// Misc
/////////////////////////////////////

bool javaUsesCoverageAa() { return false; }

void javaLongToast(const char *toastText) {
    LOGE("Incomplete function 10 called");
}

void javaShortToast(const char *toastText) {
    LOGE("Incomplete function 11 called");
}

void javaBrowseUrl(const char *url, const char *alternativeUrl) {
    NSString *urlNSString = [NSString stringWithUTF8String:url];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlNSString]];
}

float javaGetDpi() {
    
    static float dpi=0;

    // First method, based on device ids,
    if (dpi == 0) {
        
        struct utsname sysinfo;
        if (uname(&sysinfo) == 0) {
            NSString *identifier = [NSString stringWithUTF8String:sysinfo.machine];

            // group devices with same points-density,
            NSArray *iDevices = @[@{@"identifiers": @[@"iPad1,1", // iPad
            @"iPad2,1", @"iPad2,2", @"iPad2,3", @"iPad2,4", // iPad 2
            @"iPad3,1", @"iPad3,2", @"iPad3,3", // iPad 3
            @"iPad3,4", @"iPad3,5", @"iPad3,6", // iPad 4
            @"iPad4,1", @"iPad4,2"], // iPad Air
            @"pointsPerInch": @132.0f},
            @{@"identifiers": @[@"iPod5,1", // iPod Touch 5th generation
            @"iPhone1,1", // iPhone 2G
            @"iPhone1,2", // iPhone 3G
            @"iPhone2,1", // iPhone 3GS
            @"iPhone3,1", @"iPhone3,2", @"iPhone3,3", // iPhone 4
            @"iPhone4,1", // iPhone 4S
            @"iPhone5,1", @"iPhone5,2", // iPhone 5
            @"iPhone5,3", @"iPhone5,4", // iPhone 5C
            @"iPhone6,1", @"iPhone6,2", // iPhone 5S
            @"iPad2,5", @"iPad2,6", @"iPad2,7", // iPad Mini
            @"iPad4,4", @"iPad4,5", // iPad Mini Retina
            @"i386", @"x86_64"], // iOS simulator (assuming iPad Mini simulator)
            @"pointsPerInch": @163.0f}];
            
            for (id deviceClass in iDevices) {
                for (NSString *deviceId in [deviceClass objectForKey:@"identifiers"]) {
                    if ([identifier isEqualToString:deviceId]) {
                        dpi = [[deviceClass objectForKey:@"pointsPerInch"] floatValue];
                        break;
                    }
                }
            }
        }
    }

    // If first method failed,
    if (dpi == 0) {
        float scale = 1;
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
            scale = [[UIScreen mainScreen] scale];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            dpi = 132 * scale;
        } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            dpi = 163 * scale;
        } else {
            dpi = 160 * scale;
        }
    }
	return dpi;
}

float javaGetTimeMillis() { 
    LOGE("Incomplete function 36 called");
	return 1;
}

void javaFinishActivity() {
    LOGE("Incomplete function 37 called");
}

void javaPreventSleep(bool prevent) {
    [[UIApplication sharedApplication] setIdleTimerDisabled:prevent];
}

void javaOpenOptionsMenu() {
    LOGE("Incomplete function 38 called");
}

void javaShowVirtualKeyboard() {
    LOGE("Incomplete function 39 called");
}

void javaHideVirtualKeyboard() {
    LOGE("Incomplete function 40 called");
}

std::vector<uint8_t> systemDownloadFile(const TextBuffer &url) {

    NSString *nsURLString = [NSString stringWithUTF8String:url.getConstText()];
    NSURL    *nsURL       = [NSURL URLWithString:nsURLString];
    NSData   *nsURLData   = [NSData dataWithContentsOfURL:nsURL];

    std::vector<uint8_t> data;
    if (nsURLData) {
        data.resize(nsURLData.length);
        memcpy(&data[0], nsURLData.bytes, nsURLData.length);
    }
    return data;
}

void javaExit() {
    LOGE("Exit called.");
}
