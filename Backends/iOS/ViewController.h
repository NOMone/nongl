//
//  ViewController.h
//  White
//
//  Created by Omar El Sayyed on 8/20/14.
//  Copyright (c) 2014 Omar El Sayyed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <GameKit/GameKit.h>

#if NONGL_GAME_SERVICES
    @interface ViewController : GLKViewController <GKGameCenterControllerDelegate>
    - (void) authenticateGameCenterLocalPlayer;
    - (void) showGameCenter;
    @end

    // Implemented in SystemGameServices.mm,
    void setGameCenterViewController(ViewController *vc);
    void setLastGameCenterAuthenticationComplete(bool complete);
    void setGameCenterAuthenticationDialog(UIViewController *vc);

#else
    @interface ViewController : GLKViewController
    @end
#endif

