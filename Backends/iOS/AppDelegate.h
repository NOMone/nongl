//
//  AppDelegate.h
//  White
//
//  Created by Omar El Sayyed on 8/20/14.
//  Copyright (c) 2014 Omar El Sayyed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
