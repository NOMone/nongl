#include "../../SystemLog.h"

#include <stdarg.h>
#include <stdio.h>

#define LOG_IMPLEMENTATION \
	va_list arguments; \
	va_start(arguments, format); \
	log(logTag, format, arguments); \
	va_end(arguments); \

void log(const char *logTag, const char *format, va_list arguments) {
	printf("%s: ", logTag);
	vprintf(format, arguments);
	printf("\n");
}

void logI(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION }
void logW(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION }
void logE(const char *logTag, const char *format, ...) { LOG_IMPLEMENTATION }
