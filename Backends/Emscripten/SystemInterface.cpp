
#include "../../SystemInterface.h"
#include "../../SystemEvents.h"
#include "../../SystemLog.h"
#include "../../Nongl.h"
#include "../../TextUtils.h"

#include "../../../Src/Config.h"

#include <string.h>
#include <math.h>
#include <vector>
#include <cstdio>
#include <cstdlib>

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include <GL/glew.h>
#include <GL/glfw.h>

#include <emscripten/emscripten.h> 

#define WINDOW_WIDTH (DESIGN_WIDTH * 0.5f)
#define WINDOW_HEIGHT (DESIGN_HEIGHT * 0.5f)
int windowWidth = WINDOW_WIDTH;
int windowHeight = WINDOW_HEIGHT;

float dpi=96.0f;

int is_dragging = 0;

void GLFWCALL onWindowResized(int width, int height) {
	windowWidth = width; 
	windowHeight = height;
	Nongl::onSurfaceChanged(windowWidth, windowHeight);
}

int init_gl() {

    const int width = windowWidth, height = windowHeight;
 
    if (glfwInit() != GL_TRUE) {
        LOGE("glfwInit() failed\n");
        return GL_FALSE;
    }
 
	//glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 8);
    if (glfwOpenWindow(width, height, 8, 8, 8, 8, 16, 0, GLFW_WINDOW) != GL_TRUE) {
        LOGE("glfwOpenWindow() failed\n");
        return GL_FALSE;
    }
    
    glfwSwapInterval(1);
 
 	// Set window resized callback,
	glfwSetWindowSizeCallback(onWindowResized);

    return GL_TRUE;
}

void do_frame() {

	int width, height, isFullscreen;
	emscripten_get_canvas_size(&width, &height, &isFullscreen);
	if ((windowWidth!=width) || (windowHeight !=height)) {
		onWindowResized(width, height);
	}
	
	Nongl::onNewFrame();
	//glfwSwapBuffers();
}
 
void shutdown_gl() {
    glfwTerminate();
}

#define MAX_CHANNELS_COUNT 32
void initAudio() {
	SDL_Init(SDL_INIT_AUDIO);
	Mix_Init(MIX_INIT_OGG | MIX_INIT_MP3);

	int ret = Mix_OpenAudio(0, 0, 0, 0); // we ignore all these..
	if (ret) {
		LOGE("couldn't open audio.");
	}	
	
	Mix_AllocateChannels(MAX_CHANNELS_COUNT);
}
 
void shutDownAudio() {
	// Force quit sdl audio,
	while(Mix_Init(0)) 	Mix_Quit();
	Mix_CloseAudio();
}

void GLFWCALL onCursorPositionChanged(int x, int y) {

    int x_pos, y_pos;
    glfwGetMousePos(&x_pos, &y_pos);
    
	float normalized_x =      x_pos / (float)  windowWidth ;
	float normalized_y = 1 - (y_pos / (float) windowHeight);

	if (is_dragging) {
		systemEventTouchDrag(0, normalized_x, normalized_y);
		
		if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_1) == GLFW_RELEASE) {
			is_dragging = 0;
			systemEventTouchUp(0, normalized_x, normalized_y);
		}
	}
}

void GLFWCALL onMouseButtonEvent(int button, int action) {

	if (button == GLFW_MOUSE_BUTTON_1) {
      
		int x_pos, y_pos;
		glfwGetMousePos(&x_pos, &y_pos);
		
		float normalized_x =      x_pos / (float)  windowWidth ;
		float normalized_y = 1 - (y_pos / (float) windowHeight);

		if (action == GLFW_PRESS) {
			is_dragging = 1;
			systemEventTouchDown(0, normalized_x, normalized_y);
		} else if (action == GLFW_RELEASE) {
			is_dragging = 0;
			systemEventTouchUp(0, normalized_x, normalized_y);
		}  
	}    
}

void onScrollEvent(int yOffset) {

	int cursorX, cursorY;
	glfwGetMousePos(&cursorX, &cursorY);

	float normalizedX =      cursorX / (float) windowWidth  ;
	float normalizedY = 1 - (cursorY / (float) windowHeight);

	glfwSetMouseWheel(0);
	
	systemEventScroll(normalizedX, normalizedY, 0, (float) -yOffset);
}

// TODO: get rid of GLFW for its poor input support.
#include <ctype.h>
#define GLFW_KEY_SHIFT 287
#define GLFW_KEY_CTRL 289
#define GLFW_KEY_ESC 255

#define OLD_RIGHT_KEY 286
#define OLD_LEFT_KEY  285
#define OLD_UP_KEY    283
#define OLD_DOWN_KEY  284

#define NEW_RIGHT_KEY 262
#define NEW_LEFT_KEY  263
#define NEW_UP_KEY    265
#define NEW_DOWN_KEY  264

static bool capsLock = false;
static bool shift = false;
void GLFWCALL onKeyEvent(int key, int action) {

	if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESC:
				systemEventBackPressed();
				return ;

			case GLFW_KEY_CAPS_LOCK:
				capsLock = !capsLock;
				break;

			case GLFW_KEY_SHIFT:
				shift = true;
				key = 340;
				break;

			case GLFW_KEY_CTRL:
				key = 341;
				break;

			case OLD_RIGHT_KEY: key = NEW_RIGHT_KEY; break;
			case OLD_LEFT_KEY : key = NEW_LEFT_KEY ; break;
			case OLD_UP_KEY   : key = NEW_UP_KEY   ; break;
			case OLD_DOWN_KEY : key = NEW_DOWN_KEY ; break;

			default:
				if ((key >= 'A') && (key <= 'Z')) {
					bool capital = shift;
					if (capsLock) capital = !capital;

					if (!capital) key = tolower(key);
				}
				break;
		}
		
		systemEventKeyDown(key);

	} else if (action == GLFW_RELEASE) {

		switch (key) {
			case GLFW_KEY_ESC:
				return ;

			case GLFW_KEY_CAPS_LOCK:
				capsLock = !capsLock;
				break;

			case GLFW_KEY_SHIFT:
				shift = false;
				key = 340;
				break;

			case GLFW_KEY_CTRL:
				key = 341;
				break;

			case OLD_RIGHT_KEY: key = NEW_RIGHT_KEY; break;
			case OLD_LEFT_KEY : key = NEW_LEFT_KEY ; break;
			case OLD_UP_KEY   : key = NEW_UP_KEY   ; break;
			case OLD_DOWN_KEY : key = NEW_DOWN_KEY ; break;

			default:
				if ((key >= 'A') && (key <= 'Z')) {
					bool capital = shift;
					if (capsLock) capital = !capital;

					if (!capital) key = tolower(key);
				}
				break;
		}	

		systemEventKeyUp(key);
	} 
}

void initInput() {
	glfwSetMouseButtonCallback(onMouseButtonEvent);
	glfwSetMousePosCallback(onCursorPositionChanged);
	glfwSetMouseWheelCallback(onScrollEvent);
	
	glfwSetKeyCallback(onKeyEvent);
}

void loadPreferences();

int main() {

	// Initialize audio,
	initAudio();
			
	// Initialize OpenGL,
    if (init_gl() == GL_TRUE) {

		// Load preferences,
		loadPreferences();

    	// Initialize graphics,
		Nongl::onSurfaceCreated();
		Nongl::onSurfaceChanged(windowWidth, windowHeight);
    
    	// Initialize input,
		initInput();

		// Main loop,
        emscripten_set_main_loop(do_frame, 60, 1);
    }
 
    shutdown_gl();
	shutDownAudio();
 
    return 0;
}

float cosf(float angleInRadian) { return (float) cos(angleInRadian); }
float sinf(float angleInRadian) { return (float) sin(angleInRadian); }
float atanf(float angleInRadian) { return (float) atan(angleInRadian); }
float sqrtf(float value) { return (float) sqrt(value); }

/////////////////////////////////////
// Preferences
/////////////////////////////////////

#define PREFERENCES_FILE "preferences"

struct Preference {
	char *key;
	char *value;
	int valueSize;
};

class PreferencesDestructor {
public:
	std::vector<Preference> preferences;
	~PreferencesDestructor() {
		for (int i=0; i<preferences.size(); i++) {
			free(preferences[i].key);
			free(preferences[i].value);
		}
	}
};

static PreferencesDestructor preferencesDestructor;
static std::vector<Preference> &preferences = preferencesDestructor.preferences;

void setCookie(const char *cookieName, const char *cookieValue, int daysToExpire) {

	char setCookieFunction[] = 
		"function setCookie(cname,cvalue,exdays) {"
		"	var d = new Date();"
		"	d.setTime(d.getTime()+(exdays*24*60*60*1000));"
		"	var expires = 'expires=' + d.toGMTString();"
		"	document.cookie = cname + '=' + cvalue + '; ' + expires;"
		"}";
					
	int scriptLength = 
		1 +
		strlen(setCookieFunction) + 
		strlen("setCookie('','',);") + 
		strlen(cookieName) + 
		strlen(cookieValue) +
		12;           // For daysToExpire

	char *script = (char *) alloca(scriptLength);
	
	sprintf(script, "%ssetCookie('%s','%s',%d);", setCookieFunction, cookieName, cookieValue, daysToExpire);
	
	emscripten_run_script(script);	
}

void savePreferences() {

	// TODO: use javascript escape function to allow storing any type of preferences.	

	// Get preferences length,
	int preferencesStringLength = 1;
	int separatorsLength = strlen(":-");
	for (int i=0; i<preferences.size(); i++) {
		preferencesStringLength += strlen(preferences[i].key);
		preferencesStringLength += strlen(preferences[i].value);
	}
	preferencesStringLength += separatorsLength * preferences.size();

	// Allocate memory for the preferences string,
	char *preferencesString = (char *) alloca(preferencesStringLength);
	
	// Create the preferencesString,
	if (preferences.size()) {
		sprintf(preferencesString, "%s:%s", preferences[0].key, preferences[0].value);
	}
	
	// TODO: optimize. No need to overwrite self, just memcpy...
	for (int i=1; i<preferences.size(); i++) {	
		sprintf(preferencesString, "%s-%s:%s", preferencesString, preferences[i].key, preferences[i].value);
	}

	setCookie("preferences", preferencesString, 1000);

	/*
	setCookie("sdsf", "dsfsf", 10);

	// Get cookies script length,
	int scriptLength = 1 + strlen("document.cookie='preferences=;';");
	int separatorsLength = strlen(":-");
	for (int i=0; i<preferences.size(); i++) {
		scriptLength += strlen(preferences[i].key);
		scriptLength += strlen(preferences[i].value);
	}
	scriptLength += separatorsLength * preferences.size();

	// Allocate memory for the script,
	char *script = (char *) malloc(scriptLength);
	
	// Create the script,
	if (preferences.size()) {
		sprintf(script, "document.cookie='preferences=%s:%s", preferences[0].key, preferences[0].value);
	}
	
	// TODO: optimize. No need to overwrite self, just memcpy...
	for (int i=1; i<preferences.size(); i++) {	
		sprintf(script, "%s-%s:%s", script, preferences[i].key, preferences[i].value);
	}

	sprintf(script, "%s;';", script);
	
	// Run script,
	emscripten_run_script(script);	
	
	// Remove memory allocated for script,
	free(script);
	*/
}

void loadPreferences() {

	// Get cookie size,
	int cookieSize=0;
	char getCookieSizeScriptFormat[] =
		"var ptr = %d;"
		"var str = document.cookie;"
		"setValue(ptr, str.length, 'i32');";
	char getCookieSizeScript[120];
	sprintf(getCookieSizeScript, getCookieSizeScriptFormat, (int) &cookieSize);
	emscripten_run_script(getCookieSizeScript);	
		
	// Allocate space for cookie,
	char *cookie = (char *) malloc(cookieSize + 1);	
	cookie[cookieSize] = 0;

	// Get cookie,
	char getCookieScriptFormat[] = 
		"var ptr = %d;"
		"var str = document.cookie;"
		"for (var i=0, strLen=str.length; i<strLen; i++) {"
		"	setValue(ptr+i, str.charCodeAt(i), 'i8');"
		"}";
			
	char getCookieScript[200];
	sprintf(getCookieScript, getCookieScriptFormat, (int) cookie);
	emscripten_run_script(getCookieScript);	
	
	// Parse cookie,
	char *currentChar = cookie;

	// TODO: make preferences key unique to application... (declare a required variable in javaInterface.h)	
	// Find preferences cookie,
	while ((*currentChar) && (!testSignature(currentChar, "preferences="))) {
		currentChar ++;
	}
	
	// If end encountered without finding preferences, just break,
	if (!(*currentChar)) {
		LOGE("Couldn't find preferences cookie.");
		return ;
	}

	// Skip signature,
	currentChar += strlen("preferences=");

	// Parse pairs,
	std::vector<char> text;
	do {
	
		// Get key,
		while ((*currentChar) && (*currentChar != ':')) {
			text.push_back(*currentChar);
			currentChar++;
		}
		
		if (!(*currentChar)) {
			LOGE("Malformed preferences cookie.");
			return ;
		}

		// Add preference,
		preferences.push_back(Preference());
		Preference &preference = preferences.back();

		// Set key,
		preference.key = (char *) malloc(text.size() + 1);
		memcpy(preference.key, &text[0], text.size());
		preference.key[text.size()] = 0;
		
		// Get value,
		currentChar++;
		text.clear();
		while ((*currentChar) && (*currentChar != '-') && (*currentChar != ';')) {
			text.push_back(*currentChar);
			currentChar++;
		}
		
		// Set value,
		preference.value = (char *) malloc(text.size() + 1);
		memcpy(preference.value, &text[0], text.size());
		preference.value[text.size()] = 0;

		text.clear();
		if (*currentChar == '-') currentChar++;
	} while ((*currentChar) && !(*currentChar == ';'));

	// Remove memory allocated for cookie,
	free(cookie);
}

void addPreference(const char *key, const char *value, int valueSizeBytes=0) {

	// Default to zero terminated string,
	if (!valueSizeBytes) {
		valueSizeBytes = strlen(value) + 1;
	}
	
	// Update the preference if key is existing,
	for (int i=0; i<preferences.size(); i++) {
		if (strcmp(preferences[i].key, key) == 0) {
			free(preferences[i].value);
			preferences[i].value = (char *) malloc(valueSizeBytes);
			memcpy(preferences[i].value, value, valueSizeBytes);
			preferences[i].valueSize = valueSizeBytes;
			savePreferences();
			return;
		}
	}
	
	// Add a new preference,
	preferences.push_back(Preference());
	Preference &newPreference = preferences.back();
	
	newPreference.key = (char *) malloc(strlen(key)+1);
	strcpy(newPreference.key, key);
	
	newPreference.value = (char *) malloc(valueSizeBytes);
	memcpy(newPreference.value, value, valueSizeBytes);
	newPreference.valueSize = valueSizeBytes;
	
	savePreferences();
}

const char *getPreference(const char *key) {

	for (int i=0; i<preferences.size(); i++) {
		if (strcmp(preferences[i].key, key) == 0) {
			return preferences[i].value;
		}
	}
	
	return 0;
}

void javaSetBooleanPreference(const char *key, bool value) {
	
	if (value) {
		addPreference(key, "true");
	} else {
		addPreference(key, "false");
	}	
}

bool javaGetBooleanPreference(const char *key, bool defaultValue) { 

	const char *valueString = getPreference(key);
	if (valueString) {
		if (strcmp(valueString, "true") == 0) {
			return true;
		} else {
			return false;
		}
	}

	return defaultValue;
}

void javaSetIntPreference(const char *key, int value) {

	char valueString[12];
	sprintf(valueString, "%d", value);
	
	addPreference(key, valueString);
}

int javaGetIntPreference(const char *key, int defaultValue) { 

	const char *valueString = getPreference(key);

	if (valueString) {
		int value;
		sscanf(valueString, "%d", &value);
		return value;
	}
	
	return defaultValue; 
}

/////////////////////////////////////
// Music
/////////////////////////////////////

std::vector<Mix_Music *> musics;

int javaLoadMusicFromAssets(const char *filename) { 

    TextBuffer completePath;
    completePath.append("assets/").append(filename).append(".ogg");

	Mix_Music *music = Mix_LoadMUS(completePath.getText());
	if (!music) {
	 	LOGE("Mix_LoadMUS: %s, %s\n", completePath.getText(), Mix_GetError());
	 	return -1;
	}
	
	musics.push_back(music);

	return musics.size()-1; 
}

void javaDeleteMusic(int musicIndex) {
	Mix_FreeMusic(musics[musicIndex]);
	musics[musicIndex] = 0;
}

void javaPlayMusic(int musicIndex) {
	if (Mix_PlayMusic(musics[musicIndex], 1) == -1) {
	 	LOGE("Mix_PlayMusic: %s\n", Mix_GetError());
 	}
}

void javaMusicSetLooping(int musicIndex, bool looping) {

	int loops = (looping) ? -1 : 1;
	if (Mix_PlayMusic(musics[musicIndex], loops) == -1) {
	 	LOGE("Mix_PlayMusic (looping): %s\n", Mix_GetError());
 	}
}

void javaStopMusic(int musicIndex) {
	Mix_HaltMusic();
}

void javaPauseMusic(int musicIndex) {
	Mix_PauseMusic();
}

void javaResumeMusic(int musicIndex) {
	Mix_ResumeMusic();
}

void javaFadeOutMusic(int musicIndex, int milliSeconds) {
	Mix_FadeOutMusic(milliSeconds);
}

/////////////////////////////////////
// Sound
/////////////////////////////////////

bool muteSounds = false;
std::vector<Mix_Chunk *> sounds;
int leastRecentlyUsedChannel = 0; 
int javaLoadSoundFromAssets(const char *filename) { 

    TextBuffer completePath;
    completePath.append("assets/").append(filename).append(".ogg");
    
	Mix_Chunk *sound = Mix_LoadWAV(completePath.getText());
	if (!sound) {
	 	LOGE("Mix_LoadWAV: %s, %s\n", completePath.getText(), Mix_GetError());
	 	return -1;
	}
	
	sounds.push_back(sound);
	
	return sounds.size()-1; 
}

void javaDeleteSound(int soundIndex) {
	Mix_FreeChunk(sounds[soundIndex]);
	sounds[soundIndex] = 0;
}

void javaPlaySound(int soundIndex, float rate, float leftVolume, float rightVolume) {

	if (muteSounds) return ;
	
	int channel = Mix_PlayChannel(-1, sounds[soundIndex], 0);
    if (channel == -1) {    
	    Mix_HaltChannel(leastRecentlyUsedChannel);
	    channel = Mix_PlayChannel(leastRecentlyUsedChannel, sounds[soundIndex], 0);
    }
    
    if (channel == -1) {    
	 	LOGE("Mix_PlayChannel: %s\n", Mix_GetError());
 	} else {
	 	Mix_SetPanning(channel, leftVolume*255, rightVolume*255);
 	}
 	
 	leastRecentlyUsedChannel = (leastRecentlyUsedChannel + 1) % MAX_CHANNELS_COUNT;
}

void javaStopSound(int soundIndex) {
	LOGE("incomplete function 20 called");
}

void javaPauseSound(int soundIndex) {
	LOGE("incomplete function 21 called");
}

void javaResumeSound(int soundIndex) {
	LOGE("incomplete function 22 called");
}

void javaSoundSetLooping(int soundIndex, bool looping) {
	LOGE("incomplete function 23 called");
}

void javaMuteSounds(bool mute) {
	muteSounds = mute;
}

/////////////////////////////////////
// Sockets
/////////////////////////////////////

int javaCreateSocket(const char *address, int port) { 
	LOGE("incomplete function 25 called");
	return 0; 
}

void javaCloseSocket(int socketId) {
	LOGE("incomplete function 26 called");
}

void javaSocketWrite(int socketId, const char *bytes, int sizeBytes) {
	LOGE("incomplete function 27 called");
}

void *javaSocketRead(int socketIndex) { 
	LOGE("incomplete function 28 called");
	return 0; 
}

/////////////////////////////////////
// Misc
/////////////////////////////////////

bool javaUsesCoverageAa() { return false; }

void javaLongToast(const char *toastText) {
	LOGE("incomplete function 4 called");
}

void javaShortToast(const char *toastText) {
	LOGE("incomplete function 5 called");
}

void javaBrowseUrl(const char *url, const char *alternativeUrl) {
    
	// Use alternative link for unsupported protocols,
	if (testSignature(url, "market://")) {
		url = alternativeUrl;
	}
	
	// Get script length,
	int scriptLength = 1 + strlen("window.open('');");
	scriptLength += strlen(url);
    
	// Allocate memory for the script,
	char *script = (char *) alloca(scriptLength);
    
	// Make the script,
	sprintf(script, "window.open('%s');", url);
	
	// Run script,
	emscripten_run_script(script);
}

float javaGetDpi() {
	return dpi;
}

float javaGetTimeMillis() { 
	LOGE("incomplete function 29 called");
	return 1; 
}

void javaFinishActivity() {
	//Nongl::exit();
}

void javaPreventSleep(bool prevent) {}

void javaOpenOptionsMenu() {
	LOGE("incomplete function 31 called");
}

void javaExit() {
	emscripten_cancel_main_loop();
}
