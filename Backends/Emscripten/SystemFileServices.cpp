
#include "../../SystemFileServices.h"
#include "../../TextUtils.h"
#include "../../File.h"

#include <cstdio>
#include <sys/stat.h>
#include <stdexcept>

static TextBuffer getFileFullPath(const Ngl::File &file) {

	// Find the file final location,
	TextBuffer fileFullPath;

	Ngl::FileLocation fileLocation = file.getLocation();
	switch (fileLocation) {
		case Ngl::FileLocation::ASSETS: fileFullPath.append("assets/"); break;
		case Ngl::FileLocation::SDCARD: fileFullPath.append("sdcard/"); break;
		default:
			throw std::runtime_error(
					TextBuffer("File location not supported (only assets or sdcard): ").
					append(file.getFullPath()).getConstText());
	}

	fileFullPath.append(file.getFullPath());
	return fileFullPath;
}

static bool fileExists(const char *filename) {
   	struct stat st;
    return stat(filename, &st) == 0;
}

bool Ngl::systemFileExists(const Ngl::File &file) {
	return fileExists(getFileFullPath(file));
}

int32_t Ngl::systemGetFileSize(const Ngl::File &file) {

	// Find the file final location,
	TextBuffer fileFullPath = getFileFullPath(file);

	// Get file size,
	struct stat fileState;
	if (stat(fileFullPath, &fileState) != 0) {
		throw std::runtime_error(TextBuffer("Couldn't get file size (doesn't exist): ").append(fileFullPath).getConstText());
	}
	return (int32_t) fileState.st_size;
}

void Ngl::systemMakeDir(const Ngl::File &file) {

    // Writing to assets not supported (for consistency with other platforms),
    if (file.getLocation() == Ngl::FileLocation::ASSETS) {
        throw std::runtime_error(TextBuffer("Can't make directories in assets: ").append(file.getFullPath()).getConstText());
    }

    // Find the file final location,
    TextBuffer directoryFullPath = getFileFullPath(file);
    if (mkdir(directoryFullPath, 0777)) {
        throw std::runtime_error(TextBuffer("Couldn't make directory: ").append(directoryFullPath).getConstText());
    }
}

int32_t Ngl::systemReadFile(const Ngl::File &file, std::vector<uint8_t> &outputVector, int32_t offsetInVector, int32_t offsetInFile, int32_t maxReadSize) {

    // Find the file final location,
    TextBuffer fileFullPath = getFileFullPath(file);

    // Get file size to prepare the output vector,
    struct stat fileState;
    if (stat(fileFullPath, &fileState) != 0) {
        throw std::runtime_error(TextBuffer("Couldn't read file (doesn't exist): ").append(fileFullPath).getConstText());
    }
    int32_t fileSize = fileState.st_size;

    // Force upper limit of read size,
    int32_t readSize = fileSize - offsetInFile;
    if (maxReadSize && (readSize > maxReadSize)) readSize = maxReadSize;

    // Open the file,
    FILE *fileHandle = fopen(fileFullPath, "rb");
    if (!fileHandle) {
        throw std::runtime_error(TextBuffer("Couldn't open file for reading: ").append(fileFullPath).getConstText());
    }

    // Forward to the offset,
    if (offsetInFile) {
        if (fseek (fileHandle, offsetInFile, SEEK_SET)) {
            throw std::runtime_error(TextBuffer("Couldn't seek in file for reading: ").append(fileFullPath).getConstText());
        }
    }

    // Prepare the output vector to hold the file data,
    int32_t readBytesCount;
    if (offsetInVector == outputVector.size()) {

        // Adjust the original vector to hold the new data,
        int32_t originalVectorSize = outputVector.size();
        outputVector.resize(originalVectorSize + readSize);

        // Read the data into the output vector directly,
        readBytesCount = fread(&outputVector[originalVectorSize], 1, readSize, fileHandle);

        // Fit the vector to the data,
        int32_t unreadBytesCount = readSize - readBytesCount;
        if (unreadBytesCount) outputVector.resize(originalVectorSize + readBytesCount);

    } else {

        // Read into a temporary vector first,
        std::vector<uint8_t> tempVector;
        tempVector.resize(readSize);

        // Read the data into the temporary vector,
        readBytesCount = fread(&tempVector[0], 1, readSize, fileHandle);

        // Fit the vector to the data,
        int32_t unreadBytesCount = readSize - readBytesCount;
        if (unreadBytesCount) tempVector.resize(readBytesCount);

        // Insert temp vector into the output vector,
        outputVector.insert(outputVector.begin()+offsetInVector, tempVector.begin(), tempVector.end());
    }

    // Close the file,
    fclose(fileHandle);

    return readBytesCount;
}

int32_t Ngl::systemWriteFile(const Ngl::File &file, const void *data, int32_t sizeBytes, bool append) {

	// Writing to assets not supported (for consistency with other platforms),
	if (file.getLocation() == Ngl::FileLocation::ASSETS) {
		throw std::runtime_error(TextBuffer("Can't write files to assets: ").append(file.getFullPath()).getConstText());
	}

	// Find the file final location,
	TextBuffer fileFullPath = getFileFullPath(file);

	// Open file,
	FILE *fileHandle;
	if (append) {
		fileHandle = fopen(fileFullPath, "ab");
		if (!fileHandle) {
			throw std::runtime_error(TextBuffer("Couldn't open file for appending: ").append(fileFullPath).getConstText());
		}
	} else {
		fileHandle = fopen(fileFullPath, "wb");
		if (!fileHandle) {
			throw std::runtime_error(TextBuffer("Couldn't open file for writing: ").append(fileFullPath).getConstText());
		}
	}

	// Write data to the file,
	int32_t writtenBytesCount = fwrite(data, 1, sizeBytes, fileHandle);
	fclose(fileHandle);

	return writtenBytesCount;
}
