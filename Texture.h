#pragma once

#include "ManagedObject.h"
#include "TextUtils.h"

class DisplayManager;

namespace Ngl {

	class InputStream;
	class TextureRegion;

	enum class TextureFormat {RAW, PNG, KTX, JET, PVR, XMF};

	class Texture {

		friend class ::DisplayManager;

		TextBuffer name;
		ManagedPointer<InputStream> source;
		TextureFormat sourceFormat;

		int width=0, height=0;

		bool loaded=false;
		int lastUsed;
		unsigned int id;

		int minFilter, magFilter;

		Texture *alphaTexture;

		bool releasable;

		// So no-one can instance this except DisplayManager,
		Texture();
		~Texture();
	public:

		inline int getWidth() { return width; }
		inline int getHeight() { return height; }

		TextBuffer getName() { return name; }

		void setTextureRegion(int x, int y, int width, int height, const char *data);

		TextureRegion *createTextureRegion(const TextBuffer &regionName);

		// Note: effects don't take place until the texture is reloaded,
		inline void setTextureFilter(int minFilter, int magFilter) {
			this->minFilter = minFilter;
			this->magFilter = magFilter;
		}

		inline Texture *getAlphaTexture() { return this->alphaTexture; }

		inline unsigned int getId() { return id; }
	};
}
