#pragma once

#include "OutputStream.h"
#include "ManagedObject.h"

#include <vector>
#include <cstdint>

namespace Ngl {

	class BufferedOutputStream : public OutputStream {
		ManagedPointer<OutputStream> outputStream;
		std::vector<uint8_t> buffer;
		int32_t maxSizeBytes;
	public:

		BufferedOutputStream(ManagedPointer<OutputStream> outputStream, int32_t maxSizeBytes=0);
		~BufferedOutputStream();

		void writeData(const void *data, int32_t sizeBytes);
		int32_t writeData(InputStream &inputStream);

		void flush();
	};
}
