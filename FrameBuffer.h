#pragma once

namespace Ngl {
	class Texture;
}

class FrameBuffer {

	friend class Nongl; // To be able to call constructor to make the default framebuffer.

	unsigned int id=0;
	unsigned int depthBufferId=0;

	int width=0, height=0;

	Ngl::Texture *texture=0;
	bool depthBufferCreated=false;

	FrameBuffer();

	void create(Ngl::Texture *texture, int width=0, int height=0, bool createDepthBuffer=false);

public:

	inline FrameBuffer(Ngl::Texture *texture, bool createDepthBuffer) { create(texture, 0, 0, createDepthBuffer); }
	inline FrameBuffer(int width, int height, bool createDepthBuffer) { create(0, width, height, createDepthBuffer); }

	~FrameBuffer();

	inline int getId() { return id; }
	inline bool hasDepthBuffer() { return depthBufferCreated; }
	inline int getDepthBufferId() { return depthBufferId; }

	inline int getWidth() { return width; }
	inline int getHeight() { return height; }
};
