#pragma once

#if __APPLE__
#include "TargetConditionals.h"
#endif

#if __ANDROID__
	#define TARGET_OS_ANDROID 1
#endif

#if __linux__
	#define TARGET_OS_LINUX 1
#endif

