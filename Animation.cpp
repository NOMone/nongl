#include "Animation.h"
#include "Threads.h"

///////////////////////////////
// Animation component
///////////////////////////////

AnimationComponent::AnimationComponent(AnimationTarget::Target target, const Interpolator &interpolator, float initialValueFloat, int initialValueInt, bool initialValueRelative, float finalValueFloat, int finalValueInt, bool finalValueRelative) {

	this->target = target;
	this->interpolator = interpolator.clone();
	this->initialValueFloat = this->initialValueFloatBackup = initialValueFloat;
	this->initialValueInt = this->initialValueIntBackup = initialValueInt;
	this->initialValueRelative = initialValueRelative;
	this->finalValueFloat = this->finalValueFloatBackup = finalValueFloat;
	this->finalValueInt = this->finalValueIntBackup = finalValueInt;
	this->finalValueRelative = finalValueRelative;

	this->targetAddress = 0;
	setTargetAddress(&dummyTarget, false);
}

AnimationComponent::~AnimationComponent() {
	delete interpolator;
}

void AnimationComponent::setTargetAddress(float *targetAddress) {
	this->targetAddress = targetAddress;
	targetType = FLOAT_TARGET;
}

void AnimationComponent::setTargetAddress(int *targetAddress, bool isColor) {
	this->targetAddress = targetAddress;
	if (isColor) {
		targetType = COLOR_TARGET;
	} else {
		targetType = INT_TARGET;
	}
}

void AnimationComponent::setTargetAddress(char *targetAddress) {
	this->targetAddress = targetAddress;
	targetType = CHAR_TARGET;
}

///////////////////////////////
// Animation
///////////////////////////////

Animation::~Animation() {
	removeAllComponents();
}

Animation *Animation::clone() const {

	Animation *newAnimation = new Animation();

	newAnimation->started = started;
	newAnimation->ended = ended;
	newAnimation->onAnimationEndTask = onAnimationEndTask;
	newAnimation->onAnimationEndTaskData = onAnimationEndTaskData;
	newAnimation->animationListener = animationListener;
	if (onUpdatedFlagTarget == &dummyOnUpdatedFlagTarget) {
		newAnimation->onUpdatedFlagTarget = &newAnimation->dummyOnUpdatedFlagTarget;
	} else {
		newAnimation->onUpdatedFlagTarget = onUpdatedFlagTarget;
	}

	std::vector<AnimationComponent *>::const_iterator iterator = components.begin();
	std::vector<AnimationComponent *>::const_iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		const AnimationComponent *component = (*iterator);
		AnimationComponent *newComponent = new AnimationComponent(component->target, *component->interpolator, component->initialValueFloat, component->initialValueInt, component->initialValueRelative, component->finalValueFloat, component->finalValueInt, component->finalValueRelative);
		newAnimation->components.push_back(newComponent);
	}

	return newAnimation;
}

void Animation::addComponent(AnimationTarget::Target target, const Interpolator &interpolator, float initialValueFloat, int initialValueInt, bool initialValueRelative, float finalValueFloat, int finalValueInt, bool finalValueRelative) {
	AnimationComponent *component = new AnimationComponent(target, interpolator, initialValueFloat, initialValueInt, initialValueRelative, finalValueFloat, finalValueInt, finalValueRelative);
	components.push_back(component);
}

void Animation::removeAllComponents() {

	std::vector<AnimationComponent *>::const_iterator iterator = components.begin();
	std::vector<AnimationComponent *>::const_iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		delete (*iterator);
	}
	components.clear();
}

void Animation::removeAllComponentsOfTarget(AnimationTarget::Target target) {

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	while (iterator!=components.end()) {
		if ((*iterator)->target == target) {
			delete *iterator;
			iterator = components.erase(iterator);
		} else {
			iterator++;
		}
	}
}

bool Animation::update(float elapsedTime) {

	if (!started) return false;
	if (ended) return true;

	bool animationFinished = true;

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();
	if (animationListener) {
		for (; iterator!=endIterator; ++iterator) {
			animationFinished &= (*iterator)->update(elapsedTime);
			if (!(*iterator)->isDelayed()) animationListener->onAnimationComponentApplied((*iterator)->target);
		}
	} else {
		for (; iterator!=endIterator; ++iterator) {
			animationFinished &= (*iterator)->update(elapsedTime);
		}
	}

	// Repeat if finished,
	if (repeat && animationFinished) {
		restart();
		animationFinished = false;
	}

	if (!animationFinished) {
		*onUpdatedFlagTarget = true;
	} else if (!ended) {
		ended = true;
		*onUpdatedFlagTarget = true;
		if ((!onAnimationEndTask.expired()) && onAnimationEndTask->run(onAnimationEndTaskData)) {
			onAnimationEndTask.reset();
		}
	}

	return animationFinished;
}

void Animation::reset() {
	started = false;
	ended = false;

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->reset();
	}
}

void Animation::start() {
	started = true;

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->start();
	}

	// Apply the animation initial values,
	update(0);
}

void Animation::restart() {
	started = true;
	ended = false;

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->restart();
	}

	// Apply the animation initial values,
	update(0);
}

void Animation::pause() {
	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();
	for (; iterator!=endIterator; ++iterator) {
		(*iterator)->pause();
	}
}

bool Animation::hasFinished() {
	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();

	for (; iterator!=endIterator; ++iterator) {
		if (!(*iterator)->hasFinished()) return false;
	}

	return true;
}

float Animation::getTotalDuration() {

	std::vector<AnimationComponent *>::iterator iterator = components.begin();
	std::vector<AnimationComponent *>::iterator endIterator = components.end();

	float totalDuration = 0;
	for (; iterator!=endIterator; ++iterator) {
		float currentComponentDuration = (*iterator)->getTotalDuration();
		if (currentComponentDuration > totalDuration) totalDuration = currentComponentDuration;
	}

	return totalDuration;
}

///////////////////////////////
// Animation controller
///////////////////////////////

AnimationController::~AnimationController() {

	if (deletesAnimationsOnDestruction) {
		std::list<Animation *>::iterator iterator = animations.begin();
		std::list<Animation *>::iterator endIterator = animations.end();
		for (; iterator!=endIterator; ) {
			delete *iterator;
			iterator = animations.erase(iterator);
		}
	}
}

void AnimationController::addAnimation(Animation *animation) {
	animations.push_back(animation);
	animations.unique();
}

void AnimationController::markAnimationsAsGarbage(const void *targetObject) {

	// Search animations,
	std::list<Animation *>::iterator iterator = animations.begin();
	std::list<Animation *>::iterator endIterator = animations.end();
	while (iterator!=endIterator) {
		Animation *currentAnimation = (*iterator);
		if (currentAnimation->targetObject == targetObject) currentAnimation->garbage = true;
		iterator++;
	}
}

void AnimationController::getAnimations(const void *targetObject, std::vector<Animation *> &outAnimations) {

	// Search animations,
	std::list<Animation *>::iterator iterator = animations.begin();
	std::list<Animation *>::iterator endIterator = animations.end();
	for (; iterator!=endIterator; ) {
		Animation *currentAnimation = (*iterator);
		if (currentAnimation->targetObject == targetObject) outAnimations.push_back(currentAnimation);
		iterator++;
	}
}

void AnimationController::deleteAllAnimations() {

	// Update animations,
	std::list<Animation *>::iterator iterator = animations.begin();
	std::list<Animation *>::iterator endIterator = animations.end();
	for (; iterator!=endIterator; ) {
		Animation *currentAnimation = (*iterator);
		delete currentAnimation;
		iterator++;
	}

	animations.clear();
}

void AnimationController::update(float elapsedTime) {

	// Update animations,
	std::list<Animation *>::iterator iterator = animations.begin();
	std::list<Animation *>::iterator endIterator = animations.end();
	for (; iterator!=endIterator; ) {

		Animation *currentAnimation = (*iterator);
		if (!currentAnimation->garbage) currentAnimation->update(elapsedTime);

		++iterator;
	}

	// Remove finished and garbage animations,
	iterator = animations.begin();
	for (; iterator!=endIterator; ) {

		Animation *currentAnimation = (*iterator);

		// Remove finished animations,
		if (currentAnimation->garbage || currentAnimation->hasFinished()) {
			if (deletesFinishedAnimations) delete currentAnimation;
			iterator = animations.erase(iterator);
		} else {
			++iterator;
		}
	}
}

