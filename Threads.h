#pragma once

#include <functional>

class Activity;

class Runnable {

public:

	Activity *activity;

	inline Runnable(Activity *activity=0) { this->activity = activity; }
	virtual inline ~Runnable() { }

	virtual bool run(void *data=0) = 0;	// Returns true if the task should be removed from scheduler.

    static Runnable *createRunnable(std::function<bool(void *data)> runnable, Activity *activity=0);
};

class RunnableWrapper : public Runnable {
	Runnable *runnable;
public:
	inline RunnableWrapper(Runnable *runnable) : Runnable(runnable->activity), runnable(runnable) {}
	virtual inline bool run(void *data) {
		return runnable->run(data);
	}
};

class RunnableFunction : public Runnable {
    std::function<bool(void *data)> function;
public:
    RunnableFunction() {}
    RunnableFunction(std::function<bool(void *data)> function) : Runnable(), function(function) {}
    std::function<bool(void *data)> getFunction() { return function; }
    void setFunction(std::function<bool(void *data)> function) { this->function = function; }
    bool run(void *data) {
        return function(data);
    }
};
