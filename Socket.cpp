#include "Socket.h"
#include "SystemInterface.h"

Socket::Socket(const char *address, int port) {
	socketId = javaCreateSocket(address, port);
}

Socket::~Socket() {
	javaCloseSocket(socketId);
}

void *Socket::read() {
	return javaSocketRead(socketId);
}

void Socket::write(const void *bytes, int sizeBytes) {
	javaSocketWrite(socketId, (const char *) bytes, sizeBytes);
}
