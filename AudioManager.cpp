#include "AudioManager.h"
#include "Audio.h"
#include "SystemLog.h"

#include <string.h>

AudioManager::~AudioManager() {
	for (int i=0; i<audios.size(); i++) {
		delete audios[i];
	}
}

int32_t AudioManager::getAudioIndex(const char *name, int *insertIndex) {

	// Binary search using the name,
	int32_t lowBoundaryIndex = 0;
	int32_t highBoundaryIndex = (int32_t) audios.size() - 1;
	int32_t currentIndex;
	while (lowBoundaryIndex <= highBoundaryIndex) {

		currentIndex = (lowBoundaryIndex + highBoundaryIndex) >> 1;

		int comparisonResult = strcmp(name, audios[currentIndex]->getName());

		if (!comparisonResult) {
			if (insertIndex) *insertIndex = currentIndex+1;
			return currentIndex;
		}

		if (comparisonResult < 0) {
			// Search below,
			highBoundaryIndex = currentIndex - 1;
		} else {
			// Search above,
			lowBoundaryIndex = currentIndex + 1;
		}
	}

	// Location in which this should have been found,
	if (insertIndex) *insertIndex = lowBoundaryIndex;

	// Not found,
	return -1;
}

Audio *AudioManager::getAudio(const char *name, bool verbose) {

	int audioIndex = getAudioIndex(name);
	if (audioIndex != -1) return audios[audioIndex];

	if (verbose) LOGE("Audio %s not found", name);
	return 0;
}

void AudioManager::addAudio(Audio *audio) {
	int audioIndex;
	getAudioIndex(audio->getName(), &audioIndex);
	audios.insert(audios.begin() + audioIndex, audio);
}

Music *AudioManager::addMusic(const char *filename, const char *musicName) {
	Music *newMusic = new Music(filename, musicName);
	addAudio(newMusic);
	return newMusic;
}

Sound *AudioManager::addSound(const char *filename, const char *soundName) {
	Sound *newSound = new Sound(filename, soundName);
	addAudio(newSound);
	return newSound;
}
