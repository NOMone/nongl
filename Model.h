#pragma once

#include <vector>
#include <cstdint>

class SpriteBatch;

namespace Ngl {

	class File;

	class MeshBase;
	class MeshBatch;
	class Material;

	class Model {
		std::vector<MeshBase *> meshes;

	public:

		~Model();
		void loadFromFile(const File &file, const Material &materialToClone);

		void draw(MeshBatch &meshBatch);
		void drawWireFrame(SpriteBatch &spriteBatch, int32_t color);
	};
}
