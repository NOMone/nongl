#include "TextUtils.h"
#include "BufferedInputStream.h"

#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

////////////////////////////////////////////////
// Text buffer
////////////////////////////////////////////////

TextBuffer::TextBuffer(const TextBuffer &source, int32_t maxLength) {
	int32_t sourceLength = source.getTextLength();
	int32_t finalLength = (maxLength < sourceLength) ? maxLength : sourceLength;
	text.reserve(finalLength+1);
	text.push_back(0);
	append((const char *) &source.text[0], finalLength);
}

TextBuffer::TextBuffer(const TextBuffer &source, int32_t sourceStartIndex, int32_t maxLength) {
	int32_t sourceLength = source.getTextLength() - sourceStartIndex;
	int32_t finalLength = (maxLength < sourceLength) ? maxLength : sourceLength;
	text.reserve(finalLength+1);
	text.push_back(0);
	append((const char *) &source.text[sourceStartIndex], finalLength);
}

TextBuffer::TextBuffer(Ngl::InputStream &inputStream) {
	inputStream.readAllData(text);
	if (text[text.size()-1] != 0) append((char) 0);
}

bool TextBuffer::operator==(const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide.getConstText()) == 0;
}

bool TextBuffer::operator==(const char *rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) == 0;
}

bool TextBuffer::operator!=(const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide.getConstText()) != 0;
}

bool TextBuffer::operator!=(const char *rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) != 0;
}

bool TextBuffer::operator> (const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) > 0;
}

bool TextBuffer::operator>=(const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) >= 0;
}

bool TextBuffer::operator< (const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) < 0;
}

bool TextBuffer::operator<=(const TextBuffer &rightHandSide) const {
    return strcmp(getConstText(), rightHandSide) >= 0;
}

TextBuffer &TextBuffer::append(const char *string) {
    int stringLength = (int) strlen(string);
    text.insert(text.end()-1, string, string + stringLength);
	return *this;
}

TextBuffer &TextBuffer::append(const char *string, int length) {
    text.insert(text.end()-1, string, string + length);
	return *this;
}

TextBuffer &TextBuffer::append(Ngl::BufferedInputStream &bufferedInputStream, int32_t length) {
	bufferedInputStream.readBlock(text, text.size()-1, length);
	return *this;
}

inline TextBuffer &TextBuffer::append(const TextBuffer &buffer, int32_t maxLength) {
	int32_t bufferLength = buffer.getTextLength();
	append((const char *) &buffer.text[0],
		   (maxLength < bufferLength) ? maxLength : bufferLength);
	return *this;
}

TextBuffer &TextBuffer::append(int number) {
	char numberString[12];
	sprintf(numberString, "%d", number);
	append(numberString);
	return *this;
}

TextBuffer &TextBuffer::append(float number) {
	char numberString[30];
	sprintf(numberString, "%f", number);
	append(numberString);
	return *this;
}

TextBuffer &TextBuffer::append(double number) {
	char numberString[30];
	sprintf(numberString, "%f", number);
	append(numberString);
	return *this;
}

TextBuffer &TextBuffer::prepend(const char *string) {

    int stringLength = (int) strlen(string);
    text.insert(text.begin(), string, string + stringLength);
    
	return *this;
}

TextBuffer &TextBuffer::prepend(int number) {
	char numberString[12];
	sprintf(numberString, "%d", number);
	prepend(numberString);
	return *this;
}

TextBuffer &TextBuffer::prepend(float number) {
	char numberString[30];
	sprintf(numberString, "%f", number);
	prepend(numberString);
	return *this;
}

TextBuffer &TextBuffer::removeWhiteSpaces() {
	::removeWhitespaces(getText());
	text.resize(strlen(getText()) + 1);
	return *this;
}

TextBuffer &TextBuffer::trimFront() {
	::trimFront(getText());
	text.resize(strlen(getText()) + 1);
	return *this;
}

TextBuffer &TextBuffer::trimEnd() {
	::trimEnd(getText());
	text.resize(strlen(getText()) + 1);
	return *this;
}

TextBuffer &TextBuffer::trim() {
	::trim(getText());
	text.resize(strlen(getText()) + 1);
	return *this;
}

TextBuffer &TextBuffer::truncate(int32_t startIndex) {
	if (startIndex >= getTextLength()) return *this;
	text.resize(startIndex+1);
	text[startIndex] = 0;
	return *this;

}

bool TextBuffer::startsWith(const TextBuffer &textToMatch) const {

	int32_t textToMatchLength = textToMatch.getTextLength();
	if (getTextLength() < textToMatchLength) return false;

	for (int32_t i=0; i<textToMatchLength; i++) {
		if (text[i] != textToMatch[i]) return false;
	}

	return true;
}

int32_t TextBuffer::getTextIndex(const TextBuffer &textToFind) const {
    const char *foundTextIndex = strstr(getConstText(), textToFind.getConstText());
    if (!foundTextIndex) return -1;
    return foundTextIndex - getConstText();
}

int32_t TextBuffer::getTextIndex(int32_t startIndex, const TextBuffer &textToFind) const {
	const char *foundTextIndex = strstr((const char *) &text[startIndex], textToFind.getConstText());
    if (!foundTextIndex) return -1;
    return foundTextIndex - getConstText();
}


////////////////////////////////////////////////
// Text parsing utilities
////////////////////////////////////////////////

bool isWhiteSpace(char character) {
	return character &&
			((character ==  ' ') ||
			 (character == '\t') ||
			 (character == '\n') ||
			 (character == '\r'));
}

int readLine(const char *src, char *outLine) {

	int i;
	for (i=0; (src[i]!=0)&&(src[i]!='\n'); i++) {
		outLine[i] = src[i];
	}

	outLine[i] = 0;

	if (src[i] == '\n') {
		return i+1;
	} else {
		return i;
	}
}

char *trimFront(char *text) {

	int srcIndex=0;
	int destIndex=0;

	while (isWhiteSpace(text[srcIndex])) {
		srcIndex++;
	}

	do {
		text[destIndex++] = text[srcIndex];
	} while (text[srcIndex++] != 0);

	return text;
}

char *trimEnd(char *text) {

	int32_t srcIndex = (int32_t) strlen(text) - 1;
	while ((srcIndex>-1) && isWhiteSpace(text[srcIndex])) {
		srcIndex--;
	}
	text[srcIndex+1] = 0;

	return text;
}

char *trim(char *text) {
	return trimFront(trimEnd(text));
}

char *removeWhitespaces(char *text) {

	int srcIndex=0;
	int destIndex=0;
	do {
		if (!isWhiteSpace(text[srcIndex])) {
			text[destIndex++] = text[srcIndex];
		}
	} while (text[srcIndex++] != 0);
	return text;
}

int testSignature(const char *text, const char *signature) {

	int32_t signatureLength = (int32_t) strlen(signature);
	int32_t textLength = (int32_t) strlen(text);

	if (textLength < signatureLength) return 0;

	for (int i=0; i<signatureLength; i++) {
		if (signature[i] != text[i]) return 0;
	}

	return 1;
}

bool parseLine(const char *lineText, const char *signature, const char *format, ...) {

	if (!testSignature(lineText, signature))
		return false;

	va_list arguments;
	va_start (arguments, format);
	vsscanf(lineText, format, arguments);
	va_end(arguments);

	return true;
}

int stringCompare(const char *string1, const char *string2) {

	int currentChar = 0;
	while (string1[currentChar] && string2[currentChar] && (string1[currentChar] == string2[currentChar])) currentChar++;

	return string1[currentChar] - string2[currentChar];
}
