#pragma once

#include "View.h"
#include "Quad.h"

class RotatingRaysView : public View {
	Quad rayQuad;
public:

	float radius;
    float currentAngle;
	float cyclesPerSecond;
    int trianglesCount;
	int centerColor[2];
	int tipColor[2];
    float scaleX, scaleY;

	RotatingRaysView(
                     float centerX, float centerY,
                     float radius,
                     float cyclesPerSecond,
                     int trianglesCount,
                     int centerColor1, int tipColor1,
                     int centerColor2, int tipColor2,
                     const Ngl::TextureRegion *textureRegion=0);
        
    virtual inline float getWidth() { return radius*2.0f; }
	virtual inline float getHeight() { return radius*2.0f; }
	virtual inline float getScaledWidth() { return getWidth()*rayQuad.scaleX; }
	virtual inline float getScaledHeight() { return getHeight()*rayQuad.scaleY; }
	virtual inline float getLeft() { return rayQuad.x - (radius*rayQuad.scaleX); }
	virtual inline float getBottom() { return rayQuad.y - (radius*rayQuad.scaleY); }
	virtual inline float getRight() { return rayQuad.x + (radius*rayQuad.scaleX); }
	virtual inline float getTop() { return rayQuad.y + (radius*rayQuad.scaleY); }
	virtual inline float getCenterX() { return rayQuad.x;  }
	virtual inline float getCenterY() { return rayQuad.y;  }
	virtual inline float getScaleX() { return rayQuad.scaleX; }
	virtual inline float getScaleY() { return rayQuad.scaleY; }
    
	virtual inline void setLeft(float left) { rayQuad.x = left + (radius*rayQuad.scaleX); }
	virtual inline void setBottom(float bottom) { rayQuad.y = bottom + (radius*rayQuad.scaleY); }
	virtual inline void setRight(float right) { rayQuad.x = right - (radius*rayQuad.scaleX); }
	virtual inline void setTop(float top) { rayQuad.y = top - (radius*rayQuad.scaleY); }
	virtual inline void setCenterX(float centerX) { rayQuad.x = centerX; }
	virtual inline void setCenterY(float centerY) { rayQuad.y = centerY; }
	virtual inline void setCenter(float centerX, float centerY) { rayQuad.x = centerX; rayQuad.y = centerY; }
	virtual inline void setScale(float scaleX, float scaleY) { rayQuad.scaleX = scaleX; rayQuad.scaleY = scaleY; }
	virtual inline void setColorMask(int colorMask) { rayQuad.colorMask = colorMask; }

	inline void adjustDepths(float maxZ, float minZ) { rayQuad.z = maxZ; }
    
	void draw(SpriteBatch *spriteBatch, bool drawTransparent, bool drawOpaque);
    
	inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }
    inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, true); }
    
	void update(float elapsedTimeMillis);
};

class CircleView : public RotatingRaysView {
public:
	inline CircleView(float centerX, float centerY, float radius, int color, int trianglesCount=32) :
		RotatingRaysView(centerX, centerY, radius, 0, trianglesCount, color, color, color, color) {}
};
