#include "Model.h"

#include "File.h"
#include "Vectors.h"
#include "Mesh.h"
#include "utils.h"
#include "DisplayManager.h"
using namespace Ngl;

// TODO: remove
#include "SystemLog.h"

#include <stdexcept>
#include <cstdlib>

// TODO: probably remove...
#include <cmath>

namespace {

	class XFileLoader {

		std::vector<MeshBase *> &meshes;

		File file;
		TextBuffer fileData;
		int32_t currentCharIndex=0;

		struct Face {
			uint16_t verticesIndices[3]={0};
			uint16_t materialIndex=0;
			Face(uint16_t verticesIndices[3]) : verticesIndices{verticesIndices[0], verticesIndices[1], verticesIndices[2]} {}
		};

		std::vector<shared_ptr<Material> > materials;
		std::vector<TexturedVertex> vertices;
		std::vector<Face> faces;

		int32_t readLine(int32_t startCharIndex, TextBuffer &outReadLine);
		void removeCommentsAndLineEndings();
		void skipWhiteSpaces();

		void skipChar(char expectedChar);
		char parseChar() { return fileData[currentCharIndex++]; }
		int32_t parseInteger(char endChar);
		float parseFloat(char endChar);
		TextBuffer parseString();
		TextBuffer parseQuotedString();
		Vec4 parseVec4(char separator, char lastSeparator);
		Vec3 parseVec3(char separator, char lastSeparator);
		Vec2 parseVec2(char separator, char lastSeparator);

		void discardBrackets();

		void parseMaterial(const Material &materialToClone);
		void parseMesh();

	public:
		XFileLoader(const File &file, std::vector<MeshBase *> &meshes, const Material &materialToClone);
	};

	int32_t XFileLoader::readLine(int32_t startCharIndex, TextBuffer &outReadLine) {

		char currentLineBuffer[256];

		// Read line,
		// Skip whitespaces and comments,
		int frontCharIndex = startCharIndex;
		do {
			frontCharIndex += ::readLine((char *) &fileData[frontCharIndex], currentLineBuffer);
			outReadLine.clear();
			outReadLine.append(currentLineBuffer);
			outReadLine.trim();
		} while ((outReadLine.getTextLength()==0) || outReadLine.startsWith("#") || outReadLine.startsWith("//"));

		return frontCharIndex - startCharIndex;
	}

	void XFileLoader::removeCommentsAndLineEndings() {

		// This modifies fileData directly. It removes all data before
		// currentCharIndex completely and cleans the rest.

		TextBuffer cleanedText;
		while (fileData[currentCharIndex]) {
			TextBuffer currentLine;
			currentCharIndex += readLine(currentCharIndex, currentLine);

			// Remove any "#" comments in the line,
			int32_t commentIndex = currentLine.getTextIndex("#");
			if (commentIndex != -1) currentLine.truncate(commentIndex);

			// Remove any "//" comments in the line,
			commentIndex = currentLine.getTextIndex("//");
			if (commentIndex != -1) currentLine.truncate(commentIndex);

			// Trim,
			currentLine.trim();

			cleanedText.append(currentLine);
		}

		fileData = cleanedText;
		currentCharIndex = 0;
	}

	void XFileLoader::skipWhiteSpaces() {
		while (fileData[currentCharIndex] && isWhiteSpace(fileData[currentCharIndex])) currentCharIndex++;
	}

	void XFileLoader::skipChar(char expectedChar) {
		skipWhiteSpaces();
		if (parseChar() != expectedChar) {
			throw std::runtime_error(
					TextBuffer("Couldn't load .x file (expected ").
					append(expectedChar).
					append(" not found).").getConstText());
		}
	}

	int32_t XFileLoader::parseInteger(char endChar) {
		int32_t endCharIndex = fileData.getTextIndex(currentCharIndex, endChar);
		if (endCharIndex == -1) {
			throw std::runtime_error("Couldn't load .x file (parsing int end character not found).");
		}
		int32_t value = strtol(&fileData[currentCharIndex], 0, 0);
		currentCharIndex = endCharIndex;
		return value;
	}

	float XFileLoader::parseFloat(char endChar) {
		int32_t endCharIndex = fileData.getTextIndex(currentCharIndex, endChar);
		if (endCharIndex == -1) {
			throw std::runtime_error("Couldn't load .x file (parsing float end character not found).");
		}
		float value = strtof(&fileData[currentCharIndex], 0);
		currentCharIndex = endCharIndex;
		return value;
	}

	TextBuffer XFileLoader::parseString() {
		skipWhiteSpaces();
		int endCharIndex = currentCharIndex;
		while ((fileData[endCharIndex]) &&
			   (!isWhiteSpace(fileData[endCharIndex])) &&
			   (fileData[endCharIndex] != '{') &&
			   (fileData[endCharIndex] != '}')) {
			endCharIndex++;
		}
		TextBuffer string = TextBuffer(fileData, currentCharIndex, endCharIndex-currentCharIndex);
		currentCharIndex = endCharIndex;
		return string;
	}

	TextBuffer XFileLoader::parseQuotedString() {
		skipChar('"');
		int endCharIndex = currentCharIndex;
		while ((fileData[endCharIndex]) &&
			   (fileData[endCharIndex] != '"')) {
			endCharIndex++;
		}
		TextBuffer string = TextBuffer(fileData, currentCharIndex, endCharIndex-currentCharIndex);
		currentCharIndex = endCharIndex;
		skipChar('"');
		return string;
	}

	Vec4 XFileLoader::parseVec4(char separator, char lastSeparator) {
		Vec4 value;
		value.x = parseFloat(separator); skipChar(separator);
		value.y = parseFloat(separator); skipChar(separator);
		value.z = parseFloat(separator); skipChar(separator);
		value.w = parseFloat(lastSeparator); skipChar(lastSeparator);
		return value;
	}

	Vec3 XFileLoader::parseVec3(char separator, char lastSeparator) {
		Vec3 value;
		value.x = parseFloat(separator); skipChar(separator);
		value.y = parseFloat(separator); skipChar(separator);
		value.z = parseFloat(lastSeparator); skipChar(lastSeparator);
		return value;
	}

	Vec2 XFileLoader::parseVec2(char separator, char lastSeparator) {
		Vec2 value;
		value.x = parseFloat(separator); skipChar(separator);
		value.y = parseFloat(lastSeparator); skipChar(lastSeparator);
		return value;
	}

	void XFileLoader::discardBrackets() {
		skipChar('{');
		int openBracketsCount=1;
		int scanCharIndex = currentCharIndex;
		while (openBracketsCount && fileData[scanCharIndex]) {
			if (fileData[scanCharIndex] == '{') {
				openBracketsCount++;
			} else if (fileData[scanCharIndex] == '}') {
				openBracketsCount--;
			}
			scanCharIndex++;
		}
		currentCharIndex = scanCharIndex;
	}

	void XFileLoader::parseMaterial(const Material &materialToClone) {

		// Check if there is a name for this material,
		TextBuffer materialName = parseString();
		shared_ptr<Material> material(materialToClone.clone());
		material->setName(materialName);
		materials.push_back(material);
		skipChar('{');

		// Parse ambient color,
		Vec4 ambientColor = parseVec4(';', ';');
		material->setColor(rgba(ambientColor.x*255, ambientColor.y*255, ambientColor.z*255, ambientColor.w*255));
		skipChar(';');

		// Parse specular power,
		float specularPower = parseFloat(';');
		// TODO: add remaining parameters to the abstract Material so they can be set...
		skipChar(';');

		// Parse specular color,
		Vec3 specularColor = parseVec3(';', ';');
		skipChar(';');

		// Parse emissive color,
		Vec3 emissiveColor = parseVec3(';', ';');
		skipChar(';');

		// Check for texture file,
		TextBuffer textureFileNameTag = parseString();
		TextBuffer textureFileName;
		if (!textureFileNameTag.isEmpty()) {

			// Texture file name tag,
			if (textureFileNameTag != "TextureFilename") {
				throw std::runtime_error("Couldn't load .x file (material TextureFilename tag not found).");
			}

			// Parse file name,
			skipChar('{');
			textureFileName = parseQuotedString();
			skipChar(';');
			skipChar('}');

			// TODO: should it really be this way?
			textureFileName.prepend("Models.");
			DisplayManager *displayManager = DisplayManager::getSingleton();
			material->setTexture(displayManager->getTextureRegion(textureFileName, true)->texture);
			// TODO: assign texture region to texture...
		}

		skipChar('}');
	}

	void XFileLoader::parseMesh() {

		// Check if there is a name for this mesh,
		TextBuffer meshName = parseString();
		skipChar('{');

		// Read vertices count,
		int32_t verticesCount = parseInteger(';');
		skipChar(';');

		// Read vertices,
		int32_t initialVerticesCount = vertices.size();
		vertices.reserve(initialVerticesCount + verticesCount);
		if (verticesCount) {
			int verticesCountMinusOne = verticesCount-1;
			for (int32_t i=0; i<verticesCountMinusOne; i++) {
				vertices.emplace_back(parseVec3(';', ';'));
				skipChar(',');
			}

			vertices.emplace_back(parseVec3(';', ';'));
			skipChar(';');
		}

		// Read faces count,
		int32_t facesCount = parseInteger(';');
		skipChar(';');

		// Read faces,
		int32_t initialFacesCount = faces.size();
		faces.reserve(initialFacesCount + facesCount);

		if (facesCount) {
			uint16_t faceVerticesIndices[3];
			int32_t facesCountMinusOne = facesCount-1;
			for (int32_t i=0; i<facesCountMinusOne; i++) {
				skipChar('3');
				skipChar(';');
				for (int32_t j=0; j<3; j++) {
					faceVerticesIndices[j] = initialVerticesCount + parseInteger(',');
					skipChar(',');
				}
				faces.emplace_back(faceVerticesIndices);
			}

			skipChar('3');
			skipChar(';');
			faceVerticesIndices[0] = initialVerticesCount + parseInteger(',');
			skipChar(',');
			faceVerticesIndices[1] = initialVerticesCount + parseInteger(',');
			skipChar(',');
			faceVerticesIndices[2] = initialVerticesCount + parseInteger(';');
			skipChar(';');
			faces.emplace_back(faceVerticesIndices);
			skipChar(';');
		}

		// Create materials table to assign materials to faces,
		// Read any child tokens,
		TextBuffer token = parseString();
		while (!token.isEmpty()) {

			// Check for mesh material list,
			if (token == "MeshMaterialList") {

				skipChar('{');

				// Read materials count,
				int32_t materialsCount = parseInteger(';');
				skipChar(';');

				// Read faces count,
				int32_t unneededFacesCount = parseInteger(';');
				skipChar(';');

				// Read faces materials'
				if (unneededFacesCount) {
					int32_t lastFaceIndex = initialFacesCount + unneededFacesCount - 1;
					for (int32_t i=initialFacesCount; i<lastFaceIndex; i++) {
						faces[i].materialIndex = parseInteger(',');
						skipChar(',');
					}
					faces[lastFaceIndex].materialIndex = parseInteger(';');
					skipChar(';');
				}

				// Read material names and populate the materials table,
				std::vector<uint16_t> materialsTable;
				materialsTable.resize(materialsCount);
				for (int32_t i=0; i<materialsCount; i++) {
					skipChar('{');
					TextBuffer currentMaterialName = parseString();
					skipChar('}');

					// Find the material of this name,
					for (int32_t j=0; j<materials.size(); j++) {
						if (materials[j]->getName() == currentMaterialName) {
							materialsTable[i] = j;
							break;
						}
					}
				}

				skipChar('}');

				// Turn face material indices from local indices of "materialsTable" into
				// indices of "materials" directly,
				int32_t totalFacesCount = faces.size();
				for (int32_t i=initialFacesCount; i<totalFacesCount; i++) {
					faces[i].materialIndex = materialsTable[faces[i].materialIndex];
				}

			} else if (token == "MeshTextureCoords") {

				skipChar('{');

				// Read vertices count,
				int32_t unneededVerticesCount = parseInteger(';');
				skipChar(';');

				// Read vertices texture coordinates,
				int32_t totalVerticesSize = initialVerticesCount + unneededVerticesCount;
				for (int32_t i=initialVerticesCount; i<totalVerticesSize; i++) {
					vertices[i].textureCoordinates = parseVec2(',', ';');

					// TODO: remove or fix...
					//vertices[i].textureCoordinates.x = 0.1f + 0.8f*fmod(vertices[i].textureCoordinates.x, 1);
					//vertices[i].textureCoordinates.y = 0.1f + 0.8f*fmod(vertices[i].textureCoordinates.y, 1);
				}
				skipChar(';');
				skipChar('}');

			} else {

				// No, won't parse normals or anything like it!
				LOGE("Parsing %s file: %s skipped", file.getFullPath().getConstText(), token.getConstText());
				discardBrackets();
			}

			token = parseString();
		};

		skipChar('}');

		// Mesh parsing complete.
	}

	XFileLoader::XFileLoader(const File &file, std::vector<MeshBase *> &meshes, const Material &materialToClone) : file(file), meshes(meshes) {

		// Read X format specification at Nongl documents folder.

		// Read file,
		FileInputStream fileInputStream(shared_ptr<File>(new File(file)));
		fileData = TextBuffer(fileInputStream);

		// Parse file,
		// Signature,
		TextBuffer signature;
		currentCharIndex += readLine(currentCharIndex, signature);
		if (!signature.startsWith("xof 0303txt 0032")) {
			throw std::runtime_error("Couldn't load .x file (signature not found).");
		}

		// Prepare file for parsing,
		removeCommentsAndLineEndings();

		// Start parsing,
		TextBuffer token = parseString();
		while(!token.isEmpty()) {

			if (token == "Material") {
				parseMaterial(materialToClone);
			} else if (token == "Mesh") {
				parseMesh();
			} else {

				// Unknown tag,
				LOGE("Parsing %s file: %s skipped", file.getFullPath().getConstText(), token.getConstText());

				// It might have a name, discard it,
				parseString();

				// Discard all the brackets contents,
				discardBrackets();
			}
			token = parseString();
		}

		// Parsing finished!
		// Turn mesh data into a real mesh(es),
		if (vertices.empty()) return;

		// Create a separate mesh for every material,
		int32_t facesCount = faces.size();
		for (int32_t i=0; i<materials.size(); i++) {

			std::vector<int32_t> verticesTable(vertices.size(), -1);
			int currentVerticesCount=0;

			Mesh<TexturedVertex> *currentMesh = new Mesh<TexturedVertex>();
			currentMesh->setMaterial(materials[i]);
			meshes.push_back(currentMesh);

			int32_t facesCount = faces.size();
			for (int32_t j=0; j<facesCount; j++) {

				if (faces[j].materialIndex == i) {
					// For every vertex in the face,
					// If the vertex was never referenced in this mesh before, add
					// it to the mesh vertices,
					int16_t faceVerticesIndices[3];
					for (int32_t k=0; k<3; k++) {
						int16_t currentVertexIndex = faces[j].verticesIndices[k];
						if (verticesTable[currentVertexIndex] == -1) {
							currentMesh->addVertex(vertices[currentVertexIndex]);
							verticesTable[currentVertexIndex] = currentVerticesCount++;
						}

						faceVerticesIndices[k] = verticesTable[currentVertexIndex];
					}

					// TODO: remove...
					/*
					// Flip faces?
					if (0)
					{
						uint16_t temp = faceVerticesIndices[0];
						faceVerticesIndices[0] = faceVerticesIndices[2];
						faceVerticesIndices[2] = temp;
					}
					*/

					currentMesh->addFace(faceVerticesIndices);
				}
			}
		}
	}
}

Model::~Model() {
	for (int32_t i=0; i<meshes.size(); i++) delete meshes[i];
}

void Model::loadFromFile(const File &file, const Material &materialToClone) {
	XFileLoader xFileLoader(file, meshes, materialToClone);
}

void Model::draw(MeshBatch &meshBatch) {
	for (int32_t i=0; i<(int32_t) meshes.size(); i++) meshes[i]->draw(meshBatch);
}

void Model::drawWireFrame(SpriteBatch &spriteBatch, int32_t color) {
	for (int32_t i=0; i<(int32_t) meshes.size(); i++) meshes[i]->drawWireFrame(spriteBatch, color);
}
