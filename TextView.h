#pragma once

#include "View.h"
#include "TextSprite.h"
#include "Gravity.h"

class TextView : public View {

	int textLength;
	std::vector<char> text;

	TextSprite textSprite;

	/** These 3 methods return lines count. */
	int drawText(float topY, SpriteBatch *spriteBatch, bool draw=true);
	int breakTextLinePreservingWordsAndGetLinesCount(char *text);
	int breakTextLinePreservingWordsAndDraw(char *text, float topY, SpriteBatch *spriteBatch);

	VerticalGravity::Value verticalGravity;
	HorizontalGravity::Value horizontalGravity;

public:

	float x, y, z;
	float width, height;
	float scaleX, scaleY;
	float textScaleX, textScaleY;

	int textColor;
	int colorMask;

	TextView();
	TextView(float x, float y, float width, float height, Gravity::Value gravity = Gravity::BOTTOM_LEFT, int textColor=0xffffffff);
	TextView(const char *text, float maxWidth, float textScale=1.0f, Gravity::Value gravity=Gravity::CENTER); // Wraps content.
	TextView *clone();

	void wrapText(float maxWidth);

	void set(float x, float y, float width, float height, Gravity::Value gravity = Gravity::BOTTOM_LEFT, int textColor=0xffffffff);

	void setText(const char *text);
	inline void setGravity(Gravity::Value gravity) { Gravity::getGravityComponents(gravity, &verticalGravity, &horizontalGravity); }

	void setFont(Font *font) { textSprite.setFont(font); }
	inline Font *getFont() { return textSprite.getFont(); }

	inline int getTextLinesCount() { return drawText(0, 0, false); }
	inline const char *getText() { return &text[0]; }

	// Returns text height when fitted to this view width.
	inline float getTextHeight() { return getTextLinesCount() * getFont()->lineHeight * textScaleY; }

	void getFormattedText(const char *text, std::vector<char> &formattedText);

	inline float getWidth() { return width; }
	inline float getHeight() { return height; }
	inline float getScaledWidth() { return width * scaleX; }
	inline float getScaledHeight() { return height * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + (width * scaleX); }
	inline float getTop() { return y + (height * scaleY); }
	inline float getCenterX() { return x + (width * scaleX * 0.5f); }
	inline float getCenterY() { return y + (height * scaleY * 0.5f); }
	inline float getDepth() { return z; }

	inline void setWidth(float width) { this->width = width; }
	inline void setHeight(float height) { this->height = height; }
	inline void setLeft(float left) { x = left; }
	inline void setBottom(float bottom) { y = bottom; }
	inline void setRight(float right) { x = right - (width * scaleX); }
	inline void setTop(float top) { y = top - (height * scaleY); }
	inline void setCenterX(float centerX) { x = centerX - (width * scaleX * 0.5f); }
	inline void setCenterY(float centerY) { y = centerY - (height * scaleY * 0.5f); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }
	inline void setColorMask(int colorMask) { this->colorMask = colorMask; }

	inline void setTextScale(float scaleX, float scaleY) { this->textScaleX = scaleX; this->textScaleY = scaleY; }

	inline void adjustDepths(float maxZ, float minZ) { this->z = maxZ; }
	void drawTransparentParts(SpriteBatch *spriteBatch);
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { drawTransparentParts(spriteBatch); };
};
