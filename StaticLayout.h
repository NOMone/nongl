#pragma once

#include "Layout.h"
#include "SpriteBatch.h"

class StaticLayout : public Layout {

	int transparentLastContextLostCount, opaqueLastContextLostCount, allLastContextLostCount;
	SpriteBatch staticTransparentSpriteBatch, staticOpaqueSpriteBatch, staticAllSpriteBatch;

public:

	StaticLayout();

	virtual void addView(View *view);

	virtual void drawOpaqueParts(SpriteBatch *spriteBatch);
	virtual void drawTransparentParts(SpriteBatch *spriteBatch);
	virtual void drawAllInOrder(SpriteBatch *spriteBatch);

	inline void invalidate() { transparentLastContextLostCount = opaqueLastContextLostCount = allLastContextLostCount = -1; }
};
