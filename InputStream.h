#pragma once

#include <stdint.h>
#include <vector>

namespace Ngl {

	class OutputStream;

	class InputStream {
	public:

		virtual inline ~InputStream() {}

		// Whether this stream has more data to deliver at the moment,
		virtual bool hasData() = 0;

		// Reads some (or all) data available from input stream. Returns
		// data size,
		virtual int32_t readData(std::vector<uint8_t> &outputVector, int32_t offsetInVector) = 0;
		inline int32_t readData(std::vector<uint8_t> &outputVector) { return readData(outputVector, (int32_t) outputVector.size()); }
		virtual int32_t readData(OutputStream &outputStream);
		int32_t readData(OutputStream &&outputStream) { return readData(outputStream); }

		// Blocks until hasData is no longer true, reads all data
		// possible. Returns data size,
		int32_t readAllData(std::vector<uint8_t> &outputVector, int32_t offsetInVector);
		inline int32_t readAllData(std::vector<uint8_t> &outputVector) {
			return readAllData(outputVector, (int32_t) outputVector.size());
		}
		int32_t readAllData(OutputStream &outputStream);
		inline int32_t readAllData(OutputStream &&outputStream) { return readAllData(outputStream); };

		virtual inline bool markSupported() { return false; }
		virtual void mark(int32_t readLimit=0);
		virtual void reset(); // Note: after resetting, the previous mark is undone. You have to
		                      // mark again to reset again to the same position.
	};

	class InputStreamWrapper : public InputStream {
		InputStream &inputStream;
	public:
		inline InputStreamWrapper(InputStream &inputStream) : inputStream(inputStream) {}

		// Wrap all the virtual methods of InputStream,
		inline bool hasData() { return inputStream.hasData(); }
		inline int32_t readData(std::vector<uint8_t> &outputVector, int32_t offset) { return inputStream.readData(outputVector, offset); }
		inline int32_t readData(OutputStream &outputStream) { return inputStream.readData(outputStream); }
		inline bool markSupported() { return inputStream.markSupported(); }
		inline void mark(int32_t readLimit=0) { inputStream.mark(readLimit); }
		inline void reset() { inputStream.reset(); }
	};
}
