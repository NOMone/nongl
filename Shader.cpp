#include "Shader.h"

#include "Nongl.h"
#include "utils.h"
#include "glWrapper.h"

Shader::Shader(const char *vertexShaderCode, const char *fragmentShaderCode) {
	setShaderCode(vertexShaderCode, fragmentShaderCode);
}

Shader::~Shader() {
	glDeleteProgram(programId);
}

void Shader::setShaderCode(const char *vertexShaderCode, const char *fragmentShaderCode) {
	programId = createProgram(vertexShaderCode, fragmentShaderCode);
}

bool Shader::use() {

	if (Nongl::getLastUsedShader() == this) return false;

	glUseProgram(programId);
	checkGlError("glUseProgram");

	Nongl::setLastUsedShader(this);
	return true;
}
