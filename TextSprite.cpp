#include "TextSprite.h"
#include "TextUtils.h"

#include <string.h>

TextSprite::TextSprite(const char *text, Font *font) {
	x = y = z = 0;
	scaleX = scaleY = 1;
	this->textLength = 0;

	if (font) {
		this->font = font;
	} else {
		this->font = Font::getDefaultFont();
	}

	setText(text);
}

TextSprite *TextSprite::clone() {

	TextSprite *newTextSprite = new TextSprite(&text[0], font);

	newTextSprite->x = x;
	newTextSprite->y = y;
	newTextSprite->z = z;
	newTextSprite->width = width;
	newTextSprite->height = height;
	newTextSprite->scaleX = scaleX;
	newTextSprite->scaleY = scaleY;

	return newTextSprite;
}

void TextSprite::setText(const char *text) {

	if (!text) {
		width = height = textLength = 0;
		return ;
	}

	// Allocate memory for new text and copy it,
	textLength = (int32_t) strlen(text);
	this->text.reserve(textLength+1);
	strcpy(&(this->text[0]), text);

	// Get new text dimensions,
	width = 0;
	height = font->getLineHeight();
	for (int i=0; i<textLength; i++) {
		const Ngl::TextureRegion *currentCharacterRegion = font->getCharacterRegion(text[i]);
		if (!currentCharacterRegion)
			continue;

		width += currentCharacterRegion->originalWidth;
		if (currentCharacterRegion->originalHeight > height) {
			height = currentCharacterRegion->originalHeight;
		}
	}

	width += font->characterSpacing * (textLength - 1);
}

TextSprite::LengthAndWidth TextSprite::getLengthAndWidthToLastFittingWordEnd(const char *text, float width, const Font *font) {

	if (!font) font = Font::getDefaultFont();

	int lastWhiteSpaceIndex = -1;
	int widthToLastWhiteSpace = 0;

	float currentTextWidth = - font->characterSpacing;
	int i;
	for (i=0; text[i]; i++) {

		if (isWhiteSpace(text[i])) {
			lastWhiteSpaceIndex = i;
			widthToLastWhiteSpace = currentTextWidth;
		}

		// Break if an end of line encountered,
		if (text[i] == '\n') {
			i++;
			break;
		}

		// Skip character if it has no region,
		const Ngl::TextureRegion *currentCharacterRegion = font->getCharacterRegion(text[i]);
		if (!currentCharacterRegion)
			continue;

		// Check width constraint,
		#define FLOATING_POINT_ERRORS_TOLERANCE 0.001f
		float newTextWidth = currentTextWidth + currentCharacterRegion->originalWidth + font->characterSpacing;
		if (newTextWidth > (width + FLOATING_POINT_ERRORS_TOLERANCE))
			break;

		currentTextWidth = newTextWidth;
	}

	// Prepare the result,
	LengthAndWidth lengthAndWidth;

	// If broken in the middle of a word,
	if (i && text[i] && (lastWhiteSpaceIndex != -1)) {
		lengthAndWidth.width = widthToLastWhiteSpace;
		lengthAndWidth.length = lastWhiteSpaceIndex+1; // Include the white-space character itself.
	} else {
		lengthAndWidth.width = currentTextWidth;
		lengthAndWidth.length = i;
	}

	return lengthAndWidth;
}

float TextSprite::getWidthToLastFittingWordEnd(const char *text, float width, const Font *font) {
	return getLengthAndWidthToLastFittingWordEnd(text, width, font).width;
}

int TextSprite::getLengthToLastFittingWordEnd(const char *text, float width, const Font *font) {
	return getLengthAndWidthToLastFittingWordEnd(text, width, font).length;
}

int TextSprite::getLengthToLastFittingWordEnd(const char *text, float width) {
	return getLengthToLastFittingWordEnd(text, width, font);
}

float TextSprite::getTextWidth(const char *text, float maxAllowedWidth, float scale, const Font *font) {

	// Since getLengthAndWidthToLastFittingWordEnd() assumes scale=1, we have to adjust width,
	maxAllowedWidth /= scale;

	LengthAndWidth lengthAndWidth;
	int currentChar = 0;
	float maxWidth = 0;
	do {
		lengthAndWidth = getLengthAndWidthToLastFittingWordEnd(&text[currentChar], maxAllowedWidth, font);

		currentChar += lengthAndWidth.length;
		if (lengthAndWidth.width > maxWidth) maxWidth = lengthAndWidth.width;
	} while (lengthAndWidth.length);

	return maxWidth * scale;
}

float TextSprite::getTextHeight(const char *text, float width, float scale, const Font *font) {
	if (!font) font = Font::getDefaultFont();
	return getTextLinesCount(text, width, scale, font) * font->getLineHeight() * scale;
}

float TextSprite::getTextLinesCount(const char *text, float width, float scale, const Font *font) {

	// If empty text, line count is 0,
	if (!text[0]) return 0;

	// Since getLengthAndWidthToLastFittingWordEnd() assumes scale=1, we have to adjust width,
	width /= scale;

	// If font not specified, use default font,
	if (!font) font = Font::getDefaultFont();

	LengthAndWidth lengthAndWidth;
	int currentChar = 0;
	int linesCount = 0;
	do {
		lengthAndWidth = getLengthAndWidthToLastFittingWordEnd(&text[currentChar], width, font);
		currentChar += lengthAndWidth.length;
		linesCount++;
	} while (lengthAndWidth.length);

	return linesCount;
}

void TextSprite::draw(SpriteBatch *spriteBatch) {

	float currentLeft = x;
	float currentBottom = y;

	for (int i=0; i<textLength; i++) {

		const Ngl::TextureRegion *currentCharacterRegion = font->getCharacterRegion(text[i]);
		if (!currentCharacterRegion)
			continue;

		characterSprite.setFromTextureRegion(currentCharacterRegion);
		characterSprite.setLeft(currentLeft);
		characterSprite.setBottom(currentBottom);
		characterSprite.z = getDepth();
		characterSprite.scaleX = scaleX;
		characterSprite.scaleY = scaleY;
		characterSprite.colorMask = colorMask;
		characterSprite.draw(spriteBatch);
		currentLeft += characterSprite.getScaledWidth() + (font->characterSpacing * scaleX);
	}
}
