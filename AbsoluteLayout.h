#pragma once

#include "Layout.h"

class AbsoluteLayout : public Layout {
protected:

	float x, y, z;
	float width, height;
	float scaleX, scaleY;
	int colorMask;

	virtual void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly);

public:

	bool clipping;

	inline AbsoluteLayout() { init(0, 0, 0, 0); }
	inline AbsoluteLayout(float x, float y, float width, float height, bool deletesChildrenOnDesruction=true) { init(x, y, width, height); }

	void init(float x, float y, float width, float height, bool deletesChildrenOnDestruction=true);

	virtual inline float getWidth() { return width; }
	virtual inline float getHeight() { return height; }
	virtual inline float getScaledWidth() { return width*scaleX; }
	virtual inline float getScaledHeight() { return height*scaleY; }
	virtual inline float getLeft() { return x; }
	virtual inline float getBottom() { return y; }
	virtual inline float getRight() { return x + getScaledWidth(); }
	virtual inline float getTop() { return y + getScaledHeight(); }
	virtual inline float getCenterX() { return x + (getScaledWidth() * 0.5f); }
	virtual inline float getCenterY() { return y + (getScaledHeight() * 0.5f); }
	virtual inline float getScaleX() { return scaleX; }
	virtual inline float getScaleY() { return scaleY; }
	virtual inline int getColorMask() { return colorMask; }

	virtual inline void setWidth(float width) { this->width = width; }
	virtual inline void setHeight(float height) { this->height = height; }
	virtual inline void setLeft(float left) { x = left; }
	virtual inline void setBottom(float bottom) { y = bottom; }
	virtual inline void setRight(float right) { x = right - getScaledWidth(); }
	virtual inline void setTop(float top) { y = top - getScaledHeight(); }
	virtual inline void setCenterX(float centerX) { x = centerX - (getScaledWidth() * 0.5f); }
	virtual inline void setCenterY(float centerY) { y = centerY - (getScaledHeight() * 0.5f); }
	virtual inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	virtual inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }
	virtual inline void setColorMask(int colorMask) { this->colorMask = colorMask; }

	virtual Ngl::Rect transformParentRectToLocalRect(const Ngl::Rect &rect);
	virtual void adjustDepths(float maxZ, float minZ);

	virtual inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, false); }
	virtual inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	virtual inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }

	virtual bool onTouchEvent(const TouchEvent *event);
};
