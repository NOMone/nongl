#pragma once

class Socket {

	int socketId;

public:

	Socket(const char *address, int port);
	~Socket();

	void *read();
	void write(const void *bytes, int sizeBytes);
};
