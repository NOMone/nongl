#pragma once

#include "TextureRegion.h"

#define FONT_CHARACTERS_COUNT 256

class Font {

	const Ngl::TextureRegion *characterRegions[FONT_CHARACTERS_COUNT];
	// TODO: use shared_ptr for this so it would be automatically removed when application exits,
	static Font *defaultFont;

public:

	float characterSpacing;
	int lineHeight;

	/** Should never be constructed before display manager loads the appropriate texture pack. */
	Font(const char *fontName);

	static Font *getDefaultFont();

	Ngl::Texture *getTexture();

	int getTextWidth(const char* text);
	int getTextHeight(const char* text);

	inline int getLineHeight() const { return lineHeight; }

	inline const Ngl::TextureRegion *getCharacterRegion(char character) const { return characterRegions[(unsigned char) character]; }
};
