#pragma once

// This is not much different from SegmentedImageView, but uses the same
// image instead of different segments.

#include <Nongl/ImageView.h>

namespace Ngl {

    class RepeatedImageView : public ImageView {
    protected:
        int32_t horizontalRepeats, verticalRepeats;
        float depth=-1;

        virtual void draw(SpriteBatch *spriteBatch);
    public:

        RepeatedImageView(const Ngl::TextureRegion *textureRegion, int32_t horizontalRepeats=1, int32_t verticalRepeats=1) :
            horizontalRepeats(horizontalRepeats), verticalRepeats(verticalRepeats), ImageView(textureRegion) {}

        float getWidth () { return sprite.width  * horizontalRepeats; }
        float getHeight() { return sprite.height *   verticalRepeats; }
        float getScaledWidth () { return sprite.getScaledWidth () * horizontalRepeats; }
        float getScaledHeight() { return sprite.getScaledHeight() *   verticalRepeats; }
        float getRight() { return sprite.getLeft  () + getScaledWidth (); }
        float getTop  () { return sprite.getBottom() + getScaledHeight(); }
        float getCenterX() { return sprite.getLeft  () + (0.5f * getScaledWidth ()); }
        float getCenterY() { return sprite.getBottom() + (0.5f * getScaledHeight()); }
        float getDepth() { return depth; }

        void setRight(float right) { sprite.setLeft  (right - getScaledWidth ()); }
        void setTop  (float   top) { sprite.setBottom(top   - getScaledHeight()); }
        void setCenterX(float centerX) { sprite.setLeft  (centerX - (0.5f * getScaledWidth ())); }
        void setCenterY(float centerY) { sprite.setBottom(centerY - (0.5f * getScaledHeight())); }
        void setCenter(float centerX, float centerY) {
            sprite.x = centerX - (0.5f*getScaledWidth ());
            sprite.y = centerY - (0.5f*getScaledHeight());
        }
        void setDepth(float depth) { this->depth = depth; }
    };
}
