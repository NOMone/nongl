#ifndef AUDIO_H
#define AUDIO_H

#include "SystemInterface.h"
#include "TextUtils.h"

class Audio {

	TextBuffer name;

public:

	inline Audio(const char *name) { this->name.append(name); }
	virtual inline ~Audio() {}

	inline const char *getName() { return name.getText(); }

	virtual inline void play() {};
	virtual inline void setLooping(bool looping) {};
	virtual inline void stop() {};
	virtual inline void pause() {};
	virtual inline void resume() {};
};

class Music : public Audio {
	int id;

public:

	inline Music(const char *filename, const char *musicName) : Audio(musicName) { id = javaLoadMusicFromAssets(filename); }
	inline ~Music() {
		if (id == -1) return ;
		javaStopMusic(id);
		javaDeleteMusic(id);
	}

	inline void play() { javaPlayMusic(id); }
	inline void setLooping(bool looping) { javaMusicSetLooping(id, looping); }
	inline void stop() { javaStopMusic(id); }
	inline void pause() { javaPauseMusic(id); }
	inline void resume() { javaResumeMusic(id); }
	inline void fadeOut(int milliSeconds) { javaFadeOutMusic(id, milliSeconds); }
};

class Activity;
class Sound : public Audio {
	int id;
public:

	inline Sound(const char *filename, const char *soundName) : Audio(soundName) { id = javaLoadSoundFromAssets(filename); }
	inline ~Sound() {
		if (id == -1) return ;
		javaDeleteSound(id);
	}

	inline void play() { javaPlaySound(id); }
	inline void play(float rate, float leftVolume=1, float rightVolume=1) { javaPlaySound(id, rate, leftVolume, rightVolume); }
	inline void setLooping(bool looping) { javaSoundSetLooping(id, looping); }
	inline void stop() { javaStopSound(id); }
	inline void pause() { javaPauseSound(id); }
	inline void resume() { javaResumeSound(id); }

	static inline void muteAll(bool mute) { javaMuteSounds(mute); }
	static void play(Sound *sound, float delayMillis=0, float rate=1, float leftVolume=1, float rightVolume=1, Activity *activity=0);
};

#endif
