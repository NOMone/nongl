#pragma once

#include "View.h"
#include "Sprite.h"

#include <vector>

namespace Ngl {
	class File;
}

/////////////////////////////
// Tile set
/////////////////////////////

class TileSet {

	int tileSetWidth, tileSetHeight;
	int tileWidth, tileHeight;
	char *regionNamesPrefix;
	std::vector<const Ngl::TextureRegion *> tileTextureRegions;

public:

	TileSet(int width, int height, int tileWidth, int tileHeight, const char *regionNamesPrefix);
	~TileSet();

	inline int getTileSetWidth () { return tileSetWidth ; }
	inline int getTileSetHeight() { return tileSetHeight; }
	inline int getTileWidth() { return tileWidth; }
	inline int getTileHeight() { return tileHeight; }

	const Ngl::TextureRegion *getTileTextureRegion(int tileIndex);
	inline const char *getTileSetName() { return regionNamesPrefix; }
};

/////////////////////////////
// Tile
/////////////////////////////

class Tile {
public:
	TileSet *tileSet;
	int32_t tileIndex;
	float displacementX, displacementY;

	Tile() {}
	Tile(TileSet *tileSet, int32_t tileIndex) : tileSet(tileSet), tileIndex(tileIndex) {}

	bool equals(const Tile *tile);
	void setFrom(const Tile *tile);
	bool isEmpty() { return tileSet==0; }
	const Ngl::TextureRegion *getTextureRegion();
};

/////////////////////////////
// Tile map layer
/////////////////////////////

class TileMapLayer {
	int width, height;
	std::vector<Tile> tiles;

	bool visible;
public:

	TileMapLayer(int width, int height);

	void copyFrom(const TileMapLayer *sourceLayer);  // Assumes both layers are the same size. Copies tiles only.

	inline Tile *getTile(int x, int y) { return &tiles[x + (y*width)]; }
	bool getTileXY(const Tile *tile, int *outTileX, int *outTileY);

	inline bool getVisible() { return visible; }
	inline void setVisible(bool visible) { this->visible = visible; }

	void clear(Tile *clearTile);
};

/////////////////////////////
// Tile map
/////////////////////////////

class TileMap {
	int mapWidth, mapHeight;
	int tileWidth, tileHeight;
	std::vector<TileMapLayer *> layers;

	int activeRectangleLeft, activeRectangleBottom;
	int activeRectangleWidth, activeRectangleHeight;

	float x, y;
	float scaleX, scaleY;
	float minZ, maxZ;

	Sprite drawingSprite;

public:

	bool drawGrid;

	TileMap();
	TileMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight, int layersCount);
	~TileMap();

	void set(int mapWidth, int mapHeight, int tileWidth, int tileHeight, int layersCount);
	void set(const TileMap *sourceTileMap);

	inline int getMapWidth () const { return mapWidth ; }
	inline int getMapHeight() const { return mapHeight; }
	inline int getTileWidth () const { return tileWidth ; }
	inline int getTileHeight() const { return tileHeight; }

	inline int32_t getLayersCount() { return (int32_t) layers.size(); }

	inline float getScaledWidth() { return mapWidth*tileWidth*scaleX; }
	inline float getScaledHeight() { return mapHeight*tileHeight*scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + getScaledWidth(); }
	inline float getTop() { return y + getScaledHeight();  }
	inline float getCenterX() { return x + (0.5f * getScaledWidth()); }
	inline float getCenterY() { return y + (0.5f * getScaledHeight()); }
	inline float getScaleX() { return scaleX; }
	inline float getScaleY() { return scaleY; }

	inline void setLeft(float left) { this->x = left; }
	inline void setBottom(float bottom) { this->y = bottom; }
	inline void setRight(float right) { this->x = right - getScaledWidth(); }
	inline void setTop(float top) { this->y = top - getScaledHeight(); }
	inline void setCenterX(float centerX) { this->x = centerX - (0.5f * getScaledWidth()); }
	inline void setCenterY(float centerY) { this->y = centerY - (0.5f * getScaledHeight()); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }

	int getDepthLayersCount();
	inline void adjustDepths(float maxZ, float minZ) { this->maxZ = maxZ; this->minZ = minZ; }
	inline float getMinDepth() { return minZ; }
	inline float getMaxDepth() { return maxZ; }

	void setActiveRectangleLeft(int activeRectangleLeft) { this->activeRectangleLeft = activeRectangleLeft; }
	void setActiveRectangleBottom(int activeRectangleBottom) { this->activeRectangleBottom = activeRectangleBottom; }
	void setActiveRectangleWidth(int activeRectangleWidth) { this->activeRectangleWidth = activeRectangleWidth; }
	void setActiveRectangleHeight(int activeRectangleHeight) { this->activeRectangleHeight = activeRectangleHeight; }

	void draw(SpriteBatch *spriteBatch, bool drawOpaque, bool drawTransparent);
	inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }
	inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, true); }

	inline Tile *getTile(int layerIndex, int x, int y) { return layers[layerIndex]->getTile(x, y); }
    inline const Tile *getTile(int layerIndex, int x, int y) const { return layers[layerIndex]->getTile(x, y); }
	Tile *getTileFromCoords(int layerIndex, float x, float y); // Map local coordinates.

	inline void clearLayer(int layerIndex, Tile *clearTile) { layers[layerIndex]->clear(clearTile); }
	bool getTileLocation(const Tile *tile, int *outTileX, int *outTileY);
	void getTileLocationFromCoords(float xCoord, float yCoord, int *outTileX, int *outTileY);

	inline bool getLayerVisible(int layerIndex) { return layers[layerIndex]->getVisible(); }
	inline void setLayerVisible(int layerIndex, bool visible) { layers[layerIndex]->setVisible(visible); }

	void pad(const std::vector<Tile> &layersPaddingTiles, int32_t horizontalPadding, int32_t verticalPadding);

	void save(const Ngl::File &file, std::vector<TileSet *> *tileSets=0);

	// Either load tilesets or provide previously loaded ones. Just don't provide empty vector,
	static TileMap *load(const Ngl::File &file, std::vector<TileSet *> &outTileSets, bool loadTileSets);
};

/////////////////////////////
// Tile map view
/////////////////////////////

class TileMapView : public View {

	TileMap *tileMap;

	float x, y;
	float scaleX, scaleY;
	float minZ, maxZ;

	void draw(SpriteBatch *spriteBatch, bool drawOpaque, bool drawTransparent);

public:

	TileMapView(TileMap *tileMap);

	inline float getScaledWidth() { return tileMap->getMapWidth() * tileMap->getTileWidth() * scaleX; }
	inline float getScaledHeight() { return tileMap->getMapHeight() * tileMap->getTileHeight() * scaleY; }
	inline float getLeft() { return x; }
	inline float getBottom() { return y; }
	inline float getRight() { return x + getScaledWidth(); }
	inline float getTop() { return y + getScaledHeight();  }
	inline float getCenterX() { return x + (0.5f * getScaledWidth()); }
	inline float getCenterY() { return y + (0.5f * getScaledHeight()); }
	inline float getScaleX() { return scaleX; }
	inline float getScaleY() { return scaleY; }

	inline void setLeft(float left) { this->x = left; }
	inline void setBottom(float bottom) { this->y = bottom; }
	inline void setRight(float right) { this->x = right - getScaledWidth(); }
	inline void setTop(float top) { this->y = top - getScaledHeight(); }
	inline void setCenterX(float centerX) { this->x = centerX - (0.5f * getScaledWidth()); }
	inline void setCenterY(float centerY) { this->y = centerY - (0.5f * getScaledHeight()); }
	inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }
	inline void setScale(float scaleX, float scaleY) { this->scaleX = scaleX; this->scaleY = scaleY; }

	inline int getDepthLayersCount() { return tileMap->getDepthLayersCount(); }
	inline void adjustDepths(float maxZ, float minZ) { this->maxZ = maxZ; this->minZ = minZ; }

	inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }
	inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, true); }

	Tile *getTileFromCoords(int layerIndex, float x, float y); // Map local coordinates.
};
