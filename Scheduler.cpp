#include "Scheduler.h"
#include "Nongl.h"
#include "Activity.h"

Scheduler::~Scheduler() {

	std::list<Task>::iterator tasksIterator = tasks.begin();
	for(; tasksIterator != tasks.end(); ) {
		tasksIterator = tasks.erase(tasksIterator);
	}
}

void Scheduler::schedule(Ngl::ManagedPointer<Runnable> task, float delay, void *data) {

	Task newTask;
	newTask.task = task;
	newTask.delay = delay;
	newTask.data = data;

	tasks.push_back(newTask);
}

void Scheduler::schedule(bool (* runnable)(void *data), float delay, void *data, Activity *activity) {
	Runnable *delayedRunnable = Runnable::createRunnable(runnable, activity);
	schedule(Ngl::shared_ptr<Runnable>(delayedRunnable), delay, data);
}

void Scheduler::updateAndDispatch(float elapsedTime) {

	std::list<Task>::iterator tasksIterator = tasks.begin();

	while (tasksIterator != tasks.end()) {

		bool runTask = false;
		Activity *taskActivity = tasksIterator->task->activity;
		if (!taskActivity) {
			tasksIterator->delay -= elapsedTime;
			runTask = true;
		} else if (taskActivity->isActive()) {
			tasksIterator->delay -= taskActivity->getFrameTime();
			runTask = true;
		}

		if (runTask && (tasksIterator->delay <= 0)) {
			tasksIterator->delay = 0;

			// Dispatch,
			if (tasksIterator->task->run(tasksIterator->data)) {
				tasksIterator = tasks.erase(tasksIterator);
			} else {
				++tasksIterator;
			}
		} else {
			++tasksIterator;
		}
	}
}

void Scheduler::removeTask(Runnable *task) {

	std::list<Task>::iterator tasksIterator = tasks.begin();

	for(; tasksIterator != tasks.end(); ) {
		Runnable *currentTask = tasksIterator->task.get();
		if (currentTask == task) {
			tasksIterator = tasks.erase(tasksIterator);
		} else {
			++tasksIterator;
		}
	}
}

void Scheduler::removeActivityTasks(Activity *activity) {

	std::list<Task>::iterator tasksIterator = tasks.begin();

	for(; tasksIterator != tasks.end(); ) {
		Runnable *currentRunnable = tasksIterator->task.get();
		if (currentRunnable->activity == activity) {

			// Delete without running,
			tasksIterator = tasks.erase(tasksIterator);
		} else {
			++tasksIterator;
		}
	}
}
