#pragma once

#include "Layout.h"
#include "Interpolator.h"

class ScrollableLayout : public Layout {

	float x, y, z;
	float width, height;

	float scrollWidth, scrollHeight;

	float scrollX, scrollY;
	float zoomScale;

	bool isReceivingGesture;
	float receivingGestureTimeout;
	float lastTouchX, lastTouchY;

	float scrollSpeedX, scrollSpeedY;
	Interpolator flickInterpolator;

	float lastScrollX, lastScrollY, lastZoomScale;
	Interpolator horizontalSpringInterpolator, verticalSpringInterpolator, zoomSpringInterpolator;

	Interpolator defaultScrollMovementInterpolator, *scrollMovementInterpolator;
	Interpolator   defaultZoomMovementInterpolator,   *zoomMovementInterpolator;
	float initialScrollX, initialScrollY;
	float targetScrollX, targetScrollY;
	float initialZoomScale, targetZoomScale;
	bool movementAboutCenter;

	bool isZooming;
	float lastZoomEventScale;

	void enforceBoundaries();

protected:
	void draw(SpriteBatch *spriteBatch, bool drawAllInOrder, bool drawTransparentOnly);

public:

	bool draggable;
	bool flickable;
	bool zoomable;
	bool clipping;
	bool horizontalSpring, verticalSpring, zoomSpring;

	float minZoomScale, maxZoomScale;

	float flickXMultiplier, flickYMultiplier;
	float snapToGridCellWidth, snapToGridCellHeight;

	ScrollableLayout();
	ScrollableLayout(
			float x, float y, float width, float height,
			float scrollWidth, float scrollHeight);

	void set(
			float x, float y, float width, float height,
			float scrollWidth, float scrollHeight);

	~ScrollableLayout();

	inline void setScrollX(float scrollX) { this->initialScrollX = this->targetScrollX = this->scrollX = scrollX; }
	inline void setScrollY(float scrollY) { this->initialScrollY = this->targetScrollY = this->scrollY = scrollY; }
	inline void setZoomScale(float zoomScale) { this->initialZoomScale = this->targetZoomScale = this->zoomScale = zoomScale; }
	inline void setScrollWidth(float scrollWidth) { this->scrollWidth = scrollWidth; }
	inline void setScrollHeight(float scrollHeight) { this->scrollHeight = scrollHeight; }
	inline void setFlickMultiplier(float xMultiplier, float yMultiplier) { this->flickXMultiplier = xMultiplier;  this->flickYMultiplier = yMultiplier; }

	inline float getScrollX() { return scrollX; }
	inline float getScrollY() { return scrollY; }
	inline float getZoomScale() { return zoomScale; }
	inline float getScrollWidth() { return scrollWidth; }
	inline float getScrollHeight() { return scrollHeight; }

	inline bool isScrolling() { return isReceivingGesture || (receivingGestureTimeout != 0) || (lastScrollX != scrollX) || (!scrollMovementInterpolator->hasFinished()); }
	inline int getClosestCellColumn() {
		if (snapToGridCellWidth == 0) return 0;
		return (int) ((scrollX + (0.5f * snapToGridCellWidth)) / snapToGridCellWidth);
	}
	inline int getClosestCellRow() {
		if (snapToGridCellHeight == 0) return 0;
		return (int) ((scrollY + (0.5f * snapToGridCellHeight)) / snapToGridCellHeight);
	}
	inline void moveToCell(int x, int y, float durationMillis=1, float delayMillis=0, Interpolator *interpolator=0) {
		moveTo(snapToGridCellWidth * x, snapToGridCellHeight * y, zoomScale, durationMillis, delayMillis, interpolator);
	}

	void zoomAbout(float zoomX, float zoomY, float zoomScale);
	void moveTo(
			float scrollX, float scrollY, float zoomScale, float durationMillis=1, float delayMillis=0,
			Interpolator *scrollInterpolator=0, Interpolator *zoomInterpolator=0, bool aboutCenter=true);

	virtual inline float getWidth() { return width; }
	virtual inline float getHeight() { return height; }
	virtual inline float getScaledWidth() { return width; }
	virtual inline float getScaledHeight() { return height; }
	virtual inline float getLeft() { return x; }
	virtual inline float getBottom() { return y; }
	virtual inline float getRight() { return x + width; }
	virtual inline float getTop() { return y + height; }
	virtual inline float getCenterX() { return x + (width * 0.5f); }
	virtual inline float getCenterY() { return y + (height * 0.5f); }

	virtual inline void setWidth(float width) { this->width = width; }
	virtual inline void setHeight(float height) { this->height = height; }
	virtual inline void setLeft(float left) { x = left; }
	virtual inline void setBottom(float bottom) { y = bottom; }
	virtual inline void setRight(float right) { x = right - width ; }
	virtual inline void setTop(float top) { y = top - height ; }
	virtual inline void setCenterX(float centerX) { x = centerX - (width * 0.5f); }
	virtual inline void setCenterY(float centerY) { y = centerY - (height * 0.5f); }
	virtual inline void setCenter(float centerX, float centerY) { setCenterX(centerX); setCenterY(centerY); }

	virtual Ngl::Rect transformParentRectToLocalRect(const Ngl::Rect &rect);
	virtual void adjustDepths(float maxZ, float minZ);

	virtual inline void drawOpaqueParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, false); }
	virtual inline void drawTransparentParts(SpriteBatch *spriteBatch) { draw(spriteBatch, false, true); }
	virtual inline void drawAllInOrder(SpriteBatch *spriteBatch) { draw(spriteBatch, true, false); }

	virtual void update(float elapsedTimeMillis);

	virtual bool onTouchEvent(const TouchEvent *event);
};
