#include "UniqueId.h"

#include "SystemLog.h"

UniqueId::UniqueId(int maxId) {
	this->maxId = maxId;
	this->currentId = 0;
	this->remainingIds = maxId;
}

bool UniqueId::isIdOccupied(int id) {
	std::list<int>::iterator iterator, endIterator;
	iterator = usedIds.begin();
	endIterator = usedIds.end();
	for (; iterator!=endIterator; ++iterator) {
		if ((*iterator) == id) return true;
	}

	return false;
}

bool UniqueId::findRemainingIds() {

	// Error check,
	if (usedIds.size() >= maxId) {
		LOGE("Alert! Can't generate new id, all ids consumed. Please fix before proceeding.");
		remainingIds = 0;
		return false;
	}

	// Find the first unoccupied id after the current one,
	while (isIdOccupied(currentId)) {
		currentId = (currentId + 1) % maxId;
	}

	// Find how many empty slots are available after current slot,
	remainingIds = maxId;

	std::list<int>::iterator iterator, endIterator;
	iterator = usedIds.begin();
	endIterator = usedIds.end();
	for (; iterator!=endIterator; ++iterator) {
		int emptySlotsCount = (*iterator) - currentId;
		if (emptySlotsCount < 0) emptySlotsCount += maxId;
		if (emptySlotsCount < remainingIds) remainingIds = emptySlotsCount;
	}

	return true;
}

int UniqueId::getNewUniqueId() {

	if (!remainingIds) {
		if (!findRemainingIds()) return 0;
	}
	remainingIds--;
	int uniqueId = currentId;
	usedIds.push_back(uniqueId);
	currentId = (currentId + 1) % maxId;

	return uniqueId;
}

void UniqueId::removeId(int id) {
	usedIds.remove(id);
}
