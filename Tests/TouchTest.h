#pragma once

#include "../Activity.h"

class TouchState;

namespace NonglTests {

	class TouchTestActivity : public Activity {
		double startTimeMillis;
		TouchState *touchState;
	public:

		static const char *activityName;
		TouchTestActivity();

		void onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ);

		bool onBackPressed();
		bool onTouchEvent(const TouchEvent *event);
	};
}
