#pragma once

#include "../Activity.h"

namespace NonglTests {

	class FileServicesTestActivity : public Activity {
	public:

		static const char *activityName;
		FileServicesTestActivity();

		bool onBackPressed();
	};
}
