#include "TouchTest.h"
using namespace NonglTests;

#include "../RotatingRaysView.h"
#include "../TextView.h"
#include "../utils.h"
#include "../TextUtils.h"
#include "../TouchState.h"

#include <vector>

#define TOUCH_CIRCLE_RADIUS 150

const char *TouchTestActivity::activityName = "Touch test activity";

TouchTestActivity::TouchTestActivity() : Activity(activityName) {
	this->startTimeMillis = getTimeMillis();
	this->touchState = 0;
}

void TouchTestActivity::onDrawFrame(float elapsedTimeMillis, float maxZ, float minZ) {

	Activity::onDrawFrame(elapsedTimeMillis, maxZ, minZ);
	if (!touchState) return;

	// Draw all the active touches,
	static std::vector<int> pointerColors;
	static CircleView pointerCircle(0, 0, TOUCH_CIRCLE_RADIUS, 0xffffffff, 32);

	static TextView pointerInfo("x: 0000.000000, y: 0000.000000\nLast updated: 000000.000000", rootLayout.getWidth(), 0.45f, Gravity::CENTER_LEFT);
	pointerInfo.setWidth(rootLayout.getWidth());

	int pointersCount = touchState->getPointersCount();
	float depthStep = (minZ - maxZ) / (pointersCount + 1);

	// Draw all down pointers,
	for (int i=0; i<pointersCount; i++) {
		PointerState &pointerState = touchState->getPointerState(i);
		if (pointerState.isDown) {

			// Make sure enough colors are present,
			while (pointerColors.size() < i+1) {
				pointerColors.push_back(0x80000000 | randomInteger(0x01000000));
			}

			// Draw pointer,
			float x = pointerState.x;
			float y = pointerState.y;
			float currentDepth = maxZ + (depthStep * i);
			pointerCircle.adjustDepths(currentDepth, currentDepth);
			pointerCircle.setColorMask(pointerColors[i]);
			pointerCircle.setCenter(x, y);
			pointerCircle.drawAllInOrder(&spriteBatch);

			// Draw pointer information,
			pointerInfo.adjustDepths(minZ, minZ);
			pointerInfo.setText(
					TextBuffer("x: ").append(x)
					.append(", y: ").append(y)
					.append("\nLast updated: ").append(pointerState.lastUpdatedTimeMillis - startTimeMillis)
					.getText());
			pointerInfo.setTop(rootLayout.getHeight() - (i*pointerInfo.getHeight()));
			pointerInfo.drawAllInOrder(&spriteBatch);
		}
	}
}

bool TouchTestActivity::onBackPressed() {
	finish();
	return false;
}

bool TouchTestActivity::onTouchEvent(const TouchEvent *event) {
	this->touchState = event->touchState;
	return true;
}
