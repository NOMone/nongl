#include "FileServicesTest.h"

#include "../SystemFileServices.h"
#include "../File.h"
#include "../TextView.h"
#include "../TextUtils.h"

using namespace Ngl;
using namespace NonglTests;

const char *FileServicesTestActivity::activityName = "File services test activity";

FileServicesTestActivity::FileServicesTestActivity() : Activity(activityName) {

	TextView *textView = new TextView(0, 0, rootLayout.getWidth(), rootLayout.getHeight(), Gravity::TOP_LEFT);
	textView->setTextScale(0.4f, 0.4f);
	rootLayout.addView(textView);

	TextBuffer message;
	message.append("besm Allah AlRa7maan AlRa7eem :)\n");
	message.append("\n");

	File file(FileLocation::SDCARD, "TestFile.txt");
	message.append("File exists: ").append(systemFileExists(file)).append('\n');

	message.append("Creating/over-writing file.\n");
	TextBuffer writtenData("besm Allah :)");
	systemWriteFile(file, writtenData.getConstText(), writtenData.getTextLength());

	message.append("File exists: ").append(systemFileExists(file)).append('\n');

	message.append("Reading file.\n");
	std::vector<uint8_t> readData;
	systemReadFile(file, readData, 0);
	readData.push_back(0);

	message.append("File contents: ").append((char *) &readData[0]).append('\n');
	message.append("File should contain: ").append(writtenData).append('\n');

	textView->setText(message);
}

bool FileServicesTestActivity::onBackPressed() {
	finish();
	return false;
}
