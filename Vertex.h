#pragma once

#include "Vectors.h"

#include <stdint.h>

namespace Ngl {

	class Vertex {

	public:

		virtual ~Vertex() {}

		virtual void initializeFromVertex(const Vertex &vertex)=0;

		template <typename VertexType>
		VertexType clone() const {
			VertexType clonedVertex;
			clonedVertex.initializeFromVertex(*this);
			return clonedVertex;
		}

		virtual Vec3 getXYZ(const Vec3 &defaultValue) const { return defaultValue; }
		virtual Vec2 getTextureCoordinates(const Vec2 &defaultValue) const { return defaultValue; }
		virtual Vec2 getTextureRegionCoordinates(const Vec2 &defaultValue) const { return defaultValue; }
		virtual Vec2 getTextureRegionSize(const Vec2 &defaultValue) const { return defaultValue; }
		virtual int32_t getColor(int32_t defaultValue) const { return defaultValue; }
	};

	class TexturedVertex : public Vertex {
	public:

		static const Vec3 defaultXYZ;
		static const Vec2 defaultTextureCoordinates;
		static const int32_t defaultColor;

		Vec3 xyz;
		Vec2 textureCoordinates;
		int32_t color=defaultColor;

		TexturedVertex() {}
		TexturedVertex(const Vec3 &xyz) : xyz(xyz) {}

		void initializeFromVertex(const Vertex &vertex) {
			this->xyz = vertex.getXYZ(defaultXYZ);
			this->textureCoordinates = vertex.getTextureCoordinates(defaultTextureCoordinates);
			this->color = vertex.getColor(defaultColor);
		}

		Vec3 getXYZ(const Vec3 &defaultValue) const { return xyz; }
		Vec2 getTextureCoordinates(const Vec2 &defaultValue) const { return textureCoordinates; }
		int32_t getColor(int32_t defaultValue) const { return color; }
	};

	class TextureRegionWrappedVertex : public Vertex {
	public:

		static const Vec2 defaultTextureRegionCoordinates;
		static const Vec2 defaultTextureRegionSize;

		Vec3 xyz;
		Vec2 textureCoordinates;
		int32_t color=TexturedVertex::defaultColor;
		Vec2 textureRegionCoordinates = defaultTextureRegionCoordinates;
		Vec2 textureRegionSize = defaultTextureRegionSize;

		TextureRegionWrappedVertex() {}
		TextureRegionWrappedVertex(const Vec3 &xyz) : xyz(xyz) {}

		void initializeFromVertex(const Vertex &vertex) {
			this->xyz = vertex.getXYZ(TexturedVertex::defaultXYZ);
			this->textureCoordinates = vertex.getTextureCoordinates(TexturedVertex::defaultTextureCoordinates);
			this->color = vertex.getColor(TexturedVertex::defaultColor);
			this->textureRegionCoordinates = vertex.getTextureRegionCoordinates(defaultTextureRegionCoordinates);
			this->textureRegionSize = vertex.getTextureRegionSize(defaultTextureRegionSize);
		}

		Vec3 getXYZ(const Vec3 &defaultValue) const { return xyz; }
		Vec2 getTextureCoordinates(const Vec2 &defaultValue) const { return textureCoordinates; }
		int32_t getColor(int32_t defaultValue) const { return color; }
		Vec2 getTextureRegionCoordinates(const Vec2 &defaultValue) const { return textureRegionCoordinates; }
		Vec2 getTextureRegionSize(const Vec2 &defaultValue) const { return textureRegionSize; }
	};
}

