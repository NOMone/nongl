#include "Particle.h"
#include "utils.h"

Particle::Particle(
		const Ngl::TextureRegion *textureRegion,
		float lifeTime,
		float emmitterX, float emmitterY,
		float minVelocityX, float maxVelocityX,
		float minVelocityY, float maxVelocityY,
		float gravityX, float gravityY,
		float minAngularVelocity, float maxAngularVelocity,
		float minScale, float maxScale,
		float minDelay, float maxDelay)
{
	this->lifeTime = lifeTime;
	this->gravityX = gravityX;
	this->gravityY = gravityY;

	sprite.setFromTextureRegion(textureRegion);
	sprite.scaleX = sprite.scaleY = randomFloatRange(minScale, maxScale);
	sprite.setCenter(emmitterX, emmitterY);

	velocityX = randomFloatRange(minVelocityX, maxVelocityX);
	velocityY = randomFloatRange(minVelocityY, maxVelocityY);
	angularVelocity = randomFloatRange(minAngularVelocity, maxAngularVelocity);
	delay = randomFloatRange(minDelay, maxDelay);
}

bool Particle::update(float elapsedTimeMillis)
{
	if (delay)
	{
		delay -= elapsedTimeMillis;
		if (delay < 0)
		{
			elapsedTimeMillis = -delay;
			delay = 0;
		}
		else
		{
			elapsedTimeMillis = 0;
		}
	}

	if (lifeTime)
	{
		lifeTime -= elapsedTimeMillis;
		if (lifeTime <= 0)
			lifeTime = 0;

		float elapsedTimeSeconds = elapsedTimeMillis / 1000.0f;

		velocityX += gravityX * elapsedTimeSeconds;
		velocityY += gravityY * elapsedTimeSeconds;

		sprite.x += velocityX * elapsedTimeSeconds;
		sprite.y += velocityY * elapsedTimeSeconds;

		sprite.rotation += angularVelocity * elapsedTimeSeconds;

		return false;
	}
	return true;
}
