#include "GestureListener.h"

void TouchEvent::setAsTouchDown(int pointerId, float x, float y) {
	this->type = TOUCH_DOWN;
	this->pointerId = pointerId;
	this->x = this->downX = x;
	this->y = this->downY = y;
}

void TouchEvent::setAsTouchDrag(int pointerId, float downX, float downY, float x, float y, float maxDraggedDistanceCms) {
	this->type = TOUCH_DRAG;
	this->pointerId = pointerId;
	this->downX = downX;
	this->downY = downY;
	this->x = x;
	this->y = y;
	this->maxDraggedDistanceCms = maxDraggedDistanceCms;
}

void TouchEvent::setAsTouchDragEnd(int pointerId, float downX, float downY, float upX, float upY, float maxDraggedDistanceCms) {
	this->type = TOUCH_DRAG_END;
	this->pointerId = pointerId;
	this->downX = downX;
	this->downY = downY;
	this->x = upX;
	this->y = upY;
	this->maxDraggedDistanceCms = maxDraggedDistanceCms;
}

void TouchEvent::setAsTouchUp(int pointerId, float x, float y, float maxDraggedDistanceCms) {
	this->type = TOUCH_UP;
	this->pointerId = pointerId;
	this->x = x;
	this->y = y;
	this->maxDraggedDistanceCms = maxDraggedDistanceCms;
}

void TouchEvent::setAsScroll(float x, float y, float xOffset, float yOffset) {
	this->type = SCROLL;
	this->pointerId = 0;
	this->x = this->downX = x;
	this->y = this->downY = y;
	this->xMagnitude = xOffset;
	this->yMagnitude = yOffset;
}

void TouchEvent::setAsClick(int pointerId, float x, float y) {
	this->type = CLICK;
	this->pointerId = pointerId;
	this->x = x;
	this->y = y;
}

void TouchEvent::setAsFlick(int pointerId, float x, float y, float xMagnitude, float yMagnitude) {
	this->type = FLICK;
	this->pointerId = pointerId;
	this->x = x;
	this->y = y;
	this->xMagnitude = xMagnitude;
	this->yMagnitude = yMagnitude;
}

void TouchEvent::setAsZoom(float x, float y, float ratio) {
	this->type = ZOOM;
	this->x = x;
	this->y = y;
	this->ratio = ratio;
}

void TouchEvent::setAsZoomEnd(float x, float y, float ratio) {
	this->type = ZOOM_END;
	this->x = x;
	this->y = y;
	this->ratio = ratio;
}
